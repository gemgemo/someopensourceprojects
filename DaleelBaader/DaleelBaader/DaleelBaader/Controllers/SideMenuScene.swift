
import UIKit

import Localize_Swift

final class SideMenuScene: BaseController
{

    // MARK: - Constants
    
    fileprivate let dataSource = ["sideMenuMainTitle", "sideMenuCallUsTitle", "sideMenuShareAppTitle", "langActionSheetTitle"]
    
    //MARK: - Variables
    
    // MARK: - Outlets
    
    @IBOutlet private weak var titleLogoWidth: NSLayoutConstraint!
    @IBOutlet private weak var titleViewHeight: NSLayoutConstraint!
    @IBOutlet fileprivate weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    internal override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        titleLogoWidth.constant = revealViewController().rightViewRevealWidth
        titleViewHeight.constant = view.bounds.height*0.20
    }
    
    
    //MARK: - Actions
    
    @objc fileprivate func updateUI()-> void {
        tableView.reloadData()
    }
    
}


// MARK: - Table view delegate & data source 

extension SideMenuScene: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.SideMenu) as? SideMenuCell {
            cell.rowName = dataSource[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        revealViewController().rightRevealToggle(animated: true)
        print(indexPath.row)
        switch (indexPath.row) {
            
        case 1:
            if let contactUsScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.ContactUs) as? ContactUsScene {
                //navigationController?.pushViewController(contactUsScreen, animated: true)
                present(UINavigationController(rootViewController: contactUsScreen), animated: true, completion: nil)
            }
            
        case 2: // TODO: Share app
            share(this: [Constants.Misc.AppLink])
            
        case 3: // TODO: cahnge language
            changeLanguage()
        default:
            break
        }
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
}



final class SideMenuCell: UITableViewCell
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    internal var rowName: string? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var rowTitle: UILabel!
    
    
    // MARK: - functions
    
    private func updateUI()-> void {
        guard let title = rowName else { return }
        rowTitle.text = title.localized()
    }
    
    //MARK: - Actions
    
}






















