
import UIKit

@IBDesignable
final class TextField: UITextField
{

    @IBInspectable
    internal var image: UIImage = UIImage() {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    internal var placeHolderColor: UIColor = .gray {
        didSet {
            setNeedsDisplay()
        }
    }
    
    
    
    internal override func draw(_ rect: CGRect) {
        borderStyle = .none
        rightViewMode = .always
        let rightImage = UIImageView(frame: Rect(x: 5, y: 0, width: 16, height: 16))
        rightImage.image = image
        rightImage.contentMode = .scaleAspectFit
        let view = UIView(frame: Rect(x: 0, y: 0, width: 27, height: 20))
        view.addSubview(rightImage)
        rightView = view
        let bottomBorder = UIView()
        bottomBorder.translatesAutoresizingMaskIntoConstraints = false
        bottomBorder.backgroundColor = Color(white: 0.80, alpha: 0.9)
        addSubview(bottomBorder)
        
        bottomBorder.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        bottomBorder.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: 1.0).isActive = true
        bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0.0).isActive = true
        attributedPlaceholder = NSAttributedString(string: placeholder ?? string.empty, attributes: [NSForegroundColorAttributeName: placeHolderColor])
    }
    

}
