
import Foundation
import Localize_Swift

final class StatisticsNumbers
{
    
    
    internal var id: int?, title, subTitle: string?, icon: UIImage?
    
    init(id: int, title: string, subTitle: string, icon: UIImage) {
        self.id = id
        self.title = title
        self.subTitle = subTitle
        self.icon = icon
    }
    
    internal class func setItems()-> [StatisticsNumbers] {
        var items = [StatisticsNumbers]()
        items.append(StatisticsNumbers(id: 1, title: "Listofthenamesofstudentsexcellinggirlslan".localized(), subTitle: "Number of honorees include each year from studentsgirlslan".localized(), icon: #imageLiteral(resourceName: "4")))
        items.append(StatisticsNumbers(id: 2, title: "Listofthenamesofstudentsexcellingboyslan".localized(), subTitle: "Number of honorees include each year from studentsboyeslan".localized(), icon: #imageLiteral(resourceName: "2")))
        items.append(StatisticsNumbers(id: 3, title: "List of names of candidates teachersgirlslan".localized(), subTitle: "Number of honorees include each of the teachersgirleslan".localized(), icon: #imageLiteral(resourceName: "1")))
        items.append(StatisticsNumbers(id: 4, title: "List of names of candidates teachersboyeslan".localized(), subTitle: "Number of honorees include each of the teachersboyeslan".localized(), icon: #imageLiteral(resourceName: "3")))
        items.append(StatisticsNumbers(id: 5, title: "qurankareemlanboyestitle".localized(), subTitle: "honersnumberslan".localized(), icon: #imageLiteral(resourceName: "5")))
        items.append(StatisticsNumbers(id: 6, title: "qurankareemlangirlestitle".localized(), subTitle: "honersnumberslan".localized(), icon: #imageLiteral(resourceName: "5")))
        return items
    }
    
    
}








