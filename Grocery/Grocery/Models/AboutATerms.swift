
import Foundation
import Gemo


final class AboutATerms: Mapper<any>
{
    
    internal var id, content: string?
    
    internal override func mapping(mapper: Mapper<any>) {
        id = mapper["id"]?.string
        content = mapper["content"]?.string
    }
    
    
    internal class func getData(from link: string, completion: @escaping(AboutATerms)->())-> void {
        Gemo.gem.startSpinning()
        Http.request(link: link, method: .post, parameters: [Misc.MainKey: Misc.MainValue, Misc.LanguageParameterName: Localizer.instance.current.rawValue])
            .response { (response) in
                let error = response.error
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("get about or terms error: \(String(describing: error))")
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
//                print("terms or about data: \(response.jsonString ?? .empty)")
                guard let json = (response.result as? [Dictionary<string, any>])?.first else {
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized())
                    return
                }
                completion(AboutATerms(JSON: json))
        }
    }
    
    
}












































