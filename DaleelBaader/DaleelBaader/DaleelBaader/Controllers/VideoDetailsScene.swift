
import UIKit

import Localize_Swift
import AVFoundation

final class VideoDetailsScene: BaseController
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    internal var movie: Movie?
    fileprivate var player: AVPlayer?, playerLayer: AVPlayerLayer?
    
    fileprivate lazy var indicator: UIActivityIndicatorView = {
       let loader = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        loader.hidesWhenStopped = true
        loader.center = self.play.center
        return loader
    }()
    
    // MARK: - Outlets
    
    @IBOutlet private weak var playerView: UIView!
    @IBOutlet private weak var playerViewHeight: NSLayoutConstraint!
    @IBOutlet fileprivate weak var details: UITextView!
    @IBOutlet fileprivate weak var play: UIButton!
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBar()
        if let vidLink = movie?.link, vidLink.contains("youtube.com") {
            play.isHidden = true
            let videoId = vidLink.components(separatedBy: "=").last ?? string.empty
            let webView = UIWebView()
            webView.delegate = self
            webView.translatesAutoresizingMaskIntoConstraints = false
            let width = view.bounds.width-10, height = view.bounds.height*0.45-35
            let videoFrame = "<iframe width=\"\(width)\" height=\"\(height)\" src=\"https://www.youtube.com/embed/\(videoId)?&playsinline=1\" frameborder=\"0\" allowfullscreen></iframe>"
            webView.allowsInlineMediaPlayback = true
            webView.loadHTMLString(videoFrame, baseURL: nil)
            playerView.addSubview(webView)
            webView.topAnchor.constraint(equalTo: playerView.topAnchor, constant: 0).isActive = true
            webView.rightAnchor.constraint(equalTo: playerView.rightAnchor, constant: 0).isActive = true
            webView.bottomAnchor.constraint(equalTo: playerView.bottomAnchor, constant: 0).isActive = true
            webView.leftAnchor.constraint(equalTo: playerView.leftAnchor, constant: 0).isActive = true
            
        } /*else {
            playerView.addSubview(indicator)
        }*/
    }
    
    internal override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        playerViewHeight.constant = view.bounds.height*0.45
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        player?.pause()
//        playerLayer?.removeFromSuperlayer()
    }
    
    //MARK: - Actions
    
    @IBAction private func playOnCLick(_ sender: UIButton) {
        print("play")
        /*sender.isHidden = true
        indicator.startAnimating()
        //"http://techslides.com/demos/sample-videos/small.mp4"
        guard let videoLink = movie?.link, let url = URL(string: "http://techslides.com/demos/sample-videos/small.mp4") else {
            dialog(with: Constants.Misc.ErrorMessage)
            return
        }
        
        player = AVPlayer(url: url)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer?.frame = playerView.bounds
        playerView.layer.addSublayer(playerLayer!)
        player?.play()
 */
    }
    
}


// MARK: - Helper functions 

extension VideoDetailsScene
{
    
    fileprivate func updateNavigationBar()-> void {
        navigationItem.title = movie?.title ?? string.empty
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(goBack(_:)))
        details.text = movie?.content ?? string.empty
    }
    
    
}



// MARK: - Web view delegate functions

extension VideoDetailsScene: UIWebViewDelegate
{
    
    internal func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.showIndicator(by: true)
    }
    
    internal func webViewDidFinishLoad(_ webView: UIWebView) {
        UIApplication.showIndicator(by: false)
    }
    
    internal func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        UIApplication.showIndicator(by: true)
    }
    
    
}



























