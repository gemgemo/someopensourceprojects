
import UIKit
import Gemo
import GoogleMaps


class BillsDetails: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var bill: Bill?
    
    // MARK: - Outlets
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var carPhoto: UIImageView!
    @IBOutlet weak var carIcon: UIImageView!
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var lblCarType: UILabel!
    @IBOutlet weak var lblPrand: UILabel!
    @IBOutlet weak var lblModel: UILabel!
    @IBOutlet weak var lblSpentFuelType: UILabel!
    @IBOutlet weak var lblUser: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblProccessNo: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblLitersNo: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblPaymentWay: UILabel!
    @IBOutlet weak var lblPayMentStatus: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var carTypeIcon: UIImageView!
    @IBOutlet weak var lblALetters: UILabel!
    @IBOutlet weak var lblELetters: UILabel!
    @IBOutlet weak var lblANumbers: UILabel!
    @IBOutlet weak var lblENumbers: UILabel!
    
    
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        super.updateUi()
        mapView.delegate = self
        setupNavigationBar(isLogo: false, isMainPage: false, title: Localization.BillDetails.localized())
        lblCarType.text = "\(Localization.CarType.localized())   \(bill?.carType ?? .empty)"
        lblPrand.text = "\(Localization.Prand.localized())   \(bill?.carPrand ?? .empty)"
        lblModel.text = "\(Localization.Model.localized())   \(bill?.carModel ?? .empty)"
        lblSpentFuelType.text = "\(Localization.SpentFuelType.localized())   \(bill?.fuel ?? .empty)"
        lblUser.text = Localization.User.localized()
        lblUserName.text = "\(bill?.name ?? .empty)"
        userPhoto.loadImage(from: bill?.userPhoto ?? .empty)
        carTypeIcon.loadImage(from: bill?.carTypePhoto ?? .empty)
        carIcon.loadImage(from: bill?.carIcon ?? .empty)
        carPhoto.loadImage(from: bill?.carPhoto ?? .empty)
        lblCarName.text = "\(bill?.carModel ?? .empty)-\(bill?.carPrand ?? .empty)"
        lblCode.text = "\(Localization.Code.localized())   \(bill?.carCode ?? .empty)"
        lblProccessNo.text = "\(Localization.ProccessNo.localized())   \("??")"
        lblOrderDate.text = "\(Localization.OrderDate.localized())   \(bill?.timestamp ?? .empty)"
        lblLitersNo.text = "\(Localization.LitresNo.localized())   \(bill?.liters ?? .empty)"
        lblPrice.text = "\(Localization.Price.localized())   \(bill?.price ?? .empty)"
        lblPaymentWay.text = "\(Localization.PaymentWay.localized())   \("??")"
        if let latitude = bill?.latitude?.toDouble, let longitude = bill?.longitude?.toDouble {
            let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            mapView.clear()
            let marker = GMSMarker()
            marker.icon = #imageLiteral(resourceName: "marker-map1")
            marker.position = coordinate
            marker.map = mapView
            marker.appearAnimation = .pop
            mapView.camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: 8.0)
        }
        lblALetters.text = bill?.aLetters ?? .empty
        lblANumbers.text = bill?.aNumbers ?? .empty
        lblELetters.text = bill?.eLetters ?? .empty
        lblENumbers.text = bill?.eNumbers ?? .empty
        lblPayMentStatus.text = "??"
        lblALetters.textAlignment = .center
        lblELetters.textAlignment = .center
        lblANumbers.textAlignment = .center
        lblENumbers.textAlignment = .center

    }
    
    
    // MARK: - Actions

}


// MARK: - map view delegate functions

extension BillsDetails: GMSMapViewDelegate {
      
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let markerView = UIView(frame: Rect(x: 0, y: 0, width: 220, height: 90))
        markerView.setCorner(radius: 4)
        markerView.setShadow(with: .zero, radius: 1.0, opacity: 0.4, color: .black)
        markerView.backgroundColor = .white
        markerView.clipsToBounds = true
        let icon = UIImageView(image: #imageLiteral(resourceName: "ic_gas_station"))
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.contentMode = .scaleAspectFit
        markerView.addSubview(icon)
        icon.topAnchor.constraint(equalTo: markerView.topAnchor, constant: 8).isActive = true
        icon.widthAnchor.constraint(equalToConstant: 40).isActive = true
        icon.heightAnchor.constraint(equalToConstant: 40).isActive = true
        icon.rightAnchor.constraint(equalTo: markerView.rightAnchor, constant: -8).isActive = true
        let lblName = UILabel()
        lblName.text = bill?.name ?? .empty
        lblName.translatesAutoresizingMaskIntoConstraints = false
        lblName.font = Font.notoKufi(of: 15)
        lblName.textColor = .black
        lblName.adjustsFontSizeToFitWidth = true
        lblName.textAlignment = .center
        markerView.addSubview(lblName)
        lblName.topAnchor.constraint(equalTo: icon.topAnchor, constant: 0).isActive = true
        lblName.rightAnchor.constraint(equalTo: icon.leftAnchor, constant: -8).isActive = true
        lblName.leftAnchor.constraint(equalTo: markerView.leftAnchor, constant: 8).isActive = true
        let lblAddress = UILabel()
        lblAddress.text = bill?.address ?? .empty
        lblAddress.translatesAutoresizingMaskIntoConstraints = false
        lblAddress.font = Font.notoKufi(of: 15)
        lblAddress.numberOfLines = 2
        lblAddress.textColor = .black
        lblAddress.textAlignment = .center
        lblAddress.adjustsFontSizeToFitWidth = true
        markerView.addSubview(lblAddress)
        lblAddress.rightAnchor.constraint(equalTo: markerView.rightAnchor, constant: -8).isActive = true
        lblAddress.bottomAnchor.constraint(equalTo: markerView.bottomAnchor, constant: 8).isActive = true
        lblAddress.leftAnchor.constraint(equalTo: markerView.leftAnchor, constant: 8).isActive = true
        lblAddress.topAnchor.constraint(equalTo: lblName.bottomAnchor, constant: 8).isActive = true
        return markerView
    }
    
}


























