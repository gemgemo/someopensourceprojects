
import UIKit

import Localize_Swift

final class MainScene: BaseController
{

    // MARK: - Constatnts
    
    
    // MARK: - Variables
    
    fileprivate var dataSource = Array<MainContent>() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var openMenu: UIBarButtonItem!
    @IBOutlet fileprivate weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
        }
    }

    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupSideMenu()
        
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(setupNavigationBar), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: - Actions
    
    

}



// MARK: - Table view delegate & data source functions 

extension MainScene: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.MainCell) as? MainCell {
            if (indexPath.row == Number.zero.intValue) {
                cell.picture.contentMode = .scaleAspectFill
                cell.imageTop.constant = 0
                cell.imageRight.constant = 0
                cell.imageBottom.constant = 0
                cell.imageLeft.constant = 0
            } else {
                cell.picture.contentMode = .scaleAspectFit
                cell.imageTop.constant = 20
                cell.imageRight.constant = 20
                cell.imageBottom.constant = 20
                cell.imageLeft.constant = 20
            }             
            cell.content = dataSource[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard revealViewController().frontViewPosition == .left else {
            revealViewController().rightRevealToggle(animated: true)
            return
        }
        switch (indexPath.row) {
            
        case 0: // goto manager screen
            if let managerScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.ManagerMessage) as? ManagerMessageScene {
                navigationController?.pushViewController(managerScreen, animated: true)
            }
            
        case 1: // about us
            if let aboutPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.AboutUs) as? AboutUsScene {
                present(UINavigationController(rootViewController: aboutPage), animated: true, completion: nil)
            }
            
        case 2: // open services
            gotoPDFViewer(with: Constants.BackendURLs.OurServices)
            
        case 3: // goto alshaer almuqdasa
            if let alshaaerScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.AlshaaerAlmuqdasa) as? AlshaaerAlmuqdasaScene {
                navigationController?.pushViewController(alshaaerScreen, animated: true)
            }
            
        case 4: // goto scoute info
            navigateToInfo(with: "4", and: "ScoutServicetitle")
            
        case 5: // goto health info
            navigateToInfo(with: (Localize.currentLanguage() == Constants.Misc.ArabicSign) ? Constants.BackendURLs.HealthServicesAR : Constants.BackendURLs.HealthServicesAR, and: "HealthServicestitle")
            
        case 6: // goto hotel info
            navigateToInfo(with: (Localize.currentLanguage() == Constants.Misc.ArabicSign) ? Constants.BackendURLs.HotelServicesAR : Constants.BackendURLs.HotelServicesEN, and: "HotelServicestitle")
            
        case 7: // goto Eaasha info
            navigateToInfo(with: (Localize.currentLanguage() == Constants.Misc.ArabicSign) ? Constants.BackendURLs.EashaaServicesAR : Constants.BackendURLs.EashaaServicesEN, and: "Servicessubsistencetitle")
            
        case 8: // goto transports
            if let transportScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Transport) as? TransportScene {
                navigationController?.pushViewController(transportScreen, animated: true)
            }
            
        case 9: // goto videos
            if let videosScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Videos) as? VideosScene {
                navigationController?.pushViewController(videosScreen, animated: true)
            }
            
        default :
            break
        }
    }
    
}


// MARK: - Helper Functions 

extension MainScene
{
    
    @objc fileprivate func setupNavigationBar()-> void {
        UIApplication.shared.isStatusBarHidden = false
        navigationItem.title = "appTitle".localized()
        loadData()
    }
    
    @objc fileprivate func setupSideMenu()-> void {
        if (revealViewController() != nil) {
            openMenu.target = revealViewController()
            openMenu.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            view.addGestureRecognizer(revealViewController().panGestureRecognizer())
            revealViewController().delegate = self
        }
    }
    
    private func loadData()-> void {
        dataSource = MainContent.getData()
    }
    
    fileprivate func navigateToInfo(with link: string, and barTitle: string)-> void {
        if let servicesScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Services) as? ServicesScene {
            servicesScreen.dataSourceLink = link
            servicesScreen.navTitle = barTitle
            navigationController?.pushViewController(servicesScreen, animated: true)
        }
    }
    
    
}



extension MainScene: SWRevealViewControllerDelegate
{
    
    internal func revealController(_ revealController: SWRevealViewController!, didMoveTo position: FrontViewPosition) {
        switch (position) {
        case .left:
            print("left closed")
            
        case .leftSide:
            print("left side opend")
            
        case .leftSideMost:
            print("leftSideMost")
            
        case .leftSideMostRemoved:
            print("leftSideMostRemoved")
            
        case .right:
            print("right")
            
        case .rightMost:
            print("right most")
            
        case .rightMostRemoved:
            print("rightMostRemoved")
        }
    }
    
}



















