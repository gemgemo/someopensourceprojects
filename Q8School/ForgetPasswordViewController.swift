//
//  ForgetPasswordViewController.swift
//  Q8School
//
//  Created by mac on 12/11/16.
//  Copyright © 2016 Atiaf. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: UIViewController {
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var forgetView: UIView!
    var dataFound: Bool!
    var message: String!
    var error:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        confirmBtn.layer.cornerRadius = 4
        confirmBtn.layer.masksToBounds = true
        if Settings.lang == "ar"
        {
            confirmBtn.setTitle("موافق", for: .normal)
            closeBtn.setTitle("الغاء", for: .normal)

            emailText.placeholder = "البريد الالكتروني"
        }
        forgetView.layer.cornerRadius = 10
        forgetView.layer.masksToBounds = true
        confirmBtn.layer.cornerRadius = 10
        confirmBtn.layer.masksToBounds = true

        // Do any additional setup after loading the view.
    }

    
    func forgetPassword(_ lang:String,_ email: String)
    {
        //shadowView.isHidden = false
        //loadingView.isHidden = false
       // loadingView.layer.cornerRadius = 4
      //  loadingView.layer.masksToBounds = true
       // loadingActivity.startAnimating()
        let request = NSMutableURLRequest(url: URL(string:Settings.ForgetPassword)!)
        request.httpMethod = "POST"
        let postString = "lang=\(lang)&email=\(email)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
            
            if let responseData = data {
                
                do {
                    
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                    print("ddd\(json)")
                    self.dataFound = json["Success"] as? Bool
                    if self.dataFound!
                    {
                        self.message = json["Message"] as! String

                    }
                    else
                    {
                        self.error = json["Message"] as! String
                    }
                    
                    DispatchQueue.main.async
                        {
                            if self.dataFound!
                            {
                                self.displayAlertMessage(self.message, "")
                            }
                            else
                            {
                                self.displayAlertMessage("", self.error)
                            }
                            
                    }
                    
                }
                catch
                {
                    print ("Could not Serialize")
                }
                
            }
            
        }
        task.resume()

    }
    func displayAlertMessage(_ Message:String,_ Error: String)
    {
        var t = ""
        var b = ""
        
        if Settings.lang == "ar"{
            t = "تنبيه"
            b = "إغلاق"
        }
        else{
            t = "Alert"
            b = "Close"
        }
        if Error == ""
        {
            let myAlert = UIAlertController(title: t, message:Message, preferredStyle: UIAlertControllerStyle.alert);
            
            let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "SignInPageViewController") as!                             SignInPageViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
            })
            myAlert.addAction(okAction);
            
            self.present(myAlert, animated: true, completion: nil)
            
        }
        else
        {
            let myAlert = UIAlertController(title: t, message:Error, preferredStyle: UIAlertControllerStyle.alert);
            
            let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler:nil)
            
            myAlert.addAction(okAction);
            
            self.present(myAlert, animated: true, completion: nil)
            
            
        }
        
    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func closeBtn(_ sender: Any)
    {
        self.view.removeFromSuperview()

    }
    
    @IBAction func confirmButton(_ sender: Any)
    {
        forgetPassword(Settings.lang, emailText.text!)
    }

    
    
    @IBAction func textReturn(_ sender: UITextField) {
        self.view.endEditing(true)
    }
    
    
    
    
}
