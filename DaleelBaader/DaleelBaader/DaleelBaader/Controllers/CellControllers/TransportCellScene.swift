
import UIKit

import Localize_Swift


final class TransportCellScene: UITableViewCell
{

    // MARK: - Constants
    
    //MARK: - Variables
    
    internal var transport: Transport? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var name: UILabel!
    @IBOutlet private weak var icon: UIImageView!
    @IBOutlet private weak var details: UILabel!
    @IBOutlet private weak var btnDownload: UIButton! {
        didSet {
            btnDownload.layer.cornerRadius = 5.0
            btnDownload.clipsToBounds = true
            btnDownload.setTitle("downloadButtonTitle".localized(), for: .normal)
        }
    }
    
    @IBOutlet private weak var container: UIView! {
        didSet {
            container.layer.borderWidth = 1.0
            container.layer.borderColor = Color(white: 0.80, alpha: 0.8).cgColor
        }
    }
    @IBOutlet private weak var iconContianer: UIView! {
        didSet {
            iconContianer.layer.cornerRadius = 8
            iconContianer.layer.borderWidth = 1.0
            iconContianer.layer.borderColor = Color(white: 0.80, alpha: 0.9).cgColor
            iconContianer.clipsToBounds = true
        }
    }
    
    
    // MARK: - Overridden functions
    
    private func updateUI()-> void {
        if (Localize.currentLanguage() == Constants.Misc.ArabicSign) {
            if let trasnName = transport?.arabicName {
                name.text = trasnName
            }
            
            if let transInfo = transport?.arabicInfo {
                details.text = transInfo
            }
        } else {
            if let trasnName = transport?.englishName {
                name.text = trasnName
            }
            
            if let transInfo = transport?.englishInfo {
                details.text = transInfo
            }
        }
        if let picture = transport?.iconName {
            icon.image = UIImage(named: picture)
        }
    }
    
    //MARK: - Actions
    
    
    @IBAction private func downloadAppOnClick(_ sender: UIButton) {
        if let link = transport?.link, let url = URL(string: link), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
    
}
