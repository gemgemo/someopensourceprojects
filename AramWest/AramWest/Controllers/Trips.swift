
import UIKit
import Localize_Swift

final class Trips: BaseController
{
    // MARK: - Constants
    
    // MARK: - Variables
    
    fileprivate var dataSource = Array<Trip>() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.trips.reloadData()
            }
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var lblSubtitle: UILabel! {
        didSet {
            lblSubtitle.text = .empty
        }
    }
    @IBOutlet fileprivate weak var trips: UICollectionView! {
        didSet {
            trips.delegate = self
            trips.dataSource = self
        }
    }
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        prepareCollectionView()
    }
    
    internal override func updateNavgationBar() {
        navigationItem.title = Constants.Localizer.Trips.localized()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ac_back"), style: .plain, target: self, action: #selector(backToMain(_:)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ac_manu"), style: .plain, target: revealViewController(), action: #selector(revealViewController().rightRevealToggle(_:)))
        view.addGestureRecognizer(revealViewController().panGestureRecognizer())
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
        loadData()
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUi), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Actions
    
    @objc fileprivate func showCallDetialsOnClick(_ sender: UIButton) {
        
        if let callDetailsScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.CallDetails) as? CallDetails {
            callDetailsScreen.trip = dataSource[sender.tag]
            navigate(to: callDetailsScreen)
        }
     }
    
    @objc private func backToMain(_ sender: UIBarButtonItem)-> void {
        if let mainNav = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.MainNav) as? UINavigationController {
            revealViewController().pushFrontViewController(mainNav, animated: true)
        }
    }
    
    
}


// MARK: - Helper functions

extension Trips
{
    
    @objc fileprivate func updateUi()-> void {
        lblSubtitle.text = Constants.Localizer.TripsDay.localized()
        navigationItem.title = Constants.Localizer.Trips.localized()
        trips.reloadData()
        
    }
    
    fileprivate func prepareCollectionView()-> void {
        trips.register(Constants.Nib.Trip, forCellWithReuseIdentifier: Constants.ReuseIdentifier.Trip)
    }
    
    fileprivate func loadData()-> void {
        guard let id = UserDefaults.standard.value(forKey: Constants.Misc.UserId) as? string else { return }
        Trip.getData(by: id, page: self) { [weak self] (data) in
            self?.dataSource = data
            self?.stopSpin()
            UIApplication.showIndicator(by: false)
        }
    }
    
}



extension Trips: UICollectionViewDelegate,
    UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout
{
    internal func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ReuseIdentifier.Trip, for: indexPath) as? TripCell {
            cell.trip = dataSource[indexPath.item]
            cell.btnCallDetails.tag = indexPath.item
            cell.btnCallDetails.addTarget(self, action: #selector(showCallDetialsOnClick(_:)), for: .touchUpInside)
            return cell
        }
        return UICollectionViewCell()
    }
    
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("selected item is: \(indexPath)")
        if let callDetailsScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.CallDetails) as? CallDetails {
            callDetailsScreen.trip = dataSource[indexPath.item]
            navigate(to: callDetailsScreen)
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return Size(width: 310, height: 300)
    }
    
}
















