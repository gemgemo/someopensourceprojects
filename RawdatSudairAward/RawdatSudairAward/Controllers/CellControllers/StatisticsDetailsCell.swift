
import UIKit
import Localize_Swift

final class StatisticsDetailsCell: UITableViewCell, CellProvider
{
    
    
    // MARK: - Constants
    
    //MARK: - Variables
    internal var statistics: PersonStatistics? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var container: UIView!
    @IBOutlet private weak var name: UILabel!
    @IBOutlet private weak var school: UILabel!
    @IBOutlet private weak var sClass: UILabel!
    @IBOutlet private weak var percentage: UILabel!
    @IBOutlet private weak var rankking: UILabel!
    @IBOutlet private weak var rating: UILabel!
    @IBOutlet private weak var mainPanal: UIView!
    @IBOutlet private weak var mainStack: UIStackView!
    @IBOutlet private weak var fotterStack: UIStackView!
    
    
    
    // MARK: - Overridden functions
    
    internal func updateUI() {
        setupContainer()
         let seperator = ": "
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            name.textAlignment = .right
            school.textAlignment = .right
            sClass.textAlignment = .right
            percentage.textAlignment = .right
            rankking.textAlignment = .right
            rating.textAlignment = .right
            mainPanal.semanticContentAttribute = .forceLeftToRight
            mainStack.semanticContentAttribute = .forceRightToLeft
            fotterStack.semanticContentAttribute = .forceLeftToRight
        } else {
            name.textAlignment = .left
            school.textAlignment = .left
            sClass.textAlignment = .left
            percentage.textAlignment = .left
            rankking.textAlignment = .left
            rating.textAlignment = .left
            mainPanal.semanticContentAttribute = .forceRightToLeft
            mainStack.semanticContentAttribute = .forceRightToLeft
            fotterStack.semanticContentAttribute = .forceRightToLeft
        }
        name.text = "namelan".localized() + seperator + (statistics?.name ?? string.empty)
        school.text = "schoollan".localized() + seperator + (statistics?.school ?? string.empty)
        sClass.text = "classlan".localized() + seperator + (statistics?.className ?? string.empty)
        percentage.text = "percentagelan".localized() + seperator + (statistics?.percentage ?? string.empty)
        rankking.text = "Rankinglan".localized() + seperator + (statistics?.rank ?? string.empty)
        rating.text = "regardlan".localized() + seperator + (statistics?.result ?? string.empty)
    }
    
    //MARK: - Actions
    
    
    // MARK: - Functions

    private func setupContainer()-> void {
        container.clipsToBounds = true
        let cornerRadius: cgFloat = 5.0
        container.layer.cornerRadius = cornerRadius
        container.layer.masksToBounds = true
        container.setShadow(with: Size(right: 0, top: 1), radius: 2, opacity: 0.8, color: .lightGray)
        container.layer.masksToBounds = false
        
    }

}


















