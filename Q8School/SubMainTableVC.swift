
import UIKit

class SubMainTableVC: UIViewController ,UITableViewDataSource , UITableViewDelegate{

    @IBOutlet var tableView: UITableView!
    var subName: String!
    var subID: String!
    var dataFound: Bool!
    var subjects: Array<Subject> = Array<Subject>()
    var mainID: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        mainID = UserDefaults.standard.string(forKey: "mainPhase")
        loadMainPhase(Settings.lang, mainID ?? "")
    }
    
    func loadMainPhase(_ lang: String,_ mainID: String) {
        let request = NSMutableURLRequest(url: URL(string:Settings.Subjects)!)
        request.httpMethod = "POST"
        let postString = "lang=\(lang)&scholarPhase=\(mainID)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(error?.localizedDescription)")
                return
            }
            
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
            
            if let responseData = data {
                
                do {
                    
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                    print("ddd\(json)")
                    self.dataFound = json["Success"] as? Bool
                    if self.dataFound!
                    {
                        
                        let data = json["Result"] as! [NSDictionary]
                        for i in data
                        {
                            var name1 = ""
                            if Settings.lang == "ar"
                            {
                                name1 = "name_ar"
                            }
                            else
                            {
                                name1 = "name"
                                
                            }
                            
                            let id = i["id"] as! String
                            let name = i[name1] as! String
                            
                            let subject = Subject(id: id, name: name)
                            self.subjects.append(subject)
                            
                        }
                    }
                    DispatchQueue.main.async
                        {
                            self.tableView.reloadData()
                            
                    }
                    
                }
                catch
                {
                    print ("Could not Serialize")
                }
                
            }
            
        }
        task.resume()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {        // #warning Incomplete implementation, return the number of sections
        return self.subjects.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "subCell", for: indexPath) as! SubMainTable
        if Settings.lang == "ar"
        {
            cell.subLbl.text = self.subjects[indexPath.row].name
            cell.subID = self.subjects[indexPath.row].id
            
        }
        else
        {
            cell.subLbl.text = self.subjects[indexPath.row].name
            cell.subID = self.subjects[indexPath.row].id
            
        }
        return cell
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
        
    {
        if segue.identifier == "unwindSub"
        {
            if let cell = sender as? UITableViewCell
            {
                if Settings.lang == "ar"
                {
                    let indexPath = tableView.indexPath(for: cell)
                    subName = self.subjects[(indexPath?.row)!].name
                    print("user name\(subName)")
                    subID = self.subjects[(indexPath?.row)!].id
                    print("user name\(subID)")

                    
                }
                
            }
            
        }
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
