
import UIKit
import CoreLocation
import Localize_Swift
import GoogleMaps

final class Mission: BaseController
{

    // MARK: - Constants
    
    fileprivate let bottomHeightPercentage: cgFloat = 0.65, bottomConstant: cgFloat = 30.0
    
    // MARK: - Enumerations
    
    fileprivate enum PayType: int {
        case wallet = 0
        case cash   = 1
        case visa   = 2
    }
    
    fileprivate enum PricingType: int {
        case app = 0
        case driver = 1
    }
    
    fileprivate enum DriverCarType: string {
        case all = "0", driver = "1", company = "2"
    }
    
    // MARK: - Variables
    internal var typeTime: int?, laterDate: string?
    fileprivate var centerX: NSLayoutConstraint!,
                    bottomMenuMargin: NSLayoutConstraint!,
                    locationManager   = CLLocationManager(),
                    location          = CLLocation(),
                    paymentDataSource = Array<PaymenyWay>(),
                    pricingDataSource = Array<PricingWay>(),
                    transportTypeDataSource = Array<SubCar>(),
                    source: CLLocationCoordinate2D?,
                    destination: CLLocationCoordinate2D?,
                    //userLocationAnnotation: MKAnnotation?,
                    sourceMarker, destinationMarker: GMSMarker?,
                    polyline: GMSPolyline?,
                    mainCars          = Array<MainCar>(),
                    subCars           = Array<SubCar>(),
                    driverTypeData    = Array<DriverType>(),
                    selectedMainCar: MainCar?,
                    selectedSubCar: SubCar?,
                    routeDistance     = Number.zero.doubleValue,
                    payType           = PayType.wallet,
                    priceType         = PricingType.app,
                    driverPaymentType = PayType.wallet,
                    selectedDriverType: DriverType?,
                    selectedMainCarForDriver: DriverMainCar?,
                    selectedSubCarForDriver: SubCar?,
                    driverMainCars = Array<DriverMainCar>(),
                    parameters = Dictionary<string, any>(),
                    driverCarType = DriverCarType.all
    
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var topTabBar: UIView! {
        didSet {
            topTabBar.setShadow(with: Size(right: 0, top: 0.5), radius: 0.5, opacity: 1.0, color: Color(white: 0.88, alpha: 1.0))
        }
    }
    @IBOutlet private weak var btnDelegate: Button!
    @IBOutlet private weak var btnDriver: Button!
    @IBOutlet private weak var btnMachines: Button!
    @IBOutlet fileprivate weak var pointer: UIView! {
        didSet {
            pointer.transform = CGAffineTransform(rotationAngle: 130/180)
        }
    }
    @IBOutlet fileprivate var delegatePanal: UIView!
    @IBOutlet fileprivate var driverPanal: UIView!
    @IBOutlet fileprivate var machinesPanal: UIView!
    @IBOutlet fileprivate weak var mapPanal: GMSMapView!
    @IBOutlet fileprivate var bottomPanal: UIView!
    @IBOutlet fileprivate weak var btnBottomMenu: UIButton!
    @IBOutlet fileprivate weak var btnAll: Button! {
        didSet {
            let margin = btnAll.frame.width-5
            btnAll.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: margin)
        }
    }
    @IBOutlet fileprivate weak var btnMissionType: Button! {
        didSet {
            let margin = btnMissionType.frame.width-5
            btnMissionType.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: margin)
        }
    }
    @IBOutlet fileprivate weak var txtMissionDetails: TextView!
    @IBOutlet private weak var btnOrderSender: Button!
    @IBOutlet private weak var lblPaymentWay: UILabel!
    @IBOutlet fileprivate weak var tblPaymentWays: UITableView! {
        didSet {
            tblPaymentWays.delegate = self
            tblPaymentWays.dataSource = self
        }
    }
    @IBOutlet fileprivate weak var fromView: UIView! {
        didSet {
            fromView.setShadow(with: .zero, radius: 2, opacity: 0.6, color: .black)
        }
    }
    @IBOutlet fileprivate weak var toView: UIView! {
        didSet {
            toView.setShadow(with: .zero, radius: 2, opacity: 0.6, color: .black)
        }
    }
    @IBOutlet fileprivate weak var txfFrom: AutoCompleteTextField! {
        didSet {
            txfFrom.delegate = self
        }
    }
    @IBOutlet fileprivate weak var txfTo: AutoCompleteTextField! {
        didSet {
            txfTo.delegate = self
        }
    }
    @IBOutlet private weak var lblDelegate: UILabel!
    @IBOutlet private weak var lblDriver: UILabel!
    @IBOutlet private weak var lblEquipments: UILabel!
    @IBOutlet private weak var lblFrom: UILabel!
    @IBOutlet private weak var lblTo: UILabel!
    // MARK: - Driver Outlets
    @IBOutlet fileprivate weak var btnDriverAll: Button! {
        didSet {
            let margin = btnDriverAll.frame.width-2
            btnDriverAll.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: margin)
        }
    }
    @IBOutlet fileprivate weak var btnDriverCarType: Button! {
        didSet {
            let margin = btnDriverCarType.frame.width-2
            btnDriverCarType.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: margin)
        }
    }
    @IBOutlet private weak var driverTransportTypePanal: UIView! {
        didSet {
            driverTransportTypePanal.setBorder(with: 1, and: Color(white: 0.88, alpha: 0.9))
        }
    }
    @IBOutlet private weak var lblDriverTransportType: UILabel!
    @IBOutlet fileprivate weak var tblDriverTransport: UITableView! {
        didSet {
            tblDriverTransport.delegate = self
            tblDriverTransport.dataSource = self
            tblDriverTransport.alwaysBounceVertical = false
        }
    }
    @IBOutlet private weak var driverWeightPanal: UIView! {
        didSet {
            driverWeightPanal.setBorder(with: 1, and: Color(white: 0.88, alpha: 0.9))
        }
    }
    @IBOutlet fileprivate weak var txfDriverWeight: UITextField! {
        didSet {
            txfDriverWeight.delegate = self
        }
    }
    @IBOutlet private weak var driverNumberOfPanal: UIView! {
        didSet {
            driverNumberOfPanal.setBorder(with: 1, and: Color(white: 0.88, alpha: 0.9))
        }
    }
    @IBOutlet fileprivate weak var txfDriverNumberOf: UITextField! {
        didSet {
            txfDriverNumberOf.delegate = self
        }
    }
    @IBOutlet private weak var lblPricingWay: UILabel!
    @IBOutlet fileprivate weak var tblDriverPricing: UITableView! {
        didSet {
            tblDriverPricing.delegate = self
            tblDriverPricing.dataSource = self
            tblDriverPricing.alwaysBounceVertical = false
        }
    }
    @IBOutlet private weak var lblDriverPaymentWay: UILabel!
    @IBOutlet fileprivate weak var tblDriverPayment: UITableView! {
        didSet {
            tblDriverPayment.delegate = self
            tblDriverPayment.dataSource = self
            tblDriverPayment.alwaysBounceVertical = false
        }
    }
    @IBOutlet private weak var btnFollowUp: Button!
    @IBOutlet fileprivate weak var transportTableHeight: NSLayoutConstraint! {
        didSet {
            transportTableHeight.constant = 8.0
        }
    }
    @IBOutlet private weak var driverScrollView: UIScrollView! {
        didSet {
            driverScrollView.delegate = self
        }
    }
    @IBOutlet fileprivate weak var wightHeight: NSLayoutConstraint! {
        didSet {
            wightHeight.constant = 0.0
        }
    }
    @IBOutlet fileprivate weak var numberOfHeight: NSLayoutConstraint! {
        didSet {
            numberOfHeight.constant = 0.0
        }
    }
    
    
   
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        centerX = pointer.centerXAnchor.constraint(equalTo: btnDelegate.centerXAnchor)
        centerX.isActive = true
        setup(this: delegatePanal)
        setup(this: driverPanal)
        setup(this: machinesPanal)
        configureLocation()
        configureAutoPlaceCompleteFields()
        autoPlaceCompleteHandlers()
        driverTypeData = DriverType.getData()
        if let driverDataType = driverTypeData.first {
            let dTitle = ((Localize.currentLanguage() == Constants.Language.Arabic) ? driverDataType.arabicName : driverDataType.englishName) ?? .empty
            btnDriverAll.setTitle(dTitle, for: .normal)
            updateButtonsView()
        }
        
        resendOrder = { [weak self] in
            self?.sendOrder()
        }
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
        show(this: delegatePanal)
        setupBottomPanal()
        configureTables()
        /*retrieveMainCars { [weak self] in
            self?.selectedMainCar = self?.mainCars.first            
            self?.retrieveSubCars(by: self?.mainCars.first?.id ?? .empty) { [weak self] in
                self?.selectedSubCar = self?.subCars.first
                if let firstObj = self?.subCars.first {
                    if let title = (Localize.currentLanguage() == Constants.Language.Arabic) ? firstObj.arabicName : firstObj.englishName {
                        self?.btnMissionType.setTitle(title, for: .normal)
                        self?.updateButtonsView()
                    }
                }
            }
        }*/
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)        
        let pointerCenterX = floor(double(pointer.center.x))
        let buttonCenterX = floor(double(btnDelegate.center.x))
        bottomPanal.isHidden = ( pointerCenterX != buttonCenterX)
        txfTo.clearButtonMode = .never
        txfFrom.clearButtonMode = .never
        NotificationCenter.default.addObserver(self, selector: #selector(updateUi), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    internal override func updateNavgationBar() {
        super.updateNavgationBar()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ac_back"), style: .plain, target: self, action: #selector(goBack(_:)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ac_manu"), style: .plain, target: revealViewController(), action: #selector(revealViewController().rightRevealToggle(_:)))
        view.addGestureRecognizer(revealViewController().panGestureRecognizer())
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: - Actions
    
    @IBAction private func delgateOnCLick(_ sender: Button) {
        bottomPanal.isHidden = false
        if (btnBottomMenu.imageView?.image == #imageLiteral(resourceName: "ordar_arrow_down1")) {
            closeBottomMenu()
        }
        movePointer(to: sender.frame.origin.x+sender.frame.width/2)
        hidePanals { [weak self] in
            guard let this = self else { return }
            this.show(this: this.delegatePanal)
        }
    }
    
    @IBAction private func driverOnClick(_ sender: Button) {
        bottomPanal.isHidden = true
        if (btnBottomMenu.imageView?.image == #imageLiteral(resourceName: "ordar_arrow_down1")) {
            closeBottomMenu()
        }
        movePointer(to: sender.frame.origin.x+sender.frame.width/2)
        hidePanals { [weak self] in
            guard let this = self else { return }
            this.show(this: this.driverPanal)
        }
    }
    
    @IBAction private func machinesOnCLick(_ sender: Button) {
        bottomPanal.isHidden = true
        if (btnBottomMenu.imageView?.image == #imageLiteral(resourceName: "ordar_arrow_down1")) {
            closeBottomMenu()
        }
        movePointer(to: sender.frame.origin.x+sender.frame.width/2)
        hidePanals { [weak self] in
            guard let this = self else { return }
            this.show(this: this.machinesPanal)
        }
    }
    
    @IBAction private func openBottomMenuOnClick(_ sender: UIButton) {
        (sender.imageView?.image == #imageLiteral(resourceName: "ordar_arrow_down1")) ? closeBottomMenu() : openBottomMenu()
    }
    
    @IBAction private func sendOrderOnClick(_ sender: Button) {
        let userId = (UserDefaults.standard.value(forKey: Constants.Misc.UserId) as? string) ?? .empty
        let sourceLatitude = source?.latitude ?? Number.zero.doubleValue
        let sourceLongitude = source?.longitude ?? Number.zero.doubleValue
        let destinationLatitude = destination?.latitude ?? Number.zero.doubleValue
        let destinationLongitude = destination?.longitude ?? Number.zero.doubleValue
        var date = string.empty
        let orderTime = typeTime ?? 3
        if (orderTime == 1) {
            date = laterDate ?? .empty
        } else {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .short
            date = dateFormatter.string(from: Date())
        }
        let subCarId = selectedSubCar?.id ?? .empty
        // validation
        guard !userId.isEmpty && !subCarId.isEmpty && !missionDetails.isEmpty && missionDetails != txtMissionDetails.placeholder else {
            showAlert(with: .empty, and: "reqiurefieldlan".localized())
            return
        }
        guard sourceLatitude != 0.0 && sourceLongitude != 0.0 && destinationLatitude != 0.0 && destinationLongitude != 0.0 else {
            showAlert(with: .empty, and: "addressreqiuredlan".localized())
            return
        }
        
        guard routeDistance != 0.0 else {
            showAlert(with: .empty, and: "distanceerrormessagelan".localized())
            return
        }
        parameters = [
            "UserID": userId,
            "slat": sourceLatitude,
            "slan": sourceLongitude,
            "elat": destinationLatitude,
            "elan": destinationLongitude,
            "date": date,
            "Kilo": routeDistance,
            "carType": subCarId,
            "dataMission": missionDetails,
            "missionType": "",
            "paytype": payType.rawValue,
            "typetime": orderTime
        ]
        //print(parameters)
        sendOrder()
    }
    
    @IBAction private func chooseAllOnClick(_ sender: Button) {
        openPopup(in: bottomPanal, with: mainCars, for: sender.bounds.width, in: sender.frame)
    }
    
    @IBAction private func chooseMissionOnClick(_ sender: Button) {
        openPopup(in: bottomPanal, with: subCars, for: sender.bounds.width, in: sender.frame)
    }
    
    @IBAction private func clearFromTextOnClick(_ sender: UIButton) {
        txfFrom.text = .empty
    }
    
    @IBAction private func clearToTextOnClick(_ sender: UIButton) {
        txfTo.text = .empty
    }
    
    @IBAction private func setSourceAnnotationOnClick(_ sender: UIButton) {
        guard !from.isEmpty else { return }
        /*if let annotation = userLocationAnnotation {
            mapView.removeAnnotation(annotation)
        }*/
        sourceMarker?.map = nil
        geocode(this: from) { [weak self] (coordinate) -> void in
            guard let this = self else { return }
            /*let sourcePin = SourcePin(coordinate: coordinate, title: .empty, subtitle: .empty)
            if let sourceCoordinate = self?.source {
                self?.mapView.removeAnnotation(SourcePin(coordinate: sourceCoordinate, title: .empty, subtitle: .empty))
            }
            self?.mapView.addAnnotation(sourcePin)
            self?.mapView.setRegion(in: coordinate)*/
            this.mapPanal.camera = this.region(for: coordinate)
            this.sourceMarker = this.annotation(for: coordinate, in: this.mapPanal, icon: #imageLiteral(resourceName: "map_mark1"))
            self?.source = coordinate
        }
    }
    
    @IBAction private func setDestinationAnnotationOnClick(_ sender: UIButton) {
        guard !to.isEmpty else { return }
        geocode(this: to) { [weak self] (coordinate) -> void in
            guard let this = self else { return }            
            /*let destinationPin = DestinationPin(coordinate: coordinate, title: .empty, subtitle: .empty)
            if let destinationCoordinate = self?.destination {
                this.mapView.removeAnnotation(DestinationPin(coordinate: destinationCoordinate, title: .empty, subtitle: .empty))
            }
            this.mapView.addAnnotation(destinationPin)
            this.mapView.setRegion(in: coordinate)*/
            this.mapPanal.camera = this.region(for: coordinate)
            this.destinationMarker = this.annotation(for: coordinate, in: this.mapPanal, icon: #imageLiteral(resourceName: "map_mark2"))
            this.destination = coordinate
            this.polyline?.map = nil
            if let sLocation = self?.source, let dLocation = self?.destination {
                this.drawRoute(from: sLocation, to: dLocation, for: this.mapPanal) { [weak self] (polyline, distance) -> void in
                    self?.polyline = polyline
                    self?.routeDistance = round(distance)
                    print("distance is \(this.routeDistance)")
                }
            }
        }
    }
    
    @objc private func updateUi()-> void {
        lblDelegate.text = "delegatelan".localized()
        lblDriver.text = "driverlan".localized()
        lblEquipments.text = "Rentalequipmentlan".localized()
        lblFrom.text = "labelfromlan".localized()
        lblTo.text = "labeltolan".localized()
        lblPaymentWay.text = "paymentwaylabellan".localized()
        txtMissionDetails.placeholder = "missiondetailsplaceholderlan".localized()
        btnOrderSender.setTitle("sendorderbuttonlan".localized(), for: .normal)
        
        lblDriverTransportType.text = "driverTypeoftransportationlan".localized()
        txfDriverWeight.placeholder = "driverweiaghtlan".localized()
        txfDriverNumberOf.placeholder = "drivernumberofplacholderlan".localized()
        btnFollowUp.setTitle("driverfollowupbuttonlan".localized(), for: .normal)
        lblDriverPaymentWay.text = "paymentwaylabellan".localized()
        lblPricingWay.text = "driverPricingwaylan".localized()
        paymentDataSource = PaymenyWay.getData()
        pricingDataSource = PricingWay.getPricingItems()
        tblDriverPayment.reloadData()
        tblDriverPricing.reloadData()
        tblPaymentWays.reloadData()
        tblDriverPricing.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
        tblPaymentWays.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
        tblDriverPayment.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
        if (!transportTypeDataSource.isEmpty) {
            tblDriverTransport.reloadData()
            tblDriverTransport.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
        }
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            fromView.semanticContentAttribute = .forceLeftToRight
            toView.semanticContentAttribute = .forceLeftToRight
            txtMissionDetails.textAlignment = .right
            lblPaymentWay.textAlignment = .right
            
            txfDriverWeight.textAlignment = .right
            txfDriverNumberOf.textAlignment = .right
            lblPricingWay.textAlignment = .right
            lblDriverPaymentWay.textAlignment = .right
            lblDriverTransportType.textAlignment = .right
        } else {
            fromView.semanticContentAttribute = .forceRightToLeft
            toView.semanticContentAttribute = .forceRightToLeft
            txtMissionDetails.textAlignment = .left
            lblPaymentWay.textAlignment = .left
            
            txfDriverWeight.textAlignment = .left
            txfDriverNumberOf.textAlignment = .left
            lblPricingWay.textAlignment = .left
            lblDriverPaymentWay.textAlignment = .left
            lblDriverTransportType.textAlignment = .left
        }
        updateButtonsView()
        retrieveMainCars { [weak self] in
            self?.selectedMainCar = self?.mainCars.first
            self?.retrieveSubCars(by: self?.mainCars.first?.id ?? .empty) { [weak self] in
                self?.selectedSubCar = self?.subCars.first
                if let firstObj = self?.subCars.first {
                    if let title = (Localize.currentLanguage() == Constants.Language.Arabic) ? firstObj.arabicName : firstObj.englishName {
                        self?.btnMissionType.setTitle(title, for: .normal)
                        self?.updateButtonsView()
                    }
                }
            }
        }
        driverTypeData = DriverType.getData()
        if let driverDataType = driverTypeData.first {
            let dTitle = ((Localize.currentLanguage() == Constants.Language.Arabic) ? driverDataType.arabicName : driverDataType.englishName) ?? .empty
            btnDriverAll.setTitle(dTitle, for: .normal)
        }
        
    }
    
    @IBAction private func driverAllClicked(_ sender: Button) {
        let rect = driverScrollView.convert(sender.frame, to: view)
        openPopup(in: view, with: driverTypeData, for: sender.bounds.width, in: rect)
    }
    
    @IBAction private func driverCarTypeClicked(_ sender: Button) {
        let rect = driverScrollView.convert(sender.frame, to: view)
        openPopup(in: view, with: driverMainCars, for: sender.bounds.width, in: rect)
    }
    
    @IBAction private func followUpClicked(_ sender: Button) {
        if let driverMapScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.DriverMap) as? DriverMap {         
            driverMapScreen.selectedMainCar = selectedMainCarForDriver
            driverMapScreen.selectedSubCar = selectedSubCarForDriver
            driverMapScreen.pricingWay = priceType.rawValue
            driverMapScreen.paymentWay = driverPaymentType.rawValue
            driverMapScreen.weight = weight
            driverMapScreen.numberOf = numberOf
            driverMapScreen.date = laterDate
            driverMapScreen.typeTime = typeTime
            driverMapScreen.dirverCarType = driverCarType.rawValue            
            (driverCarType != .all) ? navigate(to: driverMapScreen) : showAlert(with: .empty, and: "choosecartypemessagelan".localized())
        }
    }
    
    
}



// MARK: - Helper functions

extension Mission
{
    
    fileprivate func updateButtonsView()-> void {
        var const: cgFloat
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            const = 35.0
            btnAll.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: btnAll.frame.width-const)
            btnMissionType.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: btnMissionType.frame.width-const)
            btnDriverAll.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: btnDriverAll.frame.width-const)
            btnDriverCarType.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: btnDriverCarType.frame.width-const)
        } else {
            const = 35.0
            btnAll.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: btnAll.frame.width-const)
            btnMissionType.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: btnMissionType.frame.width-const)
            btnDriverAll.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: btnDriverAll.frame.width-const)
            btnDriverCarType.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: btnDriverCarType.frame.width-const)
        }
    }
    
    fileprivate func movePointer(to value: cgFloat)-> void {
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.7, options: .curveEaseIn, animations: { [weak self] in
            self?.pointer.center.x = value
        }, completion: nil)
    }
    
    fileprivate func sendOrder()-> void {
        Order.send(to: Constants.BackendURLs.SendDelegateOrder, with: parameters, in: self) { [weak self] (isSuccess) -> void in
            guard let this = self else { return }
            if (isSuccess) {
                self?.showAramWestDialog(in: this.view, for: .success, with: "yourordersendlan".localized()) { [weak self] in
                    self?.closeAramWestAlert(after: 3) { [weak self] in
                        _ = self?.navigationController?.popViewController(animated: true)
                    }
                }
            } else {
                this.showAramWestDialog(in: this.view, for: .fail, with: .empty) {}
            }
            self?.stopSpin()
        }
    }
    
    fileprivate func setup(this panal: UIView)-> void {
        panal.isHidden = true
        panal.alpha = 0.0
        panal.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(panal)
        panal.topAnchor.constraint(equalTo: topTabBar.bottomAnchor, constant: 8.0).isActive = true
        panal.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        panal.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        panal.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 8.0).isActive = true
    }
    
    fileprivate func show(this panal: UIView)-> void {
        panal.isHidden = false
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.7, options: .curveEaseIn, animations: { 
            panal.alpha = 1.0
        }) { (isFinished) in
            
        }
    }
    
    fileprivate func close(this panal: UIView)-> void {
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.7, options: .curveEaseOut, animations: {
            panal.alpha = 0.0
        }) { (isFinished) in
            panal.isHidden = true
        }
    }
    
    fileprivate func hidePanals(complationHandler: @escaping()-> void)-> void {
        [delegatePanal, machinesPanal, driverPanal].forEach { (panal) in
            panal?.alpha = 0.0
            panal?.isHidden = true
        }
        complationHandler()
    }
    
    fileprivate func setupBottomPanal()-> void {
        bottomPanal.translatesAutoresizingMaskIntoConstraints = false
        bottomPanal.isHidden = false
        bottomPanal.setShadow(with: Size(right: 0.0, top: -1.0), radius: 2.0, opacity: 0.4, color: .black)
        view.addSubview(bottomPanal)
        bottomMenuMargin = bottomPanal.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: view.bounds.height*bottomHeightPercentage-bottomConstant)
        bottomMenuMargin.isActive = true
        bottomPanal.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        bottomPanal.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        bottomPanal.heightAnchor.constraint(equalToConstant: view.bounds.height*bottomHeightPercentage).isActive = true
    }
    
    fileprivate func openBottomMenu()-> void {
        bottomMenuMargin.constant = 0
        UIView.animate(withDuration: 0.7, delay: 0.0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.7, options: .curveEaseOut, animations: { [weak self] in
            self?.view.layoutIfNeeded()
            self?.btnBottomMenu.setImage(#imageLiteral(resourceName: "ordar_arrow_down1"), for: .normal)
        }, completion: nil)
    }
    
    fileprivate func closeBottomMenu()-> void {
        bottomMenuMargin.constant = view.bounds.height*bottomHeightPercentage-bottomConstant
        UIView.animate(withDuration: 0.7, delay: 0.0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.7, options: .curveEaseIn, animations: { [weak self] in
            self?.view.layoutIfNeeded()
            self?.btnBottomMenu.setImage(#imageLiteral(resourceName: "ordar_arrow_top1"), for: .normal)
            }, completion: nil)
    }
    
    fileprivate func openPopup(in view: UIView, with items: [PopoverItem], for width: cgFloat, in rect: Rect)-> void {
        if let popupScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Popover) as? Popup {
            popupScreen.dataSource = items
            popupScreen.onSelected = { [weak self] (item, index) in
                guard let this = self else { return }
                if (item is MainCar) {
                    self?.selectedMainCar = item as? MainCar
                    let title = (Localize.currentLanguage() == Constants.Language.Arabic) ? item.arabicName : item.englishName
                    self?.btnAll.setTitle(title ?? .empty, for: .normal)
                    self?.updateButtonsView()
                    self?.retrieveSubCars(by: item.id ?? .empty) { [weak self] () -> void in
                        let firstObj = self?.subCars.first
                        let title = (Localize.currentLanguage() == Constants.Language.Arabic) ? firstObj?.arabicName : firstObj?.englishName
                        self?.btnMissionType.setTitle(title ?? .empty, for: .normal)
                        self?.updateButtonsView()
                    }
                } else if (item is SubCar) {
                    self?.selectedSubCar = item as? SubCar
                    let title = (Localize.currentLanguage() == Constants.Language.Arabic) ? item.arabicName : item.englishName
                    self?.btnMissionType.setTitle(title ?? .empty, for: .normal)
                    self?.updateButtonsView()
                } else if (item is DriverType) {
                    self?.selectedDriverType = item as? DriverType
                    self?.btnDriverAll.setTitle(item.arabicName, for: .normal)
                    //print("selected driver type is \(self?.selectedDriverType?.id)")
                    if let typeId = self?.selectedDriverType?.id {
                        this.driverCarType = DriverCarType(rawValue: typeId) ?? .all
                        print("driver car type \(this.driverCarType)")
                    }
                    self?.updateButtonsView()
                } else if (item is DriverMainCar) {
                    let title = (Localize.currentLanguage() == Constants.Language.Arabic) ? item.arabicName : item.englishName
                    self?.btnDriverCarType.setTitle(title, for: .normal)
                    self?.updateButtonsView()
                    self?.selectedMainCarForDriver = item as? DriverMainCar
                    if let id = item.id {
                        if (id == "1") {
                            self?.numberOfHeight.constant = 0.0
                            self?.wightHeight.constant = 0.0
                        } else {
                            //35
                            self?.numberOfHeight.constant = 35.0
                            self?.wightHeight.constant = 35.0
                        }
                        self?.retrieveDriverSubCars(by: id) { [weak self] in
                            self?.tblDriverTransport.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
                            self?.selectedSubCarForDriver = self?.transportTypeDataSource.first
                        }
                    }
                }
            }
            popupScreen.modalPresentationStyle = .popover
            popupScreen.popoverPresentationController?.delegate = self
            popupScreen.popoverPresentationController?.permittedArrowDirections = .up
            popupScreen.popoverPresentationController?.sourceView = view
            popupScreen.popoverPresentationController?.sourceRect = rect
            let height: cgFloat = cgFloat(popupScreen.dataSource.count*35)
            popupScreen.preferredContentSize = Size(width: width, height: height-5)
            present(popupScreen, animated: true, completion: nil)
        }
    }
    
    fileprivate func configureLocation()-> void {
        //mapView.setupCamera()
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let this = self else { return }
            this.locationManager.delegate = self
            this.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            this.locationManager.requestAlwaysAuthorization()
            if (CLLocationManager.locationServicesEnabled()) {
                this.locationManager.startUpdatingLocation()
            } else {
                DispatchQueue.main.async { [weak self] in
                    guard let this = self else { return }
                    let alert = UIAlertController(title: "warningtitlelan".localized(), message: "locationsubtitlelan".localized() , preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "okLan".localized(), style: .default, handler: { (action) in
                        if let settingsUrl = URL(string: UIApplicationOpenSettingsURLString), UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.openURL(settingsUrl)
                        }
                    }))
                    alert.addAction(UIAlertAction(title: "cancellan".localized(), style: .default, handler: nil))
                    this.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    fileprivate func configureTables()-> void {
        tblPaymentWays.register(Constants.Nib.Selection, forCellReuseIdentifier: Constants.ReuseIdentifier.Selection)
        paymentDataSource = PaymenyWay.getData()
        tblPaymentWays.reloadData()
        tblPaymentWays.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
        // TODO:- Pricing table view
        tblDriverPricing.register(Constants.Nib.Selection, forCellReuseIdentifier: Constants.ReuseIdentifier.Selection)
        pricingDataSource = PricingWay.getPricingItems()
        tblDriverPricing.reloadData()
        tblDriverPricing.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
        // TODO:- Driver payment
        tblDriverPayment.register(Constants.Nib.Selection, forCellReuseIdentifier: Constants.ReuseIdentifier.Selection)
        tblDriverPayment.reloadData()
        tblDriverPayment.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
        // TODO:- Transport type tableview
        tblDriverTransport.register(Constants.Nib.TransportType, forCellReuseIdentifier: Constants.ReuseIdentifier.TranspoertType)
    }
    
    fileprivate func configureAutoPlaceCompleteFields()-> void {
        [txfFrom, txfTo].forEach { (field) in
            field?.autoCompleteTextColor = .darkGray
            field?.autoCompleteTextFont = Font(name: "HelveticaNeue-Light", size: 12.0)!
            field?.autoCompleteCellHeight = 44.0
            if let tableView = field?.autoCompleteTableView {
                field?.superview?.superview?.superview?.bringSubview(toFront: tableView)
            }
            field?.maximumAutoCompleteCount = 10
            field?.hidesWhenSelected = true
            field?.hidesWhenEmpty = true
            field?.enableAttributedText = true
            var attributes = [string: object]()
            attributes[NSForegroundColorAttributeName] = UIColor.black
            attributes[NSFontAttributeName] = UIFont(name: "HelveticaNeue-Bold", size: 12.0)
            field?.autoCompleteAttributes = attributes
        }
    }
    
    fileprivate func autoPlaceCompleteHandlers()-> void {
        txfFrom.onTextChange = { [weak self] (text) in
            guard let this = self else { return }
            this.toView.isHidden = true
            if (!text.isEmpty) {
                this.getPlaces(by: text) { (places) -> void in
                    DispatchQueue.main.async { [weak self] in
                        self?.txfFrom.autoCompleteStrings = places
                    }
                    UIApplication.showIndicator(by: false)
                }
            } else {
                this.toView.isHidden = false
            }
        }
        
        txfTo.onTextChange = { [weak self] (text) in
            guard let this = self else { return }
            if (!text.isEmpty) {
                /*this.fetchPlaces(by: text, region: this.mapView.region) { [weak self] (items) in
                    self?.txfTo.autoCompleteStrings = items.map { $0.name ?? .empty }
                }*/
                this.getPlaces(by: text) { (places) -> void in
                    DispatchQueue.main.async { [weak self] in
                        self?.txfTo.autoCompleteStrings = places
                    }
                    UIApplication.showIndicator(by: false)
                }
            }
        }
        
        txfFrom.onSelect = { [weak self] (text, index) in
            self?.view.endEditing(true)
        }
        
        txfTo.onSelect = { [weak self] (text, index) in
            self?.view.endEditing(true)
        }
        
    }
    
    fileprivate func retrieveMainCars(onCompelete: @escaping()-> void)-> void {
        MainCar.getMainCars(from: Constants.BackendURLs.MainCars, page: self) { [weak self] (mCars) -> void in
            guard let this = self else { return }
            this.mainCars = mCars
            this.driverMainCars = this.mainCars.map { DriverMainCar(id: $0.id ?? .empty, aName: $0.arabicName ?? .empty, eName: $0.englishName ?? .empty, icon: $0.icon ?? .empty) }
            this.selectedMainCarForDriver = this.driverMainCars.first
            if let firstObject = mCars.first {
                let title = (Localize.currentLanguage() == Constants.Language.Arabic) ? firstObject.arabicName : firstObject.englishName
                this.btnAll.setTitle(title ?? .empty, for: .normal)
                this.btnDriverCarType.setTitle(title ?? .empty, for: .normal)
                this.updateButtonsView()
                this.retrieveDriverSubCars(by: firstObject.id ?? .empty) {
                    this.numberOfHeight.constant = 0.0
                    this.wightHeight.constant = 0.0
                    this.tblDriverTransport.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
                }
                onCompelete()
            }
            UIApplication.showIndicator(by: false)
        }
    }
    
    fileprivate func retrieveSubCars(by id: string, onComplete: @escaping()-> void)-> void {
        SubCar.getSubCars(from: Constants.BackendURLs.SubCars, by: id, page: self) { [weak self] (sCars) -> void in
            self?.subCars = sCars
            onComplete()
            UIApplication.showIndicator(by: false)
        }
    }
    
    fileprivate func retrieveDriverSubCars(by id: string, onComplete: @escaping()-> Void)-> void {
        transportTypeDataSource.removeAll()
        tblDriverTransport.reloadData()
        transportTableHeight.constant = 0.0
        SubCar.getSubCars(from: Constants.BackendURLs.SubCars, by: id, page: self) { [weak self] (sCars) -> void in
            guard let this = self  else { return }
            this.transportTypeDataSource = sCars
            this.selectedSubCarForDriver = sCars.first
            this.tblDriverTransport.reloadData()
            this.transportTableHeight.constant = this.tblDriverTransport.contentSize.height
            UIApplication.showIndicator(by: false)
            onComplete()
        }
    }
    
    
}


// MARK: - Map view delegate functions

/*extension Mission: MKMapViewDelegate
{
    internal func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        UIApplication.showIndicator(by: false)
    }
    
    internal func mapViewDidStopLocatingUser(_ mapView: MKMapView) {
        UIApplication.showIndicator(by: false)
    }
    
    internal func mapViewWillStartLoadingMap(_ mapView: MKMapView) {
        UIApplication.showIndicator(by: true)
    }
    
    internal func mapViewWillStartLocatingUser(_ mapView: MKMapView) {
        
    }
    
    internal func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if (annotation.isKind(of: MKUserLocation.self)) {
            return nil
        }
        if (annotation is SourcePin) {
            let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: Constants.Misc.SourceAnnotation)
            annotationView.image = #imageLiteral(resourceName: "map_mark1")
            return annotationView
        } else if (annotation is DestinationPin) {
            let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: Constants.Misc.DestinationAnnotation)
            annotationView.image = #imageLiteral(resourceName: "map_mark2")
            return annotationView
        } else {
            let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: Constants.Misc.SourceAnnotation)
            annotationView.image = #imageLiteral(resourceName: "map_mark1")
            return annotationView
        }
    }

    internal func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let render = MKPolylineRenderer(overlay: overlay)
        render.strokeColor = .darkGray
        render.lineWidth = 3.5
        return render
    }
    
}
*/

// MARK: - Location manager delegate functions

extension Mission: CLLocationManagerDelegate
{
    
    internal func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("location error is: \(error)")
    }
    
    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let userLocation = locations.last {
            mapPanal.clear()
            location = userLocation
            let coordinate = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
            source = coordinate
            /*mapView.annotations.forEach { mapView.removeAnnotation($0) }
            mapView.setRegion(in: coordinate)
            userLocationAnnotation = SourcePin(coordinate: coordinate, title: .empty, subtitle: .empty)
            if let annotation = userLocationAnnotation {
                mapView.addAnnotation(annotation)
            }*/
            //print("delegate coordinate", userLocation)
            mapPanal.camera = region(for: coordinate)
            sourceMarker = annotation(for: coordinate, in: mapPanal, icon: #imageLiteral(resourceName: "map_mark1"))
            address(from: userLocation) { [weak self] (address) -> void in
                self?.txfFrom.text = address
            }
        }
        manager.stopUpdatingLocation()
    }
    
}



// MARK: - Table view delegate & data source functions

extension Mission: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch(tableView) {
        case tblPaymentWays, tblDriverPayment:
            return paymentDataSource.count
            
        case tblDriverPricing:
            return pricingDataSource.count
            
        case tblDriverTransport:
            return transportTypeDataSource.count
            
        default:
            return 0
        }
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (tableView) {
        case tblPaymentWays, tblDriverPayment:
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.Selection) as? SelectionCell {
                cell.payment = paymentDataSource[indexPath.row]
                if (Localize.currentLanguage() == Constants.Language.Arabic) {
                    cell.mainPanal.semanticContentAttribute = .forceRightToLeft
                    cell.lblTitle.textAlignment = .right
                } else {
                    cell.mainPanal.semanticContentAttribute = .forceLeftToRight
                    cell.lblTitle.textAlignment = .left
                }
                return cell
            }
            
        case tblDriverPricing:
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.Selection) as? SelectionCell {
                cell.payment = pricingDataSource[indexPath.row]
                if (Localize.currentLanguage() == Constants.Language.Arabic) {
                    cell.mainPanal.semanticContentAttribute = .forceRightToLeft
                    cell.lblTitle.textAlignment = .right
                } else {
                    cell.mainPanal.semanticContentAttribute = .forceLeftToRight
                    cell.lblTitle.textAlignment = .left
                }
                return cell
            }
            
        case tblDriverTransport:
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.TranspoertType) as? TransportTypeCell {
                cell.subCar = transportTypeDataSource[indexPath.row]
                if (Localize.currentLanguage() == Constants.Language.Arabic) {
                    cell.container.semanticContentAttribute = .forceLeftToRight
                    cell.lblTitle.textAlignment = .right
                } else  {
                    cell.container.semanticContentAttribute = .forceRightToLeft
                    cell.lblTitle.textAlignment = .left
                }
                return cell
            }
            
        default:
            return UITableViewCell()
        }
        
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (tableView) {
        case tblPaymentWays:
            payType = PayType.init(rawValue: indexPath.row) ?? .wallet
            
        case tblDriverPricing:
            priceType = PricingType.init(rawValue: indexPath.row) ?? .app
            
        case tblDriverPayment:
            driverPaymentType = PayType.init(rawValue: indexPath.row) ?? .wallet
            
        case tblDriverTransport:
            //print("index \(indexPath.row)")
            selectedSubCarForDriver = transportTypeDataSource[indexPath.row]
            
        default:
            break
        }
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
}



// MARK: - Text Field delegate functions

extension Mission: UITextFieldDelegate
{
    
    internal func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    internal func textFieldDidEndEditing(_ textField: UITextField) {
        if (toView.isHidden) {
            toView.isHidden = false
        }
    }
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (toView.isHidden) {
            toView.isHidden = false
        }
        view.endEditing(true)
        return true
    }
    
}


// MARK: - Computed properties

extension Mission
{
    
    fileprivate var to: string {
        return txfTo.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var from: string {
        return txfFrom.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var missionDetails: string {
        return txtMissionDetails.text.trimmingCharacters(in: .whitespaces)
    }
    
    fileprivate var weight: string {
        return txfDriverWeight.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var numberOf: string {
        return txfDriverNumberOf.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
}


// MARK: - Scroll view delegate functions

extension Mission: UIScrollViewDelegate
{
    internal func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}















