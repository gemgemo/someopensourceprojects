
import UIKit
import Gemo

class MyBills: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var dataSource = Array<Bill>() { didSet { Threads.main { [weak self] in
        self?.billsCollection.reloadData()
        }}}
    fileprivate var usertype = Usertype.main
    
    // MARK: - Outlets
    
    @IBOutlet weak var billsCollection: UICollectionView! { didSet {
        billsCollection.delegate = self
        billsCollection.dataSource = self
        }}
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        usertype = Usertype(rawValue: UserDefaults.standard.integer(forKey: Misc.UserTypeKey)) ?? .sub
        prepareCollections()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadBills()
        
    }
    
    internal override func updateUi() {
        super.updateUi()
        
    }
    
    // MARK: - Actions
    
    

    
}


// MARK: - Helper functions 

fileprivate extension MyBills {
    
    fileprivate func prepareCollections()-> void {
        billsCollection.alwaysBounceVertical = true
        if (usertype == .main) {
            billsCollection.register(Nibs.MyBills, forCellWithReuseIdentifier: ReuseIdentifiers.MyBills)
        } else {
            billsCollection.register(Nibs.UserCarBill, forCellWithReuseIdentifier: ReuseIdentifiers.UserCarBill)
        }
    }
    
    func loadBills()-> void {        
        Bill.fetchBills(from: (usertype == .main) ? "all_report_1" : "all_report_sub", self) { [weak self] (bills) in
            self?.dataSource = bills
        }
    }
    
}


// MARK: - collection view delegate & data source functions

extension MyBills: UICollectionViewDelegate,
    UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout
{
    internal func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (usertype == .main) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ReuseIdentifiers.MyBills, for: indexPath) as! MyBillsCell
            cell.bill = dataSource[indexPath.item]
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ReuseIdentifiers.UserCarBill, for: indexPath) as! UserCArBillCell
            cell.bill = dataSource[indexPath.item]
            return cell
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("selected item is: \(indexPath)")
        if (usertype == .main) {
            guard let billDetailsPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.BillsDetails) as? BillsDetails else {
                return
            }
            billDetailsPage.bill = dataSource[indexPath.item]
            navigate(to: billDetailsPage)
        } else {
            guard let billDetailsPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.CarUserBillDetails) as? CarUserBillDetails else {
                return
            }
            billDetailsPage.bill = dataSource[indexPath.item]
            navigate(to: billDetailsPage)
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //collectionView.frame.width-(collectionView.frame.width * 0.10)
        return Size(width: collectionView.frame.width-8, height: 215.0)
    }
    
}

























