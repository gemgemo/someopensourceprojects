//
//  Category.swift
//  KWTSads
//
//  Created by Mostafa on 2/14/17.
//  Copyright © 2017 Mostafa. All rights reserved.
//

import Foundation
public class Category{
    
    var cat_id: String!
    var cat_name: String!
    var color: String!
    var cat_img: String!
    var section_img: String!
  
    init(cat_id: String,cat_name: String,color: String,cat_img: String,section_img: String) {
        self.cat_id = cat_id
        self.cat_name = cat_name
        self.color = color
        self.cat_img = cat_img
        self.section_img = section_img
    }
    
   
}
