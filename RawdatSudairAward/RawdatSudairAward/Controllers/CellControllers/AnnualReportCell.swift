
import UIKit
import Localize_Swift

final class AnnualReportCell: UITableViewCell, CellProvider
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    internal var report: AnnualReport? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var stackView: UIStackView!
    
    // MARK: - Overridden functions
    
    internal func updateUI() {
        print(report?.link ?? "empty link")
        lblTitle.text = report?.title ?? string.empty
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            lblTitle.textAlignment = .right
            stackView.semanticContentAttribute = .forceLeftToRight
        } else {
            lblTitle.textAlignment = .left
            stackView.semanticContentAttribute = .forceRightToLeft
        }
    }
    
    //MARK: - Actions
    
    
}






























