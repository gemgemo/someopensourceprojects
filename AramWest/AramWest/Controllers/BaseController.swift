
import UIKit
import Localize_Swift
import MobileCoreServices
import Alamofire
import CoreLocation
import MapKit
import GoogleMaps

class BaseController: UIViewController
{
    
    // MARK: - Constants
    
    //fileprivate let geocoder = CLGeocoder()
    
    // MARK: - Variables
    
    fileprivate lazy var indicator: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.hidesWhenStopped = true
        spinner.translatesAutoresizingMaskIntoConstraints = false
        return spinner
    }()
    
    fileprivate lazy var spinnerView: UIVisualEffectView = {
        let visualView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: UIBlurEffect(style: .light)))
        visualView.translatesAutoresizingMaskIntoConstraints = false
        return visualView
    }()
    
    fileprivate lazy var alertContainer: UIView = {
        let iv = UIView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.backgroundColor = .darkGray
        iv.alpha = 0
        return iv
    }()
    
    fileprivate lazy var alertView: UIView = {
        let alert = UIView()
        alert.translatesAutoresizingMaskIntoConstraints = false
        alert.backgroundColor = .white
        alert.layer.cornerRadius = 3.0
        alert.clipsToBounds = true
        return alert
    }()
    
    fileprivate lazy var lblTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.textAlignment = .center
        return label
    }()
    
    fileprivate lazy var lblMessage: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.textAlignment = .center
        return label
    }()
    
    fileprivate lazy var btnDone: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Alertdonebuttontitlelan".localized(), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.tintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        return btn
    }()

    
    // MARK: - Outlets
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        updateNavgationBar()
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(dialogUserToOpenNotification), name: Constants.Misc.ForgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToEndedMission), name: Constants.Misc.MissionEnded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToRateView), name: Constants.Misc.RateView, object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(navigateToHome), name: Notification.Name("navigatetohome"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    @objc private func navigateToHome()-> void {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK: - Aram west alert dialog
    
    private lazy var alertMainView: UIView = {
        let av = UIView()
        av.translatesAutoresizingMaskIntoConstraints = false
        av.backgroundColor = Color.black.withAlphaComponent(0.6)
        return av
    }()
    
    private lazy var alertPanal: UIView = {
       let av = UIView()
        av.translatesAutoresizingMaskIntoConstraints = false
        av.backgroundColor = .white
        av.layer.cornerRadius = 10
        av.clipsToBounds = true
        return av
    }()
    
    private lazy var alertImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var alertLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        label.font = Font.systemFont(ofSize: 15)
        label.textColor = .black
        label.textAlignment = .center
        return label
    }()
    
    private lazy var alertButton: Button = {
       let btn = Button(type: .system)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("resendbuttontitlelan".localized(), for: .normal)
        btn.titleLabel?.font = Font.systemFont(ofSize: 13.0)
        btn.isHidden = true
        btn.tintColor = .white
        btn.cornerRadius = 8.0
        btn.backgroundColor = #colorLiteral(red: 0.9734930396, green: 0.5028870702, blue: 0.02652942948, alpha: 1)
        btn.addTarget(self, action: #selector(onAlertButtonClick(_:)), for: .touchUpInside)
        return btn
    }()
    
    @objc fileprivate func onAlertButtonClick(_ sender: Button) {
        closeAramWestAlert(after: 0) { [weak self] in
            self?.resendOrder()
        }
    }
    
    internal var resendOrder: ()-> void = { }
    
    private func setupAlertViews(in panal: UIView)-> void {
        panal.addSubview(alertMainView)
        alertMainView.addSubview(alertPanal)
        alertPanal.addSubview(alertImage)
        alertPanal.addSubview(alertLabel)
        alertPanal.addSubview(alertButton)
        // constraints
        // main
        alertMainView.topAnchor.constraint(equalTo: panal.topAnchor).isActive = true
        alertMainView.rightAnchor.constraint(equalTo: panal.rightAnchor).isActive = true
        alertMainView.bottomAnchor.constraint(equalTo: panal.bottomAnchor).isActive = true
        alertMainView.leftAnchor.constraint(equalTo: panal.leftAnchor).isActive = true
        // alert
        alertPanal.widthAnchor.constraint(equalToConstant: panal.bounds.width*0.85).isActive = true
        alertPanal.heightAnchor.constraint(equalToConstant: panal.bounds.height*0.35).isActive = true
        alertPanal.centerXAnchor.constraint(equalTo: alertMainView.centerXAnchor).isActive = true
        alertPanal.centerYAnchor.constraint(equalTo: alertMainView.centerYAnchor).isActive = true
        // image view
        alertImage.widthAnchor.constraint(equalToConstant: panal.bounds.width*0.20).isActive = true
        alertImage.heightAnchor.constraint(equalToConstant: panal.bounds.height*0.20).isActive = true
        alertImage.centerXAnchor.constraint(equalTo: alertPanal.centerXAnchor, constant: 0.0).isActive = true
        alertImage.topAnchor.constraint(equalTo: alertPanal.topAnchor, constant: 8.0).isActive = true
        // alert label
        alertLabel.topAnchor.constraint(equalTo: alertImage.bottomAnchor, constant: 0.0).isActive = true
        alertLabel.rightAnchor.constraint(equalTo: alertPanal.rightAnchor, constant: 0.0).isActive = true
        alertLabel.bottomAnchor.constraint(equalTo: alertPanal.bottomAnchor, constant: 8.0).isActive = true
        alertLabel.leftAnchor.constraint(equalTo: alertPanal.leftAnchor, constant: 8.0).isActive = true
        // alert button  
        alertButton.bottomAnchor.constraint(equalTo: alertPanal.bottomAnchor, constant: 3).isActive = true
        alertButton.centerXAnchor.constraint(equalTo: alertPanal.centerXAnchor, constant: 0).isActive = true
        alertButton.widthAnchor.constraint(equalToConstant: 130).isActive = true
        alertButton.heightAnchor.constraint(equalToConstant: 35).isActive = true
    }
    
    internal enum DialogState {
        case fail, success
    }
    
    internal func showAramWestDialog(in panal: UIView, for state: DialogState, with message: string, onComplete: @escaping()-> void)-> void {
        DispatchQueue.main.async { [weak self] in
            self?.setupAlertViews(in: panal)
            switch (state) {
            case .success:
                self?.alertImage.image = #imageLiteral(resourceName: "ordar_confirm")
                self?.alertButton.isHidden = true
                self?.alertLabel.isHidden = false
            case .fail:
                self?.alertImage.image = #imageLiteral(resourceName: "ordar_re")
                self?.alertButton.isHidden = false
                self?.alertLabel.isHidden = true
            }
            self?.alertLabel.text = message
            onComplete()
        }
    }
    
    internal func closeAramWestAlert(after: double, onComplete: @escaping()-> void)-> void {
        DispatchQueue.main.asyncAfter(deadline: .now()+after) { [weak self] in
            self?.alertMainView.removeFromSuperview()
            onComplete()
        }
    }
    
    // MARK: - Actions
    
    internal func goBack(_ sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc fileprivate func doneAlertViewOnCLick(_ sender: UIButton) {
        print("done button tapped")
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options: .curveEaseIn, animations: { [weak self] in
            guard let this = self else { return }
            this.alertView.transform = CGAffineTransform().scaledBy(x: 0.0, y: 0.0)
            this.alertContainer.alpha = 0.0
        }) { [weak self] (isCompleted) in
            guard let this = self else { return }
            this.lblMessage.removeFromSuperview()
            this.lblTitle.removeFromSuperview()
            this.btnDone.removeFromSuperview()
            this.alertView.removeFromSuperview()
            this.alertContainer.removeFromSuperview()
        }
    }
    
    // MARK: - Functions   
    
    internal func navigateToEndedMission()-> void {
        openEndedMission()
    }
    
    internal func navigateToRateView()-> void {
        openRatePopup()
    }

    
    internal func updateNavgationBar()-> void {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "ac_logo"))
        navigationItem.titleView = imageView
        navigationController?.navigationBar.semanticContentAttribute = .forceLeftToRight
    }

    
    internal func share(this items: [any])-> void {
        let activity = UIActivityViewController(activityItems: items, applicationActivities: nil)
        activity.excludedActivityTypes = [.copyToPasteboard, .mail, .message, .postToFacebook, .postToFlickr, .postToTencentWeibo, .openInIBooks, .addToReadingList, .airDrop, .postToVimeo, .postToWeibo, .print, .saveToCameraRoll]
        activity.popoverPresentationController?.sourceRect = Rect(x: 0, y: 0, width: 0, height: 0)
        activity.popoverPresentationController?.sourceView = view
        present(activity, animated: true, completion: nil)
    }
    
    internal func changeLanguage()-> void {
        let alert = UIAlertController(title: "تغيير اللغه", message: "Change Language", preferredStyle: .alert)
        //alert.popoverPresentationController?.sourceView = view
        //alert.popoverPresentationController?.sourceRect = Rect(x: 0, y: 0, width: 0, height: 0)
        alert.addAction(UIAlertAction(title: "عربي", style: .default, handler: { (action) in
            Localize.setCurrentLanguage(Constants.Language.Arabic)
        }))
        
        alert.addAction(UIAlertAction(title: "English", style: .default, handler: { (action) in
            Localize.setCurrentLanguage(Constants.Language.English)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(alert, animated: true, completion: nil)

    }
    
    internal func startSpin()-> void {
        UIApplication.showIndicator(by: true)
        DispatchQueue.main.async { [weak self] in
            guard let this = self else { return }
            this.view.addSubview(this.spinnerView)
            this.spinnerView.addSubview(this.indicator)
            
            this.spinnerView.topAnchor.constraint(equalTo: this.view.topAnchor, constant: 0).isActive = true
            this.spinnerView.rightAnchor.constraint(equalTo: this.view.rightAnchor, constant: 0).isActive = true
            this.spinnerView.bottomAnchor.constraint(equalTo: this.view.bottomAnchor, constant: 0).isActive = true
            this.spinnerView.leftAnchor.constraint(equalTo: this.view.leftAnchor, constant: 0).isActive = true
            
            this.indicator.centerXAnchor.constraint(equalTo: this.spinnerView.centerXAnchor).isActive = true
            this.indicator.centerYAnchor.constraint(equalTo: this.spinnerView.centerYAnchor).isActive = true
            
            this.indicator.startAnimating()
        }
    }
    
    internal func stopSpin()-> void {
        DispatchQueue.main.async { [weak self] in
            guard let this = self else { return }
            this.indicator.stopAnimating()
            this.indicator.removeFromSuperview()
            this.spinnerView.removeFromSuperview()
        }
        UIApplication.showIndicator(by: false)
    }

    internal func showAlert(with title: string, and message: string)-> void {
        DispatchQueue.main.async { [weak self] in
            guard let this = self else { return }
            
            this.view.addSubview(this.alertContainer)
            this.alertContainer.addSubview(this.alertView)
            
            this.alertContainer.topAnchor.constraint(equalTo: this.view.topAnchor, constant: 0).isActive = true
            this.alertContainer.rightAnchor.constraint(equalTo: this.view.rightAnchor, constant: 0).isActive = true
            this.alertContainer.bottomAnchor.constraint(equalTo: this.view.bottomAnchor, constant: 0).isActive = true
            this.alertContainer.leftAnchor.constraint(equalTo: this.view.leftAnchor, constant: 0).isActive = true
            
            this.alertView.widthAnchor.constraint(equalToConstant: this.view.bounds.width*0.85).isActive = true
            this.alertView.heightAnchor.constraint(equalToConstant: this.view.bounds.height*0.23).isActive = true
            this.alertView.centerXAnchor.constraint(equalTo: this.alertContainer.centerXAnchor).isActive = true
            this.alertView.centerYAnchor.constraint(equalTo: this.alertContainer.centerYAnchor).isActive = true
            
            this.lblTitle.text = "alerttitlelan".localized()
            
            this.lblMessage.text = message
            this.lblMessage.font = Font.systemFont(ofSize: 14.0)
            
            this.alertView.addSubview(this.lblTitle)
            this.alertView.addSubview(this.lblMessage)
            this.alertView.addSubview(this.btnDone)
            
            this.lblTitle.topAnchor.constraint(equalTo: this.alertView.topAnchor, constant: 8).isActive = true
            this.lblTitle.centerXAnchor.constraint(equalTo: this.alertView.centerXAnchor).isActive = true
            
            this.btnDone.bottomAnchor.constraint(equalTo: this.alertView.bottomAnchor, constant: -8).isActive = true
            this.btnDone.centerXAnchor.constraint(equalTo: this.alertView.centerXAnchor, constant: 0).isActive = true
            
            this.lblMessage.centerXAnchor.constraint(equalTo: this.alertView.centerXAnchor).isActive = true
            this.lblMessage.centerYAnchor.constraint(equalTo: this.alertView.centerYAnchor).isActive = true
            
            this.btnDone.addTarget(self, action: #selector(this.doneAlertViewOnCLick(_:)), for: .touchUpInside)
            this.view.layoutIfNeeded()
            this.alertView.transform = CGAffineTransform().scaledBy(x: 0, y: 0)
            // show alert with animation
            UIView.animate(withDuration: 0.6, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.7, options: .curveEaseOut, animations: { [weak self] in
                guard let this = self else { return }
                this.alertContainer.alpha = 1.0
                this.alertContainer.backgroundColor = Color.lightGray.withAlphaComponent(0.7)
                this.alertView.transform = CGAffineTransform.identity
                }, completion: { (isFinished) in
                    
            })
            
        }
    }
    
    internal func choosePhoto(with delegate: UIViewController)-> void {
        let alert = UIAlertController(title: .empty, message: .empty, preferredStyle: .alert)
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = delegate as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.mediaTypes = [kUTTypeImage as string]
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { [weak self] (action) in
            if (UIImagePickerController.isSourceTypeAvailable(.camera)) {
                imagePicker.sourceType = .camera
                self?.present(imagePicker, animated: true, completion: nil)
            } else {
                self?.showAlert(with: string.empty, and: "camera not available")
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Photo Libarary", style: .default, handler: { [weak self] (action) in
            imagePicker.sourceType = .photoLibrary
            self?.present(imagePicker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    internal func fetchPlaces(by key: string, region: MKCoordinateRegion, OnComplete: @escaping([MKMapItem])->())-> void {
        DispatchQueue.global(qos: .userInitiated).async {
            let request = MKLocalSearchRequest()
            request.naturalLanguageQuery = key
            request.region = region
            MKLocalSearch(request: request).start { (response, error) in
                if (error != nil) {
                    print("error is: \(error)")
                    return
                }
                if let items = response?.mapItems {
                    DispatchQueue.main.async {
                        OnComplete(items)
                    }
                }
            }
        }
    }
    
    internal func getPlaces(by key: string, complationHandler: @escaping([string])-> void)-> void {
        guard !key.isEmpty else { return }
        UIApplication.showIndicator(by: true)
        let link = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
        //let apiKey = "AIzaSyCgMnZG8jiB3nCOwbWZfJ8HZi8X0vXbxxE"
        Alamofire.request(link, method: .get, parameters: [ "input": key, "key": Constants.Misc.GoogleMapApiKey])
            .responseJSON { (response) in
                let result = response.result
                if (result.error != nil) {
                    print("fetch places error is: \(result.error)")
                    UIApplication.showIndicator(by: false)
                    return
                }
                //print("places value is: \(result.value)")
                if let value = result.value as? [string: any] {
                    let isSuccess = value["status"] as? string ?? .empty == "OK"
                    if (isSuccess) {
                        if let predictions = value["predictions"] as? [[string: any]] {
                            var places = Array<string>()
                            predictions.forEach { places.append(($0["description"] as? string) ?? string.empty) }
                            complationHandler(places)
                        } else {
                            UIApplication.showIndicator(by: false)
                            return
                        }
                    } else {
                        UIApplication.showIndicator(by: false)
                        return
                    }
                } else {
                    UIApplication.showIndicator(by: false)
                    return
                }
        }
    }
    
    internal func geocode(this address: string, complationHandler: @escaping(CLLocationCoordinate2D)-> void)-> void {
        if (Gemo.gem.isConnected) {
            //maps.googleapis.com/maps/api/geocode/json?address=\()&key=\()
            let link = "https://maps.googleapis.com/maps/api/geocode/json"
            Alamofire.request(link, method: .get, parameters: [ "address": address, "key": Constants.Misc.GoogleMapApiKey])
                .responseJSON { [weak self] (response) in
                    guard let this = self else { return }
                    let result = response.result
                    if (result.error != nil) {
                        print("geocode address error is: \(result.error)")
                        this.showAlert(with: .empty, and: "invalidaddresslan".localized())
                        return
                    }
                    if let value = response.value as? Dictionary<string, any>, let fResult = (value["results"] as? [Dictionary<string, any>])?.first, let geometry = fResult["geometry"] as? [string: any], let location = geometry["location"] as? Dictionary<string, any>, let latitude = location["lat"] as? double, let longitude = location["lng"] as? double {
                        print("latitude is: \(latitude), longitude is: \(longitude)")                        
                        complationHandler(CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
                    } else {
                        print("google geocode nix data")
                    }
            }
            /*let geocoder = CLGeocoder()
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                geocoder.geocodeAddressString(address) { (placemarks, error) in
                    if (error != nil) {
                        print("geocode error is: \(error)")
                        self?.showAlert(with: .empty, and: "invalidaddresslan".localized())
                        return
                    }
                    if let places = placemarks {
                        guard places.count > Number.zero.intValue else { return }
                        DispatchQueue.main.async {
                            complationHandler(places[0])
                        }
                    } else {
                        print("empty places")
                    }
                }
            }*/
        } else {
            showAlert(with: .empty, and: Constants.Localizer.InternetConnection.localized())
        }
    }
    
    private func reverse(this location: CLLocation, onComplate: @escaping(string)-> void)-> void {
        if (Gemo.gem.isConnected) {
            /*let geocoder = CLGeocoder()
            DispatchQueue.global(qos: .userInitiated).async { //[weak self] in
                geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
                    if (error != nil) {
                        print("reverse location error is: \(error)")
                        return
                    }
                    if let placemark = placemarks?.first {
                        DispatchQueue.main.async {
                            onComplate(placemark)
                        }
                    } else {
                        print("no placesmarks")
                    }
                }
            }*/
            let geocoder = GMSGeocoder()
            geocoder.reverseGeocodeCoordinate(location.coordinate) { (response, error) in
                if let address = response?.firstResult(), let lines = address.lines {
                    onComplate(lines.joined(separator: "\n"))
                } else {
                    print("null info when reverse location to address")
                }
            }
        } else {
            showAlert(with: .empty, and: Constants.Localizer.InternetConnection.localized())
        }
        
    }
    
    internal func address(from location: CLLocation, onComplete: @escaping(string)->void)-> void {
        /*reverse(this: location) { (placemark) -> void in
            var addresses = [string.empty]
            if let name = placemark.addressDictionary?["Name"] as? string {
                addresses.append(name)
            }
            if let city = placemark.addressDictionary?["City"] as? string {
                addresses.append(city)
            }
            if let state = placemark.addressDictionary?["State"] as? string {
                addresses.append(state)
            }
            if let country = placemark.country {
                addresses.append(country)
            }
            //print(addresses)
            var address = string.empty
            addresses.forEach { (str) in
                if (!str.isEmpty) {
                    address += "\(str), "
                }
            }
            let fAddress = address.trimmingCharacters(in: .whitespaces)
            let index = fAddress.index(fAddress.endIndex, offsetBy: -1)
            let truncatedAddress = fAddress.substring(to: index)
            onComplete(truncatedAddress)
        }*/
        reverse(this: location, onComplate: onComplete)
    }
    
    internal func drawRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D, for map: GMSMapView, onComplete: @escaping(GMSPolyline ,double)->void)-> void {
        let link = "https://maps.googleapis.com/maps/api/directions/json"
        Alamofire.request(link, method: .get, parameters: [
            "key": Constants.Misc.GoogleMapApiKey,
            "sensor": false,
            "origin": "\(source.latitude),\(source.longitude)",
            "destination": "\(destination.latitude),\(destination.longitude)",
            "mode": "driving",
            "transit_mode": "car",
            "alternatives": true
            ])
            .responseJSON { (response) in
                let result = response.result
                if (result.error != nil) {
                    print("get directions error is: \(result.error)")
                    return
                }
                if let value = response.value as? Dictionary<string, any>, let route = (value["routes"] as? [[string: any]])?.first {
                    if let overviewPolyline = route["overview_polyline"] as? Dictionary<string, any>, let points = overviewPolyline["points"] as? string, let leg = (route["legs"] as? [[string: any]])?.first, let distance = (leg["distance"] as? [string: any])?["value"] as? double {
                        let path = GMSPath(fromEncodedPath: points)
                        let polyline = GMSPolyline(path: path)
                        polyline.strokeColor = .darkGray
                        polyline.strokeWidth = 3.0
                        polyline.map = map
                        onComplete(polyline, distance/1000)
                    }
                } else {
                    print("none directions value")
                }
        }
        /*DispatchQueue.global(qos: .userInitiated).async {
            // place marks
            let sourcePlacMark = MKPlacemark(coordinate: source, addressDictionary: nil)
            let destinationPlaceMark = MKPlacemark(coordinate: destination, addressDictionary: nil)
            // map items
            let sourceMapItem = MKMapItem(placemark: sourcePlacMark)
            let destinationMapItem = MKMapItem(placemark: destinationPlaceMark)
            // annotations
            let sourcePin = SourcePin(coordinate: source, title: .empty, subtitle: .empty)
            let destinationPin = DestinationPin(coordinate: destination, title: .empty, subtitle: .empty)
            DispatchQueue.main.async {
                map.removeAnnotations(map.annotations)
                map.showAnnotations([sourcePin, destinationPin], animated: true)
            }
            // directions
            let directionRequest = MKDirectionsRequest()
            directionRequest.source = sourceMapItem
            directionRequest.destination = destinationMapItem            
            directionRequest.transportType = .any
            directionRequest.requestsAlternateRoutes = false
            // calculate directions
            let directions = MKDirections(request: directionRequest)            
            directions.calculate { [weak self] (response, error) in
                if (error != nil) {
                    //self?.showAlert(with: .empty, and: "cannotdrwaroutelan".localized())
                    print("draw route request error is: \(error)")
                    return
                }
                let overlays = map.overlays
                DispatchQueue.main.async {
                    map.removeOverlays(overlays)
                    var distance = Number.zero.doubleValue
                    response?.routes.forEach { (route) in
                        map.add(route.polyline, level: .aboveRoads)
                        distance = route.distance
                    }
                    onComplete(distance/1000)
                }
                /*if let route = response?.routes.first {
                 map.add(route.polyline, level: .aboveRoads)
                 map.setRegion(MKCoordinateRegionForMapRect(route.polyline.boundingMapRect), animated: true)
                 onComplete(route.distance/1000)
                 }*/
            }

        }*/
    }
    
    internal func region(for coordinate: CLLocationCoordinate2D)-> GMSCameraPosition {
        return GMSCameraPosition.camera(withTarget: coordinate, zoom: 16.0)
    }
    
    internal func annotation(for coordinate: CLLocationCoordinate2D, in map: GMSMapView?, icon: UIImage?)-> GMSMarker {
        let annotation = GMSMarker()
        annotation.position = coordinate
        annotation.icon = icon
        annotation.appearAnimation = .pop
        annotation.map = map
        return annotation
    }

    
    internal func openNotificationPopup(who: string, name: string, driverId: string, avatar: string, driverPhone: string, salary: string, salaryType: string, callid: string, carId: string, carColor: string, carTypeA: string, carTypeE: string)-> void {
        if let popScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Notification) as? Message {
            popScreen.modalPresentationStyle = .popover
            popScreen.popoverPresentationController?.delegate = self
            popScreen.popoverPresentationController?.permittedArrowDirections = .init(rawValue: 0)
            popScreen.popoverPresentationController?.sourceView = view
            let height = view.bounds.height*0.60
            popScreen.preferredContentSize = Size(width: view.bounds.width, height: height)
            let yPoint = view.bounds.height/2
            popScreen.popoverPresentationController?.sourceRect = Rect(x: 0, y: yPoint, width: 0, height: 0)
            popScreen.whoNum = who
            popScreen.name = name
            popScreen.id = driverId
            popScreen.avatar = avatar
            popScreen.carColor = carColor
            popScreen.carId = carId
            popScreen.carTypeA = carTypeA
            popScreen.carTypeE = carTypeE
            popScreen.price = salary
            popScreen.priceType = salaryType
            popScreen.cId = callid
            popScreen.phone = driverPhone
            present(popScreen, animated: true, completion: nil)
        }
    }
    
    internal func openDriverMessagePopup(who: string, name: string, driverId: string, avatar: string, driverPhone: string, salary: string, salaryType: string, callid: string, carId: string, carColor: string, carTypeA: string, carTypeE: string)-> void {
        if let popScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.DriverOrderMessage) as? DriverMessage {
            popScreen.modalPresentationStyle = .popover
            popScreen.popoverPresentationController?.delegate = self
            popScreen.popoverPresentationController?.permittedArrowDirections = .init(rawValue: 0)
            popScreen.popoverPresentationController?.sourceView = view
            let height = view.bounds.height*0.60
            popScreen.preferredContentSize = Size(width: view.bounds.width, height: height)
            let yPoint = view.bounds.height/2
            popScreen.popoverPresentationController?.sourceRect = Rect(x: 0, y: yPoint, width: 0, height: 0)
            popScreen.whoNum = who
            popScreen.name = name
            popScreen.id = driverId
            popScreen.avatar = avatar
            popScreen.carColor = carColor
            popScreen.carId = carId
            popScreen.carTypeA = carTypeA
            popScreen.carTypeE = carTypeE
            popScreen.price = salary
            popScreen.priceType = salaryType
            popScreen.cId = callid
            popScreen.phone = driverPhone
            present(popScreen, animated: true, completion: nil)
        }
    }
    
    internal func openRatePopup()-> void {
        if let rateScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Rate) as? Rate {
            rateScreen.modalPresentationStyle = .popover
            rateScreen.popoverPresentationController?.delegate = self
            rateScreen.popoverPresentationController?.permittedArrowDirections = .init(rawValue: 0)
            rateScreen.popoverPresentationController?.sourceView = view
            let height = view.bounds.height*0.60
            rateScreen.preferredContentSize = Size(width: view.bounds.width, height: height)
            let yPoint = view.bounds.height/2
            rateScreen.popoverPresentationController?.sourceRect = Rect(x: 0, y: yPoint, width: 0, height: 0)
            rateScreen.isModalInPopover = true
            present(rateScreen, animated: true, completion: nil)
        }
    }
    
    internal func openEndedMission()-> void {
        if let endedScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.MissionEndedMessage) as? EndedMissionMessage {
            endedScreen.modalPresentationStyle = .popover
            endedScreen.popoverPresentationController?.delegate = self
            endedScreen.popoverPresentationController?.permittedArrowDirections = .init(rawValue: 0)
            endedScreen.popoverPresentationController?.sourceView = view
            let height = view.bounds.height*0.60
            endedScreen.preferredContentSize = Size(width: view.bounds.width, height: height)
            let yPoint = view.bounds.height/2
            endedScreen.popoverPresentationController?.sourceRect = Rect(x: 0, y: yPoint, width: 0, height: 0)
            endedScreen.whoNum = who
            endedScreen.name = driverName
            endedScreen.id = driverId
            endedScreen.avatar = driverAvatar
            endedScreen.carColor = carColor
            endedScreen.carId = carNumber
            endedScreen.carTypeA = carTypeArabic
            endedScreen.carTypeE = carTypeEnglish
            endedScreen.price = salary
            endedScreen.priceType = salaryType
            endedScreen.cId = callId
            endedScreen.phone = driverPhone
            present(endedScreen, animated: true, completion: nil)
        }

    }
    
    internal func navigate(to page: UIViewController, animated: bool = true)-> void {
        navigationController?.pushViewController(page, animated: animated)
    }
    
    internal func dialogUserToOpenNotification()-> void {
        let alert = UIAlertController(title: "warningtitlelan".localized(), message: "notificationMessagelan".localized(), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak self] (action) in
            self?.checkNotification()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)        
    }
    
    @objc fileprivate func checkNotification()-> void {
        /*switch (who) {
        case "14":
            print("goto map screen")
            NotificationCenter.default.post(name: Constants.Misc.UserJourney, object: nil)
        case "12":
            NotificationCenter.default.post(name: Constants.Misc.Message, object: nil)
        case "15":
            NotificationCenter.default.post(name: Constants.Misc.MissionEnded, object: nil)
        case "1":
            NotificationCenter.default.post(name: Constants.Misc.DriverMessage, object: nil)
        default:
            break
        }*/
        switch (who) {
        case "14", "3":
            NotificationCenter.default.post(name: Constants.Misc.UserJourney, object: nil)
            
        case "12":
            NotificationCenter.default.post(name: Constants.Misc.Message, object: nil)
            
        case "15":
            NotificationCenter.default.post(name: Constants.Misc.MissionEnded, object: nil)
            
        case "1":
            NotificationCenter.default.post(name: Constants.Misc.DriverMessage, object: nil)
            
        case "4":
            isRateDriver = true
            NotificationCenter.default.post(name: Constants.Misc.MissionEnded, object: nil)
            
        case "6":
            isCompany = true            
            NotificationCenter.default.post(name: Constants.Misc.DriverMessage, object: nil)
            
        case "10":
            isRateCompany = true
            NotificationCenter.default.post(name: Constants.Misc.MissionEnded, object: nil)
        default:
            break
        }
    }


}


// MARK: - Popover screen delegate functions 

extension BaseController: UIPopoverPresentationControllerDelegate
{
    
    internal func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}

























