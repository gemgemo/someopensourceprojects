
import Gemo


// MARK: - Storyboard
struct Storyboards {
    internal static let SignIn = "com.store.signin.view.id",
                        RegisterMainNav = "com.store.register.main.nav.view.id",
                        SignUp = "com.store.signup.view.id",
                        StoreInfo = "com.store.register.store.info.view.id",
                        MainMainNavigation = "com..store.main.main.nav.view.id.io",
                        Main = "Main",
                        Registration = "Registration",
                        Home = "com.home.view.id.io",
                        Popover = "com.popover.view.id.io",
                        EditProduct = "com.add.ptoduct.view.id.io",
                        Orders = "view.orders.view.id.io",
                        Complaints = "com.complaints.view.id.io",
                        Notifications = "com.notifications.view.id.io",
                        Settings = "com.settings.view.id.io",
                        AboutAndTerms = "com.view.abou.and.terms.id.io",
                        ProductDetails = "com.product.details.view.id.io"
}

// MARK: - Nibs
struct Nibs {
    internal static let Product = UINib(nibName: "ProductCell", bundle: nil),
    Notification = UINib(nibName: "NotificationCell", bundle: nil),
    NotificationProduct = UINib(nibName: "NotificationProductCell", bundle: nil),
    Order = UINib(nibName: "OrderCell", bundle: nil)
}

// MARK: - Reuse Identifier
struct ReuseIdentifiers {
    internal static let Product = "com.product.cell.view.id.io",
    Popup = "com.popup.cell.view.id.io",
    Notification = "com.notification.cell.view.id.io",
    NotificationProduct = "com.notification.product.cell.view.id.io",
    Complaint = "com.complaint.view.cell.id.io",
    Slider = "com.slider.cell.view.id.io",
    Order = "com.order.cell.view.id.io"
}

// MARK: - Misc
struct Misc {
    internal static let MapKey = "AIzaSyCUnUBl_V3_dI3mGKQLljjpVNAbBhbkU0A",
                        MainKey = "key",
                        MainValue = "BeQALa",
                        IdKey = "id",
                        LanguageParameterName = "lang",
                        UserLoggedIn = "com.user.looged.in.key.io",
                        GroceryName = "com.grocery.name.io.key",
                        GroceryId = "com.Grocery.id.io.key",
                        GroceryAvatar = "com.Grocery.avatar.key.io",
                        GroceryUsername = "com.Grocery.username.io.key",
                        GroceryPhone = "com.Grocery.phone.io.key",
                        GroceryEmail = "com.Grocery.email.io.key",
                        GroceryLatitude = "com.Grocery.latitude.io.key",
                        GroceryLongitude = "com.Grocery.longitude.io.key",
                        GroceryDepartments = "com.Grocery.spesc.io.key",
                        GroceryDescription = "com.Grocery.description.io.key"
}


// MARK: - Enumerations

internal enum Gender: int {
    case male = 1, female = 2
}

// MARK: - Backend Urls
internal struct Urls {
    
    internal static let Sections = getUrl(by: "category"),
                        Register = getUrl(by: "register"),
                        Login = getUrl(by: "login"),
                        ResetPassword = getUrl(by: "recover"),
                        Products = getUrl(by: "all_product"),
                        DeleteProduct = getUrl(by: "delete_product"),
                        Notifications = getUrl(by: "select_all_orders_progress"),
                        UpdateStore = getUrl(by: "edit_user"),
                        Logout = "http://192.232.214.91/~shopkeeper/JSON_CLIENT/delete_token.php",//getUrl(by: "delete_token"),
                        About = getUrl(by: "about_us"),
                        Terms = getUrl(by: "term"),
                        Complaints = getUrl(by: "comp"),
                        AddProduct = getUrl(by: "add_product"),
                        SectionsById = getUrl(by: "matjer_cat"),
                        UpdateProduct = getUrl(by: "edit_product"),
                        CurrentOrders = getUrl(by: "select_all_orders_now"),
                        LastOrders = getUrl(by: "select_all_old_orders"),
                        AddToken = "http://192.232.214.91/~shopkeeper/JSON_CLIENT/users_noti.php"
    
    
    private static func getUrl(by name: string)-> string {
        return "http://192.232.214.91/~shopkeeper/JSON/\(name).php" 
    }
}


// MARK: - Colors

extension Color {
    
    internal static var main: Color {
        return Color.rgb(red: 77, green: 111, blue: 139, alpha: 1.0)
    }
}


extension Notification.Name {
    
    internal static var UpdateProducts: Notification.Name {
        return Notification.Name("com.update.products.io.name")
    }
    
    internal static var OpenNotifications: Notification.Name {
        return Notification.Name("com.open.notifications.io.name")
    }
    
    internal static var RefreshNotifications: Notification.Name {
        return Notification.Name("com.notifications.refresh.io.gr")
    }
    
    internal static var UpdateNotificationsSwitch: Notification.Name {
        return Notification.Name("notifications.switch.update.io.gr")
    }
}












