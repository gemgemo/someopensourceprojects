
import UIKit

final class TeachersVC: UIViewController,
    UICollectionViewDelegate,
    UICollectionViewDataSource
{

    @IBOutlet weak var myAdsing: UIImageView!
    @IBOutlet weak var offerLbl: UILabel!
    @IBOutlet weak var myAdsLbl: UILabel!
    @IBOutlet weak var addAdslbl: UILabel!
    @IBOutlet weak var callUsLbl: UILabel!
    @IBOutlet weak var openMenu: UIBarButtonItem!
    @IBOutlet weak var loadingLbl: UILabel!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var shadowView: UIView!
    var selectedId = ""
    var selectedName = ""
    var selectedGender = ""
    var scholarPhase = ""
    var dataFound: Bool!
    var type:String!

    var teachers = Array<Teacher>()
    var images_cache = [String: UIImage]()
    @IBOutlet weak var teacherCollection: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        loadingView.layer.cornerRadius = 4
        loadingView.layer.masksToBounds = true
        loadingView.isHidden = true
        shadowView.isHidden = true
        teacherCollection.delegate = self
        teacherCollection.dataSource = self
        getTeacher(Settings.lang, selectedGender, selectedId, scholarPhase)
        self.title = selectedName
        if Settings.lang == "ar"
        {
            loadingLbl.text = "جاري التحميل..."
            callUsLbl.text = "اتصل بنا"
            addAdslbl.text = "اضف اعلانك"
            myAdsLbl.text = "اعلاناتي"
            offerLbl.text = "العروض"

        }
        if revealViewController() != nil {
            
            revealViewController().rightViewRevealWidth = 235
            openMenu.target = revealViewController()
            openMenu.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        type = UserDefaults.standard.string(forKey: "userType")
        if type == "1" {
            if Settings.lang == "ar" {
                myAdsLbl.text = "المفضلة"
                myAdsing.image = UIImage(named: "fav")
            } else {
                myAdsLbl.text = "Favorite"
                myAdsing.image = UIImage(named: "fav")
            }
        }
    }
    
    func getTeacher(_ Lang: String,_ gender: String,_ subjectID: String,_ scholarID: String)  {
        shadowView.isHidden = false
        loadingView.isHidden = false
        loadingActivity.startAnimating()
        let request = NSMutableURLRequest(url: URL(string:Settings.Teachers)!)
        request.httpMethod = "POST"
        let postString = "lang=\(Lang)&gender=\(gender)&subject=\(subjectID)&scholarPhase=\(scholarID)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
            
            if let responseData = data {
                
                do {
                    
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                    print("ddd\(json)")
                    self.dataFound = json["Success"] as? Bool
                    if self.dataFound!
                    {
                        
                        let data = json["Result"] as! [NSDictionary]
                        for i in data
                        {
                            
                            let id = i["id"] as! String
                            let teacherName = i["username"] as! String
                            let teacherImg = i["img"] as! String
                            let subjectName = i["subjectName"] as! String
                            let phase = i["scholarPhaseName"] as! String
                            let detial = i["details"] as! String
                            let date = i["date_time"] as! String

                            
                            let teacher = Teacher(id: id, teacherName: teacherName, teacherImg: teacherImg, subjectName: subjectName, phase: phase, detial: detial, date: date)
                            self.teachers.append(teacher)
                            
                        }
                    }
                    DispatchQueue.main.async
                        {
                            self.shadowView.isHidden = true
                            self.loadingView.isHidden = true
                            self.loadingActivity.stopAnimating()
                            self.teacherCollection.reloadData()
                            
                    }
                    
                }
                catch
                {
                    print ("Could not Serialize")
                }
                
            }
            
        }
        task.resume()
        

        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        let size = self.teacherCollection.frame.width
//        let height = self.teacherCollection.frame.height / 3
        
        return CGSize(width: size  - 10, height: 150)
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "teacherCell", for: indexPath)
            as! Teachers
        if Settings.lang == "ar"
        {
            if self.teachers[indexPath.row].teacherImg != ""
            {
                cell.teacherImg.image = nil
                let escapedString = self.teachers[indexPath.row].teacherImg.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
                load_image(escapedString!, imageview: cell.teacherImg)
                cell.teacherLbl.text = "الاستاذ:"
                cell.detialsLbl.text = "تفاصيل:"
                cell.subjectLbl.text = "\(self.teachers[indexPath.row].subjectName):"
                cell.phaseName.text = self.teachers[indexPath.row].phase
                cell.teacherName.text = self.teachers[indexPath.row].teacherName
                print("count\(self.teachers[indexPath.row].detial.characters.count)")
                if self.teachers[indexPath.row].detial.characters.count > 10
                {
                    let str = self.teachers[indexPath.row].detial
                    var strTemp = str as NSString
                    //Keep the first 30 characters.
                    strTemp = strTemp.substring(to: 10) as NSString
                    cell.detailLbl.text = strTemp as String
                    print("detialsss\(strTemp as String)")
                    
                    
                }
                else
                {
                    cell.detailLbl.text = self.teachers[indexPath.row].detial
                }
            
                cell.teacherImg.layer.cornerRadius = 0.5 * cell.teacherImg.bounds.size.width
                cell.teacherImg.clipsToBounds = true
                cell.teacherImg.layer.borderWidth = 2
                cell.teacherImg.layer.borderColor = UIColor.white.cgColor
                cell.teacherView.layer.cornerRadius = 10
                cell.teacherView.layer.masksToBounds = true
                let fullNameArr = self.teachers[indexPath.row].date.characters.split{$0 == " "}.map(String.init)
                cell.dateLbl.text = fullNameArr[0]
                cell.timeLbl.text = fullNameArr[1]
                cell.id = self.teachers[indexPath.row].id

                
            }
            else
            {
                cell.teacherImg.image = nil
                cell.teacherImg.image = UIImage(named: "pro_img")
                cell.teacherLbl.text = "الاستاذ:"
                cell.detialsLbl.text = "تفاصيل:"
                cell.subjectLbl.text = "\(self.teachers[indexPath.row].subjectName):"
                cell.phaseName.text = self.teachers[indexPath.row].phase
                cell.teacherName.text = self.teachers[indexPath.row].teacherName
                if self.teachers[indexPath.row].detial.characters.count > 10
                {
                    let str = self.teachers[indexPath.row].detial
                    var strTemp = str as NSString
                    //Keep the first 30 characters.
                    strTemp = strTemp.substring(to: 10) as NSString
                    cell.detailLbl.text = strTemp as String
                    print("detialsss\(strTemp as String)")
                    
                    
                }
                else
                {
                    cell.detailLbl.text = self.teachers[indexPath.row].detial
                }
                cell.detailLbl.text = self.teachers[indexPath.row].detial                
                cell.teacherImg.layer.cornerRadius = 0.5 * cell.teacherImg.bounds.size.width
                cell.teacherImg.clipsToBounds = true
                cell.teacherImg.layer.borderWidth = 2
                cell.teacherImg.layer.borderColor = UIColor.white.cgColor
                cell.teacherView.layer.cornerRadius = 10
                cell.teacherView.layer.masksToBounds = true
                let fullNameArr = self.teachers[indexPath.row].date.characters.split{$0 == " "}.map(String.init)
                cell.dateLbl.text = fullNameArr[0]
                cell.timeLbl.text = fullNameArr[1]
                cell.id = self.teachers[indexPath.row].id


                
            }
            
        }
        else
        {
            if self.teachers[indexPath.row].teacherImg != ""
            {
                cell.teacherImg.image = nil
                let escapedString = self.teachers[indexPath.row].teacherImg.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
                
                load_image(escapedString!, imageview: cell.teacherImg)
                cell.subjectLbl.text = "\(self.teachers[indexPath.row].subjectName):"
                cell.phaseName.text = self.teachers[indexPath.row].phase
                cell.teacherName.text = self.teachers[indexPath.row].teacherName
                print("count\(self.teachers[indexPath.row].detial.characters.count)")
                if self.teachers[indexPath.row].detial.characters.count > 10
                {
                    let str = self.teachers[indexPath.row].detial
                    var strTemp = str as NSString
                    //Keep the first 30 characters.
                    strTemp = strTemp.substring(to: 10) as NSString
                    cell.detailLbl.text = strTemp as String
                    print("detialsss\(strTemp as String)")
                    
                    
                }
                else
                {
                    cell.detailLbl.text = self.teachers[indexPath.row].detial
                }
                
                cell.teacherImg.layer.cornerRadius = 0.5 * cell.teacherImg.bounds.size.width
                cell.teacherImg.clipsToBounds = true
                cell.teacherImg.layer.borderWidth = 2
                cell.teacherImg.layer.borderColor = UIColor.white.cgColor
                cell.teacherView.layer.cornerRadius = 10
                cell.teacherView.layer.masksToBounds = true
                let fullNameArr = self.teachers[indexPath.row].date.characters.split{$0 == " "}.map(String.init)
                cell.dateLbl.text = fullNameArr[0]
                cell.timeLbl.text = fullNameArr[1]
                cell.id = self.teachers[indexPath.row].id
                
                
            }
            else
            {
                cell.teacherImg.image = nil
                cell.teacherImg.image = UIImage(named: "pro_img")
                cell.subjectLbl.text = "\(self.teachers[indexPath.row].subjectName):"
                cell.phaseName.text = self.teachers[indexPath.row].phase
                cell.teacherName.text = self.teachers[indexPath.row].teacherName
                if self.teachers[indexPath.row].detial.characters.count > 10
                {
                    let str = self.teachers[indexPath.row].detial
                    var strTemp = str as NSString
                    //Keep the first 30 characters.
                    strTemp = strTemp.substring(to: 10) as NSString
                    cell.detailLbl.text = strTemp as String
                    print("detialsss\(strTemp as String)")
                    
                    
                }
                else
                {
                    cell.detailLbl.text = self.teachers[indexPath.row].detial
                }
                cell.detailLbl.text = self.teachers[indexPath.row].detial
                cell.teacherImg.layer.cornerRadius = 0.5 * cell.teacherImg.bounds.size.width
                cell.teacherImg.clipsToBounds = true
                cell.teacherImg.layer.borderWidth = 2
                cell.teacherImg.layer.borderColor = UIColor.white.cgColor
                cell.teacherView.layer.cornerRadius = 10
                cell.teacherView.layer.masksToBounds = true
                let fullNameArr = self.teachers[indexPath.row].date.characters.split{$0 == " "}.map(String.init)
                cell.dateLbl.text = fullNameArr[0]
                cell.timeLbl.text = fullNameArr[1]
                cell.id = self.teachers[indexPath.row].id
                
                
                
            }

            
        }
        
        
        return cell
        
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.teachers.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let cell = teacherCollection.cellForItem(at: indexPath) as! Teachers
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TeacherDetialsVC") as! TeacherDetialsVC
        vc.selectedID = cell.id
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func load_image(_ link:String, imageview:UIImageView)
    {
        
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.timeoutInterval = 10
        
        
        let task = session.dataTask(with: url, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
                
                guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                    
                    return
                }
                
                
                var image = UIImage(data: data!)
                
                if (image != nil)
                {
                    
                    
                    func set_image()
                    {
                        self.images_cache[link] = image
                        imageview.image = image
                    }
                    
                    
                    DispatchQueue.main.async(execute: set_image)
                    
                }
                
        })
        
        task.resume()
        
    }
    func displayAlerts(_ Massage: String)
    {
        var t = ""
        var b = ""
        
        if Settings.lang == "ar"{
            t = "تنبيه"
            b = "إغلاق"
        }
        else{
            t = "Alert"
            b = "Close"
        }
        
        let myAlert = UIAlertController(title: t, message:Massage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }




    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
    
    @IBAction func callusBtn(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CallUsVc") as! CallUsVc
        self.navigationController?.pushViewController(vc, animated: false)

    }

    @IBAction func addAdsBtn(_ sender: UIButton) {
        if type == "1"
        {
        if Settings.lang == "ar"
        {
            let msg = "انت طالب لا يمكنك اضافة اعلان"
            self.displayAlerts(msg)
            
        }
        else
        {
            let msg = "you'r student can't add Ads"
            self.displayAlerts(msg)
            
        }
        }
    else
    {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let vc = storyboard.instantiateViewController(withIdentifier: "AddAdsVc") as! AddAdsVc
    self.navigationController?.pushViewController(vc, animated: false)
    
    }

    }


    @IBAction func myAdsBtn(_ sender: UIButton) {
        if type == "1"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyFavoriteVC") as! MyFavoriteVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyAdsVC") as! MyAdsVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        

    }
    
    @IBAction func offerBtn(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AdsPageViewController") as! AdsPageViewController
        self.navigationController?.pushViewController(vc, animated: false)

    }
  

}

