
import Foundation

final class HttpClient
{
    
    private static let session = URLSession.shared
    
    internal enum Method: string {
        case get = "GET", post = "POST", put = "PUT", delete = "DELETE"
    }
    
    internal class func request(link: string, method: Method, parameters: [string: any] = [:], _ onComplete: @escaping(_ JSON: any?, _ response: URLResponse?, _ error: Error?)-> void)-> void {
        if (link.isEmpty || !link.isLink) {
            print("requested link \(link)")
            onComplete(nil, nil, NSError(domain: "Invalid Link", code: 1221, userInfo: nil))
            return
        }
        guard let url = URL(string: link) else {
            onComplete(nil, nil, NSError(domain: "Invalid URL", code: 2112, userInfo: nil))
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        if (method == .post) {
            var params = string.empty
            parameters.forEach{ params += "\($0)=\($1)&" }
            request.httpBody = params.substring(to: params.index(params.endIndex, offsetBy: -1)).data(using: .utf8)
        }
        session.dataTask(with: request) { (data, response, error) in
            if (error != nil) {
                onComplete(nil, nil, error)
                return
            }
            guard let jsonData = data else {
                onComplete(nil, nil, NSError(domain: "Not found data!", code: 3223, userInfo: nil))
                return
            }
            do {
                onComplete(try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments), response, nil)
            } catch let catchError {
                print("the error here")
                onComplete(nil, nil, catchError)
            }
            
        }.resume()
    }
    
    
    /*internal class func requestWithMultipart(link: string, onComplete: @escaping(_ JSON: any?, _ response: URLResponse?, _ error: Error?)->void)-> void {
        guard !link.isEmpty else {
            onComplete(nil, nil, NSError(domain: "Link is empty", code: 0890, userInfo: nil))
            return
        }
        guard let url = URL(string: link) else {
            onComplete(nil, nil, NSError(domain: "Url error", code: 9080, userInfo: nil))
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = Method.post.rawValue
        let boundary = "Boundary-\(UUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = createBody(parameters: <#T##[String : String]#>, boundary: boundary, data: <#T##Data#>, mimeType: <#T##String#>, filename: <#T##String#>)
    }
    
    private static func createBody(parameters: [String: String], boundary: String, data: Data, mimeType: String, filename: String) -> Data {
        let body = NSMutableData()
        
        let boundaryPrefix = "--\(boundary)\r\n"
        
        for (key, value) in parameters {
            body.appendString(boundaryPrefix)
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }
        
        body.appendString(boundaryPrefix)
        body.appendString("Content-Disposition: form-data; name=\"file\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimeType)\r\n\r\n")
        body.append(data)
        body.appendString("\r\n")
        body.appendString("--".appending(boundary.appending("--")))
        
        return body as Data
    }*/
    
    
}


/*extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}*/



// MARK:- Dictionary Mapper

internal class Mapper<T>: Object
{
    typealias dicObject = Dictionary<string, any>
    private var json: dicObject?, currentValue: any?
    
    internal init(JSON: dicObject) {
        self.json = JSON
        super.init()
        mapping(mapper: self)
    }
    
    override init() {
        super.init()
    }
    
    internal func mapping(mapper: Mapper)-> void {}
    
    private init(currentVal: any) {
        self.currentValue = currentVal
    }
    
    internal subscript(key: string)-> Mapper? {
        return Mapper(currentVal: json?[key] as any)
    }
    
    final internal var text: string {
        return currentValue as? string ?? .empty
    }
    
    final internal var integer: int {
        return currentValue as? int ?? Number.zero.intValue
    }
    
    final internal var dictionary: dicObject {
        return currentValue as? dicObject ?? [:]
    }
    
    final internal var dictionaries: Array<dicObject> {
        return currentValue as? Array<dicObject> ?? Array()
    }
    
    final internal var array: Array<T> {
        return currentValue as? Array<T> ?? Array()
    }
    
    final internal var boolean: bool? {
        return currentValue as? bool
    }
    
    
}















