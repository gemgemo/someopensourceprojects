
import UIKit

final class Shops: BaseController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var dataSource = Array<Shop>() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tblShops.reloadData()
            }
        }
    }
    
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var titlePanal: UIView! {
        didSet {
            shadow(for: titlePanal)
        }
    }   
    @IBOutlet fileprivate weak var tblShops: UITableView! {
        didSet {
            tblShops.delegate = self
            tblShops.dataSource = self
        }
    }
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var btnMenu: UIButton!
    @IBOutlet fileprivate weak var btnFavorite: UIButton!
    @IBOutlet fileprivate weak var btnSearch: UIButton!
    
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        print("userName \(UserDefaults.standard.string(forKey: Constants.Misc.UserName) ?? .empty)")
        setupTableView()        
        if (Gemo.gem.isConnected) {
            loadData()
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarItems?.selectItem(at: IndexPath(item: 3, section: 0), animated: false, scrollPosition: .top)
        selectedTabBarItem = 3
        updateUi()
        selectedItemIndex = 4
    }
    
    
    // MARK: - Actions
    
    @IBAction private func menuClicked(_ sender: UIButton) {
        menu(withState: .opened)
    }
    
    @IBAction private func searchClicked(_ sender: UIButton) {
        navigateToSearch()
    }
    
    @IBAction private func favoritesClicked(_ sender: UIButton) {
        navigateToFavorites()
    }

}


// MARK: - Helper Functions

extension Shops
{
    
    fileprivate func setupTableView()-> void {
        tblShops.register(Constants.Nibs.Shop, forCellReuseIdentifier: Constants.ReuseIdentifiers.Shop)
        tblShops.alwaysBounceVertical = true
        let margin = Insets(top: 0, left: 0, bottom: 60, right: 0)
        tblShops.contentInset = margin
        tblShops.scrollIndicatorInsets = margin
    }
    
    fileprivate func updateUi()-> void {
        lblTitle.text = Localization.Shops.localized()
        btnMenu.transform = CGAffineTransform.identity
        view.bringSubview(toFront: titlePanal)
        if (Localizer.instance.current == .arabic) {
            btnMenu.rightAnchor.constraint(equalTo: titlePanal.rightAnchor, constant: -8.0).isActive = true
            btnFavorite.leftAnchor.constraint(equalTo: titlePanal.leftAnchor, constant: 8.0).isActive = true
            btnSearch.leftAnchor.constraint(equalTo: btnFavorite.rightAnchor, constant: 20.0).isActive = true
            btnMenu.transform = CGAffineTransform.identity
        } else {
            btnMenu.leftAnchor.constraint(equalTo: titlePanal.leftAnchor, constant: 8.0).isActive = true
            btnFavorite.rightAnchor.constraint(equalTo: titlePanal.rightAnchor, constant: -8.0).isActive = true
            btnSearch.rightAnchor.constraint(equalTo: btnFavorite.leftAnchor, constant: -20.0).isActive = true
            btnMenu.transform = btnMenu.transform.rotated(by: cgFloat.pi)
        }
    }
    
    fileprivate func loadData()-> void {      
        Shop.getData(from: Constants.Urls.Shops, withParameters: [Constants.Misc.MainKey: Constants.Misc.MainValue], self) { [weak self] (shops) in
            if (!shops.isEmpty) {
                self?.dataSource = shops
            }
        }
    }
    
}




// MARK: - Table view delegate & data source functions

extension Shops: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifiers.Shop, for: indexPath) as? ShopCell else {
            return UITableViewCell()
        }
        cell.shop = dataSource[indexPath.row]
        return cell
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected store", dataSource[indexPath.row])
        guard let storePage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.Store) as? Store else { return }
        storePage.store = dataSource[indexPath.row] 
        navigate(to: storePage)
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130.0
    }
    
}






















