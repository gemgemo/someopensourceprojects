
import Foundation

final class Album
{
    internal var id, name, sort, active, photoId, photoCategory, cover: String?, count: Int?, pictures =  [Photo]()
    
    init(dictionary: [String: Any]) {
        id = dictionary["album_id"] as? String
        name = dictionary["name"] as? String
        sort = dictionary["sort"] as? String
        active = dictionary["active"] as? String
        photoId = dictionary["photo_id"] as? String
        photoCategory = dictionary["photo_category"] as? String
        cover = "http://hmmc.gov.sa/assets/uploads/"+(dictionary["image"] as? String ?? "")
        count = dictionary["count"] as? Int
        if let photos = dictionary["photos"] as? [NSDictionary] {
            photos.forEach { (element) in
                if let dic = element as? [String: Any] {
                    let photo = Photo(dictionary: dic)
                    pictures.append(photo)
                } else {
                    print("none photos")
                }
            }
        }
    }
    
}


final class Photo
{
    internal var id, image: String?
    
    init(dictionary: [String: Any]) {
        id = dictionary["photo_id"] as? String
        image = "http://hmmc.gov.sa/assets/uploads/thumbs/"+(dictionary["image"] as? String ?? "" )
    }
}



