
import UIKit

let IsCompletedRegister = "com.register.complated.io", SentCode = "com.sent.code.io"

class SignUpViewController: UIViewController,
    UIPopoverPresentationControllerDelegate
{
    
    @IBOutlet weak var resendView: UIView!
    @IBOutlet weak var resendLbl: UILabel!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var codeTxt: UITextField!
    @IBOutlet weak var enterLbl: UILabel!
    @IBOutlet weak var sendCodeView: UIView!
    @IBOutlet weak var codeView: UIView!
    @IBOutlet weak var Presonsview: UIView!
    @IBOutlet weak var signInLbl: UILabel!
    @IBOutlet weak var newAccountLbl: UILabel!
    @IBOutlet weak var loadingLabel: UILabel!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var femaleImage: UIImageView!
    @IBOutlet weak var manImage: UIImageView!
    @IBOutlet weak var femaleView: UIView!
    @IBOutlet weak var maleView: UIView!
    @IBOutlet weak var userTypeName: UILabel!
    @IBOutlet weak var userNameText: UITextField!
    @IBOutlet weak var userPhone: UITextField!
    @IBOutlet var signUpView: UIView!
    
    var usersTypeName: String!
    var usersTypeID = "1"
    var massage: String!
    var error: String!
    var dataFound: Bool!
    var keyboardLefted = false
    var code: Int!
    let defaults = UserDefaults.standard
    var check = "0"
    
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        Presonsview.layer.cornerRadius = 5
        Presonsview.layer.masksToBounds = true
        shadowView.isHidden = true
        loadingView.isHidden = true
        codeView.isHidden = true
        sendCodeView.isHidden = true
        resendView.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        loadingView.layer.cornerRadius = 4
        loadingView.layer.masksToBounds = true
        userTypeName.text = "Student"
        if Settings.lang == "ar" {
            enterLbl.text = "يرجى إدخال كود التفعيل"
            sendBtn.setTitle("ارسال", for: .normal)
            resendLbl.text = "إعادة إرسال كود تفعيل"
            signInLbl.text = "تسجيل دخول"
            newAccountLbl.text = "تسجيل حساب جديد"
            newAccountLbl.font = UIFont(name: newAccountLbl.font.fontName, size: 12)
            userNameText.placeholder = "اسم المستخدم"
            userPhone.placeholder = "رقم الجوال"
            loadingLabel.text = "جاري التحميل..."
            userTypeName.text = "طالب"
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func showKeyboard(_ sender: NSNotification){
        if !keyboardLefted
        {
            signUpView.frame.origin.y -= 216
            keyboardLefted = true
            
        }
    }
    
    func hideKeyboard(_ sender: NSNotification){
        signUpView.frame.origin.y = 0
        keyboardLefted = false
    }
    
    func sendActivationCode(_ code: String) {
        let request = NSMutableURLRequest(url: URL(string:Settings.ActivationCode)!)
        request.httpMethod = "POST"
        let postString = "mobile=\(code)"
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        { [weak self] data, response, error in
            guard let this = self else { return }
            guard error == nil && data != nil else {
                print("error=\(String(describing: error?.localizedDescription))")
                return
            }
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
            
            if let responseData = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: responseData, options: .allowFragments) as! [String: AnyObject]
                    print("ddd\(json)")
                    this.dataFound = json["Success"] as? Bool
                    if this.dataFound!
                    {
                        if Settings.lang == "ar"
                        {
                            this.massage = "تم التسجيل بنجاح"
                            this.defaults.set(true, forKey: IsCompletedRegister)
                        } else {
                            this.massage = "Registration Successfully"
                        }
                        
                    }
                    else
                    {
                        if Settings.lang == "ar"
                        {
                            this.error = "حدث خطاء اثناء التسجيل"
                        }
                        else
                        {
                            this.error = "Registration Failed"
                            
                        }
                        
                    }
                    DispatchQueue.main.async
                        {
                            if this.dataFound!
                            {
                                this.displayAlertMessage(this.massage, "")
                                
                            }
                            else
                            {
                                this.displayAlertMessage("" ,this.error)
                            }
                            
                    }
                } catch {
                    print ("Could not Serialize")
                }
                
            }
        }
        task.resume()
        
    }
    
    private func signUpUser(_ lang:String,_ usertype:String,_ userName:String, _ userPhone: String)-> Void {
        print("user is activate account: \(defaults.bool(forKey: IsCompletedRegister))")
        defaults.setValue(userPhone, forKey: "userPhone")
        defaults.set(false, forKey: IsCompletedRegister)
        shadowView.isHidden = false
        loadingView.isHidden = false
        loadingView.layer.cornerRadius = 4
        loadingView.layer.masksToBounds = true
        loadingActivity.startAnimating()
        let request = NSMutableURLRequest(url: URL(string:Settings.SignUp)!)
        request.httpMethod = "POST"
        let postString = "lang=\(lang)&type=\(usertype)&username=\(userName)&mobile=\(userPhone)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {
                print("register error=\(String(describing: error))")
                return
            }
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print(" register responseString = \(String(describing: responseString))")
            if let responseData = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: responseData, options: .allowFragments) as! [String:AnyObject]
                    print("register json: \(json)")
                    self.dataFound = json["Success"] as? Bool
                    if self.dataFound! {
                        self.massage = json["Message"] as! String
                        self.code = json["Code"] as! Int
                    } else {
                        self.error = json["Message"] as! String
                    }
                    DispatchQueue.main.async {
                            if self.dataFound! {
                                self.shadowView.isHidden = true
                                self.loadingView.isHidden = true
                                self.loadingActivity.stopAnimating()
                                self.defaults.set(self.code, forKey: SentCode)
                                self.displayAlertMessage(self.massage, "")
                            } else {
                                self.shadowView.isHidden = true
                                self.loadingView.isHidden = true
                                self.loadingActivity.stopAnimating()
                                self.displayAlertMessage("" ,self.error)
                            }
                    }
                    
                } catch {
                    print ("Could not Serialize")
                }
            }
        }
        task.resume()
    }
    
    func resendAgain(_ mobile: String) {
        let request = NSMutableURLRequest(url: URL(string:Settings.ResendCode)!)
        request.httpMethod = "POST"
        let postString = "mobile=\(mobile)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        { data, response, error in
            guard error == nil && data != nil else {
                print("error=\(String(describing: error))")
                return
            }
            
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
            
            if let responseData = data {
                
                do {
                    
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                    print("ddd\(json)")
                    self.dataFound = json["Success"] as? Bool
                    if self.dataFound!
                    {
                        if Settings.lang == "ar"
                        {
                            self.massage = "تم الارسال بنجاح"
                            self.code = json["Code"] as! Int
                        } else {
                            self.massage = "Successfully sent"
                            self.code = json["Code"] as! Int
                        }
                        DispatchQueue.main.async {
                            UserDefaults.standard.set(self.code, forKey: SentCode)
                        }
                    }
                    else
                    {
                        if Settings.lang == "ar"
                        {
                            self.error = "حدث خطأ"
                        }
                        else
                        {
                            self.error = " Failed"
                            
                        }
                        
                    }
                    
                    DispatchQueue.main.async
                        {
                            if self.dataFound!
                            {
                                self.displayAlertMessage("", self.massage)
                                
                            }
                            else
                            {
                                self.displayAlertMessage("" ,self.error)
                            }
                            
                    }
                    
                }
                catch
                {
                    print ("Could not Serialize")
                }
                
            }
            
        }
        task.resume()
    }
    
    func displayAlertMessage(_ Message:String,_ Error: String) {
        var t = ""
        var b = ""
        if Settings.lang == "ar" {
            t = "تنبيه"
            b = "إغلاق"
        } else {
            t = "Alert"
            b = "Close"
        }
        if Error == "" {
            let myAlert = UIAlertController(title: t, message:Message, preferredStyle: UIAlertControllerStyle.alert);
            
            let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
                if self.check == "1" {
                    self.performSegue(withIdentifier: "signInSegue", sender: self)
                } else {
                    self.shadowView.isHidden = false
                    self.loadingView.isHidden = true
                    self.codeView.isHidden = false
                    self.sendCodeView.isHidden = false
                    self.resendView.isHidden = false
                    self.view.endEditing(true)
                    
                }
                
            })
            myAlert.addAction(okAction);
            
            self.present(myAlert, animated: true, completion: nil)
            
        } else {
            let myAlert = UIAlertController(title: t, message:Error, preferredStyle: UIAlertControllerStyle.alert);
            
            let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler:nil)
            
            myAlert.addAction(okAction);
            
            self.present(myAlert, animated: true, completion: nil)
            
            
        }
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return.none
    }
    
    @IBAction func unwindWithMain(_ segue:UIStoryboardSegue) {
        if let userTypeView = segue.source as? UserTypeViewController,
            let selectedUser = userTypeView.usersTypeName {
            usersTypeName = selectedUser
            print("this user\(usersTypeName)")
            userTypeName.text = self.usersTypeName
            let userTypeID = userTypeView.usersTypeID
            usersTypeID = userTypeID!
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "userType"   {
            let vc = segue.destination as! UserTypeViewController
            vc.preferredContentSize = CGSize(width: 300, height: 88)
            let controller = vc.popoverPresentationController
            controller?.permittedArrowDirections = .up
            if controller != nil { controller?.delegate = self }
        }
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if (!defaults.bool(forKey: IsCompletedRegister) && defaults.string(forKey: "userPhone") != nil) {
            self.shadowView.isHidden = false
            self.codeView.isHidden = false
            self.sendCodeView.isHidden = false
            self.resendView.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @IBAction func femaleButton(_ sender: Any)
    {
        femaleView.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
        maleView.backgroundColor = UIColor(red: 222/255, green: 139/255, blue: 0, alpha: 1)
        femaleImage.image = UIImage(named:"woman2")
        manImage.image = UIImage(named:"man")
    }
    
    
    @IBAction func maleButton(_ sender: Any) {
        femaleView.backgroundColor = UIColor(red: 222/255, green: 139/255, blue: 0, alpha: 1)
        maleView.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
        femaleImage.image = UIImage(named:"woman")
        manImage.image = UIImage(named:"man2")
        //Gender = "m"
    }
    
    @IBAction private func signUpButton(_ sender: Any) {
        guard userPhone.text!.characters.count == 8 else {
            self.displayAlertMessage("", "Enter valid phone number")
            return
        }
        
        if usersTypeID == "" || userNameText.text == "" || userPhone.text == "" {
            self.displayAlertMessage("", "Please Fill All Fields")
        } else {
            guard let phoneNumber = userPhone.text else { return }
            signUpUser(Settings.lang, usersTypeID, userNameText.text!, phoneNumber)
        }
        
    }
    
    
    @IBAction func sigbInBttn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SignInPageViewController") as! SignInPageViewController
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    
    @IBAction func sendBtn(_ sender: UIButton) {
        guard let NumberStr = codeTxt.text else { return }
        let Formatter = NumberFormatter()
        Formatter.locale = Locale(identifier: "EN")
        let final = Formatter.number(from: NumberStr)?.intValue
        let sentCode = defaults.integer(forKey: SentCode)
        
        if final! == sentCode {
            check = "1"
            let userPhone = UserDefaults.standard.string(forKey: "userPhone")
            //print(userPhone!)
            sendActivationCode(userPhone!)
        } else {
            if Settings.lang == "ar" {
                let msg = "من فضلك ادخل الكود صحيح"
                self.displayAlertMessage("", msg)
            } else {
                let msg = "please check your code"
                self.displayAlertMessage("", msg)
                
            }
        }
        
    }
    
    @IBAction func resendBtn(_ sender: UIButton) {
        let userPhone = UserDefaults.standard.string(forKey: "userPhone")
        print(userPhone!)
        resendAgain(userPhone!)
    }
    
    @IBAction func codeText(_ sender: UITextField) {
        keyboardLefted = true
    }
    
    @IBAction func codeTextE(_ sender: UITextField) {
        keyboardLefted = false
    }
    
    
}
