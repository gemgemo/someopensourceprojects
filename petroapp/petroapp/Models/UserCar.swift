
import Foundation
import Gemo

class UserCar: Mapper<any> {
    var id, userId, type, prand, model, style, fuel, aLetters, aNumbers, eLetters, eNumbers, fName, userName, code, carPhoto, userPhoto, balance, carIcon, carTypeImage: string?
    
    override func mapping(mapper: Mapper<any>) {
        id = mapper["id"]?.string
        userId = mapper["user_id"]?.string
        type = mapper["type_car"]?.string
        prand = mapper["brand_car"]?.string
        model = mapper["model_car"]?.string
        style = mapper["year_car"]?.string
        fuel = mapper["fuel_type"]?.string
        aNumbers = mapper["num_ar"]?.string
        eNumbers = mapper["num_en"]?.string
        aLetters = mapper["ar_car"]?.string
        eLetters = mapper["en_car"]?.string
        fName = mapper["full_name"]?.string
        userName = mapper["username"]?.string
        code = mapper["code_car"]?.string
        carPhoto = mapper["photo_car"]?.string
        userPhoto = mapper["photo_user"]?.string
        balance = mapper["balance"]?.string
        carIcon = mapper["photo_b"]?.string
        carTypeImage = mapper["photo_n"]?.string
    }
    
    internal func save()-> void {
        let db = UserDefaults.standard
        db.set(id, forKey: Misc.CarId)
        db.set(userId, forKey: Misc.UserId)
        db.set(type, forKey: Misc.CarType)
        db.set(prand, forKey: Misc.CarPrand)
        db.set(model, forKey: Misc.CarModel)
        db.set(style, forKey: Misc.CarStyle)
        db.set(fuel, forKey: Misc.CarFuel)
        db.set(aLetters, forKey: Misc.ALetters)
        db.set(aNumbers, forKey: Misc.ANumbers)
        db.set(eLetters, forKey: Misc.ELetters)
        db.set(eNumbers, forKey: Misc.ENumbers)
        db.set(fName, forKey: Misc.FullName)
        db.set(userName, forKey: Misc.Username)
        db.set(code, forKey: Misc.CarCode)
        db.set(carPhoto, forKey: Misc.CarPhoto)
        db.set(userPhoto, forKey: Misc.Photo)
        db.set(balance, forKey: Misc.CarBalance)
        db.set(carIcon, forKey: Misc.CarIcon)
        db.set(carTypeImage, forKey: Misc.CarTypeImage)
        db.synchronize()
    }

    
    class func signIn(for user: User, _ page: BaseController?, _ completion: @escaping(UserCar) -> ()) -> void {
        guard Gemo.gem.isConnected else {
            page?.displayAlert(with: Localization.InternetConnection.localized())
            return
        }
        guard let username = user.username, !username.isEmpty else {
            page?.displayAlert(with: Localization.UserNameReqiured.localized())
            return
        }
        guard let password = user.password, !password.isEmpty else {
            page?.displayAlert(with: Localization.PasswordReqiured.localized())
            return
        }
        let paramters: Dictionary<string, any> = [
            Misc.MainKey: Misc.MainValue,
            Misc.LanguageKey: Localizer.instance.current.rawValue,
            "username": user.username ?? .empty,
            "password": user.password ?? .empty
        ]
        Gemo.gem.startSpinning()
        Http.request(link: Misc.getUrl(for: "login_car"), method: .post, parameters: paramters)
            .response { (response) in
                let error = response.error
                print("\(#function): \(response.jsonString ?? .empty)")
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("sign in error:  \(error!)")
                    page?.displayAlert(with: error?.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
                guard let json = response.result as? Dictionary<string, any>,
                    let success = json["result"] as? string else {
                        page?.displayAlert(with: Localization.NoData.localized())
                        return
                }
                if (success == "true") {
                    completion(UserCar(JSON: json))
                } else {
                    page?.displayAlert(with: json["data"] as? string)
                }

        }
        
    }
    
}

//id, userId, type, prand, model, style, fuel, aLetters, aNumbers, eLetters, eNumbers, fName, userName, code, carPhoto, userPhoto, balance
extension Misc {
    internal static var CarId = "user.car.id.pe",
    CarType = "user.car.type.pe",
    CarPrand = "user.car.prand.pe",
    CarModel = "user.car.model.pe",
    CarStyle = "user.car.style.pe",
    CarFuel = "user.car.fuel.pe",
    ALetters = "user.car.a.letters.pe",
    ANumbers = "user.car.a.numbers.pe",
    ELetters = "user.car.e.letter.pe",
    ENumbers = "user.car.e.numbers.pe",
    FullName = "user.car.full.name.pe",
    CarCode = "user.car.code.pe",
    CarPhoto = "user.car.photo.pe",
    CarBalance = "user.car.balance.pe",
    CarIcon = "user.car.icon.pe",
    CarTypeImage = "car.type.image.pe"
    
}




































