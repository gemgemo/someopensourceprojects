//
//  Teacher.swift
//  Q8School
//
//  Created by mac on 12/12/16.
//  Copyright © 2016 Atiaf. All rights reserved.
//

import Foundation
public class Teacher{
    var id: String
    var teacherName: String
    var teacherImg: String
    var subjectName: String
    var phase: String
    var detial: String
    var date: String
    
    
    init(id:String,teacherName:String,teacherImg:String,subjectName:String,phase:String,detial:String,date:String) {
        self.id = id
        self.teacherName = teacherName
        self.teacherImg = teacherImg
        self.subjectName = subjectName
        self.phase = phase
        self.detial = detial
        self.date = date
    }

}
