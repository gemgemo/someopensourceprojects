
import UIKit

final class MenuCell: BaseTableCell
{

    // MARK: - Constants
    
    // MARK: - Variables
    internal var item: Menu? {
        didSet {
            updateUi()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var icon: UIImageView!
    @IBOutlet private weak var lblTitle: UILabel!
    
    
    // MARK: - Overridden functions
    internal override func updateUi() {
        lblTitle.text = item?.title ?? string.empty
        icon.image = item?.icon
    }
    
    // MARK: - Actions
}



class BaseTableCell: UITableViewCell
{
    
    internal func updateUi()-> void {
        
    }
    
}









