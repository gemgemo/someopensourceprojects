//
//  Menu.swift
//  Q8School
//
//  Created by mac on 12/17/16.
//  Copyright © 2016 Atiaf. All rights reserved.
//

import UIKit

class Menu: UICollectionViewCell {
    
    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var menuLabel: UILabel!
}
