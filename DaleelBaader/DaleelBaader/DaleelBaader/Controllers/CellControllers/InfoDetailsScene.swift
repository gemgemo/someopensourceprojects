
import UIKit

import Localize_Swift


final class InfoDetailsScene: UITableViewCell
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    internal var item: Item? {
        didSet {
            updateUI()
        }
    }
    
    //internal var isSet = false
    internal var btnInstagram, btnTwitter: UIButton!, stackView: UIStackView?
    
    // MARK: - Outlets
    
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var icon: UIImageView!
    @IBOutlet private weak var container: UIView!
    
    // MARK: - Overridden functions

    internal override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Actions

    
    // MARK: - Functions
    
    private func updateUI()-> void {
        let iconName = item?.iconName ?? .empty
        let itemTitle = item?.title ?? .empty
        removeSomeView()
        //print( iconName.isEmpty && itemTitle.isEmpty)
        if ( iconName.isEmpty && itemTitle.isEmpty) {
            lblTitle.isHidden = true
            icon.isHidden = true
            setupEmptyRow()
        } else {
            lblTitle.isHidden = false
            icon.isHidden = false
            if let title = item?.title {
                lblTitle.text = title
            }
            icon.image = nil
            if let iconName = item?.iconName {
                icon.image = UIImage(named: iconName)
            }
        }
    }
  
    
    private func setupEmptyRow()-> void {
        stackView = UIStackView()
        stackView?.axis = .horizontal
        stackView?.alignment = .fill
        stackView?.distribution = .fillEqually
        stackView?.spacing = 30
        stackView?.translatesAutoresizingMaskIntoConstraints = false
        container.addSubview(stackView ?? UIView())
        
        // stack constraints
        
        stackView?.centerXAnchor.constraint(equalTo: container.centerXAnchor, constant: 0).isActive = true
        stackView?.centerYAnchor.constraint(equalTo: container.centerYAnchor, constant: 0).isActive = true
        stackView?.widthAnchor.constraint(equalToConstant: 80).isActive = true
        stackView?.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        btnInstagram = UIButton(type: .system)
        btnInstagram.setImage(#imageLiteral(resourceName: "instagram"), for: .normal)
        btnInstagram.tag = 0
        
        btnTwitter = UIButton(type: .system)
        btnTwitter.setImage(#imageLiteral(resourceName: "twitter"), for: .normal)
        btnTwitter.tag = 1
        
        stackView?.addArrangedSubview(btnInstagram)
        stackView?.addArrangedSubview(btnTwitter)
        
    }
    
    private func removeSomeView()-> void {
        stackView?.removeFromSuperview()
    }
    

}



















