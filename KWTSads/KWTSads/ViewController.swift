//
//  ViewController.swift
//  KWTSads
//
//  Created by Mostafa on 8/12/16.
//  Copyright © 2016 Mostafa. All rights reserved.
//

import UIKit
import SwiftSpinner



class ViewController: UIViewController ,UICollectionViewDelegate ,UICollectionViewDataSource{

    @IBOutlet var countryCollectionView: UICollectionView!
    var countryName =  [String]()
    var countryIDs = [String]()
    let defaults = UserDefaults.standard


    override func viewDidLoad() {
        super.viewDidLoad()
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
            let deviceID = UIDevice.current.identifierForVendor!.uuidString
            UserDefaults.standard.set(deviceID, forKey: "MyDeviceID")
            
            self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "ac_back-arrow")
            self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "ac_back-arrow")
            self.navigationController?.navigationBar.tintColor = UIColor(red: 0.96, green: 0.69, blue:  0.25, alpha: 1 )
            
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
            
            
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage
            
            countryCollectionView.delegate = self
            countryCollectionView.dataSource = self
            SwiftSpinner.show("Loading...")
            loadAreas()

        } else {
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage

            let err = "Internet connection FAILED"
            self.displayAlertMessage(err)
            
        }
    
        
        
    }
    func loadAreas()
    {
        let urlString = "\(baseUrl)cats.php?t=1&p_id=1&lang=0"
        let session = URLSession.shared
        let url = URL(string: urlString)!
        session.dataTask(with: url, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
                if let responseData = data {
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments)as! [String:AnyObject]
                        
                        let data = json["data"] as! NSArray
                        for i in 0..<data.count {
                            if let dict = data[i] as? NSDictionary {
                                
                                self.countryName.append(dict["cat_name"] as! String)
                                self.countryIDs.append(dict["cat_id"] as! String)
                            }
                            
                            
                        }
                        
                        DispatchQueue.main.async {
                            
                            
                            self.countryCollectionView.reloadData()
                            SwiftSpinner.hide()
                            
                            
                        }
                    }
                        
                    catch
                    {
                        print ("Could not Serialize")
                    }
                    
                    
                }
                
        }).resume()
        

    }
    
    
    func displayAlertMessage(_ userMessage:String)
    {
        let myAlert = UIAlertController(title: "Alert", message:userMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "countryCell", for: indexPath) as! CountryCollectionViewCell
        
        
        let bcolor : UIColor = UIColor( red: 0.2, green: 0.2, blue:0.2, alpha: 0.3 )
        
        cell.layer.borderColor = bcolor.cgColor
        cell.layer.borderWidth = 0.5
        cell.layer.cornerRadius = 3
        cell.layer.borderColor = UIColor.gray.cgColor
        
        cell.backgroundColor=UIColor.white
        cell.countryCellLabel.text = countryName[(indexPath as NSIndexPath).row]
        
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return countryName.count
    }
   
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "countrySegue" {
            let departmentViewController = segue.destination as! DepartmentsViewController
            
            // Get the cell that generated this segue.
            if let selectedCell = sender as? CountryCollectionViewCell {
                let indexPath = countryCollectionView.indexPath(for: selectedCell)!
                let selectedCountry = countryName[(indexPath as NSIndexPath).row]
                defaults.setValue(countryIDs[(indexPath as NSIndexPath).row], forKey: "CountryIDS")
                defaults.setValue(countryName[(indexPath as NSIndexPath).row], forKey: "countryId")
                departmentViewController.countryName = selectedCountry
            }
        }
        
        
        
        
        
    }


    }

   


