
import UIKit
import Localize_Swift

final class BoardOfTrustees: BaseController
{

    // MARK: - Constants
    
    //MARK: - Variables
    
    fileprivate var dataSource = Array<Person>() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let this = self else { return }
                this.tblMembers.reloadData()
            }
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var tblMembers: UITableView! {
        didSet {
            tblMembers.delegate = self
            tblMembers.dataSource = self
        }
    }
    @IBOutlet fileprivate weak var lblImageTitle: UILabel!
    /*@IBOutlet fileprivate weak var imageScrollView: UIScrollView! {
        didSet {
            imageScrollView.contentSize = graphImage.bounds.size
            imageScrollView.backgroundColor = .normal
            //imageScrollView.clipsToBounds = true
            imageScrollView.layer.cornerRadius = 5.0
            imageScrollView.layer.borderWidth = 0.8
            imageScrollView.layer.borderColor = Color(white: 0.80, alpha: 0.8).cgColor
        }
    }*/
    @IBOutlet fileprivate weak var graphImage: UIImageView!
    //@IBOutlet private weak var tableViewContainer: UIView!
    @IBOutlet fileprivate weak var tableFooterView: UIView!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        loadData()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBar()
        updateUi()
        /*tblMembers.layer.cornerRadius = 7.0
        tableViewContainer.layer.cornerRadius = 7.0
        tableViewContainer.clipsToBounds = true
        tableViewContainer.layer.masksToBounds = true
        tableViewContainer.setShadow(with: Size(right: 0, top: 1), radius: 1, opacity: 0.8, color: .gray)
        tableViewContainer.layer.masksToBounds = false*/
    }
    
    //MARK: - Actions

    internal override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        tableFooterView.frame.size.height = view.frame.size.height-30
    }
}


// MARK: - Helper function

extension BoardOfTrustees
{
    
    internal func updateNavigationBar()-> void {
        updateNavigationBarDirections()
        navigationItem.title = "AwardBoardofTrusteeslan".localized()
        navigationController?.navigationBar.castShadow = "set"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: getBackImage(), style: .plain, target: self, action: #selector(goBack(_:)))
    }
    
    fileprivate func configureTableView()-> void {
        tblMembers.isScrollEnabled = true
        tblMembers.register(Constants.Nib.Member, forCellReuseIdentifier: Constants.ReuseIdentifier.Member)
    }
    
    fileprivate func updateUi()-> void {
        lblTitle.text = "AwardBoardofTrusteessubtitlelan".localized()
        lblImageTitle.text = "Prizestructurelan".localized()
    }
    
    fileprivate func loadData()-> void {
        startSpin()
        Person.getData(by: Constants.BackendURLs.shared.BoardOfTrutees, viewController: self) { [weak self] (persons) -> void in
            guard let this = self else { return }
            this.dataSource = persons
            this.stopSpin()
        }
        getImage()
    }
    
    private func getImage()-> void {
        HttpClient.request(link: "http://sudyer.co/JSON/str_w.php?key=RAWDAT_SUDAIR", method: .get) { [weak self] (result, response, error) -> void in
            if (error != nil) {
                self?.showAlert(with: string.empty, and: Constants.Misc.ErrorMessage)
                return
            }
            
            if let obj = result as? [string: any] {
                if let imageLink = obj["image"] as? string {
                    self?.graphImage.loadImage(from: imageLink, onComplete: nil)
                }
            } else {
                print("nix image")
            }
        }
    }
    
}


// MARK: - Table view delegate & data source functions

extension BoardOfTrustees: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.Member) as? MemberCell {
            cell.person = dataSource[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
}



























