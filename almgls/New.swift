//
//  New.swift
//  almgls
//
//  Created by User on 12/29/16.
//  Copyright © 2016 Mostafa. All rights reserved.
//

import Foundation
public class New{
    
    
    var id: String
    var title: String
    var photo: String
    
    init(id: String, title: String, photo:String) {
        self.id = id
        self.title = title
        self.photo = photo
    }
}

