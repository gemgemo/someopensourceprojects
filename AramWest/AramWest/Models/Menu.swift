
import UIKit
import Localize_Swift

final class Menu
{
    
    internal var id: int?, title: string?, icon: UIImage?
    
    init(id: int, title: string, icon: UIImage) {
        self.id = id
        self.title = title
        self.icon = icon
    }
    
    internal class func getItems()-> [Menu] {
        var items = [Menu]()
        items.append(Menu(id: 1, title: "personalprofilelan".localized(), icon: #imageLiteral(resourceName: "menu_arrow")))
        items.append(Menu(id: 2, title: "orderslan".localized(), icon: #imageLiteral(resourceName: "menu_arrow")))
        items.append(Menu(id: 3, title: "neworderlan".localized(), icon: #imageLiteral(resourceName: "menu_arrow")))
        //items.append(Menu(id: 4, title: "notificationslan".localized(), icon: #imageLiteral(resourceName: "menu_arrow")))
        items.append(Menu(id: 5, title: "settingslan".localized(), icon: #imageLiteral(resourceName: "menu_arrow")))
        items.append(Menu(id: 6, title: "termsofuselan".localized(), icon: #imageLiteral(resourceName: "menu_arrow")))
        items.append(Menu(id: 7, title: "aboutapplan".localized(), icon: #imageLiteral(resourceName: "menu_arrow")))
        return items
    }
    
    

}

/*
 
  = "Profile";
  = "Orders";
  = "New Order";
  = "Notifications";
  = "Settings";
  = "Terms of use";
  = "About App";
 */
