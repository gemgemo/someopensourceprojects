
import Foundation

import Localize_Swift


final class MenuItem
{
    
    internal var id: int?, title: string?
    
    init(id: int, title: string) {
        self.id = id
        self.title = title
    }
    
    
    internal class func getItems()-> [MenuItem] {
        var items = Array<MenuItem>()
        items.append(MenuItem(id: 1, title: "sidemenumaintitle".localized()))
        items.append(MenuItem(id: 2, title: "sidemenuaboutapptitle".localized()))
        items.append(MenuItem(id: 3, title: "sidemenushareapptitle".localized()))
        items.append(MenuItem(id: 4, title:  "sidemenuchangelangtitle".localized()))
        return items
    }
    
    
}











