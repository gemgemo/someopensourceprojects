
import UIKit
import Gemo
import CoreLocation

class SignUp: BaseController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var isUpdate = false,
                 userLocation = CLLocation(),
                 locationManager = CLLocationManager()
    
    
    // MARK: - Outlets
    
  
    @IBOutlet fileprivate weak var txfStoreName: TextField! { didSet { txfStoreName.delegate = self } }
    @IBOutlet fileprivate weak var txfUsername: TextField! {
        didSet {
            txfUsername.delegate = self
        }
    }
    @IBOutlet fileprivate weak var txfSEmail: TextField! {
        didSet {
            txfSEmail.delegate = self
        }
    }
    @IBOutlet fileprivate weak var txfSPhoneNumber: TextField! {
        didSet {
            txfSEmail.delegate = self
        }
    }
    @IBOutlet fileprivate weak var txfSPassword: TextField! {
        didSet {
            txfSPassword.delegate = self
        }
    }
    @IBOutlet fileprivate weak var btnSSignUp: UIButton!
    //@IBOutlet fileprivate weak var txfReSPassword: TextField! { didSet { txfReSPassword.delegate = self } }
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        configureLocation()
        print("update mode = \(isUpdate)")
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBar.isHidden = true
        navigationController?.navigationBar.isHidden = false
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    
    // MARK: - Actions
    
    @IBAction private func continueRegistrationOnCLick(_ sender: UIButton) {
        if (Gemo.gem.isConnected) {
            guard !userName.isEmpty else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.UserNameReqiured.localized())
                return
            }
            guard !email.isEmpty || email.isEmail else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InvaildEmail.localized())
                return
            }
            guard !phoneNumber.isEmpty else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.PhoneNumberReqiured.localized())
                return
            }
            guard !password.isEmpty, !(password.characters.count < 6) else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.PasswordReqiured.localized())
                return
            }
            guard userLocation.coordinate.latitude != Number.zero.doubleValue || userLocation.coordinate.longitude != Number.zero.doubleValue else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InvalidLocation.localized())
                return
            }
            /*guard !rePassword.isEmpty else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.RePasswordReqiured.localized())
                return
            }*/
            /*guard password == rePassword else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.PasswordNotConfirmed.localized())
                return
            }*/
            if (isUpdate) {
                guard let storeInfoPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.StoreInfo) as? StoreInfo else { return }
                storeInfoPage.isUpdate = isUpdate
                storeInfoPage.password = password
                storeInfoPage.grocery = Grocery(id: .empty,
                                                name: storeName,
                                                password: password,
                                                email: email,
                                                phone: phoneNumber,
                                                username: userName,
                                                latitude: userLocation.coordinate.latitude.toString,
                                                longitude: userLocation.coordinate.longitude.toString)
                navigate(to: storeInfoPage)
            } else {
                validate { [weak self] in
                    guard let this = self else { return }
                    guard let storeInfoPage = this.storyboard?.instantiateViewController(withIdentifier: Storyboards.StoreInfo) as? StoreInfo else { return }
                    storeInfoPage.isUpdate = this.isUpdate
                    storeInfoPage.password = this.password
                    storeInfoPage.grocery = Grocery(id: .empty,
                                                    name: this.storeName,
                                                    password: this.password,
                                                    email: this.email,
                                                    phone: this.phoneNumber,
                                                    username: this.userName,
                                                    latitude: this.userLocation.coordinate.latitude.toString,
                                                    longitude: this.userLocation.coordinate.longitude.toString)
                    this.navigate(to: storeInfoPage)
                }

            }
            
            /*validate { [weak self] in
                guard let this = self else { return }
                guard let storeInfoPage = this.storyboard?.instantiateViewController(withIdentifier: Storyboards.StoreInfo) as? StoreInfo else { return }
                storeInfoPage.isUpdate = this.isUpdate
                storeInfoPage.password = this.password
                storeInfoPage.grocery = Grocery(id: .empty,
                                                name: this.storeName,
                                                password: this.password,
                                                email: this.email,
                                                phone: this.phoneNumber,
                                                username: this.userName,
                                                latitude: this.userLocation.coordinate.latitude.toString,
                                                longitude: this.userLocation.coordinate.longitude.toString)
                this.navigate(to: storeInfoPage)
            }*/
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized())
        }
    }
    
    
}


// MARK: - Helper functions

extension SignUp
{
    
    internal override func updateUi()-> void {
        super.updateUi()
        btnSSignUp.setCorner(radius: 20.0)
        btnSSignUp.setShadow(with: .zero, radius: 0.4, opacity: 0.9, color: #colorLiteral(red: 0.3008661568, green: 0.4334716499, blue: 0.5458672643, alpha: 1))
        if (Localizer.instance.current == .arabic) {
            txfSEmail.textAlignment = .right
            txfSPassword.textAlignment = .right
            txfUsername.textAlignment = .right
            txfSPhoneNumber.textAlignment = .right
            txfStoreName.textAlignment = .right
            //txfReSPassword.textAlignment = .right
        } else {
            txfSEmail.textAlignment = .left
            txfSPassword.textAlignment = .left
            txfUsername.textAlignment = .left
            txfSPhoneNumber.textAlignment = .left
            txfStoreName.textAlignment = .left
            //txfReSPassword.textAlignment = .left
        }
        txfStoreName.placeholder = Localization.StoreName.localized()
        txfSEmail.placeholder = Localization.Email.localized()
        txfSPassword.placeholder = Localization.Password.localized()
        txfUsername.placeholder = Localization.UserName.localized()
        txfSPhoneNumber.placeholder = Localization.PhoneNumber.localized()
        btnSSignUp.setTitle(Localization.Continuation.localized(), for: .normal)
        //txfReSPassword.placeholder = Localization.RePassword.localized()
        if (isUpdate) {
//            btnSSignUp.setTitle(Localization.EditProfile.localized(), for: .normal)
            navigationItem.title = Localization.UpdateProfile.localized()
            let db = UserDefaults.standard
            txfStoreName.text = db.string(forKey: Misc.GroceryName) ?? .empty
            txfUsername.text = db.string(forKey: Misc.GroceryUsername) ?? .empty
            txfSEmail.text = db.string(forKey: Misc.GroceryEmail) ?? .empty
            txfSPhoneNumber.text = db.string(forKey: Misc.GroceryPhone) ?? .empty
        } else {
            navigationItem.title = Localization.NewSignUp.localized()
        }
    }
    
    fileprivate func configureLocation()-> void {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        guard CLLocationManager.locationServicesEnabled() else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.OpenLocation.localized())
            return
        }
        locationManager.startUpdatingLocation()
        
    }
    
    fileprivate func validate(_ completion: @escaping()->())-> void {
        guard Gemo.gem.isConnected else {
            dialogUserWithNoInternetConnectionMessage()
            return
        }
        let paramters: [string: any] = [
            Misc.MainKey: Misc.MainValue,
            "matjer_name": storeName,
            "username": userName,
            "email": email,
            "phone": phoneNumber,
            "password": password,
            "password_again": password,
            "do_submit": "do_submit"
        ]
        print("validation paramters: \(paramters)")
        print("validation link: \(Urls.Register)")
        Gemo.gem.startSpinning()
        Http.request(link: Urls.Register, method: .post, parameters: paramters)
            .response { (response) in
                let error = response.error
                print("validate response: \(response.jsonString ?? .empty)")
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("validate error: \(string(describing: error)), error code: \((error as NSError?)?.code ?? 000)")
                    //3840
                    if let code = (error as NSError?)?.code, code == 3840 {
                        
                        Threads.main { completion() }
                    } else {
                        Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription)
                    }
                    return
                }
                Gemo.gem.stopSpinning()
                guard let json = (response.result as? [Dictionary<string, any>])?.first, let success = json["result"] as? string else {
                    return
                }
                if (success == "false") {
                    let aMessage = json["data"] as? string ?? .empty
                    let eMessage = json["data_en"] as? string ?? .empty
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: (Localizer.instance.current == .arabic) ? aMessage : eMessage)
                } else {
                    Threads.main { completion() }
                }
        }
    }
    
    
}



// MARK: - Computed properties

extension SignUp
{
    
    
    fileprivate var storeName: string {
        return txfStoreName.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var userName: string {
        return txfUsername.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var email: string {
        return txfSEmail.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var phoneNumber: string {
        return txfSPhoneNumber.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var password: string {
        return txfSPassword.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    /*fileprivate var rePassword: string {
        return txfReSPassword.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }*/
    
    
}



// MARK: - Text Field delegate functions

extension SignUp: UITextFieldDelegate
{
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}



// MARK: - Location

extension SignUp: CLLocationManagerDelegate
{
    
    internal func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("location error \(string(describing: error))")
        manager.stopUpdatingLocation()
    }
    
    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            userLocation = location
        }
        manager.stopUpdatingLocation()
    }
    
}

















































