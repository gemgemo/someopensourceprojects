
import Foundation
import Gemo



class ReportsRes: Mapper<any> {
    
    internal var reports = Array<Report>(), reportsTotalData: ReportsTotalData?
    
    override func mapping(mapper: Mapper<any>) {
        if let reportsData = mapper["item"]?.dictionaries {
            reports = reportsData.map { Report(JSON: $0) }
        }
        if let totalData = mapper["total"]?.dictionary {
            reportsTotalData = ReportsTotalData(JSON: totalData)
        }
    }
    
    
        
}

class Report: Mapper<any> {
    
    internal var carAName, carPrand, carId, carCode, carEName, fuel, fName, id, carModel, aNumbers, eNumbers, liters, address, latitude, longitude, name, carTypeIcon, carPhoto, carIcon, userPhoto, price, addedTime, carType, userId, carStyle: string?
    
    override func mapping(mapper: Mapper<any>) {
        carAName = mapper["ar_car"]?.string
        carPrand = mapper["brand_car"]?.string
        carId = mapper["car_id"]?.string
        carCode = mapper["code_car"]?.string
        fuel = mapper["en_car"]?.string
        carAName = mapper["fuel_type"]?.string
        fName = mapper["full_name"]?.string
        id = mapper["id"]?.string
        carModel = mapper["model_car"]?.string
        aNumbers = mapper["num_ar"]?.string
        eNumbers = mapper["num_en"]?.string
        liters = mapper["num_lit"]?.string
        address = mapper["petro_address"]?.string
        latitude = mapper["petro_lati"]?.string
        longitude = mapper["petro_longi"]?.string
        name = mapper["petro_name"]?.string
        carIcon = mapper["photo_b"]?.string
        carPhoto = mapper["photo_car"]?.string
        carTypeIcon = mapper["photo_n"]?.string
        userPhoto = mapper["photo_user"]?.string
        price = mapper["price"]?.string
        addedTime = mapper["time_add"]?.string
        carType = mapper["type_car"]?.string
        userId = mapper["user_id"]?.string
        carStyle = mapper["year_car"]?.string
        
    }
    
}

class ReportsTotalData: Mapper<any> {
    
    internal var liters, price: string?
    
    override func mapping(mapper: Mapper<any>) {
        liters = mapper["total_lit"]?.string
        price = mapper["total_price"]?.string
    }
}



























