
import Foundation


final class User: Mapper<Any>
{
    
    internal var uId, name, firstName, secondName, email, phone, avatar, details, latitude, longitude, gender: string?, success = false
    
    internal override var description: String {
        return "\n id: \(uId ?? .empty), name: \(name ?? .empty), userLocation: (latitude: \(latitude ?? .empty), longitude: \(longitude ?? .empty)) \n"
    }
    
    // MARK: - Overriden functions
    
    internal override func mapping(mapper: Mapper<Any>) {
        uId = mapper["user_id"]?.text
        name = mapper["username"]?.text
        firstName = mapper["first_name"]?.text
        secondName = mapper["second_name"]?.text
        email = mapper["email"]?.text
        phone = mapper["phone"]?.text
        avatar = mapper["photo_users"]?.text
        details = mapper["description"]?.text
        latitude = mapper["lati"]?.text
        longitude = mapper["longi"]?.text
        success = (mapper["result"]?.text ?? "false" == "true")
        gender = mapper["gender"]?.text
        
    }
    
    
    // MARK: - Functions
    
    
    
    
}





























