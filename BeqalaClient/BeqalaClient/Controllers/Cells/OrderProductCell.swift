
import UIKit


final class OrderProductCell: BaseTableCell
{
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var orderProduct: OrderProduct? { didSet { updateUi() } }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var mainPanel: UIView!
    @IBOutlet private weak var icon: UIImageView! { didSet { icon.rounded(withColor: Color(white: 0.88, alpha: 0.9), borderWidth: 1, cornerRadius: icon.frame.height/2)} }
    @IBOutlet private weak var labelsContainer: UIView!
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var lblQuantityTitle: UILabel!
    @IBOutlet private weak var lblQuantity: UILabel!
    @IBOutlet private weak var lblPriceTitle: UILabel!
    @IBOutlet private weak var lblPrice: UILabel!
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        lblTitle.text = orderProduct?.name ?? .empty
        lblPriceTitle.text = "\(Localization.Price.localized()): "
        lblPrice.text = "\(orderProduct?.price ?? .empty) \(Localization.SR.localized())"
        lblQuantityTitle.text = "\(Localization.Quantity.localized()): "
        lblQuantity.text = orderProduct?.quantity ?? .empty
        icon.loadImage(from: orderProduct?.photo ?? .empty)
        if (Localizer.instance.current == .arabic) {
            lblPrice.textAlignment = .right
            lblTitle.textAlignment = .right
            lblQuantity.textAlignment = .right
            lblPriceTitle.textAlignment = .right
            lblQuantityTitle.textAlignment = .right
            icon.rightAnchor.constraint(equalTo: mainPanel.rightAnchor, constant: -8).isActive = true
            labelsContainer.rightAnchor.constraint(equalTo: icon.leftAnchor, constant: -8).isActive = true
            labelsContainer.leftAnchor.constraint(equalTo: mainPanel.leftAnchor, constant: 8).isActive = true
            lblQuantityTitle.rightAnchor.constraint(equalTo: labelsContainer.rightAnchor, constant: 0).isActive = true
            lblQuantity.rightAnchor.constraint(equalTo: lblQuantityTitle.leftAnchor, constant: -8).isActive = true
            lblPrice.leftAnchor.constraint(equalTo: labelsContainer.leftAnchor, constant: 0).isActive = true
            lblPriceTitle.leftAnchor.constraint(equalTo: lblPrice.rightAnchor, constant: 8).isActive = true
            
        } else {
            lblPrice.textAlignment = .left
            lblTitle.textAlignment = .left
            lblQuantity.textAlignment = .left
            lblPriceTitle.textAlignment = .left
            lblQuantityTitle.textAlignment = .left
            icon.leftAnchor.constraint(equalTo: mainPanel.leftAnchor, constant: 8).isActive = true
            labelsContainer.leftAnchor.constraint(equalTo: icon.rightAnchor, constant: 8).isActive = true
            labelsContainer.rightAnchor.constraint(equalTo: mainPanel.rightAnchor, constant: -8).isActive = true
            lblQuantityTitle.leftAnchor.constraint(equalTo: labelsContainer.leftAnchor, constant: 0).isActive = true
            lblQuantity.leftAnchor.constraint(equalTo: lblQuantityTitle.rightAnchor, constant: 8).isActive = true
            lblPrice.rightAnchor.constraint(equalTo: labelsContainer.rightAnchor, constant: 0).isActive = true
            lblPriceTitle.rightAnchor.constraint(equalTo: lblPrice.leftAnchor, constant: -8).isActive = true
            
        }
    }
    
    
    // MARK: - Actions

    
}









































