
import UIKit
import Localize_Swift

final class AnnualReports: BaseController
{
    
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    fileprivate var dataSource = [AnnualReport]() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tblReports.reloadData()
            }
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var tblReports: UITableView! {
        didSet {
            tblReports.delegate = self
            tblReports.dataSource = self
        }
    }
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBar()
        loadData()
    }
    
    //MARK: - Actions
    
    
}



// MARK: - Helper functions

extension AnnualReports
{
    
    fileprivate func updateNavigationBar()-> void {
        updateNavigationBarDirections()
        navigationItem.title = "annualreportslan".localized()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: getBackImage(), style: .plain, target: self, action: #selector(goBack(_:)))
        navigationController?.navigationBar.castShadow = "set"
    }
    
    fileprivate func loadData()-> void {
        startSpin()
        AnnualReport.getData(from: Constants.BackendURLs.shared.AnnualReport, viewController: self) { [weak self] (reports) -> void in
            self?.dataSource = reports            
            self?.stopSpin()
        }
    }
    
}


// MARK: - Table view delegate & data source functions

extension AnnualReports: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.AnnualReport) as? AnnualReportCell {
            cell.report = dataSource[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        // open file
        if let viewerScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.PDFViewer) as? PdfViewer {
            viewerScreen.link = dataSource[indexPath.row].link
            present(UINavigationController(rootViewController: viewerScreen), animated: true, completion: nil)
        }
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}





























