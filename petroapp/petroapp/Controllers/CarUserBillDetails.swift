
import UIKit
import Gemo
import GoogleMaps

class CarUserBillDetails: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var bill: Bill?
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var lblProcessNo: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblLitersNo: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblPayment: UILabel!
    @IBOutlet weak var lblPaymentStatus: UILabel!
    @IBOutlet weak var mapView: GMSMapView! { didSet { mapView.delegate = self }}
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        super.updateUi()
        setupNavigationBar(isLogo: false, isMainPage: false, title: Localization.BillDetails.localized())
        lblProcessNo.text = "\(Localization.ProccessNo.localized())   \("??")"
        lblOrderDate.text = "\(Localization.OrderDate.localized())   \(bill?.timestamp ?? .empty)"
        lblLitersNo.text = "\(Localization.LitresNo.localized()) \(bill?.liters ?? .empty)"
        lblPrice.text = "\(Localization.Price.localized()) \(bill?.price ?? .empty) \(Localization.SR.localized())"
        lblPayment.text = "\(Localization.PaymentWay.localized())  \("??")"
        lblPaymentStatus.text = "??"
        if let latitude = bill?.latitude?.toDouble, let longitude = bill?.longitude?.toDouble {
            mapView.clear()
            let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            let marker = GMSMarker()
            marker.position = coordinate
            marker.icon = #imageLiteral(resourceName: "marker-map1")
            marker.appearAnimation = .pop
            marker.map = mapView
            mapView.camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: 10.0)
        }
        
    }
    
    
    // MARK: - Actions
    

}

// MARK: - map view delegate functions

extension CarUserBillDetails: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let markerView = UIView(frame: Rect(x: 0, y: 0, width: 220, height: 90))
        markerView.setCorner(radius: 4)
        markerView.setShadow(with: .zero, radius: 1.0, opacity: 0.4, color: .black)
        markerView.backgroundColor = .white
        markerView.clipsToBounds = true
        let icon = UIImageView(image: #imageLiteral(resourceName: "ic_gas_station"))
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.contentMode = .scaleAspectFit
        markerView.addSubview(icon)
        icon.topAnchor.constraint(equalTo: markerView.topAnchor, constant: 8).isActive = true
        icon.widthAnchor.constraint(equalToConstant: 40).isActive = true
        icon.heightAnchor.constraint(equalToConstant: 40).isActive = true
        icon.rightAnchor.constraint(equalTo: markerView.rightAnchor, constant: -8).isActive = true
        let lblName = UILabel()
        lblName.text = bill?.name ?? .empty
        lblName.translatesAutoresizingMaskIntoConstraints = false
        lblName.font = Font.notoKufi(of: 15)
        lblName.textColor = .black
        lblName.adjustsFontSizeToFitWidth = true
        lblName.textAlignment = .center
        markerView.addSubview(lblName)
        lblName.topAnchor.constraint(equalTo: icon.topAnchor, constant: 0).isActive = true
        lblName.rightAnchor.constraint(equalTo: icon.leftAnchor, constant: -8).isActive = true
        lblName.leftAnchor.constraint(equalTo: markerView.leftAnchor, constant: 8).isActive = true
        let lblAddress = UILabel()
        lblAddress.text = bill?.address ?? .empty
        lblAddress.translatesAutoresizingMaskIntoConstraints = false
        lblAddress.font = Font.notoKufi(of: 15)
        lblAddress.numberOfLines = 2
        lblAddress.textColor = .black
        lblAddress.textAlignment = .center
        lblAddress.adjustsFontSizeToFitWidth = true
        markerView.addSubview(lblAddress)
        lblAddress.rightAnchor.constraint(equalTo: markerView.rightAnchor, constant: -8).isActive = true
        lblAddress.bottomAnchor.constraint(equalTo: markerView.bottomAnchor, constant: 8).isActive = true
        lblAddress.leftAnchor.constraint(equalTo: markerView.leftAnchor, constant: 8).isActive = true
        lblAddress.topAnchor.constraint(equalTo: lblName.bottomAnchor, constant: 8).isActive = true
        return markerView
    }
    
}
























