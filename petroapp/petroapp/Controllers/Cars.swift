
import UIKit
import Gemo

internal var isShowReport = false
class Cars: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var dataSource = Array<Car>() { didSet { Threads.main { [weak self] in
            self?.tblcars.reloadData()
        } } }
    private var carCellDictionary: [Int: CarCell] = [:]
    internal weak var selectedCarsData: SelectedCarsData?
    
    // MARK: - Outlets
    
    @IBOutlet weak var tblcars: UITableView! { didSet {
        tblcars.delegate = self
        tblcars.dataSource = self
        }}
    @IBOutlet weak var noDataPanel: UIView! { didSet { noDataPanel.isHidden = true } }
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        prepareTableView()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isShowReport = false
        carCellDictionary.removeAll()
        loadData()
    }
    
    
    // MARK: - Actions
    
    func checkBoxClicked(_ sender: UIButton) {        
        guard let cell = tblcars.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as? CarCell else {
            return
        }
        cell.checkBox.isChecked = !cell.checkBox.isChecked
        if (cell.checkBox.isChecked) {
            carCellDictionary[sender.tag] = cell
            
        } else {
            if (carCellDictionary.values.contains(cell)) {
                carCellDictionary.removeValue(forKey: sender.tag)
            }
        }
        selectedCarsData?.selectedCars(carCellDictionary)
        isShowReport = !carCellDictionary.isEmpty        
        NotificationCenter.default.post(name: .OpenReportsMode, object: nil)
    }
       
    
}


// MARK: - Helper functions 

fileprivate extension Cars {
    
    fileprivate func prepareTableView()-> void {
        tblcars.alwaysBounceVertical = true
        tblcars.register(Nibs.Car, forCellReuseIdentifier: ReuseIdentifiers.Car)
    }
    
    fileprivate func loadData()-> void {
        guard Gemo.gem.isConnected else {
            displayAlert(with: Localization.InternetConnection.localized())
            return
        }
        Car.fetchCars(self) { [weak self] (cars) in
            if (cars.isEmpty) {
                Threads.main { [weak self] in
                    self?.noDataPanel.isHidden = false
                }
            } else {
                self?.dataSource = cars
                Threads.main { [weak self] in
                    NotificationCenter.default.post(name: .OpenReportsMode, object: nil)
                    self?.noDataPanel.isHidden = true
                }
            }
        }
    }
    
}


// MARK: - Table views delegate & data source functions

extension Cars: UITableViewDelegate, UITableViewDataSource {
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifiers.Car) as! CarCell
        cell.car = dataSource[indexPath.row]
        cell.btnCheckBox.tag = indexPath.row
        cell.btnCheckBox.addTarget(self, action: #selector(checkBoxClicked(_:)), for: .touchUpInside)
        return cell
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print("selected cell: \(indexPath)")
        guard let carDetailsPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.CarDetails) as? CarDetails else {
            return
        }
        carDetailsPage.car = dataSource[indexPath.row]
        navigate(to: carDetailsPage)
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140.0
    }
    
}




// MARK: - Cars objects

protocol SelectedCarsData: class {
    func selectedCars(_ carsObject: [Int: CarCell])-> void
}




















