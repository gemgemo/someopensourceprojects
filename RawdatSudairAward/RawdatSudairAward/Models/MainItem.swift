
import Foundation
import Localize_Swift

final class MainItem
{
    
    internal var id: int?, title: string?, icon: UIImage?
    
    init(id: int, title: string, icon: UIImage) {
        self.id = id
        self.title = title
        self.icon = icon
    }
    
    internal class func getItems()-> [MainItem] {
        var items = Array<MainItem>()
        items.append(MainItem(id: 2, title: "awardphotoslan".localized(), icon: #imageLiteral(resourceName: "landscape")))
        items.append(MainItem(id: 1, title: "awardlistlan".localized(), icon: #imageLiteral(resourceName: "clipboard copy")))
        items.append(MainItem(id: 4, title: "Statisticsandnumberslan".localized(), icon: #imageLiteral(resourceName: "bar-chart")))
        items.append(MainItem(id: 3, title: "videogallerylan".localized(), icon: #imageLiteral(resourceName: "video-player")))
        items.append(MainItem(id: 6, title: "boardofawardmanagementlan".localized(), icon: #imageLiteral(resourceName: "teamwork")))
        items.append(MainItem(id: 5, title: "AwardBoardofTrusteeslan".localized(), icon: #imageLiteral(resourceName: "teamwork 2")))
        items.append(MainItem(id: 8, title: "annualreportslan".localized(), icon: #imageLiteral(resourceName: "analytics")))
        items.append(MainItem(id: 7, title: "Barcodecompetitionlan".localized(), icon: #imageLiteral(resourceName: "badge")))        
        return items
    }
    
}
