
import UIKit
import Localize_Swift

final class Statistics: BaseController
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    fileprivate var dataSource = [StatisticsNumbers]() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tblStatistics.reloadData()
            }
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var tblStatistics: UITableView! {
        didSet {
            tblStatistics.delegate = self
            tblStatistics.dataSource = self
        }
    }
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()        
        configureTableView()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBar()
        dataSource = StatisticsNumbers.setItems()
    }
    
    //MARK: - Actions
    
}



// MARK: - Helper functions

extension Statistics
{
    
    fileprivate func updateNavigationBar()-> void {
        navigationItem.title = "Statisticsandnumberslan".localized()
        updateNavigationBarDirections()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: getBackImage(), style: .plain, target: self, action: #selector(goBack(_:)))
        navigationController?.navigationBar.castShadow = "set"
    }
    
    fileprivate func configureTableView()-> void {
        tblStatistics.register(Constants.Nib.Statistics, forCellReuseIdentifier: Constants.ReuseIdentifier.Statistics)
    }
    
   
    
}



// MARK: - Table view delegate & data source functions

extension Statistics: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.Statistics) as? StatisticsCell {
            cell.statistics = dataSource[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        if let detailsScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.StatisticsDetails) as? StatisticsDetails {
            detailsScreen.statistics = dataSource[indexPath.row]
            navigationController?.pushViewController(detailsScreen, animated: true)
        }
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
}
























