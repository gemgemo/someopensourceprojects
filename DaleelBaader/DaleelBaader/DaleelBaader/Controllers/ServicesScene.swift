
import UIKit

import Localize_Swift
import MessageUI

final class ServicesScene: BaseController
{
    
    // MARK: - Constants
    
    fileprivate let urlSession = URLSession.shared
    
    //MARK: - Variables
    
    fileprivate var titlesDataSource = Array<Service>() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let this = self else { return }
                this.tblTitles.reloadData()
            }
        }
    }
    fileprivate var detailsDataSource = Array<Item>() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tblDetails.reloadData()
            }
        }
    }
    fileprivate var selectedService: Service?, isTranslation = false, isMen = false, isWomen = false
    internal var dataSourceLink, navTitle: string?
    
    // MARK: - Outlets
    @IBOutlet fileprivate weak var tblTitles: UITableView! {
        didSet {
            tblTitles.delegate = self
            tblTitles.dataSource = self
        }
    }
    @IBOutlet fileprivate weak var tblDetails: UITableView! {
        didSet {
            tblDetails.delegate = self
            tblDetails.dataSource = self
        }
    }
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        if(AppManager.shared.isConnected) {
            if (dataSourceLink == "4") {
                //scout services
                loadScoutData(index: Number.zero.intValue)
            } else {
                loadData()
            }
        } else {
            dialog(with: Constants.Misc.InternetMessage)
        }
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBar()
        configureTables()
    }
    
    internal override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK: - Actions
    
    @objc fileprivate func socialNetworkClicked(_ sender: UIButton) {
        switch (sender.tag) {
        case 0:
            guard let link = selectedService?.instagramLink else {
                dialog(with: "ErrorMessage".localized())
                print("instgram link error")
                return
            }
            UIApplication.open(this: link)
            
        case 1:
            guard let link = selectedService?.twitterLink else {
                dialog(with: "ErrorMessage".localized())
                print("twitter link error")
                return
            }
            UIApplication.open(this: link)
            
        default:
            break
        }
    }

}


// MARK: - Helper functions 

extension ServicesScene
{
    
    fileprivate func updateNavigationBar()-> void {
        navigationItem.title = navTitle?.localized()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(goBack(_:)))
    }
    
    fileprivate func configureTables()-> void {
        tblTitles.register(Constants.Nib.InfoTitles, forCellReuseIdentifier: Constants.ReuseIdentifier.InfoTitles)
        tblDetails.register(Constants.Nib.InfoDetails, forCellReuseIdentifier: Constants.ReuseIdentifier.InfoDetails)
    }
    
    fileprivate func loadData()-> void {
        startSpin()
        guard let link = dataSourceLink else {
            dialog(with: "ErrorMessage".localized())
            print("empty llink")
            stopSpin()
            return
        }
        Service.getData(by: link, viewController: self) { [weak self] (services) -> void in
            guard let this = self else { return }
            this.titlesDataSource = services
            if (!services.isEmpty) {
                this.select(this: services.first, at: Number.zero.intValue)
            } else {
                this.dialog(with: "ErrorMessage".localized())
            }
            UIApplication.showIndicator(by: false)
            this.stopSpin()
        }
    }
    
    fileprivate func loadScoutData(index: int)-> void {
        startSpin()
        titlesDataSource.removeAll()
        titlesDataSource.append(Service(id: "1", title: "Baadercenterforvolunteerworktitle".localized(), phone: string.empty, latitude: string.empty, longitude: string.empty, link: string.empty, email: string.empty, instagram: string.empty, twitter: string.empty))
        titlesDataSource.append(Service(id: "2", title: "Guidancedrifterstitle".localized(), phone: string.empty, latitude: string.empty, longitude: string.empty, link: string.empty, email: string.empty, instagram: string.empty, twitter: string.empty))
        titlesDataSource.append(Service(id: "3", title: "Translationtitle".localized(), phone: string.empty, latitude: string.empty, longitude: string.empty, link: string.empty, email: string.empty, instagram: string.empty, twitter: string.empty))
        titlesDataSource.append(Service(id: "4", title: "Mentoiletstitle".localized(), phone: string.empty, latitude: string.empty, longitude: string.empty, link: string.empty, email: string.empty, instagram: string.empty, twitter: string.empty))
        titlesDataSource.append(Service(id: "5", title: "Womensbathroomtitle".localized(), phone: string.empty, latitude: string.empty, longitude: string.empty, link: string.empty, email: string.empty, instagram: string.empty, twitter: string.empty))
        select(this: titlesDataSource[index], at: index)
        var link = string.empty
        switch(index) {
        case 0:
            link = (Localize.currentLanguage() == Constants.Misc.ArabicSign) ? Constants.BackendURLs.BaaderCenterAR : Constants.BackendURLs.BaaderCenterEN
        case 1:
            link = (Localize.currentLanguage() == Constants.Misc.ArabicSign) ? Constants.BackendURLs.ErshaadAR : Constants.BackendURLs.ErshaadEN
        default:
            break
        }
        print("selected index is \(index)")
        getSelectedRowData(by: link) { [weak self] (service) -> void in
            self?.selectedService = service
            self?.stopSpin()
        }
    }
    
    private func select(this row: Service?, at index: int)-> void {
        DispatchQueue.main.async { [weak self] in
            guard let this = self else { return }
            this.tblTitles.selectRow(at: IndexPath(row: index, section: 0), animated: false, scrollPosition: .top)
            this.selectedService = row
            this.detailsDataSource = Item.getStaticData()
            this.detailsDataSource.append(Item(title: string.empty, iconName: string.empty))
        }
    }
    
    fileprivate func getSelectedRowData(by link: string, onComplete: @escaping (Service)->void)-> void {
        UIApplication.showIndicator(by: true)
        
        if (link.isEmpty || !link.isLink) {
            dialog(with: "ErrorMessage".localized())
            print("link error")
            UIApplication.showIndicator(by: false)
            return
        }
        guard let urlRequest = URL(string: link) else {
            dialog(with: "ErrorMessage".localized())
            print("url request error")
            UIApplication.showIndicator(by: false)
            return
        }
        
        urlSession.dataTask(with: urlRequest) { [weak self] (data, response, error) in
            guard let this = self else { return }
            if (error != nil) {
                this.dialog(with: "ErrorMessage".localized())
                print("response error")
                UIApplication.showIndicator(by: false)
                return
            }
            
            guard let jsonData = data else {
                this.dialog(with: "ErrorMessage".localized())
                print("none data error")
                UIApplication.showIndicator(by: false)
                return
            }
            
            do {
                if let jsonString = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [[string: any]] {
                    let service = Service(dictionary: jsonString.first ?? ["" : ""])
                    onComplete(service)
                }
                UIApplication.showIndicator(by: false)
            } catch {
                this.handle(this: error)
                print("none data error")
                UIApplication.showIndicator(by: false)
            }
            
        }.resume()
        
    }
    
    fileprivate func openMap(with title:string, latitude: string, longitude: string)-> void {
        openMap(with: title, and: makeLocationCoordinate(latitude: latitude, longitude: longitude))
        
    }
    
}


// MARK: - Table view delegate & data source functions

extension ServicesScene: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (tableView == tblTitles) ? titlesDataSource.count : detailsDataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (tableView) {
        case tblTitles:
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.InfoTitles) as? InfoTitlesScene {
                cell.service = titlesDataSource[indexPath.row]
                return cell
            }
        case tblDetails:
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.InfoDetails) as? InfoDetailsScene {
                //print(detailsDataSource.count)
                cell.item = detailsDataSource[indexPath.row]                
                if (cell.btnTwitter != nil && cell.btnInstagram != nil) {
                    cell.btnInstagram.addTarget(self, action: #selector(socialNetworkClicked(_:)), for: .touchUpInside)
                    cell.btnTwitter.addTarget(self, action: #selector(socialNetworkClicked(_:)), for: .touchUpInside)
                }
                return cell
            }
        default:
            return UITableViewCell()
        }
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (tableView) {
        case tblTitles:
            isTranslation = false
            isMen = false
            isWomen = false
            selectedService = titlesDataSource[indexPath.row]
            
            if (dataSourceLink == "4" ) {
                print("slected index path \(indexPath.row)")
                switch (indexPath.row) {
                case 0,1:
                    if (detailsDataSource.count == 4) {
                        detailsDataSource.append(Item(title: string.empty, iconName: string.empty))
                    }
                default:
                    if (detailsDataSource.count == 5) {
                        detailsDataSource.removeLast()
                    }
                }
                
                switch (indexPath.row) {
                case 0, 1:
                    loadScoutData(index: indexPath.row)
                    
                case 2: // translation
                    detailsDataSource.removeAll()
                    isTranslation = true
                    detailsDataSource.append(Item(id: string.empty, title: "translationtitlefirstrow".localized(), iconName: string.empty, link: Constants.Misc.TranslatedAppLink, latitude: string.empty, longitude: string.empty))
                    Item.fetchTranslationData(viewController: self) { [weak self] (items) -> void in
                        guard let this = self else { return }
                        this.detailsDataSource += items
                        this.isTranslation = true
                        UIApplication.showIndicator(by: false)
                    }
                
                case 3:// men
                        detailsDataSource.removeAll()
                        isMen = true
                        let link = (Localize.currentLanguage() == Constants.Misc.ArabicSign) ? Constants.BackendURLs.MenAR : Constants.BackendURLs.MenEN
                        Item.fetchToilets(by: link, viewController: self) { [weak self] (items) -> void in
                            guard let this = self else { return }
                            this.detailsDataSource = items
                            UIApplication.showIndicator(by: false)
                            this.isMen = true
                        }
                    
                case 4: // women
                        detailsDataSource.removeAll()
                        isWomen = true
                        let link  = (Localize.currentLanguage() == Constants.Misc.ArabicSign) ? Constants.BackendURLs.WomenAR : Constants.BackendURLs.WomenEN
                        Item.fetchToilets(by: link, viewController: self) { [weak self] (items) -> void in
                            guard let this = self else { return }
                            this.detailsDataSource = items
                            UIApplication.showIndicator(by: false)
                            this.isWomen = true
                        }
                    
                default:
                    break
                }
            }
            
            // end od title label
        case tblDetails:
            if (isTranslation) {
                if let link = detailsDataSource[indexPath.row].link {
                    UIApplication.open(this: link)
                }
            } else if (isMen || isWomen) {
                // goto map screen
                let selectedInfoItem = detailsDataSource[indexPath.row]
                openMap(with: selectedInfoItem.title ?? string.empty, latitude: selectedInfoItem.latitude ?? string.empty, longitude: selectedInfoItem.title ?? string.empty)
                
            } else {
                switch (indexPath.row) {
                    
                case 0:
                    print("open call")
                    if let number = selectedService?.phone {
                        print("phone number is : \(number)")
                        UIApplication.open(this: "tel://\(number)")
                    }
                    
                case 1:
                    print("open map")
                    openMap(with: selectedService?.latitude ?? string.empty, latitude: selectedService?.longitude ?? string.empty, longitude: selectedService?.title ?? string.empty)
                    
                case 2:
                    print("e-site")
                    if let link = selectedService?.link {
                        UIApplication.open(this: link)
                    }
                    
                case 3:
                    print("e-mail")
                    if (MFMailComposeViewController.canSendMail()) {
                        let mail = MFMailComposeViewController()
                        mail.mailComposeDelegate = self
                        mail.setToRecipients([titlesDataSource[indexPath.row].email ?? string.empty])
                        present(mail, animated: true, completion: nil)
                    } else {
                        print("can't open mail composer")
                    }
                    
                default :
                    break
                }
            }
            
        default:
            break
        }
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
}


// MARK: - Mail view controller delegate functions 

extension ServicesScene: MFMailComposeViewControllerDelegate
{
    
    internal func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
}


































