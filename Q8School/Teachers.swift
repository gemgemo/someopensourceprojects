//
//  Teachers.swift
//  Q8School
//
//  Created by mac on 12/12/16.
//  Copyright © 2016 Atiaf. All rights reserved.
//

import UIKit

class Teachers: UICollectionViewCell {
    
    @IBOutlet weak var teacherImg: UIImageView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var subjectLbl: UILabel!
    @IBOutlet weak var phaseName: UILabel!
    @IBOutlet weak var teacherName: UILabel!
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var teacherLbl: UILabel!
    @IBOutlet weak var detialsLbl: UILabel!
    @IBOutlet weak var teacherView: UIView!
    var id = ""

}
