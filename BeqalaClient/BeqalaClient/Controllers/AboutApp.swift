
import UIKit

final class AboutApp: BaseController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    
    // MARK: - Outlets
    
    
    @IBOutlet private weak var titlePanal: UIView! {
        didSet {
            shadow(for: titlePanal)
        }
    }
    @IBOutlet fileprivate weak var lblHeaderTitle: UILabel!
    @IBOutlet fileprivate weak var cover: UIImageView!
    @IBOutlet fileprivate weak var txtInfo: UITextView! {
        didSet {
            txtInfo.text = .empty
            txtInfo.alwaysBounceVertical = true
        }
    }
    @IBOutlet fileprivate weak var coverHeight: NSLayoutConstraint!
    @IBOutlet fileprivate weak var btnBack: UIButton!
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        coverHeight.constant = view.bounds.height*0.25
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBar.isHidden = true
        lblHeaderTitle.text = Localization.AboutApp.localized()
        btnBack.transform = CGAffineTransform.identity
        if (Localizer.instance.current == .arabic) {
            btnBack.leftAnchor.constraint(equalTo: titlePanal.leftAnchor, constant: 8).isActive = true
        } else {
            btnBack.rightAnchor.constraint(equalTo: titlePanal.rightAnchor, constant: -8).isActive = true
            btnBack.transform = btnBack.transform.rotated(by: cgFloat.pi)
        }
        getData()
    }
    
    // MARK: - Actions
    
    @IBAction private func backOnClick(_ sender: UIButton) {
        back()
    }
    

}



// MARK: - Helper functions

extension AboutApp
{
    
    fileprivate func getData()-> void {
        Gemo.gem.startSpinning(in: view)
        let parameters: [string: any] = [
            Constants.Misc.MainKey: Constants.Misc.MainValue,
            "lang": Localizer.instance.current.rawValue
        ]
        HttpClient.request(link: Constants.Urls.AboutApp, method: .post, parameters: parameters) { [weak self] (data, response, error) -> void in
            guard let this = self else { return }
            if (error != nil) {
                Gemo.gem.stopSpinning()
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.Error.localized(), in: this.view)
                print("get app information error \(string(describing: error))")
                return
            }
            Gemo.gem.stopSpinning()
            if let value = (data as? [[string: any]])?.first, let content = value["content"] as? string {
                DispatchQueue.main.async { [weak self] in
                    self?.txtInfo.text = content
                    self?.txtInfo.textAlignment = (Localizer.instance.current == .arabic) ? .right : .left
                }
            } else {
                print("get about app information nix data")
            }
        }
    }
}




















