
import UIKit
import Localize_Swift

final class Popup: BaseController
{
    
    // MARK: - Constants
    
    // MARK: - Variables
    
    internal var dataSource = Array<PopoverItem>()
    internal var onSelected: (PopoverItem, IndexPath)-> void = {_, _ in}
    
    // MARK: - Outlets
    
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.superview?.layer.cornerRadius = 3.0
        view.superview?.clipsToBounds = true
        view.superview?.setShadow(with: .zero, radius: 2.0, opacity: 0.8, color: .black)
        
        tableView.reloadData()
    }
    
    // MARK: - Actions
    
    
}


// MARK: - Popup cell

final class PopupCell: BaseTableCell
{
    
    // MARK: - Variables
    
    internal var item: PopoverItem? {
        didSet{
            updateUi()
        }
    }
    
    // MARK: - Outlets
    @IBOutlet private weak var icon: UIImageView!
    @IBOutlet private weak var lblTitle: UILabel!
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        lblTitle.text = ((Localize.currentLanguage() == Constants.Language.Arabic) ? item?.arabicName : item?.englishName) ?? .empty
        if let imageLink = item?.icon {
            icon.loadImage(from: imageLink, onComplete: nil)
        }
    }
    
}



// MARK: - Table view delegate & data source functions

extension Popup: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.Popover) as? PopupCell {
            cell.item = dataSource[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print("selected row is: ", indexPath.row)
        onSelected(dataSource[indexPath.row], indexPath)
        dismiss(animated: true, completion: nil)
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35.0
    }
    
}
































