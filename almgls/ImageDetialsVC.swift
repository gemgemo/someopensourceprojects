
import UIKit

class ImageDetialsVC: UIViewController,
    UICollectionViewDelegate,
    UICollectionViewDataSource
{
    
    
    internal var thumbnails = Array<Photo>()
    private var selectedImage: String!
    
    internal var album: Album?

    @IBOutlet private weak var detialImageCollection: UICollectionView! {
        didSet {
            detialImageCollection.delegate = self
            detialImageCollection.dataSource = self
        }
    }
    @IBOutlet private weak var detialImage: UIImageView!
    @IBOutlet private weak var loadingView: UIView!
    @IBOutlet private weak var shadowView: UIView!
    @IBOutlet private weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet private weak var loadingLbl: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        loadingView.isHidden = true
        shadowView.isHidden = true
        self.title = "Photos"
        if Settings.lang == "ar" {
            self.title = "الصور"
            loadingLbl.text = "جاري التحميل..."
        }
    }
 
   
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (Settings.isConnected) {
            loadPhotos()
        } else {
            dialogUser(with: "لا يوجد انترنت")
        }
    }
    
    private func loadPhotos()-> Void {
        if let images = album?.pictures {
            thumbnails = images
            detialImageCollection.reloadData()
            selectedImage =  images.first?.image ?? ""
            detialImage.loadImage(from: selectedImage, onComplete: nil)
        } else {
            print("nix pics")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "detialCell", for: indexPath)
            as? ImageDetial {
            cell.imageLink = thumbnails[indexPath.row].image
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return thumbnails.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        let size = self.detialImageCollection.frame.width
        
        return CGSize(width: size/3, height: 90)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedImage = thumbnails[indexPath.row].image
        /*if let imageName = selectedImage.components(separatedBy: "/").last {
            detialImage.loadImage(from: "http://hmmc.gov.sa/assets/uploads/\(imageName)", onComplete: nil)
        }*/
        detialImage.loadImage(from: selectedImage, onComplete: nil)
    }
    
    
    @IBAction func shareBtn(_ sender: UIBarButtonItem) {
        let tst = detialImage.image
        // set up activity view controller
        let objectsToShare = [tst as Any]
        let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)

    }

    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        self.navigationController!.popViewController(animated: true)
    }
   

}
