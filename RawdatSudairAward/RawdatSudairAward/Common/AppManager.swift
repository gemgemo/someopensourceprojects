
import UIKit


final class AppManager: NSObject
{

    
    // MARK: - Functions
    
    internal class func setupNavigationBar()-> void {
        let nav = UINavigationBar.appearance()
        nav.setBackgroundImage(UIImage(), for: .default)
        nav.shadowImage = UIImage()
        nav.barTintColor = .normal
        nav.tintColor = .fixed
        nav.isTranslucent = false
        nav.backgroundColor = .normal
        nav.castShadow = "set"
        /*if let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView {
         statusBar.backgroundColor = .normal
         }*/
        UIApplication.shared.isStatusBarHidden = false
        
    }
    
    
}
