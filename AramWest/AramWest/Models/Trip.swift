
import UIKit
import Alamofire

final class Trip: Mapper
{
    
    internal var id, sourceLatitude, sourceLongitude, destinationLatitude, destinationLongitude, timestamp, clientId, type, payType, requestId, rate, salary, driverAvatar, driverName: string?
    
    internal override var description: String {
        return "\n id:\(id), driver name: \(driverName) \n"
    }
    
    internal override func mapping(mapper: Mapper) {
        id = mapper["ID"]?.text
        sourceLatitude = mapper["Slat"]?.text
        sourceLongitude = mapper["Slan"]?.text
        destinationLatitude = mapper["Elat"]?.text
        destinationLongitude = mapper["Elan"]?.text
        timestamp = mapper["datetime"]?.text
        clientId = mapper["clientID"]?.text
        type = mapper["type"]?.text
        payType = mapper["paytype"]?.text
        requestId = mapper["RequestID"]?.text
        rate = mapper["Rate"]?.text
        salary = mapper["Salary"]?.text
        driverAvatar = mapper["DriverImage"]?.text
        driverName = mapper["DriverName"]?.text
        
    }
    
    // MARK: - Functions
    
    
    internal class func getData(by id: string, page: BaseController?, _ onCompelete: @escaping([Trip])->())-> void {
        page?.startSpin()
        Alamofire.request(Constants.BackendURLs.GetCallEnded, method: .post, parameters: ["ClientID": id])
            .responseJSON { (response) in
                let result = response.result
                if (result.error != nil) {
                    page?.stopSpin()
                    page?.showAlert(with: .empty, and: Constants.Localizer.Error.localized())
                    print("get ended called error \(result.error)")
                    return
                }
                if let value = result.value as? Dictionary<string, any>, let data = value["Data"] as? [Dictionary<string, any>], let success = value["Success"] as? string {
                    if (success == "True") {
                        onCompelete(data.map { Trip(JSON: $0) })
                    } else {
                        page?.stopSpin()
                        page?.showAlert(with: .empty, and: Constants.Localizer.NoData.localized())
                        print("success false")
                    }
                } else {
                    page?.stopSpin()
                    page?.showAlert(with: .empty, and: Constants.Localizer.NoData.localized())
                    print("null called data")
                }
        }
    }

}





























