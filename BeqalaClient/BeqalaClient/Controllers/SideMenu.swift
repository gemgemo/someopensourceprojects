
import UIKit
import KYDrawerController

internal var selectedItemIndex = -1
final class SideMenu: BaseController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var items = Array<MenuItem>() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tblItems.reloadData()
                self?.tblItems.selectRow(at: IndexPath(row: selectedItemIndex, section: 0), animated: true, scrollPosition: .middle)
            }
        }
    }
    
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var tblItems: UITableView! {
        didSet {
            tblItems.delegate = self
            tblItems.dataSource = self
        }
    }
    @IBOutlet fileprivate weak var headerView: UIView!
    @IBOutlet fileprivate weak var userAvatar: UIImageView!
    @IBOutlet fileprivate weak var lblName: UILabel! {
        didSet {
            lblName.text = .empty
        }
    }
    @IBOutlet fileprivate weak var lblUserPhone: UILabel! {
        didSet {
            lblUserPhone.text = .empty
        }
    }
    @IBOutlet fileprivate weak var avatarHeight: NSLayoutConstraint!
    @IBOutlet fileprivate weak var avatarWidth: NSLayoutConstraint!
    @IBOutlet fileprivate weak var titleLabelsPanel: UIView!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        selectedItemIndex = 4
        tblItems.showsVerticalScrollIndicator = false
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBar.isHidden = true
        let fullName = "\(UserDefaults.standard.value(forKey: Constants.Misc.FirstName) as? string ?? .empty) \(UserDefaults.standard.value(forKey: Constants.Misc.LastName) as? string ?? .empty)"
        lblName.text = fullName
        lblUserPhone.text = UserDefaults.standard.value(forKey: Constants.Misc.Phone) as? string ?? .empty
        userAvatar.image = ((UserDefaults.standard.value(forKey: Constants.Misc.Gender) as? string ?? "1") == "1") ? #imageLiteral(resourceName: "manLogo") : #imageLiteral(resourceName: "womenLogo")
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadItems()
        updateUi()
        self.tblItems.selectRow(at: IndexPath(row: selectedItemIndex, section: 0), animated: true, scrollPosition: .none)
    }
    
    internal override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        headerView.frame.size.height = view.bounds.height*0.28
        let dimention = headerView.frame.width*0.30
        avatarWidth.constant = dimention
        avatarHeight.constant = dimention
        userAvatar.rounded(cornerRadius: dimention/2)
    }
    
    
    // MARK: - Actions

    
}



// MARK: - Helper functions

extension SideMenu
{
    
    fileprivate func updateUi()-> void {
        if (Localizer.instance.current == .arabic) {
            userAvatar.rightAnchor.constraint(equalTo: headerView.rightAnchor, constant: -8.0).isActive = true
            titleLabelsPanel.rightAnchor.constraint(equalTo: userAvatar.leftAnchor, constant: -8.0).isActive = true
            titleLabelsPanel.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 8.0).isActive = true
            lblName.textAlignment = .right
            lblUserPhone.textAlignment = .right
        } else {
            userAvatar.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 8.0).isActive = true
            titleLabelsPanel.rightAnchor.constraint(equalTo: headerView.rightAnchor, constant: -8.0).isActive = true
            titleLabelsPanel.leftAnchor.constraint(equalTo: userAvatar.rightAnchor, constant: 8.0).isActive = true
            lblName.textAlignment = .left
            lblUserPhone.textAlignment = .left
        }
    }
    
    fileprivate func loadItems()-> void {
        MenuItem.items { [weak self] (items) in
            self?.items = items
        }
    }
    
    fileprivate func navigate(to vc: BaseController, with indexPath: IndexPath)-> void {
        guard let drawer = parent as? KYDrawerController else { return }
        print("last selected menu item: \(selectedItemIndex.toString)")
        if (selectedItemIndex != indexPath.row) {
            (drawer.mainViewController as? UINavigationController)?.pushViewController(vc, animated: false)
        }
    }
    
    fileprivate func configureTableView()-> void {
        tblItems.register(Constants.Nibs.Menu, forCellReuseIdentifier: Constants.ReuseIdentifiers.SideMenu)
    }
    
}







// MARK: - Table view delegate & data source functions

extension SideMenu: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifiers.SideMenu) as? SideMenuCell {
            cell.item = items[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print(indexPath.row)
        if let drawer = parent as? KYDrawerController {
            drawer.setDrawerState(.closed, animated: true)
        }
        //print("item id is: \(items[indexPath.row].id)")
        switch (items[indexPath.row].id) {
        case 1: // open profile
            if let profileScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.Profile) as? Profile {
                navigate(to: profileScreen, with: indexPath)
            }
            
        case 2: // open sections page
            if let departmentsScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.Deparments) as? Departments {
                navigate(to: departmentsScreen, with: indexPath)
                selectedItemIndex = indexPath.row
            }
            
        case 3: // goto basket
            if let basketPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.Basket) as? Basket {
                navigate(to: basketPage, with: indexPath)
                selectedItemIndex = indexPath.row
            }
            
        case 4: // goto promotioms
            if let promotionsPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.Promotions) as? Promotions {
                navigate(to: promotionsPage, with: indexPath)
                selectedItemIndex = indexPath.row
            }
            
        case 5: // goto shops locations
            /*if let storesLocations = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.ShopsLocations) as? ShopsLocations {
                navigate(to: storesLocations, with: indexPath)
                selectedItemIndex = indexPath.row
            }*/
            if let shopsPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.Shops) as? Shops {
                navigate(to: shopsPage, with: indexPath)
                selectedItemIndex = indexPath.row
            }
            
        case 6: // goto favorites
            if let favoritePage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.Favorites) as? Favorites {
                navigate(to: favoritePage, with: indexPath)
            }
            
        case 7: // goto current orders
            if let currentOrdersPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.CurrentOrders) as? CurrentOrders {
                navigate(to: currentOrdersPage, with: indexPath)
            }
            
        case 8: // open send complaint
            if let sendComplaintPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.SendComplaint) as? SendComplaint {
                navigate(to: sendComplaintPage, with: indexPath)
            }
            
        case 9: // open settings page
            if let settingsScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.Settings) as? Settings {
                navigate(to: settingsScreen, with: indexPath)
            }
            
        case 10:
            if let aboutScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.AboutApp) as? AboutApp {
                navigate(to: aboutScreen, with: indexPath)
            }
            
        default:
            break
        }
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
}































