
import Foundation
import Localize_Swift

class PaymenyWay
{
    internal var id: int?, title: string?
    
    init(id: int, title: string) {
        self.id = id
        self.title = title
    }
    
    final internal class func getData()-> [PaymenyWay] {
        var items = Array<PaymenyWay>()
        items.append(PaymenyWay(id: 0, title: "walletlan".localized()))
        items.append(PaymenyWay(id: 1, title: "cashlan".localized()))
        items.append(PaymenyWay(id: 2, title: "visalan".localized()))
        return items
    }
    
    
}

final class PricingWay: PaymenyWay
{
    
    final internal class func getPricingItems()-> [PricingWay] {
        var items = Array<PricingWay>()
        items.append(PricingWay(id: 0, title: "pricingfromapplan".localized()))
        items.append(PricingWay(id: 1, title: "driverpricingLan".localized()))
        return items
    }
    
}




















