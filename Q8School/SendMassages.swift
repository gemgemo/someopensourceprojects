
import UIKit

class SendMassages: UICollectionViewCell
{

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var messageImage: UIImageView!
    @IBOutlet weak var sendView: UIView!
    @IBOutlet weak var leftMargin: NSLayoutConstraint!
    @IBOutlet weak var trueIcon: UIImageView!
    
}
