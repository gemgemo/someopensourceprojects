
import UIKit


final class AlshaaerAluqdsaCellScene: UITableViewCell
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    internal var alshaaer: AlshaaerAluqdsa? {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.updateUI()
            }
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var btnRadio: RadioView!
    @IBOutlet private weak var lblTitle: UILabel!
    
    
    // MARK: - Overridden functions
   
    internal override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        DispatchQueue.main.async { [weak self] in
            self?.btnRadio.isSelected = selected
        }
    }
    
    // MARK: - Functions
    
    private func updateUI()-> void {
        if let title = alshaaer?.title {
            lblTitle.text = title
        }
    }

    
    //MARK: - Actions
}



















