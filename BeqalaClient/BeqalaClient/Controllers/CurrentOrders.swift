
import UIKit

final class CurrentOrders: BaseController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var dataSource = Array<Order>() {
        didSet {
            ThreadQueue.main { [weak self] in
                self?.tblCurrentOrders.reloadData()
            }
        }
    }
    
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var titlePanal: UIView! {
        didSet {
            shadow(for: titlePanal)
        }
    }
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var tblCurrentOrders: UITableView! {
        didSet {
            tblCurrentOrders.delegate = self
            tblCurrentOrders.dataSource = self
        }
    }
    @IBOutlet fileprivate weak var btnBack: UIButton!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        loadData()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
    }
    
    
    // MARK: - Actions

    @IBAction private func backClicked(_ sender: UIButton) {
        back()
    }
}




// MARK: - Helper functions

extension CurrentOrders
{
    
    fileprivate func updateUi()-> void {
        tabBar.isHidden = true
        view.bringSubview(toFront: titlePanal)
        lblTitle.text = Localization.CurrentOrders.localized()
        btnBack.transform = CGAffineTransform.identity
        if (Localizer.instance.current == .arabic) {
            btnBack.leftAnchor.constraint(equalTo: titlePanal.leftAnchor, constant: 8).isActive = true
            tblCurrentOrders.semanticContentAttribute = .forceLeftToRight
        } else {
            btnBack.rightAnchor.constraint(equalTo: titlePanal.rightAnchor, constant: -8).isActive = true
            btnBack.transform = btnBack.transform.rotated(by: cgFloat.pi)
            tblCurrentOrders.semanticContentAttribute = .forceRightToLeft 
        }
    }
    
    fileprivate func configureTableView()-> void {
        tblCurrentOrders.register(Constants.Nibs.Order, forCellReuseIdentifier: Constants.ReuseIdentifiers.Order)
        tblCurrentOrders.alwaysBounceVertical = true
    }
    
    fileprivate func loadData()-> void {
        if (Gemo.gem.isConnected) {
            Order.fetchData(page: self) { [weak self] (orders) in
                Gemo.gem.stopSpinning()
                if (!orders.isEmpty) {
                    self?.dataSource = orders
                }
            }
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(),
                               message: Localization.InternetConnection.localized(),
                               in: view)
        }
    }
    
}


// MARK: - Table view delegate & data source functions

extension CurrentOrders: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifiers.Order, for: indexPath) as? OrderCell else { return UITableViewCell() }
        cell.order = dataSource[indexPath.row]
        return cell
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: cgFloat = 200+120+33+8+8+8+20
        let products = dataSource[indexPath.row].orderProducts
        if (!products.isEmpty) {
            height += cgFloat(products.count*80)
        }
        return height
    }
    
}






















