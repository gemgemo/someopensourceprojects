
import UIKit
import Gemo

class AddCar: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var isUpdate = false, car: Car?
    
    // MARK: - Outlets
    
    @IBOutlet weak var btnMainData: Button! { didSet { btnMainData.isChoosed = false } }
    @IBOutlet weak var btnBoardNo: Button! { didSet { btnBoardNo.isChoosed = false } }
    @IBOutlet weak var btnCarUser: Button! { didSet { btnCarUser.isChoosed = false } }
    @IBOutlet weak var containerPanel: UIView!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        guard let carMainDataPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.CarMainData) as? CarMainData else {
            return
        }
        carMainDataPage.isUpdate = isUpdate
        carMainDataPage.car = car
        move(this: carMainDataPage, in: containerPanel)
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnMainData.checkImage = UIImage()
        NotificationCenter.default.addObserver(self, selector: #selector(showBoardNo), name: .ShowBoardNo, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showCarUser), name: .ShowCarUser, object: nil)
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    internal override func updateUi() {
        super.updateUi()
        if (isUpdate) {
            setupNavigationBar(isLogo: false, isMainPage: false, title: Localization.UpdateCar.localized())
        } else {
            setupNavigationBar(isLogo: false, isMainPage: false, title: Localization.AddNewCar.localized())
        }
        
        btnMainData.setTitle(Localization.MainData.localized(), for: .normal)
        btnBoardNo.setTitle(Localization.BoardNumber.localized(), for: .normal)
        btnCarUser.setTitle(Localization.CarUser.localized(), for: .normal)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    internal override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.shadowImage = UIImage(named: Misc.ShadowImageName)
    }
    
    
    // MARK: - Actions      
    
    func showBoardNo()-> void {
        btnBoardNo.isClicked = true
        btnBoardNo.isAddImage = false
        btnBoardNo.checkImage = nil
        btnBoardNo.isChoosed = true
        
        btnMainData.isAddImage = true
        btnMainData.checkImage = #imageLiteral(resourceName: "ic_check-1")
        btnMainData.isClicked = true
        btnMainData.isChoosed = false
        guard let boardNoPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.BoardNumber) as? BoardNumber else {
            return
        }
        boardNoPage.isUpdate = isUpdate
        boardNoPage.car = car
        move(this: boardNoPage, in: containerPanel)
    }
    
    func showCarUser()-> void {
        btnCarUser.isClicked = true
        btnCarUser.isAddImage = false
        btnCarUser.checkImage = UIImage()
        btnCarUser.isChoosed = true
        
        btnBoardNo.isAddImage = true
        btnBoardNo.checkImage = #imageLiteral(resourceName: "ic_check-1")
        btnBoardNo.isChoosed = false
        btnBoardNo.isClicked = true
        guard let carUserPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.CarUser) as? CarUser else {
            return
        }
        carUserPage.isUpdate = isUpdate
        carUserPage.car = car
        move(this: carUserPage, in: containerPanel)
    }
    

}


// MARK: - Notification names

extension Notification.Name {
    
    static var ShowBoardNo: Notification.Name {
        return Notification.Name("show.board.no.id.pe")
    }
    
    static var ShowCarUser: Notification.Name {
        return Notification.Name("show.car.user.id.pe")
    }
}

























