
import UIKit
import Localize_Swift

final class Profile: BaseController
{
    
    // MARK: - Constants
    
    // MARK: - Variables
    
    fileprivate var user: User?
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var avatar: UIImageView! {
        didSet {
            avatar.rounded()
        }
    }
    @IBOutlet fileprivate weak var txfName: UITextField! {
        didSet {
            txfName.delegate = self
            txfName.textColor = .gray
        }
    }
    @IBOutlet fileprivate weak var txfPhone: UITextField! {
        didSet {
            txfPhone.delegate = self
            txfPhone.textColor = .gray
        }
    }
    @IBOutlet fileprivate weak var txfEmail: UITextField! {
        didSet {
            txfEmail.delegate = self
            txfEmail.textColor = .gray
        }
    }
    @IBOutlet private weak var badge: UIView! {
        didSet {
            badge.layer.cornerRadius = badge.bounds.height/2
            badge.clipsToBounds = true
        }
    }
    @IBOutlet private weak var lblBadgeNumber: UILabel!
    @IBOutlet fileprivate weak var btnNotifications: Button!
    @IBOutlet fileprivate weak var btnProccess: Button!
    @IBOutlet fileprivate weak var profileStack: UIStackView!
    @IBOutlet fileprivate weak var notificationsView: UIView!
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(updateUi), name: Notification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    internal override func updateNavgationBar() {
        super.updateNavgationBar()
        navigationItem.rightBarButtonItem =  UIBarButtonItem(image: #imageLiteral(resourceName: "ac_manu"), style: .plain, target: revealViewController(), action: #selector(revealViewController().rightRevealToggle(_:)))
        view.addGestureRecognizer(revealViewController().panGestureRecognizer())
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ac_back"), style: .plain, target: self, action: #selector(backToMain(_:)))
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            if (UIDevice.current.userInterfaceIdiom == .pad) {
                btnNotifications.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right:  btnNotifications.bounds.width-60)
                btnProccess.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: btnProccess.bounds.width-60)
                btnNotifications.titleEdgeInsets = Insets(top: 0, left: 28, bottom: 0, right: 0)
                btnProccess.titleEdgeInsets = Insets(top: 0, left: 35, bottom: 0, right: 0)
            } else {
                btnNotifications.titleEdgeInsets = Insets(top: 0, left: 30, bottom: 0, right: 0)
                btnNotifications.imageEdgeInsets = Insets(top: 0, left: btnNotifications.bounds.width-50, bottom: 0, right: 0)
                btnProccess.titleEdgeInsets = Insets(top: 0, left: 30, bottom: 0, right: 0)
                btnProccess.imageEdgeInsets = Insets(top: 0, left: btnProccess.bounds.width-50, bottom: 0, right: 0)
            }
        } else {
            if (UIDevice.current.userInterfaceIdiom == .pad) {
                btnNotifications.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right:  btnNotifications.bounds.width-80)
                btnProccess.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: btnProccess.bounds.width-60)
                btnNotifications.titleEdgeInsets = Insets(top: 0, left: 28, bottom: 0, right: 0)
                btnProccess.titleEdgeInsets = Insets(top: 0, left: 35, bottom: 0, right: 0)
            } else {
                btnNotifications.titleEdgeInsets = Insets(top: 0, left: -30, bottom: 0, right: 0)
                btnNotifications.imageEdgeInsets = Insets(top: 0, left: -95, bottom: 0, right: 0)
                btnProccess.titleEdgeInsets = Insets(top: 0, left: -57, bottom: 0, right: 0)
                btnProccess.imageEdgeInsets = Insets(top: 0, left: -110, bottom: 0, right: 0)
            }
        }
        loadUserData()
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    // MARK: - Actions
    
    @IBAction private func updateUserInfoOnClick(_ sender: UIButton) {
        if let editScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.UpadteUserProfile) as? UpdateUserProfile {
            editScreen.userName = user?.fullName
            editScreen.userEmail = user?.email
            editScreen.userPhone = user?.phone
            navigate(to: editScreen)
        }
    }
   
    @IBAction private func proccessClicked(_ sender: UIButton) {
        if let tripsScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Trips) as? Trips {
            navigate(to: tripsScreen)
        }
    }
    
    @IBAction private func notificationsClicked(_ sender: UIButton) {
        
    }
    
    @IBAction private func logoutOnClick(_ sender: UIButton) {
        // logout
        /*
         1. update login value to false
         2. goto login screen in storyborad login+register
         */
        UserDefaults.standard.set(false, forKey: Constants.Misc.UserLoggedIn)
        if let loginScreen = UIStoryboard(name: "Register+Login", bundle: nil).instantiateInitialViewController() as? UINavigationController {
            (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController = loginScreen
        }
    }
    
    @objc private func backToMain(_ sender: UIBarButtonItem)-> void {
        if let mainNav = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.MainNav) as? UINavigationController {
            revealViewController().pushFrontViewController(mainNav, animated: true)
        }
    }
    
    
}


// MARK: - Helper functions

extension Profile
{
    
    @objc fileprivate func updateUi()-> void {
        let sign = ": "
        txfName.text = "profilenameplacholderlan".localized()+sign+(user?.fullName ?? string.empty)
        txfPhone.text = "profilephoneplacholderlan".localized()+sign+(user?.phone ?? string.empty)
        txfEmail.text = "profileemailplacholderlan".localized()+sign+(user?.email ?? string.empty)
        print("user image is: \(user?.avatar)")
        avatar.loadImage(from: user?.avatar ?? string.empty) { (isSuccess) in
            UIApplication.showIndicator(by: false)
        }
        btnProccess.setTitle("profilebtnproccesstitlelan".localized(), for: .normal)
        btnNotifications.setTitle("profilebtnnotificationstitlelan".localized(), for: .normal)
        print(profileStack.frame)
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            txfName.textAlignment = .right
            txfEmail.textAlignment = .right
            txfPhone.textAlignment = .right
            
            /*if (UIDevice.current.userInterfaceIdiom == .pad) {
                btnNotifications.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right:  btnNotifications.bounds.width-60)
                btnProccess.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: btnProccess.bounds.width-60)
                btnNotifications.titleEdgeInsets = Insets(top: 0, left: 30, bottom: 0, right: 0)
                btnProccess.titleEdgeInsets = Insets(top: 0, left: 35, bottom: 0, right: 0)
            } else {
                btnNotifications.titleEdgeInsets = Insets(top: 0, left: 30, bottom: 0, right: 0)
                btnNotifications.imageEdgeInsets = Insets(top: 0, left: btnNotifications.bounds.width-50, bottom: 0, right: 0)
                btnProccess.titleEdgeInsets = Insets(top: 0, left: 30, bottom: 0, right: 0)
                btnProccess.imageEdgeInsets = Insets(top: 0, left: btnProccess.bounds.width-50, bottom: 0, right: 0)
            }*/
        
        } else {
            txfName.textAlignment = .left
            txfEmail.textAlignment = .left
            txfPhone.textAlignment = .left
            
            /*if (UIDevice.current.userInterfaceIdiom == .pad) {
                btnNotifications.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right:  btnNotifications.bounds.width-60)
                btnProccess.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: btnProccess.bounds.width-60)
                btnNotifications.titleEdgeInsets = Insets(top: 0, left: 30, bottom: 0, right: 0)
                btnProccess.titleEdgeInsets = Insets(top: 0, left: 35, bottom: 0, right: 0)
            } else {
                btnNotifications.titleEdgeInsets = Insets(top: 0, left: -30, bottom: 0, right: 0)
                btnNotifications.imageEdgeInsets = Insets(top: 0, left: -95, bottom: 0, right: 0)
                btnProccess.titleEdgeInsets = Insets(top: 0, left: -57, bottom: 0, right: 0)
                btnProccess.imageEdgeInsets = Insets(top: 0, left: -110, bottom: 0, right: 0)
            }*/
        }
    }
    
    fileprivate func textFields()-> [UITextField] {
        return [txfName, txfPhone, txfEmail]
    }
    
    fileprivate func loadUserData()-> void {
        startSpin()
        let userId = (UserDefaults.standard.value(forKey: Constants.Misc.UserId) as? string) ?? string.empty
        User.instance.getData(by: userId, page: self) { [weak self] (isSuccess, info) -> void in
            if (!isSuccess) {
                self?.showAlert(with: string.empty, and: "erroralertlan".localized())
                self?.stopSpin()
                return
            }
            self?.user = info
            DispatchQueue.main.async { [weak self] in
                //print(info)
                self?.updateUi()
            }
            self?.stopSpin()
        }
    }
    
}



// MARK: - Text Field delegate functions

extension Profile: UITextFieldDelegate
{
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}


// MARK: - Computed properties

extension Profile
{
    
    fileprivate var name: string {
        return txfName.text?.trimmingCharacters(in: .whitespaces) ?? string.empty
    }
    
    fileprivate var phone: string {
        return txfPhone.text?.trimmingCharacters(in: .whitespaces) ?? string.empty
    }
    
    fileprivate var email: string {
        return txfEmail.text?.trimmingCharacters(in: .whitespaces) ?? string.empty
    }
    
}























