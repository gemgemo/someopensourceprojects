
import UIKit
import Alamofire

final class Register: BaseController
{
    
    // MARK: - Constants
    
    // MARK: - Variables
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var txfName: TextField!
    @IBOutlet fileprivate weak var txfUserName: TextField!
    @IBOutlet fileprivate weak var txfPassword: TextField!
    @IBOutlet fileprivate weak var txfEmail: TextField!
    @IBOutlet fileprivate weak var txfPhone: TextField!
    @IBOutlet fileprivate weak var userImage: UIImageView! {
        didSet {
            userImage.rounded(with: Color(white: 0.88, alpha: 1.0), and: 1.0)
        }
    }
    @IBOutlet fileprivate weak var btnRegister: Button!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupTextFields(textFields: txfPhone, txfEmail, txfPassword, txfUserName, txfName)
        updateUi()
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    // MARK: - Actions
    
    @IBAction private func setImageOnClick(_ sender: UIButton) {
        choosePhoto(with: self)
    }
    
    @IBAction private func registerOnClick(_ sender: UIButton) {
        guard !name.isEmpty, !userName.isEmpty, !password.isEmpty, !email.isEmpty, !phone.isEmpty else {
            showAlert(with: string.empty, and: "reqiurefieldlan".localized())
            return
        }
        // validate email
        guard email.isEmail else {
            showAlert(with: string.empty, and: "invalidemaillan".localized())
            return
        }
        
        // upload data        
        startSpin()
        Alamofire.upload(multipartFormData: { [weak self] (multipart) in
            guard let this = self else { return }
            if let image = this.userImage.image, let imageData = UIImageJPEGRepresentation(image, 0.50) {
                multipart.append(imageData, withName: "img", fileName: "\(NSUUID().uuidString).png", mimeType: "image/jpeg")
            }
            multipart.append(this.name.toData(), withName: "fullname")
            multipart.append(this.userName.toData(), withName: "username")
            multipart.append(this.password.toData(), withName: "password")
            multipart.append(this.email.toData(), withName: "email")
            multipart.append(this.phone.toData(), withName: "phone")
            multipart.append("0".toData(), withName: "type")
        }, to: Constants.BackendURLs.Register) { [weak self] (encodingResult) in
            guard let this = self else { return }
            switch (encodingResult) {
            case .success(request: let request, _, _):
                request.responseJSON { [weak self] (response) in
                    let result = response.result
                    if (result.error != nil) {
                        print("success error is: \(result.error)")
                        self?.showAlert(with: string.empty, and: "erroralertlan".localized())
                        self?.stopSpin()
                        return
                    }
                    if let res = result.value as? [string: any], let success = res["Success"] as? string {
                        if (success == "True") {
                            print("goto code activation")
                            if let activationScreen = self?.storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Activation) as? Activation {
                                activationScreen.id = res["ID"] as? string
                                self?.navigationController?.pushViewController(activationScreen, animated: true)
                            }
                        } else {
                            self?.showAlert(with: string.empty, and: (res["Massage"] as? string) ?? string.empty)
                        }
                    }
                    
                    self?.stopSpin()
                }
                break
            case .failure(let error):
                print("register error is: ", error)
                this.showAlert(with: string.empty, and: "Error Occurred")
                this.stopSpin()
            }
        }
        
        
    }
    
}


// MARK: - Helper functions

extension Register
{
    
    fileprivate func updateUi()-> void {
        let langCode = Locale.current.languageCode ?? "en"
        btnRegister.setTitle("registerlan".localized(), for: .normal)
        txfName.placeholder = "profilenameplacholderlan".localized()
        txfUserName.placeholder = "UserNamelan".localized()
        txfPassword.placeholder = "passwordlan".localized()
        txfPhone.placeholder = "profilephoneplacholderlan".localized()
        txfEmail.placeholder = "profileemailplacholderlan".localized()
        if (langCode == Constants.Language.English) { // english
            txfName.semanticContentAttribute = .forceRightToLeft
            txfEmail.semanticContentAttribute = .forceRightToLeft
            txfPhone.semanticContentAttribute = .forceRightToLeft
            txfPassword.semanticContentAttribute = .forceRightToLeft
            txfUserName.semanticContentAttribute = .forceRightToLeft
            txfUserName.textAlignment = .left
            txfEmail.textAlignment = .left
            txfPhone.textAlignment = .left
            txfPassword.textAlignment = .left
            txfName.textAlignment = .left
        } else { // arabic
            txfName.semanticContentAttribute = .forceLeftToRight
            txfEmail.semanticContentAttribute = .forceLeftToRight
            txfPhone.semanticContentAttribute = .forceLeftToRight
            txfPassword.semanticContentAttribute = .forceLeftToRight
            txfUserName.semanticContentAttribute = .forceLeftToRight
            txfUserName.textAlignment = .right
            txfEmail.textAlignment = .right
            txfPhone.textAlignment = .right
            txfPassword.textAlignment = .right
            txfName.textAlignment = .right
        }
    }
    
    fileprivate func setupNavigationBar()-> void {
        navigationItem.title = "registerlan".localized();
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ac_back"), style: .plain, target: self, action: #selector(goBack(_:)))
        navigationController?.navigationBar.semanticContentAttribute = .forceLeftToRight
    }
    
    
    fileprivate func setupTextFields(textFields: UITextField...)-> void {
        textFields.forEach { $0.delegate = self }
    }
    
    
    
    
}


// MARK: - Computed properties

extension Register
{
    
    
    fileprivate var name: string {
        return txfName.text?.trimmingCharacters(in: .whitespaces) ?? string.empty
    }
    
    fileprivate var userName: string {
        return txfUserName.text?.trimmingCharacters(in: .whitespaces) ?? string.empty
    }
    
    fileprivate var password: string {
        return txfPassword.text?.trimmingCharacters(in: .whitespaces) ?? string.empty
    }
    
    fileprivate var email: string {
        return txfEmail.text?.trimmingCharacters(in: .whitespaces) ?? string.empty
    }
    
    fileprivate var phone: string {
        return txfPhone.text?.trimmingCharacters(in: .whitespaces) ?? string.empty
    }
    
    
}


// MARK: - Text Field delegate functions

extension Register: UITextFieldDelegate
{
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}


// MARK: - Image pivker delegate functions

extension Register: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    
    internal func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        //showAlert(with: string.empty, and: "تم الالغاء")
        dismiss(animated: true, completion: nil)
    }
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        userImage.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        dismiss(animated: true, completion: nil)
    }
    
}



















