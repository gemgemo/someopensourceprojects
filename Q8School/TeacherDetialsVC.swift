//
//  TeacherDetialsVC.swift
//  Q8School
//
//  Created by mac on 12/13/16.
//  Copyright © 2016 Atiaf. All rights reserved.
//

import UIKit

class TeacherDetialsVC: UIViewController {
    var selectedID: String!
    var dataFound: Bool!
    
    @IBOutlet weak var myAdsImg: UIImageView!
    @IBOutlet weak var offerLbl: UILabel!
    @IBOutlet weak var myAdsLbl: UILabel!
    @IBOutlet weak var addAdsLbl: UILabel!
    @IBOutlet weak var callUsLbl: UILabel!
    @IBOutlet weak var openMenu: UIBarButtonItem!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var directCallLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var scholarPhaseLbl: UILabel!
    @IBOutlet weak var subjectNameLbl: UILabel!
    var images_cache = [String:UIImage]()
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    var userId: String!
    var date: String!
    var id: String!
    var userImages: String!
    var userNames: String!
    var scholarPhases: String!
    var subjectNames: String!
    var detialS: String!
    var message: String!
    var phone: String!
    @IBOutlet weak var v2: UIView!
    @IBOutlet weak var v3: UIView!
    @IBOutlet weak var v4: UIView!
    @IBOutlet weak var v5: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var loadingview: UIView!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet weak var loadingLbl: UILabel!
    var type: String?
    var ID: String!
    var img: UIImage!
    
    @IBOutlet weak var directCallView: UIView!
    @IBOutlet weak var deatialTxtView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.contentSize.height = 650
        userImage.layer.cornerRadius = 0.5 * userImage.bounds.size.width
        userImage.clipsToBounds = true
        self.title = "Ads Detail"
        teacherDetial(Settings.lang, selectedID)
        shadowView.isHidden = true
        loadingview.isHidden = true
        ID = UserDefaults.standard.string(forKey: "users_ID")
        if Settings.lang == "ar"{
            directCallLbl.text = "اتصال مباشر"
            phoneLabel.text = "هاتف"
            v2.semanticContentAttribute = .forceRightToLeft
            v3.semanticContentAttribute = .forceRightToLeft
            v4.semanticContentAttribute = .forceRightToLeft
            v5.semanticContentAttribute = .forceRightToLeft
            scholarPhaseLbl.textAlignment = .right
            deatialTxtView.textAlignment = .right
            phoneLabel.textAlignment = .right
            phoneLbl.textAlignment = .right
            subjectNameLbl.textAlignment = .right
            phoneLabel.textAlignment = .right
            self.title = "تفاصيل الاعلان "
            loadingLbl.text = "جاري التحميل"
            callUsLbl.text = "اتصل بنا"
            addAdsLbl.text = "اضف اعلانك"
            myAdsLbl.text = "اعلاناتي"
            offerLbl.text = "العروض"
            print("..h..\(selectedID)")
            print("..h..\(ID)")
        }
        if revealViewController() != nil {
            
            revealViewController().rightViewRevealWidth = 235
            openMenu.target = revealViewController()
            openMenu.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        guard let type = UserDefaults.standard.string(forKey: "userType") else { return }
        print("user type: \(type)")
        if type == "1" {
            directCallView.isHidden = false
            if Settings.lang == "ar" {
                myAdsLbl.text = "المفضلة"
                myAdsImg.image = UIImage(named: "fav")
            } else {
                myAdsLbl.text = "Favorite"
                myAdsImg.image = UIImage(named: "fav")
            }
        } else {
            directCallView.isHidden = true
        }
    }
    
    
    func load_image(_ link: String, imageview: UIImageView) {
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: url)
        request.timeoutInterval = 10
        session.dataTask(with: url) { (data :Data?, response :URLResponse?, error:Error?) in
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                return
            }
            var image = UIImage(data: data!)
            if (image != nil) {
                func set_image() {
                    self.images_cache[link] = image
                    imageview.image = image
                    self.img = image
                }
                DispatchQueue.main.async(execute: set_image)
            }
            }
            .resume()
    }
    
    private func teacherDetial(_ lang: String, _ teacherID: String)-> Void {
        loadingview.isHidden = false
        shadowView.isHidden = false
        loadingActivity.startAnimating()
        let request = NSMutableURLRequest(url: URL(string:Settings.TeacherDetials)!)
        request.httpMethod = "POST"
        let postString = "lang=\(lang)&ad_id=\(teacherID)&studentId=\(selectedID)"
        print("teacher paramters: \(postString)")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {
                print("error=\(String(describing: error))")
                return
            }
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("response teacher details String = \(String(describing: responseString))")
            if let responseData = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
                    print("get teacher details json: \(json)")
                    self.dataFound = json["Success"] as? Bool
                    if self.dataFound! {
                        let data = json["Result"] as! [NSDictionary]
                        for i in data {
                            self.id = i["id"] as! String
                            self.userNames = i["username"] as? String
                            self.userImages = i["img"] as! String
                            self.subjectNames = i["subjectName"] as? String
                            self.scholarPhases = i["scholarPhaseName"] as? String
                            self.detialS = i["details"] as? String
                            self.date = i["date_time"] as! String
                            self.userId = i["userID"] as! String
                            self.phone = i["mobile"] as? String
                        }
                    }
                    DispatchQueue.main.async { [unowned self] in
                        self.userName.text = self.userNames
                        self.subjectNameLbl.text = self.subjectNames
                        self.scholarPhaseLbl.text = self.scholarPhases
                        self.deatialTxtView.text = self.detialS
                        self.phoneLbl.text = self.phone
                        self.load_image(self.userImages, imageview: self.userImage)
                        let fullNameArr = self.date.characters.split{$0 == " "}.map(String.init)
                        self.dateLbl.text = fullNameArr[0]
                        self.timeLbl.text = fullNameArr[1]
                        if self.userId == self.ID {
                            self.directCallView.isHidden = true
                        }
                        self.shadowView.isHidden = true
                        self.loadingview.isHidden = true
                        self.loadingActivity.stopAnimating()
                    }
                } catch {
                    print ("Could not Serialize: \(error)")
                }
            }
            }
            .resume()
    }
    
    private func favoriteBtn( _ lang: String, _ user_id: String, _ ad_id: String)-> Void {
        print("\(user_id)")
        print("\(ad_id)")
        let request = NSMutableURLRequest(url: URL(string:Settings.LikeUnlike)!)
        request.httpMethod = "POST"
        let postString = "lang=\(lang)&user_id=\(user_id)&ad_id=\(ad_id)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {
                print("error=\(String(describing: error))")
                return
            }
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
            if let responseData = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                    print("fav\(json)")
                    self.dataFound = json["Success"] as? Bool
                    if self.dataFound!
                    {
                        self.message = json["Message"] as! String
                    }
                    else
                    {
                        self.message = json["Message"] as! String
                        
                    }
                    DispatchQueue.main.async { [unowned self] in
                        self.displayAlerts(self.message)
                    }
                } catch {
                    print ("Could not Serialize")
                }
                
            }
            
        }
        task.resume()
    }
    
    func adReport(_ lang: String,_ user_id: String,_ ad_id: String) {
        let request = NSMutableURLRequest(url: URL(string:Settings.ReportAds)!)
        request.httpMethod = "POST"
        let postString = "lang=\(lang)&user_id=\(user_id)&ad_id=\(ad_id)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        { data, response, error in
            guard error == nil && data != nil else {
                print("error=\(String(describing: error))")
                return
            }
            
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("ad report response String = \(String(describing: responseString))")
            
            if let responseData = data {
                
                do {
                    
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                    print("fav\(json)")
                    self.dataFound = json["Success"] as? Bool
                    if self.dataFound!
                    {
                        self.message = json["Message"] as! String
                        
                    }
                    else
                    {
                        self.message = json["Message"] as! String
                        
                    }
                    DispatchQueue.main.async
                        {
                            self.displayAlerts(self.message)
                            
                    }
                    
                }
                catch
                {
                    print ("Could not Serialize")
                }
                
            }
            
        }
        task.resume()
        
    }
    
    
    private func displayAlerts(_ Massage: String)-> Void {
        var t = ""
        var b = ""
        if Settings.lang == "ar" {
            t = "تنبيه"
            b = "إغلاق"
        } else {
            t = "Alert"
            b = "Close"
        }
        let myAlert = UIAlertController(title: t, message:Massage, preferredStyle: UIAlertControllerStyle.alert);
        let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler:nil)
        myAlert.addAction(okAction);
        self.present(myAlert, animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func directCallBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SendMassageVC") as! SendMassageVC
        vc.selectedID = self.userId
        vc.selectedName = self.userName.text
        vc.selectedImage = self.userImages
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        self.navigationController!.popViewController(animated: true)
        
    }
    
    @IBAction func callUsBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CallUsVc") as! CallUsVc
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func addAdsBtn(_ sender: Any) {
        if type == "1"
        {
            if Settings.lang == "ar"
            {
                let msg = "انت طالب لا يمكنك اضافة اعلان"
                self.displayAlerts(msg)
                
            }
            else
            {
                let msg = "you'r student can't add Ads"
                self.displayAlerts(msg)
                
            }
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AddAdsVc") as! AddAdsVc
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        
    }
    
    
    @IBAction func myAdsBtn(_ sender: Any) {
        if type == "1"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyFavoriteVC") as! MyFavoriteVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyAdsVC") as! MyAdsVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        
        
    }
    
    @IBAction func offerBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AdsPageViewController") as! AdsPageViewController
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func shareBtn(_ sender: UIButton) {
        let tst = "\n" + self.userNames! + "\n" + self.subjectNames! + "\n" + self.scholarPhases! + "\n" + self.detialS! + "\n" + self.phone! + "\n" + self.userImages!
        // set up activity view controller
        let objectsToShare: [AnyObject] = [ tst  as AnyObject ]
        let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func blockBtn(_ sender: UIButton) {
        adReport(Settings.lang, ID, selectedID)
        
    }
    
    @IBAction func favoriteBtn(_ sender: Any) {
        
        favoriteBtn(Settings.lang, ID, selectedID)
        
    }
    
    
    
}
