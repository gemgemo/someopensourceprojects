
import Foundation
import Gemo
import UIKit

@IBDesignable
class Button: UIButton {
    
    @IBInspectable
    internal var isClicked: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    internal var isSpinner: Bool = false { didSet { setNeedsDisplay() } }
    
    internal var nonSelectedColor: UIColor = Color.lightGray {
        didSet {
            setNeedsDisplay()
        }
    }
    
    internal var isChoosed: bool = false { didSet { setNeedsDisplay() } }
    
    internal var isAddImage = false
    
    private var divider: UIView = {
        let seperator = UIView()
        seperator.translatesAutoresizingMaskIntoConstraints = false
        seperator.backgroundColor = .main
        seperator.isHidden = false
        return seperator
    }()
    
    private var checkImageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    @IBInspectable
    internal var checkImage: UIImage?
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        addSubview(divider)
        addSubview(checkImageView)
        divider.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        let height = divider.heightAnchor.constraint(equalToConstant: 1)
        height.isActive = true
        divider.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        divider.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        checkImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        checkImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        checkImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        checkImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        if (isSpinner) {
            divider.backgroundColor = nonSelectedColor
            setTitleColor(.black, for: .normal)
        } else {
            if (isClicked) {
                divider.backgroundColor = .main
                setTitleColor(.main, for: .normal)
                height.constant = 1.3
            } else {
                divider.backgroundColor = nonSelectedColor
                setTitleColor(.lightGray, for: .normal)
                height.constant = 1.0
            }
            if (isChoosed && isClicked) {
                checkImageView.image = nil
                divider.backgroundColor = .main
            } else if (!isChoosed && isClicked) {
                divider.backgroundColor =  .main
            }
            if (isAddImage) {
                divider.backgroundColor = nonSelectedColor
                checkImageView.image = checkImage
            }
        }
    }
    
    
}



























