
import Foundation
import Gemo
import Alamofire

final class User: Mapper<any> {
    internal var id, name, username, email, phone, latitude, longitude, address, password, confirmedPassword, photo: string?
    
    internal override func mapping(mapper: Mapper<any>) {
        id = mapper["user_id"]?.string
        name = mapper["name"]?.string
        username = mapper["username"]?.string
        email = mapper["email"]?.string
        phone = mapper["phone"]?.string
        latitude = mapper["lati"]?.string
        longitude = mapper["longi"]?.string
        address = mapper["address"]?.string
        photo = mapper["user_photo"]?.string
    }
    
}


/*
 
 {
 "result": "true",
 "data": "تم التعديل بنجاح",
 "user_id": "22",
 "name": "gamal",
 "username": "gemogem",
 "user_photo": "http:\/\/192.232.214.91\/~petroapp\/\/upload\/users\/pics\/about_us_321615231500309453.jpg",
 "email": "gamal@gamal.gamal",
 "phone": "9893475873485",
 "lati": "30.001689",
 "longi": "31.172076",
 "address": "gize"
 }
 
 */



class UserViewModel {
    
    internal func signUp(for user: User, in page: BaseController?, _ completion: @escaping(User?, bool)->())-> void {
        guard Gemo.gem.isConnected else {
            page?.displayAlert(with: Localization.InternetConnection.localized())
            completion(nil, false)
            return
        }
        guard let name = user.name, !name.isEmpty else {
            page?.displayAlert(with: Localization.FirstNameReqiured.localized())
            completion(nil, false)
            return
        }
        guard let username = user.username, !username.isEmpty else {
            page?.displayAlert(with: Localization.UserNameReqiured.localized())
            completion(nil, false)
            return
        }
        guard let phone = user.phone, !phone.isEmpty else {
            page?.displayAlert(with: Localization.PhoneNumberReqiured.localized())
            completion(nil, false)
            return
        }
        guard let email = user.email, email.isEmail else {
            page?.displayAlert(with: Localization.InvaildEmail.localized())
            completion(nil, false)
            return
        }
        guard let address = user.address, !address.isEmpty else {
            page?.displayAlert(with: Localization.AddressReqiured.localized())
            completion(nil, false)
            return
        }
        guard let latitude = user.latitude, let longitude = user.longitude, !latitude.isEmpty, !longitude.isEmpty else {
            page?.displayAlert(with: Localization.LocationReqiured.localized())
            completion(nil, false)
            return
        }
        guard let password = user.password, !password.isEmpty else {
            page?.displayAlert(with: Localization.PasswordReqiured.localized())
            completion(nil, false)
            return
        }
        guard let confirmedPassword = user.confirmedPassword, !confirmedPassword.isEmpty else {
            page?.displayAlert(with: Localization.RePasswordReqiured.localized())
            completion(nil, false)
            return
        }
        guard password.characters.count >= 6 else {
            page?.displayAlert(with: Localization.PasswordCount.localized())
            completion(nil, false)
            return
        }
        guard password == confirmedPassword else {
            page?.displayAlert(with: Localization.PasswordNotConfirmed.localized())
            completion(nil, false)
            return
        }
        let paramters: Dictionary<string, any> = [
            Misc.MainKey: Misc.MainValue,
            Misc.LanguageKey: Localizer.instance.current.rawValue,
            "name": name,
            "email": email,
            "username": username,
            "phone": phone,
            "address": address,
            "lati": latitude,
            "longi": longitude,
            "password": password,
            "password_again": confirmedPassword
        ]
        //Gemo.gem.startSpinning()
        Http.request(link: Misc.getUrl(for: "register"), method: .post, parameters: paramters)
            .response { (response) in
                let error = response.error
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("sign up error: \(string(describing: error))")
                    page?.displayAlert(with: error?.localizedDescription)
                    completion(nil, false)
                    return
                }
                Gemo.gem.stopSpinning()
                guard let json = response.result as? Dictionary<string, any>,
                let success = json["result"] as? string else {
                    page?.displayAlert(with: Localization.NoData.localized())
                    completion(nil, false)
                    return
                }
                if (success == "false") {
                    page?.displayAlert(with: json["data"] as? string)
                    completion(nil, false)
                } else {
                    completion(User(JSON: json), true)
                }
        }
    }
    
    internal func save(this person: User?)-> void {
        guard let user = person else {
            print("empty user")
            return
        }
        let db = UserDefaults.standard
        db.set(user.id, forKey: Misc.UserId)
        db.set(user.name, forKey: Misc.Name)
        db.set(user.username, forKey: Misc.Username)
        db.set(user.email, forKey: Misc.Email)
        db.set(user.phone, forKey: Misc.Phone)
        db.set(user.latitude, forKey: Misc.Latitude)
        db.set(user.longitude, forKey: Misc.Longitude)
        db.set(user.address, forKey: Misc.Address)
        db.set(user.photo, forKey: Misc.Photo)
        db.synchronize()
    }
    
    internal func signIn(for user: User, in page: BaseController?, _ completion: @escaping(User)->())-> void {
        guard let username = user.username, !username.isEmpty else {
            page?.displayAlert(with: Localization.UserNameReqiured.localized())
            return
        }
        guard let password = user.password, !password.isEmpty else {
            page?.displayAlert(with: Localization.PasswordReqiured.localized())
            return
        }
        let paramaters: [string: any] = [
            Misc.MainKey: Misc.MainValue,
            Misc.LanguageKey: Localizer.instance.current.rawValue,
            "do_login": username,
            "password": password
        ]
        Gemo.gem.startSpinning()
        Http.request(link: Misc.getUrl(for: "login"), method: .post, parameters: paramaters)
            .response { (response) in
                let error = response.error
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("sign in error: \(string(describing: error))")
                    page?.displayAlert(with: error?.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
                guard let json = response.result as? Dictionary<string, any>,
                let success = json["result"] as? string else {
                    page?.displayAlert(with: Localization.NoData.localized())
                    return
                }
                if (success == "true") {
                    completion(User(JSON: json))
                } else {
                    page?.displayAlert(with: json["data"] as? string)
                }
                
        }
    }
    
}



extension Misc {
    internal static let UserId = "user.id.db",
    Name = "user.name.db",
    Username = "user.user.name.db",
    Email = "user.email.db",
    Phone = "user.phone.db",
    Latitude = "user.latitude.db",
    Longitude = "user.longitude.db",
    Address = "user.address.db",
    Photo = "user.photo.db"
}










/*
 
 {
 "result": "true",
 "data": "تم التعديل بنجاح",
 "user_id": "22",
 "name": "gamal",
 "username": "gemogem",
 "user_photo": "http:\/\/192.232.214.91\/~petroapp\/\/upload\/users\/pics\/about_us_321615231500309453.jpg",
 "email": "gamal@gamal.gamal",
 "phone": "9893475873485",
 "lati": "30.001689",
 "longi": "31.172076",
 "address": "gize"
 }
 */
























