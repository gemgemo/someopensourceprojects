
import UIKit


final class AwardListImages: BaseController
{
    // MARK: - Constants
    
    //MARK: - Variables
    
    fileprivate var dataSource = Array<Album>() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tblImages.reloadData()
            }
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var tblImages: UITableView! {
        didSet {
            tblImages.delegate = self
            tblImages.dataSource = self
        }
    }
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarDirections()
        updateNavigationBar()
    }
    
    //MARK: - Actions
    
}



// MARK: - Helper functions

extension AwardListImages
{
    
    fileprivate func updateNavigationBar()-> void {
        navigationItem.title = "awardphotoslan".localized()
        navigationController?.navigationBar.castShadow = "set"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: getBackImage(), style: .plain, target: self, action: #selector(goBack(_:)))
    }
    
    fileprivate func loadData()-> void {
        startSpin()
        //print(Constants.BackendURLs.shared.Albumes)
        Album.getData(by: Constants.BackendURLs.shared.Albumes, viewController: self) { [weak self] (albums) -> void in
            self?.dataSource = albums
            self?.stopSpin()
        }
    }
    
    
}


// MARK: - Table view delegate & data source functions

extension AwardListImages: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.AwardListImages) as? AwardListImagesCell {
            cell.album = dataSource[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        
        if let imagesDetailsScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.AwardListImagesDetails) as? AwardListImagesDetails {
            imagesDetailsScreen.album = dataSource[indexPath.row]
            navigationController?.pushViewController(imagesDetailsScreen, animated: true)
        }
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.bounds.height/3.2
    }
    
}
































