
import UIKit

final class InfoCell: UITableViewCell
{
    // MARK: - Constants
    
    // MARK: - Variables
    
    internal var info: InfoImage? {
        didSet {
            updateUi()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var mainPanal: UIView!
    @IBOutlet private weak var icon: UIImageView!
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var divider: UIView! {
        didSet {
            divider.backgroundColor = Color(white: 0.80, alpha: 1.0)
        }
    }
    
    // MARK: - Overridden functions
    
    private func updateUi()-> void {
        lblTitle.text = info?.title ?? .empty
        icon.image = info?.icon
    }
    
    // MARK: - Actions
    
}


















