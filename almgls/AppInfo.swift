
import UIKit
import Alamofire
import Gemo

final class AppInfo: UIViewController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    
    // MARK: - Outlets
    
    @IBOutlet private weak var dotView: UIView! {
        didSet {
            dotView.layer.cornerRadius = dotView.frame.height/2
            dotView.layer.masksToBounds = true
        }
    }
    @IBOutlet fileprivate weak var lblTitle: UILabel! { didSet { lblTitle.text = "" } }
    @IBOutlet fileprivate weak var txtInfo: UITextView! { didSet { txtInfo.text = "" } }
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = "من نحن"
        let button = UIBarButtonItem(image: #imageLiteral(resourceName: "back").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(backOnClick(_:)))
        button.tintColor = .white
        navigationItem.leftBarButtonItem = button
        updateUi()
    }
    
    
    // MARK: - Actions
    
    @objc private func backOnClick(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }

}


// MARK: - Helper functions 

extension AppInfo
{
    
    fileprivate func updateUi()-> void {
        txtInfo.textAlignment = .right
    }
    
    fileprivate func loadData()-> Void {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        Alamofire.request(Settings.AboutUs).responseJSON { [weak self] (response) in
            let result = response.result
            if (result.error != nil) {
                print("error is \(String(describing: result.error))")
                self?.dialogUser(with: result.error!.localizedDescription)
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                return
            }
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            if let json = (result.value as? [Dictionary<String, Any>])?.first, let title = json["title"] as? String, let content = json["content"] as? String {
                DispatchQueue.main.async { [weak self] in
                    self?.lblTitle.text = title
                    if let htmlData = content.data(using: .unicode, allowLossyConversion: true) {
                        let paragraph = NSMutableParagraphStyle()
                        paragraph.alignment = .right
                        let attributes = try? NSAttributedString(data: htmlData, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSParagraphStyleAttributeName: paragraph, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
                        self?.txtInfo.attributedText = attributes
                        self?.txtInfo.contentMode = .right
                    }
                    
                }
                
            }
        }
    }
    
    
}
































