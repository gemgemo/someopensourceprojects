
import Foundation


internal final class Section: Mapper<Any>
{
    
    internal var id, title, cover: string?
    
    internal override var description: String {
        return "\n id: \(string(describing: id)), title: \(string(describing: title)), coverLink: \(string(describing: cover)) \n"
    }
    
    internal override func mapping(mapper: Mapper<Any>) {
        self.id = mapper["id"]?.text
        self.title = mapper["title"]?.text
        self.cover = mapper["logo"]?.text
    }
    
    internal class func getData(page: BaseController, _ complation: @escaping ([Section])->())-> void {
        let parameters: [string: any] = [
            Constants.Misc.MainKey: Constants.Misc.MainValue,
            Constants.Misc.LanguageBackendKey: Localizer.instance.current.rawValue
        ]
        HttpClient.request(link: Constants.Urls.Categories, method: .post, parameters: parameters) { (data, response, error) -> void in
            if (error != nil) {
                Gemo.gem.stopSpinning()
                print("get categories data error \(string(describing: error))")
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription, in: page.view)
                return
            }
            guard let jsonValue = data as? Dictionary<string, any>, let value = jsonValue["all_data"] as? [Dictionary<string, any>] else {
                print("categories none data")
                Gemo.gem.stopSpinning()
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized(), in: page.view)
                return
            }
            complation(value.map { Section(JSON: $0) })
        }
    }
    
    //matjer_id
    
    internal class func getSection(idStore: string?, _ page: BaseController, _ complation: @escaping ([Section])->())-> void {
        let parameters: [string: any] = [
            Constants.Misc.MainKey: Constants.Misc.MainValue,
            Constants.Misc.LanguageBackendKey: Localizer.instance.current.rawValue,
            "matjer_id": idStore ?? .empty
        ]
        HttpClient.request(link: Constants.Urls.StoreSections, method: .post, parameters: parameters) { (data, response, error) -> void in
            if (error != nil) {
                Gemo.gem.stopSpinning()
                print("get categories data error \(string(describing: error))")
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription, in: page.view)
                return
            }
            guard let jsonValue = data as? Dictionary<string, any>, let value = jsonValue["all_data"] as? [Dictionary<string, any>] else {
                print("categories none data")
                Gemo.gem.stopSpinning()
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized(), in: page.view)
                return
            }
            complation(value.map { Section(JSON: $0) })
        }
    }
    
    
}


































