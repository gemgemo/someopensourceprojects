
import UIKit
import Gemo
import Alamofire

final class EditProduct: BaseController
{
    
    
    // MARK: - Constatnts
    
    
    // MARK: - Variables
    
    internal var isNew = false, product: Product?
    fileprivate var selectedTag = -1, categories = Array<PopupItem>(), selectedSection: PopupItem?
    
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var txfProductName: UITextField! { didSet { txfProductName.delegate = self } }
    @IBOutlet fileprivate weak var txfProductPrice: UITextField! { didSet { txfProductPrice.delegate = self } }
    @IBOutlet fileprivate weak var txfProductQuantity: UITextField! { didSet { txfProductQuantity.delegate = self } }
    @IBOutlet private weak var lblProductImages: UILabel!
    @IBOutlet private weak var btnEditProduct: UIButton!
    @IBOutlet fileprivate var productImages: [UIImageView]!
    @IBOutlet fileprivate var productImagesButton: [UIButton]!
    @IBOutlet private weak var btnCategoriesSpinner: UIButton!
    @IBOutlet fileprivate weak var txtDescription: TextView!
    @IBOutlet private weak var checkBoxPanel: UIView! { didSet {
        checkBoxPanel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(checkBoxClicked)))
        }}
    @IBOutlet fileprivate weak var checkBox: CheckBox! { didSet {
        checkBox.selectedColor = .main
        checkBox.image = #imageLiteral(resourceName: "ic-true")
        }}
    @IBOutlet private weak var lblCheckBox: UILabel!
    
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        txtDescription.placeholder = Localization.Description.localized()
        btnCategoriesSpinner.setTitle(Localization.Categories.localized(), for: .normal)
        fetchSections()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    internal override func updateUi() {
        super.updateUi()
        tabBar.isHidden = true
        btnEditProduct.setCorner(radius: 8)
        btnEditProduct.setShadow(with: .zero, radius: 0.7, opacity: 0.5, color: .main)
        txfProductName.placeholder = Localization.ProductName.localized()
        txfProductPrice.placeholder = Localization.Price.localized()
        txfProductQuantity.placeholder = Localization.Quantity.localized()
        lblProductImages.text = Localization.ImagesForProduct.localized()
        lblCheckBox.text = Localization.SpecialAd.localized()
        if (isNew) {
            navigationItem.title = Localization.AddProduct.localized()
            btnEditProduct.setTitle(Localization.AddProduct.localized(), for: .normal)
        } else {
            navigationItem.title = Localization.UpdateProduct.localized()
            btnEditProduct.setTitle(Localization.UpdateProduct.localized(), for: .normal)
            fillDateWithProduct()
        }
        var margin = btnCategoriesSpinner.bounds.width
        print(margin)
        //print(UIScreen.main.bounds.width)
        if (UIScreen.main.bounds.width == 320.0) {
            margin = btnCategoriesSpinner.bounds.width - 20
        } else {
            margin = btnCategoriesSpinner.frame.width + 40
        }
        print(margin)
        if (Localizer.instance.current == .arabic) {
            txfProductQuantity.textAlignment = .right
            txfProductPrice.textAlignment = .right
            txfProductName.textAlignment = .right
            txtDescription.textAlignment = .right
            btnCategoriesSpinner.contentHorizontalAlignment = .right
            btnCategoriesSpinner.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: margin)
            btnCategoriesSpinner.titleEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: 8)
            checkBox.rightAnchor.constraint(equalTo: checkBoxPanel.rightAnchor, constant: 0).isActive = true
            lblCheckBox.rightAnchor.constraint(equalTo: checkBox.leftAnchor, constant: -8).isActive = true
        } else {
            txfProductQuantity.textAlignment = .left
            txfProductPrice.textAlignment = .left
            txfProductName.textAlignment = .left
            txtDescription.textAlignment = .left
            btnCategoriesSpinner.contentHorizontalAlignment = .left
            btnCategoriesSpinner.imageEdgeInsets = Insets(top: 0, left: margin, bottom: 0, right: 0)
            btnCategoriesSpinner.titleEdgeInsets = Insets(top: 0, left: -16, bottom: 0, right: 0)
            checkBox.leftAnchor.constraint(equalTo: checkBoxPanel.leftAnchor, constant: 0).isActive = true
            lblCheckBox.leftAnchor.constraint(equalTo: checkBox.rightAnchor, constant: 8).isActive = true
        }
        
    }
    
    
    // MARK: - Actions
    
    @IBAction private func editProductOnClick(_ sender: UIButton) {
        view.endEditing(true)
        guard Gemo.gem.isConnected else {
            dialogUserWithNoInternetConnectionMessage()
            return
        }
        guard !productName.isEmpty else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.ProductNameReqiured.localized())
            return
        }
        guard !price.isEmpty else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.PriceReqiured.localized())
            return
        }
        guard !quantity.isEmpty else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.QuantityReqiured.localized())
            return
        }
        guard !categoryId.isEmpty else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.SectionReqiured.localized())
            return
        }
        guard !details.isEmpty && details != txtDescription.placeholder else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.DescriptionReqiured.localized())
            return
        }
        guard !productImages.isEmpty else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.ImageReqiured.localized())
            return
        }
        (isNew) ? editProduct(link: Urls.AddProduct, isUpdate: false) : editProduct(link: Urls.UpdateProduct, isUpdate: true)
    }
    
    @IBAction private func chooseImageOnCLick(_ sender: UIButton) {
        view.endEditing(true)
        selectedTag = sender.tag
        openImagePicker(with: self)
    }
    
    @IBAction private func showCategories(_ sender: UIButton) {
        view.endEditing(true)
        if (categories.isEmpty) {
            fetchSections()
        } else {
            let rect = sender.convert(sender.frame, to: view)
            openPopover(with: categories, in: rect, and: Size(width: sender.frame.width, height: 200), .up) { [weak self] (index) in
                self?.selectedSection = self?.categories[index]
                sender.setTitle(self?.selectedSection?.title ?? .empty, for: .normal)
            }
        }
    }
    
    @objc private func checkBoxClicked()-> void {
        checkBox.isChecked = !checkBox.isChecked
    }
    
    
}



// MARK: - Text Field delegate functions

extension EditProduct: UITextFieldDelegate
{
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}


// MARK: - Image picker delegate functions

extension EditProduct: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var choosedImage = UIImage()
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            choosedImage = editedImage
        } else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            choosedImage = image
        }
        productImages[selectedTag].image = choosedImage
        productImagesButton[selectedTag].setImage(#imageLiteral(resourceName: "photo-camera2"), for: .normal)
        picker.dismiss(animated: true)
    }
    
    internal func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
}


// MARK: - Helper functions

extension EditProduct
{
    
    fileprivate func fillDateWithProduct()-> void {
        txfProductName.text = product?.name ?? .empty
        txfProductPrice.text = product?.price ?? .empty
        txfProductQuantity.text = product?.quantity ?? .empty
        txtDescription.text = product?.details ?? .empty
        txtDescription.color = .black        
        if let offer = product?.offer, offer == "1" {
            checkBox.isChecked = true
        } else {
            checkBox.isChecked = false
        }
        productImages[0].loadImage(from: product?.photo1 ?? .empty, nil)
        productImages[1].loadImage(from: product?.photo2 ?? .empty, nil)
        productImages[2].loadImage(from: product?.photo3 ?? .empty, nil)
        productImages[3].loadImage(from: product?.photo4 ?? .empty, nil)
    }
    
    fileprivate func editProduct(link: string, isUpdate: bool)-> void {
        Gemo.gem.startSpinning()
        Alamofire.upload(multipartFormData: { [weak self] (fromData) in
            guard let this = self else { return }
            fromData.append(Misc.MainValue.toData, withName: Misc.MainKey)
            if (isUpdate) {
                fromData.append((this.product?.id ?? .empty).toData, withName: "id")
            } else {
                fromData.append((UserDefaults.standard.string(forKey: Misc.GroceryId) ?? .empty).toData, withName: "user_id")
            }
            fromData.append(this.productName.toData, withName: "title")
            fromData.append(this.price.toData, withName: "price")
            fromData.append(this.quantity.toData, withName: "quantity")
            fromData.append(this.categoryId.toData, withName: "id_cat")
            fromData.append((this.checkBox.isChecked ? "1" : "0").toData, withName: "offer")
            fromData.append(this.details.toData, withName: "details")
            if let image = this.productImages[0].image {
                fromData.append(image.toData(quality: 0.40), withName: "photo_1", fileName: "\(NSUUID().uuidString)\(Date()).png", mimeType: "image/png")
            }
            if let image = this.productImages[1].image {
                fromData.append(image.toData(quality: 0.40), withName: "photo_2", fileName: "\(NSUUID().uuidString)\(Date()).png", mimeType: "image/png")
            }
            if let image = this.productImages[2].image {
                fromData.append(image.toData(quality: 0.40), withName: "photo_3", fileName: "\(NSUUID().uuidString)\(Date()).png", mimeType: "image/png")
            }
            if let image = this.productImages[3].image {
                fromData.append(image.toData(quality: 0.40), withName: "photo_4", fileName: "\(NSUUID().uuidString)\(Date()).png", mimeType: "image/png")
            }
            print("data multi parted")
        }, to: link) { (result) in
            switch (result) {
            case .failure(let error):
                print("edit product error is: \(string(describing: error))")
                Gemo.gem.stopSpinning()
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error.localizedDescription)
            case .success(request: let request, streamingFromDisk: _, streamFileURL: _):
                request.responseJSON { (response) in
                    let error = response.error
                    if (error != nil) {
                        print("edit product error: \(string(describing: error))")
                        Gemo.gem.stopSpinning()
                        Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription)
                        return
                    }
                    Gemo.gem.stopSpinning()
                    guard let json = (response.result.value as? [Dictionary<string, any>])?.first,
                        let _ = json["data"] as? string,
                        let _ = json["data_en"] as? string else {
                            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized())
                            return
                    }
                    //Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: (Localizer.instance.current == .arabic) ? aMessage : eMessage)
                    Threads.main { [weak self] in
                        _ = self?.navigationController?.popToRootViewController(animated: true)
                        NotificationCenter.default.post(name: .UpdateProducts, object: nil)
                    }
                    
                }
                
            }
        }
    }
    
    fileprivate func fetchSections()-> void {
        let paramters: [string: any] = [
            Misc.MainKey: Misc.MainValue,
            Misc.LanguageParameterName: Localizer.instance.current.rawValue,
            "user_id": UserDefaults.standard.string(forKey: Misc.GroceryId) ?? .empty
        ]
        SelectedSection.getSections(for: Urls.SectionsById, with: paramters) { [weak self] (sections) in
//            print("sections: \(sections)")
            if (!sections.isEmpty) {
                self?.categories = sections.map { PopupItem(id: $0.id?.toInt, title: $0.title) }
            } else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: "Empty sections")
            }
        }
    }
    
    
}


// MARK: - Computed properties

extension EditProduct
{
    
    fileprivate var productName: string {
        return txfProductName.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var price: string {
        return txfProductPrice.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var quantity: string {
        return txfProductQuantity.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var categoryId: string {
        return selectedSection?.id?.toString ?? .empty
    }
    
    fileprivate var details: string {
        return txtDescription.text.trimmingCharacters(in: .whitespaces)
    }
    
    
}



















