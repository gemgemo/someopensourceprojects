
import UIKit
import UserNotifications
import Firebase

final class SignIn: BaseController
{
    // MARK: - Constants
    
    // MARK: - Variables
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var footerView: UIView! {
        didSet {
            footerView.setShadow(with: .zero, radius: 2, opacity: 0.4, color: Color(white: 0.88, alpha: 0.9))
        }
    }
    @IBOutlet fileprivate weak var txfUserName: TextField! {
        didSet {
            txfUserName.delegate = self
        }
    }
    @IBOutlet fileprivate weak var txfPassword: TextField! {
        didSet {
            txfPassword.delegate = self
        }
    }
    @IBOutlet fileprivate weak var btnReset: UIButton!
    @IBOutlet fileprivate weak var btnLogin: Button!
    @IBOutlet fileprivate weak var btnRegister: UIButton!
    @IBOutlet fileprivate weak var registerIcon: UIImageView!
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    
    // MARK: - Actions
    
    @IBAction private func resetPasswordOnClick(_ sender: UIButton) {
        let alert = UIAlertController(title: "تعديل كلمة المرور", message: "ادخل بريدك الالكتروني", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "البريد الالكتروني"
            textField.textAlignment = .right
        }
        alert.addAction(UIAlertAction(title: "موافق", style: .default, handler: { [weak self] (action) in
            guard let this = self else { return }
            if (Gemo.gem.isConnected) {
                let email = alert.textFields?[0].text?.trimmingCharacters(in: .whitespaces) ?? .empty
                guard email.isEmail && !email.isEmpty else {
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InvaildEmail.localized(), in: this.view)
                    return
                }
                let parameters: [string: any] = [
                    Constants.Misc.MainKey: Constants.Misc.MainValue,
                    "email": email
                ]
                this.resetPassword(withParameters: parameters)
            } else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(),
                                   message: Localization.InternetConnection.localized(),
                                   in: this.view)
            }
        }))
        alert.addAction(UIAlertAction(title: "الغاء", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction private func signInOnCLick(_ sender: Button) {
        if (Gemo.gem.isConnected) {
            guard !userName.isEmpty else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.UserNameReqiured.localized(), in: view)
                return
            }
            guard !password.isEmpty else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.PasswordReqiured.localized(), in: view)
                return
            }
            
            guard !(password.characters.count < 6) else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.PasswordCount.localized(), in: view)
                return
            }
            let params: [string: any] = [
                Constants.Misc.MainKey: Constants.Misc.MainValue,
                "username": userName,
                "password": password
            ]
            print(params)
            login(withParameters: params)
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
    
    @IBAction private func signUpOnClick(_ sender: UIButton) {
        guard let signUpScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.SignUp) as? SignUp  else { return }
        signUpScreen.isUpdate = false
        navigate(to: signUpScreen)
    }
    
}



// MARK: - Text Field delegate functions

extension SignIn: UITextFieldDelegate
{
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}


// MARK: - Computed Properties

extension SignIn
{
    
    fileprivate var userName: string {
        return txfUserName.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var password: string {
        return txfPassword.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    
}


// MARK: - Helper Functions

extension SignIn
{
    
    fileprivate func updateUi()-> void {
        navigationController?.navigationBar.isHidden = true
        tabBar.isHidden = true
        if (Localizer.instance.current == .arabic) {
            txfUserName.textAlignment = .right
            txfPassword.textAlignment = .right
            footerView.semanticContentAttribute = .forceRightToLeft
            btnReset.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16.0).isActive = true
            registerIcon.rightAnchor.constraint(equalTo: footerView.rightAnchor, constant: -12.0).isActive = true
        } else {
            txfUserName.textAlignment = .left
            txfPassword.textAlignment = .left
            footerView.semanticContentAttribute = .forceLeftToRight
            btnReset.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16.0).isActive = true
            registerIcon.leftAnchor.constraint(equalTo: footerView.leftAnchor, constant: 12.0).isActive = true
        }
        txfUserName.placeholder = Localization.UserName.localized()
        txfPassword.placeholder = Localization.Password.localized()
        btnLogin.setTitle(Localization.Login.localized(), for: .normal)
        btnRegister.setTitle(Localization.RegisterButton.localized(), for: .normal)
        btnReset.setTitle(Localization.ForgotPassword.localized(), for: .normal)
        btnRegister.titleLabel?.font = Font.systemFont(ofSize: 10)
    }
      
    fileprivate func resetPassword(withParameters params: Dictionary<string, any>)-> void {
        print(params)
        Gemo.gem.startSpinning(in: view)
        HttpClient.request(link: Constants.Urls.ResetPassword, method: .post, parameters: params) { [weak self] (data, response, error) -> void in
            guard let this = self else { return }
            if (error != nil) {
                print("login error \(string(describing: error))")
                Gemo.gem.stopSpinning()
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error?.localizedDescription ?? .empty, in: this.view)
                return
            }
            Gemo.gem.stopSpinning()
            print("reset password response: \(string(describing:data))")
            if let value = (data as? [[string: any]])?.first, let message = value["data"] as? string {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: message, in: this.view)
            } else {
                print("reset password nil data")
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.Error, in: this.view)
            }
        }
    }
    
    fileprivate func navigateToHome()-> void {
        AppManager.manage.sideMenu(for: (UIApplication.shared.delegate as? AppDelegate)?.window)
    }
    
    fileprivate func login(withParameters: Dictionary<string, any>)-> void {
        Gemo.gem.startSpinning(in: view)
        HttpClient.request(link: Constants.Urls.SignIn, method: .post, parameters: withParameters) { [weak self] (data, response, error) -> void in
            guard let this = self else { return }
            if (error != nil) {
                print("login error \(string(describing: error))")
                Gemo.gem.stopSpinning()
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error?.localizedDescription ?? .empty, in: this.view)
                return
            }
            Gemo.gem.stopSpinning()
            print("login data: \(string(describing: data))")
            if let value = (data as? [[string: any]])?.first, let success = value["result"] as? string {
                if (success == "true") {
                    let user = User(JSON: value)
                    UserDefaults.standard.set(user.uId, forKey: Constants.Misc.UserID)
                    UserDefaults.standard.set(true, forKey: Constants.Misc.IsLoggedIn)
                    UserDefaults.standard.set(user.firstName, forKey: Constants.Misc.FirstName)
                    UserDefaults.standard.set(user.secondName, forKey: Constants.Misc.LastName)
                    UserDefaults.standard.set(user.phone, forKey: Constants.Misc.Phone)
                    UserDefaults.standard.set(user.name, forKey: Constants.Misc.UserName)
                    UserDefaults.standard.set(user.email, forKey: Constants.Misc.Email)
                    UserDefaults.standard.set(user.gender, forKey: Constants.Misc.Gender)
                    UserDefaults.standard.set(user.latitude, forKey: Constants.Misc.UserLatitude)
                    UserDefaults.standard.set(user.longitude, forKey: Constants.Misc.UserLongitude)
                    // goto main screen 
                    print("goto main screen")
                    DispatchQueue.main.async { [weak self] in
                        guard let me = self else { return }
                        if let token = FIRInstanceID.instanceID().token() {
                            let parameters: Dictionary<string, any> = [
                                Constants.Misc.MainKey: Constants.Misc.MainValue,
                                "user_id": UserDefaults.standard.string(forKey: Constants.Misc.UserID) ?? .empty,
                                "d_token": token,
                                "type": 1
                            ]
                            print("remote notifications paramters: \(string(describing: parameters))")
                            HttpClient.request(link: Constants.Urls.DeviceToken, method: .post, parameters: parameters) { (data, response, error) -> void in
                                if (error != nil) {
                                    print("send device token error \(string(describing: error))")
                                    return
                                }
                                print("send device token data \(string(describing: data))")
                            }
                        } else {
                            me.configureRemoteNotifications()
                        }
                        me.navigateToHome()
                    }
                } else {
                    guard let message = value["data"] as? string else { return }
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: message, in: this.view)
                }
            } else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.Error.localized(), in: this.view)
            }
        }
    }

}


// MARK: - User notifications delegate and firebase messageing functions

/*extension SignIn: UNUserNotificationCenterDelegate, FIRMessagingDelegate
{

    internal func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("ios 10 user notification received--------------------->>>>>>>>.")
        completionHandler()
    }
    
    internal func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("ios 10 notification received in foreground", notification.request.content.userInfo)
        completionHandler([.alert, .sound])
    }
    
    internal func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        print("message data: \(remoteMessage.appData)")
    }
    
}*/


























