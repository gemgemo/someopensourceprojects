
import UIKit
import KYDrawerController
import UserNotifications
import Firebase


internal class BaseController: UIViewController
{
    
    // MARK: - Constants
    
    // MARK: - Variables
    
    // MARK: - Outlets
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isStatusBarHidden = false
        setupTabBar()
        tabBarItems?.reloadData()
        
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBar.isHidden = false
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("user id: \(UserDefaults.standard.string(forKey: Constants.Misc.UserID) ?? .empty)")
        NotificationCenter.default.addObserver(self, selector: #selector(gotoProductDetails), name: .OpenProductDetails, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(gotoCurrentOrders), name: .OpenCurrentOrders, object: nil)
    }
    
    internal override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        automaticallyAdjustsScrollViewInsets = false
        tabBarItems?.collectionViewLayout.invalidateLayout()
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: - Actions
    
    internal func goBackOnCLick(_ sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    internal func back()-> void {
        _ = navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Functions
    
    @objc private func gotoProductDetails()-> void {
        guard let productPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.ProductDetails) as? ProductDetails else { return }
        productPage.productId = (UIApplication.shared.delegate as? AppDelegate)?.productId
        navigate(to: productPage)
    }
    
    @objc private func gotoCurrentOrders()-> void {
        guard let currentOrdersPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.CurrentOrders) as? CurrentOrders else { return }
        navigate(to: currentOrdersPage)
    }
    
    internal func navigateToBasket()-> void {
        guard let basketPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.Basket) as? Basket else {
            return
        }
        navigate(to: basketPage, animated: false)        
    }
    
    internal func navigateToFavorites()-> void {
        guard let favoritesPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.Favorites) as? Favorites else { return }
        navigate(to: favoritesPage)
    }
    
    internal func navigateToSearch()-> void {
        guard let searchPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.Search) as? Search else { return }
        navigate(to: searchPage)
    }
    
    internal func add(to link: string, forUserId uId: string, andProductId pId: string)-> void { 
        if (Gemo.gem.isConnected) {
            Gemo.gem.startSpinning(in: view)
            let parameters: [string: string] = [
                Constants.Misc.MainKey: Constants.Misc.MainValue,
                "user_id": uId,
                "product_id": pId
            ]
            HttpClient.request(link: link, method: .post, parameters: parameters) { [weak self] (data, response, error) -> void in
                guard let this = self else { return }
                if (error != nil) {
                    print("add to user account error \(string(describing: error))")
                    Gemo.gem.stopSpinning()
                    return
                }
                print("add to user account info \(string(describing: data))")
                Gemo.gem.stopSpinning()
                guard let json = (data as? [[string: any]])?.first, let arabicMessage = json["data"] as? string, let englishMessage = json["data_en"] as? string else {
                    print("none message when add product")
                    return
                }
                let message = (Localizer.instance.current == .arabic) ? arabicMessage : englishMessage
                Gemo.gem.showAlert(withTitle: .empty, message: message, in: this.view)
            }
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
    
    internal func delete(from link: string, userId uId: string, productId pId: string, _ completion: @escaping()->())-> void {
        if (Gemo.gem.isConnected) {
            UIApplication.showIndicator(by: true)
            let parameters: [string: any] = [
                Constants.Misc.MainKey: Constants.Misc.MainValue,
                "user_id": uId,
                "product_id": pId
            ]
            HttpClient.request(link: link, method: .post, parameters: parameters) { [weak self] (data, response, error) -> void in
                guard let this = self else { return }
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("delete function error \(string(describing: error))")
                    UIApplication.showIndicator(by: false)
                    return
                }
                print("remove item response: \(string(describing: data))")
                guard let json = (data as? [[string: any]])?.first, let success = json["result"] as? string, let arabicMessage = json["data"] as? string, let englishMessage = json["data_en"] as? string else {
                    print("none message when remove product")
                    return
                }
                UIApplication.showIndicator(by: false)
                let message = (Localizer.instance.current == .arabic) ? arabicMessage : englishMessage
                Gemo.gem.showAlert(withTitle: .empty, message: message, in: this.view)
                if (success == "true") {
                    completion()
                }
            }
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
    
    internal func navigate(to controller: UIViewController, animated: bool = true)-> void {
        navigationController?.pushViewController(controller, animated: animated)
    }           
    
    internal func menu(withState state: KYDrawerController.DrawerState)-> void {        
        if let drawer = navigationController?.parent as? KYDrawerController {
            drawer.delegate = self
            drawer.setDrawerState(state, animated: true)
        }
    }
    
    internal func shadow(for panal: UIView)-> void {
        panal.setShadow(with: Size(right: 0, top: 1), radius: 0.6, opacity: 0.3, color: .black)
    }
    
    
    // MARK: - Custom tab bar
    
    fileprivate let tabBarDataSource = [#imageLiteral(resourceName: "store"), #imageLiteral(resourceName: "offer"), #imageLiteral(resourceName: "cart"), #imageLiteral(resourceName: "home")]
    
    internal lazy var tabBar: UIView = {
       let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.setShadow(with: Size(right: 0, top: -1), radius: 1.0, opacity: 0.4, color: .black)
        v.isHidden = false
        return v
    }()
    
    internal lazy var tabBarItems: UICollectionView? = { [weak self] in
        guard let this = self else { return nil }
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let grid = UICollectionView(frame: this.tabBar.frame, collectionViewLayout: layout)
        grid.translatesAutoresizingMaskIntoConstraints = false
        grid.alwaysBounceHorizontal = false
        grid.alwaysBounceVertical = false
        grid.backgroundColor = .white
        grid.delegate = self
        grid.dataSource = self
        return grid
    }()
    
    fileprivate let tabBarCellId = "com.tab.bar.cell.id"
    
    private func setupTabBar()-> void {
        view.addSubview(tabBar)
        tabBarItems?.bounces = false
        tabBarItems?.alwaysBounceVertical = false
        tabBarItems?.alwaysBounceHorizontal = false
        tabBarItems?.isScrollEnabled = false
        tabBarItems?.showsVerticalScrollIndicator = false
        tabBarItems?.showsHorizontalScrollIndicator = false
        if let collectionView = tabBarItems {
            tabBar.addSubview(collectionView)
            collectionView.register(TabBarCell.self, forCellWithReuseIdentifier: tabBarCellId)
        }
        // main view constraints
        tabBar.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tabBar.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tabBar.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tabBar.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        // items constraints
        tabBarItems?.rightAnchor.constraint(equalTo: tabBar.rightAnchor).isActive = true
        tabBarItems?.bottomAnchor.constraint(equalTo: tabBar.bottomAnchor).isActive = true
        tabBarItems?.leftAnchor.constraint(equalTo: tabBar.leftAnchor).isActive = true
        tabBarItems?.topAnchor.constraint(equalTo: tabBar.topAnchor).isActive = true
        if (Localizer.instance.current == .arabic) {
            tabBarItems?.semanticContentAttribute = .forceLeftToRight
        } else {
            tabBarItems?.semanticContentAttribute = .forceRightToLeft
        }
    }
    
    internal func configureRemoteNotifications()-> void {
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            print("granted: \(granted)")
            if (granted) {
                FIRMessaging.messaging().remoteMessageDelegate = self
                UIApplication.shared.registerForRemoteNotifications()
            } else {
                print("notification error \(string(describing: error))")
            }
        }
    }

}

internal var selectedTabBarItem = -1

extension BaseController: UICollectionViewDelegate,
    UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout
{
    internal func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabBarDataSource.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: tabBarCellId, for: indexPath) as? TabBarCell else {
            return UICollectionViewCell()
        }
        cell.image = tabBarDataSource[indexPath.item]
        return cell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch(indexPath.item) {
        case 0 : // stores
            if (selectedTabBarItem != indexPath.item) {
                if let shopsLocations = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.ShopsLocations) as? ShopsLocations {
                    navigate(to: shopsLocations, animated: false)
                }
            }
            
        case 1 : // pormotions
            if (selectedTabBarItem != indexPath.item) {
                if let promotionsScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.Promotions) as? Promotions {
                    navigate(to: promotionsScreen, animated: false)
                }
            }

        case 2 : //basket
            if (selectedTabBarItem != indexPath.item) {
                if let basketScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.Basket) as? Basket {
                    navigate(to: basketScreen, animated: false)
                }
            }

        case 3 : // shops            
            if (selectedTabBarItem != indexPath.item) {
                if let shopsScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.Shops) as? Shops {
                    navigate(to: shopsScreen, animated: false)
                }
            }
            
        default:
            break
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = tabBar.frame.size.width*0.25
        return Size(width: cellWidth, height: collectionView.frame.size.height-0.3)
    }
    
}



// MARK: - Tab bar cell

internal final class TabBarCell: UICollectionViewCell
{
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    internal var image: UIImage? {
        didSet {
            icon.image = image
        }
    }
    
    internal lazy var mainPanal: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var icon: UIImageView = { [weak self] in
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private func setupViews()-> void {
        contentView.addSubview(mainPanal)
        mainPanal.addSubview(icon)
        // main panal constraints
        mainPanal.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        mainPanal.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        mainPanal.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        mainPanal.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        // icon constraints y,x,w,h
        icon.centerXAnchor.constraint(equalTo: mainPanal.centerXAnchor).isActive = true
        icon.centerYAnchor.constraint(equalTo: mainPanal.centerYAnchor).isActive = true
        icon.widthAnchor.constraint(equalToConstant: 25.0).isActive = true
        icon.heightAnchor.constraint(equalToConstant: 25.0).isActive = true
    }
    
    override var isSelected: Bool {
        didSet {
            mainPanal.backgroundColor = (isSelected) ? Color(white: 0.88, alpha: 0.9) : .white
        }
    }
    
}


// MARK: - Drawer delegate functions

extension BaseController: KYDrawerControllerDelegate
{
    
    internal func drawerController(_ drawerController: KYDrawerController, stateChanged state: KYDrawerController.DrawerState) {        
    }
    
}



// MARK: - User notifications delegate and firebase messageing functions

extension BaseController: UNUserNotificationCenterDelegate, FIRMessagingDelegate
{
    
    internal func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("ios 10 user notification received--------------------->>>>>>>>.")
        completionHandler()
    }
    
    internal func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("ios 10 notification received in foreground", notification.request.content.userInfo)
        completionHandler([.alert, .sound])
    }
    
    internal func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        print("message data: \(remoteMessage.appData)")
    }
    
}
















