
import UIKit

import Localize_Swift

final class MainContent: NSObject
{
    
    internal var id: int?, title: string?, logo: UIImage?
    
    override init() {
        super.init()
    }
    
    init(id: int, title: string, logo: UIImage) {
        self.title = title
        self.logo = logo
    }
    
    
    internal class func getData()-> [MainContent] {
        var array = Array<MainContent>()
        
        array.append(MainContent(id: 1, title: "managerMessagesPageTitle".localized(), logo: #imageLiteral(resourceName: "muhammedAlharthy")))
        array.append(MainContent(id: 2, title: "aboutUsPageTitle".localized(), logo: #imageLiteral(resourceName: "icon1")))
        array.append(MainContent(id: 3, title: "ourServicestitle".localized(), logo: #imageLiteral(resourceName: "icon2")))
        array.append(MainContent(id: 4, title: "alshaaeralmuqadasatitle".localized(), logo: #imageLiteral(resourceName: "icon3")))
        array.append(MainContent(id: 5, title: "ScoutServicetitle".localized(), logo: #imageLiteral(resourceName: "icon4")))
        array.append(MainContent(id: 6, title: "HealthServicestitle".localized(), logo: #imageLiteral(resourceName: "icon5")))
        array.append(MainContent(id: 7, title: "HotelServicestitle".localized(), logo: #imageLiteral(resourceName: "icon6")))
        array.append(MainContent(id: 8, title: "Servicessubsistencetitle".localized(), logo: #imageLiteral(resourceName: "icon7")))
        array.append(MainContent(id: 9, title: "Transportservicestitle".localized(), logo: #imageLiteral(resourceName: "icon8")))
        array.append(MainContent(id: 10, title: "videosSerivecestitle".localized(), logo: #imageLiteral(resourceName: "videos_ic")))
        
        return array
    }
    

}































