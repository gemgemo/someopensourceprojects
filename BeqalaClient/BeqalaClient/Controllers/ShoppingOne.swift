
import UIKit

final class ShoppingOne: BaseController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var quantities = Array<int>()
    fileprivate enum DeliveryWay: int {
        case store = 1, order = 2
    }
    fileprivate var deliveryWay = DeliveryWay.store
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var titlePanel: UIView! { didSet { shadow(for: titlePanel) } }
    @IBOutlet fileprivate weak var lblTitle: UILabel! { didSet { lblTitle.text = .empty } }
    @IBOutlet fileprivate weak var lblsubTitle: UILabel! { didSet { lblsubTitle.text = .empty } }
    @IBOutlet fileprivate weak var lblFromShop: UILabel! { didSet { lblFromShop.text = .empty } }
    @IBOutlet fileprivate weak var rdFromShop: RadioView!
    @IBOutlet fileprivate weak var rdReceiveOrder: RadioView!
    @IBOutlet fileprivate weak var lblReceiveOrder: UILabel! { didSet { lblReceiveOrder.text = .empty } }
    @IBOutlet fileprivate weak var btnSubmit: Button!
    @IBOutlet fileprivate weak var leftPointer: View!
    @IBOutlet fileprivate weak var lblLeftPointer: UILabel!
    @IBOutlet fileprivate weak var btnBack: UIButton!
    @IBOutlet fileprivate weak var rightPointer: View!
    @IBOutlet fileprivate weak var lblRightPointer: UILabel!
    @IBOutlet fileprivate weak var fromShopPanel: View!
    @IBOutlet fileprivate weak var fromReceverPanel: View!
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
    }
    
    
    // MARK: - Actions
    
    @IBAction private func backOnCLick(_ sender: UIButton) {
        back()
    }
    
    @IBAction private func fromShopClicked(_ sender: UIButton) {
        deliveryWay = DeliveryWay.store
        rdFromShop.isSelected = true
        rdReceiveOrder.isSelected = false
    }

    @IBAction private func receiveOrderClicked(_ sender: UIButton) {
        deliveryWay = DeliveryWay.order
        rdFromShop.isSelected = false
        rdReceiveOrder.isSelected = true
    }
    
    @IBAction private func submitOnCLick(_ sender: Button) {
        if (deliveryWay == .store) {
            guard let storeShoppingPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.StoreShopping) as? StoreShopping else { return }
            storeShoppingPage.deliveryWay = deliveryWay.rawValue
            storeShoppingPage.quantities = quantities
            navigate(to: storeShoppingPage)
        } else {
            guard let recipientPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.ReceiverOrder) as? ReceiveOrder else { return }
            recipientPage.deliveryWay = deliveryWay.rawValue
            recipientPage.quantities = quantities
            navigate(to: recipientPage)
        }
    }
    
}


// MARK: - Helper functions 

extension ShoppingOne
{
    
    fileprivate func updateUi()-> void {
        view.bringSubview(toFront: titlePanel)
        tabBar.isHidden = true
        lblTitle.text = Localization.Shopping.localized()
        lblsubTitle.text = Localization.DeliveryWay.localized()
        lblFromShop.text = Localization.DeliveryByStore.localized()
        lblReceiveOrder.text = Localization.ReceivingOrder.localized()
        btnSubmit.setTitle(Localization.Continuation.localized(), for: .normal)
        btnBack.transform = CGAffineTransform.identity        
        if (Localizer.instance.current == .arabic) {
            btnBack.leftAnchor.constraint(equalTo: titlePanel.leftAnchor, constant: 8).isActive = true
            rightPointer.backgroundColor = #colorLiteral(red: 0.04690527171, green: 0.7682238221, blue: 0.02999053523, alpha: 1)
            lblRightPointer.text = "1"
            leftPointer.backgroundColor = .darkGray
            lblLeftPointer.text = "2"
            rdFromShop.rightAnchor.constraint(equalTo: fromShopPanel.rightAnchor, constant: -8).isActive = true
            lblFromShop.textAlignment = .right
            lblFromShop.rightAnchor.constraint(equalTo: rdFromShop.leftAnchor, constant: -16).isActive = true
            lblFromShop.leftAnchor.constraint(equalTo: fromShopPanel.leftAnchor, constant: 8).isActive = true
            rdReceiveOrder.rightAnchor.constraint(equalTo: fromReceverPanel.rightAnchor, constant: -8).isActive = true
            lblReceiveOrder.textAlignment = .right
            lblReceiveOrder.rightAnchor.constraint(equalTo: rdReceiveOrder.leftAnchor, constant: -16).isActive = true
            lblReceiveOrder.leftAnchor.constraint(equalTo: fromReceverPanel.leftAnchor, constant: 8).isActive = true
        } else {
            btnBack.rightAnchor.constraint(equalTo: titlePanel.rightAnchor, constant: -8).isActive = true
            btnBack.transform = btnBack.transform.rotated(by: cgFloat.pi)
            rightPointer.backgroundColor = .darkGray
            lblRightPointer.text = "2"
            leftPointer.backgroundColor = #colorLiteral(red: 0.04690527171, green: 0.7682238221, blue: 0.02999053523, alpha: 1)
            lblLeftPointer.text = "1"
            rdFromShop.leftAnchor.constraint(equalTo: fromShopPanel.leftAnchor, constant: 8).isActive = true
            lblFromShop.textAlignment = .left
            lblFromShop.rightAnchor.constraint(equalTo: fromShopPanel.rightAnchor, constant: -8).isActive = true
            lblFromShop.leftAnchor.constraint(equalTo: rdFromShop.rightAnchor, constant: 16).isActive = true
            rdReceiveOrder.leftAnchor.constraint(equalTo: fromReceverPanel.leftAnchor, constant: 8).isActive = true
            lblReceiveOrder.textAlignment = .left
            lblReceiveOrder.rightAnchor.constraint(equalTo: fromReceverPanel.rightAnchor, constant: -8).isActive = true
            lblReceiveOrder.leftAnchor.constraint(equalTo: rdReceiveOrder.rightAnchor, constant: 16).isActive = true
        }
    }
    
}





















