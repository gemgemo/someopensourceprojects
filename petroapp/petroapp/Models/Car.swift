
import Foundation
import Gemo

class Car: Mapper<any> {
    internal var id, userId, type, icon, photo, prand, model, style, fuel, aNumbers, aLetters, eNumbers, eLetters, fullname, username, code, userPhoto, userCarPhoto, balance: string?
    
    internal override func mapping(mapper: Mapper<any>) {
        id = mapper["id"]?.string
        userId = mapper["user_id"]?.string
        type = mapper["type_car"]?.string
        icon = mapper["photo_b"]?.string
        photo = mapper["photo_n"]?.string
        prand = mapper["brand_car"]?.string
        model = mapper["model_car"]?.string
        style = mapper["year_car"]?.string
        fuel = mapper["fuel_type"]?.string
        aNumbers = mapper["num_ar"]?.string
        eNumbers = mapper["num_en"]?.string
        aLetters = mapper["ar_car"]?.string
        eLetters = mapper["en_car"]?.string
        fullname = mapper["full_name"]?.string
        username = mapper["username"]?.string
        code = mapper["code_car"]?.string
        userCarPhoto = mapper["photo_car"]?.string
        userPhoto = mapper["photo_user"]?.string
        balance = mapper["balance"]?.string
    }
    
    internal class func fetchCars(_ page: BaseController?,_ completion: @escaping([Car])->())-> void {
        Gemo.gem.startSpinning()
        Http.request(link: Misc.getUrl(for: "select_user_cars"), method: .post,
                     parameters: [Misc.MainKey: Misc.MainValue,
                                  Misc.LanguageKey: Localizer.instance.current.rawValue, "user_id": UserDefaults.standard.string(forKey: Misc.UserId) ?? .empty])
            .response { (response) in
                //print("cars json string: \(response.jsonString ?? .empty)")
                let error = response.error
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("\(#function) error: \(error!)")
                    page?.displayAlert(with: error?.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
                guard let json = response.result as? [Dictionary<string, any>] else {
                    page?.displayAlert(with: Localization.NoData.localized())
                    return
                }
                completion(json.map { Car(JSON: $0) })
        }
    }
}








































