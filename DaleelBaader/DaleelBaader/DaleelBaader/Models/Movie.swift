
import UIKit

import Localize_Swift

final class Movie: NSObject
{
    
    internal var id, title, content, link: string?
    
    override init() {
        super.init()
    }
    
    internal override var description: String {
        return "\n id: \(id),title: \(title),content: \(content),link: \(link) \n"
    }
    
    init(id: string, title: string, content: string, link: string) {
        self.id = id
        self.title = title
        self.content = content
        self.link = link
    }
    
    init(dictionary: [string: any]) {
        id = dictionary["id"] as? string
        title = dictionary["title"] as? string
        content = dictionary["content"] as? string
        link = dictionary["url"] as? string
    }
    
    // Functions
    
    internal class func retrieveData(viewController: UIViewController?, onComplete: @escaping([Movie])-> void)-> void {
        UIApplication.showIndicator(by: true)
        let link = (Localize.currentLanguage() == Constants.Misc.ArabicSign) ? Constants.BackendURLs.VideosAR : Constants.BackendURLs.VideosEN
        if (link.isEmpty || !link.isLink) {
            viewController?.dialog(with: Constants.Misc.ErrorMessage)
            print("link error")
            UIApplication.showIndicator(by: false)
            return
        }
        
        guard let requestUrl = URL(string: link) else {
            viewController?.dialog(with: Constants.Misc.ErrorMessage)
            print("request url error")
            UIApplication.showIndicator(by: false)
            return
        }
        
        URLSession.shared.dataTask(with: URLRequest(url: requestUrl)) { (data, response, error) in
            if (error != nil) {
                viewController?.handle(this: error)
                print("response error")
                UIApplication.showIndicator(by: false)
                return
            }
            
            guard let jsonData = data else {
                viewController?.dialog(with: Constants.Misc.ErrorMessage)
                print("null data error")
                UIApplication.showIndicator(by: false)
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [[string: any]] {
                    onComplete(json.map { Movie(dictionary: $0) })
                }
            } catch {
                viewController?.handle(this: error)
                print("catch error")
                UIApplication.showIndicator(by: false)
            }
            
        }.resume()
    }

}





















