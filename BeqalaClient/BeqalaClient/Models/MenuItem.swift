
import Foundation
import UIKit


final class MenuItem {
    
    
    internal var id = Number.zero.uintValue, title = string.empty, icon = UIImage()
    
    init(id: uint, title: string, icon: UIImage) {
        self.id = id
        self.title = title
        self.icon = icon
    }
    
    
    internal static func items(_ complation: @escaping(Array<MenuItem>)->())-> void {
        DispatchQueue.global(qos: .userInteractive).async {
            var items = Array<MenuItem>()
            items.append(MenuItem(id: 1, title: Localization.Profile.localized(), icon: (UserDefaults.standard.string(forKey: Constants.Misc.Gender) ?? "1" == "1" ? #imageLiteral(resourceName: "man") : #imageLiteral(resourceName: "women"))))
            items.append(MenuItem(id: 2, title: Localization.Sections.localized(), icon: #imageLiteral(resourceName: "list2")))
            items.append(MenuItem(id: 3, title: Localization.Basket.localized(), icon: #imageLiteral(resourceName: "cart")))
            items.append(MenuItem(id: 4, title: Localization.Promotions.localized(), icon: #imageLiteral(resourceName: "offer")))
            items.append(MenuItem(id: 5, title: Localization.Shops.localized(), icon: #imageLiteral(resourceName: "store")))
            items.append(MenuItem(id: 6, title: Localization.Favorits.localized(), icon: #imageLiteral(resourceName: "favorite")))
            items.append(MenuItem(id: 7, title: Localization.Orders.localized(), icon: #imageLiteral(resourceName: "order")))
            items.append(MenuItem(id: 8, title: Localization.SendComplaint.localized(), icon: #imageLiteral(resourceName: "sendComplaint")))
            items.append(MenuItem(id: 9, title: Localization.Settings.localized(), icon: #imageLiteral(resourceName: "setting")))
            items.append(MenuItem(id: 10, title: Localization.AboutApp.localized(), icon: #imageLiteral(resourceName: "about")))
            complation(items)
        }
    }
    
    
}













































