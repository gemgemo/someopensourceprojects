
import Foundation


extension Notification.Name {
    internal static var LanguageChanged: Notification.Name {
        return Notification.Name("com.lang.change.invoke.id")
    }
}

internal class Localizer
{
    
    private let signKey = "com.langauge.sign.key"
    
    internal static var instance: Localizer {
        struct Obj {
            internal static let ob = Localizer()
        }
        return Obj.ob
    }
    
    // computed properties to get and set current app language
    
    internal var current: Language {
        if (UserDefaults.standard.value(forKey: signKey) != nil) {
            guard let sign = UserDefaults.standard.value(forKey: signKey) as? string else { return .arabic }
            return Language(rawValue: sign) ?? .arabic
        } else {
            var language = Language.arabic
            guard let langSign = Locale.current.languageCode else { return .arabic }
            language = langSign.contains(Language.arabic.rawValue) ? Language.arabic : Language.english
            //print("lang \(language)")
            //UserDefaults.standard.set(language.rawValue, forKey: signKey)
            return language
        }
    }
    
    internal func set(language: Language)-> void {
        if (UserDefaults.standard.value(forKey: signKey) as? string != nil) {
            UserDefaults.standard.set(language.rawValue, forKey: signKey)
        } else {
            UserDefaults.standard.set(Language.arabic.rawValue, forKey: signKey)
        }
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: .LanguageChanged, object: nil)
        /*else {
            var language = Language.arabic
            guard let langSign = Locale.current.languageCode else {
                language = Language.arabic
                return
            }
            language = langSign.contains(Language.arabic.rawValue) ? Language.arabic : Language.english
            UserDefaults.standard.set(language.rawValue, forKey: signKey)
        }*/
    }
                    
    
}


internal enum Language: string {
    case arabic = "ar", english = "en"
}



extension string {
    
    internal func localized()-> string {
        return ((Localizer.instance.current == .arabic) ? LocalizeStrings.arabicWords[self] : LocalizeStrings.englishWords[self]) ?? .empty
    }
    
}


































