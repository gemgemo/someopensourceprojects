
import Foundation
import UIKit
import Localize_Swift


internal struct Constants
{
    
    internal struct Storyboard {
        internal static let SideMenu = "sidemenuViewid",
                            MainNavigationBar = "mainNavid",
                            Main = "mainviewId",
                            AwardList = "awardlistviewdid",
                            AwardListDetails = "awardlistdetailsviewid",
                            AwardListImages = "awardlistimagesviewid",
                            AwardListImagesDetails = "awardlistimagesdeatailsid",
                            AwardVideos = "awardvideosviewid",
                            AwardVideosDetails = "awardvideosdetalisviewid",
                            Statistics = "statisticsviewid",
                            StatisticsDetails = "statisticsdetailsviewid",
                            BoardOfTrustees = "BoardOfTrusteesviewid",
                            BoardOfAwardManagement = "boardOfAwardManagmentviewid",
                            AboutApp = "aboutappviewid",
                            BarcodeCompetition = "barcodeCompetitionviewid",
                            Definitions = "defintionsviewid",
                            SchoolsPrizes = "shcoolsprizesviewid",
                            AnnualReports = "annualReportsViewid",
                            PDFViewer = "openpdflinkviewid",
                            FirstStage = "firstStageviewid",
                            SecondStage = "secondstageviewid",
                            Splash = "customsplashviewid",
                            Reveal = "revealviewid"
    }
    
    internal struct Nib {
        internal static let Menu = UINib(nibName: "MenuCell", bundle: nil),
                            Main = UINib(nibName: "MainCell", bundle: nil),
                            AwardImagesDetails = UINib(nibName: "AwardImagesDetailsCell", bundle: nil),
                            AwardVideos = UINib(nibName: "AwardVideosDetailsCell", bundle: nil),
                            Statistics = UINib(nibName: "StatisticsCell", bundle: nil),
                            StatisticsDetails = UINib(nibName: "StatisticsDetailsCell", bundle: nil),
                            Member = UINib(nibName: "MemberCell", bundle: nil),
                            Schools = UINib(nibName: "SchoolCell", bundle: nil),
                            Keeper = UINib(nibName: "KeeperCell", bundle: nil)
    }
    
    internal struct ReuseIdentifier {
        internal static let Menu = "sidemenucellid",
                            Main = "maincellviewid",
                            AwardList = "awardlistcellviewId",
                            AwardListImages = "awardimagescellviewid",
                            AwardImagesDetails = "awardimagesDetailscellviewid",
                            AwardVideos = "awardvideoscellviewid",
                            Statistics = "statisticscellviewid",
                            StatisticsDetails = "statisticsDetailsViewcellid",
                            Member = "membercellviewid",
                            PrizeManagement = "prizeManagementcellviewid",
                            Schools = "schoolsprizeCellviewid",
                            AnnualReport = "annualreportCellviewid",
                            Keeper = "keeperviewcellid"
    }
    
    internal struct Misc {
        internal static let AppLink = "", ErrorMessage = "errormessage".localized()
    }
    
    internal struct Language {
        internal static let Arabic = "ar-EG",
                            English = "en"
    }
    
    
    internal class BackendURLs {
        internal class var shared: BackendURLs {
            struct Object {
                internal static let object = BackendURLs()
            }
            return Object.object
        }
        internal var localizeUrl: string {
            return (Localize.currentLanguage() == Constants.Language.Arabic) ? "JSON" : "JSON_EN"
        }
        private func baseUrl()-> string {
            return "http://192.232.214.91/~rawdatsudeer/\(localizeUrl)/"
        }
        
        private func baseUrlTail()-> string {
            return ".php?key=RAWDAT_SUDAIR"
        }
        
        internal var Vision: string  { return returnLink(by: "vision") }
        internal var Message: string { return returnLink(by: "message") }
        internal var Goals: string { return returnLink(by: "goals") }
        internal var Prin: string { return returnLink(by: "prin") }
        internal var AboutApp: string { return returnLink(by: "about_us") }
        internal var Members: string { return returnLink(by: "cou_mem") }
        internal var BoardOfTrutees: string { return returnLink(by: "b_tr") }
        internal var Albumes: string { return returnLink(by: "album") }
        internal var Videos: string { return returnLink(by: "videos") }
        internal var AboutCompetition: string { return returnLink(by: "about_bar") }
        internal var Schools: string { return returnLink(by: "bar_code") }
        internal var AnnualReport: string { return returnLink(by: "report") }
        internal var DateTime: string { return returnLink(by: "date_h") }
        
        private func returnLink(by name: string)-> string {
            return "\(baseUrl())\(name)\(baseUrlTail())"
        }
        
        internal func awardListImagesLink(by id: string)-> string {
            return "\(baseUrl())all_images_album.php?get_id=\(id)&key=RAWDAT_SUDAIR"
        }
        
        internal func schoolImagesLink(by id: string)-> string{
            return "\(baseUrl())all_images_bar.php?get_id=\(id)&key=RAWDAT_SUDAIR"
        }
    }
    
    
}

