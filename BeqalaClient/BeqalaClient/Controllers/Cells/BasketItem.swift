
import Foundation
import UIKit


internal final class BasketItem: BaseGridCell
{
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var counter = 1
    internal var product: Product? { didSet { updateUi() } }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var mainPanel: View!
    @IBOutlet internal weak var btnRemoveItem: UIButton!
    @IBOutlet private weak var cover: UIImageView!
    @IBOutlet private weak var lblProductName: UILabel!
    @IBOutlet private weak var lblProductPrice: UILabel!
    @IBOutlet private weak var footerPanel: UIView!
    @IBOutlet private weak var seperator: UIView!
    @IBOutlet private weak var counterPanel: View!
    @IBOutlet private weak var lblCounter: UILabel!
    @IBOutlet private weak var btnDecrease: UIButton!
    @IBOutlet private weak var btnIncrease: UIButton!
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi()-> void {
        if (Localizer.instance.current == .arabic) {
            lblProductName.textAlignment = .right
            lblProductPrice.textAlignment = .right
            btnRemoveItem.rightAnchor.constraint(equalTo: mainPanel.rightAnchor, constant: 0).isActive = true
            btnDecrease.rightAnchor.constraint(equalTo: seperator.rightAnchor, constant: -10).isActive = true
            btnIncrease.leftAnchor.constraint(equalTo: seperator.leftAnchor, constant: 10).isActive = true
        } else {
            lblProductName.textAlignment = .left
            lblProductPrice.textAlignment = .left
            btnRemoveItem.leftAnchor.constraint(equalTo: mainPanel.leftAnchor, constant: 0).isActive = true
            btnDecrease.leftAnchor.constraint(equalTo: seperator.leftAnchor, constant: 10).isActive = true
            btnIncrease.rightAnchor.constraint(equalTo: seperator.rightAnchor, constant: -10).isActive = true
        }
        lblProductName.text = product?.title ?? .empty
        lblProductPrice.text = "\(product?.price ?? .empty) \(Localization.SR.localized())"
        if let imageLink = product?.photo1 {
            cover.loadImage(from: imageLink)
        }
    }
    
    
    // MARK: - Actions
    
    @IBAction private func decreaseCountOnCLick(_ sender: UIButton) {
        guard counter > 1 else { return }
        counter -= 1
        lblCounter.text = counter.toString
        NotificationCenter.default.post(name: .CalculatePriceKey, object: nil)
    }
    
    @IBAction private func increaseCountOnCLick(_ sender: UIButton) {
        counter += 1
        lblCounter.text = counter.toString
        NotificationCenter.default.post(name: .CalculatePriceKey, object: nil)
    }

    
}






































