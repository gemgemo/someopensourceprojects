
import UIKit
import Localize_Swift

final class AwardList: BaseController
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    fileprivate var dataSource = Array<AwardListItem>() {
        didSet {
            tblAwardList.reloadData()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var tblAwardList: UITableView! {
        didSet {
            tblAwardList.delegate = self
            tblAwardList.dataSource = self
        }
    }
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)        
        dataSource = AwardListItem.getItems()
        updateNavigationBarDirections()
        updateNavigationBar()
    }
    
    //MARK: - Actions
    

}


// MARK: - Helper Functions

extension AwardList
{
    
    fileprivate func updateNavigationBar()-> void {
        navigationController?.navigationBar.castShadow = "set shadow"
        navigationItem.title = "awardlistlan".localized()        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: getBackImage(), style: .plain, target: self, action: #selector(goBack(_:)))
    }
    
}


// MARK: - Table view delegate & data source functions 

extension AwardList: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.AwardList) as? AwardListCell {
            cell.item = dataSource[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
        if let awardInfoScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.AwardListDetails) as? AwardListDetails {
            awardInfoScreen.row = dataSource[indexPath.row]
            navigationController?.pushViewController(awardInfoScreen, animated: true)
        }
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}


























