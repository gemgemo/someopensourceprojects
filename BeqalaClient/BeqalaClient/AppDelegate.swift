
import UIKit
import GoogleMaps
import UserNotifications
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{

    var window: UIWindow?, productId: string?
    fileprivate var isNewOrder = false

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        if (UserDefaults.standard.bool(forKey: Constants.Misc.IsLoggedIn)) {
            AppManager.manage.sideMenu(for: window)
        }
        AppManager.manage.setupNavigationBar()
        DispatchQueue.global(qos: .userInitiated).async {
            GMSServices.provideAPIKey(Constants.Misc.MapKey)
        }
        FIRApp.configure()
        ThreadQueue.delay(after: 0.3) {
            print("delay splash")
        }
        return true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return .portrait
    }
    
    
    // MARK: - remote notifications
    
    @objc private func refreshTokenDevice(_ notification: Notification)-> void {
        //print("deive tokn \(FIRInstanceID.instanceID().token() ?? .empty) refreshed")
        if let token = FIRInstanceID.instanceID().token() {
            let parameters: Dictionary<string, any> = [
                Constants.Misc.MainKey: Constants.Misc.MainValue,
                "user_id": UserDefaults.standard.string(forKey: Constants.Misc.UserID) ?? .empty,
                "d_token": token,
                "type": 1
            ]
            print("remote notifications paramters: \(string(describing: parameters))")
            HttpClient.request(link: Constants.Urls.DeviceToken, method: .post, parameters: parameters) { (data, response, error) -> void in
                if (error != nil) {
                    print("send device token error \(string(describing: error))")
                    return
                }
                print("send device token data \(string(describing: data))")
            }
        }
        connectToFcm()
    }

    
    func applicationDidBecomeActive(_ application: UIApplication) {
        connectToFcm()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        /*print("device token data \(deviceToken)")
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print(deviceTokenString)*/
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: .sandbox)
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: .prod)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenDevice(_:)), name: .firInstanceIDTokenRefresh, object: nil)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("remote notification register error \(error)")
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(#function, userInfo)
        UNUserNotificationCenter.current().delegate = self
        productId = userInfo["gcm.notification.id_product"] as? string
        print("product id =======>", productId ?? .empty)
        var title = string.empty
        var body = string.empty
        if (application.applicationState == .active) {
            if (productId == nil || productId == .empty) {
                isNewOrder = true
                /*if (Localizer.instance.current == .arabic) {
                    title = "بقالة"
                    body = "تم قبول طلبك"
                } else {
                    title = "Grocery"
                    body = "Your request has been accepted."
                }
                Gemo.gem.showAlert(withTitle: title, message: body, in: window!)*/
            } else {
                isNewOrder = false
                let aps = userInfo["aps"] as? Dictionary<string, any>
                let alert = aps?["alert"] as? Dictionary<string, any>
                title = alert?["title"] as? string ?? .empty
                body = alert?["body"] as? string ?? .empty
                /*let alertController = UIAlertController(title: title, message: body, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: Localization.Submit.localized(), style: .default, handler: { (action) in
                    NotificationCenter.default.post(name: .OpenProductDetails, object: nil)
                }))
                alertController.addAction(UIAlertAction(title: (Localizer.instance.current == .arabic) ? "غلق" : "Close", style: .cancel))
                window?.rootViewController?.present(alertController, animated: true)*/
            }
            print("foreground notifications-=-=-=---=-=-=-=-=-=-=-=-=")
        } else if (application.applicationState == .inactive){
            print("inactive notifications-=-=-=---=-=-=-=-=-=-=-=-=")
        } else {
            print("background notifications-=-=-=---=-=-=-=-=-=-=-=-=")
        }
        completionHandler(.newData)
    }
    
    fileprivate func connectToFcm()-> void {
        guard FIRInstanceID.instanceID().token() != nil else { return }
        FIRMessaging.messaging().disconnect()
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
}


// MARK: - User notifications delegate and firebase messageing functions

extension AppDelegate: UNUserNotificationCenterDelegate, FIRMessagingDelegate
{
    
    internal func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("ios 10 user notification received--------------tapped on it------->>>>>>>>.")
        (isNewOrder) ? NotificationCenter.default.post(name: .OpenCurrentOrders, object: nil) : NotificationCenter.default.post(name: .OpenProductDetails, object: nil)
        completionHandler()
    }
    
    internal func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("ios 10 notification received in foreground", notification.request.content.userInfo)
        completionHandler([.alert, .sound])
    }
    
    internal func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        print("message data: \(remoteMessage.appData)")
    }
    
}





























