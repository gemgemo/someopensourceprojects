
import Foundation
import UIKit

import Localize_Swift
import MapKit
import AVFoundation
import QuickLook


// MARK: - View controller

extension UIViewController {
    
    internal func handle(this error: Error?)-> void {
        print(error?.localizedDescription ?? string.nixStringMessage)
    }
    
    internal func dialog(with message: string)-> void {
        print(message)
        (self as? BaseController)?.showAlert(with: "Warning!", and: message)
    }
    
    internal func changeLanguage()-> void {
        let actionSheet = UIAlertController(title: "langActionSheetTitle".localized(), message: "langActionSheetMessgae".localized(), preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "عربي", style: .default, handler: { (action) in
            Localize.setCurrentLanguage(Constants.Misc.ArabicSign)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "English", style: .default, handler: { (action) in
            Localize.setCurrentLanguage(Constants.Misc.EnglishSign)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "canceltionTitle".localized(), style: .cancel, handler: nil))
        actionSheet.popoverPresentationController?.sourceRect = CGRect(x: view.bounds.width/2, y: view.bounds.height/2, width: 0, height: 0)
        actionSheet.popoverPresentationController?.sourceView = view
        present(actionSheet, animated: true, completion: nil)
    }
    
    @objc internal func goBack(_ sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    internal func share(this items: [any])-> void {
        let shareActivity = UIActivityViewController(activityItems: items, applicationActivities: nil)
        shareActivity.excludedActivityTypes = [.mail, .message, .copyToPasteboard, .postToFacebook, .postToFlickr, .postToTencentWeibo, .postToTwitter, .print]
        shareActivity.popoverPresentationController?.sourceView = view
        shareActivity.popoverPresentationController?.sourceRect = Rect(x: 0, y: 0, width: 0, height: 0)
        present(shareActivity, animated: true, completion: nil)
    }
    
    
    
   
}



// MARK: - Image view

extension UIImageView {
    
    internal func round()-> void {
        layer.cornerRadius = bounds.height/2
        clipsToBounds = true
        layer.borderColor = Color(white: 0.88, alpha: 0.8).cgColor
        layer.borderWidth = 1.0
    }
    
}


// MARK: - Navigation Bar

extension UINavigationBar {
    
    var castShadow : String {
        get { return "anything fake" }
        set {
            self.layer.shadowOffset = Size(width: 0, height: 1.5)
            self.layer.shadowRadius = 1.0
            self.layer.shadowColor = UIColor.gray.cgColor
            self.layer.shadowOpacity = 0.4
            
        }
    }
    
}



// MARK: - Colors

extension Color {
    
    internal static var normal: Color {
        return Color(red:0.953, green:0.953, blue:0.953, alpha:1)
    }
    
    internal static var selected: Color {
        return Color(red:0.325, green:0.596, blue:0.871, alpha:1)
    }
}


// MARK: - Map view

extension MKMapView {
    
    internal func setupCamera()-> void {
        camera.altitude = 1400
        camera.pitch = 50
        camera.heading = 180
    }
    
    internal func setAnnotation(in point: CLLocationCoordinate2D, with title: string, subTitle: string)-> void {
        let annotation = MKPointAnnotation()
        annotation.coordinate = point
        annotation.title = title
        annotation.subtitle = subTitle
        addAnnotation(annotation)
    }
    
    internal func setRegion(in point: CLLocationCoordinate2D)-> void {
        guard (point.latitude < 90.0 && point.latitude > -90.0) else {
            print("error in latitude")
            return
        }
        
        guard (point.longitude < 180.0 && point.longitude > -180.0) else {
            print("error in longitude")
            return
        }
            
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let region = MKCoordinateRegion(center: point, span: span)
        setRegion(region, animated: true)                
    }
    
    internal func makeLocationCoordinate(latitude: string, longitude: string)-> CLLocationCoordinate2D {
        let formatter = NumberFormatter()
        let lati = formatter.number(from: latitude)?.doubleValue ?? Number.zero.doubleValue
        let longi = formatter.number(from: longitude)?.doubleValue ?? Number.zero.doubleValue
        return CLLocationCoordinate2D(latitude: lati, longitude: longi)
    }
}



// MARK: - Application

extension UIApplication {
    
    internal class func open(this link: string)-> void {
        guard let url = URL(string: link), UIApplication.shared.canOpenURL(url) else {
            print("can't open link")            
            return
        }
        UIApplication.shared.openURL(url)
    }
    
    internal class func generateThumb(fromVideo link: string, onComplete: @escaping (UIImage)-> void)-> void {
        UIApplication.showIndicator(by: true)
        DispatchQueue.global(qos: .userInitiated).async {
            guard let url = URL(string: link) else {
                UIApplication.showIndicator(by: false)
                return
            }
            let asset = AVAsset(url: url)
            let generator = AVAssetImageGenerator(asset: asset)
            generator.appliesPreferredTrackTransform = true
            var time = asset.duration
            time.value = min(time.value, 1)
            do {
                let imageRef = try generator.copyCGImage(at: time, actualTime: nil)
                onComplete(UIImage(cgImage: imageRef))
            } catch {
                print(error.localizedDescription)
                UIApplication.showIndicator(by: false)
            }
        }
    }
    
    
}

extension string {
    
    internal static var empty: string {
        return ""
    }
    
    internal static var nixStringMessage: string {
        return "returns none data"
    }
    
    internal var isLink: Bool {
        return contains("http://") || contains("https://")
    }
    
    internal var isEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
}


public extension Number {
    
    internal static var zero: Number {
        return 0
    }
    
}



// image caching
private let imageCache = NSCache<NSString,UIImage>()
public extension UIImageView {
    
    internal func loadImage(from link: string, onComplete: ((_ isLoaded: Bool)-> void)?)-> void {
        UIApplication.showIndicator(by: true)
        image = nil
        if (!link.isLink || link.isEmpty) {
            print("empty link or invalid link")
            onComplete?(false)
            return
        }
        
        if let cachedImage = imageCache.object(forKey: link as NSString) {
            image = cachedImage
            onComplete?(true)
            UIApplication.showIndicator(by: false)
            return
        }
        // download and cache image
        // download
        if let url = URL(string: link as string) {
            URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
                if (error != nil) {
                    onComplete?(false)
                    UIApplication.showIndicator(by: false)
                    return
                }
                if let imageData = data, let imageToCache = UIImage(data: imageData) {
                    DispatchQueue.main.async { [weak self] in
                        // cache image
                        imageCache.setObject(imageToCache, forKey: link as NSString)
                        self?.image = imageToCache
                        onComplete?(true)
                        UIApplication.showIndicator(by: false)
                    }
                } else {
                    onComplete?(false)
                    UIApplication.showIndicator(by: false)
                }
                }.resume()
        }
    }
    
}



 extension UIApplication {
    
    internal class func showIndicator(by isOn: bool)-> void {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = isOn
        }
    }
    
    internal class func playActions()-> void {
        if (UIApplication.shared.isIgnoringInteractionEvents) {
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
    
    internal class func pauseActions()-> void {
        if (!UIApplication.shared.isIgnoringInteractionEvents) {
            UIApplication.shared.beginIgnoringInteractionEvents()
        }
    }
    
}

// uicolor

 extension UIColor {
    
    internal class func rgb(red: Int, green: Int, blue: Int, alpha: Float)-> UIColor {
        return UIColor(colorLiteralRed: Float(red)/255, green: Float(green)/255, blue: Float(blue)/255, alpha: alpha)
    }
    
    internal convenience init?(hex hash: string) {
        let r, g, b, a: CGFloat
        if (hash.hasPrefix("#")) {
            let start = hash.index(hash.startIndex, offsetBy: 1)
            let hexColor = hash.substring(from: start)
            let scanner = Scanner(string: hexColor)
            var hexNumber: UInt64 = 0
            if (scanner.scanHexInt64(&hexNumber)) {
                r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                a = CGFloat(hexNumber & 0x000000ff) / 255
                
                self.init(red: r, green: g, blue: b, alpha: a)
                return
            }
        }
        return nil
    }
    
    
    
}


// uiview

 extension UIView {
    
    internal func setShadow(with offset: CGSize, radius: CGFloat, opacity: CGFloat, color: Color)-> Void {
        layer.shadowColor = color.cgColor
        layer.shadowOffset = offset
        layer.shadowRadius = radius
        layer.shadowOpacity = Float(opacity)
    }
    
}













