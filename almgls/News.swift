//
//  News.swift
//  almgls
//
//  Created by User on 12/27/16.
//  Copyright © 2016 Mostafa. All rights reserved.
//

import UIKit

class News: UICollectionViewCell {
    
    @IBOutlet weak var newImage: UIImageView!
    @IBOutlet weak var title: UILabel!
}
