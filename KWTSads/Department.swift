//
//  Department.swift
//  KWTSads
//
//  Created by Mostafa on 2/14/17.
//  Copyright © 2017 Mostafa. All rights reserved.
//

import Foundation
public class Department{
    var img_adv: String!
    var img_adv2: String!
    var adv_name: String!
    var tel: String!
    var mobile: String!
    var category = [Category]()
    
    init(img_adv: String,img_adv2: String,adv_name: String,tel: String,mobile: String,category: [Category]) {
        self.img_adv = img_adv
        self.img_adv2 = img_adv2
        self.adv_name = adv_name
        self.tel = tel
        self.mobile = mobile
        self.category = category
    }
}
