
import Foundation
import Gemo
import CoreLocation

class Site: Mapper<Any> {
    
    internal var id, name, latitude, longitude, address, distance: String?
    
    
    override func mapping(mapper: Mapper<Any>) {
        id = mapper["id"]?.string
        name = mapper["name"]?.string
        latitude = mapper["lati"]?.string
        longitude = mapper["longi"]?.string
        address = mapper["address"]?.string
        distance = mapper["km"]?.string
    }
    
    internal class func fetchStationsLocations(by location: CLLocationCoordinate2D, _ page: BaseController?, _ completion: @escaping([Site])->())-> void {
        Gemo.gem.startSpinning()
        let paramters: [string: any] = [
            Misc.MainKey: Misc.MainValue,
            Misc.LanguageKey: Localizer.instance.current.rawValue,
            "latitude": location.latitude,
            "longitude": location.longitude
        ]
        Http.request(link: Misc.getUrl(for: "side"), method: .post, parameters: paramters)//side
            .response { (response) in
                let error = response.error
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("\(#function) error: \(error!)")
                    page?.displayAlert(with: error?.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
                guard let json = response.result as? Array<[string: any]> else {
                    page?.displayAlert(with: Localization.NoData.localized())
                    return
                }
                completion(json.map { Site(JSON: $0) })
        }
    }
    
}



































