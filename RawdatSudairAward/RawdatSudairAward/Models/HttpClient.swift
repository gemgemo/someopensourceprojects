
import Foundation

final class HttpClient
{
    
    private static let session = URLSession.shared
    
    enum Method: string {
        case get = "GET", post = "POST", put = "PUT", delete = "DELETE"
    }
    
    internal class func request(link: string, method: Method, onComplete: @escaping(_ JSON: any?, _ response: URLResponse?, _ error: Error?)-> void)-> void {
        if (link.isEmpty || !link.isLink) {
            onComplete(nil, nil, NSError(domain: "Invalid Link", code: 1221, userInfo: nil))
            return
        }
        guard let url = URL(string: link) else {
            onComplete(nil, nil, NSError(domain: "Invalid URL", code: 2112, userInfo: nil))
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        session.dataTask(with: request) { (data, response, error) in
            if (error != nil) {
                onComplete(nil, nil, error)
                return
            }
            guard let jsonData = data else {
                onComplete(nil, nil, NSError(domain: "Not found data!", code: 3223, userInfo: nil))
                return
            }
            do {
                onComplete(try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments), response, nil)
            } catch let catchError {
                print("the error here")
                onComplete(nil, nil, catchError)
            }
            
        }.resume()
    }
}












