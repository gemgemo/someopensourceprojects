
import Foundation

internal final class Product: Mapper<Any>
{
    
    internal var id, sectionId, title, price, quantity, photo1, photo2, photo3, photo4: string?
    
    internal override var description: String {
        return "\n id: \(string(describing: id)), title: \(string(describing: title)) \n"
    }
    
    
    internal override func mapping(mapper: Mapper<Any>) {
        id = mapper["id"]?.text
        sectionId = mapper["id_cat"]?.text
        title = mapper["title"]?.text
        price = mapper["price"]?.text
        quantity = mapper["quantity"]?.text
        photo1 = mapper["photo_1"]?.text
        photo2 = mapper["photo_2"]?.text
        photo3 = mapper["photo_3"]?.text
        photo4 = mapper["photo_4"]?.text
    }
    
    
    internal class func getData(by id: string, page: BaseController, _ completion: @escaping([Product])->())-> void {
        Gemo.gem.startSpinning(in: page.view)
        let parameters: [string: string] = [
            Constants.Misc.MainKey: Constants.Misc.MainValue,
            "id_cat": id
        ]
        HttpClient.request(link: Constants.Urls.ProductsPerSection, method: .post, parameters: parameters) { (data, response, error) -> void in
            if (error != nil) {
                Gemo.gem.stopSpinning()
                print("fetch products error \(string(describing: error))")
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription, in: page.view)
                return
            }
            guard let json = data as? Dictionary<string, any>, let value = json["all_data"] as? [Dictionary<string, any>] else {
                Gemo.gem.stopSpinning()
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized(), in: page.view)
                return
            }
            completion(value.map { Product(JSON: $0) })
        }
    }
    
    
    internal class func getData(from link: string, with parameters: [string: any], in page: BaseController, _ completion: @escaping([Product])->())-> void {
        Gemo.gem.startSpinning(in: page.view)
        HttpClient.request(link: link, method: .post, parameters: parameters) { (data, response, error) -> void in
            if (error != nil) {
                Gemo.gem.stopSpinning()
                print("fetch products error \(string(describing: error))")
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription, in: page.view)
                return
            }
            print("data \(string(describing: data))")
            guard let json = data as? Dictionary<string, any>, let value = json["all_data"] as? [Dictionary<string, any>] else {
                Gemo.gem.stopSpinning()
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized(), in: page.view)
                return
            }
            completion(value.map { Product(JSON: $0) })
        }
    }
    
    
    internal class func removeItem(from link: string, with parameters: Dictionary<string, any>, in page: BaseController, _ completion: @escaping()->())-> void {
        Gemo.gem.startSpinning(in: page.view)
        HttpClient.request(link: link, method: .post, parameters: parameters) { (data, response, error) -> void in
            if (error != nil) {
                Gemo.gem.stopSpinning()
                print("fetch products error \(string(describing: error))")
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription, in: page.view)
                return
            }
            print("data \(string(describing: data))")
            guard let json = (data as? [Dictionary<string, any>])?.first, let success = json["result"] as? string, let arabicMessage = json["data"] as? string, let englishMessage = json["data_en"] as? string else {
                Gemo.gem.stopSpinning()
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized(), in: page.view)
                return
            }
            Gemo.gem.stopSpinning()
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: (Localizer.instance.current == .arabic) ? arabicMessage : englishMessage, in: page.view)
            if (success == "true") {
                completion()
            }
        }
    }
    
    
}


































