
import UIKit
import Localize_Swift

final class AwardListDetails: BaseController
{
    // MARK: - Constants
    
    //MARK: - Variables
    
    internal var row: AwardListItem?
    
    // MARK: - Outlets
    
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var lblDetails: UITextView! {
        didSet {
            lblDetails.text = string.empty
            lblDetails.alwaysBounceVertical = true
        }
    }
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBar()
        updateNavigationBarDirections()
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            lblTitle.textAlignment = .right
            lblDetails.textAlignment = .right
        } else {
            lblTitle.textAlignment = .left
            lblDetails.textAlignment = .left
        }
    }
    
    //MARK: - Actions
    
    @objc fileprivate func gotoHome(_ sender: UIBarButtonItem) {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
}




// MARK: - Helper functions 

extension AwardListDetails
{
    
    fileprivate func updateNavigationBar()-> void {
        navigationController?.navigationBar.castShadow = "set shadow"
        navigationItem.title = row?.title ?? string.empty
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: getBackImage(), style: .plain, target: self, action: #selector(goBack(_:)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "home"), style: .plain, target: self, action: #selector(gotoHome(_:)))
        
    }
    
    fileprivate func loadData()-> void {
        startSpin()
        guard let id = row?.id else {
            showAlert(with: string.empty, and: Constants.Misc.ErrorMessage)
            stopSpin()            
            return
        }
        
        AwardDetails.getData(by: getLink(by: id), viewController: self) { [weak self] (award) -> void in
            guard let this = self else { return }
            if let content = award.content {
                DispatchQueue.main.async { [weak self] in
                    self?.lblDetails.text = content.trimmingCharacters(in: .whitespaces)
                }
            }
            this.stopSpin()
        }
    }
    
    private func getLink(by id: int)-> string {
        switch (id) {
        case 1:
            return Constants.BackendURLs.shared.Vision
            
        case 2:
            return Constants.BackendURLs.shared.Message
            
        case 3:
            return Constants.BackendURLs.shared.Goals
            
        case 4:
            return Constants.BackendURLs.shared.Prin
            
        default:
            return string.empty
        }

    }
    
}



























