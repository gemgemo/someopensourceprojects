
import UIKit
import Alamofire
import UserNotifications
import Firebase

final class Login: BaseController
{
    // MARK: - Constants
    
    // MARK: - Variables
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var txtUserName: TextField!
    @IBOutlet fileprivate weak var txtPassword: TextField!
    @IBOutlet fileprivate weak var btnLogin: Button!
    @IBOutlet fileprivate weak var btnRegister: Button!
    @IBOutlet fileprivate weak var btnReset: UIButton!
    @IBOutlet fileprivate weak var loginBox: Shape!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        updateNavigationBar()
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupTextFields(textFileds: txtUserName, txtPassword)
        updateUi()
    }
    
    // MARK: - Actions
    
    @IBAction private func loginOnClick(_ sender: UIButton) {
        startSpin()
        guard !userName.isEmpty, !password.isEmpty else {
            showAlert(with: string.empty, and: "invalidemaillan".localized())
            stopSpin()
            return
        }
        login(by: userName, with: password) { [weak self] (isSuccess, message, userID) -> void in
            if (isSuccess == "True") {
                let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                if let rootView = mainStoryboard.instantiateInitialViewController() as? SWRevealViewController {
                    UserDefaults.standard.set(true, forKey: Constants.Misc.UserLoggedIn)
                    UserDefaults.standard.set(userID, forKey: Constants.Misc.UserId)
                    (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController = rootView
                    self?.configureRemoteNotifications(in: UIApplication.shared)
                }
            } else {
                self?.showAlert(with: string.empty, and: message)
            }
            self?.stopSpin()
        }
    }
    
    @IBAction private func registerOnClick(_ sender: UIButton) {
        if let registerScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Register) as? Register {
            navigationController?.pushViewController(registerScreen, animated: true)
        }
    }
    
    @IBAction private func resetPasswordOnClick(_ sender: UIButton) {
        startSpin()
        let alertController = UIAlertController(title: "sendpasswordlan".localized(), message: "resetpasswordlan".localized(), preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.placeholder = "profileemailplacholderlan".localized()
            textField.keyboardType = .emailAddress
        }
        alertController.addAction(UIAlertAction(title: "okLan".localized(), style: .default, handler: { [weak self] (action) in
            //print("send password")
            let email = alertController.textFields?.first?.text?.trimmingCharacters(in: .whitespaces) ?? string.empty
            guard !email.isEmpty, email.isEmail else {
                self?.showAlert(with: string.empty, and: "invalidemaillan".localized())
                self?.stopSpin()
                return
            }
            self?.restPassword(by: email, onComplete: { (isSuccess, message) -> void in
                self?.showAlert(with: string.empty, and: "resendpasswordmessagelan".localized())
                self?.stopSpin()
            })
        }))
        alertController.addAction(UIAlertAction(title: "cancellan".localized(), style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    @objc fileprivate func refreshTokenDevice(_ notification: Notification)-> void {
        if let token = FIRInstanceID.instanceID().token() {
            let parametres = [
                "ID" : UserDefaults.standard.value(forKey: Constants.Misc.UserId) ?? string.empty,
                "devicetype" : 1,
                "devicetoken" : token
            ]
            Alamofire.request(Constants.BackendURLs.DeviceToken, method: .post, parameters: parametres)
                .responseJSON { (response) in
                    let result = response.result
                    if (result.error != nil) {
                        print("send device token error: \(String(describing: result.error))")
                        return
                    }
                    print("device token sent response: \(String(describing: result.value))")
            }
        }
        connectToFcm()
    }

    
}




// MARK: - Helper functions

extension Login
{
    
    fileprivate func updateUi()-> void {
        let langCode = Locale.current.languageCode ?? "en"
        txtPassword.placeholder = "passwordlan".localized()
        txtUserName.placeholder = "UserNamelan".localized()
        btnLogin.setTitle("loginbuttontitlelaninbutton".localized(), for: .normal)
        btnRegister.setTitle("registernewlan".localized(), for: .normal)
        btnReset.setTitle("resetpasswordlan".localized(), for: .normal)
        if (langCode == Constants.Language.English) { // english
            txtPassword.textAlignment = .left
            txtUserName.textAlignment = .left
            txtUserName.semanticContentAttribute = .forceRightToLeft
            txtPassword.semanticContentAttribute = .forceRightToLeft
        } else { // arabic
            txtPassword.textAlignment = .right
            txtUserName.textAlignment = .right
            txtUserName.semanticContentAttribute = .forceLeftToRight
            txtPassword.semanticContentAttribute = .forceLeftToRight
        }
    }
    
    fileprivate func connectToFcm() {
        // Won't connect since there is no token
        guard FIRInstanceID.instanceID().token() != nil else {
            return
        }
        FIRMessaging.messaging().disconnect()
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    fileprivate func configureRemoteNotifications(in application: UIApplication)-> void {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { (_, error) in
                if (error != nil) {
                    print("ios 10 notifications error \(String(describing: error))")
                    return
                }
                UNUserNotificationCenter.current().delegate = self
                FIRMessaging.messaging().remoteMessageDelegate = self
            }
        } else {
            application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert, .sound], categories: nil))
        }
        application.registerForRemoteNotifications()
        NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenDevice(_:)), name: .firInstanceIDTokenRefresh, object: nil)
    }

    
    fileprivate func updateNavigationBar()-> void {
        UIApplication.shared.isStatusBarHidden = false
        navigationItem.title = "registerlan".localized()
    }
    
    fileprivate func setupTextFields(textFileds: UITextField...)-> void {
        textFileds.forEach { $0.delegate = self }
    }
    
    fileprivate func login(by name: string , with password: string, oncomplete: @escaping(string, string, string)-> void)-> void {
        Alamofire.request(Constants.BackendURLs.Login, method: .post, parameters: ["username": name, "password": password])
            .responseJSON { [weak self] (response) in
                let result = response.result
                if (result.error != nil) {
                    print("login error is: \(result.error)")
                    self?.showAlert(with: string.empty, and: "erroralertlan".localized())
                    self?.stopSpin()
                    return
                }
                //print("user login value: \(result.value)")
                if let value = result.value as? [string: any], let success = value["Success"] as? string, let alertMessage = value["Massage"] as? string, let userId = value["ID"] as? string {
                    oncomplete(success, alertMessage, userId)
                } else {
                    print("null value when user login")
                    self?.showAlert(with: string.empty, and: "usernotfoundlan".localized())
                    self?.stopSpin()
                }
        }
    }
    
    fileprivate func restPassword(by email: string, onComplete: @escaping(string, string)-> void)-> void {
        Alamofire.request(Constants.BackendURLs.ResetPassword, method: .post, parameters: ["email": email])
            .responseJSON { [weak self] (response) in
                let result = response.result
                if (result.error != nil) {
                    print("reset password error is: \(result.error)")
                    self?.showAlert(with: string.empty, and: "erroralertlan".localized())
                    self?.stopSpin()
                    return
                }
                if let value = result.value as? [string: any], let success = value["Success"] as? string, let message = value["Massage"] as? string {
                    onComplete(success, message)
                } else {
                    print("none value when reset password")
                }
        }
    }
    
}

// MARK: - Computed properties

extension Login
{
    
    fileprivate var userName: string {
        return txtUserName.text?.trimmingCharacters(in: .whitespaces) ?? string.empty
    }
    
    fileprivate var password: string {
        return txtPassword.text?.trimmingCharacters(in: .whitespaces) ?? string.empty
    }
    
}


// MARK: - Text Field delegate functions

extension Login: UITextFieldDelegate
{
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}




extension Login: UNUserNotificationCenterDelegate, FIRMessagingDelegate
{
    
    @available(iOS 10.0, *)
    internal func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("ios 10 user notification recevied")
        completionHandler()
    }
    
    @available(iOS 10.0, *)
    internal func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("ios 10 notification preseted in foreground", notification.request.content.userInfo)
        completionHandler([.alert])
    }
    
    internal func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        print("message data: \(remoteMessage.appData)")
    }
    
}




















