
import UIKit


final class SectionDetails: BaseController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var dataSource = Array<Product>() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.collectionView.reloadData()
            }
        }
    }
    internal var section: Section?
    
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var titlePanal: UIView! {
        didSet {
            shadow(for: titlePanal)
        }
    }
    @IBOutlet fileprivate weak var collectionView: UICollectionView! {
        didSet {
            collectionView.delegate = self
            collectionView.dataSource = self
        }
    }
    @IBOutlet fileprivate weak var btnBack: UIButton!
    @IBOutlet fileprivate weak var btnSearch: UIButton!
    @IBOutlet fileprivate weak var btnBasket: UIButton!
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()        
        if (Gemo.gem.isConnected) {
            loadData()
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
        
    }
    
    
    // MARK: - Actions
    
    @IBAction private func basketOnCLick(_ sender: UIButton) {
        navigateToBasket()
    }
    
    @IBAction private func searchOnCLick(_ sender: UIButton) {
        navigateToSearch()
    }
    
    @IBAction private func backOnCLick(_ sender: UIButton) {
        back()
    }
    
    @objc fileprivate func addToBasketClicked(_ sender: UIButton) {
        guard let userId = UserDefaults.standard.value(forKey: Constants.Misc.UserID) as? string, let productId = dataSource[sender.tag].id else { return }
        add(to: Constants.Urls.Basket, forUserId: userId, andProductId: productId)
    }
    
    @objc fileprivate func addToFavoritClicked(_ sender: UIButton) {
        guard let userId = UserDefaults.standard.value(forKey: Constants.Misc.UserID) as? string, let productId = dataSource[sender.tag].id else { return }
        print("add to fav user id \(userId), product id \(productId)")
        add(to: Constants.Urls.Favorite, forUserId: userId, andProductId: productId)
    }

}


// MARK: - Helper Functions

extension SectionDetails
{
    
    fileprivate func updateUi()-> void {
        view.bringSubview(toFront: titlePanal)
        tabBar.isHidden = true
        lblTitle.text = section?.title ?? .empty
        btnBack.transform = CGAffineTransform.identity
        if (Localizer.instance.current == .arabic) {
            btnBack.leftAnchor.constraint(equalTo: titlePanal.leftAnchor, constant: 8).isActive = true
            btnBasket.rightAnchor.constraint(equalTo: titlePanal.rightAnchor, constant: -8.0).isActive = true
            btnSearch.rightAnchor.constraint(equalTo: btnBasket.leftAnchor, constant: -20.0).isActive = true
        } else {
            btnBack.rightAnchor.constraint(equalTo: titlePanal.rightAnchor, constant: -8).isActive = true
            btnBack.transform = btnBack.transform.rotated(by: cgFloat.pi)
            btnBasket.leftAnchor.constraint(equalTo: titlePanal.leftAnchor, constant: 8.0).isActive = true
            btnSearch.leftAnchor.constraint(equalTo: btnBasket.rightAnchor, constant: 20.0).isActive = true
        }
    }
    
    fileprivate func setupCollectionView()-> void {
        collectionView.register(Constants.Nibs.SectionDetails, forCellWithReuseIdentifier: Constants.ReuseIdentifiers.SectionDetails)
        collectionView.alwaysBounceVertical = true
    }
    
    fileprivate func loadData()-> void {
        if (Gemo.gem.isConnected) {
            guard let id = section?.id else { return }
            Product.getData(by: id, page: self) { [weak self] (products) in
                self?.dataSource = products
                Gemo.gem.stopSpinning()
            }
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
    
}


// MARK: - Collection view delegate & data source functions 

extension SectionDetails
{
    internal override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch(collectionView) {
        case self.collectionView:
            return dataSource.count
        default:
            return super.collectionView(collectionView, numberOfItemsInSection: section)
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch(collectionView) {
        case self.collectionView:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ReuseIdentifiers.SectionDetails, for: indexPath) as? SectionDetailsCell else { return UICollectionViewCell() }
            cell.product = dataSource[indexPath.item]
            cell.btnLeft.setImage(#imageLiteral(resourceName: "add-favorite"), for: .normal)
            cell.btnRight.tag = indexPath.item
            cell.btnLeft.tag = indexPath.item
            cell.btnRight.addTarget(self, action: #selector(addToBasketClicked(_:)), for: .touchUpInside) // add to basket
            cell.btnLeft.addTarget(self, action: #selector(addToFavoritClicked(_:)), for: .touchUpInside) // to favorites
            return cell
        default:
            return super.collectionView(collectionView, cellForItemAt: indexPath)
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch(collectionView) {
        case self.collectionView:
            print("selected item is: \(indexPath)")
            guard let productDetailsPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.ProductDetails) as? ProductDetails else { return }
            productDetailsPage.productId = dataSource[indexPath.item].id
            navigate(to: productDetailsPage)
            
        default:
            super.collectionView(collectionView, didSelectItemAt: indexPath)
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        switch (collectionView) {
        case self.collectionView:
            return Insets(top: 8, left: 0, bottom: 0, right: 0)
        default:
            return .zero
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch(collectionView) {
        case self.collectionView:
            return Size(width: collectionView.frame.width, height: collectionView.frame.height*0.33)
        default:
            return super.collectionView(collectionView, layout: collectionViewLayout, sizeForItemAt: indexPath)
        }
    }
    
}

















