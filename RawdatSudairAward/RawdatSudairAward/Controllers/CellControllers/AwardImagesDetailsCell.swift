
import UIKit


final class AwardImagesDetailsCell: UICollectionViewCell, CellProvider
{
    // MARK: - Constants
    
    //MARK: - Variables
    
    internal var photo: Photo? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var container: UIView!
    @IBOutlet private weak var picture: UIImageView!
    
    // MARK: - Overridden functions
    
    internal override var isSelected: Bool {
        didSet {
            container.backgroundColor = (isSelected) ? .yellow : Color(hex: "#242424")
        }
    }
    
    
    internal func updateUI() {
        if let imgLink = photo?.link {
            picture.loadImage(from: imgLink, onComplete: nil)
        }
    }
    
    
    
    //MARK: - Actions
}
