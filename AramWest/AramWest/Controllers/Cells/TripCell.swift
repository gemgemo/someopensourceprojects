
import Foundation
import Cosmos
import Localize_Swift

internal final class TripCell: CollectionCell
{
    
    // MARK: - Constants
    
    // MARK: - Variables
    internal var trip: Trip? {
        didSet {
            updateUi()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var lblDate: UILabel!
    @IBOutlet private weak var icon: UIImageView! {
        didSet {
            icon.rounded(with: Color(white: 0.88, alpha: 0.9), and: 5.0)
        }
    }
    @IBOutlet private weak var lblName: UILabel!
    @IBOutlet private weak var state: UIImageView!
    @IBOutlet private weak var lblStateMessage: UILabel!
    @IBOutlet private weak var lblCostTitle: UILabel!
    @IBOutlet private weak var lblCost: UILabel!
    @IBOutlet private weak var eye: UIImageView!
    @IBOutlet private weak var lblDetails: UILabel!
    @IBOutlet private weak var lblRateTitle: UILabel!
    @IBOutlet private weak var rateView: CosmosView!
    @IBOutlet internal weak var btnCallDetails: UIButton!
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        lblDate.text = trip?.timestamp ?? .empty
        if let iconLink = trip?.driverAvatar {
            icon.loadImage(from: iconLink, onComplete: nil)
        }
        lblName.text = trip?.driverName ?? .empty
        rateView.rating = NumberFormatter().number(from: trip?.rate ?? "0")?.doubleValue ?? Number.zero.doubleValue
        lblCostTitle.text = "costofthemissionlan".localized()
        lblCost.text = trip?.salary ?? .empty
        lblRateTitle.text = "ratetitlemiddlelan".localized()
        lblDetails.text = "jobdetailslan".localized()
        lblStateMessage.text = "tasksuccesslan".localized()
    }
    
    // MARK: - Actions
    
    /*@IBAction private func showDetailsOnClick(_ sender: UIButton) {
        if let callDetailsScreen = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constants.Storyboard.CallDetails) as? CallDetails {
            
        }
    }*/
    
    
}





internal class CollectionCell: UICollectionViewCell
{
    
    internal func updateUi()-> void {
        
    }
    
}


















