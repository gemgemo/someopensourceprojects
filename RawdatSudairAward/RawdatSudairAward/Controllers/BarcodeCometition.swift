
import UIKit
import CarbonKit
import Localize_Swift

internal var selectedTab = 3
final class BarcodeCometition: BaseController
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    // MARK: - Outlets
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBar()
        createPager()
    }
    
    //MARK: - Actions
    
    @objc fileprivate func backOnClick(_ sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
        selectedTab = 3
    }
    
}


// MARK: - Helper functions

extension BarcodeCometition
{

    fileprivate func updateNavigationBar()-> void {
        updateNavigationBarDirections()
        navigationItem.title = "Barcodecompetitionlan".localized()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: getBackImage(), style: .plain, target: self, action: #selector(backOnClick(_:)))
        navigationController?.navigationBar.layer.shadowColor = Color.clear.cgColor
    }
    
    fileprivate func createPager()-> void {
        var tabsNames = [string]()
        var pager = CarbonTabSwipeNavigation()
        switch (phoneLanguage()) {
        case "ar":
            if(Localize.currentLanguage() == Constants.Language.Arabic) {
                print("arabic")
                tabsNames = ["aboutcompetitionlan".localized(), "schoolscompetitionlan".localized()]
            } else {
                print("english")
                tabsNames = ["schoolscompetitionlan".localized(), "aboutcompetitionlan".localized()]
            }
        case "en":
            if(Localize.currentLanguage() == Constants.Language.Arabic) {
                print("arabic")
                tabsNames = ["schoolscompetitionlan".localized(), "aboutcompetitionlan".localized()]
            } else {
                print("english")
                tabsNames = ["aboutcompetitionlan".localized(), "schoolscompetitionlan".localized()]
            }
        default:
            break
        }
        pager = CarbonTabSwipeNavigation(items: tabsNames, delegate: self)
        print("selected tab is: ", selectedTab)
        if (selectedTab != 3) {
            pager.setCurrentTabIndex((Localize.currentLanguage() == Constants.Language.Arabic) ? 0 : 1, withAnimation: true) // en = 1
        } else {
            switch (phoneLanguage()) {
            case "ar":
                if(Localize.currentLanguage() == Constants.Language.Arabic) {
                    print("arabic")
                    pager.setCurrentTabIndex(0, withAnimation: true)
                } else {
                    print("english")
                    pager.setCurrentTabIndex(1, withAnimation: true)
                }
            case "en":
                if(Localize.currentLanguage() == Constants.Language.Arabic) {
                    print("arabic")
                    pager.setCurrentTabIndex(1, withAnimation: true)
                } else {
                    print("english")
                    pager.setCurrentTabIndex(0, withAnimation: true)
                }
            default:
                break
            }
        }
        
        pager.setTabExtraWidth(30)
        pager.setTabBarHeight(45)
        pager.setIndicatorHeight(2)
        pager.insert(intoRootViewController: self)
        pager.carbonSegmentedControl?.backgroundColor = .normal
        pager.toolbar.isTranslucent = false
        let sectionWidth = view.bounds.width/2
        pager.carbonSegmentedControl?.setWidth(sectionWidth, forSegmentAt: 0)
        pager.carbonSegmentedControl?.setWidth(sectionWidth, forSegmentAt: 1)
        pager.setIndicatorColor(.fixed)
        pager.setNormalColor(.black, font: Font.systemFont(ofSize: 13))
        pager.setSelectedColor(.fixed, font: Font.systemFont(ofSize: 13))
        pager.toolbar.setShadow(with: Size(right: 0, top: 1), radius: 1, opacity: 0.8, color: .lightGray)
        
    }
    
    
}



// MARK: - Pager delegate functions 

extension BarcodeCometition: CarbonTabSwipeNavigationDelegate
{
    
    internal func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt) {
        //print(#function)
    }
    
    internal func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt) {
        //print(#function)
    }
    
    internal func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didFinishTransitionTo index: UInt) {
        //print(#function)
    }
    
    internal func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willBeginTransitionFrom index: UInt) {
        //print(#function)
    }
    
    internal func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        switch (phoneLanguage()) {
        case "ar":
            if(Localize.currentLanguage() == Constants.Language.Arabic) {
                print("arabic")
                switch (index) {
                case 0:
                    if let definitions = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Definitions) as? Definitions {
                        return definitions
                    }
                    
                case 1:
                    if let schoolsPrizes = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.SchoolsPrizes) as? SchoolsPrizes {
                        return schoolsPrizes
                    }
                    
                default:
                    return UIViewController()
                }

            } else {
                print("english")
                switch (index) {
                case 1:
                    if let definitions = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Definitions) as? Definitions {
                        return definitions
                    }
                    
                case 0:
                    if let schoolsPrizes = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.SchoolsPrizes) as? SchoolsPrizes {
                        return schoolsPrizes
                    }
                    
                default:
                    return UIViewController()
                }

            }
        case "en":
            if(Localize.currentLanguage() == Constants.Language.Arabic) {
                print("arabic")
                switch (index) {
                case 1:
                    if let definitions = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Definitions) as? Definitions {
                        return definitions
                    }
                    
                case 0:
                    if let schoolsPrizes = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.SchoolsPrizes) as? SchoolsPrizes {
                        return schoolsPrizes
                    }
                    
                default:
                    return UIViewController()
                }

            } else {
                print("english")
                switch (index) {
                case 0:
                    if let definitions = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Definitions) as? Definitions {
                        return definitions
                    }
                    
                case 1:
                    if let schoolsPrizes = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.SchoolsPrizes) as? SchoolsPrizes {
                        return schoolsPrizes
                    }
                    
                default:
                    return UIViewController()
                }

            }
        default:
            break
        }
        return UIViewController()
    }
    
    /*internal func barPosition(for carbonTabSwipeNavigation: CarbonTabSwipeNavigation) -> UIBarPosition {
        
    }*/
    
}




























