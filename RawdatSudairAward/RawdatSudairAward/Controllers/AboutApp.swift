
import UIKit
import Localize_Swift

final class AboutApp: BaseController
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    fileprivate var btnMenu: UIBarButtonItem!
    
    // MARK: - Outlets
    
    @IBOutlet private weak var subTitleViewHeight: NSLayoutConstraint!
    @IBOutlet fileprivate weak var lblInfo: UITextView! {
        didSet {
            lblInfo.text = string.empty
            lblInfo.alwaysBounceVertical = true
        }
    }
    @IBOutlet private weak var lblContainer: UIView!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        updateNavigationBar()
        configureSideMenu()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblContainer.setShadow(with: Size(right: 0, top: 0), radius: 2.5, opacity: 1.0, color: .darkGray)
        loadData()
        NotificationCenter.default.addObserver(self, selector: #selector(updateUi), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    internal override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        subTitleViewHeight.constant = view.bounds.height*0.27
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Actions
    
}



// MARK: - Helper functions

extension AboutApp
{
    
    @objc fileprivate func updateNavigationBar()-> void {
        navigationItem.title = "aboutappnavtitlelan".localized()
        navigationController?.navigationBar.layer.shadowColor = Color.clear.cgColor
        navigationController?.navigationBar.tintColor = .fixed
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: Color.fixed]
        btnMenu = UIBarButtonItem(image: #imageLiteral(resourceName: "menu"), style: .plain, target: self, action: nil)
        navigationItem.rightBarButtonItem = btnMenu
        navigationController?.navigationBar.semanticContentAttribute = .forceLeftToRight
    }
    
    @objc fileprivate func updateUi()-> void {
        navigationController?.navigationBar.semanticContentAttribute = .forceLeftToRight
        navigationItem.title = "aboutappnavtitlelan".localized()
        lblInfo.textAlignment = (Localize.currentLanguage() == Constants.Language.Arabic) ? .right : .left
        loadData()
    }
    
    fileprivate func configureSideMenu()-> void {
        btnMenu.target = revealViewController()
        btnMenu.action = #selector(revealViewController().rightRevealToggle(_:))
        view.addGestureRecognizer(revealViewController().panGestureRecognizer())
    }
    
    fileprivate func loadData()-> void {
        startSpin()
        AwardDetails.getData(by: Constants.BackendURLs.shared.AboutApp, viewController: self) { [weak self] (object) -> void in
            if let text = object.content {
                DispatchQueue.main.async { [weak self] in
                    self?.lblInfo.text = text.trimmingCharacters(in: .whitespaces)
                }
            }
            self?.stopSpin()
        }
    }
    
    
}





































