
import UIKit
import Gemo

final class Register: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var usertype = Usertype.main    
    
    
    // MARK: - Outlets
    
    @IBOutlet private weak var registerActionsPanel: UIStackView!
    @IBOutlet private weak var btnSignUp: UIButton!
    @IBOutlet private weak var signUpSeperator: UIView!
    @IBOutlet private weak var btnSignIn: UIButton!
    @IBOutlet private weak var signInSeperator: UIView!
    @IBOutlet fileprivate weak var containerPanel: UIView!
    @IBOutlet weak var stackButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var actionPanelTopMargin: NSLayoutConstraint!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        signInView()
        switch (usertype) {
        case .main:
            registerActionsPanel.isHidden = false
            containerPanel.topAnchor.constraint(equalTo: registerActionsPanel.bottomAnchor, constant: 10.0).isActive = true
        case .sub:
            registerActionsPanel.isHidden = true
            containerPanel.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        }
        btnSignIn.setTitle(Localization.Login.localized(), for: .normal)
        btnSignUp.setTitle(Localization.NewSignUp.localized(), for: .normal)
        btnSignIn.setTitleColor(#colorLiteral(red: 0, green: 0.5182415843, blue: 0.8504658341, alpha: 1), for: .normal)
        signInSeperator.backgroundColor = #colorLiteral(red: 0, green: 0.5182415843, blue: 0.8504658341, alpha: 1)
        btnSignUp.setTitleColor(.lightGray, for: .normal)
        signUpSeperator.backgroundColor = .white

    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar(isLogo: true, isMainPage: false)
    }
    
    internal override func updateUi() {
        super.updateUi()
        /*switch (usertype) {
        case .main:
            registerActionsPanel.isHidden = false
            containerPanel.topAnchor.constraint(equalTo: registerActionsPanel.bottomAnchor, constant: 10.0).isActive = true
        case .sub:
            registerActionsPanel.isHidden = true
            containerPanel.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        }
        btnSignIn.setTitle(Localization.Login.localized(), for: .normal)
        btnSignUp.setTitle(Localization.NewSignUp.localized(), for: .normal)
        btnSignIn.setTitleColor(#colorLiteral(red: 0, green: 0.5182415843, blue: 0.8504658341, alpha: 1), for: .normal)
        signInSeperator.backgroundColor = #colorLiteral(red: 0, green: 0.5182415843, blue: 0.8504658341, alpha: 1)
        btnSignUp.setTitleColor(.lightGray, for: .normal)
        signUpSeperator.backgroundColor = .white*/
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //signInView()
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    
    // MARK: - Actions
    
    
    @IBAction private func signInOnCLick(_ sender: UIButton) {
        btnSignIn.setTitleColor(#colorLiteral(red: 0, green: 0.5182415843, blue: 0.8504658341, alpha: 1), for: .normal)
        signInSeperator.backgroundColor = #colorLiteral(red: 0, green: 0.5182415843, blue: 0.8504658341, alpha: 1)
        btnSignUp.setTitleColor(.lightGray, for: .normal)
        signUpSeperator.backgroundColor = .white
        signInView()
    }
    
    @IBAction private func signUpOnCLick(_ sender: UIButton) {
        btnSignUp.setTitleColor(#colorLiteral(red: 0, green: 0.5182415843, blue: 0.8504658341, alpha: 1), for: .normal)
        signUpSeperator.backgroundColor = #colorLiteral(red: 0, green: 0.5182415843, blue: 0.8504658341, alpha: 1)
        btnSignIn.setTitleColor(.lightGray, for: .normal)
        signInSeperator.backgroundColor = .white
        signUpView()
    }

}


// MARK: - Helper functions

extension Register {
    
    fileprivate func signInView()-> void {
        guard let signinPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.SignIn) as? SignIn else { return }
        move(this: signinPage, in: containerPanel)
        /*signinPage.remove()
        signinPage.view.frame = containerPanel.frame
        signinPage.willMove(toParentViewController: self)
        view.addSubview(signinPage.view)
        addChildViewController(signinPage)
        signinPage.didMove(toParentViewController: self)*/
    }
    
    fileprivate func signUpView()-> void {
        guard let signupPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.SignUp) as? SignUp else { return }
        move(this: signupPage, in: containerPanel)
        /*signupPage.remove()
        signupPage.view.frame = containerPanel.frame        
        signupPage.willMove(toParentViewController: self)
        view.addSubview(signupPage.view)
        addChildViewController(signupPage)
        signupPage.didMove(toParentViewController: self)*/
    }
}























