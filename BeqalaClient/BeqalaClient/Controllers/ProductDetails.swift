
import UIKit

final class ProductDetails: BaseController
{

    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var productId: string?
    fileprivate var images = Array<string>() {
        didSet {
            ThreadQueue.main { [weak self] in
                self?.photoViewer?.reloadData()
            }
        }
    }
    fileprivate var rate = Number.zero.intValue
    
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var titlePanel: UIView! { didSet { shadow(for: titlePanel) } }
    @IBOutlet fileprivate weak var lblTitle: UILabel! { didSet { lblTitle.text = .empty } }
    @IBOutlet fileprivate weak var headerPanel: View!
    @IBOutlet fileprivate weak var pagerControl: UIPageControl! { didSet { pagerControl.numberOfPages = 0 } }
    @IBOutlet fileprivate weak var photoViewer: UICollectionView! {
        didSet {
            photoViewer.delegate = self
            photoViewer.dataSource = self
        }
    }
    @IBOutlet fileprivate weak var midPanel: View!
    @IBOutlet fileprivate weak var lblProductName: UILabel! { didSet { lblProductName.text = .empty } }
    @IBOutlet fileprivate weak var lblProductPrice: UILabel! { didSet { lblProductPrice.text = .empty } }
    @IBOutlet fileprivate weak var lblRealProductQuantity: UILabel!  { didSet { lblRealProductQuantity.text = .empty } }
    @IBOutlet fileprivate weak var lblProductDescription: UILabel! { didSet { lblProductDescription.text = .empty } }
    @IBOutlet fileprivate weak var lblProductInfo: UILabel! { didSet { lblProductInfo.text = .empty } }
    @IBOutlet fileprivate weak var footerPanel: View!
    @IBOutlet fileprivate weak var storeIcon: UIImageView!
    @IBOutlet fileprivate weak var lblStoreName: UILabel! { didSet { lblStoreName.text = .empty } }
    @IBOutlet fileprivate weak var lblStoreInfo: UILabel! { didSet { lblStoreInfo.text = .empty } }
    @IBOutlet fileprivate var starts: [UIImageView]!
    @IBOutlet private weak var headerPanelHeight: NSLayoutConstraint!
    @IBOutlet private weak var footerPanelHeight: NSLayoutConstraint!
    @IBOutlet fileprivate weak var btnAddRate: Button!
    @IBOutlet fileprivate var ratePanel: UIView!
    @IBOutlet private var reviews: [UIButton]!
    @IBOutlet fileprivate weak var lblRatePanelTitle: UILabel!
    @IBOutlet fileprivate weak var btnShowRatePanel: UIButton!
    @IBOutlet fileprivate weak var btnBasket: UIButton!
    @IBOutlet fileprivate weak var btnBack: UIButton!
    @IBOutlet fileprivate weak var starsStack: UIStackView!
    @IBOutlet fileprivate weak var rateComponentsPanel: UIView!
    @IBOutlet fileprivate weak var actionPanel: UIView!
    @IBOutlet fileprivate weak var divider: UIView!
    @IBOutlet fileprivate weak var btnAddToBasket: UIButton!
    @IBOutlet fileprivate weak var btnAddToFavorite: UIButton!
    
    
    
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        setupRatePanel()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
    }
    
    internal override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        headerPanelHeight.constant = view.frame.height*0.33
        footerPanelHeight.constant = view.frame.height*0.20
    }
    
    
    // MARK: - Actions
    
    @IBAction private func backOnCLick(_ sender: UIButton) {
        back()
    }
    
    @IBAction private func basketOnCLick(_ sender: UIButton) {
        navigateToBasket()
    }
    
    @IBAction private func addToBasket(_ sender: UIButton) {
        add(to: Constants.Urls.Basket, forUserId: UserDefaults.standard.string(forKey: Constants.Misc.UserID) ?? .empty, andProductId: productId ?? .empty)
    }
    
    @IBAction private func addToFavorits(_ sender: UIButton) {
        add(to: Constants.Urls.Favorite, forUserId: UserDefaults.standard.string(forKey: Constants.Misc.UserID) ?? .empty, andProductId: productId ?? .empty)
    }
    
    @IBAction private func showRatePanelOnCLick(_ sender: UIButton) {
        ratePanel.isHidden = false
        let animator = UIViewPropertyAnimator(duration: 0.5, dampingRatio: 0.5) { [weak self] in
            self?.ratePanel.alpha = 1
        }
        animator.startAnimation()
    }
    
    @IBAction private func addRateOnCLick(_ sender: Button) {
        func hideRatePanel() {
            let animator = UIViewPropertyAnimator(duration: 0.5, dampingRatio: 0.5) { [weak self] in
                self?.ratePanel.alpha = 0
            }
            animator.addCompletion { [weak self] (position) in
                self?.ratePanel.isHidden = true
            }
            animator.startAnimation()
        }
        if (Gemo.gem.isConnected) {
            print("rate value \(rate)")
            let paramters: [string: any] = [
                Constants.Misc.MainKey: Constants.Misc.MainValue,
                "user_id": UserDefaults.standard.string(forKey: Constants.Misc.UserID) ?? .empty,
                "id_product": productId ?? .empty,
                "rate": rate
            ]
            UIApplication.showIndicator(by: true)
            HttpClient.request(link: Constants.Urls.RateProduct, method: .post, parameters: paramters) { [weak self] (result, response, error) -> void in
                if (error != nil) {
                    UIApplication.showIndicator(by: false)
                    print("rate error: \(string(describing: error))")
                    return
                }
                UIApplication.showIndicator(by: false)
                print("request result: \(string(describing: result))")
                ThreadQueue.main { [weak self] in
                    hideRatePanel()
                    self?.fetchPromotionDetails()
                }
            }
            
        } else {
            hideRatePanel()
        }
    }
    
    @IBAction private func setRateOnCLick(_ sender: UIButton) {
        reviews.forEach { $0.setImage(#imageLiteral(resourceName: "star-border"), for: .normal) }
        rate = sender.tag
        for i in stride(from: 0, to: rate, by: 1) {
            reviews[i].setImage(#imageLiteral(resourceName: "star"), for: .normal)
        }
    }
    
    @objc fileprivate func ratePanelTapped(_ gesture: UITapGestureRecognizer) {
        /*let animator = UIViewPropertyAnimator(duration: 0.5, dampingRatio: 0.5) { [weak self] in
         self?.ratePanel.alpha = 0
         }
         animator.addCompletion { [weak self] (position) in
         self?.ratePanel.isHidden = true
         }
         animator.startAnimation()*/
    }

}


// MARK: - Helper functions

extension ProductDetails
{
    
    fileprivate func setupRatePanel()-> void {
        view.addSubview(ratePanel)
        ratePanel.isHidden = true
        ratePanel.alpha = 0
        ratePanel.frame = UIScreen.main.bounds
        ratePanel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ratePanelTapped(_:))))
    }
    
    fileprivate func updateUi()-> void {
        tabBar.isHidden = true
        view.bringSubview(toFront: titlePanel)
        btnShowRatePanel.setTitle(Localization.AddRate.localized(), for: .normal)
        btnAddRate.setTitle(Localization.AddRate.localized(), for: .normal)
        lblRatePanelTitle.text = Localization.AddRate.localized()
        btnBack.transform = CGAffineTransform.identity
        if (Localizer.instance.current == .arabic) {
            btnBack.leftAnchor.constraint(equalTo: titlePanel.leftAnchor, constant: 8).isActive = true
            btnBasket.rightAnchor.constraint(equalTo: titlePanel.rightAnchor, constant: -8).isActive = true
            lblStoreInfo.textAlignment = .right
            lblStoreName.textAlignment = .right
            lblProductInfo.textAlignment = .right
            lblProductName.textAlignment = .right
            lblProductPrice.textAlignment = .right
            lblProductDescription.textAlignment = .right
            lblRealProductQuantity.textAlignment = .right
            storeIcon.rightAnchor.constraint(equalTo: footerPanel.rightAnchor, constant: -8).isActive = true
            lblStoreName.rightAnchor.constraint(equalTo: storeIcon.leftAnchor, constant: -16).isActive = true
            lblStoreName.leftAnchor.constraint(equalTo: footerPanel.leftAnchor, constant: 8).isActive = true
            btnShowRatePanel.leftAnchor.constraint(equalTo: rateComponentsPanel.leftAnchor, constant: 0).isActive = true
            starsStack.rightAnchor.constraint(equalTo: rateComponentsPanel.rightAnchor, constant: 0).isActive = true
            btnAddToBasket.topAnchor.constraint(equalTo: actionPanel.topAnchor).isActive = true
            btnAddToBasket.rightAnchor.constraint(equalTo: actionPanel.rightAnchor).isActive = true
            btnAddToBasket.bottomAnchor.constraint(equalTo: actionPanel.bottomAnchor).isActive = true
            btnAddToBasket.leftAnchor.constraint(equalTo: divider.rightAnchor).isActive = true
            
            btnAddToFavorite.topAnchor.constraint(equalTo: actionPanel.topAnchor).isActive = true
            btnAddToFavorite.leftAnchor.constraint(equalTo: actionPanel.leftAnchor).isActive = true
            btnAddToFavorite.bottomAnchor.constraint(equalTo: actionPanel.bottomAnchor).isActive = true
            btnAddToFavorite.rightAnchor.constraint(equalTo: divider.leftAnchor).isActive = true
        } else {
            btnBack.rightAnchor.constraint(equalTo: titlePanel.rightAnchor, constant: -8).isActive = true
            btnBack.transform = btnBack.transform.rotated(by: cgFloat.pi)
            btnBasket.leftAnchor.constraint(equalTo: titlePanel.leftAnchor, constant: 8).isActive = true
            lblStoreInfo.textAlignment = .left
            lblStoreName.textAlignment = .left
            lblProductInfo.textAlignment = .left
            lblProductName.textAlignment = .left
            lblProductPrice.textAlignment = .left
            lblProductDescription.textAlignment = .left
            lblRealProductQuantity.textAlignment = .left
            storeIcon.leftAnchor.constraint(equalTo: footerPanel.leftAnchor, constant: 8).isActive = true
            lblStoreName.rightAnchor.constraint(equalTo: footerPanel.rightAnchor, constant: -8).isActive = true
            lblStoreName.leftAnchor.constraint(equalTo: storeIcon.rightAnchor, constant: 16).isActive = true
            btnShowRatePanel.rightAnchor.constraint(equalTo: rateComponentsPanel.rightAnchor, constant: 0).isActive = true
            starsStack.leftAnchor.constraint(equalTo: rateComponentsPanel.leftAnchor, constant: 0).isActive = true
            
            btnAddToBasket.topAnchor.constraint(equalTo: actionPanel.topAnchor).isActive = true
            btnAddToBasket.leftAnchor.constraint(equalTo: actionPanel.leftAnchor).isActive = true
            btnAddToBasket.bottomAnchor.constraint(equalTo: actionPanel.bottomAnchor).isActive = true
            btnAddToBasket.rightAnchor.constraint(equalTo: divider.leftAnchor).isActive = true
            
            btnAddToFavorite.topAnchor.constraint(equalTo: actionPanel.topAnchor).isActive = true
            btnAddToFavorite.rightAnchor.constraint(equalTo: actionPanel.rightAnchor).isActive = true
            btnAddToFavorite.bottomAnchor.constraint(equalTo: actionPanel.bottomAnchor).isActive = true
            btnAddToFavorite.leftAnchor.constraint(equalTo: divider.rightAnchor).isActive = true
            
        }
        fetchPromotionDetails()
    }
    
    fileprivate func fetchPromotionDetails()-> void {
        if (Gemo.gem.isConnected) {
            Gemo.gem.startSpinning(in: view)
            let parameters: [string: any] = [
                Constants.Misc.MainKey: Constants.Misc.MainValue,
                "id": productId ?? . empty
            ]
            HttpClient.request(link: Constants.Urls.OneProduct, method: .post, parameters: parameters) { [weak self] (data, response, error) -> void in
                guard let this = self else { return }
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("get product info error: \(string(describing: error))")
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription, in: this.view)
                    return
                }
                Gemo.gem.stopSpinning()
                print("get product data \(string(describing: data))")
                if let json = (data as? Dictionary<string, any>), let value = (json["all_data"] as? [Dictionary<string, any>])?.first {
                    ThreadQueue.main { [weak self] in
                        guard let me = self else { return }
                        me.lblTitle.text = value["title"] as? string ?? .empty
                        me.lblProductPrice.text = "\(value["price"] as? string ?? .empty) \(Localization.SR.localized())"
                        me.lblProductName.text = value["title"] as? string ?? .empty
                        me.lblRealProductQuantity.text = Localization.QuantityAvailable.localized() + ": " + (value["quantity"] as? string ?? .empty)
                        me.lblProductDescription.text = Localization.Description.localized()
                        me.lblProductInfo.text = value["details"] as? string ?? .empty
                        let photo1 = value["photo_1"] as? string ?? .empty, photo2 = value["photo_2"] as? string ?? .empty, photo3 = value["photo_3"] as? string ?? .empty, photo4 = value["photo_4"] as? string ?? .empty
                        [photo1, photo2, photo3, photo4].forEach { [weak self] in
                            if ($0 != .empty) {
                                self?.images.append($0)
                            }
                        }
                        me.pagerControl.numberOfPages = me.images.count
                        me.lblStoreName.text = value["title_matjar"] as? string ?? .empty
                        me.lblStoreInfo.text = value["des_matjar"] as? string ?? .empty
                        me.storeIcon.loadImage(from: value["photo_matjar"] as? string ?? .empty)
                        let rateValue = (value["average"] as? string ?? "0").toDouble
                        let intRate = int(ceil(rateValue))
                        print(intRate)
                        if (Localizer.instance.current == .arabic) {
                            self?.starts.reverse()
                        }
                        if (intRate <= 5) {
                            for i in stride(from: 0, to: intRate, by: 1) {
                                me.starts[i].image = #imageLiteral(resourceName: "star")
                            }
                        }
                    }
                } else {
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized(), in: this.view)
                }
            }
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
    
}


// MARK: - Collection view delegate & data source functions

extension ProductDetails
{
    internal override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == photoViewer) {
            return images.count
        }
        return super.collectionView(collectionView, numberOfItemsInSection: section)
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (collectionView == photoViewer) {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ReuseIdentifiers.ProductSlider, for: indexPath) as? ProductSliderCell else { return UICollectionViewCell() }
            cell.imageLink = images[indexPath.item]
            return cell
        }
        return super.collectionView(collectionView, cellForItemAt: indexPath)
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView == photoViewer) {
            print("selected item is: \(indexPath)")
        } else {
            super.collectionView(collectionView, didSelectItemAt: indexPath)
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (collectionView == photoViewer) {
            return Size(width: collectionView.frame.width, height: collectionView.frame.height)
        }
        return super.collectionView(collectionView, layout: collectionViewLayout, sizeForItemAt: indexPath)
    }
    
    internal func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let pageNumber = targetContentOffset.pointee.x/photoViewer.frame.width
        pagerControl.currentPage = int(pageNumber)
    }
    
}


// MARK: - Slider Cell controller 

final class ProductSliderCell: BaseGridCell
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var imageLink: string? { didSet { updateUi() } }
    
    
    // MARK: - Outlets
    
    @IBOutlet private weak var cover: UIImageView?
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        guard let link = imageLink else { return }
        cover?.loadImage(from: link)
    }
    
    
    // MARK: - Actions

    
}

























