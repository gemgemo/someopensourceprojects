
import Foundation
import Gemo


class MyBillsCell: BaseCollectionCell {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var bill: Bill? { didSet { updateUi() } }
    
    
    // MARK: - Outlets
    
        
    @IBOutlet weak var lblProccessNumber: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblNoOfLiters: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var carTypeIcon: UIImageView!
    @IBOutlet weak var lblALetters: UILabel!
    @IBOutlet weak var lblELetters: UILabel!
    @IBOutlet weak var lblANumbers: UILabel!
    @IBOutlet weak var lblENumbers: UILabel!
    @IBOutlet weak var carPhoto: UIImageView!
    @IBOutlet weak var carIcon: UIImageView!
    
    
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        lblProccessNumber.text = "\(Localization.ProccessNo.localized())   \("??")"
        lblOrderDate.text = "\(Localization.OrderDate.localized())   \(bill?.timestamp ?? .empty)"
        lblNoOfLiters.text = "\(Localization.LitresNo.localized())   \(bill?.liters ?? .empty)"
        lblPrice.text = "\(Localization.Price.localized())   \(bill?.price ?? .empty) \(Localization.SR.localized())"
        lblCarName.text = "\(bill?.carModel ?? .empty)-\(bill?.carPrand ?? .empty)"
        carTypeIcon.loadImage(from: bill?.carIcon ?? .empty)
        lblALetters.text = bill?.aLetters ?? .empty
        lblELetters.text = bill?.eLetters ?? .empty
        lblANumbers.text = bill?.aNumbers ?? .empty
        lblENumbers.text = bill?.eNumbers ?? .empty
        carIcon.loadImage(from: bill?.carIcon ?? .empty)
        carPhoto.loadImage(from: bill?.carPhoto ?? .empty)
        carTypeIcon.loadImage(from: bill?.carTypePhoto ?? .empty)
        lblALetters.textAlignment = .center
        lblELetters.textAlignment = .center
        lblANumbers.textAlignment = .center
        lblENumbers.textAlignment = .center

        
        
    }
    
    
    // MARK: - Actions

}


























