
import Foundation
import Gemo

class ReportCell: BaseTableCell {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    var report: Report? { didSet { updateUi() } }
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var lblSpentFuelNo: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var carIcon: UIImageView!
    @IBOutlet weak var carTypeIcon: UIImageView!
    @IBOutlet weak var lblaLetters: UILabel!
    @IBOutlet weak var lblANumbers: UILabel!
    @IBOutlet weak var lblELetters: UILabel!
    @IBOutlet weak var lblENumbers: UILabel!
    
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        carImage.loadImage(from: report?.carPhoto ?? .empty)
        carIcon.loadImage(from: report?.carIcon ?? .empty)
        carTypeIcon.loadImage(from: report?.carTypeIcon ?? .empty)
        lblCarName.text = "\(report?.carAName ?? .empty)-\(report?.carPrand ?? .empty)"
        lblSpentFuelNo.text = "\(Localization.SpentLitresNo.localized())   \(report?.liters ?? .empty)"
        lblPrice.text = "\(Localization.AmountsSpent.localized())   \(report?.price ?? .empty)"
        //lblaLetters.text = report?.c ?? .empty
        lblANumbers.text = report?.aNumbers ?? .empty
        lblENumbers.text = report?.eNumbers ?? .empty
        //lblELetters.text = report.
    }
    
    
    // MARK: - Actions

    
}




























