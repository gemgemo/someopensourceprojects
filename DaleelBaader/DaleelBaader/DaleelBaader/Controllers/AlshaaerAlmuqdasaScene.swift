
import UIKit

import Localize_Swift
import MapKit


final class AlshaaerAlmuqdasaScene: BaseController
{
    // MARK: - Constants
    
    //MARK: - Variables
    
    fileprivate var dataSource = Array<AlshaaerAluqdsa>() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tblChooses.reloadData()
            }
        }
    }
        
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var mapView: MKMapView! {
        didSet {
            mapView.delegate = self
            mapView.setupCamera()
        }
    }
    @IBOutlet private weak var choosesView: UIView! {
        didSet {
            choosesView.layer.borderColor = Color(white: 0.88, alpha: 0.8).cgColor
            choosesView.layer.borderWidth = 1.0
            choosesView.layer.cornerRadius = 5.0
        }
    }
    @IBOutlet fileprivate weak var tblChooses: UITableView! {
        didSet {
            tblChooses.delegate = self
            tblChooses.dataSource = self
        }
    }
    @IBOutlet private weak var mapHeight: NSLayoutConstraint!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        configureTable()
        if (AppManager.shared.isConnected) {
            loadData()
        } else {
            dialog(with: Constants.Misc.InternetMessage)
        }
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBar()
    }
    
    internal override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        mapHeight.constant = view.bounds.height*0.60
    }
    
    //MARK: - Actions
    
}



// MARK: - Helper functions

extension AlshaaerAlmuqdasaScene
{
    
    fileprivate func updateNavigationBar()-> void {
        navigationItem.title = "alshaaeralmuqadasatitle".localized()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(goBack(_:)))
    }
    
    fileprivate func configureTable()-> void {
        tblChooses.register(Constants.Nib.AlshaaerAlmuqdasa, forCellReuseIdentifier: Constants.ReuseIdentifier.AlshaaerAlmuqdasa)
    }
    
    fileprivate func loadData()-> void {
        startSpin()
        AlshaaerAluqdsa.getData(viewController: self) { [weak self] (response) -> void in
            guard let this = self else { return }
            this.dataSource = response
            UIApplication.showIndicator(by: false)
            DispatchQueue.main.async { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.tblChooses.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
                if let alshaaerObject = response.first, let latitude = alshaaerObject.latitude, let longitude = alshaaerObject.longitude {
                    let coordinate = strongSelf.mapView.makeLocationCoordinate(latitude: latitude, longitude: longitude)
                    strongSelf.mapView.setRegion(in: coordinate)
                    if let objcTitle = alshaaerObject.title {
                        strongSelf.mapView.setAnnotation(in: coordinate, with: objcTitle, subTitle: string.empty)
                    }
                }
            }
            this.stopSpin()
        }
    }
    
//    fileprivate func makeLocationCoordinate(latitude: string, longitude: string)-> CLLocationCoordinate2D {
//        let formatter = NumberFormatter()
//        let lati = formatter.number(from: latitude)?.doubleValue ?? Number.zero.doubleValue
//        let longi = formatter.number(from: longitude)?.doubleValue ?? Number.zero.doubleValue
//        return CLLocationCoordinate2D(latitude: lati, longitude: longi)
//    }
    
}


// MARK: - Table view delegate & data source functions 

extension AlshaaerAlmuqdasaScene: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.AlshaaerAlmuqdasa) as? AlshaaerAluqdsaCellScene {
            cell.alshaaer = dataSource[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objec = dataSource[indexPath.row]
        if let latitude = objec.latitude, let longitude = objec.longitude, let title = objec.title {
            let coordinate = mapView.makeLocationCoordinate(latitude: latitude, longitude: longitude)
            mapView.annotations.forEach { mapView.removeAnnotation($0) }            
            mapView.setRegion(in: coordinate)                        
            mapView.setAnnotation(in: coordinate, with: title, subTitle: string.empty)
        }
    }
    
}



// MARK: - Map view delegate functions 

extension AlshaaerAlmuqdasaScene: MKMapViewDelegate
{
    
    internal func mapViewWillStartLoadingMap(_ mapView: MKMapView) {
        UIApplication.showIndicator(by: true)
    }
    
    internal func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        UIApplication.showIndicator(by: false)
    }
    
    internal func mapViewDidFailLoadingMap(_ mapView: MKMapView, withError error: Error) {
        UIApplication.showIndicator(by: false)
        dialog(with: "ErrorMessage".localized())
    }
    
}




































