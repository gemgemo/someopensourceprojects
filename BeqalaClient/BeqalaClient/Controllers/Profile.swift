
import UIKit

final class Profile: BaseController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    
    
    // MARK: - Outlets
 
    
    @IBOutlet fileprivate weak var titlePanal: UIView! {
        didSet {
            shadow(for: titlePanal)
        }
    }
    @IBOutlet fileprivate weak var lblTitleHeader: UILabel!
    @IBOutlet fileprivate weak var header: UIView! {
        didSet {
            header.setShadow(with: Size(right: 0, top: 0.6), radius: 0.7, opacity: 0.2, color: .black)
        }
    }
    @IBOutlet fileprivate weak var middel: View!
    @IBOutlet fileprivate weak var footer: View!
    @IBOutlet fileprivate weak var avatar: UIImageView!
    @IBOutlet fileprivate weak var lblName: UILabel! {
        didSet {
            lblName.text = .empty
        }
    }
    @IBOutlet fileprivate weak var lblPhone: UILabel! {
        didSet {
            lblPhone.text = .empty
        }
    }
    @IBOutlet fileprivate weak var btnUpdateProfile: UIButton!
    @IBOutlet fileprivate weak var btnFavorite: UIButton!
    @IBOutlet fileprivate weak var btnOrders: UIButton!
    @IBOutlet fileprivate weak var btnLogout: UIButton!
    @IBOutlet fileprivate weak var btnBack: UIButton!
    @IBOutlet fileprivate weak var iconsStack: UIStackView!
    @IBOutlet fileprivate weak var buttonsStack: UIStackView!
    @IBOutlet fileprivate weak var logoutIcon: UIImageView!
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBar.isHidden = true
        updateUi()
    }
    
    // MARK: - Actions
    
    @IBAction private func updateProfileOnClick(_ sender: UIButton) {
        if let updateScreen = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: Constants.Storyboards.SignUp) as? SignUp {
            updateScreen.isUpdate = true
            navigate(to: updateScreen)
        }
    }
    
    @IBAction private func showFavouritesOnCLick(_ sender: UIButton) {
        navigateToFavorites()
    }
    
    @IBAction private func ordersOnCLick(_ sender: UIButton) {
        guard let ordersPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.CurrentOrders) as? CurrentOrders else { return }
        navigate(to: ordersPage)
    }
    
    @IBAction private func logoutOnCLick(_ sender: UIButton) {
        if (Gemo.gem.isConnected) {
            Gemo.gem.startSpinning(in: view)
            let parameters: Dictionary<string, string> = [
                Constants.Misc.MainKey: Constants.Misc.MainValue,
                "user_id": UserDefaults.standard.string(forKey: Constants.Misc.UserID) ?? .empty
            ]
            HttpClient.request(link: Constants.Urls.Logout, method: .post, parameters: parameters) { [weak self] (result, response, error) -> void in
                guard let this = self else { return }
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("logout error is: \(string(describing: error))")
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription, in: this.view)
                    return
                }
                Gemo.gem.stopSpinning()
                if let json = result as? Dictionary<string, any>, let success = json["result"] as? string, success == "true"  {
                    ThreadQueue.main {
                        UserDefaults.standard.set(string.empty, forKey: Constants.Misc.UserID)
                        UserDefaults.standard.set(string.empty, forKey: Constants.Misc.Email)
                        UserDefaults.standard.set(string.empty, forKey: Constants.Misc.FirstName)
                        UserDefaults.standard.set(string.empty, forKey: Constants.Misc.Gender)
                        UserDefaults.standard.set(string.empty, forKey: Constants.Misc.LastName)
                        UserDefaults.standard.set(string.empty, forKey: Constants.Misc.Phone)
                        UserDefaults.standard.set(false, forKey: Constants.Misc.IsLoggedIn)
                        UserDefaults.standard.synchronize()
                        (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController = UIStoryboard(name: "Register", bundle: nil).instantiateInitialViewController()
                    }
                }
            }
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
    
    @IBAction private func backOnClick(_ sender: UIButton) {
        back()
    }
    
    

}



// MARK: - Helper functions

extension Profile
{
    
    fileprivate func updateUi()-> void {
        lblTitleHeader.text = Localization.MyAccount.localized()
        let name = "\(UserDefaults.standard.value(forKey: Constants.Misc.FirstName) as? string ?? .empty) \(UserDefaults.standard.value(forKey: Constants.Misc.LastName) as? string ?? .empty)"
        lblName.text = name
        lblPhone.text = UserDefaults.standard.value(forKey: Constants.Misc.Phone) as? string ?? .empty
        btnUpdateProfile.setTitle(Localization.UpdateProfile.localized(), for: .normal)
        btnFavorite.setTitle(Localization.Favorits.localized(), for: .normal)
        btnOrders.setTitle(Localization.Orders.localized(), for: .normal)
        btnLogout.setTitle(Localization.Logout.localized(), for: .normal)
        avatar.image = ((UserDefaults.standard.value(forKey: Constants.Misc.Gender) as? string ?? "1") == "1") ? #imageLiteral(resourceName: "manLogo") : #imageLiteral(resourceName: "womenLogo")
        btnBack.transform = CGAffineTransform.identity
        if (Localizer.instance.current == .arabic) {
            btnBack.leftAnchor.constraint(equalTo: titlePanal.leftAnchor, constant: 8).isActive = true
            iconsStack.rightAnchor.constraint(equalTo: middel.rightAnchor, constant: -8).isActive = true
            buttonsStack.rightAnchor.constraint(equalTo: iconsStack.leftAnchor, constant: -16).isActive = true
            buttonsStack.leftAnchor.constraint(equalTo: middel.leftAnchor, constant: 8).isActive = true
            logoutIcon.rightAnchor.constraint(equalTo: footer.rightAnchor, constant: -8).isActive = true
            btnLogout.rightAnchor.constraint(equalTo: logoutIcon.leftAnchor, constant: -16).isActive = true
            btnLogout.leftAnchor.constraint(equalTo: footer.leftAnchor, constant: 8).isActive = true
            btnLogout.contentHorizontalAlignment = .right
            btnOrders.contentHorizontalAlignment = .right
            btnFavorite.contentHorizontalAlignment = .right
            btnUpdateProfile.contentHorizontalAlignment = .right
        } else {
            btnBack.rightAnchor.constraint(equalTo: titlePanal.rightAnchor, constant: -8).isActive = true
            btnBack.transform = btnBack.transform.rotated(by: cgFloat.pi)
            iconsStack.leftAnchor.constraint(equalTo: middel.leftAnchor, constant: 8).isActive = true
            buttonsStack.rightAnchor.constraint(equalTo: middel.rightAnchor, constant: -8).isActive = true
            buttonsStack.leftAnchor.constraint(equalTo: iconsStack.rightAnchor, constant: 16).isActive = true
            logoutIcon.leftAnchor.constraint(equalTo: footer.leftAnchor, constant: 8).isActive = true
            btnLogout.rightAnchor.constraint(equalTo: footer.rightAnchor, constant: -8).isActive = true
            btnLogout.leftAnchor.constraint(equalTo: logoutIcon.rightAnchor, constant: 16).isActive = true
            btnLogout.contentHorizontalAlignment = .left
            btnOrders.contentHorizontalAlignment = .left
            btnFavorite.contentHorizontalAlignment = .left
            btnUpdateProfile.contentHorizontalAlignment = .left
        }
    }
    
    
}
















