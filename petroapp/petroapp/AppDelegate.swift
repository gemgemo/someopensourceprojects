//  AppDelegate.swift
//  petroapp
//  Created by GeMoOo on 6/29/17.
//  Copyright © 2017 Atiaf. All rights reserved.

import UIKit
import Gemo
import GoogleMaps
import GooglePlaces

/*
 http://atiafapps.com/des3/Petro-App/des5/a1.html
 http://atiafapps.com/des3/Petro-App/delegate/des4/pet.html
 http://192.232.214.91/~petroapp/webserv.php
 */

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.statusBarStyle = .default
        Threads.background(QOS: .userInitiated) { 
            GMSServices.provideAPIKey(Misc.MapsKey)
            GMSPlacesClient.provideAPIKey(Misc.MapsKey)
        }       
        return true
    }

    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return .portrait
    }
}
