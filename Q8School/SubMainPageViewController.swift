//
//  SubMainPageViewController.swift
//  Q8School
//
//  Created by mac on 12/12/16.
//  Copyright © 2016 Atiaf. All rights reserved.
//

import UIKit

class SubMainPageViewController: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource{

    @IBOutlet weak var myAdsImg: UIImageView!
    @IBOutlet weak var offerLbl: UILabel!
    @IBOutlet weak var myAdsLbl: UILabel!
    @IBOutlet weak var addAdsLbl: UILabel!
    @IBOutlet weak var callUslbl: UILabel!
    @IBOutlet weak var loadingLbl: UILabel!
    @IBOutlet weak var loadinActivity: UIActivityIndicatorView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var femaleTeacherLbl: UILabel!
    @IBOutlet weak var maleTeacherLbl: UILabel!
    @IBOutlet weak var femaleTeacherView: UIView!
    @IBOutlet weak var maleTeacherView: UIView!
    @IBOutlet weak var subMainCollection: UICollectionView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var openMenu: UIBarButtonItem!
    var type:String!
    var selectedId = ""
    var selectedName = ""
    var gender = "m"
    var dataFound: Bool!
    var subjects: Array<Subject> = Array<Subject>()

    let defaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ID\(selectedId)")
        subMainCollection.delegate = self
        subMainCollection.dataSource = self
        headerView.layer.cornerRadius = 4
        headerView.layer.masksToBounds = true
        shadowView.isHidden = true
        loadingView.isHidden = true
        loadingView.layer.cornerRadius = 4
        loadingView.layer.masksToBounds = true
        loadMainPhase(Settings.lang, selectedId)
        if Settings.lang == "ar"{
            subMainCollection.semanticContentAttribute = .forceRightToLeft
            loadingLbl.text = "جاري التحميل..."
            self.title = selectedName
            callUslbl.text = "اتصل بنا"
            addAdsLbl.text = "اضف اعلانك"
            myAdsLbl.text = "اعلاناتي"
            offerLbl.text = "العروض"
            femaleTeacherLbl.text = "استاذة"
            maleTeacherLbl.text = "استاذ"
        }
        if revealViewController() != nil {
            
            revealViewController().rightViewRevealWidth = 235
            openMenu.target = revealViewController()
            openMenu.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        type = UserDefaults.standard.string(forKey: "userType")
        if type == "1"
        {
            if Settings.lang == "ar"
            {
                myAdsLbl.text = "المفضلة"
                myAdsImg.image = UIImage(named: "fav")
                
            }
            else
            {
                myAdsLbl.text = "Favorite"
                myAdsImg.image = UIImage(named: "fav")
                
                
            }
        }




        // Do any additional setup after loading the view.
    }
    func loadMainPhase(_ lang: String,_ mainID: String)
    {
        shadowView.isHidden = false
        loadingView.isHidden = false
        loadinActivity.startAnimating()
        let request = NSMutableURLRequest(url: URL(string:Settings.Subjects)!)
        request.httpMethod = "POST"
        let postString = "lang=\(lang)&scholarPhase=\(mainID)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
            
            if let responseData = data {
                
                do {
                    
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                    print("ddd\(json)")
                    self.dataFound = json["Success"] as? Bool
                    if self.dataFound!
                    {
                        
                            let data = json["Result"] as! [NSDictionary]
                            for i in data
                            {
                                var name1 = ""
                                if Settings.lang == "ar"
                                {
                                    name1 = "name_ar"
                                }
                                else
                                {
                                    name1 = "name"
                                    
                                }
                                
                                let id = i["id"] as! String
                                let name = i[name1] as! String
                                
                                let subject = Subject(id: id, name: name)
                                self.subjects.append(subject)
                                
                            }
                    }
                    DispatchQueue.main.async
                        {
                            self.shadowView.isHidden = true
                            self.loadingView.isHidden = true
                            self.loadinActivity.stopAnimating()
                            self.subMainCollection.reloadData()
                            
                    }
                    
                }
                catch
                {
                    print ("Could not Serialize")
                }
                
            }
            
        }
        task.resume()
        
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        let size = self.subMainCollection.frame.width
        
        return CGSize(width: size  - 80, height: 50)
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "subMainCell", for: indexPath)
            as! SubMainPageCollectionViewCell
        if indexPath.row % 2 == 1
        {
        cell.subMainLbl.text = self.subjects[indexPath.row].name
        cell.id = self.subjects[indexPath.row].id
        cell.layer.cornerRadius = 10
        cell.layer.masksToBounds = true
        cell.contentView.backgroundColor = UIColor(red: 222/255, green: 140/255, blue: 0/255, alpha: 1)
        cell.subMainLbl.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)

        }
        else
        {
            cell.subMainLbl.text = self.subjects[indexPath.row].name
            cell.id = self.subjects[indexPath.row].id
            cell.layer.cornerRadius = 10
            cell.layer.masksToBounds = true
            cell.contentView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            cell.subMainLbl.textColor = UIColor(red: 222/255, green: 140/255, blue: 0/255, alpha: 1)
            
        }
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.subjects.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let cell = subMainCollection.cellForItem(at: indexPath) as! SubMainPageCollectionViewCell
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TeachersVC") as! TeachersVC
        vc.selectedId = cell.id
        vc.selectedName = cell.subMainLbl.text!
        vc.selectedGender = gender
        vc.scholarPhase = selectedId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func displayAlerts(_ Massage: String)
    {
        var t = ""
        var b = ""
        
        if Settings.lang == "ar"{
            t = "تنبيه"
            b = "إغلاق"
        }
        else{
            t = "Alert"
            b = "Close"
        }
        
        let myAlert = UIAlertController(title: t, message:Massage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }


    

    @IBAction func maleTeacherBtn(_ sender: Any)
    {
        maleTeacherView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        femaleTeacherView.backgroundColor = UIColor(red: 222/255, green: 140/255, blue: 0/255, alpha: 1)
        maleTeacherLbl.textColor =  UIColor(red: 222/255, green: 140/255, blue: 0/255, alpha: 1)
        femaleTeacherLbl.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        gender = "m"
        
        
    }

    @IBAction func femaleTeacherBtn(_ sender: Any)
    {
        maleTeacherView.backgroundColor = UIColor(red: 222/255, green: 140/255, blue: 0/255, alpha: 1)
        femaleTeacherView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        maleTeacherLbl.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        femaleTeacherLbl.textColor = UIColor(red: 222/255, green: 140/255, blue: 0/255, alpha: 1)

        gender = "f"
        

    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
    
    @IBAction func callUsBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CallUsVc") as! CallUsVc
        self.navigationController?.pushViewController(vc, animated: false)

    }
    
    @IBAction func addAdsBtn(_ sender: Any) {
        if type == "1"
        {
            if Settings.lang == "ar"
            {
                let msg = "انت طالب لا يمكنك اضافة اعلان"
                self.displayAlerts(msg)
                
            }
            else
            {
                let msg = "you'r student can't add Ads"
                self.displayAlerts(msg)
                
            }
            
            
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AddAdsVc") as! AddAdsVc
            self.navigationController?.pushViewController(vc, animated: false)
            
        }

    }
    
    @IBAction func myAdsBtn(_ sender: Any) {
        if type == "1"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyFavoriteVC") as! MyFavoriteVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyAdsVC") as! MyAdsVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }

    }
    
    @IBAction func offerBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AdsPageViewController") as! AdsPageViewController
        self.navigationController?.pushViewController(vc, animated: false)

    }
    
}
