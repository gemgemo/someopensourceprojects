
import Foundation
import Alamofire

final class Order
{
    
    
    // MARK: - Functions
    
    internal class func send(to link: string, with params: [string: any], in page: BaseController?, onComplete: @escaping(bool)-> void)-> void {
        page?.startSpin()
        Alamofire.request(link, method: .post, parameters: params)
            .responseJSON { (response) in
                let result = response.result
                if (result.error != nil) {
                    print("send order error is: \(result.error)")
                    page?.stopSpin()
                    page?.showAlert(with: .empty, and: "erroralertlan".localized())
                    return
                }
                //print("send order value \(result.value)")
                if let value = result.value as? [string: any], let success = value["Success"] as? string {
                    onComplete((success == "True"))
                    //page?.stopSpin()
                    //page?.showAlert(with: .empty, and: Constants.Localization.NoDelegates)
                    print("value: \(result.value)")
                } else {
                    print("null data")
                    page?.stopSpin()
                    page?.showAlert(with: .empty, and: "erroralertlan".localized())
                }
        }
    }
    
}





































