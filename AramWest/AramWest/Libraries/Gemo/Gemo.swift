
import Foundation
import UIKit
import MapKit
import SystemConfiguration

final class Gemo: Object
{
    
    // MARK:-  shared instance
    internal static var gem: Gemo {
        struct Objc { internal static let instance = Gemo() }
        return Objc.instance
    }
    
    internal var isConnected: bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    // MARK:- Create spinner
    private lazy var indicatorPanal: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Color.darkGray.withAlphaComponent(0.4)
        return view
    }()
    
    private lazy var indicator: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.hidesWhenStopped = true
        return spinner
    }()
    
    private func setupSpinnerViews(in view: UIView?)-> void {
        guard let superPanal = view else {
            print("null view")
            return
        }
        superPanal.addSubview(indicatorPanal)
        indicatorPanal.addSubview(indicator)
        // main panal: right, left, top, bottom
        indicatorPanal.rightAnchor.constraint(equalTo: superPanal.rightAnchor).isActive = true
        indicatorPanal.bottomAnchor.constraint(equalTo: superPanal.bottomAnchor).isActive = true
        indicatorPanal.leftAnchor.constraint(equalTo: superPanal.leftAnchor).isActive = true
        indicatorPanal.topAnchor.constraint(equalTo: superPanal.topAnchor).isActive = true
        // spinner: center y and x
        indicator.centerXAnchor.constraint(equalTo: indicatorPanal.centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: indicatorPanal.centerYAnchor).isActive = true
    }
    
    internal func stopSpinning()-> void {
        UIApplication.showIndicator(by: false)
        DispatchQueue.main.async { [weak self] in
            self?.indicator.stopAnimating()
            self?.indicator.removeFromSuperview()
            self?.indicatorPanal.removeFromSuperview()
        }
    }
    
    internal func startSpinning(in view: UIView?)-> void {
        DispatchQueue.main.async { [weak self] in
            self?.setupSpinnerViews(in: view)
            self?.indicator.startAnimating()
        }
        UIApplication.showIndicator(by: true)
    }
    
}


internal extension String {
    
    internal static var empty: string {
        return ""
    }
    
    internal static var nixStringMessage: string {
        return "returns none data"
    }
    
    internal var isLink: Bool {
        return contains("http://") || contains("https://")
    }
    
    internal var isEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPredicate.evaluate(with: self)
    }
    
    internal func convertToEnNumbers()-> string {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "EN")
        return formatter.number(from: self)?.stringValue ?? string.empty
    }
    
    internal func toData()-> Data {
        return data(using: .utf8) ?? Data()
    }
    
}

internal extension Number {
    
    internal static var zero: Number {
        return 0
    }
    
}



// image caching
private let imageCache = NSCache<NSString,UIImage>()
internal extension UIImageView {
    
    internal func loadImage(from link: string, onComplete: ((_ isLoaded: Bool)-> void)?)-> void {
        UIApplication.showIndicator(by: true)
        image = nil
        if (!link.isLink || link.isEmpty) {
            print("empty image link or invalid link")
            onComplete?(false)
            UIApplication.showIndicator(by: false)
            return
        }
        
        if let cachedImage = imageCache.object(forKey: link as NSString) {
            image = cachedImage
            onComplete?(true)
            UIApplication.showIndicator(by: false)
            return
        }
        // download and cache image
        // download
        if let url = URL(string: link as string) {
            URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
                if (error != nil) {
                    onComplete?(false)
                    UIApplication.showIndicator(by: false)
                    return
                }
                if let imageData = data, let imageToCache = UIImage(data: imageData) {
                    DispatchQueue.main.async { [weak self] in
                        // cache image
                        imageCache.setObject(imageToCache, forKey: link as NSString)
                        self?.image = imageToCache
                        onComplete?(true)
                        UIApplication.showIndicator(by: false)
                    }
                } else {
                    onComplete?(false)
                    UIApplication.showIndicator(by: false)
                }
                }.resume()
        }
    }
    
    internal func rounded(with color: Color = Color.clear, and width: cgFloat = 0.0)-> void {
        clipsToBounds = true
        layer.cornerRadius = bounds.height/2
        layer.borderColor = color.cgColor 
        layer.borderWidth = width
    }
    
}



internal extension UIApplication {
    
    internal class func showIndicator(by isOn: bool)-> void {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = isOn
        }
    }
    
    internal class func playActions()-> void {
        if (UIApplication.shared.isIgnoringInteractionEvents) {
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
    
    internal class func pauseActions()-> void {
        if (!UIApplication.shared.isIgnoringInteractionEvents) {
            UIApplication.shared.beginIgnoringInteractionEvents()
        }
    }
    
    internal class func open(this link: string)-> void {
        guard let url = URL(string: link), UIApplication.shared.canOpenURL(url) else {
            print("can't open link")
            return
        }
        UIApplication.shared.openURL(url)
    }
    
}

// uicolor

internal extension UIColor {
    
    internal class func rgb(red: Int, green: Int, blue: Int, alpha: Float)-> UIColor {
        return UIColor(colorLiteralRed: Float(red)/255, green: Float(green)/255, blue: Float(blue)/255, alpha: alpha)
    }
    
    internal convenience init?(hex hash: string) {
        let r, g, b, a: CGFloat
        if (hash.hasPrefix("#")) {
            let start = hash.index(hash.startIndex, offsetBy: 1)
            let hexColor = hash.substring(from: start)
            let scanner = Scanner(string: hexColor)
            var hexNumber: UInt64 = 0
            if (scanner.scanHexInt64(&hexNumber)) {
                r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                a = CGFloat(hexNumber & 0x000000ff) / 255
                
                self.init(red: r, green: g, blue: b, alpha: a)
                return
            }
        }
        return nil
    }
    
    
    
}


// uiview

internal extension UIView {
    
    internal func setShadow(with offset: CGSize, radius: CGFloat, opacity: CGFloat, color: Color)-> Void {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOffset = offset
        layer.shadowRadius = radius
        layer.shadowOpacity = Float(opacity)
    }
    
    internal func setBorder(with width: cgFloat, and color: Color)-> void {
        layer.borderColor = color.cgColor
        layer.borderWidth = width
    }
    
    internal func setCorner(radius: cgFloat)-> void {
        layer.cornerRadius = radius
    }
    
}

internal typealias string = String
internal typealias void = Void
internal typealias any = Any
internal typealias double = Double
internal typealias bool = Bool
internal typealias float = Float
internal typealias int = Int
internal typealias Number = NSNumber
internal typealias object = AnyObject
internal typealias cgFloat = CGFloat
internal typealias Color = UIColor
internal typealias Font = UIFont
internal typealias Size = CGSize
internal typealias Rect = CGRect
internal typealias Insets = UIEdgeInsets
internal typealias uint = UInt
internal typealias Object = NSObject
internal typealias null = NSNull
internal typealias Point = CGPoint



@IBDesignable
internal class TextView: UITextView, UITextViewDelegate
{
    @IBInspectable
    internal var placeholder: String = "placholder" {
        didSet {
            setup()
        }
    }
    
    @IBInspectable
    internal var placeholderColor: UIColor = Color.lightGray {
        didSet {
            setup()
        }
    }
    
    @IBInspectable
    internal var color: UIColor = Color.black {
        didSet {
            setup()
        }
    }
    
    
    internal override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        setup()
    }
    
    required internal init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
    
    private func setup()-> void {
        delegate = self
        text = placeholder
        textColor = placeholderColor
    }
    
    internal func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text == placeholder && textView.textColor == placeholderColor) {
            textView.text = string.empty
            textView.textColor = color
        }
    }
    
    internal func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text.isEmpty) {
            textView.text = placeholder
            textView.textColor = placeholderColor
        }
    }
    
    
    
    
}


extension Size
{
    
    init(right: cgFloat, top: cgFloat) {
        width = right
        height = top
    }
    
}


// MARK: - Map view

extension MKMapView
{
    
    internal func setupCamera()-> void {
        camera.altitude = 1400
        camera.pitch = 50
        camera.heading = 180
    }
    
    internal func setAnnotation(in point: CLLocationCoordinate2D, with title: string, and subTitle: string)-> void {
        let annotation = MKPointAnnotation()
        annotation.coordinate = point
        annotation.title = title
        annotation.subtitle = subTitle
        addAnnotation(annotation)
    }
    
//    internal func setCustomAnnotation(in point: CLLocationCoordinate2D, with title: String, and subTitle: string)-> void {
//        
//    }
    
    internal func setRegion(in point: CLLocationCoordinate2D)-> void {        
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let region = MKCoordinateRegion(center: point, span: span)
        setRegion(region, animated: true)
        //setupCamera()
    }
    
}


// MARK:- Fonts

extension Font
{
    
    internal class func getFonts()-> void {
        Font.familyNames.forEach { (family) in
            print("--------------------------\(family)--------------------------")
            Font.fontNames(forFamilyName: family).forEach { (font) in
                print("font name is: ", font)
            }
        }
    }
    
}

































