
import Foundation


final class LocalizeStrings
{
    
    
    // MARK: - Arabic strings
    
    internal static let arabicWords: [string: string] = [
        "warninglan" : "",//تحذير
        "internentconnectionlan": "لا يوجد اتصال بالانترنت",
        "firstnamereqlan": "الاسم الاول مطلوب",
        "secondnamereqlan": "الاسم الاخير مطلوب",
        "usernamereqlan": "اسم المستخدم مطلوب",
        "invalidemaillan": "بريد غير صالح",
        "phonenumberreqlan": "مطلوب رقم الهاتف",
        "passwordreqlan": "ادخل كلمة مرور",
        "repasswordreqlan": "اكد كلمة المرور",
        "unconfirmedpasswordlan": "كلمات المرور غير متطابقة",
        "errorlan": "حدث خطآ",
        "openlocationlan": "يرجي تفعيل خاصية تحديد الموقع",
        "invalidlocationlan": "موقع غير صالح",
        "passwordcountlan": "كلمة مرور غير صحيحه",
        "profilelan": "الملف الشخصي",
        "mainlan": "الرئيسية",
        "basketlan": "سلة المشتريات",
        "promotionslan": "العروض",
        "shopslan": "المتاجر",
        "favoritslan": "المفضله",
        "orderslan": "الطلبات الحالية",
        "settingslan": "الإعدادات",
        "aboutapplan": "عن التطبيق",
        "updateprofilelan": "تعديل حسابي",
        "logoutlan": "تسجيل خروج",
        "chanageapplangaugelan": "لغة التطبيق",
        "firstNamelan": "الاسم الاول",
        "lastnamelan": "الاسم الثاني",
        "usernamelan": "اسم المستخدم",
        "emaillan": "البريد الالكتروني",
        "phonenumberlan": "رقم الهاتف",
        "passwordlan": "كلمة المرور",
        "re-passwordlan": "اعادة كلمة المرور",
        "malelan": "ذكر",
        "femalelan": "انثي",
        "registerlan": "تسجيل",
        "newsignuplan": "تسجيل جديد",
        "editprofillan": "تعديل",
        "nodatalan": "لا يوجد بيانات",
        "showinmenulan": "عرض في القائمة",
        "departmentstitlelan": "الاقسام",
        "currentorderstitlelan": "الطلبات الحالية",
        "storeinfotitlelan": "حول المتجر",
        "storeproductstitlelan": "المنتجات",
        "locationtitlelan": "الموقع",
        "typestitlelan": "التخصص",
        "productscounttitlelan": "عدد المنتجات",
        "ratetitlelan": "التقييم",
        "producttitlelan": "منتج",
        "sortedbytitlelan": "ترتيب حسب",
        "submittitlelan": "موافق",
        "pricetitlelan": "السعر",
        "sectiontitlelan": "القسم",
        "alltitlelan": "الكل",
        "Juicestitlelan": "عصائر",
        "cocktailtitlelan": "كوكتيل",
        "icecreamtitlelan": "ايس كريم",
        "Colddrinkstitlelan": "المشروبات الباردة",
        "Softdrinkstitlelan": "المشروبات الغازية",
        "defaulttitlelan": "افتراضي",
        "fromhieghestpricetitlelan": "من السعر الاكثر الي الاقل",
        "fromlowestpricelan": "من السعر الاقل الي الاكثر",
        "totaltitlelan": "الاجمالي: ",
        "sendordertitlelan": "ارسال طلب",
        "srtitlelan": "ر.س",
        "shoppingtitlelan": "متابعة عملية الشراء",
        "Choosethedeliverymethodlan": "اختيار طريقة التوصيل",
        "deliverybystorelan": "توصيل من قبل المتجر",
        "receivingorderlan": "استلام الطلب",
        "continuationlan": "متابعة",
        "nametitlelan": "الاسم",
        "buildingandhousenumberlan": "العمارة - رقم البيت",
        "floortitlelan": "الدور",
        "sendtitlelan": "ارسال",
        "addtitlelan": "اضف عنوان",
        "somedatareqlan": "بعض البيانات طلوبة",
        "Recipient'snametitlelan": "اسم المستلم",
        "Recipient'saddtitlelan": "اضف مستلم",
        "descriptiontitlelan": "وصف",
        "orderstatetitlelan": "حالة الطلب",
        "storetitlelan": "متجر",
        "deliverymodetitlelande": "طريقة التوصيل",
        "buildingtitlelantow": "العمارة",
        "orderdatetitlelan": "تاريخ الطلب",
        "quantitytitlelan": "الكمية",
        "sendcomplainttitlelan": "ارسال شكوي",
        "complainttitlelantile": "عنوان الشكوي",
        "complaintcontenttitlelan": "نص الشكوي",
        "addratetitlelan": "اضافة تقييم",
        "Forgotpasswordtitlelan": "هل نسيت كلمة المرور...؟",
        "logintitlelan": "تسجيل دخول",
        "btnregistertitlelan": "هل هذة زيارتك الاولي للتطبيق.؟ تسجيل جديد",
        "Quantityavailabletitlelan": "الكمية المتوفرة",
        Localization.Recipient: "المستلم",
        Localization.Mobile: "هاتف",
        Localization.MyAccount: "الملف الشخصي"
        
    ]
    
    
    
    // MARK: - English words
    
    internal static let englishWords: [string: string] = [
        "warninglan" : "",//Warning
        "internentconnectionlan": "No Internet Connection",
        "firstnamereqlan": "First name reqiured",
        "secondnamereqlan": "Last name Reqiured",
        "usernamereqlan": "User name reqiured",
        "invalidemaillan": "Invalid email",
        "phonenumberreqlan": "Phone number reqiured",
        "passwordreqlan": "Enter password",
        "repasswordreqlan": "Re-enter password",
        "unconfirmedpasswordlan": "Password not confirmed",
        "errorlan": "Error Occurred",
        "openlocationlan": "Location has been closed",
        "invalidlocationlan": "Invalid Location",
        "passwordcountlan": "Worng password",
        "profilelan": "Profile",
        "mainlan": "Main",
        "basketlan": "Basket",
        "promotionslan": "Deals",
        "shopslan": "Stores",
        "favoritslan": "Favourites",
        "orderslan": "Current requests",
        "settingslan": "Settings",
        "aboutapplan": "About app",
        "updateprofilelan": "Modify my account",
        "logoutlan": "Logout",
        "chanageapplangaugelan": "Application Language",
        "firstNamelan": "First Name",
        "lastnamelan": "Last Name",
        "usernamelan": "User Name",
        "emaillan": "Email",
        "phonenumberlan": "Phone", //Number
        "passwordlan": "Password",
        "re-passwordlan": "Re-enter Password",
        "malelan": "Male",
        "femalelan": "Female",
        "registerlan": "Register",
        "newsignuplan": "SignUp",
        "editprofillan": "Update",
        "nodatalan": "No data",
        "showinmenulan": "Display in list",
        "departmentstitlelan": "Department",
        "currentorderstitlelan": "Current requests",
        "storeinfotitlelan": "About Shop",
        "storeproductstitlelan": "Products",
        "locationtitlelan": "Site",
        "typestitlelan": "Speciality",
        "productscounttitlelan": "Number of products",
        "ratetitlelan": "Evaluation",
        "producttitlelan": "Product",
        "sortedbytitlelan": "Arrange by",
        "submittitlelan": "ok",
        "pricetitlelan": "Price",
        "sectiontitlelan": "Department",
        "alltitlelan": "All",
        "Juicestitlelan": "Juices",
        "cocktailtitlelan": "Cocktail",
        "icecreamtitlelan": "Ice cream",
        "Colddrinkstitlelan": "Cold drinks",
        "Softdrinkstitlelan": "Soft drinks",
        "defaulttitlelan": "default",
        "fromhieghestpricetitlelan": "Of the highest price",
        "fromlowestpricelan": "Of the lowest price",
        "totaltitlelan": "Total: ",
        "sendordertitlelan": "Send request",
        "srtitlelan": "SAR",
        "shoppingtitlelan": "Continue your purchase",
        "Choosethedeliverymethodlan": "Choose the delivery method",
        "deliverybystorelan": "Delivery by store",
        "receivingorderlan": "Receipt of the request",
        "continuationlan": "Continue",
        "nametitlelan": "Name",
        "buildingandhousenumberlan": "Housing number",
        "floortitlelan": "Floor",
        "sendtitlelan": "Send",
        "addtitlelan": "Add Address",
        "somedatareqlan": "Some data reqiured",
        "Recipient'snametitlelan": "Recipient's name",
        "Recipient'saddtitlelan": "Add Recipient",
        "descriptiontitlelan": "Description",
        "orderstatetitlelan": "Order Status",
        "storetitlelan": "Store",
        "deliverymodetitlelande": "Method of connection",
        "buildingtitlelantow": "Housing number:",//"Building",
        "orderdatetitlelan": "Order Date",
        "quantitytitlelan": "Quantity",
        "sendcomplainttitlelan": "Send Complaint",
        "complainttitlelantile": "Complaint Title",
        "complaintcontenttitlelan": "Complaint Text",
        "addratetitlelan": "Add rating",
        "Forgotpasswordtitlelan": "Forgot password...?",
        "logintitlelan": "Login",
        "btnregistertitlelan": "Is this your first visit to the application? Register",
        "Quantityavailabletitlelan": "Quantity available",
        Localization.Recipient: "Recipient",
        Localization.Mobile: "Mobile",
        Localization.MyAccount: "My account"
    ]
        
    
    
    
    
}















