
import UIKit
import Localize_Swift

final class Main: BaseController
{
    
    // MARK: - Constants
    
    // MARK: - Variables
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var btnMenu: UIBarButtonItem!
    @IBOutlet fileprivate weak var btnTaskNow: Button!
    @IBOutlet fileprivate weak var btnTaskLater: Button!
    @IBOutlet fileprivate weak var btnProfile: UIButton!
    @IBOutlet fileprivate weak var calender: UIDatePicker! {
        didSet {
            calender.backgroundColor = .white
            calender.addTarget(self, action: #selector(calenderOnValueChanged(_:)), for: .valueChanged)
        }
    }
    @IBOutlet fileprivate weak var datePickerPanal: UIView! {
        didSet {
            datePickerPanal.isHidden = true
            datePickerPanal.translatesAutoresizingMaskIntoConstraints = false
            datePickerPanal.backgroundColor = Color.black.withAlphaComponent(0.6)
            datePickerPanal.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(datePickerPanalOnTap(_:))))
        }
    }
    @IBOutlet weak var btnChooseDate: UIButton!
    @IBOutlet weak var btnCloseDatePicker: UIButton!
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
        setupCalender()
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addObservers()
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Actions
    
    @IBAction private func taskOrderNowClicked(_ sender: UIButton) {
        if let missionsScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Missions) as? Mission {
            missionsScreen.typeTime = 0
            navigationController?.pushViewController(missionsScreen, animated: true)
        }
    }

    @IBAction private func taskOrderLaterClicked(_ sender: UIButton) {
        datePickerPanal.isHidden = false
    }
    
    @IBAction private func gotoProfileOnClick(_ sender: UIButton) {
        print(#function)
        if let profileScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Profile) as? Profile {
            revealViewController().pushFrontViewController(UINavigationController(rootViewController: profileScreen), animated: true)
        }
    }
    
    @objc private func calenderOnValueChanged(_ sender: UIDatePicker) {
     
    }
    
    @objc private func datePickerPanalOnTap(_ gesture: UITapGestureRecognizer)-> void {
        switch(gesture.state) {
        case .ended:
            gesture.view?.isHidden = true
        default:
            break
        }
    }
    
    @IBAction private func chooseDateOnClick(_ sender: UIButton) {
        datePickerPanal.isHidden = true
        if let missionsScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Missions) as? Mission {
            missionsScreen.typeTime = 1
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .short
            missionsScreen.laterDate = dateFormatter.string(from: calender.date)
            navigate(to: missionsScreen)
        }
    }
    
    @IBAction private func closeDatePickerOnClick(_ sender: UIButton) {
        datePickerPanal.isHidden = true
    }
    
    
}

// MARK: - Helper functions

extension Main
{
    
    fileprivate func configureSideMenu()-> void {
        UIApplication.shared.isStatusBarHidden = false
        btnMenu.target = revealViewController()
        btnMenu.action = #selector(revealViewController().rightRevealToggle(_:))
        view.addGestureRecognizer(revealViewController().panGestureRecognizer())
    }
    
    fileprivate func addObservers()-> void {
        NotificationCenter.default.addObserver(self, selector: #selector(updateUi), name: Notification.Name(LCLLanguageChangeNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showNotificationPopup), name: Constants.Misc.Message, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToJourneyMap), name: Constants.Misc.UserJourney, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(navigateToEndedMission), name: Constants.Misc.MissionEnded, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(navigateToRateView), name: Constants.Misc.RateView, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showDriverMessagePopup), name: Constants.Misc.DriverMessage, object: nil)
    }
    
    @objc fileprivate func updateUi()-> void {
        configureSideMenu()
        btnTaskNow.setTitle("taskordernowlan".localized(), for: .normal)
        btnTaskLater.setTitle("taskorderlaterlan".localized(), for: .normal)
        btnTaskLater.titleLabel?.numberOfLines = 2
        btnTaskLater.imageView?.contentMode = .scaleAspectFit
        btnProfile.setTitle("gotoprofilemainlan".localized(), for: .normal)
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            calender.locale = Locale(identifier: "ar")
            btnProfile.semanticContentAttribute = .forceRightToLeft
            btnTaskNow.semanticContentAttribute = .forceRightToLeft
            btnTaskLater.semanticContentAttribute = .forceRightToLeft
            
            btnTaskNow.titleEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: 10)
            btnTaskNow.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: -30)            
            
            btnTaskLater.titleEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: 0)//-10
            btnTaskLater.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: -20)
            
            btnProfile.titleEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: -10)
            btnProfile.imageEdgeInsets = Insets(top: 0, left: 0, bottom: 0, right: -15)
            
        } else {
            calender.locale = Locale(identifier: "en")
            btnProfile.semanticContentAttribute = .forceLeftToRight
            btnTaskNow.semanticContentAttribute = .forceLeftToRight
            btnTaskLater.semanticContentAttribute = .forceLeftToRight
            
            btnTaskNow.titleEdgeInsets = Insets(top: 0, left: -30, bottom: 0, right: 0)
            btnTaskNow.imageEdgeInsets = Insets(top: 0, left: -60, bottom: 0, right: 0)
            
            
            btnTaskLater.titleEdgeInsets = Insets(top: 0, left: 30, bottom: 0, right: 0)
            btnTaskLater.imageEdgeInsets = Insets(top: 0, left: 5, bottom: 0, right: 0)
            
            btnProfile.titleEdgeInsets = Insets(top: 0, left: -30, bottom: 0, right: 0)
            btnProfile.imageEdgeInsets = Insets(top: 0, left: -95, bottom: 0, right: 0)
        }
        btnChooseDate.setTitle("datepickechoosebuttonlan".localized(), for: .normal)
        btnCloseDatePicker.setTitle("datepickerclosebuttonlan".localized(), for: .normal)
    }
    
    fileprivate func setupCalender()-> void {
        view.addSubview(datePickerPanal)
        datePickerPanal.isHidden = true
        datePickerPanal.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        datePickerPanal.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        datePickerPanal.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        datePickerPanal.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
    }
    
    @objc private func showNotificationPopup()-> void {
        openNotificationPopup(who: who, name: driverName, driverId: driverId, avatar: driverAvatar, driverPhone: driverPhone, salary: salary, salaryType: salaryType, callid: callId, carId: carNumber, carColor: carColor, carTypeA: carTypeArabic, carTypeE: carTypeEnglish)
    }
    
    @objc private func showDriverMessagePopup()-> void {
        openDriverMessagePopup(who: who, name: driverName, driverId: driverId, avatar: driverAvatar, driverPhone: driverPhone, salary: salary, salaryType: salaryType, callid: callId, carId: carNumber, carColor: carColor, carTypeA: carTypeArabic, carTypeE: carTypeEnglish)
    }
    
    @objc private func navigateToJourneyMap()-> void {
        if let journeyScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.UserLocation) as? UserLocationUpdate {
            navigate(to: journeyScreen)
        }
    }
    
    
}




























