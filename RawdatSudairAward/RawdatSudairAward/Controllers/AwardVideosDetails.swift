
import UIKit
import Localize_Swift

final class AwardVideosDetails: BaseController
{
    // MARK: - Constants
    
    //MARK: - Variables
    
    fileprivate var dataSource = Array<Video>() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tblVideos.reloadData()
            }
        }
    }
    
    fileprivate lazy var webView: UIWebView = {
        let wv = UIWebView()
        wv.backgroundColor = .black
        wv.tintColor = .black
        wv.translatesAutoresizingMaskIntoConstraints = false
        return wv
    }()
    
    internal var video: Video?, videos: [Video]?
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var playerPanal: UIView!
    @IBOutlet private weak var controls: UIView!
    @IBOutlet private weak var poster: UIImageView!
    @IBOutlet fileprivate weak var lblVidDetails: UILabel!
    @IBOutlet fileprivate weak var lblMoreTitle: UILabel!
    @IBOutlet fileprivate weak var tblVideos: UITableView! {
        didSet {
            tblVideos.delegate = self
            tblVideos.dataSource = self
        }
    }
    @IBOutlet private weak var playerViewHeight: NSLayoutConstraint!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        
        setupPlayerView()
    }
    
    internal override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        playerViewHeight.constant = view.bounds.height*0.40
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBar()
        updateUi()
    }
    
    
    //MARK: - Actions
    
    @objc fileprivate func shareOnClick(_ sender: UIBarButtonItem) {
        share(this: dataSource.map { $0.link ?? string.empty })
    }
    
}



// MARK: - Helper functions

extension AwardVideosDetails
{
    
    fileprivate func updateNavigationBar()-> void {
        navigationItem.title = video?.title ?? string.empty
        updateNavigationBarDirections()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: getBackImage(), style: .plain, target: self, action: #selector(goBack(_:)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "sharing"), style: .plain, target: self, action: #selector(shareOnClick(_:)))
        tblVideos.register(Constants.Nib.AwardVideos, forCellReuseIdentifier: Constants.ReuseIdentifier.AwardVideos)
    }
    
    fileprivate func updateUi()-> void {
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            lblMoreTitle.textAlignment = .right
            lblVidDetails.textAlignment = .right
        } else {
            lblMoreTitle.textAlignment = .left
            lblVidDetails.textAlignment = .left
        }
        lblVidDetails.text = video?.content ?? string.empty
        if let allVideos = videos {
            dataSource = allVideos
            let vidId = (dataSource.first?.link ?? string.empty).components(separatedBy: "=").last ?? string.empty
            openVideo(by: vidId)
            tblVideos.reloadData()
        }
        lblMoreTitle.text = "morevideoslan".localized()
    }
    
    fileprivate func openVideo(by videoId: string)-> void {
        print("video id is: \(videoId)")
        let width = view.bounds.width-10, height = view.bounds.height*0.45-35
        let videoFrame = "<iframe width=\"\(width)\" height=\"\(height)\" src=\"https://www.youtube.com/embed/\(videoId)?&playsinline=1\" frameborder=\"0\" allowfullscreen></iframe>"
        webView.allowsInlineMediaPlayback = true
        webView.loadHTMLString(videoFrame, baseURL: nil)
    }
    
    fileprivate func setupPlayerView()-> void {
        webView.delegate = self
        playerPanal.addSubview(webView)
        webView.topAnchor.constraint(equalTo: playerPanal.topAnchor, constant: 0).isActive = true
        webView.rightAnchor.constraint(equalTo: playerPanal.rightAnchor, constant: 0).isActive = true
        webView.bottomAnchor.constraint(equalTo: playerPanal.bottomAnchor, constant: 0).isActive = true
        webView.leftAnchor.constraint(equalTo: playerPanal .leftAnchor, constant: 0).isActive = true
        
    }

}


// MARK: - Table view delegate & data source functions

extension AwardVideosDetails: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.AwardVideos) as? AwardVideosCell {
            cell.video = dataSource[indexPath.row]
            cell.lblTitle.textColor = .white
            return cell
        }        
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        // run video
        let row = dataSource[indexPath.row]
        let vidId = (row.link ?? string.empty).components(separatedBy: "=").last ?? string.empty
        openVideo(by: vidId)
        lblVidDetails.text = row.title
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    internal func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundView?.backgroundColor = .clear
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
    }
    
}



// MARK: - Web view delegate functions

extension AwardVideosDetails: UIWebViewDelegate
{
    
    internal func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.showIndicator(by: true)
    }
    
    internal func webViewDidFinishLoad(_ webView: UIWebView) {
        UIApplication.showIndicator(by: false)
    }
    
    internal func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        UIApplication.showIndicator(by: false)
    }
    
}



























