
import UIKit
import Gemo

final class SignIn: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var usertype = Usertype.main
    private var userViewModel: UserViewModel?
    
    // MARK: - Outlets
    
    @IBOutlet weak var txfUsername: TextField!
    @IBOutlet weak var txfPassword: TextField!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnLoginAsMasterUser: UIButton!
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        usertype = Usertype(rawValue: UserDefaults.standard.integer(forKey: Misc.UserTypeKey)) ?? .main
        userViewModel = UserViewModel()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar(isLogo: true, isMainPage: false)
    }
    
    internal override func updateUi() {
        super.updateUi()
        btnLoginAsMasterUser.setCorner(radius: 3)
        btnLoginAsMasterUser.setShadow(with: .zero, radius: 0.7, opacity: 0.8, color: .main)
        btnLoginAsMasterUser.setTitle((usertype == .main) ? Localization.SiginAsMasterUser.localized() : Localization.Login.localized(), for: .normal)
        btnForgotPassword.setTitle(Localization.ForgotPassword.localized(), for: .normal)
        if (usertype == .main) {
            txfUsername.hint = Localization.NameUsernameEmail.localized()
        } else {
            txfUsername.hint = Localization.UserName.localized()
        }
        txfPassword.hint = Localization.Password.localized()
        if(Localizer.instance.current == .arabic) {
            txfUsername.textAlignment = .right
            txfPassword.textAlignment = .right
            btnForgotPassword.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        } else {
            txfUsername.textAlignment = .left
            txfPassword.textAlignment = .left
            btnForgotPassword.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        }
        btnForgotPassword.isHidden = (usertype == .sub)
    }
    
    
    // MARK: - Actions
    
    @IBAction func forgotPasswordOnCLick(_ sender: UIButton) {
        let alertController = UIAlertController(title: Localization.ForgotPassword.localized(), message: .empty, preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.placeholder = Localization.Email.localized()
            textField.textAlignment = (Localizer.instance.current == .arabic) ? .right : .left
            textField.keyboardType = .emailAddress
        }
        alertController.addAction(UIAlertAction(title: Localization.Send.localized(), style: .default, handler: { [weak self] (action) in
            let email = alertController.textFields?[0].text?.trimmingCharacters(in: .whitespaces) ?? .empty
            guard email.isEmail else {
                self?.displayAlert(with: Localization.InvaildEmail.localized())
                return
            }
            Gemo.gem.startSpinning()
            let paramters: Dictionary<String, any> = [
                Misc.MainKey: Misc.MainValue,
                Misc.LanguageKey: Localizer.instance.current.rawValue,
                "email": email
            ]
            Http.request(link: Misc.getUrl(for: "recover"), method: .post, parameters: paramters)
                .response { [weak self] (response) in
                    let error = response.error
                    if (error != nil) {
                        Gemo.gem.stopSpinning()
                        print("reset email error: \(string(describing: error))")
                        self?.displayAlert(with: error?.localizedDescription)
                        return
                    }
                    Gemo.gem.stopSpinning()
                    guard let json = (response.result as? [Dictionary<string, any>])?.first, let message = json["data"] as? string else {
                        self?.displayAlert(with: Localization.NoData.localized())
                        return
                    }
                    self?.displayAlert(with: message)
                }
        }))
        present(alertController, animated: true)
    }
    
    @IBAction func signInAsMasterOnCLick(_ sender: UIButton) {
        view.endEditing(true)
        let user = User()
        user.username = username
        user.password = password
        if (usertype == .main) {
            userViewModel?.signIn(for: user, in: self) { [weak self] (person) in
                Threads.main { [weak self] in
                    self?.userViewModel?.save(this: person)
                    UserDefaults.standard.set(true, forKey: Misc.UserLoggedInKey)
                    UserDefaults.standard.synchronize()
                    AppManager.configureSideMenu()
                }
            }
        } else {
            UserCar.signIn(for: user, self) { (userCar) in
                Threads.main {
                    userCar.save()
                    UserDefaults.standard.set(true, forKey: Misc.UserLoggedInKey)
                    UserDefaults.standard.synchronize()
                    AppManager.configureSideMenu()
                }
            }
        }
    }
    

}


// MARK: - Helper function

fileprivate extension SignIn {
    
   
}

// MARK: - Computed properties

fileprivate extension SignIn {
    
    var username: string {
        return txfUsername.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    var password: string {
        return txfPassword.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
}
























