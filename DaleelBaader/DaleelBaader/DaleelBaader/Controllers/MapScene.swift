
import UIKit

import Localize_Swift
import MapKit

final class MapScene: BaseController
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    internal var location: Location?
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var mapView: MKMapView! {
        didSet {
            mapView.delegate = self
            mapView.setupCamera()
            mapView.mapType = .standard
        }
    }
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        updateNavigationBar()
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadLocation()
    }
    
    //MARK: - Actions
    
    @IBAction private func changeMapTypeOnClick(_ sender: UISegmentedControl) {
        var mapType = MKMapType.standard
        switch (sender.selectedSegmentIndex) {
        case 0:
            mapType = .standard
            
        case 1:
            mapType = .hybrid
            
        case 2:
            mapType = .satellite
            
        default:
            break
        }
        mapView.mapType = mapType
    }
    
    @objc fileprivate func dismissOnClick(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    

}


// MARK: -  Helper funcations

extension MapScene
{
    
    fileprivate func loadLocation()-> void {
        guard let latitude = location?.latitude, let longitude = location?.longitude else {
            print("location error")
            return
        }
        
        let coordinate = mapView.makeLocationCoordinate(latitude: latitude, longitude: longitude)
        mapView.setRegion(in: coordinate)
        mapView.setAnnotation(in: coordinate, with: location?.title ?? string.empty, subTitle: string.empty)
             
        
    }
    
    fileprivate func updateNavigationBar()-> void {
        navigationItem.title = string.empty
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_close").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(dismissOnClick(_:)))
        navigationController?.navigationBar.tintColor = .black
    }
    
}



// MARK: - Map delegate functions

extension MapScene: MKMapViewDelegate
{
    
    internal func mapViewWillStartLoadingMap(_ mapView: MKMapView) {
        UIApplication.showIndicator(by: true)
    }
    
    internal func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        UIApplication.showIndicator(by: false)
    }
    
    internal func mapView(_ mapView: MKMapView, didFailToLocateUserWithError error: Error) {
        UIApplication.showIndicator(by: false)
    }
    
    
}
















