
import UIKit

final class StoreProducts: BaseController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var store: Shop?
    fileprivate let rightCellId = "com.right.cell.id.io", leftCellId = "com.left.cell.id.io"
    fileprivate var isRadio = false, selectedPrice = Number.zero.intValue, selectedSection = string.empty, sortingPanelBottom = NSLayoutConstraint()
    fileprivate var dataSource = Array<Product>() {
        didSet {
            ThreadQueue.main { [weak self] in
                self?.products.reloadData()
            }
        }
    }
    fileprivate var rightPanalDataSource = Array<string>() {
        didSet {
            ThreadQueue.main { [weak self] in
                self?.tblRightPanel.reloadData()
                self?.tblRightPanel.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
            }
        }
    }
    fileprivate var leftPanelDataSource = Array<string>(), sections = Array<Section>()
    fileprivate lazy var overlayPanel: UIImageView = { [weak self] in
        guard let this = self else { return UIImageView() }
        let overlay = UIImageView()
        overlay.backgroundColor = Color.black.withAlphaComponent(0.5)
        let tapGesture = UITapGestureRecognizer(target: this, action: #selector(bottomPanalTapped(_:)))
        overlay.addGestureRecognizer(tapGesture)
        overlay.isUserInteractionEnabled = true
        return overlay
        }()
    
    fileprivate lazy var sortingPanel: UIView = {
        let panal = UIView()
        panal.translatesAutoresizingMaskIntoConstraints = false
        panal.backgroundColor = .white
        panal.isUserInteractionEnabled = true
        panal.setShadow(with: .zero, radius: 0.6, opacity: 0.6, color: .darkGray)
        return panal
    }()
    
    fileprivate lazy var headerPanel: UIView = {
        let panal = UIView()
        panal.translatesAutoresizingMaskIntoConstraints = false
        panal.setBorder(with: 1, and: Color(white: 0.88, alpha: 0.9))
        panal.backgroundColor = .white
        return panal
    }()
    
    fileprivate lazy var lblHeaderTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .right
        label.text = Localization.SortedBy.localized()
        label.font = Font.boldSystemFont(ofSize: 11)
        return label
    }()
    
    fileprivate lazy var btnClose: UIButton = { [weak self] in
        guard let this = self else { return UIButton() }
        let btn = UIButton(type: .custom)
        btn.setImage(#imageLiteral(resourceName: "cancel"), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(this, action: #selector(closeBottomPanalOnCLick(_:)), for: .touchUpInside)
        return btn
        }()
    
    fileprivate var footerPanel: UIView = {
        let panel = UIView()
        panel.backgroundColor = .main
        panel.setShadow(with: .zero, radius: 0.8, opacity: 0.9, color: .main)
        panel.translatesAutoresizingMaskIntoConstraints = false
        return panel
    }()
    
    fileprivate lazy var btnSubmit: UIButton = { [weak self] in
        guard let this = self else { return UIButton() }
        let btn = UIButton(type: .custom)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(this, action: #selector(submitOnCLick(_:)), for: .touchUpInside)
        if (Localizer.instance.current == .arabic) {
            btn.semanticContentAttribute = .forceRightToLeft
            btn.imageEdgeInsets = Insets(top: 0, left: 20, bottom: 0, right: 0)
        } else {
            btn.semanticContentAttribute = .forceLeftToRight
            btn.imageEdgeInsets = Insets(top: 0, left: -20, bottom: 0, right: 0)
        }
        btn.setImage(#imageLiteral(resourceName: "ic-true"), for: .normal)
        btn.setTitle(Localization.Submit.localized(), for: .normal)
        return btn
        }()
    
    fileprivate lazy var tblRightPanel: UITableView = { [weak self] in
        guard let this = self else { return UITableView() }
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.alwaysBounceVertical = true
        tableView.backgroundColor = Color(white: 0.94, alpha: 1.0)
        tableView.register(RightCell.self, forCellReuseIdentifier: this.rightCellId)
        return tableView
        }()
    
    fileprivate lazy var tblLeftPanel: UITableView = { [weak self] in
        guard let this = self else { return UITableView() }
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.alwaysBounceVertical = true
        tableView.backgroundColor = .white
        tableView.register(LeftCell.self, forCellReuseIdentifier: this.leftCellId)
        return tableView
        }()
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var products: UICollectionView! {
        didSet {
            products.delegate = self
            products.dataSource = self
        }
    }
    @IBOutlet fileprivate weak var btnSort: UIButton!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        fetchSections()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBar.isHidden = true
        updateUi()
        if (Gemo.gem.isConnected) {
            loadData()
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }        
    }
    
    
    // MARK: - Actions
    
    @IBAction private func sortOnCLick(_ sender: UIButton) {
        openBottomPanal()
    }
    
    @objc fileprivate func addToFavoritesClicked(_ sender: UIButton) {
        guard let userId = UserDefaults.standard.string(forKey: Constants.Misc.UserID), let productId = dataSource[sender.tag].id else { return }
        add(to: Constants.Urls.Favorite, forUserId: userId, andProductId: productId)
    }
    
    @objc fileprivate func addToBasketClicked(_ sender: UIButton) {
        guard let userId = UserDefaults.standard.string(forKey: Constants.Misc.UserID), let productId = dataSource[sender.tag].id else { return }
        add(to: Constants.Urls.Basket, forUserId: userId, andProductId: productId)
    }
    
    @objc fileprivate func bottomPanalTapped(_ gesture: UITapGestureRecognizer) {
        closeBottomPanal()
    }
    
    @objc private func closeBottomPanalOnCLick(_ sender: UIButton) {
        closeBottomPanal()
    }
    
    @objc private func submitOnCLick(_ sender: UIButton) {
        closeBottomPanal()
        dataSource.removeAll()
        if (isRadio) {
            sortByPrice()
        } else {
            sortBySection()
        }
    }
    
}



// MARK: - Helper functions

extension StoreProducts
{
    
    fileprivate func updateUi()-> void {
        if (Localizer.instance.current == .arabic) {
            btnSort.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        } else {
            btnSort.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        }
    }
    
    fileprivate func setupCollectionView()-> void {
        products.register(Constants.Nibs.SectionDetails, forCellWithReuseIdentifier: Constants.ReuseIdentifiers.SectionDetails)
        products.alwaysBounceVertical = true
    }
    
    fileprivate func loadData()-> void {
        let parameters: Dictionary<string, any> = [
            Constants.Misc.MainKey: Constants.Misc.MainValue,
            "id_matjer": store?.id ?? .empty,
            Constants.Misc.LanguageBackendKey: Localizer.instance.current.rawValue
        ]
        Product.getData(from: Constants.Urls.StoreProducts, with: parameters, in: self) { [weak self] (products) in
            Gemo.gem.stopSpinning()
            if (!products.isEmpty) {
                self?.dataSource = products
            }
        }
    }
    
    fileprivate func sortByPrice()-> void {
        if (Gemo.gem.isConnected) {
            let parametres: [string: any] = [
                Constants.Misc.MainKey: Constants.Misc.MainValue,
                "id_matjer": store?.id ?? .empty,
                "price": (selectedPrice == 0) ? "" : selectedPrice.toString
            ]
            Product.getData(from: Constants.Urls.SortByPrice, with: parametres, in: self) { [weak self] (products) in
                Gemo.gem.stopSpinning()
                if (!products.isEmpty) {
                    self?.dataSource = products
                    ThreadQueue.main { [weak self] in
                        self?.products.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
                    }
                }
            }
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
    
    fileprivate func sortBySection()-> void {
        if (Gemo.gem.isConnected) {
            let parameters: [string: any] = [
                Constants.Misc.MainKey: Constants.Misc.MainValue,
                "id_matjer": store?.id ?? .empty,
                "id_cat": selectedSection
            ]
            Product.getData(from: Constants.Urls.SortBySection, with: parameters, in: self) { [weak self] (products) in
                Gemo.gem.stopSpinning()
                if (!products.isEmpty) {
                    self?.dataSource = products
                    ThreadQueue.main { [weak self] in
                        self?.products.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
                    }
                }
            }
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
    
    fileprivate func fetchSections()-> void {
        let allSection = Section()
        allSection.id = "all"
        allSection.title = Localization.All.localized()
        sections.append(allSection)
        Section.getSection(idStore: store?.id, self) { [weak self] (cats) in
            if (!cats.isEmpty) {
                self?.sections += cats
                ThreadQueue.main { [weak self] in
                    self?.setupSortingPanel()
                }
            }
        }
        /*Section.getData(page: self) { [weak self] (cats) in
            if (!cats.isEmpty) {
                self?.sections += cats
                ThreadQueue.main { [weak self] in
                    self?.setupSortingPanel()
                }
            }
        }*/
    }
    
    fileprivate func setupSortingPanel()-> void {
        guard let window = UIApplication.shared.keyWindow else { return }
        overlayPanel.frame = window.frame
        overlayPanel.alpha = 0.0
        overlayPanel.isHidden = true
        let height = window.frame.height*0.70
        window.addSubview(overlayPanel)
        window.addSubview(sortingPanel)
        sortingPanel.addSubview(headerPanel)
        headerPanel.addSubview(lblHeaderTitle)
        headerPanel.addSubview(btnClose)
        footerPanel.addSubview(btnSubmit)
        sortingPanel.addSubview(tblRightPanel)
        sortingPanel.addSubview(tblLeftPanel)
        sortingPanel.addSubview(footerPanel)
        // sorting panel constraints -> h,b,r,l
        sortingPanel.heightAnchor.constraint(equalToConstant: height).isActive = true
        sortingPanel.rightAnchor.constraint(equalTo: window.rightAnchor).isActive = true
        sortingPanelBottom = sortingPanel.bottomAnchor.constraint(equalTo: window.bottomAnchor, constant: height)
        sortingPanelBottom.isActive = true
        sortingPanel.leftAnchor.constraint(equalTo: window.leftAnchor).isActive = true
        // header panal constaraints-> h,t,l,r
        headerPanel.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        headerPanel.topAnchor.constraint(equalTo: sortingPanel.topAnchor).isActive = true
        headerPanel.rightAnchor.constraint(equalTo: sortingPanel.rightAnchor).isActive = true
        headerPanel.leftAnchor.constraint(equalTo: sortingPanel.leftAnchor).isActive = true
        // header label constraints-> cy, r, l
        lblHeaderTitle.centerYAnchor.constraint(equalTo: headerPanel.centerYAnchor).isActive = true
        // close  button constraints-> cy, l
        btnClose.centerYAnchor.constraint(equalTo: headerPanel.centerYAnchor, constant: 0.0).isActive = true
        
        // footer panel constraints-> r,b,h,l
        footerPanel.rightAnchor.constraint(equalTo: sortingPanel.rightAnchor).isActive = true
        footerPanel.bottomAnchor.constraint(equalTo: sortingPanel.bottomAnchor).isActive = true
        footerPanel.leftAnchor.constraint(equalTo: sortingPanel.leftAnchor).isActive = true
        footerPanel.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        // submit button constraints-> t,r,b,l
        btnSubmit.topAnchor.constraint(equalTo: footerPanel.topAnchor).isActive = true
        btnSubmit.rightAnchor.constraint(equalTo: footerPanel.rightAnchor).isActive = true
        btnSubmit.bottomAnchor.constraint(equalTo: footerPanel.bottomAnchor).isActive = true
        btnSubmit.leftAnchor.constraint(equalTo: footerPanel.leftAnchor).isActive = true
        // right panel constraints-> w,t,r,b
        tblRightPanel.topAnchor.constraint(equalTo: headerPanel.bottomAnchor, constant: 0.0).isActive = true
        
        tblRightPanel.bottomAnchor.constraint(equalTo: footerPanel.topAnchor, constant: 0.0).isActive = true
        tblRightPanel.widthAnchor.constraint(equalToConstant: window.frame.width*0.35).isActive = true
        // left panel constraints-> t,r,b,l
        tblLeftPanel.topAnchor.constraint(equalTo: headerPanel.bottomAnchor, constant: 0.0).isActive = true
        tblLeftPanel.bottomAnchor.constraint(equalTo: footerPanel.topAnchor, constant: 0.0).isActive = true
        if (Localizer.instance.current == .arabic) {
            lblHeaderTitle.textAlignment = .right
            btnClose.leftAnchor.constraint(equalTo: headerPanel.leftAnchor, constant: 8.0).isActive = true
            lblHeaderTitle.leftAnchor.constraint(equalTo: btnClose.rightAnchor, constant: 8.0).isActive = true
            lblHeaderTitle.rightAnchor.constraint(equalTo: headerPanel.rightAnchor, constant: -8.0).isActive = true
            tblRightPanel.rightAnchor.constraint(equalTo: sortingPanel.rightAnchor).isActive = true
            tblLeftPanel.rightAnchor.constraint(equalTo: tblRightPanel.leftAnchor).isActive = true
            tblLeftPanel.leftAnchor.constraint(equalTo: sortingPanel.leftAnchor, constant: 0.0).isActive = true
        } else {
            lblHeaderTitle.textAlignment = .left
            lblHeaderTitle.leftAnchor.constraint(equalTo: headerPanel.leftAnchor, constant: 8.0).isActive = true
            btnClose.rightAnchor.constraint(equalTo: headerPanel.rightAnchor, constant: -8.0).isActive = true
            lblHeaderTitle.rightAnchor.constraint(equalTo: btnClose.leftAnchor, constant: 8.0).isActive = true
            tblRightPanel.leftAnchor.constraint(equalTo: sortingPanel.leftAnchor).isActive = true
            tblLeftPanel.rightAnchor.constraint(equalTo: sortingPanel.rightAnchor).isActive = true
            tblLeftPanel.leftAnchor.constraint(equalTo: tblRightPanel.rightAnchor).isActive = true
        }
        isRadio = false
        rightPanalDataSource = [Localization.Section.localized() , Localization.Price.localized()]
        tblLeftPanel.reloadData()
        if let cell = tblLeftPanel.cellForRow(at: IndexPath(row: 0, section: 0)) as? LeftCell {
            cell.checkBox.isChecked = true
            cell.lblTitle.textColor = .main
            if (!sections.isEmpty) {
                tblLeftPanel.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
            }
        }
    }
    
    fileprivate func openBottomPanal()-> void {
        overlayPanel.isHidden = false
        sortingPanelBottom.constant = 0
        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseInOut, animations: {
            [weak self] in
            UIApplication.shared.keyWindow?.layoutIfNeeded()
            self?.overlayPanel.alpha = 1.0
        })
    }
    
    fileprivate func closeBottomPanal()-> void {
        sortingPanelBottom.constant = UIApplication.shared.keyWindow!.frame.height*70
        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseInOut, animations: {
            [weak self] in
            UIApplication.shared.keyWindow?.layoutIfNeeded()
            self?.overlayPanel.alpha = 0.0
        }) {  [weak self] (hasFinished) in
            self?.overlayPanel.isHidden = true
        }
    }
    
}


extension StoreProducts
{
    internal override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == products) {
            return dataSource.count
        } else {
            return super.collectionView(collectionView, numberOfItemsInSection: section)
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (collectionView == products) {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ReuseIdentifiers.SectionDetails, for: indexPath) as? SectionDetailsCell else {
                return super.collectionView(collectionView, cellForItemAt: indexPath)
            }
            cell.btnLeft.setImage(#imageLiteral(resourceName: "add-favorite"), for: .normal)
            cell.btnLeft.tag = indexPath.item
            cell.btnRight.tag = indexPath.item
            cell.btnLeft.addTarget(self, action: #selector(addToFavoritesClicked(_:)), for: .touchUpInside) // favorite button
            cell.btnRight.addTarget(self, action: #selector(addToBasketClicked(_:)), for: .touchUpInside) // basket button
            cell.product = dataSource[indexPath.item]
            return cell
        } else {
            return super.collectionView(collectionView, cellForItemAt: indexPath)
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView == products) {
            print("selected item is: \(indexPath)")
            guard let productDetailsPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.ProductDetails) as? ProductDetails else { return }
            productDetailsPage.productId = dataSource[indexPath.item].id
            navigate(to: productDetailsPage)
        } else {
            super.collectionView(collectionView, didSelectItemAt: indexPath)
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (collectionView == products) {
            return Size(width: collectionView.frame.width, height: collectionView.frame.height*0.33)
        } else {
            return super.collectionView(collectionView, layout: collectionViewLayout, sizeForItemAt: indexPath)
        }
    }
    
    
}



// MARK: - Table view delegate & data source functions

extension StoreProducts: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == tblRightPanel) {
            return rightPanalDataSource.count
        } else {
            if (isRadio) {
                return leftPanelDataSource.count
            } else {
                return sections.count
            }
        }
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (tableView == tblRightPanel) {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: rightCellId, for: indexPath) as? RightCell else { return UITableViewCell() }
            cell.title = rightPanalDataSource[indexPath.row]
            return cell
        } else { // left table view
            guard let cell = tableView.dequeueReusableCell(withIdentifier: leftCellId, for: indexPath) as? LeftCell else { return UITableViewCell() }
            cell.isRadio = isRadio
            if (isRadio) {
                cell.title = leftPanelDataSource[indexPath.row]
            } else {
                cell.section = sections[indexPath.row]
                /*let isFound = selectedSections.contains { $0.key == indexPath.row }
                 if (isFound) {
                 cell.checkBox.isChecked = true
                 cell.lblTitle.textColor = .main
                 } else {
                 cell.checkBox.isChecked = false
                 cell.lblTitle.textColor = .black
                 }*/
            }
            return cell
        }
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (tableView == tblRightPanel) {
            if (indexPath.row == 0) {
                isRadio = false
                tblLeftPanel.reloadData()
                tblLeftPanel.selectRow(at: IndexPath(row: selectedPrice, section: 0), animated: true, scrollPosition: .top)
            } else {
                isRadio = true
                leftPanelDataSource = [Localization.Default, Localization.LowestPrice, Localization.HighestPrice]
                tblLeftPanel.reloadData()
                tblLeftPanel.selectRow(at: IndexPath(row: selectedPrice, section: 0), animated: true, scrollPosition: .top)
            }
        } else {
            if (isRadio) {
                // selected price
                selectedPrice = indexPath.row
            } else {
                selectedSection = sections[indexPath.row].id ?? .empty
                // checked sections
                /*let isFound = selectedSections.contains { $0.key == indexPath.row }
                 if (isFound) {
                 // remove from dictionary and update views
                 selectedSections.removeValue(forKey: indexPath.row)
                 if let cell = tableView.cellForRow(at: indexPath) as? LeftCell {
                 cell.checkBox.isChecked = false
                 cell.lblTitle.textColor = .black
                 }
                 } else {
                 // add in dictionary
                 if let cell = tableView.cellForRow(at: indexPath) as? LeftCell {
                 cell.checkBox.isChecked = true
                 cell.lblTitle.textColor = .main
                 selectedSections[indexPath.row] = sections[indexPath.row]
                 }
                 }*/
            }
        }
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
}


// MARK: - Right table view cell

fileprivate final class RightCell: BaseTableCell
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var title: string? {
        didSet {
            updateUi()
        }
    }
    
    let normalColor = Color(white: 0.93, alpha: 1.0)
    
    private lazy var mainPanel: UIView = { [weak self] in
        let panel = UIView()
        panel.translatesAutoresizingMaskIntoConstraints = false
        panel.backgroundColor = self?.normalColor
        return panel
        }()
    
    private var divider: UIView = {
        let panel = UIView()
        panel.translatesAutoresizingMaskIntoConstraints = false
        panel.backgroundColor = .main
        return panel
    }()
    
    private var lblTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .right
        label.font = Font.systemFont(ofSize: 15.0)
        return label
    }()
    
    // MARK: - Outlets
    
    
    // MARK: - Overridden functions
    
    internal override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
    
    internal override func updateUi() {
        lblTitle.text = title ?? .empty
    }
    
    internal override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if (selected) {
            mainPanel.backgroundColor = .white
            divider.backgroundColor = .main
        } else {
            mainPanel.backgroundColor = normalColor
            divider.backgroundColor = normalColor
        }
    }
    
    
    // MARK: - Actions
    
    
    // MARK: - Functions
    
    private func setupViews()-> void {
        addSubview(mainPanel)
        mainPanel.addSubview(divider)
        mainPanel.addSubview(lblTitle)
        // main panel constraints -> t,r,b,l
        if (Localizer.instance.current == .arabic) {
            lblTitle.textAlignment = .right
            divider.rightAnchor.constraint(equalTo: mainPanel.rightAnchor).isActive = true
            lblTitle.rightAnchor.constraint(equalTo: divider.leftAnchor, constant: -16.0).isActive = true
            lblTitle.leftAnchor.constraint(equalTo: mainPanel.leftAnchor).isActive = true
        } else {
            lblTitle.textAlignment = .left
            divider.leftAnchor.constraint(equalTo: mainPanel.leftAnchor).isActive = true
            lblTitle.leftAnchor.constraint(equalTo: divider.rightAnchor, constant: 16.0).isActive = true
            lblTitle.rightAnchor.constraint(equalTo: mainPanel.rightAnchor).isActive = true
        }
        mainPanel.topAnchor.constraint(equalTo: topAnchor).isActive = true
        mainPanel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        mainPanel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        mainPanel.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        // divider constraints-> w,r,b,t
        divider.widthAnchor.constraint(equalToConstant: 2.5).isActive = true
        divider.topAnchor.constraint(equalTo: mainPanel.topAnchor).isActive = true
        divider.bottomAnchor.constraint(equalTo: mainPanel.bottomAnchor).isActive = true
        // title label constraints -> t,r,b,l
        lblTitle.topAnchor.constraint(equalTo: mainPanel.topAnchor).isActive = true
        lblTitle.bottomAnchor.constraint(equalTo: mainPanel.bottomAnchor).isActive = true
        
    }
}

// MARK: - Left table view cell

fileprivate final class LeftCell: BaseTableCell
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var isRadio = false
    internal var title: string? { didSet { updateUi() } }
    internal var section: Section? { didSet { updateUi() } }
    private var mainPanel: UIView = {
        let panel = UIView()
        panel.translatesAutoresizingMaskIntoConstraints = false
        panel.backgroundColor = .white
        return panel
    }()
    
    internal var lblTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .right
        label.font = Font.systemFont(ofSize: 14.0)
        return label
    }()
    
    fileprivate var radioView: RadioView = {
        let radio = RadioView()
        radio.translatesAutoresizingMaskIntoConstraints = false
        radio.selectedColor = .main
        radio.backgroundColor = .white
        return radio
    }()
    
    fileprivate var checkBox: CheckBox = {
        let chkBox = CheckBox()
        chkBox.translatesAutoresizingMaskIntoConstraints = false
        chkBox.image = #imageLiteral(resourceName: "ic-true")
        chkBox.selectedColor = .main
        return chkBox
    }()
    
    // MARK: - Overridden functions
    
    internal override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if (isRadio) {
            radioView.isSelected = selected
        } else {
            checkBox.isChecked = selected
        }
        lblTitle.textColor = (selected) ? .main : .black
    }
    
    internal override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        setupViews()
    }
    
    internal required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    internal override func updateUi() {
        if (isRadio) {
            checkBox.removeFromSuperview()
            mainPanel.addSubview(radioView)
            if (Localizer.instance.current == .arabic) {
                radioView.leftAnchor.constraint(equalTo: mainPanel.leftAnchor, constant: 8.0).isActive = true
                lblTitle.leftAnchor.constraint(equalTo: radioView.rightAnchor).isActive = true
            } else {
                lblTitle.rightAnchor.constraint(equalTo: radioView.leftAnchor).isActive = true
                radioView.rightAnchor.constraint(equalTo: mainPanel.rightAnchor, constant: -8.0).isActive = true
            }
            radioView.heightAnchor.constraint(equalToConstant: 16.0).isActive = true
            radioView.widthAnchor.constraint(equalToConstant: 16.0).isActive = true
            radioView.centerYAnchor.constraint(equalTo: mainPanel.centerYAnchor).isActive = true
            
            lblTitle.text = (title ?? .empty).localized()
        } else {
            radioView.removeFromSuperview()
            mainPanel.addSubview(checkBox)
            if (Localizer.instance.current == .arabic) {
                lblTitle.leftAnchor.constraint(equalTo: checkBox.rightAnchor).isActive = true
                checkBox.leftAnchor.constraint(equalTo: mainPanel.leftAnchor, constant: 8.0).isActive = true
            } else {
                lblTitle.rightAnchor.constraint(equalTo: checkBox.leftAnchor).isActive = true
                checkBox.rightAnchor.constraint(equalTo: mainPanel.rightAnchor, constant: -8.0).isActive = true
            }
            checkBox.heightAnchor.constraint(equalToConstant: 16.0).isActive = true
            checkBox.widthAnchor.constraint(equalToConstant: 16.0).isActive = true
            checkBox.centerYAnchor.constraint(equalTo: mainPanel.centerYAnchor).isActive = true
            lblTitle.text = section?.title ?? .empty
        }
    }
    
    
    // MARK: - Functions
    
    private func setupViews()-> void {
        addSubview(mainPanel)
        mainPanel.addSubview(lblTitle)
        // main panel constraints-> t,r,b,l
        if (Localizer.instance.current == .arabic) {
            lblTitle.textAlignment = .right
            lblTitle.rightAnchor.constraint(equalTo: mainPanel.rightAnchor, constant: -16.0).isActive = true
        } else {
            lblTitle.textAlignment = .left
            lblTitle.leftAnchor.constraint(equalTo: mainPanel.leftAnchor, constant: 16.0).isActive = true
        }
        mainPanel.topAnchor.constraint(equalTo: topAnchor).isActive = true
        mainPanel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        mainPanel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        mainPanel.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        // title label constraints -> t,r,b,l
        lblTitle.topAnchor.constraint(equalTo: mainPanel.topAnchor).isActive = true
        lblTitle.bottomAnchor.constraint(equalTo: mainPanel.bottomAnchor).isActive = true
    }
    
    
}




















