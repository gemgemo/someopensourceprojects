

import UIKit
import SwiftSpinner


class DepartmentAdsScreenViewController: UIViewController,
    UICollectionViewDelegate,
    UICollectionViewDataSource,
    UITabBarDelegate
{
    let defaults = UserDefaults.standard
    
    @IBOutlet var departName: UILabel!
    var departmentiD: String!
    let imagesUrl = "http://142.4.4.149/~kwtads/all_images/"
    var from = ""
    var departmenSName = [String]()
    var departmentSImg = [String]()
    var departmentSView = [String]()
    var departmentSFromDate = [String]()
    var departmentSExpDate = [String]()
    var departmentSLikes = [String]()
    var departmentSID = [String]()
    var specialNames = [String]()
    var specialFdate = [String]()
    var specialEdate = [String]()
    var specialImgs =  [String]()
    var specialViews = [String]()
    var specialLikes = [String]()
    var collNames = [String]()
    var collFdate = [String]()
    var collEdate = [String]()
    var collImgs = [String]()
    var collViews = [String]()
    var collLikes = [String]()
    var likes = [String]()
    var dislikes = [String]()
    
    
    @IBOutlet var buttonSP: UIButton!
    @IBOutlet var buttonColl: UIButton!
    @IBOutlet var departmentAdsScreenView: UICollectionView!
    
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        if Reachability.isConnectedToNetwork() == true {
            self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "ac_back-arrow")
            self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "ac_back-arrow")
            self.navigationController?.navigationBar.tintColor = UIColor(red: 0.96, green: 0.69, blue:  0.25, alpha: 1 )
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
            let departmentName = UserDefaults.standard.string(forKey: "departmentName")!
            departName.text = departmentName
            departmentAdsScreenView.delegate = self
            departmentAdsScreenView.dataSource = self
            buttonSP.layer.cornerRadius = 15
            buttonColl.layer.cornerRadius = 15
            SwiftSpinner.show("Loading...")
            loadDepartmentAds()
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage
        } else {
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage
            let err = "Internet connection FAILED"
            self.displayAlertMessage(err)
        }
    }
    
    func loadDepartmentAds()-> Void {
        let departmentIDs = UserDefaults.standard.string(forKey: "departmentID")!
        let countryId = UserDefaults.standard.string(forKey: "CountryIDS")!
        let urlString = "\(baseUrl)v_cat_adv_is_normal.php?t=0&c_id=\(departmentIDs)&area_id=\(countryId)&is_normal=1&lang=0"
        //print("ads url \(urlString)")
        let session = URLSession.shared
        let url = URL(string: urlString)!
        session.dataTask(with: url) { (data :Data?, response :URLResponse?, error:Error?) in
                if let responseData = data {
                    print("ads response:  \(String(data: responseData, encoding: .utf8) ?? "")")
                    do {
                        let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments)as! [String:AnyObject]
                        if let data = json["data"] as? NSArray {
                            if (data.count == 0) {
                                //                            let err = "There Is No Ads"
                                //                            self.displayAlertMessage(err)
                                //                            print(err)
                            } else {
                                for i in 0..<data.count {
                                    if let dict = data[i] as? NSDictionary {
                                        self.departmenSName.append(dict["adv_name"] as! String)
                                        self.departmentSImg.append(dict["img_adv"] as! String)
                                        self.departmentSView.append(dict["num_view"] as! String)
                                        self.departmentSFromDate.append(dict["from_date"] as! String)
                                        self.departmentSExpDate.append(dict["expire_date"] as! String)
                                        self.categoriesIds.append(dict["cat_id"] as? String ?? " ")
                                        self.departmentSLikes.append(dict["likes"] as! String)
                                        self.departmentSID.append(dict["adv_id"] as! String)
                                        self.likes.append(dict["likes_num"] as! String)
                                        self.dislikes.append(dict["dislike_num"] as! String)
                                    }
                                }
                            }
                            DispatchQueue.main.async {
                                self.departmentAdsScreenView.reloadData()
                                SwiftSpinner.hide()
                            }
                        }
                    } catch {
                        print ("Could not Serialize")
                    }
                }
        }.resume()
       
    }
    
    func displayAlertMessage(_ userMessage:String) {
        let myAlert = UIAlertController(title: "Alert", message:userMessage, preferredStyle: UIAlertControllerStyle.alert);
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
        myAlert.addAction(okAction);
        present(myAlert, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let screenSize = collectionView.bounds
        let screenWidth = screenSize.width
        return CGSize(width: screenWidth, height: screenWidth)
    }
            
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "adsScreenCell", for: indexPath) as! DepartmentAdsScreenCollectionViewCell
        let date1 = NSDate()
        var isExpired = true
        if (!departmentSExpDate.isEmpty) {
            let date2 = departmentSExpDate[indexPath.row]
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            if let date3 = formatter.date(from: date2) {
                isExpired = date1.compare(date3) == .orderedDescending
            }
        }
      cell.id = departmentSID[indexPath.item]
        if (isExpired) {
            cell.expireDateImg.isHidden = false
            cell.isExpired = true
        } else {
            cell.expireDateImg.isHidden = true
            cell.isExpired = false
        }
        if from == "collection" {
            if(collNames.isEmpty) {
                //let err = "There Is No Ads"
                //self.displayAlertMessage(err)
            } else {
                cell.adsName.text = collNames[indexPath.row%collNames.count]
                cell.adsScreenView.text = collViews[indexPath.row%collViews.count]
                cell.adsScreenFdate.text = collFdate[indexPath.row%collFdate.count]
                cell.adsScreenEdate.text = collEdate[indexPath.row%collEdate.count]
                cell.adsScreenLike.text = collLikes[indexPath.row%collLikes.count]
                cell.lblLike.text = likes[indexPath.item]
                cell.adsScreenImg.image = nil
                load_image(imagesUrl+collImgs[indexPath.row%collImgs.count], imageview: cell.adsScreenImg)
            }
        } else {
            if from == "special" {
                if(!specialNames.isEmpty) {
                    if (categoriesIds[indexPath.row] == "2") {
                        cell.adsScreenLike.isHidden = false
                        cell.adsscreenImgLike.isHidden = false
                    } else {
                        cell.adsScreenLike.isHidden = true
                        cell.adsscreenImgLike.isHidden = true
                    }
                    
                    cell.adsName.text = specialNames[indexPath.row%specialNames.count]
                    cell.adsScreenView.text = specialViews[indexPath.row%specialViews.count]
                    cell.adsScreenFdate.text = specialFdate[indexPath.row%specialFdate.count]
                    cell.adsScreenEdate.text = specialEdate[indexPath.row%specialEdate.count]
                    cell.adsScreenLike.text = specialLikes[indexPath.row%specialLikes.count]
                    cell.lblLike.text = likes[indexPath.item]
                    load_image(imagesUrl+specialImgs[indexPath.row], imageview: cell.adsScreenImg)
                }
            } else {
                //print("------------------>>>>>> cat id : \(categoriesIds[indexPath.row])")// crash if empty
                //let departmentIDs = UserDefaults.standard.string(forKey: "departmentID")!
                if (!categoriesIds.isEmpty) {
                    if (categoriesIds[indexPath.row] == "2") {//departmentIDs == "2"
                        cell.adsName.text = departmenSName[indexPath.row]
                        cell.adsScreenView.text = departmentSView[indexPath.row]
                        cell.adsScreenFdate.text = departmentSFromDate[indexPath.row]
                        cell.adsScreenEdate.text = departmentSExpDate[indexPath.row]
                        cell.adsScreenLike.text = departmentSLikes[indexPath.row]
                        cell.lblLike.text = likes[indexPath.item]
                        cell.adsScreenLike.isHidden = false
                        cell.adsscreenImgLike.isHidden = false
                        cell.adsBottomLabel.textAlignment = .left
                    } else {
                        if (!departmenSName.isEmpty) {
                            cell.adsName.text = departmenSName[indexPath.row]
                            cell.adsScreenView.text = departmentSView[indexPath.row]
                            cell.adsScreenFdate.text = departmentSFromDate[indexPath.row]
                            cell.adsScreenEdate.text = departmentSExpDate[indexPath.row]
                            cell.lblLike.text = likes[indexPath.item]
                            cell.adsScreenLike.isHidden = true
                            cell.adsscreenImgLike.isHidden = true
                            cell.adsBottomLabel.textAlignment = .right
                        }
                    }
                    
                }
                cell.lblLike.text = likes[indexPath.item]
                cell.lblDislike.text = dislikes[indexPath.item]
                load_image(imagesUrl+departmentSImg[indexPath.row], imageview: cell.adsScreenImg)
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch (from) {
        case "special":
            return specialNames.count
        case "collection":
            return collNames.count
        default:
            return departmenSName.count
        }
    }
    
    func load_image(_ link:String, imageview:UIImageView) {
        imageview.image = nil
        let url:URL = URL(string: link)!
        let request = NSMutableURLRequest(url: url)
        request.timeoutInterval = 10
        URLSession.shared.dataTask(with: url) { (data :Data?, response :URLResponse?, error:Error?) in
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                return
            }
            var image = UIImage(data: data!)
            if (image != nil) {
                func set_image() {
                    imageview.image = image
                }
                DispatchQueue.main.async(execute: set_image)
            }
        }.resume()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetialScreen" {
            let tabViewController = segue.destination as! deatialscreensViewController
            if let selectedCell = sender as? DepartmentAdsScreenCollectionViewCell {
                let indexPath = departmentAdsScreenView.indexPath(for: selectedCell)!
                print(departmentSID)
                print(indexPath.row)
                let selectedDepartmentId = departmentSID[indexPath.row]
                tabViewController.isExpired =  selectedCell.isExpired
                tabViewController.departmentSids = selectedDepartmentId
            }
        }
    }
    
    internal override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if (departmentSID.isEmpty) {
            return false
        }
        return true
    }
    
    @IBAction func buttonSPTapped(_ sender: AnyObject) {
        //reload()
        specialNames.removeAll()
        specialFdate.removeAll()
        specialImgs.removeAll()
        specialViews.removeAll()
        specialLikes.removeAll()
        specialEdate.removeAll()
        departmentSID.removeAll()
        departmenSName.removeAll()
        departmentSView.removeAll()
        departmentSFromDate.removeAll()
        departmentSExpDate.removeAll()
        categoriesIds.removeAll()
        buttonSP.backgroundColor = UIColor(red: 1, green: 0.4, blue: 0.4, alpha: 1)
        buttonColl.backgroundColor = UIColor.white
        buttonSP.setTitleColor(UIColor.white, for: UIControlState())
        buttonColl.setTitleColor(UIColor.darkGray, for: UIControlState())
        let countryId = UserDefaults.standard.string(forKey: "CountryIDS")!
        let urlString = "\(baseUrl)v_cat_adv.php?t=0&c_id=&area_id=\(countryId)&favo=1&lang=0"
        let url = URL(string: urlString)!
        URLSession.shared
            .dataTask(with: url) { (data, response, error) in
                if let responseData = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments)as! [String:AnyObject]
                        let data = json["data"] as! NSArray
                        print("sp response: \(String(describing: data))")
                        for i in 0..<data.count {
                            if let dict = data[i] as? NSDictionary {
                                self.specialNames.append(dict["adv_name"] as! String)
                                self.specialFdate.append(dict["from_date"] as! String)
                                self.specialEdate.append(dict["expire_date"] as! String)
                                self.specialImgs.append(dict["img_adv"] as! String)
                                self.specialViews.append(dict["num_view"] as! String)
                                self.specialLikes.append(dict["likes"] as! String)
                                self.departmentSID.append(dict["adv_id"] as! String)
                                self.categoriesIds.append(dict["cat_id"] as? String ?? " ")
                                self.departmentSFromDate.append(dict["from_date"] as! String)
                                self.departmentSExpDate.append(dict["expire_date"] as! String)
                            }
                        }
                        DispatchQueue.main.async {
                            self.from = "special"
                            self.departmentAdsScreenView.reloadData()
                        }
                    } catch {
                        print ("Could not Serialize")
                    }
                }
            }
            .resume()
    }
    
   var categoriesIds = Array<String>()
    
    @IBAction func departTapped(_ sender: AnyObject) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "departmentPage") as! DepartmentsViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    private func reload()-> Void {
        
        specialNames.removeAll()
        specialFdate.removeAll()
        specialImgs.removeAll()
        specialViews.removeAll()
        specialLikes.removeAll()
        specialEdate.removeAll()
        
        collNames.removeAll()
        collViews.removeAll()
        collFdate.removeAll()
        collEdate.removeAll()
        collLikes.removeAll()
        collImgs.removeAll()
        
        departmentSID.removeAll()
        departmenSName.removeAll()
        departmentSView.removeAll()
        departmentSFromDate.removeAll()
        departmentSExpDate.removeAll()
        
        departmentAdsScreenView.reloadData()
    }
    
    
    @IBAction func buttonCollTapped(_ sender: AnyObject) {
        //reload()
        collNames.removeAll()
        collViews.removeAll()
        collFdate.removeAll()
        collEdate.removeAll()
        collLikes.removeAll()
        collImgs.removeAll()
        departmentSID.removeAll()
        departmenSName.removeAll()
        departmentSView.removeAll()
        departmentSFromDate.removeAll()
        departmentSExpDate.removeAll()
        categoriesIds.removeAll()
        buttonColl.backgroundColor = UIColor(red: 1, green: 0.4, blue: 0.4, alpha: 1)
        buttonSP.backgroundColor = UIColor.white
        buttonColl.setTitleColor(UIColor.white, for: UIControlState())
        buttonSP.setTitleColor(UIColor.darkGray, for: UIControlState())
        
        let countryId = UserDefaults.standard.string(forKey: "CountryIDS")!
        
        let urlString = "\(baseUrl)v_cat_adv_is_diverse.php?t=0&area_id=\(countryId)&is_diverse=1&lang=0"
        let session = URLSession.shared
        let url = URL(string: urlString)!
        session.dataTask(with: url, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
                
                if let responseData = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments)as! [String:AnyObject]
                        print("wanted response is \(String(describing: json))")
                        let data = json["data"] as! NSArray
                        for i in 0..<data.count
                        {
                            if let dict = data[i] as? NSDictionary {
                                self.collNames.append(dict["adv_name"] as! String)
                                self.collFdate.append(dict["from_date"] as! String)
                                self.collEdate.append(dict["expire_date"] as! String)
                                self.collImgs.append(dict["img_adv"] as! String)
                                self.collViews.append(dict["num_view"] as! String)
                                self.collLikes.append(dict["likes"] as! String)
                                self.departmentSID.append(dict["adv_id"] as! String)
                                self.categoriesIds.append(dict["cat_id"] as? String ?? " ")
                                self.departmentSFromDate.append(dict["from_date"] as! String)
                                self.departmentSExpDate.append(dict["expire_date"] as! String)
                            }
                        }
                        DispatchQueue.main.async {
                            self.from = "collection"
                            self.departmentAdsScreenView.reloadData()
                        }
                    } catch {
                        print ("Could not Serialize")
                    }
                }
        }).resume()
    }
    
    @IBAction func MenuTapped(_ sender: AnyObject) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuPage") as! MenuPageViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dailyNew(_ sender: AnyObject) {
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "DailyNews") as! DailyNewsViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func favPage(_ sender: AnyObject) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "FavPage") as! FavPageViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func nextButton(_ sender: AnyObject) {
        //get cell size
        let cellSize = CGSize(width: self.view.frame.width, height: self.view.frame.height);
        //get current content Offset of the Collection view
        let contentOffset = departmentAdsScreenView.contentOffset;
        //scroll to next cell
        departmentAdsScreenView.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width-10, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true);
    }
    
    @IBAction func backButton(_ sender: AnyObject) {
        //get cell size
        let cellSize = CGSize(width: self.view.frame.width, height: self.view.frame.height);
        //get current content Offset of the Collection view
        let contentOffset = departmentAdsScreenView.contentOffset;
        //scroll to next cell
        departmentAdsScreenView.scrollRectToVisible(CGRect(x: contentOffset.x - cellSize.width+10, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true);
    }
}
