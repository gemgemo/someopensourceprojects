
import Foundation
import UIKit





@IBDesignable
public class TextView: UITextView, UITextViewDelegate
{
    @IBInspectable
    public var placeholder: String = "placholder" {
        didSet {
            setup()
        }
    }
    
    @IBInspectable
    public var placeholderColor: UIColor = UIColor.lightGray {
        didSet {
            setup()
        }
    }
    
    @IBInspectable
    public var color: UIColor = UIColor.black {
        didSet {
            setup()
        }
    }
    
    
    public override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
    
    private func setup()-> Void {
        delegate = self
        text = placeholder
        textColor = placeholderColor
    }
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text == placeholder && textView.textColor == placeholderColor) {
            textView.text = ""
            textView.textColor = color
        }
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text.isEmpty) {
            textView.text = placeholder
            textView.textColor = placeholderColor
        }
    }
    
    
    
    
}










