
import UIKit
import MapKit
import CoreLocation
import Localize_Swift

final class UserLocationUpdate: BaseController
{
    
    
    // MARK: - Constants
    
    // MARK: - Variables
    
    fileprivate var locationManager = CLLocationManager()
    private var menuButton: UIBarButtonItem!
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var mapView: MKMapView! {
        didSet {
            mapView.delegate = self
            mapView.setupCamera()
        }
    }
    @IBOutlet fileprivate weak var details: UILabel!
    @IBOutlet fileprivate weak var footterView: UIView!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        configureUserLocation()
    }
    
    internal override func updateNavgationBar() {
        navigationItem.leftBarButtonItem  = UIBarButtonItem(image: #imageLiteral(resourceName: "ac_back"), style: .plain, target: self, action: #selector(goBack(_:)))
        menuButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ac_manu"), style: .plain, target: self, action: nil)
        menuButton.target = revealViewController()
        menuButton.action = #selector(revealViewController().rightRevealToggle(_:))
        navigationItem.rightBarButtonItem = menuButton
        let imageView = UIImageView(image: #imageLiteral(resourceName: "ac_logo"))
        navigationItem.titleView = imageView
        
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUi), name: Notification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
    }
    
    internal override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: - Actions
   
}



// MARK: - Helper functions

extension UserLocationUpdate
{
    
    fileprivate func configureUserLocation()-> void {
        DispatchQueue.global(qos: .background).async { [weak self ] in
            guard let this = self else { return }
            this.locationManager.delegate = self
            this.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            this.locationManager.requestAlwaysAuthorization()
            if (CLLocationManager.locationServicesEnabled()) {
                this.locationManager.startUpdatingLocation()
            } else {
                DispatchQueue.main.async { [weak self] in
                    guard let this = self else { return }
                    let alert = UIAlertController(title: "warningtitlelan".localized(), message: "locationsubtitlelan".localized() , preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "okLan".localized(), style: .default, handler: { (action) in
                        if let settingsUrl = URL(string: UIApplicationOpenSettingsURLString), UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.openURL(settingsUrl)
                        }
                    }))
                    alert.addAction(UIAlertAction(title: "cancellan".localized(), style: .default, handler: nil))
                    this.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc fileprivate func updateUi()-> void {
        details.text = "userlocationmapdetailslan".localized()
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            details.textAlignment = .right
            footterView.semanticContentAttribute = .forceLeftToRight
        } else {
            details.textAlignment = .left
            footterView.semanticContentAttribute = .forceRightToLeft
        }
    }
    
}


// MARK: - Map view delegate functions

extension UserLocationUpdate: MKMapViewDelegate
{
    internal func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        UIApplication.showIndicator(by: false)
    }
    
    internal func mapViewDidStopLocatingUser(_ mapView: MKMapView) {
        UIApplication.showIndicator(by: false)
    }
    
    internal func mapViewWillStartLoadingMap(_ mapView: MKMapView) {
        UIApplication.showIndicator(by: true)
    }
    
    internal func mapViewWillStartLocatingUser(_ mapView: MKMapView) {
        print("start locating")
    }
    
}


extension UserLocationUpdate: CLLocationManagerDelegate
{
    internal func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("location manager error \(error)")
        UIApplication.showIndicator(by: false)
    }
    
    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            let coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            mapView.annotations.forEach { mapView.removeAnnotation($0) }
            mapView.setRegion(in: coordinate)
            mapView.setAnnotation(in: coordinate, with: .empty, and: .empty)
        }
    }
}





























