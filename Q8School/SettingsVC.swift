//
//  SettingsVC.swift
//  Q8School
//
//  Created by Mac on 12/19/16.
//  Copyright © 2016 Atiaf. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {

    @IBOutlet weak var openMenu: UIBarButtonItem!
    @IBOutlet weak var callLbl: UILabel!
    @IBOutlet weak var addLbl: UILabel!
    @IBOutlet weak var myAdsLbl: UILabel!
    @IBOutlet weak var offerLabel: UILabel!
    @IBOutlet weak var arabicBtn: UIButton!
    @IBOutlet weak var englishBtn: UIButton!
    @IBOutlet weak var settingLbl: UILabel!
    @IBOutlet weak var settingView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Settings"
        englishBtn.layer.cornerRadius = 10
        englishBtn.layer.masksToBounds = true
        arabicBtn.layer.cornerRadius = 10
        arabicBtn.layer.masksToBounds = true
        settingView.layer.cornerRadius = 10
        settingView.layer.masksToBounds = true

        if Settings.lang == "ar"
        {
            settingLbl.text = "تغير اللغة"
            callLbl.text = "اتصل بنا"
            addLbl.text = "اضف اعلانك"
            myAdsLbl.text = "اعلاناتي"
            offerLabel.text = "العروض"
            englishBtn.setTitle("الانجليزية", for: .normal)
            arabicBtn.setTitle("العربية", for: .normal)
            self.title = "الاعدادات"



        }
        if revealViewController() != nil {
            
            revealViewController().rightViewRevealWidth = 235
            openMenu.target = revealViewController()
            openMenu.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }

        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MainPageViewController") as! MainPageViewController
        self.navigationController?.pushViewController(vc, animated: false)

    }

    @IBAction func englishBtn(_ sender: UIButton) {
        Settings.lang = "en"
        self.performSegue(withIdentifier: "setting", sender: self)
    }

    @IBAction func arabicBtn(_ sender: UIButton) {
        Settings.lang = "ar"
        self.performSegue(withIdentifier: "setting", sender: self)

    }
}
