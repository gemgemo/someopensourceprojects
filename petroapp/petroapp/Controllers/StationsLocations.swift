
import UIKit
import Gemo
import GoogleMaps
import CoreLocation
import MapKit

class StationsLocations: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var locationManager = CLLocationManager(), selectedLocation = CLLocationCoordinate2D()
    
    // MARK: - Outlets
    
    @IBOutlet weak var mapView: GMSMapView! { didSet { mapView.delegate = self } }
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupLocation()
    }
    
    
    // MARK: - Actions

    
}


// MARK: - Helper functions 

fileprivate extension StationsLocations {
    
    fileprivate func setupLocation()-> void {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        guard CLLocationManager.locationServicesEnabled() else {
            displayAlert(with: Localization.OpenLocation.localized())
            return
        }
        locationManager.startUpdatingLocation()
    }
    
    func loadData()-> void {
        Site.fetchStationsLocations(by: selectedLocation, self) { [weak self] (locations) in
            if (locations.isEmpty) {
                self?.displayAlert(with: Localization.NoData.localized())
            } else {
                Threads.main { [weak self] in
                    locations.forEach { [weak self] (location) in
                        guard let latitude = location.latitude?.toDouble, let longitude = location.longitude?.toDouble else {
                            return
                        }
                        self?.setMarker(in: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), with: #imageLiteral(resourceName: "marker-map1"), location)
                    }
                }
            }
        }
    }
    
    fileprivate func setMarker(in coordinate: CLLocationCoordinate2D, with icon: UIImage, _ markerData: any?)-> void {
        let marker = GMSMarker()
        marker.position = coordinate
        marker.icon = icon
        marker.map = mapView        
        marker.appearAnimation = .pop
        marker.userData = markerData        
    }

    
}


// MARK: - Location manager delegate functions

extension StationsLocations: CLLocationManagerDelegate {
    
    internal func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        displayAlert(with: error.localizedDescription)
    }
    
    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let coordinate = locations.last?.coordinate {            
            setMarker(in: coordinate, with: #imageLiteral(resourceName: "marker-map2"), nil)
            selectedLocation = coordinate
            mapView.camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 8.0)
        }
        manager.stopUpdatingLocation()
    }
}


// MARK: - Map view delegate functions 

extension StationsLocations: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print("goto navigation")
        guard let site = marker.userData as? Site,
            let latitude = site.latitude?.toDouble,
            let longitude = site.longitude?.toDouble else {
                return
        }
        if (UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!)) {
            UIApplication.shared.open(URL(string: "comgooglemaps://?saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving")!, options: [:])
        } else {
            // open apple map view
            let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude)))
            mapItem.openInMaps(launchOptions: nil)
            /*
             [
             MKLaunchOptionsMapCenterKey: NSValue(MKCoordinate: regionSpan.center),
             MKLaunchOptionsMapSpanKey: NSValue(MKCoordinateSpan: regionSpan.span)
             ]
             */
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if (marker.icon == #imageLiteral(resourceName: "marker-map2")) {
            return true
        }
        return false
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let site = marker.userData as? Site
        let markerWindow = UIView(frame: CGRect(x: 0, y: 0, width: 220, height: 90))
        markerWindow.setCorner(radius: 4)
        markerWindow.setShadow(with: .zero, radius: 1.0, opacity: 0.4, color: .black)
        markerWindow.backgroundColor = .white
        markerWindow.clipsToBounds = true
        let arrowImage = UIImageView()
        arrowImage.backgroundColor = .main
        arrowImage.contentMode = .scaleAspectFit
        arrowImage.image = #imageLiteral(resourceName: "ic_next").withRenderingMode(.alwaysOriginal)
        arrowImage.translatesAutoresizingMaskIntoConstraints = false
        markerWindow.addSubview(arrowImage)
        arrowImage.rightAnchor.constraint(equalTo: markerWindow.rightAnchor).isActive = true
        arrowImage.bottomAnchor.constraint(equalTo: markerWindow.bottomAnchor).isActive = true
        arrowImage.widthAnchor.constraint(equalToConstant: 40).isActive = true
        arrowImage.topAnchor.constraint(equalTo: markerWindow.topAnchor).isActive = true
        let lblAddress = UILabel()
        lblAddress.text = site?.address ?? .empty
        lblAddress.translatesAutoresizingMaskIntoConstraints = false
        lblAddress.font = Font.notoKufi(of: 15)
        lblAddress.numberOfLines = 2
        lblAddress.textColor = .black
        lblAddress.textAlignment = .center
        lblAddress.adjustsFontSizeToFitWidth = true
        markerWindow.addSubview(lblAddress)
        lblAddress.rightAnchor.constraint(equalTo: arrowImage.leftAnchor, constant: -8).isActive = true
        lblAddress.bottomAnchor.constraint(equalTo: markerWindow.bottomAnchor, constant: 8).isActive = true
        lblAddress.leftAnchor.constraint(equalTo: markerWindow.leftAnchor, constant: 8).isActive = true
        let icon = UIImageView()
        icon.contentMode = .scaleAspectFit
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.image = #imageLiteral(resourceName: "ic_gas_station")
        markerWindow.addSubview(icon)
        icon.topAnchor.constraint(equalTo: arrowImage.topAnchor, constant: 8).isActive = true
        icon.widthAnchor.constraint(equalToConstant: 40).isActive = true
        icon.heightAnchor.constraint(equalToConstant: 40).isActive = true
        icon.rightAnchor.constraint(equalTo: arrowImage.leftAnchor, constant: -8).isActive = true
        let lblName = UILabel()
        lblName.text = site?.name ?? .empty
        lblName.translatesAutoresizingMaskIntoConstraints = false
        lblName.font = Font.notoKufi(of: 15)
        lblName.textColor = .black
        lblName.adjustsFontSizeToFitWidth = true
        lblName.textAlignment = .center
        markerWindow.addSubview(lblName)
        lblName.topAnchor.constraint(equalTo: icon.topAnchor, constant: 0).isActive = true
        lblName.rightAnchor.constraint(equalTo: icon.leftAnchor, constant: -8).isActive = true
        lblName.leftAnchor.constraint(equalTo: markerWindow.leftAnchor, constant: 8).isActive = true
        return markerWindow
    }
}
























