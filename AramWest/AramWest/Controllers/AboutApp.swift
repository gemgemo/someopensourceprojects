
import UIKit
import Alamofire
import Localize_Swift

final class AboutApp: BaseController
{
    // MARK: - Constants
    
    // MARK: - Variables
    
    internal var isAbout = false
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var txtData: UITextView! {
        didSet {
            txtData.alwaysBounceVertical = true
        }
    }
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (isAbout) ?  getData(from: Constants.BackendURLs.AboutData): getTerms(from: Constants.BackendURLs.TermsData)
    }
    
    internal override func updateNavgationBar() {
        navigationItem.title = (isAbout) ? "aboutapptitlelan".localized() : "termstitlelan".localized()
        let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ac_back").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(closeOnCLick(_:)))
        leftButton.tintColor = .black
        navigationItem.leftBarButtonItem = leftButton
    }
    
    // MARK: - Actions
    
    @objc private func closeOnCLick(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

}


// MARK: - Helper functions

extension AboutApp
{
    
    
    fileprivate func getData(from link: string)-> void {
        startSpin()
        Alamofire.request(link)
            .responseJSON { [weak self] (response) in
                guard let this = self else { return }
                let result = response.result
                if (result.error != nil) {
                    print("fetch about data error \(result.error)")
                    this.showAlert(with: .empty, and: "erroralertlan".localized())
                    this.stopSpin()
                    return
                }
                
                if let value = result.value as? Dictionary<string, any>, let success = value["Success"] as? string, let message = value["Massage"] as? string, let aboutA = value["About"] as? string, let aboutE = value["AboutA"] as? string {
                    if (success == "True") {
                        if (Localize.currentLanguage() == Constants.Language.Arabic) {
                            this.txtData.text = aboutA
                            this.txtData.textAlignment = .right
                        } else {
                            this.txtData.text = aboutE
                            this.txtData.textAlignment = .left
                        }
                        this.stopSpin()
                    } else {
                        this.showAlert(with: .empty, and: message)
                        this.stopSpin()
                    }
                } else {
                    print("fetch nil data when get about data")
                    this.showAlert(with: .empty, and: "erroralertlan".localized())
                }
        }
    }
    
    
    fileprivate func getTerms(from link: string)-> void {
        startSpin()
        Alamofire.request(link)
            .responseJSON { [weak self] (response) in
                guard let this = self else { return }
                let result = response.result
                if (result.error != nil) {
                    print("fetch terms data error \(result.error)")
                    this.showAlert(with: .empty, and: "erroralertlan".localized())
                    this.stopSpin()
                    return
                }
                
                if let value = result.value as? Dictionary<string, any>, let success = value["Success"] as? string, let message = value["Massage"] as? string, let aboutA = value["Police"] as? string, let aboutE = value["PoliceA"] as? string {
                    if (success == "True") {
                        if (Localize.currentLanguage() == Constants.Language.Arabic) {
                            this.txtData.text = aboutA
                            this.txtData.textAlignment = .right
                        } else {
                            this.txtData.text = aboutE
                            this.txtData.textAlignment = .left
                        }
                        this.stopSpin()
                    } else {
                        this.showAlert(with: .empty, and: message)
                        this.stopSpin()
                    }
                } else {
                    print("fetch nil data when get terms data")
                    this.showAlert(with: .empty, and: "erroralertlan".localized())
                    this.stopSpin()
                }
        }
    }

    
}


























