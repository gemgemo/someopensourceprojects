
import Foundation
import UIKit
import SystemConfiguration

public class Settings
{
    // languages
    static var lang = "ar"
    // webservice
    static var AboutUs = "http://192.232.214.91/~almglsalbldyinmk//about_us_2.php?key=albldy"
    static var Members = "http://192.232.214.91/~almglsalbldyinmk//cou_mem.php?key=albldy"
    static var Images = "http://192.232.214.91/~almglsalbldyinmk//album.php?key=albldy"
    static var CallUs = "http://192.232.214.91/~almglsalbldyinmk//contact_us_2.php?key=albldy"
    static var News = "http://www.hmmc.gov.sa/Ws/GetAllNews"
    static var DetialNews = "http://www.hmmc.gov.sa/Ws/GetNewsByID"
    static var Visitors = "http://192.232.214.91/~almglsalbldyinmk//visitor.php?key=albldy"
    static var CurrentVisitor = "http://192.232.214.91/~almglsalbldyinmk//visitor_2.php?key=albldy"
    static var ComplainUrl = "http://192.232.214.91/~almglsalbldyinmk//comp.php?key=albldy"
    
    static var x = "http://192.232.214.91/~almglsalbldyinmk//number_2.php?user_num="
    
    
    
    
    // complainVC
    static var complainV = ""
    // Check
    static var check = "0"
    static var flag = ""
    
    internal static var isConnected: Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)   
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }

}


extension UIApplication {
    internal class func open(this link: String)-> Void {
        if let url = URL(string: link), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
}

extension UIViewController
{
    
    internal func dialogUser(with message: String)-> Void {
        let myAlert = UIAlertController(title: "تنبية", message: message, preferredStyle: UIAlertControllerStyle.alert);
        let okAction = UIAlertAction(title: "موافق", style: .default, handler: { (action: UIAlertAction!) in
        })
        myAlert.addAction(okAction);
        self.present(myAlert, animated: true, completion: nil)
    }
    
}






