
import UIKit

final class Settings: BaseController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    
    // MARK: - Outlets
    
    
    @IBOutlet fileprivate weak var lblTitleHeader: UILabel!
    @IBOutlet fileprivate weak var titlePanal: UIView! {
        didSet {
            shadow(for: titlePanal)
        }
    }
    @IBOutlet fileprivate weak var containerPanal: UIView! {
        didSet {
            containerPanal.setShadow(with: .zero, radius: 1, opacity: 0.4, color: .black)
        }
    }
    @IBOutlet fileprivate weak var lblUpdateProfile: UILabel!
    @IBOutlet fileprivate weak var lblAppLanguage: UILabel!
    @IBOutlet fileprivate weak var updatePanal: UIView!
    @IBOutlet fileprivate weak var languagePanal: UIView!
    @IBOutlet fileprivate weak var mainPanal: UIView!
    @IBOutlet fileprivate weak var upadateRightIcon: UIImageView!
    @IBOutlet fileprivate weak var updateLeftIcon: UIImageView!
    @IBOutlet fileprivate weak var languageRightIcon: UIImageView!
    @IBOutlet fileprivate weak var languageLeftIcon: UIImageView!
    @IBOutlet fileprivate weak var btnBack: UIButton!
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBar.isHidden = true
        updateUi()
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUi), name: .LanguageChanged, object: nil)
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: - Actions
    
    @IBAction private func navigateToUpdateProfileOnCLick(_ sender: UIButton) {
        if let updateScreen = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: Constants.Storyboards.SignUp) as? SignUp {
            updateScreen.isUpdate = true
            navigate(to: updateScreen)
        }
    }
    
    @IBAction private func changeAppLanguageOnCLick(_ sender: UIButton) {
        let alert = UIAlertController(title: "اختار لغة التطبيق", message: "Choose App Language", preferredStyle: .alert)
        alert.view.tintColor = .main
        // arabic
        alert.addAction(UIAlertAction(title: "اللغه العربية", style: .default, handler: { (action) in
            Localizer.instance.set(language: .arabic)
            AppManager.manage.sideMenu(for: UIApplication.shared.keyWindow)
        }))
        // english
        alert.addAction(UIAlertAction(title: "English", style: .default, handler: { (action) in
            Localizer.instance.set(language: .english)
            AppManager.manage.sideMenu(for: UIApplication.shared.keyWindow)
        }))
        // cancel
        alert.addAction(UIAlertAction(title: "اغلاق/Close", style: .cancel))
        present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction private func backOnCLick(_ sender: UIButton) {
        back()
    }
    

}



// MARK: - Helper functions


extension Settings
{
    
    @objc fileprivate func updateUi()-> void {
        lblUpdateProfile.text = Localization.UpdateProfile.localized()
        lblAppLanguage.text = Localization.AppLanguage.localized()
        lblTitleHeader.text = Localization.Settings.localized()
        btnBack.transform = CGAffineTransform.identity
        if (Localizer.instance.current == .arabic) {
            upadateRightIcon.isHidden = false
            languageRightIcon.isHidden = false
            updateLeftIcon.isHidden = true
            languageLeftIcon.isHidden = true
            lblUpdateProfile.textAlignment = .right
            lblAppLanguage.textAlignment = .right
            btnBack.leftAnchor.constraint(equalTo: titlePanal.leftAnchor, constant: 8).isActive = true
        } else {
            upadateRightIcon.isHidden = true
            languageRightIcon.isHidden = true
            updateLeftIcon.isHidden = false
            languageLeftIcon.isHidden = false
            lblUpdateProfile.textAlignment = .left
            lblAppLanguage.textAlignment = .left
            btnBack.rightAnchor.constraint(equalTo: titlePanal.rightAnchor, constant: -8).isActive = true
            btnBack.transform = btnBack.transform.rotated(by: cgFloat.pi)
        }       
    }
    
}






















