
import UIKit
import SwiftSpinner

class DailyNewsViewController: UIViewController,
    UICollectionViewDelegate,
    UICollectionViewDataSource
{
    
    let imagesUrl = "http://142.4.4.149/~kwtads/all_images/"
    var images_cache = [String:UIImage]()
    var adsNames = [String]()
    var adsFdate = [String]()
    var adsEdate = [String]()
    var adsImgs = [String]()
    var adsViews = [String]()
    var adsLikes = [String]()
    var catid = [String]()
    var departmentSID = [String]()
    let defaults = UserDefaults.standard
    var likes = [String]()
    var disLikes = [String]()
    
    @IBOutlet var adsCollectionView: UICollectionView!
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        if Reachability.isConnectedToNetwork() == true {
            adsCollectionView.dataSource = self
            adsCollectionView.delegate = self
            SwiftSpinner.show("Loading...")
            
            self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "ac_back-arrow")
            self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "ac_back-arrow")
            self.navigationController?.navigationBar.tintColor = UIColor(red: 0.96, green: 0.69, blue:  0.25, alpha: 1 )
            
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage
            loadDailyNews()
            
            let layout1:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout1.sectionInset = UIEdgeInsets(top:0,left:0,bottom:0,right:0)
            
        }
        else
        {
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage
            
            let err = "Internet connection FAILED"
            self.displayAlertMessage(err)
        }
        
    }
    func loadDailyNews()
    {
        
        let urlString = "\(baseUrl)v_cat_adv_is_day.php?t=0&c_id=&area_id=&is_day=1&lang=0"
        
        let session = URLSession.shared
        let url = URL(string: urlString)!
        session.dataTask(with: url, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
                if let responseData = data {
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                        
                        let data = json["data"] as! NSArray
                        if data.count == 0
                        {
                            let err = "There Is No Daily Ads"
                            self.displayAlertMessage(err)
                        }
                        else
                        {
                            
                            for i in 0..<data.count
                            {
                                if let dict = data[i] as? NSDictionary {
                                    
                                    self.adsNames.append(dict["adv_name"] as! String)
                                    self.adsFdate.append(dict["from_date"] as! String)
                                    self.adsEdate.append(dict["expire_date"] as! String)
                                    self.adsImgs.append(dict["img_adv"] as! String)
                                    self.adsViews.append(dict["num_view"] as! String)
                                    self.adsLikes.append(dict["likes"] as! String)
                                    self.departmentSID.append(dict["adv_id"] as! String)
                                    self.catid.append(dict["cat_id"] as! String)
                                    self.likes.append(dict["likes_num"] as! String)
                                    self.disLikes.append(dict["dislike_num"] as! String)
                                }
                            }
                        }
                        DispatchQueue.main.async {
                            self.adsCollectionView.reloadData()
                            SwiftSpinner.hide()
                        }
                    }
                    catch
                    {
                        print ("Could not Serialize")
                    }
                    
                }
                
        }).resume()
        
    }
    func displayAlertMessage(_ userMessage:String)
    {
        let myAlert = UIAlertController(title: "Alert", message:userMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let screenSize: CGRect = self.adsCollectionView.bounds
        
        let screenWidth = screenSize.width
        return CGSize(width: screenWidth, height: screenWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dailyNew", for: indexPath) as! DailyNewsCollectionViewCell
        let date1 = NSDate()
        let date2 = adsEdate[indexPath.row]
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let date3 = formatter.date(from: date2) { // Returns "Jul 27, 2015, 12:29 PM" PST
            cell.id = likes[indexPath.item]
            if  date1.compare(date3) == ComparisonResult.orderedDescending
            {
                NSLog("date1 after date2");
                cell.expireDateImg.isHidden = false
                cell.isExpired = true
            }
            else
            {
                cell.expireDateImg.isHidden = true
                cell.isExpired = false
            }
        }
        
      
        
        defaults.setValue(catid[(indexPath as NSIndexPath).row], forKey:"categoryIDs")
        let catID = UserDefaults.standard.string(forKey: "categoryIDs")!
        
        if (catID == "2") // true
        {
            cell.dailyName.text = adsNames[indexPath.row]
            cell.dailyFDate.text = adsFdate[indexPath.row]
            cell.dailyEDate.text = adsEdate[indexPath.row]
            cell.dailyLikes.text = adsLikes[indexPath.row]
            cell.dailyView.text = adsViews[indexPath.row]
            cell.dailyBottomLabel.textAlignment = .left
            
        }
        else
        {
            
            cell.dailyName.text = adsNames[indexPath.row]
            cell.dailyFDate.text = adsFdate[indexPath.row]
            cell.dailyEDate.text = adsEdate[indexPath.row]
            cell.dailyView.text = adsViews[indexPath.row]
            cell.dailyLikes.isHidden = true
            cell.dailylikeImg.isHidden = true
            cell.dailyBottomLabel.textAlignment = .right
            
        }
        cell.dailyImg.image = nil
        cell.lblLike.text = likes[indexPath.item]
        cell.lblDislike.text = disLikes[indexPath.item]
        load_image(imagesUrl+adsImgs[indexPath.row], imageview: cell.dailyImg)
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return adsNames.count
    }
    
    func load_image(_ link:String, imageview:UIImageView)
    {
        
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.timeoutInterval = 10
        
        let task = session.dataTask(with: url, completionHandler:
        { (data :Data?, response :URLResponse?, error:Error?) in
            
            
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                
                return
            }
            
            
            var image = UIImage(data: data!)
            
            if (image != nil)
            {
                
                
                func set_image()
                {
                    self.images_cache[link] = image
                    imageview.image = image
                }
                
                
                DispatchQueue.main.async(execute: set_image)
                
            }
            
        })
        
        task.resume()
        
    }
    
    
    
//    func refresh()
//    {
//        self.adsCollectionView.reloadData()
//    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "dailyAds" {
            let tabViewController = segue.destination as! deatialscreensViewController
            // Get the cell that generated this segue.
            if let selectedCell = sender as? DailyNewsCollectionViewCell {                
                let indexPath = adsCollectionView.indexPath(for: selectedCell)!
                let selectedDepartmentId = departmentSID[indexPath.row]
                tabViewController.isExpired = selectedCell.isExpired
                defaults.setValue(departmentSID[indexPath.row], forKey: "departmentSid")
                tabViewController.departmentSids = selectedDepartmentId
            }
        }
        
        
    }
   
    
    @IBAction func departTapped(_ sender: AnyObject) {
        //        dismissViewControllerAnimated(true, completion: nil)
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "departmentPage") as! DepartmentsViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func MenuTapped(_ sender: AnyObject) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuPage") as! MenuPageViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func favPage(_ sender: AnyObject) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "FavPage") as! FavPageViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
}
