//
//  UserTypeTableViewCell.swift
//  Q8School
//
//  Created by Mostafa on 12/10/16.
//  Copyright © 2016 Atiaf. All rights reserved.
//

import UIKit

class UserTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var userTypeLabel: UILabel!
    var userTypeId: String!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
