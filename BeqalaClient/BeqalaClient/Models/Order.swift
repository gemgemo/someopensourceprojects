
import Foundation

final class Order: Mapper<Any>
{
    
    internal var id, name, phone, building, floor, address, sendStatus, time, date, storeId, storeName, storeImage, status: string?, orderProducts = [OrderProduct]()
    
    internal override func mapping(mapper: Mapper<Any>) {
        id = mapper["id"]?.text
        name = mapper["name"]?.text
        phone = mapper["phone"]?.text
        building = mapper["building"]?.text
        floor = mapper["floor"]?.text
        address = mapper["addres"]?.text
        sendStatus = mapper["type_send"]?.text
        time = mapper["time_add"]?.text
        date = mapper["date_add"]?.text
        storeId = mapper["id_matjer"]?.text
        storeName = mapper["matjer_name"]?.text
        storeImage = mapper["matjer_photo"]?.text
        status = mapper["type_status"]?.text
        orderProducts = mapper["Data"]?.dictionaries.map { OrderProduct(JSON: $0) } ?? [OrderProduct]()
    }
    
    
    
    // MARK: - Functions 
    
    internal class func fetchData(page: BaseController, _ completion: @escaping([Order])->())-> void {
        Gemo.gem.startSpinning(in: page.view)
        let parameters: [string: any] = [
            Constants.Misc.MainKey: Constants.Misc.MainValue,
            Constants.Misc.LanguageBackendKey: Localizer.instance.current.rawValue,
            "user_id": UserDefaults.standard.string(forKey: Constants.Misc.UserID) ?? .empty
        ]
        HttpClient.request(link: Constants.Urls.CurrentOrders, method: .post, parameters: parameters) { (result, response, error) -> void in
            if (error != nil) {
                print("fetch current orders error \(string(describing: error))")
                Gemo.gem.stopSpinning()
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription, in: page.view)
                return
            }
            print("fetch current orders data \(result ?? string.empty)")
            if let json = result as? Dictionary<string, any>, let data = json["All_data"] as? [Dictionary<string, any>] {
                completion(data.map { Order(JSON: $0) })
            } else {
                Gemo.gem.stopSpinning()
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized(), in: page.view)
            }
        }
    }
    
}



final class OrderProduct: Mapper<any>
{
    
    internal var id, name, price, quantity, photo: string?
    
    internal override func mapping(mapper: Mapper<any>) {
        id = mapper["product_id"]?.text
        name = mapper["product_name"]?.text
        price = mapper["product_price"]?.text
        quantity = mapper["quantity"]?.text
        photo = mapper["photo_1"]?.text
    }
    
}































