
import UIKit
import Localize_Swift

final class AwardListImagesCell: UITableViewCell, CellProvider
{
    // MARK: - Constants
    
    //MARK: - Variables
    
    internal var album: Album? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var container: UIView!
    @IBOutlet private weak var picture: UIImageView!
    @IBOutlet private weak var overlayPic: UIImageView!
    @IBOutlet private weak var cameraIcon: UIImageView!
    @IBOutlet private weak var lblTitle: UILabel!
    
    
    // MARK: - Overridden functions
    
    internal func updateUI() {
        lblTitle.text = album?.title ?? String.empty
            //((Localize.currentLanguage() == Constants.Language.Arabic) ? album?.arabicTitle : album?.englishTitle) ?? string.empty
        lblTitle.textAlignment = (Localize.currentLanguage() == Constants.Language.Arabic) ? .right : .left
        if let imageLink = album?.cover {
            picture.loadImage(from: imageLink, onComplete: nil)
        }
    }
    
    //MARK: - Actions
}
