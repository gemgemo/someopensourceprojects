
import UIKit



@IBDesignable
final class RadioView: UIView
{
    @IBInspectable
    internal var isSelected: Bool = false {
        didSet {
            setNeedsDisplay();
        }
    }
    
    
    private lazy var markView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    
    internal override func draw(_ rect: CGRect) {
        configure()
    }
    
    private func configure()-> void {
        backgroundColor = .clear
        layer.cornerRadius = bounds.height/2
        layer.borderWidth = 2.0
        var selectedColor: Color
        if (isSelected) {
            selectedColor = Color(red:0.102, green:0.463, blue:0.827, alpha:1)
            markView.backgroundColor = selectedColor
            self.addSubview(markView)
            // constraints
            markView.heightAnchor.constraint(equalToConstant: self.bounds.height/2).isActive = true
            markView.widthAnchor.constraint(equalToConstant: self.bounds.width/2).isActive = true
            markView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
            markView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
            markView.layer.cornerRadius = (bounds.height/2)/2
            markView.clipsToBounds = true
        } else {
            selectedColor = Color.black
            markView.removeFromSuperview()
        }
        layer.borderColor = selectedColor.cgColor
    }
    

}










