
import Foundation




// MARK: - Colors

extension Color {
    
    internal static var normal: Color {
        return Color(red:0.941, green:0.941, blue:0.941, alpha:1)
    }
    
    internal static var selected: Color {
        return Color(red:1, green:1, blue:1, alpha:1)
    }
    
    internal static var fixed: Color {
        return Color(red:0.427, green:0.235, blue:0.090, alpha:1)
    }
    
    
}


// MARK: - Navigation bars

extension UINavigationBar {
    
    internal var castShadow: string {
        get { return "anything fake" }
        set {
            self.layer.shadowOffset = Size(width: 0, height: 1.5)
            self.layer.shadowRadius = 1.0
            self.layer.shadowColor = UIColor.gray.cgColor
            self.layer.shadowOpacity = 0.4
            
        }
    }
}



// MARK: - Image view

extension UIImageView
{
    
    internal func round()-> void {
        layer.cornerRadius = bounds.height/2
        layer.borderColor = Color(white: 0.88, alpha: 0.9).cgColor
        layer.borderWidth = 1.0
    }
    
}

// MARK: - View controllers

extension UIViewController
{
    
    
    
}











