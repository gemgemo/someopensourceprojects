
import UIKit
import Gemo

internal var cPhoto: UIImage?, cType, cPrand, cModel, cStyle, cFuel: string?

class CarMainData: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var selectedImage = UIImage(),
    carType, carPrand, carModel, carStyle, carFuel: string?
    internal var isUpdate = false, car: Car?
    
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var btnSelectImage: UIButton!
    @IBOutlet weak var btnContiune: UIButton!
    @IBOutlet weak var btnCarType: Button!
    @IBOutlet weak var btnCarBrand: Button!
    @IBOutlet weak var btnCarModel: Button!
    @IBOutlet weak var btnCarStyle: Button!
    @IBOutlet weak var btnSpentFuel: Button!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblCarImage: UILabel!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        btnCarType.setTitle(Localization.CarType.localized(), for: .normal)
        btnCarBrand.setTitle(Localization.Prand.localized(), for: .normal)
        btnCarModel.setTitle(Localization.Model.localized(), for: .normal)
        btnCarStyle.setTitle(Localization.Style.localized(), for: .normal)
        btnSpentFuel.setTitle(Localization.SpentFuelType.localized(), for: .normal)
    }
    
    internal override func updateUi() {
        btnContiune.setTitle(Localization.Continuation.localized(), for: .normal)
        lblCarImage.text = Localization.CarImage.localized()
        if (isUpdate) {
            btnCarType.setTitle(car?.type, for: .normal)
            //carType = car?.type
            btnCarBrand.setTitle(car?.prand, for: .normal)
            //carPrand = car?.prand
            btnCarModel.setTitle(car?.model, for: .normal)
            //carModel = car?.prand
            btnCarStyle.setTitle(car?.style, for: .normal)
            //carStyle = car?.style
            btnSpentFuel.setTitle(car?.fuel, for: .normal)
            //carFuel = car?.fuel
            carImage.loadImage(from: car?.userCarPhoto ?? .empty)            
            btnSelectImage.setImage(nil, for: .normal)
        }
    }
    
    
    // MARK: - Actions
    
    @IBAction func selectCarTypeOnCLick(_ sender: UIButton) {
        let rect = sender.convert(sender.bounds, to: view)
        CarInfo.fetchInfo(for: Misc.getUrl(for: "type_car"), self) { [weak self] (carInfo) in
            Threads.main { [weak self] in
                self?.showPopover(in: rect, with: carInfo, onSelect: { (index) in
                    self?.carType = carInfo[index].id
                    sender.setTitle(carInfo[index].title, for: .normal)
                })
            }
        }
    }
    
    @IBAction func selectCarPrandOnCLick(_ sender: UIButton) {
        let rect = sender.convert(sender.bounds, to: view)
        CarInfo.fetchInfo(for: Misc.getUrl(for: "brand_car"), self) { [weak self] (carInfo) in
            Threads.main { [weak self] in
                self?.showPopover(in: rect, with: carInfo, onSelect: { (index) in
                    self?.carPrand = carInfo[index].id
                    sender.setTitle(carInfo[index].title, for: .normal)
                })
            }
        }
    }
    
    @IBAction func selectCarModelOnCLick(_ sender: UIButton) {
        let rect = sender.convert(sender.bounds, to: view)
        //model_car
        CarInfo.fetchInfo(for: Misc.getUrl(for: "model_car"), self) { [weak self] (carInfo) in
            Threads.main { [weak self] in
                self?.showPopover(in: rect, with: carInfo, onSelect: { (index) in
                    self?.carModel = carInfo[index].id
                    sender.setTitle(carInfo[index].title, for: .normal)
                })
            }
        }
    }
    
    @IBAction func selectCarStyleOnCLick(_ sender: UIButton) {
        let rect = sender.convert(sender.bounds, to: view)
        //year_car
        CarInfo.fetchInfo(for: Misc.getUrl(for: "year_car"), self) { [weak self] (carInfo) in
            Threads.main { [weak self] in
                self?.showPopover(in: rect, with: carInfo, onSelect: { (index) in
                    self?.carStyle = carInfo[index].id
                    sender.setTitle(carInfo[index].title, for: .normal)
                })
            }
        }
    }
    
    @IBAction func selectCarSpentFuelTypeOnCLick(_ sender: UIButton) {
        let rect = sender.convert(sender.bounds, to: view)
        //fuel_type
        CarInfo.fetchInfo(for: Misc.getUrl(for: "fuel_type"), self) { [weak self] (carInfo) in
            Threads.main { [weak self] in
                self?.showPopover(in: rect, with: carInfo, onSelect: { (index) in
                    self?.carFuel = carInfo[index].id
                    sender.setTitle(carInfo[index].title, for: .normal)
                })
            }
        }
    }
    
    @IBAction func contiuneOnCLick(_ sender: UIButton) {
        if (isUpdate) {
            cPhoto = selectedImage
            cType = carType ?? .empty
            cModel = carModel ?? .empty
            cStyle = carStyle ?? .empty
            cFuel = carFuel ?? .empty
            cPrand = carPrand ?? .empty
        } else {
            guard let type = carType,
                let model = carModel,
                let style = carStyle,
                let fuel = carFuel,
                let photo = carImage.image,
                let prand = carPrand else {
                    displayAlert(with: Localization.SomeDataReqiured.localized())
                    return
            }
            cPhoto = photo
            cType = type
            cModel = model
            cStyle = style
            cFuel = fuel
            cPrand = prand
        }
        
        NotificationCenter.default.post(name: .ShowBoardNo, object: nil)
    }
    
    @IBAction func chooseImageOnCLick(_ sender: UIButton) {
        openImagePicker(delegate: self)
    }

}


// MARK: - Image picker delegate functions

extension CarMainData: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            selectedImage = editedImage
        } else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            selectedImage = image
        }
        carImage.image = selectedImage
        btnSelectImage.setImage(nil, for: .normal)
        picker.dismiss(animated: true)
    }
    
    internal func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
}

























