
import UIKit

import Localize_Swift

final class InfoTitlesScene: UITableViewCell
{

    
    // MARK: - Constants
    
    //MARK: - Variables
    
    internal var service: Service? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var container: UIView!
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var rightBorder: UIView!

    // MARK: - Overridden functions
    
    internal override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)        
        if (selected) {
            rightBorder.backgroundColor = .selected
            container.backgroundColor = .white
        } else {
            rightBorder.backgroundColor = .normal
            container.backgroundColor = .normal
        }
    }
    
    // MARK: - Functions
    
    private func updateUI()-> void {
        if let serviceTitle = service?.title {
            lblTitle.text = serviceTitle
        }
    }
    
    //MARK: - Actions


}





















