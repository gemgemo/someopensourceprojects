
import UIKit
import Gemo



class Home: BaseController {
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var selectedCars: [int: CarCell] = [:]
    private var userType = Usertype.main
    
    // MARK: - Outlets
    
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var containerPanel: UIView!
    @IBOutlet weak var btnCars: Button!
    @IBOutlet weak var btnPetroLocations: Button!
    @IBOutlet weak var btnBills: Button!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        userType = Usertype(rawValue: UserDefaults.standard.integer(forKey: Misc.UserTypeKey)) ?? .sub
        print("user id: \(UserDefaults.standard.string(forKey: Misc.UserId) ?? .empty)")
        btnAdd.isHidden = userType == .sub
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        unckeckButtons()
        btnCars.isClicked = true
        btnCars.checkImage = nil
        btnBills.checkImage = nil
        btnPetroLocations.checkImage = nil
        if (userType == .main) {
            if let carsPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.Cars) as? Cars {
                carsPage.selectedCarsData = self
                move(this: carsPage, in: containerPanel)
            }
            NotificationCenter.default.addObserver(self, selector: #selector(openReportsMode), name: .OpenReportsMode, object: nil)
        } else {
            gotoCarDetails()
            
        }
    }
    
    internal override func updateUi() {
        super.updateUi()
        UIApplication.shared.isStatusBarHidden = false
        setupNavigationBar(isLogo: true, isMainPage: true)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        btnAdd.setCorner(radius: btnAdd.frame.height / 2)
        btnAdd.imageView?.contentMode = .scaleAspectFit
        btnCars.setTitle((userType == .main) ? Localization.Cars.localized() : Localization.Car.localized(), for: .normal)
        btnBills.setTitle(Localization.MyBills.localized(), for: .normal)
        btnPetroLocations.setTitle(Localization.PetroLocations.localized(), for: .normal)
        
    }
    
    internal override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.shadowImage = UIImage(named: Misc.ShadowImageName)
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: - Actions
    
    @IBAction func showCarsOnCLick(_ sender: Button) {
        unckeckButtons()
        sender.isClicked = true
        if (userType == .main) {
            guard let carsPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.Cars) as? Cars else {
                return
            }
            carsPage.selectedCarsData = self
            btnAdd.isHidden = false
            move(this: carsPage, in: containerPanel)
        } else {
            gotoCarDetails()
        }
    }
    
    @IBAction func showPetroLocationsOnCLick(_ sender: Button) {
        guard let peteroLocationsPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.StationsLocations) as? StationsLocations else {
            return
        }
        unckeckButtons()
        sender.isClicked = true
        btnAdd.isHidden = true
        move(this: peteroLocationsPage, in: containerPanel)
    }
    
    @IBAction func showBillsOnCLick(_ sender: Button) {
        guard let billsPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.MyBills) as? MyBills else {
            return
        }
        unckeckButtons()
        sender.isClicked = true
        btnAdd.isHidden = true
        move(this: billsPage, in: containerPanel)
    }
    
    @IBAction func addOnCLick(_ sender: UIButton) {
        guard let addCarPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.AddCar) as? AddCar else {
            return
        }
        navigate(to: addCarPage)
    }
    
    func openReportsMode() {
        if (isShowReport) {
            setupNavigationBar(isLogo: false, isMainPage: true, title: Localization.ShowReports.localized())
            let btnShowReports = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_check").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(showReportsOnCLick(_:)))
            if (Localizer.instance.current == .arabic) {
                navigationItem.leftBarButtonItem = btnShowReports
            } else {
                navigationItem.rightBarButtonItem = btnShowReports
            }
        } else {
            setupNavigationBar(isLogo: true, isMainPage: true)
            if (Localizer.instance.current == .arabic) {
                navigationItem.leftBarButtonItem = nil
            } else {
                navigationItem.rightBarButtonItem = nil
            }
        }
    }
    
    func showReportsOnCLick(_ sender: UIBarButtonItem) {
        //print(#function)
        guard let reportsPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.Reports) as? Reports else {
            return
        }
        reportsPage.cars = selectedCars
        navigate(to: reportsPage)        
    }

    

}


// MARK: - Helper funtions

fileprivate extension Home {
    
    func gotoCarDetails() -> void {
        if let carDetailsPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.CarDetails) as? CarDetails {
            let car = Car()
            let db = UserDefaults.standard
            car.id = db.string(forKey: Misc.CarId)
            car.photo = db.string(forKey: Misc.CarTypeImage)
            car.type = db.string(forKey: Misc.CarType)
            car.prand = db.string(forKey: Misc.CarPrand)
            car.model = db.string(forKey: Misc.CarModel)
            car.style = db.string(forKey: Misc.CarStyle)
            car.fuel = db.string(forKey: Misc.CarFuel)
            car.aLetters = db.string(forKey: Misc.ALetters)
            car.aNumbers = db.string(forKey: Misc.ANumbers)
            car.eLetters = db.string(forKey: Misc.ELetters)
            car.eNumbers = db.string(forKey: Misc.ENumbers)
            car.userId = db.string(forKey: Misc.UserId)
            car.fullname = db.string(forKey: Misc.FullName)
            car.username = db.string(forKey: Misc.Username)
            car.code = db.string(forKey: Misc.CarCode)
            car.balance = db.string(forKey: Misc.CarBalance)
            car.icon = db.string(forKey: Misc.CarIcon)
            car.userPhoto = db.string(forKey: Misc.Photo)
            car.userCarPhoto = db.string(forKey: Misc.CarPhoto)
            carDetailsPage.car = car
            move(this: carDetailsPage, in: containerPanel)
        }
    }
    
    func unckeckButtons()-> void {
        [btnCars, btnBills, btnPetroLocations].forEach {
            $0?.checkImage = UIImage()
            $0?.isClicked = false
        }
    }
    
}


extension Notification.Name {
    
    static var OpenReportsMode: Notification.Name {
        return Notification.Name("com.open.reports.mode.pe")
    }
    
}

// MARK: - selected cars

extension Home: SelectedCarsData {
    
    internal func selectedCars(_ carsObject: [Int : CarCell]) {
        //print("cars", carsObject)
        selectedCars.removeAll()
        selectedCars = carsObject
    }
    
}















