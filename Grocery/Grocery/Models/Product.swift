
import Foundation
import Gemo


final class Product: Mapper<any>
{
    
    internal var id, storeId, name, price, quantity, details, offer, photo1, photo2, photo3, photo4: string?
    
    internal override var description: String {
        return "\n id: \(id ?? .empty), storeId: \(storeId ?? .empty), name: \(name ?? .empty), description: \(details ?? .empty)  \n"
    }
    
    internal override func mapping(mapper: Mapper<any>) {
        id = mapper["id"]?.string
        storeId = mapper["id_cat"]?.string
        name = mapper["title"]?.string
        price = mapper["price"]?.string
        quantity = mapper["quantity"]?.string
        details = mapper["details"]?.string
        offer = mapper["offer"]?.string
        photo1 = mapper["photo_1"]?.string
        photo2 = mapper["photo_2"]?.string
        photo3 = mapper["photo_3"]?.string
        photo4 = mapper["photo_4"]?.string
    }
    
    
    internal class func getProducts(_ completion: @escaping([Product])->())-> void {
        Gemo.gem.startSpinning()
        Http.request(link: Urls.Products, method: .post,
                     parameters: [Misc.MainKey: Misc.MainValue, "user_id": UserDefaults.standard.string(forKey: Misc.GroceryId) ?? .empty])
            .response { (response) in
                let error = response.error
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("fetch products error is: \(String(describing: error))")
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
//                print("products: \(response.jsonString ?? .empty)")
                guard let json = response.result as? [Dictionary<String, any>] else {
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized())
                    return
                }                
                completion(json.map { Product(JSON: $0) })
        }
    }
    
    internal func delete(_ completion: @escaping()->())-> void {
        Gemo.gem.startSpinning()
        let parameters: [string: any] = [
            Misc.MainKey: Misc.MainValue,
            "user_id": UserDefaults.standard.string(forKey: Misc.GroceryId) ?? .empty,
            "id": self.id ?? .empty
        ]
//        print("delete paramters: \(parameters)")
//        print("delete product response: \(response.jsonString ?? .empty)")
        Http.request(link: Urls.DeleteProduct, method: .post, parameters: parameters)
            .response { (response) in
                let error = response.error
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("delete product error: \(String(describing: error))")
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
                guard let json = (response.result as? [Dictionary<String, any>])?.first,
                    let success = json["result"] as? string,
                    let aMessage = json["data"] as? string,
                    let eMessage = json ["data_en"] as? string else {
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized())
                    return
                }
                if (success == "true") {
                    completion()
                } else {
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: (Localizer.instance.current == .arabic) ? aMessage : eMessage )
                }
        }
    }
    
    internal class func edit(for link: string, with paramters: Dictionary<string, any>, _ completion: @escaping()->())-> void {
        Gemo.gem.startSpinning()
        Http.upload(fileWith: paramters, to: link)
            .response { (response) in
                let error = response.error
                print("add product response: \(response.jsonString ?? .empty)")
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("add product error: \(String(describing: error))")
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
                guard let json = (response.result as? [Dictionary<string, any>])?.first,
                    let aMessage = json["data"] as? string,
                    let eMessage = json["data_en"] as? string else {
                        Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized())
                        return
                }
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: (Localizer.instance.current == .arabic) ? aMessage : eMessage)
                completion()
        }
    }
    
}









































