
import UIKit

final class Search: BaseController
{
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var dataSource = Array<Product>() {
        didSet {
            ThreadQueue.main { [weak self] in
                self?.products.reloadData()
            }
        }
    }
    
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var titlePanel: UIView! { didSet { shadow(for: titlePanel) } }
    @IBOutlet fileprivate weak var txfSearch: TextField! { didSet { txfSearch.delegate = self } }
    @IBOutlet fileprivate weak var products: UICollectionView! {
        didSet {
            products.delegate = self
            products.dataSource = self
        }
    }
    @IBOutlet fileprivate weak var btnSearch: UIButton!
    @IBOutlet fileprivate weak var btnBack: UIButton!
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
        search()
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)        
    }
    
    internal override func becomeFirstResponder() -> Bool {
        return true
    }
    
    
    // MARK: - Actions
    
    @IBAction private func searchOnCLick(_ sender: UIButton) {
        search()
    }
    
    @IBAction private func backClicked(_ sender: UIButton) {
        back()
    }
    
    @objc fileprivate func addToFavouriteOnCLick(_ sender: UIButton) {
        guard let userId = UserDefaults.standard.string(forKey: Constants.Misc.UserID), let productId = dataSource[sender.tag].id else { return }
        add(to: Constants.Urls.Favorite, forUserId: userId, andProductId: productId)
    }
    
    @objc fileprivate func addToBasketOnCLick(_ sender: UIButton) {
        guard let userId = UserDefaults.standard.string(forKey: Constants.Misc.UserID), let productId = dataSource[sender.tag].id else { return }
        add(to: Constants.Urls.Basket, forUserId: userId, andProductId: productId)
    }
    

}

// MARK: - Helper Functions

extension Search
{
    
    fileprivate func updateUi()-> void {
        tabBar.isHidden = true
        view.bringSubview(toFront: titlePanel)
        txfSearch.becomeFirstResponder()
        btnBack.transform = CGAffineTransform.identity
        if (Localizer.instance.current == .arabic) {
            txfSearch.textAlignment = .right
            btnSearch.rightAnchor.constraint(equalTo: titlePanel.rightAnchor, constant: -8.0).isActive = true
            btnBack.leftAnchor.constraint(equalTo: titlePanel.leftAnchor, constant: 8.0).isActive = true
            txfSearch.rightAnchor.constraint(equalTo: btnSearch.leftAnchor, constant: -8.0).isActive = true
            txfSearch.leftAnchor.constraint(equalTo: btnBack.rightAnchor, constant: 8.0).isActive = true
        } else {
            txfSearch.textAlignment = .left
            btnSearch.leftAnchor.constraint(equalTo: titlePanel.leftAnchor, constant: 8.0).isActive = true
            btnBack.rightAnchor.constraint(equalTo: titlePanel.rightAnchor, constant: -8.0).isActive = true
            txfSearch.rightAnchor.constraint(equalTo: btnBack.leftAnchor, constant: -8.0).isActive = true
            txfSearch.leftAnchor.constraint(equalTo: btnSearch.rightAnchor, constant: 8.0).isActive = true
            btnBack.transform = btnBack.transform.rotated(by: cgFloat.pi)
        }
    }
    
    fileprivate func configureCollectionView()-> void {
        products.register(Constants.Nibs.SectionDetails, forCellWithReuseIdentifier: Constants.ReuseIdentifiers.SectionDetails)
        products.alwaysBounceVertical = true
    }
    
    fileprivate func search()-> void {
        if (Gemo.gem.isConnected) {
            Gemo.gem.startSpinning(in: view)
            let parameters: [string: string] = [
                Constants.Misc.MainKey: Constants.Misc.MainValue,
                "title": searchText
            ]
            HttpClient.request(link: Constants.Urls.Search, method: .post, parameters: parameters) { [weak self] (result, response, error) -> void in
                guard let this = self else { return }
                if (error != nil) {
                    print("search error: \(string(describing: error))")
                    Gemo.gem.stopSpinning()
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription, in: this.view)
                    return
                }
                Gemo.gem.stopSpinning()
                guard let json = result as? Dictionary<string, any>, let data = json["all_data"] as? [Dictionary<string, any>] else {
                    return
                }
                this.dataSource = data.map { Product(JSON: $0) }
            }
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
    
}


// MARK: - Text Field delegate functions

extension Search: UITextFieldDelegate
{
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        search()
        return true
    }
    
}



// MARK: - Computed properties

extension Search
{
    fileprivate var searchText: string {
        return txfSearch.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
}


// MARK: - Collectio view delegate & data source functions
extension Search
{
    internal override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (collectionView == products) ? dataSource.count : super.collectionView(collectionView, numberOfItemsInSection: section)
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (collectionView == products) {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ReuseIdentifiers.SectionDetails, for: indexPath) as? SectionDetailsCell else { return super.collectionView(collectionView, cellForItemAt: indexPath) }
            cell.product = dataSource[indexPath.item]
            cell.btnLeft.setImage(#imageLiteral(resourceName: "add-favorite"), for: .normal)
            cell.btnLeft.tag = indexPath.item
            cell.btnRight.tag = indexPath.item
            cell.btnRight.addTarget(self, action: #selector(addToBasketOnCLick(_:)), for: .touchUpInside)
            cell.btnLeft.addTarget(self, action: #selector(addToFavouriteOnCLick(_:)), for: .touchUpInside)
            return cell
        } else {
            return super.collectionView(collectionView, cellForItemAt: indexPath)
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView == products) {
            //print("selected row: \(indexPath)")
            guard let productDetailsPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.ProductDetails) as? ProductDetails else { return }
            productDetailsPage.productId = dataSource[indexPath.item].id
            navigate(to: productDetailsPage)
        } else {
            super.collectionView(collectionView, didSelectItemAt: indexPath)
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return (collectionView == products) ? Size(width: collectionView.frame.width, height: collectionView.frame.height*0.33) : super.collectionView(collectionView, layout: collectionViewLayout, sizeForItemAt: indexPath)
    }
    
}
























