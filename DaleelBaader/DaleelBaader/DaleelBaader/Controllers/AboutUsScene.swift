
import UIKit
import Localize_Swift


final class AboutUsScene: BaseController
{
    
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var webView: UIWebView! {
        didSet {
            webView.delegate = self
        }
    }
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        getData()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBar()
    }
    
    //MARK: - Actions

    @objc fileprivate func dismissOnCLick(_ sender: UIBarButtonItem)-> void {
        dismiss(animated: true, completion: nil)
    }
   
    
}


// MARK: - Helper Functions

extension AboutUsScene
{
    
    fileprivate func updateNavigationBar()-> void {
        navigationItem.title = "aboutUsPageTitle".localized()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_close"), style: .plain, target: self, action: #selector(dismissOnCLick(_:)))
    }
    
    fileprivate func getData()-> void {
        UIApplication.showIndicator(by: true)
        startSpin()
        let link = "http://192.232.214.91/~baaderapplicatio/JSON/about_us_v.php?key=Daleel_Bader"
        if (link.isEmpty || !link.isLink) {
            print("link error")
            dialog(with: Constants.Misc.ErrorMessage)
            UIApplication.showIndicator(by: false)
            stopSpin()
            return
        }
        guard let requestURL = URL(string: link) else {
            print("url error")
            dialog(with: Constants.Misc.ErrorMessage)
            UIApplication.showIndicator(by: false)
            stopSpin()
            return
        }
        
        URLSession.shared.dataTask(with:  URLRequest(url: requestURL)) { [weak self] (data, response, error) in
            if (error != nil) {
                print("response error")
                self?.dialog(with: Constants.Misc.ErrorMessage)
                UIApplication.showIndicator(by: false)
                self?.stopSpin()
                return
            }
            guard let jsonData = data else {
                print("none data")
                self?.dialog(with: Constants.Misc.ErrorMessage)
                UIApplication.showIndicator(by: false)
                self?.stopSpin()
                return
            }
            
            do {
                if let jsonString = (try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [[string: any]])?.first {
                    // handle json response here
                    if let urlString = jsonString["url"] as? string {
                        print(urlString)
                        self?.showVideo(from: urlString)
                        self?.stopSpin()
                        UIApplication.showIndicator(by: false)
                    } else {
                        print("none data")
                        self?.stopSpin()
                        UIApplication.showIndicator(by: false)
                    }
                } else {
                    print("nix data")
                    self?.stopSpin()
                    UIApplication.showIndicator(by: false)
                }
            } catch {
                print("serialization error")
                self?.dialog(with: Constants.Misc.ErrorMessage)
                UIApplication.showIndicator(by: false)
                self?.stopSpin()
            }
            }.resume()
    }

    private func showVideo(from link: string)-> void {
        guard link.isLink else {
            print("not link")
            return
        }
        guard let vidId = link.components(separatedBy: "=").last else {
            print("nil vid id")
            return
        }
        print(vidId)
        DispatchQueue.main.async { [weak self] in
            guard let this = self else { return }
            let width = this.view.bounds.width-10, height = this.view.bounds.height-35
            let videoFrame = "<iframe width=\"\(width)\" height=\"\(height)\" src=\"https://www.youtube.com/embed/\(vidId)?&playsinline=1\" frameborder=\"0\" allowfullscreen></iframe>"
            this.webView.allowsInlineMediaPlayback = true
            this.webView.loadHTMLString(videoFrame, baseURL: nil)
        }
    }
    
}



// MARK: - Web view delegate functions

extension AboutUsScene: UIWebViewDelegate
{
    
    internal func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.showIndicator(by: true)
    }
    
    internal func webViewDidFinishLoad(_ webView: UIWebView) {
        UIApplication.showIndicator(by: false)
    }
    
    internal func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        UIApplication.showIndicator(by: false)
    }
    
}






























