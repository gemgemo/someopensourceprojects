//
//  MyAd.swift
//  Q8School
//
//  Created by Mostafa on 12/18/16.
//  Copyright © 2016 Atiaf. All rights reserved.
//

import Foundation
public class MyAd{
    
    var id: String
    var teacherName: String
    var teacherImg: String
    var detial: String
    var date: String
    
    
    init(id:String,teacherName:String,teacherImg:String,detial:String,date:String) {
        self.id = id
        self.teacherName = teacherName
        self.teacherImg = teacherImg
        self.detial = detial
        self.date = date
    }

}
