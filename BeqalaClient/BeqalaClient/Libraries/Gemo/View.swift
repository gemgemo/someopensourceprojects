
import Foundation
import UIKit

@IBDesignable
internal class View: UIView
{
    
    @IBInspectable
    internal var borderColor: UIColor = UIColor(white: 0.88, alpha: 0.9) {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    internal var borderWidth: CGFloat = 1.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    internal var cornerRadius: CGFloat = 10.0 {
        didSet {
            setNeedsDisplay()
        }
    }

    
    internal override func draw(_ rect: CGRect) {
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
        backgroundColor = UIColor.rgb(red: 249, green: 249, blue: 249, alpha: 1.0)
    }
    
}










































