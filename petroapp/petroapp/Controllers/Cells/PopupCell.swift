
import Foundation
import Gemo

class PopupCell: BaseTableCell {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var item: Item? { didSet { updateUi() } }
    
    // MARK: - Outlets
    
    @IBOutlet weak var lblTitle: UILabel!
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        lblTitle.text = item?.title ?? .empty
        lblTitle.textAlignment = (Localizer.instance.current == .arabic) ? .right : .left
    }
    
    
    // MARK: - Actions

    
}


@objc protocol Item {
    var title: String { get set }
    @objc optional var id: string { get set }
}

























