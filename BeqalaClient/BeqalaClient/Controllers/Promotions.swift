
import UIKit

final class Promotions: BaseController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var dataSource = Array<Promotion>() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.promotionsCollection.reloadData()
            }
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var titlePanal: UIView! {
        didSet {
            shadow(for: titlePanal)
        }
    }
    @IBOutlet fileprivate weak var promotionsCollection: UICollectionView! {
        didSet {
            promotionsCollection.delegate = self
            promotionsCollection.dataSource = self
        }
    }
    @IBOutlet fileprivate weak var btnMenu: UIButton!
    @IBOutlet fileprivate weak var btnSearch: UIButton!
    @IBOutlet fileprivate weak var btnFavorites: UIButton!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
        loadData()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.bringSubview(toFront: titlePanal)
        tabBarItems?.selectItem(at: IndexPath(item: 1, section: 0), animated: false, scrollPosition: .top)
        selectedTabBarItem = 1
        updateUi()
        selectedItemIndex = 3
    }
    
    
    // MARK: - Actions

    @IBAction private func openMenuOnCLick(_ sender: UIButton) {
        menu(withState: .opened)
    }
    
    @IBAction private func searchOnCLick(_ sender: UIButton) {
        navigateToSearch()
    }
    
    @IBAction private func favoriteOnCLick(_ sender: UIButton) {
        navigateToFavorites()
    }
    

}


// MARK: - Helper Functions

extension Promotions
{
    
    fileprivate func configureCollectionView()-> void {
        promotionsCollection.register(Constants.Nibs.Slider, forCellWithReuseIdentifier: Constants.ReuseIdentifiers.Slider)
        promotionsCollection.alwaysBounceVertical = true
        let margin = Insets(top: 0, left: 0, bottom: 60, right: 0)
        promotionsCollection.contentInset = margin
        promotionsCollection.scrollIndicatorInsets = margin
    }
    
    fileprivate func loadData()-> void {
        if (Gemo.gem.isConnected) {
            Gemo.gem.startSpinning(in: view)
            Promotion.getData(page: self) { [weak self] (promotions) in
                self?.dataSource = promotions
                Gemo.gem.stopSpinning()
            }
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
    
    fileprivate func updateUi()-> void {
        btnMenu.transform = CGAffineTransform.identity
        if (Localizer.instance.current == .arabic) {
            btnMenu.rightAnchor.constraint(equalTo: titlePanal.rightAnchor, constant: -8.0).isActive = true
            btnFavorites.leftAnchor.constraint(equalTo: titlePanal.leftAnchor, constant: 8.0).isActive = true
            btnSearch.leftAnchor.constraint(equalTo: btnFavorites.rightAnchor, constant: 20.0).isActive = true
        } else {
            btnMenu.leftAnchor.constraint(equalTo: titlePanal.leftAnchor, constant: 8.0).isActive = true
            btnFavorites.rightAnchor.constraint(equalTo: titlePanal.rightAnchor, constant: -8.0).isActive = true
            btnSearch.rightAnchor.constraint(equalTo: btnFavorites.leftAnchor, constant: -20.0).isActive = true
            btnMenu.transform = btnMenu.transform.rotated(by: cgFloat.pi)
        }

    }
    
}

// MARK: - Collection view delegate & data source functions

extension Promotions {
    
    internal override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch (collectionView) {
        case promotionsCollection:
            return dataSource.count
        default:
            return super.collectionView(collectionView, numberOfItemsInSection: section)
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch(collectionView) {
        case promotionsCollection:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ReuseIdentifiers.Slider, for: indexPath) as? SliderCell else { return UICollectionViewCell() }
            cell.promotion = dataSource[indexPath.item]
            cell.mainPanal.setShadow(with: Size(right: 0, top: 1.5), radius: 0.5, opacity: 0.4, color: .black)
            return cell
        default:
            return super.collectionView(collectionView, cellForItemAt: indexPath)
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch(collectionView) {
        case promotionsCollection:
            //print("selected item is: \(indexPath.item)")
            guard let detailsPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.ProductDetails) as? ProductDetails else { return }
            detailsPage.productId = dataSource[indexPath.item].id
            navigate(to: detailsPage)
        default:
            super.collectionView(collectionView, didSelectItemAt: indexPath)
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        switch(collectionView) {
        case promotionsCollection:
            return 16
        default:
            return 1
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch(collectionView) {
        case promotionsCollection:
            return Size(width: collectionView.frame.width, height: collectionView.frame.height*0.40)
        default:
            return super.collectionView(collectionView, layout: collectionViewLayout, sizeForItemAt: indexPath)
        }
    }
    
}




























