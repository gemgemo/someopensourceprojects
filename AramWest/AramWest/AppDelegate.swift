
import UIKit
import Firebase
import UserNotifications
import Alamofire
import GoogleMaps


internal var who = string.empty, driverId = string.empty, driverAvatar = string.empty, driverName = string.empty, salaryType = string.empty, salary = string.empty, callId = string.empty, carNumber = string.empty, carColor = string.empty, driverPhone = string.empty, carTypeArabic = string.empty, carTypeEnglish = string.empty, isRateDriver = false, isCompany = false, reqId = string.empty, companyId = string.empty, companySalaryType = string.empty, companyImage = string.empty, companyPhone = string.empty, companyName = string.empty, isRateCompany = false

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        configureNavigationBar()
        if (UserDefaults.standard.bool(forKey: Constants.Misc.UserLoggedIn)) {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            if let rootView = mainStoryboard.instantiateInitialViewController() as? SWRevealViewController {
                (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController = rootView
            }
        }
        FIRApp.configure()
        GMSServices.provideAPIKey(Constants.Misc.GoogleMapApiKey)
        return true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return .portrait
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        print("local notification received")        
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        print("local notification registerd")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //print("token data \(deviceToken)")
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: .sandbox)
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: .prod)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("remote notification register error \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(#function, userInfo)
        who            = userInfo["who"] as? string ?? .empty
        driverId       = userInfo["DriverID"] as? string ?? .empty
        driverAvatar   = userInfo["DriverImage"] as? string ?? .empty
        driverName     = userInfo["DriverName"] as? string ?? .empty
        salaryType     = userInfo["SalaryType"] as? string ?? .empty
        salary         = userInfo["Salary"] as? string ?? .empty
        callId         = userInfo["CallID"] as? string ?? .empty
        carNumber      = userInfo["CarNum"] as? string ?? .empty
        carColor       = userInfo["CarColor"] as? string ?? .empty
        driverPhone    = userInfo["DriverPhone"] as? string ?? .empty
        carTypeArabic  = userInfo["CarTypeA"] as? string ?? .empty
        carTypeEnglish = userInfo["CarType"] as? string ?? .empty
        reqId          = userInfo["ReqID"] as? string ?? .empty
        companyId      = userInfo["CompanyID"] as? string ?? .empty
        companySalaryType = userInfo["typeSalary"] as? string ?? .empty
        companyImage = userInfo["img"] as? string ?? .empty
        companyName = userInfo["name"] as? string ?? .empty
        if (application.applicationState == .active) {
            NotificationCenter.default.post(name: Constants.Misc.ForgroundNotification, object: nil)
        } else {            
            checkNotification()
        }
        completionHandler(.newData)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        connectToFcm()
    }

}


// MARK: - Helper Functions

extension AppDelegate
{
    
    fileprivate func checkNotification()-> void {
        print("who is \(who)") // 4 to end mission
        switch (who) {
        case "14", "3":
            NotificationCenter.default.post(name: Constants.Misc.UserJourney, object: nil)
            
        case "12":
            NotificationCenter.default.post(name: Constants.Misc.Message, object: nil)
            
        case "15":
            NotificationCenter.default.post(name: Constants.Misc.MissionEnded, object: nil)
            
        case "1":
            NotificationCenter.default.post(name: Constants.Misc.DriverMessage, object: nil)
            
        case "4":
            isRateDriver = true
            NotificationCenter.default.post(name: Constants.Misc.MissionEnded, object: nil)
            
        case "6":
            isCompany = true
            NotificationCenter.default.post(name: Constants.Misc.DriverMessage, object: nil)
            
        case "10":
            isRateCompany = true
            NotificationCenter.default.post(name: Constants.Misc.MissionEnded, object: nil)
            
        default:
            break
        }
    }
    
    fileprivate func configureNavigationBar()-> void {
        let navBar = UINavigationBar.appearance()
        navBar.titleTextAttributes = [NSForegroundColorAttributeName: Color.gray]
    }
    
    fileprivate func connectToFcm() {
        // Won't connect since there is no token
        guard FIRInstanceID.instanceID().token() != nil else {
            return
        }
        FIRMessaging.messaging().disconnect()
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
}






















