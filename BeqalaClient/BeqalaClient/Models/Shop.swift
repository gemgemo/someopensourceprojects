
import Foundation

internal final class Shop: Mapper<Any>
{
    
    internal var id, userName, shopName, result, email, phone, info, latitude, longitude, photo, userId, rate: string?, productsCount: int?, productsTypes = [string]()
    
    internal override var description: String {
        return "\n id: \(string(describing: id)), shopName: \(string(describing: shopName)) \n"
    }
    
    
    internal override func mapping(mapper: Mapper<Any>) {
        id = mapper["id"]?.text
        userName = mapper["username"]?.text
        shopName = mapper["matjer_name"]?.text
        result = mapper["result"]?.text
        email = mapper["email"]?.text
        phone = mapper["phone"]?.text
        info = mapper["description"]?.text
        latitude = mapper["lati"]?.text
        longitude = mapper["longi"]?.text
        photo = mapper["photo_users"]?.text
        productsCount = mapper["num_product"]?.integer
        userId = mapper["user_id"]?.text
        rate = mapper["average"]?.text
        productsTypes = mapper["spec"]?.array as? [string] ?? Array()
    }
    
    
    internal class func getData(from link : string, withParameters params: [string: any], _ page: BaseController, _ completion: @escaping([Shop])->())-> void {
        Gemo.gem.startSpinning(in: page.view)
        HttpClient.request(link: link, method: .post, parameters: params) { (data, response, error) -> void in
            
            if (error != nil) {
                Gemo.gem.stopSpinning()
                print("fetch shops error is: \(string(describing: error))")
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription, in: page.view)
                return
            }
            Gemo.gem.stopSpinning()
            //print("stores \(string(describing: data))")
            guard let json = data as? [Dictionary<string, any>] else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized(), in: page.view)
                return
            }
            completion(json.map { Shop(JSON: $0) })
        }

    }
    
    
}































