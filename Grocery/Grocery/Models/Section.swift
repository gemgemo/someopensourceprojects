
import Foundation
import Gemo

final class Section: Mapper<Any>
{
    internal var id, title: string?
    
    internal override var description: String {
        return "\n id: \(id ?? .empty), title: \(title ?? .empty) \n"
    }
    
    internal override func mapping(mapper: Mapper<Any>) {
        id = mapper["id"]?.string
        title = mapper["title"]?.string
    }
    
   
    internal class func getSections(for link: string, with parameters: Dictionary<string, any>, _ compeltion: @escaping([Section])->())-> void {
        UIApplication.showIndicator(by: true)
        Http.request(link: link, method: .post, parameters: parameters)
            .response { (response) in
                let error = response.error
                if (error != nil) {
                    print("fetching sections error is: \(String(describing: error))")
                    UIApplication.showIndicator(by: false)
                    return
                }
                //print("fetching sections data: \(String(describing: response.result))")
                UIApplication.showIndicator(by: false)
                guard let json = response.result as? Dictionary<string, any>, let data = json["all_data"] as? [Dictionary<string, any>] else {
                    print("no sections")
                    return
                }
                compeltion(data.map { Section(JSON: $0) })
        }
    }
    
    
}


final class SelectedSection: Mapper<any>
{
    internal var id, title: string?
    
    internal override var description: String {
        return "\n id: \(id ?? .empty), title: \(title ?? .empty) \n"
    }
    
    internal override func mapping(mapper: Mapper<Any>) {
        id = mapper["id_cat"]?.string
        title = mapper["title"]?.string
    }
    
    
    internal class func getSections(for link: string, with parameters: Dictionary<string, any>, _ compeltion: @escaping([SelectedSection])->())-> void {
        UIApplication.showIndicator(by: true)
        Http.request(link: link, method: .post, parameters: parameters)
            .response { (response) in
                let error = response.error
                //print("fetching sections data: \(String(describing: response.result))")
                if (error != nil) {
                    print("fetching selected sections error is: \(String(describing: error))")
                    UIApplication.showIndicator(by: false)
                    return
                }
                UIApplication.showIndicator(by: false)
                guard let json = response.result as? [Dictionary<string, any>] else {
                    print("can't cast response")
                    return
                }
                compeltion(json.map { SelectedSection(JSON: $0) })
        }
    }
    
}






































