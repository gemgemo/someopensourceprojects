
import UIKit


final class PdfViewer: BaseController
{

    internal var link: string?
    
    @IBOutlet private weak var webView: UIWebView! {
        didSet {
            webView.delegate = self
        }
    }
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.castShadow = "set"
        navigationItem.title = string.empty
        navigationController?.navigationBar.tintColor = .fixed
        updateNavigationBarDirections()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_close"), style: .plain, target: self, action: #selector(close(_:)))
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let urlString = link , let url = URL(string: urlString) {
            webView.loadRequest(URLRequest(url: url))
        }
    }
    
    
    @objc private func close(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

}




extension PdfViewer: UIWebViewDelegate
{
    
    internal func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.showIndicator(by: true)
    }
    
    internal func webViewDidFinishLoad(_ webView: UIWebView) {
        UIApplication.showIndicator(by: false)
    }
    
    internal func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        UIApplication.showIndicator(by: false)
    }
}






















