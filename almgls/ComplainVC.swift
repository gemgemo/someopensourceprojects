
import UIKit
import Alamofire

class ComplainVC: UIViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextViewDelegate,UITextFieldDelegate {

    @IBOutlet weak var loadingLbl: UILabel!
    @IBOutlet weak var LoadingActivty: UIActivityIndicatorView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var btn8: UIButton!
    @IBOutlet weak var btn7: UIButton!
    @IBOutlet weak var btn6: UIButton!
    @IBOutlet weak var btn5: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var myLocationLbl: UILabel!
    @IBOutlet weak var bottoomView: UIView!
    @IBOutlet weak var midleView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var img8: UIImageView!
    @IBOutlet weak var img7: UIImageView!
    @IBOutlet weak var img6: UIImageView!
    @IBOutlet weak var img5: UIImageView!
    @IBOutlet weak var img4: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var view9: UIView!
    @IBOutlet weak var view8: UIView!
    @IBOutlet weak var view7: UIView!
    @IBOutlet weak var view6: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var complainTxtView: UITextView!
    @IBOutlet weak var complainTitle: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    var imgcount : Int!
    var selectedName:String!
    var selectedMobile:String!
    var selectedEmail:String!
    internal var idNumber = ""
    var lati: String!
    var long: String!
    var result: String!
    var data: String!
    var dataEn:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.alwaysBounceVertical = true
        scrollView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.shadowViewTapped(_:)))
        scrollView.addGestureRecognizer(tapGesture)
        
        scrollView.contentSize.height = 636
        complainTxtView.layer.cornerRadius = 10
        complainTxtView.layer.masksToBounds = true
        
        sendBtn.layer.cornerRadius = 10
        sendBtn.layer.masksToBounds = true
        view1.layer.cornerRadius = 10
        view1.layer.masksToBounds = true
        view2.layer.cornerRadius = 10
        view2.layer.masksToBounds = true
        view3.layer.cornerRadius = 10
        view3.layer.masksToBounds = true
        view4.layer.cornerRadius = 10
        view4.layer.masksToBounds = true
        view5.layer.cornerRadius = 10
        view5.layer.masksToBounds = true
        view6.layer.cornerRadius = 10
        view6.layer.masksToBounds = true
        view7.layer.cornerRadius = 10
        view7.layer.masksToBounds = true
        view8.layer.cornerRadius = 10
        view8.layer.masksToBounds = true
        view9.layer.cornerRadius = 10
        view9.layer.masksToBounds = true
        complainTxtView.delegate = self
        complainTitle.delegate = self
        loadingView.isHidden = true
        shadowView.isHidden = true
        let phoneNumber = UserDefaults.standard.string(forKey: "phone")!
        let email = UserDefaults.standard.string(forKey: "email")!
        let name = UserDefaults.standard.string(forKey: "name")!
        let number = UserDefaults.standard.string(forKey: "image")!
        selectedName = name
        idNumber = number
        selectedEmail = email
        selectedMobile = phoneNumber
        self.complainTitle.attributedPlaceholder = NSAttributedString(string:"Proposal  Title",attributes:[NSForegroundColorAttributeName: UIColor.black])
        print(Settings.complainV)
        if Settings.complainV == "1"
        {
            print("here")

            if Settings.lang == "ar"
            {
                self.complainTitle.attributedPlaceholder = NSAttributedString(string:"عنوان الشكوى",attributes:[NSForegroundColorAttributeName: UIColor.black])
                complainTitle.placeholder = "عنوان الشكوى"
                complainTitle.textAlignment = .right
                complainTxtView.text = "الشكوى"
                complainTxtView.textAlignment = .right
                sendBtn.setTitle("إرسال", for: .normal)
                myLocationLbl.text = "تحديد موقعك"
                headerView.semanticContentAttribute = .forceRightToLeft
                midleView.semanticContentAttribute = .forceRightToLeft
                bottoomView.semanticContentAttribute = .forceRightToLeft
                self.title = "إرسال شكوى"
                loadingLbl.text = "جاري التحميل..."
            }
            else
            {
                self.complainTitle.attributedPlaceholder = NSAttributedString(string:"Complain Title",attributes:[NSForegroundColorAttributeName: UIColor.black])
                complainTitle.placeholder = "Complain Title"
                complainTxtView.text = "Your Complain"
                self.title = "Send Complain"
                loadingLbl.text = "loading..."
            }
        }
        else
        {
            print("here1")

            self.title = "Send Proposal"
            if Settings.lang == "ar"
            {
                print("here2")
                self.complainTitle.attributedPlaceholder = NSAttributedString(string:"عنوان المقترح",attributes:[NSForegroundColorAttributeName: UIColor.black])
                complainTitle.placeholder = "عنوان المقترح"
                complainTitle.textAlignment = .right
                complainTxtView.text = "المقترح"
                complainTxtView.textAlignment = .right
                sendBtn.setTitle("إرسال", for: .normal)
                myLocationLbl.text = "تحديد موقعك"
                headerView.semanticContentAttribute = .forceRightToLeft
                midleView.semanticContentAttribute = .forceRightToLeft
                bottoomView.semanticContentAttribute = .forceRightToLeft
                self.title = "إرسال مقترح"
                loadingLbl.text = "جاري التحميل..."
            }

        }
        // Do any additional setup after loading the view.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    private func alertView()-> Void {
        let alert = UIAlertController(title: "Select Image", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in            
            let ImagePicker = UIImagePickerController()
            ImagePicker.delegate = self
            ImagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self.present(ImagePicker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            let ImagePicker = UIImagePickerController()
            ImagePicker.delegate = self
            ImagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.present(ImagePicker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
          print("view tapped")
        self.view.endEditing(true)
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        scrollView.delegate = self
    }
    
    @objc private func shadowViewTapped(_ gesture: UITapGestureRecognizer) {
        print("view tapped")
        view.endEditing(true)
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if Settings.complainV == "1"
        {
            if Settings.lang == "ar"
            {
                if self.complainTxtView.text == "الشكوى"{
                    self.complainTxtView.text = ""
                }
                
            }
            else
            {
                if self.complainTxtView.text == "Your Complain"{
                    self.complainTxtView.text = ""
                }
                print("here")
            }

        } else {
            if Settings.lang == "ar"
            {
                if self.complainTxtView.text == "المقترح" {
                    self.complainTxtView.text = ""
                }
                
            }
            else
            {
                if self.complainTxtView.text == "Your Proposal" {
                    self.complainTxtView.text = ""
                }
                print("here")
            }

        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if Settings.complainV == "1"
        {
            if Settings.lang == "ar"
            {
                
                if self.complainTxtView.text == ""{
                    self.complainTxtView.text = "الشكوي"
                }
                
            }
            else
            {
                
                if self.complainTxtView.text == ""
                {
                    self.complainTxtView.text = "Your Complain"
                }
                
                
            }
            

        }
        else
        {
            if Settings.lang == "ar"
            {
                if self.complainTxtView.text == ""{
                    self.complainTxtView.text = "المقترح"
                }
                
            }
            else
            {
                if self.complainTxtView.text == ""{
                    self.complainTxtView.text = "Your Proposal"
                }
                print("here")
                
                
                
            }

            
        }
    }
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        if(text == "\n") {
//            textView.resignFirstResponder()
//            return false
//        }
//        if (range.length == 0 )
//        {
//                if ([text.E])
//        }
//        return true
//    }


    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        //let percentage: CGFloat = 0.30
        if imgcount == 1 {
            img1.image = selectedImage//.resized(with: percentage)
            btn1.setImage(UIImage(named: "camera1"), for: UIControlState.normal)
            self.dismiss(animated: true, completion: nil)
        }
        else if imgcount == 2{
            img2.image = selectedImage//.resized(with: percentage)
            btn2.setImage(UIImage(named: "camera1"), for: UIControlState.normal)
            self.dismiss(animated: true, completion: nil)
        }
        else if imgcount == 3{
            img3.image = selectedImage//.resized(with: percentage)
            btn3.setImage(UIImage(named: "camera1"), for: UIControlState.normal)

            self.dismiss(animated: true, completion: nil)
        }
        else if imgcount == 4{
            img4.image = selectedImage//.resized(with: percentage)
            btn4.setImage(UIImage(named: "camera1"), for: UIControlState.normal)

            self.dismiss(animated: true, completion: nil)
        }
        else if imgcount == 5{
            img5.image = selectedImage//.resized(with: percentage)
            btn5.setImage(UIImage(named: "camera1"), for: UIControlState.normal)

            self.dismiss(animated: true, completion: nil)
        }
        else if imgcount == 6{
            img6.image = selectedImage//.resized(with: percentage)
            btn6.setImage(UIImage(named: "camera1"), for: UIControlState.normal)

            self.dismiss(animated: true, completion: nil)
        }
        else if imgcount == 7{
            img7.image = selectedImage//.resized(with: percentage)
            btn7.setImage(UIImage(named: "camera1"), for: UIControlState.normal)

            self.dismiss(animated: true, completion: nil)
        }
        else {
            img8.image = selectedImage//.resized(with: percentage)
            btn8.setImage(UIImage(named: "camera1"), for: UIControlState.normal)

            self.dismiss(animated: true, completion: nil)
        }
        
    }
   
    func AlertMessage(_ Message:String,_ Error: String) {
        var t = ""
        var b = ""
        
        if Settings.lang == "ar"{
            t = "تنبيه"
            b = "إغلاق"
        }
        else{
            t = "Alert"
            b = "Close"
        }
        if Error == ""
        {
            let myAlert = UIAlertController(title: t, message:Message, preferredStyle: UIAlertControllerStyle.alert);
            
            let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                self.navigationController?.pushViewController(vc, animated: true)

            })
            myAlert.addAction(okAction);
            
            self.present(myAlert, animated: true, completion: nil)
            
        }
        else
        {
            let myAlert = UIAlertController(title: t, message:Error, preferredStyle: UIAlertControllerStyle.alert);
            
            let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler:nil)
            
            myAlert.addAction(okAction);
            
            self.present(myAlert, animated: true, completion: nil)
        }
        
    
    }
    
    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        //self.navigationController?.popToRootViewController(animated: true)//!.popViewController(animated: true)
        //guard let rootPage = storyboard?.instantiateInitialViewController() else { return }
        //_ = navigationController?.popToViewController(rootPage, animated: true)
        if let rootPage = storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController {
            navigationController?.pushViewController(rootPage, animated: false)
        }
    }

    @IBAction func img1Btn(_ sender: Any) {
        imgcount = 1
        alertView()

    }
    
    @IBAction func img2Btn(_ sender: UIButton) {
        imgcount = 2
        alertView()
    }
    
    @IBAction func img3Btn(_ sender: UIButton) {
        imgcount = 3
      
        alertView()

    }
    
    @IBAction func img4Btn(_ sender: UIButton) {
        imgcount = 4
       
        
        alertView()

    }
    
    @IBAction func img5Btn(_ sender: UIButton) {
        imgcount = 5
     
        alertView()

    }
    
    @IBAction func img6Btn(_ sender: UIButton) {
        imgcount = 6
       
        alertView()
    }
    
    @IBAction func img7Btn(_ sender: UIButton) {
        imgcount = 7
 
        alertView()
    }
    
    @IBAction func img8Btn(_ sender: UIButton) {
        imgcount = 8
 
        alertView()

    }
    
    /*private func compress(this image: UIImage)-> Data? {
        var compression: CGFloat = 0.9, maxCompression: CGFloat = 0.1, maxSize = 25*1024
        if var imageData = UIImageJPEGRepresentation(image, compression) {
            while imageData.count > maxSize && compression > maxCompression {
                compression -= 0.1
                imageData = UIImageJPEGRepresentation(image, compression)!
                return imageData
            }
        }
        return nil
    }*/
    
    @IBAction func sendBtn(_ sender: UIButton) {
        if (Settings.isConnected) {
            loadingLbl.font = UIFont.systemFont(ofSize: 10.0)
            loadingLbl.text = "جاري التحميل من فضلك انتظر....."
            let imageEx = "\(Date().timeIntervalSinceNow).png"
            let manager = Alamofire.SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            loadingView.isHidden = false
            shadowView.isHidden = false
            loadingView.layer.cornerRadius = 10
            loadingView.layer.masksToBounds = true
            LoadingActivty.startAnimating()
            if complainTitle.text!.isEmpty == false && complainTxtView.text!.isEmpty == false && UserDefaults.standard.string(forKey: "latitude") != nil && UserDefaults.standard.string(forKey: "longitude") != nil
            {
                let imageQuality: CGFloat = 0.25
                if Settings.complainV == "1"
                {
                    print("compalin")
                    self.lati = UserDefaults.standard.string(forKey: "latitude")!
                    self.long = UserDefaults.standard.string(forKey: "longitude")!
                    
                    manager.upload(
                        multipartFormData: { multipartFormData in
                            if self.img1.image != nil
                            {
                                let imageData0 = UIImageJPEGRepresentation(self.img1.image!, imageQuality)
                                multipartFormData.append(imageData0!, withName: "photo_1", fileName: imageEx, mimeType: "image/jpeg")
                            }
                            if self.img2.image != nil
                            {
                                let imageData1 = UIImageJPEGRepresentation(self.img2.image!, imageQuality)
                                multipartFormData.append(imageData1!, withName: "photo_2", fileName: imageEx , mimeType: "image/jpeg")
                            }
                            if self.img3.image != nil
                            {
                                let imageData2 = UIImageJPEGRepresentation(self.img3.image!, imageQuality)
                                multipartFormData.append(imageData2!, withName: "photo_3", fileName: imageEx, mimeType: "image/jpeg")
                            }
                            if self.img4.image != nil
                            {
                                let imageData3 =  UIImageJPEGRepresentation(self.img4.image!, imageQuality)
                                multipartFormData.append(imageData3!, withName: "photo_4", fileName: imageEx, mimeType: "image/jpeg")
                                
                            }
                            if self.img5.image != nil
                            {
                                let imageData4 =  UIImageJPEGRepresentation(self.img5.image!, imageQuality)
                                multipartFormData.append(imageData4!, withName: "photo_5", fileName: imageEx, mimeType: "image/jpeg")
                                
                            }
                            if self.img6.image != nil
                            {
                                let imageData5 =  UIImageJPEGRepresentation(self.img6.image!, imageQuality)
                                multipartFormData.append(imageData5!, withName: "photo_6", fileName: imageEx, mimeType: "image/jpeg")
                            }
                            if self.img7.image != nil
                            {
                                let imageData6 =  UIImageJPEGRepresentation(self.img7.image!, imageQuality)
                                multipartFormData.append(imageData6!, withName: "photo_7", fileName: imageEx, mimeType: "image/jpeg")
                            }
                            if self.img8.image != nil
                            {
                                let imageData7 =  UIImageJPEGRepresentation(self.img8.image!, imageQuality)
                                multipartFormData.append(imageData7!, withName: "photo_8", fileName: imageEx, mimeType: "image/jpeg")
                            }
                            
                            //multipartFormData.append(self.selectedImg!, withName: "user_photo", fileName: imageEx, mimeType: "image/jpeg")
                            multipartFormData.append(self.idNumber.data(using: .utf8)!, withName: "num_p")
                            multipartFormData.append(self.complainTitle.text!.data(using: .utf8)!, withName: "title")
                            multipartFormData.append(self.complainTxtView.text!.data(using: .utf8)!, withName: "content" )
                            multipartFormData.append(self.selectedName!.data(using: .utf8)!, withName: "name")
                            multipartFormData.append(self.selectedMobile!.data(using: .utf8)!, withName: "phone")
                            multipartFormData.append(self.selectedEmail!.data(using: .utf8)!, withName: "email")
                            multipartFormData.append(self.lati!.data(using: .utf8)!, withName: "lati")
                            multipartFormData.append(self.long!.data(using: .utf8)!, withName: "longi")
                            print("images multiparted")
                    },
                        to: Settings.ComplainUrl,
                        
                        encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                //print("request is: ", upload.response)
                                upload.uploadProgress(closure: { (progrees) in
                                    print("total unit", progrees.totalUnitCount)
                                    print("progress", progrees.fractionCompleted)
                                })
                                upload.responseString { response in
                                    debugPrint(response)
                                }
                                upload.responseJSON { response in
                                    if (response.result.error != nil) {
                                        print("upload response error is: ", response.result.error!)
                                        self.loadingView.isHidden = true
                                        self.shadowView.isHidden = true
                                        self.LoadingActivty.stopAnimating()
                                        self.sendBtn.isEnabled = true
                                        self.AlertMessage("", "حدث خطأ")
                                        return
                                    }
                                    DispatchQueue.main.async
                                        {
                                            if let test = response.result.value as? [NSDictionary]{
                                                print("this test ", test)
                                                for i in test
                                                {
                                                    self.result = i["result"] as! String
                                                    if self.result == "true"
                                                    {
                                                        self.data = i["data"] as! String
                                                        self.dataEn = i["data_en"] as! String
                                                    }
                                                    else
                                                    {
                                                        self.data = i["data"] as! String
                                                        self.dataEn = i["data_en"] as! String
                                                        
                                                    }
                                                }
                                                
                                            }
                                            
                                            if self.result == "true"{
                                                if Settings.lang == "ar"
                                                {
                                                    self.loadingView.isHidden = true
                                                    self.shadowView.isHidden = true
                                                    self.LoadingActivty.stopAnimating()
                                                    self.sendBtn.isEnabled = true
                                                    //if (!self.data.isEmpty) {
                                                    self.AlertMessage(self.data, "")
                                                    //}
                                                }
                                                else
                                                {
                                                    self.loadingView.isHidden = true
                                                    self.shadowView.isHidden = true
                                                    self.LoadingActivty.stopAnimating()
                                                    self.sendBtn.isEnabled = true
                                                    //if (!self.dataEn.isEmpty) {
                                                    self.AlertMessage(self.dataEn,"")
                                                    //}
                                                }
                                                
                                            }
                                            else
                                            {
                                                if Settings.lang == "ar"
                                                {
                                                    self.loadingView.isHidden = true
                                                    self.shadowView.isHidden = true
                                                    self.LoadingActivty.stopAnimating()
                                                    self.sendBtn.isEnabled = true
                                                    //if (!self.data.isEmpty) {
                                                    self.AlertMessage("", self.data)
                                                    //}
                                                }
                                                else
                                                {
                                                    self.loadingView.isHidden = true
                                                    self.shadowView.isHidden = true
                                                    self.LoadingActivty.stopAnimating()
                                                    self.sendBtn.isEnabled = true
                                                    //if (!self.dataEn.isEmpty) {
                                                    self.AlertMessage("",self.dataEn)
                                                    //}
                                                }
                                                
                                            }
                                            
                                            
                                    }
                                    
                                }
                            case .failure(let encodingError):
                                print("we falied\(encodingError)")
                            }
                    }
                    )
                    
                }
                else
                {
                    print("proposal")
                    loadingView.isHidden = false
                    shadowView.isHidden = false
                    loadingView.layer.cornerRadius = 10
                    loadingView.layer.masksToBounds = true
                    LoadingActivty.startAnimating()
                    self.lati = UserDefaults.standard.string(forKey: "latitude")!
                    self.long = UserDefaults.standard.string(forKey: "longitude")!
                    let URLb="http://192.232.214.91/~almglsalbldyinmk//prop.php?key=albldy"
                    manager.upload(
                        multipartFormData: { multipartFormData in
                            if self.img1.image != nil
                            {
                                let imageData0 = UIImageJPEGRepresentation(self.img1.image!, imageQuality)
                                multipartFormData.append(imageData0!, withName: "photo_1", fileName: imageEx, mimeType: "image/jpeg")
                            }
                            if self.img2.image != nil
                            {
                                let imageData1 = UIImageJPEGRepresentation(self.img2.image!, imageQuality)
                                multipartFormData.append(imageData1!, withName: "photo_2", fileName: imageEx , mimeType: "image/jpeg")
                            }
                            if self.img3.image != nil
                            {
                                let imageData2 = UIImageJPEGRepresentation(self.img3.image!, imageQuality)
                                multipartFormData.append(imageData2!, withName: "photo_3", fileName: imageEx, mimeType: "image/jpeg")
                            }
                            if self.img4.image != nil
                            {
                                let imageData3 =  UIImageJPEGRepresentation(self.img4.image!, imageQuality)
                                multipartFormData.append(imageData3!, withName: "photo_4", fileName:  imageEx, mimeType: "image/jpeg")
                                
                            }
                            if self.img5.image != nil
                            {
                                let imageData4 =  UIImageJPEGRepresentation(self.img5.image!, imageQuality)
                                multipartFormData.append(imageData4!, withName: "photo_5", fileName: imageEx, mimeType: "image/jpeg")
                                
                            }
                            if self.img6.image != nil
                            {
                                let imageData5 =  UIImageJPEGRepresentation(self.img6.image!, imageQuality)
                                multipartFormData.append(imageData5!, withName: "photo_6", fileName: imageEx, mimeType: "image/jpeg")
                                
                            }
                            if self.img7.image != nil
                            {
                                let imageData6 =  UIImageJPEGRepresentation(self.img7.image!, imageQuality)
                                multipartFormData.append(imageData6!, withName: "photo_7", fileName: imageEx, mimeType: "image/jpeg")
                                
                            }
                            if self.img8.image != nil
                            {
                                let imageData7 =  UIImageJPEGRepresentation(self.img8.image!, imageQuality)
                                multipartFormData.append(imageData7!, withName: "photo_8", fileName: imageEx, mimeType: "image/jpeg")
                                
                            }
                            
                            multipartFormData.append(self.complainTitle.text!.data(using: .utf8)!, withName: "title")
                            multipartFormData.append(self.complainTxtView.text!.data(using: .utf8)!, withName: "content" )
                            multipartFormData.append(self.selectedName!.data(using: .utf8)!, withName: "name")
                            multipartFormData.append(self.selectedMobile!.data(using: .utf8)!, withName: "phone")
                            multipartFormData.append(self.selectedEmail!.data(using: .utf8)!, withName: "email")
                            multipartFormData.append(self.lati!.data(using: .utf8)!, withName: "lati")
                            multipartFormData.append(self.long!.data(using: .utf8)!, withName: "longi")
                            print("images multiparted")
                    },
                        to: URLb,
                        
                        encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.uploadProgress(closure: { (progrees) in
                                    print("total unit", progrees.totalUnitCount)
                                    print("progress", progrees.fractionCompleted)
                                })
                                upload.responseString { response in
                                    debugPrint(response)
                                }
                                upload.responseJSON { response in
                                    debugPrint(response)
                                    let error = response.result.error
                                    if (error != nil) {
                                        print("send moqtarah error is: ", error!)
                                        self.loadingView.isHidden = true
                                        self.shadowView.isHidden = true
                                        self.LoadingActivty.stopAnimating()
                                        self.sendBtn.isEnabled = true
                                        self.AlertMessage("", "حدث خطأ")
                                        
                                        return
                                    }
                                    //print(response)
                                    if let test = response.result.value as? [NSDictionary]
                                    {
                                        for i in test
                                        {
                                            self.result = i["result"] as! String
                                            if self.result == "true"
                                            {
                                                self.data = i["data"] as! String
                                                self.dataEn = i["data_en"] as! String
                                            }
                                            else
                                            {
                                                self.data = i["data"] as! String
                                                self.dataEn = i["data_en"] as! String
                                                
                                            }
                                        }
                                    }
                                    DispatchQueue.main.async
                                        {
                                            self.loadingView.isHidden = true
                                            self.shadowView.isHidden = true
                                            self.LoadingActivty.stopAnimating()
                                            self.sendBtn.isEnabled = true
                                            if self.result == "true"{
                                                if Settings.lang == "ar"
                                                {
                                                    self.AlertMessage(self.data, "")
                                                }
                                                else
                                                {
                                                    self.AlertMessage(self.dataEn, "")
                                                }
                                                
                                            }
                                            else
                                            {
                                                if Settings.lang == "ar"
                                                {
                                                    self.AlertMessage("", self.data)
                                                    
                                                }
                                                else
                                                {
                                                    self.AlertMessage("", self.dataEn)
                                                }
                                                
                                                
                                            }
                                            
                                            
                                    }
                                    
                                }
                            case .failure(let encodingError):
                                print("we falied\(encodingError)")
                            }
                    }
                    )
                    
                }
                
            }
            else
            {
                if Settings.lang == "ar"
                {
                    let msgA = "من فضلك أدخل بياناتك"
                    self.AlertMessage("", msgA)
                    self.sendBtn.isEnabled = true
                    self.loadingView.isHidden = true
                    self.shadowView.isHidden = true
                    self.LoadingActivty.stopAnimating()
                    
                    
                }
                else
                {
                    let msg = "Please Fill all Field"
                    self.AlertMessage("", msg)
                    self.sendBtn.isEnabled = true
                    self.loadingView.isHidden = true
                    self.shadowView.isHidden = true
                    self.LoadingActivty.stopAnimating()
                    
                    
                }
            }

        } else {
            dialogUser(with: "لا يوجد انترنت")
        }
    }
    
    
    @IBAction func mapBtn(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MapVc") as! MapVc
        self.navigationController?.pushViewController(vc, animated: true)

        
    }
    
   

}




extension UIImage
{
    
    
    internal func resized(with percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    /*internal func compress()-> Data? {
        var compression: CGFloat = 0.9, maxCompression: CGFloat = 0.1, maxSize = 25*1024
        if var imageData = UIImageJPEGRepresentation(self, compression) {
            while imageData.count > maxSize && compression > maxCompression {
                compression -= 0.1
                imageData = UIImageJPEGRepresentation(self, compression)!
                return imageData
            }
        }
        return nil
    }
    */
}



