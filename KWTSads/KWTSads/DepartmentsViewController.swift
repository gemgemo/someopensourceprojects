
import UIKit
import SwiftSpinner


class DepartmentsViewController: UIViewController,
    UICollectionViewDataSource,
    UICollectionViewDelegate
{
   
    @IBOutlet var countryNames: UILabel!
    let defaults = UserDefaults.standard


    var countryName: String!
    var images_cache = [String:UIImage]()
    var departmentName = [String]()
    var departmentImg = [String]()
    var departmentColor = [String]()
    var departmentId = [String]()
    var bannerImg = [String]()
    var bannerImg2 = [String]()
    var bannerName = [String]()
    var bannerMob1 = [String]()
    var bannerMob2 = [String]()
    var catogery = [String]()
    let imagesUrl = "http://142.4.4.149/~kwtads/all_images/"
    var departArray = [Department]()
    



    @IBOutlet var departmentCollectionView: UICollectionView! {
        didSet {
            departmentCollectionView.alwaysBounceVertical = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if Reachability.isConnectedToNetwork() == true {
            self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "ac_back-arrow")
            self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "ac_back-arrow")
            self.navigationController?.navigationBar.tintColor = UIColor(red: 0.96, green: 0.69, blue:  0.25, alpha: 1 )
            self.navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
            let countryNamess = "محافظة \(UserDefaults.standard.string(forKey: "countryId")!)"
            countryNames.text = countryNamess
            
            let bgImage = UIImageView();
            bgImage.image = UIImage(named: "BackGround");
            bgImage.contentMode = .scaleToFill
            self.departmentCollectionView?.backgroundView = bgImage
            departmentCollectionView.delegate = self
            departmentCollectionView.dataSource = self
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage
            BannerLoad()
        } else {
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage
            
            let err = "Internet connection FAILED"
            self.displayAlertMessage(err)
        }
    }
    
    func displayAlertMessage(_ userMessage:String)
    {
        let myAlert = UIAlertController(title: "Alert", message:userMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }
    
    func BannerLoad() {
        let countryId = UserDefaults.standard.string(forKey: "CountryIDS")!
        let urlStrings = "\(baseUrl)v_cat_adv_ads_ios.php?area_id=\(countryId)"
        let sessions = URLSession.shared
        let urls = URL(string: urlStrings)!
        sessions.dataTask(with: urls, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
                if let responseData = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: responseData, options: .allowFragments) as! Dictionary<String, Any>
                        //print("deparments json: \(String(describing: json))")
                        if let data = json["data"] as? NSArray {
                            for i in 0..<data.count
                            {
                                if let dict = data[i] as? NSDictionary {
                                    print("department json: ", String(describing: dict))
                                    let img_adv = dict["img_adv"] as! String
                                    let img_adv2 = dict["img_adv2"] as! String
                                    let adv_name = dict["adv_name"] as! String
                                    let tel = dict["tel"] as! String
                                    let mobile = dict["mobile"] as! String
                                    var catogeryArray = [Category]()
                                    let cat = dict["categories"] as! NSArray
                                    for x in 0..<cat.count{
                                        if let dic = cat[x] as? NSDictionary{
                                            let cat_id = dic["cat_id"] as! String
                                            let cat_name = dic["cat_name"] as! String
                                            print("department name: \(cat_name)")
                                            let color = dic["color"] as! String
                                            let cat_img = dic["cat_img"] as! String
                                            let section_img = dic["section_img"] as! String
                                            
                                            let cat  = Category(cat_id: cat_id, cat_name: cat_name, color: color, cat_img: cat_img, section_img: section_img)
                                            catogeryArray.append(cat)
                                        }
                                        
                                    }
                                    let derpatment = Department(img_adv: img_adv, img_adv2: img_adv2, adv_name: adv_name, tel: tel, mobile: mobile, category: catogeryArray)
                                    
                                    self.departArray.append(derpatment)
                                }
                            }
                            DispatchQueue.main.async {
                                self.departmentCollectionView.reloadData()
                            }
                        }
                    } catch {
                        print ("Could not Serialize")
                    }
                }
        }).resume()    
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "departmentCell", for: indexPath) as! DepartmentCollectionViewCell
        if indexPath.item == departArray.count - 1 {
            load_image(imagesUrl+departArray[indexPath.item].img_adv, imageview: cell.bannerImage)
            cell.bannerBtn.addTarget(self, action: #selector(bannerBtn(_:)), for: .touchUpInside)
            cell.bottomView.isHidden = true
            cell.bottomView.bounds.size.height = 0
            cell.bottomViewH.constant = 0
        } else {
            cell.bottomViewH.constant = 122
            cell.bottomView.bounds.size.height = 122
            cell.bottomView.isHidden = false
            load_image(imagesUrl+departArray[indexPath.item].img_adv, imageview: cell.bannerImage)
            load_image(imagesUrl+departArray[indexPath.item].category[0].section_img, imageview: cell.firstMainImg)
            load_image(imagesUrl+departArray[indexPath.item].category[1].section_img, imageview: cell.secondMainImg)
            load_image(imagesUrl+departArray[indexPath.item].category[2].section_img, imageview: cell.thirdMainImg)
            load_image(imagesUrl+departArray[indexPath.item].category[0].cat_img, imageview: cell.firstMainLogo)
            load_image(imagesUrl+departArray[indexPath.item].category[1].cat_img, imageview: cell.secondMainLogo)
            load_image(imagesUrl+departArray[indexPath.item].category[2].cat_img, imageview: cell.thirdMainLogo)
            cell.firstMainLbk.text = departArray[indexPath.item].category[0].cat_name
            cell.secondMainLbl.text = departArray[indexPath.item].category[1].cat_name
            cell.thirdMainLbl.text = departArray[indexPath.item].category[2].cat_name
            cell.firstMainLbk.textColor = UIColor().HexToColor(hexStrings: departArray[indexPath.item].category[0].color)
            cell.secondMainLbl.textColor = UIColor().HexToColor(hexStrings: departArray[indexPath.item].category[1].color)
            cell.thirdMainLbl.textColor = UIColor().HexToColor(hexStrings: departArray[indexPath.item].category[2].color)
            cell.bannerBtn.addTarget(self, action: #selector(bannerBtn(_:)), for: .touchUpInside)
            cell.firstBtn.addTarget(self, action: #selector(firstBtn(_:)), for: .touchUpInside)
            cell.secondBtn.addTarget(self, action: #selector(secondBtn(_:)), for: .touchUpInside)
            cell.thirdBtn.addTarget(self, action: #selector(thirdBtn(_:)), for: .touchUpInside)
            cell.tag = indexPath.item
        }
        return cell
    }
    
    func bannerBtn(_ sender: UIButton) {
        let myImageViewPage:BannerViewerViewController = self.storyboard?.instantiateViewController(withIdentifier: "BannerViewer") as! BannerViewerViewController
        let cell = ((sender.superview)?.superview)?.superview as! DepartmentCollectionViewCell
        
        let department = self.departArray[cell.tag]
        myImageViewPage.selectedImage = imagesUrl+department.img_adv2
        myImageViewPage.bannerNames = department.adv_name
        myImageViewPage.bannermob1 =  department.mobile
        myImageViewPage.bannermob2 = department.tel
        self.navigationController?.pushViewController(myImageViewPage, animated: true)

    }
    
    func firstBtn(_ sender: UIButton) {
        let myImageViewPage:DepartmentAdsScreenViewController = self.storyboard?.instantiateViewController(withIdentifier: "DepartmentAds") as! DepartmentAdsScreenViewController
        let cell = (((sender.superview)?.superview)?.superview)?.superview as! DepartmentCollectionViewCell
            let department = self.departArray[cell.tag]
            defaults.setValue(department.category[0].cat_name, forKey: "departmentName")
            defaults.setValue(department.category[0].cat_id, forKey: "departmentID")
        self.navigationController?.pushViewController(myImageViewPage, animated: true)
    }
    
    func secondBtn(_ sender: UIButton) {
        let myImageViewPage:DepartmentAdsScreenViewController = self.storyboard?.instantiateViewController(withIdentifier: "DepartmentAds") as! DepartmentAdsScreenViewController
        let cell = (((sender.superview)?.superview)?.superview)?.superview as! DepartmentCollectionViewCell
        let department = self.departArray[cell.tag]
        defaults.setValue(department.category[1].cat_name, forKey: "departmentName")
        defaults.setValue(department.category[1].cat_id, forKey: "departmentID")
        self.navigationController?.pushViewController(myImageViewPage, animated: true)

        
    }
    
    func thirdBtn(_ sender: UIButton){
        let myImageViewPage:DepartmentAdsScreenViewController = self.storyboard?.instantiateViewController(withIdentifier: "DepartmentAds") as! DepartmentAdsScreenViewController
        let cell = (((sender.superview)?.superview)?.superview)?.superview as! DepartmentCollectionViewCell
        let department = self.departArray[cell.tag]
        defaults.setValue(department.category[2].cat_name, forKey: "departmentName")
        defaults.setValue(department.category[2].cat_id, forKey: "departmentID")
        self.navigationController?.pushViewController(myImageViewPage, animated: true)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return departArray.count
    }
    
    func load_image(_ link:String, imageview:UIImageView)
    {
        
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.timeoutInterval = 10
        
        
        let task = session.dataTask(with: url, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
            guard let _:Data = data, let _:URLResponse = response  , error == nil else { return }
            var image = UIImage(data: data!)
            if (image != nil) {
                func set_image() {
                    self.images_cache[link] = image
                    imageview.image = image
                }
                DispatchQueue.main.async(execute: set_image)
            }
        })
        task.resume()
    }
    
    
    
    func refresh()
    {
        self.departmentCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        if indexPath.item == departArray.count - 1{
            let size = self.departmentCollectionView.frame.width
            
            return CGSize(width: size , height: 65)

        } else {
            let size = self.departmentCollectionView.frame.width
            
            return CGSize(width: size , height: 182)
        }
        
        
        
    }

    
}
extension UIColor {
    
    func HexToColor(hexStrings: String, alpha:CGFloat? = 1.0) -> UIColor {
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStrs: hexStrings))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let alpha = alpha!
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func intFromHexString(hexStrs: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStrs)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = NSCharacterSet(charactersIn: "#") as CharacterSet
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
}



