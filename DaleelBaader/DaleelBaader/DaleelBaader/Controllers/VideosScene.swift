
import UIKit

import Localize_Swift

final class VideosScene: BaseController
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    fileprivate var dataSource = Array<Movie>() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tblVideos.reloadData()
            }
        }
    }
    fileprivate var images = Array<InfoImage>()
    
    // MARK: - Outlets
    
    @IBOutlet private weak var tblVideos: UITableView! {
        didSet {
            tblVideos.delegate = self
            tblVideos.dataSource = self
        }
    }
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        tblVideos.register(Constants.Nib.Info, forCellReuseIdentifier: Constants.ReuseIdentifier.Info)
        images = InfoImage.getData()
        if (AppManager.shared.isConnected) {
            loadData()
        } else {
            dialog(with: Constants.Misc.InternetMessage)
        }
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBar()
    }
    
    //MARK: - Actions
    

}


// MARK: - Helper functions 

extension VideosScene
{
    
    fileprivate func updateNavigationBar()-> void {
        navigationItem.title = "videosSerivecestitle".localized()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(goBack(_:)))
    }
    
    fileprivate func loadData()-> void {
        startSpin()
        Movie.retrieveData(viewController: self) { [weak self] (movies) -> void in
            guard let this = self else { return }
            this.dataSource = movies
            UIApplication.showIndicator(by: false)
            this.stopSpin()
        }
    }
    
}


// MARK: - Table view delegate & data source functions 

extension VideosScene: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (section == 0) ?  2 : dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0) {
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.Info) as? InfoCell {
                cell.info = images[indexPath.row]
                return cell
            }
            return UITableViewCell()
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.Video) as? VideoCellScene {
                cell.video = dataSource[indexPath.row]
                return cell
            }
        }
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch(indexPath.section) {
        case 0:
            print("section index is: \(indexPath.section)")
            if let imageViewer = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.ImagePreviwer) as? ImagePreviewer {
                imageViewer.info = images[indexPath.row]
                present(UINavigationController(rootViewController: imageViewer), animated: true, completion: nil)
            }
            
        default:
            // navigate to info about video page
            if let vidInfoScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Video) as? VideoDetailsScene {
                vidInfoScreen.movie = dataSource[indexPath.row]
                navigationController?.pushViewController(vidInfoScreen, animated: true)
            }
        }
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
}



































