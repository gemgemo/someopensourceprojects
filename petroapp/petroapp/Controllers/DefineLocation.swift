
import UIKit
import Gemo
import CoreLocation
import GooglePlaces
import GoogleMaps


final class DefineLocation: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var locationManager = CLLocationManager(), locationCoordinate = CLLocationCoordinate2D(), userModel: UserViewModel?, marker: GMSMarker?
    internal var name, username, email, phone, password, rePassword: string?, isUpdateProfile = false
    internal weak var chosenLocation: ChosenLocation?
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var mapView: GMSMapView!
    @IBOutlet private weak var searchPanel: UIView!
    @IBOutlet private weak var btnSearch: UIButton!
    @IBOutlet private weak var btnContiune: UIButton!
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        setupLocation()
        userModel = UserViewModel()
        configureMapView()
    }
    
    internal override func updateUi() {
        super.updateUi()
        setupNavigationBar(isLogo: false, isMainPage: false, title: Localization.DefineLocation.localized())
        searchPanel.setCorner(radius: 3.0)
        searchPanel.setShadow(with: .zero, radius: 1.5, opacity: 0.4, color: .darkGray)
        btnContiune.setCorner(radius: 3.0)
        btnContiune.setShadow(with: .zero, radius: 0.7, opacity: 0.6, color: .main)
        btnSearch.setTitle(Localization.Search.localized(), for: .normal)
        btnContiune.setTitle(Localization.Continuation.localized(), for: .normal)
        if (Localizer.instance.current == .arabic) {
            btnSearch.contentHorizontalAlignment = .right
        } else {
            btnSearch.contentHorizontalAlignment = .left
        }
        btnContiune.isHidden = isUpdateProfile
        if (isUpdateProfile) {
            let btnSubmit = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_check").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(submitLocationOnCLick(_:)))
            if (Localizer.instance.current == .arabic) {
                navigationItem.leftBarButtonItem = btnSubmit
            } else {
                navigationItem.rightBarButtonItem = btnSubmit
            }
        }
    }
    
    
    
    // MARK: - Actions
    
    
    @IBAction private func contiuneOnCLick(_ sender: UIButton) {
        guard let activationPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.ActivationCode) as? ActivationCode else { return }
        let user = User()
        user.name = name
        user.username = username
        user.email = email
        user.password = password
        user.confirmedPassword = rePassword
        user.phone = phone
        user.latitude = locationCoordinate.latitude.toString
        user.longitude = locationCoordinate.longitude.toString
        address(for: locationCoordinate) { (address) in
            user.address = address
            Threads.main { [weak self] in
                activationPage.user = user
                self?.navigate(to: activationPage)
            }            
        }
    }

    @IBAction private func searchOnCLick(_ sender: UIButton) {
        let locationAutoCompletePage = GMSAutocompleteViewController()
        locationAutoCompletePage.delegate = self
        locationAutoCompletePage.tintColor = .main
        present(locationAutoCompletePage, animated: true)
    }
    
    @objc private func submitLocationOnCLick(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }

}


// MARK: - Helper functions

extension DefineLocation {
    
    fileprivate func setupLocation()-> void {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        guard CLLocationManager.locationServicesEnabled() else {
            displayAlert(with: Localization.OpenLocation.localized())
            return
        }
        locationManager.startUpdatingLocation()
    }
    
    fileprivate func setMarker(in point: CLLocationCoordinate2D)-> void {
        marker?.map = nil
        marker = GMSMarker()
        marker?.position = point
        marker?.icon = #imageLiteral(resourceName: "marker-map2")
        marker?.appearAnimation = .pop
        print(point)
        marker?.map = mapView
        mapView.camera = GMSCameraPosition.camera(withLatitude: point.latitude, longitude: point.longitude, zoom: 16.0)
        chosenLocation?.location(point)
    }
    
    fileprivate func configureMapView()-> void {
        mapView.delegate = self
        mapView.isBuildingsEnabled = true
        mapView.mapType = .terrain
    }
}


// MARK: - Location manager delegate functions

extension DefineLocation: CLLocationManagerDelegate {
    
    internal func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        displayAlert(with: error.localizedDescription)
    }
    
    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let coordinate = locations.last?.coordinate {
            locationCoordinate = coordinate
            setMarker(in: coordinate)
        }
        manager.stopUpdatingLocation()
    }
}


// MARK: - Map view delegate functions 

extension DefineLocation: GMSMapViewDelegate {
    
    internal func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        locationCoordinate = coordinate
        setMarker(in: coordinate)
    }
    
}


// MARK: - Auto complete delegate functions

extension DefineLocation: GMSAutocompleteViewControllerDelegate {
    
    internal func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        locationCoordinate = place.coordinate
        dismiss(animated: true)
        setMarker(in: locationCoordinate)
    }
    
    internal func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("error get place: \(error)")
        displayAlert(with: error.localizedDescription)
        dismiss(animated: true)
    }
    
    internal func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.showIndicator(by: false)
    }
    
    internal func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.showIndicator(by: true)
    }
    
    internal func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true)
    }
    
}


// MARK: - Choose location protocol

protocol ChosenLocation: class {
    func location( _ location: CLLocationCoordinate2D)-> void
    
}


















