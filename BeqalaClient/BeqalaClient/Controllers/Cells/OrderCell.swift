
import UIKit

final class OrderCell: BaseTableCell
{
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var dataSource = Array<OrderProduct>() {
        didSet {
            ThreadQueue.main { [weak self] in
                guard let this = self else { return }
                this.tblProducts.reloadData()
                this.tableViewHeight.constant = this.tblProducts.contentSize.height
            }
        }
    }
    internal var order: Order? { didSet { updateUi() } }
    
    
    // MARK: - Outlets
    
    @IBOutlet private weak var mainPanel: View!
    @IBOutlet private weak var titlePanel: UIView!
    @IBOutlet private weak var lblOrderStateTitle: UILabel!
    @IBOutlet private weak var lblOrderState: UILabel!
    @IBOutlet private weak var tblProducts: UITableView! {
        didSet {
            tblProducts.delegate = self
            tblProducts.dataSource = self
        }
    }
    @IBOutlet private weak var midPanel: UIView!
    @IBOutlet private weak var storeIcon: UIImageView!
    @IBOutlet private weak var lblStoreName: UILabel!
    @IBOutlet private weak var lblStoreTitle: UILabel!
    @IBOutlet private weak var lblTotalTitle: UILabel!
    @IBOutlet private weak var lblTotal: UILabel!
    @IBOutlet private weak var footerPanel: View!
    @IBOutlet private weak var lblOrderDateTitle: UILabel!
    @IBOutlet private weak var lblDateTimeoOrder: UILabel!
    @IBOutlet private weak var lblFloorTitle: UILabel!
    @IBOutlet private weak var lblFloor: UILabel!
    @IBOutlet private weak var lblBuildingTitle: UILabel!
    @IBOutlet private weak var lblBuilding: UILabel!
    @IBOutlet private weak var placeIcon: UIImageView!
    @IBOutlet private weak var lblAddress: UILabel!
    @IBOutlet private weak var lblDeliveryWayTitle: UILabel!
    @IBOutlet private weak var lblDeliveryWay: UILabel!
    @IBOutlet private weak var tableViewHeight: NSLayoutConstraint! {
        didSet {
            tableViewHeight.constant = 0.0
        }
    }
    @IBOutlet private weak var locationIconCell: NSLayoutConstraint!
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        configureTable()
        dataSource = order?.orderProducts ?? Array<OrderProduct>()
        lblOrderStateTitle.text = "\(Localization.OrderState.localized()): "
        lblOrderState.text = order?.status ?? .empty
        lblTotalTitle.text = Localization.Total.localized()
        var price = Number.zero.doubleValue
        ThreadQueue.thread(QOS: .userInitiated) { [weak self] in
            self?.order?.orderProducts.forEach { (item) in
                if let p = NumberFormatter.formate.number(from: item.price ?? "0")?.doubleValue {
                    price += p
                }
            }
            ThreadQueue.main { [weak self] in
                self?.lblTotal.text = "\(price.toString) \(Localization.SR.localized())"
            }
        }
        lblStoreTitle.text = Localization.Store.localized()
        if let imageLink = order?.storeImage {
            storeIcon.loadImage(from: imageLink)
        }
        lblStoreName.text = order?.storeName ?? .empty
        lblDeliveryWayTitle.text = Localization.DeliveryMode.localized()
        lblDeliveryWay.text = order?.sendStatus ?? .empty
        lblAddress.text = order?.address ?? .empty
        if let building = order?.building, !building.isEmpty {
            print("building------->>>>", building)
            lblBuildingTitle.text = "\(Localization.Building.localized()): "
            lblBuilding.text = order?.building ?? .empty
            lblFloorTitle.text = "\(Localization.Floor.localized()): "
            lblFloor.text = order?.floor ?? .empty
            locationIconCell.constant = 50.0
            placeIcon.image = #imageLiteral(resourceName: "marker-store2")
        } else {
            //#Warning: - hide location icon
            print("show number and phone")
            lblBuildingTitle.text = "\(Localization.Recipient.localized()): "
            lblBuilding.text = order?.name ?? .empty
            lblFloorTitle.text = "\(Localization.Mobile.localized()): "
            lblFloor.text = order?.phone ?? .empty
            locationIconCell.constant = 0.0
            placeIcon.image = nil
        }
        contentView.layoutIfNeeded()
        lblOrderDateTitle.text = "\(Localization.OrderDate.localized()): "
        lblDateTimeoOrder.text = "\(order?.date ?? .empty)   \(order?.time ?? .empty)"
        if (Localizer.instance.current == .arabic) {
            // title
            lblOrderStateTitle.rightAnchor.constraint(equalTo: titlePanel.rightAnchor, constant: -8).isActive = true
            lblOrderState.rightAnchor.constraint(equalTo: lblOrderStateTitle.leftAnchor, constant: -8).isActive = true
            lblOrderState.leftAnchor.constraint(equalTo: titlePanel.leftAnchor, constant: 8).isActive = true
            // mid panel
            lblTotalTitle.rightAnchor.constraint(equalTo: midPanel.rightAnchor, constant: -8).isActive = true
            lblTotal.rightAnchor.constraint(equalTo: lblTotalTitle.leftAnchor, constant: -8).isActive = true
            lblTotal.leftAnchor.constraint(equalTo: midPanel.leftAnchor, constant: 8).isActive = true
            lblStoreTitle.rightAnchor.constraint(equalTo: midPanel.rightAnchor, constant: -8).isActive = true
            storeIcon.rightAnchor.constraint(equalTo: midPanel.rightAnchor, constant: -8).isActive = true
            lblStoreName.rightAnchor.constraint(equalTo: storeIcon.leftAnchor, constant: -8).isActive = true
            lblStoreName.leftAnchor.constraint(equalTo: midPanel.leftAnchor, constant: 8).isActive = true
            // footer 
            lblDeliveryWayTitle.rightAnchor.constraint(equalTo: footerPanel.rightAnchor, constant: -8).isActive = true
            lblDeliveryWay.rightAnchor.constraint(equalTo: lblDeliveryWayTitle.leftAnchor, constant: -8).isActive = true
            lblDeliveryWay.leftAnchor.constraint(equalTo: footerPanel.leftAnchor, constant: 8).isActive = true
            placeIcon.rightAnchor.constraint(equalTo: footerPanel.rightAnchor, constant: -8).isActive = true
            lblAddress.rightAnchor.constraint(equalTo: placeIcon.leftAnchor, constant: 8).isActive = true
            lblAddress.leftAnchor.constraint(equalTo: footerPanel.leftAnchor, constant: 8).isActive = true
            lblBuildingTitle.rightAnchor.constraint(equalTo: footerPanel.rightAnchor, constant: -8).isActive = true
            lblBuilding.rightAnchor.constraint(equalTo: lblBuildingTitle.leftAnchor, constant: -8).isActive = true
            lblBuilding.leftAnchor.constraint(equalTo: footerPanel.leftAnchor, constant: 8).isActive = true
            lblFloorTitle.rightAnchor.constraint(equalTo: footerPanel.rightAnchor, constant: -8).isActive = true
            lblFloor.rightAnchor.constraint(equalTo: lblFloorTitle.leftAnchor, constant: -8).isActive = true
            lblFloor.leftAnchor.constraint(equalTo: footerPanel.leftAnchor, constant: 8).isActive = true
            lblOrderDateTitle.rightAnchor.constraint(equalTo: footerPanel.rightAnchor, constant: -8).isActive = true
            lblDateTimeoOrder.rightAnchor.constraint(equalTo: lblOrderDateTitle.leftAnchor, constant: -8).isActive = true
            lblDateTimeoOrder.leftAnchor.constraint(equalTo: footerPanel.leftAnchor, constant: 8).isActive = true
            lblOrderStateTitle.textAlignment = .right
            lblOrderState.textAlignment = .right
            lblTotalTitle.textAlignment = .right
            lblTotal.textAlignment = .right
            lblStoreTitle.textAlignment = .right
            lblStoreName.textAlignment = .right
            lblDeliveryWayTitle.textAlignment = .right
            lblDeliveryWay.textAlignment = .right
            lblAddress.textAlignment = .right
            lblBuildingTitle.textAlignment = .right
            lblBuilding.textAlignment = .right
            lblFloorTitle.textAlignment = .right
            lblFloor.textAlignment = .right
            lblOrderDateTitle.textAlignment = .right
            lblDateTimeoOrder.textAlignment = .right
            
        } else {
            // title
            lblOrderStateTitle.leftAnchor.constraint(equalTo: titlePanel.leftAnchor, constant: 8).isActive = true
            lblOrderState.leftAnchor.constraint(equalTo: lblOrderStateTitle.rightAnchor, constant: 8).isActive = true
            lblOrderState.rightAnchor.constraint(equalTo: titlePanel.rightAnchor, constant: -8).isActive = true
            // mid panel
            lblTotalTitle.leftAnchor.constraint(equalTo: midPanel.leftAnchor, constant: 8).isActive = true
            lblTotal.leftAnchor.constraint(equalTo: lblTotalTitle.rightAnchor, constant: 8).isActive = true
            lblTotal.rightAnchor.constraint(equalTo: midPanel.rightAnchor, constant: -8).isActive = true
            lblStoreTitle.leftAnchor.constraint(equalTo: midPanel.leftAnchor, constant: 8).isActive = true
            storeIcon.leftAnchor.constraint(equalTo: midPanel.leftAnchor, constant: 8).isActive = true
            lblStoreName.leftAnchor.constraint(equalTo: storeIcon.rightAnchor, constant: 8).isActive = true
            lblStoreName.rightAnchor.constraint(equalTo: midPanel.rightAnchor, constant: -8).isActive = true
            // footer
            lblDeliveryWayTitle.leftAnchor.constraint(equalTo: footerPanel.leftAnchor, constant: 8).isActive = true
            lblDeliveryWay.leftAnchor.constraint(equalTo: lblDeliveryWayTitle.rightAnchor, constant: 8).isActive = true
            lblDeliveryWay.rightAnchor.constraint(equalTo: footerPanel.rightAnchor, constant: -8).isActive = true
            placeIcon.leftAnchor.constraint(equalTo: footerPanel.leftAnchor, constant: 8).isActive = true
            lblAddress.leftAnchor.constraint(equalTo: placeIcon.rightAnchor, constant: 8).isActive = true
            lblAddress.rightAnchor.constraint(equalTo: footerPanel.rightAnchor, constant: -8).isActive = true
            lblBuildingTitle.leftAnchor.constraint(equalTo: footerPanel.leftAnchor, constant: 8).isActive = true
            lblBuilding.leftAnchor.constraint(equalTo: lblBuildingTitle.rightAnchor, constant: 8).isActive = true
            lblBuilding.rightAnchor.constraint(equalTo: footerPanel.rightAnchor, constant: -8).isActive = true
            lblFloorTitle.leftAnchor.constraint(equalTo: footerPanel.leftAnchor, constant: 8).isActive = true
            lblFloor.leftAnchor.constraint(equalTo: lblFloorTitle.rightAnchor, constant: 8).isActive = true
            lblFloor.rightAnchor.constraint(equalTo: footerPanel.rightAnchor, constant: -8).isActive = true
            lblOrderDateTitle.leftAnchor.constraint(equalTo: footerPanel.leftAnchor, constant: 8).isActive = true
            lblDateTimeoOrder.leftAnchor.constraint(equalTo: lblOrderDateTitle.rightAnchor, constant: 8).isActive = true
            lblDateTimeoOrder.rightAnchor.constraint(equalTo: footerPanel.rightAnchor, constant: -8).isActive = true
            lblOrderStateTitle.textAlignment = .left
            lblOrderState.textAlignment = .left
            lblTotalTitle.textAlignment = .left
            lblTotal.textAlignment = .left
            lblStoreTitle.textAlignment = .left
            lblStoreName.textAlignment = .left
            lblDeliveryWayTitle.textAlignment = .left
            lblDeliveryWay.textAlignment = .left
            lblAddress.textAlignment = .left
            lblBuildingTitle.textAlignment = .left
            lblBuilding.textAlignment = .left
            lblFloorTitle.textAlignment = .left
            lblFloor.textAlignment = .left
            lblOrderDateTitle.textAlignment = .left
            lblDateTimeoOrder.textAlignment = .left
            
        }
        
    }
    
    
    // MARK: - Functions 
    
    private func configureTable()-> void {
        tblProducts.alwaysBounceVertical = false
        tblProducts.alwaysBounceHorizontal = false
        tblProducts.register(Constants.Nibs.ProductOrder, forCellReuseIdentifier: Constants.ReuseIdentifiers.ProductOrder)
    }
    
    
    // MARK: - Actions    
    
}





// MARK: - Table view delegate & data source functions

extension OrderCell: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifiers.ProductOrder, for: indexPath) as? OrderProductCell else { return UITableViewCell() }
        cell.orderProduct = dataSource[indexPath.row]
        return cell
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

    
}






































