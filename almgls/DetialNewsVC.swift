
import UIKit

class DetialNewsVC: UIViewController ,UIScrollViewDelegate
{
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var loadingLbl: UILabel!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var detialView: UIView!
    @IBOutlet private weak var sliderHeight: NSLayoutConstraint!
    
    var selectedID: String!
    var selectedTitle:String!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pagingControl: UIPageControl!
    @IBOutlet weak var newDetial: UITextView!
    var titles: String!
    var titleEn: String!
    var content:String!
    var contentEn:String!
    var images = [String]()
    var downloadedImgs = [UIImage]()
    var dataFound:String!
    var detialNew: Array<DetialNew> = Array<DetialNew>()
    var imgs: String!
    var url:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        //self.title = selectedTitle
        navigationItem.title = selectedTitle.cleanHtml()
        shadowView.isHidden = true
        loadingView.isHidden = true
        if Settings.lang == "ar"
        {
            titleLbl.textAlignment = .right
            detialView.semanticContentAttribute = .forceRightToLeft
            newDetial.textAlignment = .right
            loadingLbl.text = "جاري التحميل..."
        }
        if (Settings.isConnected) {
            detialNews(selectedID)
        } else {
            dialogUser(with: "لا يوجد انترنت")
        }
        
    }
    
    internal override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        sliderHeight.constant = view.frame.height*0.35
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pagingControl.currentPage = Int(pageNumber)
    }
    
    
    func detialNews(_ ID:String!)
    {
        loadingView.isHidden = false
        shadowView.isHidden = false
        loadingView.layer.cornerRadius = 10
        loadingView.layer.masksToBounds = true
        loadingActivity.startAnimating()
        let request = NSMutableURLRequest(url: URL(string:Settings.DetialNews)!)
        request.httpMethod = "POST"
        let postString = "ID=\(ID!)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        { data, response, error in
            guard error == nil && data != nil else {
                print("error=\(error)")
                return
            }
            
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
            
            if let responseData = data {
                
                do {
                    
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                    //print("ddd\(json)")
                    self.dataFound = json["Success"] as? String
                    self.url = json["URL"] as! String
                    if self.dataFound == "True"
                    {
                        let data = json["Data"] as! NSArray
                        for i in 0..<data.count
                        {
                            let datas = data[i] as! NSDictionary
                            var name1 = ""
                            if Settings.lang == "ar"
                            {
                                name1 = "title"
                            }
                            else
                            {
                                name1 = "title"
                                
                            }
                            
                            let content = (datas["content"] as! String).cleanHtml()
                            let name = datas[name1] as! String
                            if let image = datas["images"] as? NSArray {
                                for x in 0..<image.count
                                {
                                    let img = image[x] as! NSDictionary
                                    self.images.append(img["scalar"] as! String)
                                }
                            }
                            
                            let detialNews = DetialNew(content: content, title: name)
                            self.detialNew.append(detialNews)
                            
                        }
                        
                        
                        
                    }
                    DispatchQueue.main.async {
                        
                        for i in 0..<self.detialNew.count
                        {
                            
                            for i in 0..<self.images.count
                            {
                                self.load_image(self.url + self.images[i])
                            }
                            self.pagingControl.numberOfPages = self.images.count
                            self.titleLbl.text = (self.detialNew[i].title).cleanHtml()
                            //self.webView.loadHTMLString(self.detialNew[i].content, baseURL: nil)
                            self.newDetial.text = self.detialNew[i].content.cleanHtml()
                            
                        }
                        self.loadingView.isHidden = true
                        self.shadowView.isHidden = true
                        self.loadingActivity.stopAnimating()
                    }
                } catch {
                    print ("Could not Serialize")
                }
            }
        }
        task.resume()
    }
    
    func load_image(_ urlString:String)
    {
        let imgURL: URL = URL(string: urlString)!
        let request: URLRequest = URLRequest(url: imgURL)
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            
            if (error == nil && data != nil && UIImage(data: data!) != nil) {
                func display_image() {
                    self.downloadedImgs.append(UIImage(data: data!)!)
                    for i in 0..<self.downloadedImgs.count {
                        //print("img\(self.downloadedImgs.count)")
                        let imageView = UIImageView()
                        imageView.contentMode = .scaleAspectFill
                        imageView.clipsToBounds = true
                        imageView.image = self.downloadedImgs[i]
                        let xPostion = self.scrollView.frame.width * CGFloat(i * 1)
                        imageView.frame = CGRect(x: xPostion, y: 0, width: self.view.frame.width, height: self.scrollView.frame.height)
                        self.scrollView.contentSize.width = self.scrollView.frame.width * CGFloat(self.downloadedImgs.count)
                        self.scrollView.addSubview(imageView)
                    }
                }
                DispatchQueue.main.async(execute: display_image)
            }
        })
        
        task.resume()
    }
    
    
    @IBAction func sharingBtn(_ sender: UIBarButtonItem) {
        if (!detialNew.isEmpty) {
            let tst = "\(self.detialNew[0].title)\n" + "\(self.detialNew[0].content)\n" + "\(self.url!)\(self.images[0])\n"
            let objectsToShare = [tst as Any]
            let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        } else {
            print("empty data")
        }
    }
    
    
    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        self.navigationController!.popViewController(animated: true)
    }
    
    
    
}


