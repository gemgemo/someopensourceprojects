
import Foundation
import UIKit
import MapKit
import SystemConfiguration

final class Gemo: Object
{
    
    // MARK:-  shared instance
    internal static var gem: Gemo {
        struct Objc { internal static let instance = Gemo() }
        return Objc.instance
    }
    
    internal var isConnected: bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    
    // MARK:- Create spinner
    private lazy var indicatorPanal: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Color.darkGray.withAlphaComponent(0.4)
        return view
    }()
    
    private lazy var indicator: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.hidesWhenStopped = true
        return spinner
    }()
    
    private func setupSpinnerViews(in view: UIView?)-> void {        
        guard let superPanal = view else {
            print("null view")
            return
        }
        superPanal.addSubview(indicatorPanal)
        indicatorPanal.addSubview(indicator)
        // main panal: right, left, top, bottom
        indicatorPanal.rightAnchor.constraint(equalTo: superPanal.rightAnchor).isActive = true
        indicatorPanal.bottomAnchor.constraint(equalTo: superPanal.bottomAnchor).isActive = true
        indicatorPanal.leftAnchor.constraint(equalTo: superPanal.leftAnchor).isActive = true
        indicatorPanal.topAnchor.constraint(equalTo: superPanal.topAnchor).isActive = true
        // spinner: center y and x
        indicator.centerXAnchor.constraint(equalTo: indicatorPanal.centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: indicatorPanal.centerYAnchor).isActive = true
    }
    
    internal func stopSpinning()-> void {
        UIApplication.showIndicator(by: false)
        DispatchQueue.main.async { [weak self] in
            self?.indicator.stopAnimating()
            self?.indicator.removeFromSuperview()
            self?.indicatorPanal.removeFromSuperview()
        }
    }
    
    internal func startSpinning(in view: UIView?)-> void {
        DispatchQueue.main.async { [weak self] in
            self?.setupSpinnerViews(in: view)
            self?.indicator.startAnimating()
        }
        UIApplication.showIndicator(by: true)
    }
    
    
    // MARK: - Custom Alert View
    
    private var alertPanal: UIView?
    
    internal func showAlert(withTitle title: string, message: string, in view: UIView)-> void {
        DispatchQueue.main.async { [weak self] in
            guard let this = self else { return }
            this.alertPanal = UIView()
            this.alertPanal?.translatesAutoresizingMaskIntoConstraints = false
            this.alertPanal?.backgroundColor = Color.darkGray.withAlphaComponent(0.6)
            guard let mainPanal = this.alertPanal else { return }
            view.addSubview(mainPanal)
            //panal constraints
            mainPanal.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            mainPanal.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
            mainPanal.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
            mainPanal.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
            
            let alertView = View()
            alertView.translatesAutoresizingMaskIntoConstraints = false
            alertView.backgroundColor = .white
            alertView.borderWidth = 1.0
            alertView.borderColor = .main
            alertView.setCorner(radius: 10.0)
            alertView.clipsToBounds = true
            mainPanal.addSubview(alertView)
            // alert view constraints
            alertView.heightAnchor.constraint(equalToConstant: view.frame.height*0.30).isActive = true
            alertView.widthAnchor.constraint(equalToConstant: view.frame.width*0.80).isActive = true
            alertView.centerXAnchor.constraint(equalTo: mainPanal.centerXAnchor).isActive = true
            alertView.centerYAnchor.constraint(equalTo: mainPanal.centerYAnchor).isActive = true
            
            let lblTitle = UILabel()
            lblTitle.translatesAutoresizingMaskIntoConstraints = false
            lblTitle.textColor = .main
            lblTitle.textAlignment = .center
            lblTitle.text = title
            alertView.addSubview(lblTitle)
            // title label constraints
            lblTitle.topAnchor.constraint(equalTo: alertView.topAnchor, constant: 8).isActive = true
            lblTitle.rightAnchor.constraint(equalTo: alertView.rightAnchor, constant: -8).isActive = true
            lblTitle.leftAnchor.constraint(equalTo: alertView.leftAnchor, constant: 8).isActive = true
            
            let btnDone = Button()
            btnDone.translatesAutoresizingMaskIntoConstraints = false
            btnDone.setTitle((Localizer.instance.current == .arabic) ? "انتهي" : "Done", for: .normal)
            btnDone.setTitleColor(.main, for: .normal)
            btnDone.borderColor = .main
            btnDone.borderWidth = 1.0
            btnDone.clipsToBounds = true
            btnDone.addTarget(self, action: #selector(this.onDoneClicked(_:)), for: .touchUpInside)
            alertView.addSubview(btnDone)
            // button constraints
            btnDone.bottomAnchor.constraint(equalTo: alertView.bottomAnchor, constant: -8).isActive = true
            btnDone.centerXAnchor.constraint(equalTo: alertView.centerXAnchor, constant: 0).isActive = true
            btnDone.widthAnchor.constraint(equalToConstant: 120).isActive = true
            btnDone.heightAnchor.constraint(equalToConstant: 35).isActive = true
            
            let lblMessage = UILabel()
            lblMessage.translatesAutoresizingMaskIntoConstraints = false
            lblMessage.textColor = .main
            lblMessage.textAlignment = .center
            lblMessage.text = message
            lblMessage.numberOfLines = 0
            lblMessage.adjustsFontSizeToFitWidth = true
            lblMessage.font = Font.systemFont(ofSize: 14)
            alertView.addSubview(lblMessage)
            // message constraints
            lblMessage.topAnchor.constraint(equalTo: lblTitle.bottomAnchor, constant: 8).isActive = true
            lblMessage.rightAnchor.constraint(equalTo: alertView.rightAnchor, constant: -8).isActive = true
            lblMessage.leftAnchor.constraint(equalTo: alertView.leftAnchor, constant: 8).isActive = true
            lblMessage.bottomAnchor.constraint(equalTo: btnDone.topAnchor, constant: 8).isActive = true
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.9, options: .allowUserInteraction, animations: {
                view.layoutIfNeeded()
            }, completion: nil)

        }
    }
    
    
    @objc private func onDoneClicked(_ sender: Button) {
        UIView.animate(withDuration: 0.7, delay: 0.0, options: .allowUserInteraction, animations: { [weak self] in
            self?.alertPanal?.alpha = 0.0
        }) { [weak self] (isFinished) in
            self?.alertPanal?.removeFromSuperview()
            self?.alertPanal = nil
        }
    }
    
}


internal extension String {
    
    internal static var empty: string {
        return ""
    }
    
    internal var isLink: Bool {
        return contains("http://") || contains("https://")
    }
    
    internal var isEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPredicate.evaluate(with: self)
    }
    
    // converts
    internal var toData: Data {
        return data(using: .utf8) ?? Data()
    }
    
    internal var toInt: int {
        return NumberFormatter.formate.number(from: self)?.intValue ?? 0
    }
    
    internal var toDouble: double {
        return NumberFormatter.formate.number(from: self)?.doubleValue ?? 0.0
    }
    
}

extension NumberFormatter {
    internal static var formate: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en")
        return formatter
    }
}


// MARK: - Number 
internal extension Number {
    
    internal static var zero: Number {
        return 0
    }
    
}

// MARK: - Errors

extension NSError {
    internal var toString: string {
        return string(describing: self)
    }
}



// MARK: - image caching
private let imageCache = NSCache<NSString,UIImage>()
internal extension UIImageView {
    
    internal func loadImage(from link: string, _ onComplete: ((_ isLoaded: Bool)-> void)? = nil)-> void {
        UIApplication.showIndicator(by: true)
        image = nil
        if (!link.isLink || link.isEmpty) {
            print("empty image link or invalid link")
            onComplete?(false)
            UIApplication.showIndicator(by: false)
            return
        }
        
        if let cachedImage = imageCache.object(forKey: link as NSString) {
            image = cachedImage
            onComplete?(true)
            UIApplication.showIndicator(by: false)
            return
        }
        // download and cache image
        // download
        if let url = URL(string: link as string) {
            URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
                if (error != nil) {
                    onComplete?(false)
                    UIApplication.showIndicator(by: false)
                    return
                }
                if let imageData = data, let imageToCache = UIImage(data: imageData) {
                    DispatchQueue.main.async { [weak self] in
                        // cache image
                        imageCache.setObject(imageToCache, forKey: link as NSString)
                        self?.image = imageToCache
                        onComplete?(true)
                        UIApplication.showIndicator(by: false)
                    }
                } else {
                    onComplete?(false)
                    UIApplication.showIndicator(by: false)
                }
                }.resume()
        }
    }
    
    internal func rounded(withColor color: Color = Color.clear, borderWidth width: cgFloat = 0.0, cornerRadius radius: cgFloat)-> void {
        clipsToBounds = true
        layer.cornerRadius = radius//bounds.height/2
        layer.borderColor = color.cgColor 
        layer.borderWidth = width
    }
    
}



internal extension UIApplication {
    
    internal class func showIndicator(by isOn: bool)-> void {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = isOn
        }
    }
    
    internal class func playActions()-> void {
        if (UIApplication.shared.isIgnoringInteractionEvents) {
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
    
    internal class func pauseActions()-> void {
        if (!UIApplication.shared.isIgnoringInteractionEvents) {
            UIApplication.shared.beginIgnoringInteractionEvents()
        }
    }
    
    /*internal class func open(this link: string)-> void {
        guard let url = URL(string: link), UIApplication.shared.canOpenURL(url) else {
            print("can't open link")
            return
        }        
        UIApplication.shared.openURL(url)
    }*/
    
}

// uicolor

internal extension Color {
    
    internal class func rgb(red: Int, green: Int, blue: Int, alpha: Float)-> UIColor {
        return UIColor(colorLiteralRed: Float(red)/255, green: Float(green)/255, blue: Float(blue)/255, alpha: alpha)
    }
    
    internal convenience init?(hex hash: string) {
        let r, g, b, a: CGFloat
        if (hash.hasPrefix("#")) {
            let start = hash.index(hash.startIndex, offsetBy: 1)
            let hexColor = hash.substring(from: start)
            let scanner = Scanner(string: hexColor)
            var hexNumber: UInt64 = 0
            if (scanner.scanHexInt64(&hexNumber)) {
                r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                a = CGFloat(hexNumber & 0x000000ff) / 255
                
                self.init(red: r, green: g, blue: b, alpha: a)
                return
            }
        }
        return nil
    }
    
    
    
}


// MARK: - UIView

internal extension UIView {
    
    internal func setShadow(with offset: CGSize, radius: CGFloat, opacity: CGFloat, color: Color, cornerRadius: CGFloat = 0.0)-> Void {
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = Float(opacity)
        layer.shadowRadius = radius
        if (cornerRadius > 0.0) {
            layer.shadowPath = UIBezierPath(roundedRect: frame, cornerRadius: cornerRadius).cgPath
        }
    }
    
    internal func setBorder(with width: cgFloat, and color: Color)-> void {
        layer.borderColor = color.cgColor
        layer.borderWidth = width
    }
    
    internal func setCorner(radius: cgFloat)-> void {
        layer.cornerRadius = radius
    }
    
}

internal typealias string = String
internal typealias void = ()
internal typealias any = Any
internal typealias double = Double
internal typealias bool = Bool
internal typealias float = Float
internal typealias int = Int
internal typealias Number = NSNumber
internal typealias object = AnyObject
internal typealias cgFloat = CGFloat
internal typealias Color = UIColor
internal typealias Font = UIFont
internal typealias Size = CGSize
internal typealias Rect = CGRect
internal typealias Insets = UIEdgeInsets
internal typealias uint = UInt
internal typealias Object = NSObject
internal typealias null = NSNull
internal typealias Point = CGPoint



@IBDesignable
internal class TextView: UITextView, UITextViewDelegate
{
    @IBInspectable
    internal var placeholder: String = "placholder" {
        didSet {
            setup()
        }
    }
    
    @IBInspectable
    internal var placeholderColor: UIColor = Color.lightGray {
        didSet {
            setup()
        }
    }
    
    @IBInspectable
    internal var color: UIColor = Color.black {
        didSet {
            setup()
        }
    }
    
    
    internal override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        setup()
    }
    
    required internal init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
    
    private func setup()-> void {
        delegate = self
        text = placeholder
        textColor = placeholderColor
    }
    
    internal func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text == placeholder && textView.textColor == placeholderColor) {
            textView.text = string.empty
            textView.textColor = color
        }
    }
    
    internal func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text.isEmpty) {
            textView.text = placeholder
            textView.textColor = placeholderColor
        }
    }
    
    
    
    
}


// MARK: - CGFloat 
internal extension cgFloat {
    internal var toFloat: float {
        return float(self)
    }
}



// MARK: - Text Fields

@IBDesignable
internal class TextField: UITextField
{
    
    @IBInspectable
    internal var bottomBorderHeight: CGFloat = 1.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    internal var bottomBorderColor: UIColor = UIColor.gray {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    internal var placeholderColor: UIColor = UIColor.black {
        didSet {
            setNeedsDisplay()
        }
    }
    
    
    
    
    internal override func draw(_ rect: CGRect) {
        attributedPlaceholder = NSAttributedString(string: placeholder ?? .empty, attributes: [NSForegroundColorAttributeName: placeholderColor])
        
        let bottomBorder = UIView()
        bottomBorder.translatesAutoresizingMaskIntoConstraints = false
        bottomBorder.backgroundColor = bottomBorderColor// UIColor(white: 0.80, alpha: 0.9)
        addSubview(bottomBorder)
        
        bottomBorder.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        bottomBorder.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: bottomBorderHeight).isActive = true
        bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0.0).isActive = true
    }
    
    
}


// MARK: - Buttons

@IBDesignable
internal class Button: UIButton
{
    @IBInspectable
    internal var cornerRadius: CGFloat = 0.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    internal var borderWidth: CGFloat = 0.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    internal var borderColor: UIColor = UIColor.gray {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    internal var shadowColor: UIColor = UIColor.black {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    internal var shadowOpacity: CGFloat = 0.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    internal var shadowRadius: CGFloat = 0.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    internal var shadowSize: CGSize = CGSize.zero {
        didSet {
            setNeedsDisplay()
        }
    }
    
    
    internal override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.cornerRadius = cornerRadius
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        layer.masksToBounds = true
    }
    
    
}


// MARK: - Size

extension Size
{
    
    init(right: cgFloat, top: cgFloat) {
        width = right
        height = top
    }
    
}


// MARK: - Map view

extension MKMapView
{
    
    internal func setupCamera()-> void {
        camera.altitude = 1400
        camera.pitch = 50
        camera.heading = 180
    }
    
    internal func setAnnotation(in point: CLLocationCoordinate2D, with title: string, and subTitle: string)-> void {
        let annotation = MKPointAnnotation()
        annotation.coordinate = point
        annotation.title = title
        annotation.subtitle = subTitle
        addAnnotation(annotation)
    }
    
    internal func setRegion(in point: CLLocationCoordinate2D)-> void {        
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let region = MKCoordinateRegion(center: point, span: span)
        setRegion(region, animated: true)
        //setupCamera()
    }
    
}


// MARK:- Fonts

extension Font
{
    
    internal class func getFonts()-> void {
        Font.familyNames.forEach { (family) in
            print("--------------------------\(family)--------------------------")
            Font.fontNames(forFamilyName: family).forEach { (font) in
                print("font name is: ", font)
            }
        }
    }
    
}


// MARK: - View controllers 

extension UIViewController
{
    
    // MARK: - Computed properties
    
    internal var target: UIViewController {
        guard let navcon = self as? UINavigationController else {
            return self
        }
        return navcon.visibleViewController ?? self
    }
    
}


// MARK: - Int extension

internal extension int
{
    
    internal var toString: string {
        return string(describing: self)
    }
    
    internal var toDouble: double {
        return double(self)
    }
    
}

// MARK: - Double 

extension double
{
    
    internal var toString: string {
        return string(describing: self)
    }
    
}


// MARK: - Threads

internal final class ThreadQueue
{
    internal class func main(_ callback: @escaping ()->())-> void {
        DispatchQueue.main.async(execute: callback)
    }
    
    internal class func thread(QOS: DispatchQoS.QoSClass, _ callback: @escaping()->())-> void {
        DispatchQueue.global(qos: QOS).async(execute: callback)
    }
    
    internal class func delay(after: double, _ callBack: @escaping()->())-> void {
        DispatchQueue.main.asyncAfter(deadline: .now()+after, execute: callBack)
    }
    
}





























