
import UIKit
import Gemo

final class Splash: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    
    // MARK: - Outlets
    
    @IBOutlet private weak var btnArabicLanguage: UIButton!
    @IBOutlet private weak var btnEnglishLanguage: UIButton!
    
    
    
    // MARK: - Overridden functions
    internal override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(onLanguageChanaged), name: .LanguageChanged, object: nil)
    }
    internal override func updateUi() {
        [btnArabicLanguage, btnEnglishLanguage].forEach {
            $0?.setCorner(radius: 3.0)
            $0?.setShadow(with: .zero, radius: 0.7, opacity: 0.8, color: .white)
        }
    }
    
    
    // MARK: - Actions
    
    @IBAction private func setArabicLanguageOnClick(_ sender: UIButton) {
        Localizer.instance.set(language: .arabic)       
    }
    
    
    @IBAction private func setEnglishLanguageOnCLick(_ sender: UIButton) {
        Localizer.instance.set(language: .english)
    }
    
    // MARK: - Functions
    
    @objc private func onLanguageChanaged()-> void {
        if (UserDefaults.standard.bool(forKey: Misc.UserLoggedInKey)) {
            AppManager.configureSideMenu()
        } else {
            guard let userTypePage = storyboard?.instantiateViewController(withIdentifier: Storyboards.RegisterNavigatior) as? UINavigationController else { return }
            present(userTypePage, animated: false)
        }
        UIView.appearance().semanticContentAttribute = (Localizer.instance.current == .arabic) ? .forceLeftToRight : .forceRightToLeft
        UILabel.appearance().textAlignment = (Localizer.instance.current == .arabic) ? .right : .left
        UITextField.appearance().textAlignment = (Localizer.instance.current == .arabic) ? .right : .left
        
    }

}

























