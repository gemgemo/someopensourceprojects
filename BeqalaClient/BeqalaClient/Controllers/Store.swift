
import UIKit
import CarbonKit

final class Store: BaseController
{
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var store: Shop?
    fileprivate weak var pager: CarbonTabSwipeNavigation?
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var titleView: UIView!
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var btnBasket: UIButton!
    @IBOutlet fileprivate weak var btnSearch: UIButton!
    @IBOutlet fileprivate weak var btnBack: UIButton!
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        setupPager()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
    }
    
    
    // MARK: - Actions

    @IBAction private func basketOnCLick(_ sender: UIButton) {
        print("goto basket")
        navigateToBasket()
    }
    
    
    @IBAction private func searchOnCLick(_ sender: UIButton) {
        navigateToSearch()
    }
    
    
    @IBAction private func backOnCLick(_ sender: UIButton) {
        back()
    }
    
    
}



// MARK: - Helper Functions

extension Store
{
    
    fileprivate func updateUi()-> void {
        view.bringSubview(toFront: titleView)
        lblTitle.text = store?.shopName ?? .empty
        tabBar.isHidden = true
        btnBack.transform = CGAffineTransform.identity
        if (Localizer.instance.current == .arabic) {
            btnBack.leftAnchor.constraint(equalTo: titleView.leftAnchor, constant: 8).isActive = true
            btnBasket.rightAnchor.constraint(equalTo: titleView.rightAnchor, constant: -8).isActive = true
            btnSearch.rightAnchor.constraint(equalTo: btnBasket.leftAnchor, constant: -20).isActive = true
        } else {
            btnBack.rightAnchor.constraint(equalTo: titleView.rightAnchor, constant: -8).isActive = true
            btnBack.transform = btnBack.transform.rotated(by: cgFloat.pi)
            btnBasket.leftAnchor.constraint(equalTo: titleView.leftAnchor, constant: 8).isActive = true
            btnSearch.leftAnchor.constraint(equalTo: btnBasket.rightAnchor, constant: 20).isActive = true
        }
    }
    
    fileprivate func setupPager()-> void {
        var items = [Localization.StoreProducts.localized(), Localization.AboutStore.localized()]
        if (Localizer.instance.current == .english) {
            items = items.reversed()
        }
        pager = CarbonTabSwipeNavigation(items: items, delegate: self)
        pager?.insert(intoRootViewController: self)
        pager?.toolbar.transform = CGAffineTransform(translationX: 0, y: 35)
        pager?.toolbar.isTranslucent = false
        pager?.toolbar.setShadow(with: Size(right: 0, top: 0.8), radius: 0.7, opacity: 0.4, color: .black)
        pager?.setIndicatorColor(.main)
        pager?.setTabBarHeight(30.0)
        let tabWidth = UIScreen.main.bounds.width/2
        pager?.setTabExtraWidth(tabWidth)
        pager?.carbonSegmentedControl?.setWidth(tabWidth, forSegmentAt: 0)
        pager?.carbonSegmentedControl?.setWidth(tabWidth, forSegmentAt: 1)
        pager?.setCurrentTabIndex((Localizer.instance.current == .arabic) ? 1 : 0, withAnimation: true)
        pager?.setNormalColor(.black)
        pager?.setSelectedColor(.main)
    }
}



// MARK: - Pager delegate functions

extension Store: CarbonTabSwipeNavigationDelegate
{
    
    internal func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        switch(index){
        case 0 where Localizer.instance.current == .arabic :
            if let productsPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.StoreProducts) as? StoreProducts {
                productsPage.store = store
                return productsPage
            }
            
        case 0 where Localizer.instance.current == .english:
            if let storeInfoPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.StoreInfo) as? StoreInfo {
                storeInfoPage.store = store
                return storeInfoPage
            }
            
        case 1 where Localizer.instance.current == .arabic:
            if let storeInfoPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.StoreInfo) as? StoreInfo {
                storeInfoPage.store = store
                return storeInfoPage
            }
            
        case 1 where Localizer.instance.current == .english:
            if let productsPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.StoreProducts) as? StoreProducts {
                productsPage.store = store
                return productsPage
            }
            
        default:
            break
        }
        return BaseController()
    }
    
    
}



















