
import UIKit
import Gemo
import CoreLocation
import GoogleMaps
import Alamofire

class EditProfile: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var selectedImage = UIImage(), selectedLocation = CLLocationCoordinate2D()
    
    // MARK: - Outlets
    
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var txfUsername: TextField! { didSet { txfUsername.delegate = self} }
    @IBOutlet weak var txfEmail: TextField! { didSet { txfEmail.delegate = self} }
    @IBOutlet weak var txfPhoneNo: TextField! { didSet { txfPhoneNo.delegate = self} }
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblAddress: UILabel! { didSet { lblAddress.text = .empty } }
    @IBOutlet weak var locationIcon: UIImageView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnChangeLocation: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnOpenimagePicker: UIButton!
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.keyboardDismissMode = .interactive
        if let latitude = UserDefaults.standard.string(forKey: Misc.Latitude)?.toDouble, let longitude = UserDefaults.standard.string(forKey: Misc.Longitude)?.toDouble {
            let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            GMSGeocoder().reverseGeocodeCoordinate(coordinate) { [weak self] (response, error) in
                if (error != nil) {
                    print("get address error: \(string(describing: error))")
                    return
                }
                Threads.main { [weak self] in
                    self?.lblAddress.text = response?.firstResult()?.lines?.joined(separator: ",") ?? .empty
                }
            }
        }
        txfUsername.text = UserDefaults.standard.string(forKey: Misc.Username) ?? .empty
        txfEmail.text = UserDefaults.standard.string(forKey: Misc.Email) ?? .empty
        txfPhoneNo.text = UserDefaults.standard.string(forKey: Misc.Phone) ?? .empty
        txfUsername.textFieldDidBeginEditing(txfUsername)
        txfEmail.textFieldDidBeginEditing(txfEmail)
        txfPhoneNo.textFieldDidBeginEditing(txfPhoneNo)
        if let image = UserDefaults.standard.string(forKey: Misc.Photo) {
            userPhoto.loadImage(from: image) { [weak self] (isFinished) in
                if (isFinished) {
                    self?.btnOpenimagePicker.setImage(nil, for: .normal)
                }
            }
        }
    }
    
    internal override func updateUi() {
        super.updateUi()
        setupNavigationBar(isLogo: false, isMainPage: false, title: Localization.EditMyData.localized())
        userPhoto.rounded(cornerRadius: 40)
        btnSave.setTitle(Localization.Save.localized(), for: .normal)
        lblLocation.text = Localization.Location.localized()
        txfUsername.hint = Localization.UserName.localized()
        txfEmail.hint = Localization.Email.localized()
        txfPhoneNo.hint = Localization.PhoneNumber.localized()
        btnSave.setCorner(radius: 3)
        
    }
    
    internal override func becomeFirstResponder() -> Bool {
        return true
    }
    
    // MARK: - Actions
    
    @IBAction func changeAddressOnClick(_ sender: UIButton) {
        guard let defineLocationPage = UIStoryboard(name: Storyboards.Registration, bundle: nil).instantiateViewController(withIdentifier: Storyboards.DefineLocation) as? DefineLocation else {
            return
        }
        defineLocationPage.isUpdateProfile = true
        defineLocationPage.chosenLocation = self
        navigate(to: defineLocationPage)
    }
    
    @IBAction func saveOnClick(_ sender: UIButton) {
        guard Gemo.gem.isConnected else {
            displayAlert(with: Localization.InternetConnection.localized())
            return
        }
        guard !name.isEmpty else {
            displayAlert(with: Localization.UserNameReqiured.localized())
            return
        }
        guard email.isEmail else {
            displayAlert(with: Localization.InvaildEmail.localized())
            return
        }
        guard !phone.isEmpty else {
            displayAlert(with: Localization.PhoneNumberReqiured.localized())
            return
        }
        guard !(lblAddress.text ?? .empty).isEmpty else {
            displayAlert(with: Localization.AddressReqiured.localized())
            return
        }
        Gemo.gem.startSpinning()
        /*var request = URLRequest(url: URL(string: Misc.getUrl(for: "edit_user"))!)
        request.httpMethod = "POST"
        let boundary = "Boundary-\(UUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.httpBody = createBody(parameters: paramters,
                                boundary: boundary,
                                data: photo,
                                mimeType: "image/png",
                                filename: "\(Date().timeIntervalSinceNow).jpg")
        URLSession.shared.dataTask(with: request) { [weak self] (result, response, error) in
            if (error != nil) {
                print("update profile error: \(string(describing: error))")
                Gemo.gem.stopSpinning()
                self?.displayAlert(with: error?.localizedDescription)
                return
            }
            Gemo.gem.stopSpinning()
            guard let jsonData = result else {
                print("no data")
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? Dictionary<string, any>
                print("update profile", json)
            } catch {
                self?.displayAlert(with: error.localizedDescription)
                print("update profile cated error: \(error)")
            }
        }
        .resume()*/
        Alamofire.upload(multipartFormData: { [weak self] (fromData) in
            guard let this = self else { return }
            fromData.append(Misc.MainValue.toData, withName: Misc.MainKey)
            fromData.append((UserDefaults.standard.string(forKey: Misc.Name) ?? .empty).toData, withName: "name")
            fromData.append(this.name.toData, withName: "username")
            fromData.append(this.email.toData, withName: "email")
            fromData.append(this.phone.toData, withName: "phone")
            fromData.append((UserDefaults.standard.string(forKey: Misc.UserId) ?? .empty).toData, withName: "user_id")
            fromData.append(string.empty.toData, withName: "password")
            fromData.append(Localizer.instance.current.rawValue.toData, withName: Misc.LanguageKey)
            fromData.append(this.selectedLocation.latitude.toString.toData, withName: "lati")
            fromData.append(this.selectedLocation.longitude.toString.toData, withName: "longi")
            fromData.append((this.lblAddress.text ?? .empty).toData, withName: "address")
            fromData.append(this.photo, withName: "user_photo", fileName: "\(Date.timeIntervalSinceReferenceDate).png", mimeType: "image/png")
        }, to: Misc.getUrl(for: "edit_user")) { [weak self] (encodingResult) in
            switch (encodingResult) {
            case .failure(let error):
                Gemo.gem.stopSpinning()
                self?.displayAlert(with: error.localizedDescription)
                print("update profile error: \(error)")
                
            case .success(request: let upload, streamingFromDisk: _ , streamFileURL: _):
                /*upload.responseString(completionHandler: { (response) in
                    print("string value", response.result.value)
                })*/
                upload.responseJSON { [weak self] (response) in
                    let error = response.error
                    if (error != nil) {
                        Gemo.gem.stopSpinning()
                        print("response jjson error: \(string(describing: error))")
                        self?.displayAlert(with: error?.localizedDescription)
                        return
                    }
                    Gemo.gem.stopSpinning()
                    //print("update profile: ", response.result.value)
                    guard let json = response.result.value as? Dictionary<string, any>, let success = json["result"] as? string else {
                        self?.displayAlert(with: Localization.NoData.localized())
                        return
                    }
                    if (success == "true") {
                        Threads.main { [weak self] in
                            let user = User(JSON: json)
                            let userViewModel = UserViewModel()
                            userViewModel.save(this: user)
                            self?.navigationController?.popViewController(animated: true)
                        }
                    } else {
                        self?.displayAlert(with: json["data"] as? String)
                    }
                }
                
            }
        }
    }
    
    @IBAction func changeUserPhotoOnCLick(_ sender: UIButton) {
        openImagePicker(delegate: self)
    }

}


// MARK: - Image picker delegate functions

extension EditProfile: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            selectedImage = editedImage
        } else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            selectedImage = image
        }
        userPhoto.image = selectedImage
        btnOpenimagePicker.setImage(UIImage(), for: .normal)
        picker.dismiss(animated: true)
    }
    
    internal func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
}


// MARK: - Text Field delegate functions

extension EditProfile: UITextFieldDelegate {
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}



// MARK: - selected location protocol functions

extension EditProfile: ChosenLocation {
    internal func location(_ location: CLLocationCoordinate2D) {
        //print("selected location",location)
        selectedLocation = location
        address(for: location) { [weak self] (address) in
            self?.lblAddress.text = address
        }
    }
}


// MARK: - Computed properties

fileprivate extension EditProfile {
    
    var name: string {
        return txfUsername.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    var email: string {
        return txfEmail.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    var phone: string {
        return txfPhoneNo.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    var photo: Data {
        return UIImageJPEGRepresentation(userPhoto.image ?? UIImage(), ImageQuality.medium.rawValue) ?? Data()
    }
    
}


// MARK: - Helper functions

fileprivate extension EditProfile {
    
    func createBody(parameters: [String: String],
                    boundary: String,
                    data: Data,
                    mimeType: String,
                    filename: String) -> Data {
        let body = NSMutableData()
        
        let boundaryPrefix = "--\(boundary)\r\n"
        
        for (key, value) in parameters {
            body.appendString(boundaryPrefix)
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }
        
        body.appendString(boundaryPrefix)
        body.appendString("Content-Disposition: form-data; name=\"user_photo\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimeType)\r\n\r\n")
        body.append(data)
        body.appendString("\r\n")
        body.appendString("--".appending(boundary.appending("--")))
        
        return body as Data
    }
    
}


extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}













