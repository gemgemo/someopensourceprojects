
import Foundation
import UIKit
import Gemo
import KYDrawerController


// MARK: - Storyboard
struct Storyboards {
    internal static let Main = "Main",
    Registration = "Registration",
    Splash = "splash.view.id.pe",
    RegisterNavigatior = "register.navigation.view.id.pe",
    UserType = "com.user.type.view.id.pe",
    SignIn = "com.sign.in.view.id.pe",
    SignUp = "com.sign.up.view.id.pe",
    Register = "com.register.view.id.pe",
    DefineLocation = "com.define.location.view.id.pe",
    ActivationCode = "com.activation.view.id.pe",
    Home = "com.home.view.id.pe",
    SideMenu = "com.side.menu.view.id.pe",
    Cars = "com.cars.view.pe",
    StationsLocations = "com.petro.locations.view.id.pe",
    MyBills = "com.bills.view.id.pe",
    AddCar = "com.add.car.view.id.pe",
    CarMainData = "com.car.main.data.view.id.pe",
    BoardNumber = "com.board.number.view.id.pe",
    CarUser = "com.car.user.view.id.pe",
    CarDetails = "com.car.details.view.id.pe",
    Reports = "com.reports.view.id.pe",
    ReportsPerCar = "com.reports.per.car.view.id.pe",
    BillsDetails = "com.bills.details.view.id.pe",
    Profile = "com.my.account.view.id.pe",
    MyPoints = "com.my.points.view.id.pe",
    ContactUs = "com.contact.us.view.id.pe",
    SendComplaint = "com.send.complaint.view.id.pe",
    AboutApp = "com.about.app.view.id.pe",
    EditProfile = "com.edit.profile.view.id.pe",
    EditPassword = "com.edit.password.view.id.pe",
    Popup = "com.popup.view.id.pe",
    CarUserBillDetails = "com.user.car.bill.details.view.id.pe"
}

// MARK: - Nibs
struct Nibs {
    internal static let Car = UINib(nibName: "CarCell", bundle: nil),
    MyBills = UINib(nibName: "MyBillsCell", bundle: nil),
    UserCarBill = UINib(nibName: "CarUserBillCell", bundle: nil)
}

// MARK: - Reuse Identifier
struct ReuseIdentifiers {
    internal static let SideMenu = "com.side.menu.cell.view.id.pe",
    Car = "com.car.cell.view.id.pe",
    MyBills = "com.my.bills.cell.view.id.pe",
    Report = "com.report.cell.view.id.pe",
    ReportPerCar = "com.report.per.car.cell.view.id.pe",
    Popup = "com.popup.view.cell.id.pe",
    UserCarBill = "com.car.user.bill.cell.id.pe"
}

// MARK: - Misc
struct Misc {
    internal static let UserLoggedInKey = "com.user.logged.in.key.id.pe",
    UserTypeKey = "com.user.type.key.id.pe.io",
    MapsKey = "AIzaSyDvfZA-75w1moeLkxArrwkydn_SZrIhzZY",
    ShadowImageName = "1pxWidthLineImage",
    MainKey = "key",
    MainValue = "Petro_App_atiaf",
    LanguageKey = "lang"    
    
    internal static func getUrl(for method: string)-> string {
        return "http://192.232.214.91/~petroapp/JSON/\(method).php"
    }
}


// MARK: - Colors

extension Color {
    internal static var main: Color {
        return Color.rgb(red: 0, green: 118, blue: 197, alpha: 1)
    }
}


// MARK: - Fonts

extension Font {
    internal static func notoKufi(of size: cgFloat = Font.systemFontSize)-> Font {
        return Font(name: "NotoKufiArabic", size: size) ?? Font.systemFont(ofSize: size)
    }
}

internal enum Usertype: int {
    case main = 0, sub = 1
}


class AppManager {
    
    internal class func configureSideMenu()-> void {
        let userType = Usertype(rawValue: UserDefaults.standard.integer(forKey: Misc.UserTypeKey)) ?? .sub
        let mainStoryboard = UIStoryboard(name: Storyboards.Main, bundle: nil)
        let mainPage = mainStoryboard.instantiateInitialViewController()
        if (userType == .main) {
            let sideMenuPage = mainStoryboard.instantiateViewController(withIdentifier: Storyboards.SideMenu) as? SideMenu
            let drawer = KYDrawerController(drawerDirection: (Localizer.instance.current == .arabic) ? .right : .left, drawerWidth: UIScreen.main.bounds.width * 0.85)
            drawer.mainViewController = mainPage
            drawer.drawerViewController = sideMenuPage
            UIApplication.shared.keyWindow?.rootViewController = drawer
        } else {
            UIApplication.shared.keyWindow?.rootViewController = mainPage
        }
    }
    
}


















