
import Foundation
import Gemo

class UserCArBillCell: BaseCollectionCell {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var bill: Bill? { didSet { updateUi() } }
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var lblProcessNo: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblLitersCount: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblPaymentWay: UILabel!
    @IBOutlet weak var lblPaymentStatus: UILabel!
    
    
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        lblProcessNo.text = "\(Localization.ProccessNo.localized())   \("??")"
        lblOrderDate.text = "\(Localization.OrderDate.localized())   \(bill?.timestamp ?? .empty)"
        lblLitersCount.text = "\(Localization.LitresNo.localized())   \(bill?.liters ?? .empty)"
        lblPrice.text = "\(Localization.Price.localized())   \(bill?.price ?? .empty)"
        lblPaymentWay.text = "\(Localization.PaymentWay.localized()) ??"
        lblPaymentStatus.text = "??"
    }
    
    
    // MARK: - Actions

    
}
