//
//  Member.swift
//  almgls
//
//  Created by User on 12/28/16.
//  Copyright © 2016 Mostafa. All rights reserved.
//

import UIKit

class Member: UICollectionViewCell {
    
    @IBOutlet weak var memberImg: UIImageView!
    @IBOutlet weak var memberName: UILabel!
    @IBOutlet weak var memberPostion: UILabel! {
        didSet {
            memberPostion.numberOfLines = 0
            memberPostion.sizeToFit()
        }
    }
}
