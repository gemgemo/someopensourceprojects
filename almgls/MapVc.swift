
import UIKit
import CoreLocation
import GoogleMaps
import Gemo

final class MapVc: UIViewController, CLLocationManagerDelegate, UIGestureRecognizerDelegate
{
    
    @IBOutlet private weak var mapView: GMSMapView! {
        didSet {
            mapView.mapType = .terrain
            mapView.delegate = self
        }
    }
    
    let locationManager = CLLocationManager()
    //var lati: Double!
    //var longi: Double!
    fileprivate var annotation: GMSMarker?
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    /*internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let latitude = defaults.double(forKey: "latitude")
        let longitude = defaults.double(forKey: "longitude")
        if (latitude != 0 && longitude != 0) {
            let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            annotation = GMSMarker()
            annotation?.position = coordinate
            annotation?.map = mapView
            mapView.camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: 16.0)
        }
    }*/
    
    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let latitude = defaults.double(forKey: "latitude")
        let longitude = defaults.double(forKey: "longitude")
        if (latitude != 0 && longitude != 0) {
            let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            self.locationManager.stopUpdatingLocation()
            annotation?.map = nil
            mapView.camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: 16.0)
            annotation = GMSMarker(position: coordinate)
            annotation?.map = mapView
        } else {
            if let location = locations.last {
                let coordinate = location.coordinate
                defaults.set(coordinate.latitude, forKey: "latitude")
                defaults.set(coordinate.longitude, forKey: "longitude")
                self.locationManager.stopUpdatingLocation()
                annotation?.map = nil
                mapView.camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: 16.0)
                annotation = GMSMarker(position: coordinate)
                annotation?.map = mapView
            } else {
                self.locationManager.stopUpdatingLocation()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Errors: " + error.localizedDescription)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @IBAction func backPressed(_ sender: UIButton) {
        self.navigationController!.popViewController(animated: true)
    }
    
    
}


extension MapVc: GMSMapViewDelegate
{
    
    internal func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        //print("long tap coordinate: \(coordinate)")
        annotation?.map = nil
        annotation = GMSMarker(position: coordinate)
        annotation?.map = mapView
        self.defaults.setValue(coordinate.latitude, forKey: "latitude")
        self.defaults.setValue(coordinate.longitude, forKey: "longitude")
        mapView.camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: 16.0)
    }
    
}

















