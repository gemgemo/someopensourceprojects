
import Foundation
import UIKit

internal final class ShopCell: BaseTableCell
{
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var shop: Shop? {
        didSet {
            updateUi()
        }
    }
    
    
    // MARK: - Outlets
    
    @IBOutlet private weak var mainPanal: View!
    @IBOutlet private weak var cover: UIImageView!
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var lblDetails: UILabel!
    @IBOutlet private weak var labels: UIView!
    
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        if let imageLink = shop?.photo {
            cover.loadImage(from: imageLink)
        }
        lblTitle.text = shop?.shopName ?? .empty
        lblDetails.text = shop?.info ?? .empty
        if (Localizer.instance.current == .arabic) {
            cover.rightAnchor.constraint(equalTo: mainPanal.rightAnchor, constant: -8.0).isActive = true
            labels.rightAnchor.constraint(equalTo: cover.leftAnchor, constant: -8.0).isActive = true
            labels.leftAnchor.constraint(equalTo: mainPanal.leftAnchor, constant: 8.0).isActive = true
            lblTitle.textAlignment = .right
            lblDetails.textAlignment = .right
        } else {
            cover.leftAnchor.constraint(equalTo: mainPanal.leftAnchor, constant: 8.0).isActive = true
            labels.rightAnchor.constraint(equalTo: mainPanal.rightAnchor, constant: -8.0).isActive = true
            labels.leftAnchor.constraint(equalTo: cover.rightAnchor, constant: 8.0).isActive = true
            lblTitle.textAlignment = .left
            lblDetails.textAlignment = .left
        }
    }
    
    
    // MARK: - Actions

    
}


































