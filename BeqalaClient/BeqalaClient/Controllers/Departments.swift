
import UIKit
import KYDrawerController

final class Departments: BaseController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var sectionsDataSource = Array<Section>() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.categoriesGrid.reloadData()
            }
        }
    }
    fileprivate var sliderDataSource = Array<Promotion>() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.sliderGrid.reloadData()
            }
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var titlePanal: UIView! {
        didSet {
            shadow(for: titlePanal)
        }
    }    
    @IBOutlet private weak var sliderPanal: UIView! {
        didSet {
            sliderPanal.setShadow(with: Size(right: 0, top: 1.3), radius: 0.8, opacity: 0.3, color: .black)
        }
    }
    @IBOutlet fileprivate weak var sliderGrid: UICollectionView! {
        didSet {
            sliderGrid.delegate = self
            sliderGrid.dataSource = self
        }
    }
    @IBOutlet fileprivate weak var sliderIndicator: UIPageControl! {
        didSet {
            sliderIndicator.numberOfPages = Number.zero.intValue
        }
    }
    @IBOutlet private weak var sliderPanalHeight: NSLayoutConstraint!
    @IBOutlet fileprivate weak var categoriesGrid: UICollectionView! {
        didSet {
            categoriesGrid.delegate = self
            categoriesGrid.dataSource = self
        }
    }
    //@IBOutlet fileprivate weak var btnBack: UIButton!
    @IBOutlet fileprivate weak var btnMenu: UIButton!
    @IBOutlet fileprivate weak var btnSearch: UIButton!
    @IBOutlet fileprivate weak var btnFavorite: UIButton!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        loadData()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
        loadSliderItems()
    }
    
    internal override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        sliderPanalHeight.constant = view.bounds.height*0.40
    }
    
    
    // MARK: - Actions
    
    /*@IBAction private func backClicked(_ sender: UIButton) {
        back()
    }*/
    @IBAction private func openMenuOnCLick(_ sender: UIButton) {
        menu(withState: .opened)
    }
    
    @IBAction private func openSearchOnCLick(_ sender: UIButton) {
        navigateToSearch()
    }
    
    @IBAction private func openFavoritesOnCLick(_ sender: UIButton) {
        navigateToFavorites()
    }
    

}


// MARK: - Helper FUnctions

extension Departments {
    
    fileprivate func updateUi()-> void {
        tabBar.isHidden = true
        btnMenu.transform = CGAffineTransform.identity
        view.bringSubview(toFront: titlePanal)
        //btnBack.transform = CGAffineTransform.identity
        if (Localizer.instance.current == .arabic) {
            //btnBack.leftAnchor.constraint(equalTo: titlePanal.leftAnchor, constant: 8).isActive = true
            btnMenu.rightAnchor.constraint(equalTo: titlePanal.rightAnchor, constant: -8.0).isActive = true
            btnFavorite.leftAnchor.constraint(equalTo: titlePanal.leftAnchor, constant: 8.0).isActive = true
            btnSearch.leftAnchor.constraint(equalTo: btnFavorite.rightAnchor, constant: 20.0).isActive = true
            categoriesGrid.semanticContentAttribute = .forceRightToLeft
            sliderGrid.semanticContentAttribute = .forceLeftToRight
        } else {
            //btnBack.rightAnchor.constraint(equalTo: titlePanal.rightAnchor, constant: -8).isActive = true
            //btnBack.transform = btnBack.transform.rotated(by: cgFloat.pi)
            btnMenu.leftAnchor.constraint(equalTo: titlePanal.leftAnchor, constant: 8.0).isActive = true
            btnFavorite.rightAnchor.constraint(equalTo: titlePanal.rightAnchor, constant: -8.0).isActive = true
            btnSearch.rightAnchor.constraint(equalTo: btnFavorite.leftAnchor, constant: -20.0).isActive = true
            categoriesGrid.semanticContentAttribute = .forceLeftToRight
            sliderGrid.semanticContentAttribute = .forceRightToLeft
            btnMenu.transform = btnMenu.transform.rotated(by: cgFloat.pi)
        }
    }
    
    fileprivate func setupCollectionView()-> void {
        sliderGrid.register(Constants.Nibs.Slider, forCellWithReuseIdentifier: Constants.ReuseIdentifiers.Slider)
        sliderGrid.showsHorizontalScrollIndicator = false
        sliderGrid.showsVerticalScrollIndicator = false
        sliderGrid.isPagingEnabled = true
        // categories collection view
        categoriesGrid.register(Constants.Nibs.Category, forCellWithReuseIdentifier: Constants.ReuseIdentifiers.Category)
        categoriesGrid.alwaysBounceVertical = true
        //categoriesGrid.contentInset = Insets(top: 0, left: 0, bottom: 60, right: 0)
        //categoriesGrid.scrollIndicatorInsets = Insets(top: 0, left: 0, bottom: 60, right: 0)
    }
    
    fileprivate func loadData()-> void {
        if (Gemo.gem.isConnected) {
            Gemo.gem.startSpinning(in: view)
            Section.getData(page: self) { [weak self] (sections) in
                self?.sectionsDataSource = sections                
                Gemo.gem.stopSpinning()
            }
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
    
    fileprivate func loadSliderItems()-> void {
        if (Gemo.gem.isConnected) {
            Promotion.getData { [weak self] (promotions) in
                if (!promotions.isEmpty) {
                    self?.sliderDataSource = promotions
                    DispatchQueue.main.async { [weak self] in
                        self?.sliderIndicator.numberOfPages = promotions.count
                        self?.sliderIndicator.currentPage = 0
                    }
                }
            }
        }
    }
    
}


// MARK: - Collection view delegate & data source functions

extension Departments
{
    internal override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch (collectionView) {
        case sliderGrid:
            return sliderDataSource.count
        case categoriesGrid:
            return sectionsDataSource.count
        default:
            return super.collectionView(collectionView, numberOfItemsInSection: section)
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch (collectionView) {
        case sliderGrid:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ReuseIdentifiers.Slider, for: indexPath) as? SliderCell else { return UICollectionViewCell() }
            cell.promotion = sliderDataSource[indexPath.item]
            return cell
            
        case categoriesGrid:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ReuseIdentifiers.Category, for: indexPath) as? CategoryCell else {
                return UICollectionViewCell()
            }
            cell.section = sectionsDataSource[indexPath.item]
            return cell
            
        default:
            return super.collectionView(collectionView, cellForItemAt: indexPath)
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch (collectionView) {
        case sliderGrid:
            print("slider selected item is: \(indexPath.item)")
            guard let promottionDetailsPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.ProductDetails) as? ProductDetails else { return }
            promottionDetailsPage.productId = sliderDataSource[indexPath.item].id
            navigate(to: promottionDetailsPage)
        case categoriesGrid:
            if let sectionInfoScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.CategoryDetails) as? SectionDetails {
                sectionInfoScreen.section = sectionsDataSource[indexPath.item]
                navigate(to: sectionInfoScreen)
            }
            
            
        default:
            super.collectionView(collectionView, didSelectItemAt: indexPath)
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch (collectionView) {
        case sliderGrid:
            return Size(width: collectionView.frame.width, height: collectionView.frame.height)//view.bounds.height*0.25
            
        case categoriesGrid:
            let dimentions = collectionView.frame.width/2-5
            return Size(width: dimentions, height: dimentions)
        default:
            return super.collectionView(collectionView, layout: collectionViewLayout, sizeForItemAt: indexPath)

        }
    }
    
    internal func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let pageNumber = targetContentOffset.pointee.x/sliderGrid.frame.width
        print("page number: \(pageNumber)")
        sliderIndicator.currentPage = int(pageNumber)
    }
    
}




























