
import UIKit
import Localize_Swift

final class MemberCell: UITableViewCell, CellProvider
{

    // MARK: - Constants
    
    //MARK: - Variables
    
    internal var person: Person? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var avatar: UIImageView!
    @IBOutlet private weak var lblName: UILabel!
    @IBOutlet private weak var mainPanal: UIView!
    
    
    // MARK: - Overridden functions
    
    internal func updateUI() {
        lblName.text = person?.name?.trimmingCharacters(in: .whitespaces) ?? string.empty
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            lblName.textAlignment = .right
            mainPanal.semanticContentAttribute = .forceLeftToRight
        } else {
            lblName.textAlignment = .left
            mainPanal.semanticContentAttribute = .forceRightToLeft
        }
    }
    
    //MARK: - Actions
    
    
}
