
import UIKit


final class SendMassageVC: UIViewController,
    UICollectionViewDelegate,
    UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout,
    UITextFieldDelegate
{
    var selectedID: String!
    var selectedImage: String!
    var selectedName: String!
    var images_cache = [String:UIImage]()
    var dataFound: Bool!
    var myID: String!
    var alertMsg: String!
    var getMsg = Array<MassageModel>()
    
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var messageText: UITextField!
    @IBOutlet weak var showMassage: UICollectionView!
    @IBOutlet weak var openMenu: UIBarButtonItem!
    @IBOutlet weak var sendMsgView: UIView!
    @IBOutlet var sendMassageView: UIView!
    @IBOutlet private weak var bottomMargin: NSLayoutConstraint!
    @IBOutlet private weak var btnBlock: UIButton!
    
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnBlock.imageView?.contentMode = .scaleAspectFit
    }
    
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        showMassage.delegate = self
        showMassage.dataSource = self
        showMassage.alwaysBounceVertical = true
        messageText.delegate = self
        myID = UserDefaults.standard.string(forKey: "users_ID")
        showMassages(Settings.lang, myID!, selectedID)
        print("\(myID)")
        print("\(selectedID)")
        userNameLbl.text = selectedName
        load_image(selectedImage, imageview: userImage)
        userImage.layer.cornerRadius = 0.5 * userImage.bounds.size.width
        userImage.clipsToBounds = true
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboard(_:)), name: .UIKeyboardWillHide, object: nil)
        self.title = "Chat"
        if (Settings.lang == "ar") {
            self.title = "محادثة"
            messageText.placeholder = "ارسال رسالة"
        }
        if (revealViewController() != nil) {
            revealViewController().rightViewRevealWidth = 235
            openMenu.target = revealViewController()
            openMenu.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    @IBAction private func blockOnClick(_ sender: UIButton) {
        guard let url = URL(string: Settings.Block) else { return }
        var request = URLRequest(url: url)
        let paramters = "studentId=\(myID ?? "")&teacherId=\(selectedID ?? "")&lang=\(Settings.lang)"
        request.httpMethod = "POST"
        request.httpBody = paramters.data(using: .utf8)
        URLSession.shared
            .dataTask(with: request) {[weak self] (result, response, error) in
                if (error != nil) {
                    print("bolck error \(String(describing: error))")
                    return
                }
                guard let jsonData = result else { return }
                print("block string response ", String(data: jsonData, encoding: .utf8) ?? "")
                do {
                    guard let json = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? Dictionary<String, Any>,
                    let message = json["Message"] as? String else { return }
                    print("serialzed json = \(json)")
                    self?.displayAlert(message)
                } catch {
                    print("cannot serialize because error: \(String(describing: error))")
                }
        }
        .resume()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
        
    }
    
    @objc private func showKeyboard(_ notification: NSNotification) {
        let keyboardFrame = notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? CGRect ?? .zero
        let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval ?? 0
        let curve = notification.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? UInt ?? 0
        bottomMargin.constant = keyboardFrame.height
        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions.init(rawValue: curve), animations: { [weak self] in
            self?.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @objc private func hideKeyboard(_ notification: NSNotification) {
        let duration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval ?? 0
        let curve = notification.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? UInt ?? 0
        bottomMargin.constant = 0.0
        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions.init(rawValue: curve), animations: { [weak self] in
            self?.view.layoutIfNeeded()
            }, completion: nil)

    }
    
    func load_image(_ link:String, imageview:UIImageView) {
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.timeoutInterval = 10
        
        let task = session.dataTask(with: url, completionHandler: { (data :Data?, response :URLResponse?, error:Error?) in
            guard let _: Data = data, let _: URLResponse = response, error == nil else { return }
            var image = UIImage(data: data!)
            if (image != nil) {
                func set_image() {
                    self.images_cache[link] = image
                    imageview.image = image
                }
                DispatchQueue.main.async(execute: set_image)
            }
            
        })
        task.resume()
    }
    
    private func showMassages(_ lang: String,_ myID: String,_ userID: String) {
        //shadowView.isHidden = false
        getMsg.removeAll()
        showMassage.reloadData()
        let request = NSMutableURLRequest(url: URL(string:Settings.ShowMassags)!)
        request.httpMethod = "POST"
        let postString = "lang=\(lang)&msgFrom=\(myID)&msgTo=\(userID)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        { data, response, error in
            guard error == nil && data != nil else {
                print("error=\(String(describing: error))")
                return
            }
            
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
            
            if let responseData = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                    print("ddd\(json)")
                    self.dataFound = json["Success"] as? Bool
                    if (self.dataFound!) {
                        let data = json["Result"] as! NSDictionary
                        let msg = data["msg"] as! [NSDictionary]
                        for i in msg {
                            let message = i["msg"] as! String
                            let date = i["date_time"] as! String
                            let userName = i["username"] as! String
                            let msg:MassageModel = MassageModel(msg: message, date: date, userName: userName)
                            self.getMsg.append(msg)
                        }
                    }
                    DispatchQueue.main.async {
                        //self.shadowView.isHidden = true
                        self.showMassage.reloadData()
                    }
                } catch {
                    print ("Could not Serialize")
                }
            }
        }
        task.resume()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func sendMassages(_ lang: String,_ myID: String,_ userID: String,_ Massage: String) {
        let request = NSMutableURLRequest(url: URL(string:Settings.SendMessages)!)
        request.httpMethod = "POST"
        let postString = "lang=\(lang)&msgFrom=\(myID)&msgTo=\(userID)&msg=\(Massage)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { [weak self] (data, response, error) in
            guard let this = self else { return }
            guard error == nil && data != nil else {
                print("error=\(String(describing: error))")
                return
            }
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
            if let responseData = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
                    print("message json data: \(json)")
                    this.dataFound = json["Success"] as? Bool
                    if (this.dataFound!) {
                        self?.showMassages(lang, myID, userID)
                    } else {
                        this.alertMsg = json["Message"] as! String
                        this.displayAlert(this.alertMsg)
                    }
                    
                    DispatchQueue.main.async { [weak self] in
                        self?.messageText.text = ""
                    }
                } catch {
                    print ("Could not Serialize")
                }
            }
        }
        task.resume()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "massagesCell", for: indexPath)
            as! SendMassages
        let myName = UserDefaults.standard.string(forKey: "userName")
        if myName == self.getMsg[indexPath.row].userName { // user messages
            cell.messageLabel.text = self.getMsg[indexPath.row].msg
            cell.messageImage.image = UIImage(named: "message2")
            cell.messageLabel.textAlignment = .left
            cell.sendView.semanticContentAttribute = .forceRightToLeft
            cell.leftMargin.constant = 16.0
            cell.trueIcon.image = #imageLiteral(resourceName: "senticon")
            cell.trueIcon.isHidden = false
        } else {
            cell.messageLabel.text = self.getMsg[indexPath.row].msg
            cell.messageImage.image = UIImage(named: "message")
            cell.messageLabel.textAlignment = .right
            cell.sendView.semanticContentAttribute = .forceLeftToRight
            cell.leftMargin.constant = 16.0
            cell.trueIcon.image = nil
            cell.trueIcon.isHidden = true
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.getMsg.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.frame.width-16
        let estimatedSize = CGSize(width: width, height: 1000)
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 16)]
        let estimatedFrame = NSString(string: getMsg[indexPath.row].msg).boundingRect(with: estimatedSize, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
        return CGSize(width: width, height: estimatedFrame.height+16)
    }
    
    func displayAlert(_ Massage: String) {
        var t = ""
        var b = ""
        
        if Settings.lang == "ar"{
            t = "تنبيه"
            b = "إغلاق"
        } else {
            t = "Alert"
            b = "Close"
        }
        
        let myAlert = UIAlertController(title: t, message:Massage, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default,handler: { (action: UIAlertAction!) in
            self.viewDidLoad()
            self.getMsg.removeAll()
        })
        myAlert.addAction(okAction);
        present(myAlert, animated: true, completion: nil)
    }
    
    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        self.navigationController!.popViewController(animated: true)
    }
    
    @IBAction func sendMessageBtn(_ sender: UIButton) {
        self.view.endEditing(true)
        guard let message = messageText.text, !message.isEmpty else {
            return
        }
        sendMassages(Settings.lang, myID , selectedID, message)
    }
    
}





















