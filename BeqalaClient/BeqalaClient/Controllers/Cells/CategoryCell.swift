
import Foundation
import UIKit


internal final class CategoryCell: BaseGridCell
{
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var section: Section? {
        didSet {
            updateUi()
        }
    }
    
    
    // MARK: - Outlets
    
    @IBOutlet private weak var mainPanal: UIView! {
        didSet {
            mainPanal.setShadow(with: .zero, radius: 0.7, opacity: 0.5, color: .gray)
        }
    }
    @IBOutlet private weak var cover: UIImageView!
    @IBOutlet private weak var lblTitle: UILabel! {
        didSet {
            lblTitle.text = .empty
        }
    }
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        if let imageLink = section?.cover {
            cover.loadImage(from: imageLink)
        }
        lblTitle.text = section?.title ?? .empty
    }
    
    
    // MARK: - Actions

    
}

































