
import Foundation

import Localize_Swift

final class Item
{
    private static let session = URLSession.shared
    internal var id, title, iconName, link, longitude, latitude: string?
    
    init(title: string, iconName: string) {
        self.title = title
        self.iconName = iconName
    }
    
    init(id: string, title: string, iconName: string, link: string, latitude: string, longitude: string) {
        self.title = title
        self.iconName = iconName
        self.id = id
        self.link = link
        self.latitude = latitude
        self.longitude = longitude
    }
    
    init(dictionary: [string: any], iconName: string?) {
        self.id = dictionary["id"] as? string
        self.title = dictionary["title"] as? string
        self.iconName = iconName
        self.link = dictionary["url"] as? string
        self.latitude = dictionary["lati"] as? string
        self.longitude = dictionary["long"] as? string
    }
    
    internal class func getStaticData()-> [Item] {
        var items = Array<Item>()
        items.append(Item(title: "callTitle".localized(), iconName: "call"))
        items.append(Item(title: "mapTitle".localized(), iconName: "map"))
        items.append(Item(title: "siteTitle".localized(), iconName: "www"))
        items.append(Item(title: "emailTitle".localized(), iconName: "mail"))
        return items
    }
    
    internal class func fetchTranslationData(viewController: UIViewController?, onComplete: @escaping ([Item])-> void)-> void {
        UIApplication.showIndicator(by: true)
        let link = (Localize.currentLanguage() == Constants.Misc.ArabicSign) ? Constants.BackendURLs.TranslationAR : Constants.BackendURLs.TranslationEN
        if (link.isEmpty || !link.isLink) {
            viewController?.dialog(with: "ErrorMessage".localized())
            print("link error")
            UIApplication.showIndicator(by: false)
            return
        }
        
        guard let urlRequest = URL(string: link) else {
            viewController?.dialog(with: "ErrorMessage".localized())
            print("url request error")
            UIApplication.showIndicator(by: false)
            return
        }
        
        session.dataTask(with: urlRequest) { (data, response, error) in
            if (error != nil) {
                viewController?.handle(this: error)
                print("response error")
                UIApplication.showIndicator(by: false)
                return
            }
            
            guard let jsonData = data else {
                viewController?.dialog(with: "ErrorMessage".localized())
                print("none data error")
                UIApplication.showIndicator(by: false)
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [[string: any]] {
                    onComplete(json.map { Item(dictionary: $0, iconName: "download") })
                }
            } catch {
                viewController?.handle(this: error)
                print("catch error")
                UIApplication.showIndicator(by: false)
            }
            
            }.resume()
        
    }
    
    internal class func fetchToilets(by link: string, viewController: UIViewController?, onComplete: @escaping ([Item])-> void)-> void {
        UIApplication.showIndicator(by: true)
        if (link.isEmpty || !link.isLink) {
            print("link error")
            UIApplication.showIndicator(by: false)
            viewController?.dialog(with: Constants.Misc.ErrorMessage)
            return
        }
        
        guard let urlRequest = URL(string: link) else {
            print("request url error")
            UIApplication.showIndicator(by: false)
            viewController?.dialog(with: Constants.Misc.ErrorMessage)
            return
        }
        
        session.dataTask(with: urlRequest) { (data, response, error) in
            if (error != nil) {
                print("request error")
                UIApplication.showIndicator(by: false)
                viewController?.handle(this: error)
                return
            }
            
            guard let jsonData = data else {
                print("nix data error")
                UIApplication.showIndicator(by: false)
                viewController?.dialog(with: Constants.Misc.ErrorMessage)
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [[string: any]] {
                    onComplete(json.map { Item(dictionary: $0, iconName: "map") })
                }
            } catch {
                print("catch error")
                UIApplication.showIndicator(by: false)
                viewController?.handle(this: error)
            }
            
        }.resume()
        
    }
    
}
