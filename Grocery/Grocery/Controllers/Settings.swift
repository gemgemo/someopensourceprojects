
import UIKit
import Gemo

final class Settings: BaseController
{
    
    // MARK: - Constatnts
    
    
    // MARK: - Variables
    
    
    // MARK: - Outlets
    
    @IBOutlet private weak var headerPanel: View!
    @IBOutlet private weak var midPanel: View!
    @IBOutlet private weak var footerPanel: View!
    @IBOutlet private weak var headerIcons: UIStackView!
    @IBOutlet private weak var headerButtons: UIStackView!
    @IBOutlet private weak var midIcon: UIImageView!
    @IBOutlet private weak var btnAboutApp: UIButton!
    @IBOutlet private weak var policyIcon: UIImageView!
    @IBOutlet private weak var btnPolicy: UIButton!
    @IBOutlet private weak var btnUpdateProfile: UIButton!
    @IBOutlet private weak var btnAppLanguage: UIButton!
    @IBOutlet private weak var btnNotifications: UIButton!
    @IBOutlet private weak var btnLogout: UIButton!
    @IBOutlet fileprivate weak var isEnabeldNorifications: UISwitch! { didSet {
        isEnabeldNorifications.tintColor = #colorLiteral(red: 0.3130916357, green: 0.3331323266, blue: 0.3498122096, alpha: 1)
        isEnabeldNorifications.layer.cornerRadius = 16
        isEnabeldNorifications.backgroundColor = #colorLiteral(red: 0.3130916357, green: 0.3331323266, blue: 0.3498122096, alpha: 1)
        }}
    @IBOutlet private weak var notificationsPanel: UIView!
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(changeLanguage), name: .LanguageChanged, object: nil)
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        selectedTabBarItem = 3
        tabBarItems?.selectItem(at: IndexPath(item: selectedTabBarItem, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        updateNotificationSwitch()
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(updateNotificationSwitch), name: .UpdateNotificationsSwitch, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    internal override func updateUi() {
        setupMenuButton()
        navigationItem.title = Localization.Settings.localized()
        btnUpdateProfile.setTitle(Localization.UpdateProfile.localized(), for: .normal)
        btnAppLanguage.setTitle(Localization.AppLanguage.localized(), for: .normal)
        btnNotifications.setTitle(Localization.TurnOffNotifications.localized(), for: .normal)
        btnLogout.setTitle(Localization.Logout.localized(), for: .normal)
        btnAboutApp.setTitle(Localization.AboutApp.localized(), for: .normal)
        btnPolicy.setTitle(Localization.TermsOfUse.localized(), for: .normal)
        if (Localizer.instance.current == .arabic) {
            btnUpdateProfile.contentHorizontalAlignment = .right
            btnAppLanguage.contentHorizontalAlignment = .right
            btnNotifications.contentHorizontalAlignment = .right
            btnLogout.contentHorizontalAlignment = .right
            btnAboutApp.contentHorizontalAlignment = .right
            btnPolicy.contentHorizontalAlignment = .right
            //header
            headerIcons.rightAnchor.constraint(equalTo: headerPanel.rightAnchor, constant: -8).isActive = true
            headerButtons.rightAnchor.constraint(equalTo: headerIcons.leftAnchor, constant: -8).isActive = true
            headerButtons.leftAnchor.constraint(equalTo: headerPanel.leftAnchor, constant: 8).isActive = true
            //mid
            midIcon.rightAnchor.constraint(equalTo: midPanel.rightAnchor, constant: -8).isActive = true
            btnAboutApp.rightAnchor.constraint(equalTo: midIcon.leftAnchor, constant: -8).isActive = true
            btnAboutApp.leftAnchor.constraint(equalTo: midPanel.leftAnchor, constant: 8).isActive = true
            //footer
            policyIcon.rightAnchor.constraint(equalTo: footerPanel.rightAnchor, constant: -8).isActive = true
            btnPolicy.rightAnchor.constraint(equalTo: policyIcon.leftAnchor, constant: -8).isActive = true
            btnPolicy.leftAnchor.constraint(equalTo: footerPanel.leftAnchor, constant: 8).isActive = true
            isEnabeldNorifications.leftAnchor.constraint(equalTo: notificationsPanel.leftAnchor, constant: 8).isActive = true
        } else {
            btnUpdateProfile.contentHorizontalAlignment = .left
            btnAppLanguage.contentHorizontalAlignment = .left
            btnNotifications.contentHorizontalAlignment = .left
            btnLogout.contentHorizontalAlignment = .left
            btnAboutApp.contentHorizontalAlignment = .left
            btnPolicy.contentHorizontalAlignment = .left
            //header
            headerIcons.leftAnchor.constraint(equalTo: headerPanel.leftAnchor, constant: 8).isActive = true
            headerButtons.leftAnchor.constraint(equalTo: headerIcons.rightAnchor, constant: 8).isActive = true
            headerButtons.rightAnchor.constraint(equalTo: headerPanel.rightAnchor, constant: -8).isActive = true
            //mid
            midIcon.leftAnchor.constraint(equalTo: midPanel.leftAnchor, constant: 8).isActive = true
            btnAboutApp.leftAnchor.constraint(equalTo: midIcon.rightAnchor, constant: 8).isActive = true
            btnAboutApp.rightAnchor.constraint(equalTo: midPanel.rightAnchor, constant: -8).isActive = true
            //footer
            policyIcon.leftAnchor.constraint(equalTo: footerPanel.leftAnchor, constant: 8).isActive = true
            btnPolicy.leftAnchor.constraint(equalTo: policyIcon.rightAnchor, constant: 8).isActive = true
            btnPolicy.rightAnchor.constraint(equalTo: footerPanel.rightAnchor, constant: -8).isActive = true
            isEnabeldNorifications.rightAnchor.constraint(equalTo: notificationsPanel.rightAnchor, constant: -8).isActive = true
        }
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Actions
    
    @IBAction private func updateProfileOnClick(_ sender: UIButton) {
        guard let updateProfilePage = UIStoryboard(name: Storyboards.Registration, bundle: nil).instantiateViewController(withIdentifier: Storyboards.SignUp) as? SignUp else { return }
        updateProfilePage.isUpdate = true
        navigate(to: updateProfilePage)
        
    }
    
    @IBAction private func changeLanguageOnClick(_ sender: UIButton) {
        let alert = UIAlertController(title: "اختار لغة التطبيق", message: "Choose App Language", preferredStyle: .alert)
        alert.view.tintColor = .main
        // arabic
        alert.addAction(UIAlertAction(title: "اللغه العربية", style: .default, handler: { (action) in
            Localizer.instance.set(language: .arabic)
        }))
        // english
        alert.addAction(UIAlertAction(title: "English", style: .default, handler: { (action) in
            Localizer.instance.set(language: .english)
        }))
        // cancel
        alert.addAction(UIAlertAction(title: "اغلاق/Close", style: .cancel))
        present(alert, animated: true)
    }
    
    @IBAction private func notificationsOnClick(_ sender: UIButton) {
        guard let notificationSettingsUrl = URL(string: UIApplicationOpenSettingsURLString) else { return }
        if (UIApplication.shared.canOpenURL(notificationSettingsUrl)) {
            UIApplication.shared.open(notificationSettingsUrl, options: [:])
        }
    }
    
    @IBAction private func logoutOnClick(_ sender: UIButton) {
        if (Gemo.gem.isConnected) {
            Gemo.gem.startSpinning()
            let parameters: Dictionary<string, string> = [
                Misc.MainKey: Misc.MainValue,
                "user_id": UserDefaults.standard.string(forKey: Misc.GroceryId) ?? .empty
            ]
            Http.request(link: Urls.Logout, method: .post, parameters: parameters)
                .response { (response) in
                    let error = response.error
                    if (error != nil) {
                        Gemo.gem.stopSpinning()
                        print("logout error is: \(string(describing: error))")
                        Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription)
                        return
                    }
                    Gemo.gem.stopSpinning()
                    if let json = response.result as? Dictionary<string, any>, let success = json["result"] as? string, success == "true"  {
                        Threads.main {
                            let db = UserDefaults.standard
                            db.removeObject(forKey: Misc.UserLoggedIn)
                            db.removeObject(forKey: Misc.GroceryId)
                            db.removeObject(forKey: Misc.GroceryName)
                            db.removeObject(forKey: Misc.GroceryEmail)
                            db.removeObject(forKey: Misc.GroceryPhone)
                            db.removeObject(forKey: Misc.GroceryAvatar)
                            db.removeObject(forKey: Misc.GroceryLatitude)
                            db.removeObject(forKey: Misc.GroceryUsername)
                            db.removeObject(forKey: Misc.GroceryLongitude)
                            db.removeObject(forKey: Misc.GroceryDescription)
                            db.removeObject(forKey: Misc.GroceryDepartments)
                            db.synchronize()
                            (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController = UIStoryboard(name: Storyboards.Registration, bundle: nil).instantiateInitialViewController()
                        }
                    }
                }
        } else {
            dialogUserWithNoInternetConnectionMessage()
        }

    }
    
    @IBAction private func aboutAppOnClick(_ sender: UIButton) {
        guard let aboutPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.AboutAndTerms) as? About_Terms else { return }
        aboutPage.isTerms = false
        navigate(to: aboutPage)
    }
    
    @IBAction private func termsOfUseOnCLick(_ sender: UIButton) {
        guard let termsPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.AboutAndTerms) as? About_Terms else { return }
        termsPage.isTerms = true
        navigate(to: termsPage)
    }
    
    

}


// MARK:-  Helper functions

extension Settings
{
    
    @objc fileprivate func changeLanguage()-> void {
        print("this action invoked when change language")
        UIApplication.shared.keyWindow?.rootViewController = storyboard?.instantiateInitialViewController()
    }
    
    @objc fileprivate func updateNotificationSwitch()-> void {
        isEnabeldNorifications.isOn = Gemo.gem.isRegisteredNotifications
    }
        
    
}























