
import UIKit

import Localize_Swift
import QuickLook

final class ManagerMessageScene: BaseController
{
    
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    
    // MARK: - Outlets
    
    @IBOutlet private weak var avatar: UIImageView! {
        didSet {
            avatar.round()
        }
    }
    @IBOutlet private weak var name: UILabel!
    @IBOutlet private weak var details: UILabel!
    @IBOutlet private weak var icon: UIImageView!
    @IBOutlet private weak var summary: UILabel!
    @IBOutlet private weak var openPdf: UIButton!
    @IBOutlet private weak var detailsView: UIView! {
        didSet {
            detailsView.layer.cornerRadius = 10.0
            detailsView.layer.borderWidth = 1.0
            detailsView.layer.borderColor = Color(white: 0.88, alpha: 0.8).cgColor
        }
    }
    @IBOutlet private weak var iconContainer: UIView! {
        didSet {
            iconContainer.layer.cornerRadius = iconContainer.bounds.height/2
            iconContainer.layer.borderWidth = 1.0
            iconContainer.layer.borderColor = Color(white: 0.88, alpha: 0.8).cgColor
        }
    }
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
        openPdf.setTitle("openPDFButtonTitle".localized(), for: .normal)
        summary.text = "managermessagetitle".localized()
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: - Actions
    
    @IBAction private func openPdfOnCLick(_ sender: UIButton) {     
        gotoPDFViewer(with: Constants.BackendURLs.ManagerMessage)
    }
    
    

}


// MARK: - Helper functions

extension ManagerMessageScene
{
    
    @objc fileprivate func setupNavigationBar()-> void {
        navigationItem.title = "managerMessagesPageTitle".localized()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(goBack(_:)))
    }
    
    
    /*fileprivate func loadPDFFile(link onComplete: @escaping(string)-> void)-> void {
        UIApplication.showIndicator(by: true)
        let link = Constants.BackendURLs.ManagerMessage
        
        if (link.isEmpty || !link.isLink) {
            print("link error!")
            UIApplication.showIndicator(by: false)
            dialog(with: Constants.Misc.ErrorMessage)
            return
        }
        
        guard let url = URL(string: link) else {
            print("url error")
            UIApplication.showIndicator(by: false)
            dialog(with: Constants.Misc.ErrorMessage)
            return
        }
        
        URLSession.shared.dataTask(with: URLRequest(url: url)) { [weak self] (data, response, error) in
            guard let this = self else { return }
            if (error != nil) {
                print("request error")
                UIApplication.showIndicator(by: false)
                this.handle(this: error)
                return
            }
            
            guard let jsonData = data else {
                print("none data error")
                UIApplication.showIndicator(by: false)
                this.dialog(with: Constants.Misc.ErrorMessage)
                return
            }
            //print(string(data: jsonData, encoding: .utf8))
            do {
                if let json = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [string: string], let pdfLink = json["image"] {
                    onComplete(pdfLink)
                } else {
                    print("serrialization error")
                }
            } catch {
                print("catch error")
                UIApplication.showIndicator(by: false)
                this.handle(this: error)
            }
            
        }.resume()
    }*/
    
}



// MARK: - Qiuck Preview

/*extension ManagerMessageScene: QLPreviewControllerDelegate, QLPreviewControllerDataSource
{
    
    internal func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        // get pdf url
//        guard let url = URL.init(fileURLWithPath: pdfLink) else {
//            return URL.init(fileURLWithPath: "") as QLPreviewItem
//        }
        //print(url)
        return URL.init(fileURLWithPath: pdfLink) as QLPreviewItem// url as QLPreviewItem
    }
    
    
    internal func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
}
*/





















