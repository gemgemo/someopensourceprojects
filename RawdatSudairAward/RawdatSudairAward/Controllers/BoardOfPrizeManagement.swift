
import UIKit

final class BoardOfPrizeManagement: BaseController
{
    // MARK: - Constants
    
    //MARK: - Variables
    
    fileprivate var dataSource = [Person]() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tblMembers.reloadData()
            }
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var tblMembers: UITableView! {
        didSet {
            tblMembers.delegate = self
            tblMembers.dataSource = self
        }
    }
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBar()
    }
    
    //MARK: - Actions
}


// MARK: - Helper functions

extension BoardOfPrizeManagement
{
    
    fileprivate func updateNavigationBar()-> void {
        updateNavigationBarDirections()
        navigationItem.title = "boardofawardmanagementlan".localized()
        navigationController?.navigationBar.castShadow = "set"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: getBackImage(), style: .plain, target: self, action: #selector(goBack(_:)))
    }
    
    fileprivate func loadData()-> void {
        startSpin()
        Person.getData(by: Constants.BackendURLs.shared.Members, viewController: self) { [weak self] (persons) -> void in
            guard let this = self else { return }
            this.dataSource = persons
            this.stopSpin()
        }
    }
    
    
}


// MARK: - Table view delegate & data source functions

extension BoardOfPrizeManagement: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.PrizeManagement) as? PrizeManagementCell {
            cell.person = dataSource[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
}































