
import UIKit
import GoogleMaps
import CoreLocation

final class StoreShopping: BaseController
{
    
    
    // MARK: - Constants
    
    fileprivate let locationManager = CLLocationManager()
    
    // MARK: - Variables
    
    internal var deliveryWay = Number.zero.intValue, quantities = Array<int>()
    fileprivate var coordinate = CLLocationCoordinate2D(), text = string.empty, selectedTextField = UITextField()
    
    fileprivate var inputComponnets: UIView = {
        let panel = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40.0))
        panel.backgroundColor = .white
        return panel
    }()
    
    fileprivate var lblWritedText: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40.0))
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Font.systemFont(ofSize: 14.0)
        label.textColor = .black
        return label
    }()
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var titlePanel: UIView! { didSet { shadow(for: titlePanel) } }
    @IBOutlet fileprivate weak var lblTitle: UILabel! { didSet { lblTitle.text = .empty } }
    @IBOutlet fileprivate weak var lblsubtitle: UILabel! { didSet { lblsubtitle.text = .empty } }
    @IBOutlet fileprivate weak var mapView: GMSMapView!
    @IBOutlet fileprivate weak var txfName: TextField! { didSet { txfName.delegate = self } }
    @IBOutlet fileprivate weak var txfPhoneNumber: TextField! { didSet { txfPhoneNumber.delegate = self } }
    @IBOutlet fileprivate weak var txfBuilding: TextField! { didSet { txfBuilding.delegate = self } }
    @IBOutlet fileprivate weak var txfFloor: TextField! { didSet { txfFloor.delegate = self } }
    @IBOutlet fileprivate weak var btnSend: Button!
    @IBOutlet fileprivate weak var btnBack: UIButton!
    @IBOutlet fileprivate weak var lblRightPointer: UILabel!
    @IBOutlet fileprivate weak var lblLeftPointer: UILabel!
    @IBOutlet private weak var scrollView: UIScrollView! { didSet { scrollView.alwaysBounceVertical = true } }
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide(_:)), name: .UIKeyboardDidHide, object: nil)
    }
    
    internal override func becomeFirstResponder() -> Bool {
        return true
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureLocation()
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: - Actions
    
    @IBAction private func backOnClick(_ sender: UIButton) {
        view.endEditing(true)
        back()
    }
    
    @IBAction private func sendOnClick(_ sender: Button) {
        view.endEditing(true)
        if (Gemo.gem.isConnected) {
            guard !name.isEmpty, !phoneNumber.isEmpty, !building.isEmpty/*, !floor.isEmpty*/ else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.SomeDataReqiured.localized(), in: view)
                return
            }
            getAddress { [weak self] (address) in
                guard let this = self else { return }
                if (!address.isEmpty) {
                    //print(address, self?.deliveryWay)
                    let parameters: [string: any] = [
                        Constants.Misc.MainKey: Constants.Misc.MainValue,
                        "user_id": UserDefaults.standard.string(forKey: Constants.Misc.UserID) ?? .empty,
                        "type_send": self?.deliveryWay ?? Number.zero,
                        "name": self?.name ?? .empty,
                        "phone": self?.phoneNumber ?? .empty,
                        "building": self?.building ?? .empty,
                        "floor": self?.floor ?? .empty,
                        "address": address,
                        "quantity": this.quantities
                    ]
                    Gemo.gem.startSpinning(in: self?.view)
                    HttpClient.request(link: Constants.Urls.SendOrder, method: .post, parameters: parameters) { [weak self] (data, response, error) -> void in
                        if (error != nil) {
                            Gemo.gem.stopSpinning()
                            print("send order error: \(string(describing: error))")
                            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription, in: self!.view)
                            return
                        }
                        print("send order response \(string(describing: data))")
                        Gemo.gem.stopSpinning()
                        guard let json = (data as? [Dictionary<string, any>])?.first,
                            let result = json["result"] as? string,
                            let aMessage = json["data"] as? string,
                            let eMessage = json["data_en"] as? string else {
                                return
                        }
                        //Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: (Localizer.instance.current == .arabic) ? aMessage : eMessage, in: self!.view)
                        if (result == "true") {
                            ThreadQueue.main { [weak self] in
                                self?.navigationController?.popToRootViewController(animated: true)
                            }
                        }
                    }
                } else {
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.SomeDataReqiured.localized(), in: this.view)
                }
            }
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
    
    @objc private func keyboardDidShow(_ notification: Notification) {
        //print("keyboard info \(notification.userInfo)")                
        guard let keybordRect = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? Rect else { return }
        scrollView.contentInset = Insets(top: 0, left: 0, bottom: keybordRect.height, right: 0)
    }
    
    @objc private func keyboardDidHide(_ notification: Notification) {
        scrollView.contentInset = .zero//Insets(top: 0, left: 0, bottom: 0, right: 0)
    }
}


// MARK: - Helper functions

extension StoreShopping
{
    
    fileprivate func setupInputAccessoryView()-> UIView {
        lblWritedText.text = .empty
        inputComponnets.addSubview(lblWritedText)
        lblWritedText.topAnchor.constraint(equalTo: inputComponnets.topAnchor).isActive = true
        lblWritedText.rightAnchor.constraint(equalTo: inputComponnets.rightAnchor).isActive = true
        lblWritedText.bottomAnchor.constraint(equalTo: inputComponnets.bottomAnchor).isActive = true
        lblWritedText.leftAnchor.constraint(equalTo: inputComponnets.leftAnchor).isActive = true
        return inputComponnets
    }
    
    fileprivate func updateUi()-> void {
        tabBar.isHidden = true
        view.bringSubview(toFront: titlePanel)
        lblTitle.text = Localization.Shopping.localized()
        lblsubtitle.text = Localization.AddTitle.localized()
        txfName.placeholder = Localization.Name.localized()
        txfPhoneNumber.placeholder = Localization.PhoneNumber.localized()
        txfBuilding.placeholder = Localization.BuildinHouseNumber.localized()
        txfFloor.placeholder = Localization.Floor.localized()
        btnSend.setTitle(Localization.Send.localized(), for: .normal)
        btnBack.transform = CGAffineTransform.identity
        if (Localizer.instance.current == .arabic) {
            btnBack.leftAnchor.constraint(equalTo: titlePanel.leftAnchor, constant: 8).isActive = true
            lblLeftPointer.text = 2.toString
            lblRightPointer.text = 1.toString
            txfName.textAlignment = .right
            txfFloor.textAlignment = .right
            txfBuilding.textAlignment = .right
            txfPhoneNumber.textAlignment = .right
            
        } else {
            btnBack.rightAnchor.constraint(equalTo: titlePanel.rightAnchor, constant: -8).isActive = true
            btnBack.transform = btnBack.transform.rotated(by: cgFloat.pi)
            lblLeftPointer.text = 1.toString
            lblRightPointer.text = 2.toString
            txfName.textAlignment = .left
            txfFloor.textAlignment = .left
            txfBuilding.textAlignment = .left
            txfPhoneNumber.textAlignment = .left
            
        }
    }
    
    fileprivate func configureLocation()-> void {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        guard CLLocationManager.locationServicesEnabled() else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.OpenLocation.localized(), in: view)
            return
        }
        locationManager.startUpdatingLocation()
    }
    
    fileprivate func getAddress(_ completion: @escaping(string)->())-> void {
        GMSGeocoder().reverseGeocodeCoordinate(coordinate) { (response, error) in
            if (error != nil) {
                return
            }
            completion(response?.firstResult()?.lines?.joined(separator: ", ") ?? .empty)
        }
    }
}


// MARK: - Computed properties

extension StoreShopping
{
    
    fileprivate var name: string {
        return txfName.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var phoneNumber: string {
        return txfPhoneNumber.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var building: string {
        return txfBuilding.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var floor: string {
        return txfFloor.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
}


// MARK: - Location manager delegate functions

extension StoreShopping: CLLocationManagerDelegate
{
    
    internal func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("localition error \(string(describing: error))")
    }
    
    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            mapView.clear()
            self.coordinate = location.coordinate
            manager.stopUpdatingLocation()
            let marker = GMSMarker(position: coordinate)
            marker.icon = #imageLiteral(resourceName: "marker-map")
            marker.map = mapView
            mapView.camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: 15.0)
        } else {
            manager.stopUpdatingLocation()
        }
    }
    
}



// MARK: - Text Field delegate functions

extension StoreShopping: UITextFieldDelegate
{
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    internal func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTextField = textField
    }
    
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let str = textField.text as NSString?
        let text = str?.replacingCharacters(in: range, with: string)
        lblWritedText.text = text ?? .empty
        return true
    }
    
}






















