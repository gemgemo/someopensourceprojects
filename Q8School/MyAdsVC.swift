
import UIKit

class MyAdsVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var myAdsImg: UIImageView!
    @IBOutlet weak var callUsLbl: UILabel!
    @IBOutlet weak var addAdsLbl: UILabel!
    @IBOutlet weak var myAdsLbl: UILabel!
    @IBOutlet weak var offerLbl: UILabel!
    @IBOutlet weak var loadingLbl: UILabel!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var myAdsCollection: UICollectionView!
    @IBOutlet weak var openMenu: UIBarButtonItem!
    var types: String!
    var images_cache = [String:UIImage]()
    var myAds: Array<MyAd> = Array<MyAd>()
    var dataFound: Bool!
    var myID:String!


    override func viewDidLoad() {
        super.viewDidLoad()
        myAdsCollection.delegate = self
        myAdsCollection.dataSource = self
        shadowView.isHidden = true
        loadingView.isHidden = true
        self.title = "My Ads"
        if revealViewController() != nil {
            
            revealViewController().rightViewRevealWidth = 235
            openMenu.target = revealViewController()
            openMenu.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        myID = UserDefaults.standard.string(forKey: "users_ID")
        getMyAds(Settings.lang, myID)
        if Settings.lang == "ar"
        {
            callUsLbl.text = "اتصل بنا"
            addAdsLbl.text = "اضف اعلانك"
            myAdsLbl.text = "اعلاناتي"
            offerLbl.text = "العروض"
            self.title = "اعلاناتي"

        }
        types = UserDefaults.standard.string(forKey: "userType")
        if types == "1"
        {
            if Settings.lang == "ar"
            {
                myAdsLbl.text = "المفضلة"
                myAdsImg.image = UIImage(named: "fav")
                
            }
            else
            {
                myAdsLbl.text = "Favorite"
                myAdsImg.image = UIImage(named: "fav")
                
                
            }
        }

        // Do any additional setup after loading the view.
    }
    func getMyAds(_ lang: String,_ userID: String)
    {
            shadowView.isHidden = false
            loadingView.isHidden = false
        loadingView.layer.cornerRadius = 4
        loadingView.layer.masksToBounds = true
            loadingActivity.startAnimating()
            let request = NSMutableURLRequest(url: URL(string:Settings.MyADs)!)
            request.httpMethod = "POST"
            let postString = "lang=\(lang)&user_id=\(userID)"
            request.httpBody = postString.data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: request as URLRequest)
            { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(error)")
                    return
                }
                
                
                let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("responseString = \(responseString)")
                
                if let responseData = data {
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                        print("ddd\(json)")
                        self.dataFound = json["Success"] as? Bool
                        if self.dataFound!
                        {
                            
                            let data = json["Result"] as! [NSDictionary]
                            for i in data
                            {
                                
                                let id = i["id"] as! String
                                let teacherName = i["username"] as! String
                                let teacherImg = i["img"] as! String
                                let detial = i["details"] as! String
                                let date = i["date_time"] as! String
                                
                                
                                let myaD = MyAd(id: id, teacherName: teacherName, teacherImg: teacherImg, detial: detial, date: date)
                                self.myAds.append(myaD)
                                
                            }
                        }
                        DispatchQueue.main.async
                            {
                                self.shadowView.isHidden = true
                                self.loadingView.isHidden = true
                                self.loadingActivity.stopAnimating()
                                self.myAdsCollection.reloadData()
                                
                        }
                        
                    }
                    catch
                    {
                        print ("Could not Serialize")
                    }
                    
                }
                
            }
            task.resume()
            

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        let size = self.myAdsCollection.frame.width
        //        let height = self.teacherCollection.frame.height / 3
        
        return CGSize(width: size  - 10, height: 150)
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myAdsCell", for: indexPath)
            as! MyAds
        if Settings.lang == "ar"
        {
            if self.myAds[indexPath.row].teacherImg != ""
            {
                cell.userImage.image = nil
                load_image(self.myAds[indexPath.row].teacherImg, imageview: cell.userImage)
                cell.teacherLbl.text = "الاستاذ:"
                cell.detialsLbl.text = "تفاصيل:"
                cell.teacherName.text = self.myAds[indexPath.row].teacherName
                cell.userDetials.text = self.myAds[indexPath.row].detial
                cell.userImage.layer.cornerRadius = 0.5 * cell.userImage.bounds.size.width
                cell.userImage.clipsToBounds = true
                cell.userImage.layer.borderWidth = 2
                cell.userImage.layer.borderColor = UIColor.white.cgColor
                cell.teacherView.layer.cornerRadius = 10
                cell.teacherView.layer.masksToBounds = true
                let fullNameArr = self.myAds[indexPath.row].date.characters.split{$0 == " "}.map(String.init)
                cell.userDate.text = fullNameArr[0]
                cell.userTime.text = fullNameArr[1]
               cell.id = self.myAds[indexPath.row].id
                
                
            }
            else
            {
                cell.userImage.image = nil
                cell.userImage.image = UIImage(named: "pro_img")
                cell.teacherLbl.text = "الاستاذ:"
                cell.detialsLbl.text = "تفاصيل:"
                cell.teacherName.text = self.myAds[indexPath.row].teacherName
                cell.userDetials.text = self.myAds[indexPath.row].detial
                cell.userImage.layer.cornerRadius = 0.5 * cell.userImage.bounds.size.width
                cell.userImage.clipsToBounds = true
                cell.userImage.layer.borderWidth = 2
                cell.userImage.layer.borderColor = UIColor.white.cgColor
                cell.teacherView.layer.cornerRadius = 10
                cell.teacherView.layer.masksToBounds = true
                let fullNameArr = self.myAds[indexPath.row].date.characters.split{$0 == " "}.map(String.init)
                cell.userDate.text = fullNameArr[0]
                cell.userTime.text = fullNameArr[1]
                cell.id = self.myAds[indexPath.row].id
            }
            
        }
        else
        {
            if self.myAds[indexPath.row].teacherImg != ""
            {
                cell.userImage.image = nil
                load_image(self.myAds[indexPath.row].teacherImg, imageview: cell.userImage)
                cell.teacherName.text = self.myAds[indexPath.row].teacherName
                cell.userDetials.text = self.myAds[indexPath.row].detial
                cell.userImage.layer.cornerRadius = 0.5 * cell.userImage.bounds.size.width
                cell.userImage.clipsToBounds = true
                cell.userImage.layer.borderWidth = 2
                cell.userImage.layer.borderColor = UIColor.white.cgColor
                cell.teacherView.layer.cornerRadius = 10
                cell.teacherView.layer.masksToBounds = true
                let fullNameArr = self.myAds[indexPath.row].date.characters.split{$0 == " "}.map(String.init)
                cell.userDate.text = fullNameArr[0]
                cell.userTime.text = fullNameArr[1]
                cell.id = self.myAds[indexPath.row].id
                
                
            }
            else
            {
                cell.userImage.image = nil
                cell.userImage.image = UIImage(named: "pro_img")
                cell.teacherName.text = self.myAds[indexPath.row].teacherName
                cell.userDetials.text = self.myAds[indexPath.row].detial
                cell.userImage.layer.cornerRadius = 0.5 * cell.userImage.bounds.size.width
                cell.userImage.clipsToBounds = true
                cell.userImage.layer.borderWidth = 2
                cell.userImage.layer.borderColor = UIColor.white.cgColor
                cell.teacherView.layer.cornerRadius = 10
                cell.teacherView.layer.masksToBounds = true
                let fullNameArr = self.myAds[indexPath.row].date.characters.split{$0 == " "}.map(String.init)
                cell.userDate.text = fullNameArr[0]
                cell.userTime.text = fullNameArr[1]
                cell.id = self.myAds[indexPath.row].id
            }

            
        }
        
        
        return cell
        
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.myAds.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let cell = myAdsCollection.cellForItem(at: indexPath) as! MyAds
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TeacherDetialsVC") as! TeacherDetialsVC
        vc.selectedID = cell.id
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func load_image(_ link:String, imageview:UIImageView)
    {
        
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.timeoutInterval = 10
        
        
        let task = session.dataTask(with: url, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
                
                guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                    
                    return
                }
                
                
                var image = UIImage(data: data!)
                
                if (image != nil)
                {
                    
                    
                    func set_image()
                    {
                        self.images_cache[link] = image
                        imageview.image = image
                    }
                    
                    
                    DispatchQueue.main.async(execute: set_image)
                    
                }
                
        })
        
        task.resume()
        
    }
    func displayAlerts(_ Massage: String)
    {
        var t = ""
        var b = ""
        
        if Settings.lang == "ar"{
            t = "تنبيه"
            b = "إغلاق"
        }
        else{
            t = "Alert"
            b = "Close"
        }
        
        let myAlert = UIAlertController(title: t, message:Massage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MainPageViewController") as! MainPageViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func callUsBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CallUsVc") as! CallUsVc
        self.navigationController?.pushViewController(vc, animated: false)

    }
    
    @IBAction func addAdsBtn(_ sender: Any) {
        if types == "1"
        {
            if Settings.lang == "ar"
            {
                let msg = "انت طالب لا يمكنك اضافة اعلان"
                self.displayAlerts(msg)
                
            }
            else
            {
                let msg = "you'r student can't add Ads"
                self.displayAlerts(msg)
                
            }
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AddAdsVc") as! AddAdsVc
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        

    }
    
    @IBAction func myAdsBtn(_ sender: Any) {
        if types == "1"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyFavoriteVC") as! MyFavoriteVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyAdsVC") as! MyAdsVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        

    }
    
    @IBAction func offerBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AdsPageViewController") as! AdsPageViewController
        self.navigationController?.pushViewController(vc, animated: false)

    }
    
    
}



