
import UIKit
import Social
import MessageUI


final class CallUsVC: UIViewController
{

    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var socilaView: UIView!
    @IBOutlet weak var website: UILabel!
    @IBOutlet weak var websiteLb: UILabel!
    @IBOutlet weak var sendMessageLbl: UILabel!
    @IBOutlet weak var faxnumber: UILabel!
    @IBOutlet weak var faxLbl: UILabel!
    @IBOutlet weak var phone1: UILabel!
    @IBOutlet weak var phoneNumber1: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    var mobile: String!
    var mobile1: String!
    var fax: String!
    var email: String!
    var webSite: String!
    var facebook:String!
    var twitter:String!
    var insta: String!
    var youtube:String!
    var emailCC: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Call Us"
        if Settings.lang == "ar"
        {
            view1.semanticContentAttribute = .forceRightToLeft
            view2.semanticContentAttribute = .forceRightToLeft
            view3.semanticContentAttribute = .forceRightToLeft
            view4.semanticContentAttribute = .forceRightToLeft
            view5.semanticContentAttribute = .forceRightToLeft
            socilaView.semanticContentAttribute = .forceRightToLeft
            phoneLbl.text = "الهاتف:"
            phone1.text = "الهاتف:"
            faxLbl.text = "الفاكس:"
            sendMessageLbl.text = "إترك رسالة"
            websiteLb.text = "موقعنا:"
            self.title = "اتصل بنا"
            phoneNumber.textAlignment = .right
            phoneNumber1.textAlignment = .right
            faxnumber.textAlignment = .right
            website.textAlignment = .right
            sendMessageLbl.textAlignment = .right
            websiteLb.textAlignment = .right
            website.textAlignment = .right
        }
        if (Settings.isConnected) {
            CallUs()
        } else {
            dialogUser(with: "لا يوجد انترنت")
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func CallUs()
    {
        let urlString = Settings.CallUs
        let sessions = URLSession.shared
        let urls = URL(string: urlString)!
        sessions.dataTask(with: urls, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
                
                if let responseData = data {
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [NSDictionary]
                        //print("json1\(json)")
                        for d in 0..<json.count
                        {
                            let data = json[d]
                            self.mobile = data["phone"] as! String
                            self.mobile1 = data["phone1"] as! String
                            self.fax = data["fax"] as! String
                            self.email = data["email"] as! String
                            self.emailCC = data["email_cc"] as? String
                            self.webSite = data["url_2"] as! String
                            self.facebook = data["facebook"] as! String
                            self.twitter = data["twitter"] as! String
                            self.insta = data["instagram"] as! String
                            self.youtube = data["youtube"] as! String
                        }
                        DispatchQueue.main.async
                         {
                            self.phoneNumber.text = self.mobile
                            self.phoneNumber1.text = self.mobile1
                            self.faxnumber.text = self.fax
                            self.website.text = self.webSite
                        }
                    }
                        
                    catch
                    {
                        print ("Could not Serialize")
                    }
                }
                
        }).resume()
        
    }

    
    @IBAction func twitter(_ sender: UIButton) {
        UIApplication.open(this: twitter)
    }
    
    @IBAction func facebook(_ sender: UIButton) {
        UIApplication.open(this: facebook)
    }
    @IBAction func instagram(_ sender: UIButton) {
        UIApplication.open(this: insta)
    }
    
    @IBAction func youtube(_ sender: UIButton) {
        UIApplication.open(this: self.youtube)
    }
    @IBAction func sendEmail(_ sender: UIButton) {
        if let sendMailScreen = storyboard?.instantiateViewController(withIdentifier: "sendmailnavviewid") as? UINavigationController {
            present(sendMailScreen, animated: true, completion: nil)
        }
    }

    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        self.navigationController!.popViewController(animated: true)
    }
    
    @IBAction func callBtn(_ sender: UIButton) {
        UIApplication.open(this: "telprompt://\(self.mobile!)")
    }
    
    @IBAction func call1Btn(_ sender: UIButton) {
        UIApplication.open(this: "telprompt://\(self.mobile1!)")
    }
    
    @IBAction func websiteUrl(_ sender: UIButton) {
        UIApplication.open(this: self.webSite!)
    }

}



























