
import UIKit
import Alamofire

final class Activation: BaseController
{
    // MARK: - Constants
    
    // MARK: - Variables
    
    internal var id: string?
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var txfCode: TextField! {
        didSet {
            txfCode.delegate = self
        }
    }
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var lblSubtitle: UILabel!
    @IBOutlet fileprivate weak var btnConfirm: Button!
    @IBOutlet fileprivate weak var btnResend: Button!
    @IBOutlet fileprivate weak var headerView: UIView!
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBar()
        print("key id is : \(id)")
        updateUi()
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    // MARK: - Actions
    
    @IBAction private func submitOnClick(_ sender: UIButton) {
        startSpin()
        guard !key.isEmail else {
            showAlert(with: string.empty, and: "enteractivationcodelan".localized())
            stopSpin()
            return
        }
        
        guard let iD = id else {
            showAlert(with: string.empty, and: "erroralertlan".localized())
            stopSpin()
            return
        }
        
        sendKey(by: iD) { [weak self] (isSuccess, message) -> void in
            if (isSuccess == "True") {
                print("goto login page")
                _ = self?.navigationController?.popToRootViewController(animated: true)
            } else {
                self?.showAlert(with: string.empty, and: "enteractivationcodemessgalan".localized())
            }
            self?.stopSpin()
        }
        
    }
    
    @IBAction private func resendCodeOnClick(_ sender: UIButton) {
        startSpin()
        guard let iD = id else {
            showAlert(with: string.empty, and: "erroralertlan".localized())
            stopSpin()
            return
        }
        resendKey(by: iD) { [weak self] (isSuccess, message) -> void in
            (isSuccess == "True") ? self?.showAlert(with: string.empty, and: "resendMessagelan".localized()) : self?.showAlert(with: string.empty, and: message)
            self?.stopSpin()
        }
    }
    
    
    
}


// MARK: - Helper Functions 

extension Activation
{
    
    fileprivate func updateUi()-> void {
        let langCode = Locale.current.languageCode ?? "en"
        txfCode.placeholder = "activiationcodeplaceholdelan".localized()
        btnResend.setTitle("resendcodebuttontitlelan".localized(), for: .normal)
        btnConfirm.setTitle("confirmbuttontitlelan".localized(), for: .normal)
        lblTitle.text = "activationtitlelan".localized()
        lblSubtitle.text = "activationsubtitlelan".localized()
        if (langCode == Constants.Language.English) { //. english
            headerView.semanticContentAttribute = .forceRightToLeft
            lblTitle.textAlignment = .left
            lblSubtitle.textAlignment = .left
        } else { // arabic
            headerView.semanticContentAttribute = .forceLeftToRight
            lblTitle.textAlignment = .right
            lblSubtitle.textAlignment = .right
        }
    }
    
    fileprivate func updateNavigationBar()-> void {
        navigationItem.title = string.empty
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ac_back"), style: .plain, target: self, action: #selector(goBack(_:)))
    }
    
    
    fileprivate var key: string {
        return txfCode.text?.trimmingCharacters(in: .whitespaces) ?? string.empty
    }
    
    fileprivate func sendKey(by id: string, onComplete: @escaping(string, string)->void)-> void {
        Alamofire.request(Constants.BackendURLs.ActivateKey, method: .post, parameters: ["ID": id, "key": key])
                .responseJSON { [weak self] (response) in
                    let result = response.result
                    if (result.error != nil) {
                        self?.showAlert(with: string.empty, and: "erroralertlan".localized())
                        self?.stopSpin()
                        return
                    }
                    if let value = result.value as? [string: any], let success = value["Success"] as? string, let message = value["Massage"] as? string {
                        onComplete(success, message)
                    } else {
                        print("nil value")
                    }
        }
    }
    
    fileprivate func resendKey(by id: string, onComplete: @escaping(string, string)-> void)-> void  {
        Alamofire.request(Constants.BackendURLs.ResendKey, method: .post, parameters: ["ID": id])
            .responseJSON { [weak self] (response) in
                let result = response.result
                if (result.error != nil) {
                    print("resend error is: \(result.error)")
                    self?.showAlert(with: string.empty, and: "erroralertlan".localized())
                    self?.stopSpin()
                    return
                }
                if let value = result.value as? [string: any], let success = value["Success"] as? string, let message = value["Massage"] as? string {
                    onComplete(success, message)
                } else {
                    print("nix value when resend key")
                }
        }
    }
    
}


// MARK: - Text Field delegate functions

extension Activation: UITextFieldDelegate
{
    
    internal func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    internal func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}






















