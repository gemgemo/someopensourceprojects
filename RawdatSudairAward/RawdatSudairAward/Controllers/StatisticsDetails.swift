
import UIKit
import CarbonKit
import Localize_Swift

final class StatisticsDetails: BaseController
{
    
    // MARK: - Constants
    
    
    //MARK: - Variables
    
    fileprivate var keeperDataSource = [Keeper]() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tblStatisticsInfo.reloadData()
            }
        }
    }
    fileprivate var datesDataSource = Array<DateTime>()
    
    fileprivate var bottom: NSLayoutConstraint!
    internal var statistics: StatisticsNumbers?
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var dateView: UIView!
    @IBOutlet fileprivate weak var lblDate: UILabel! {
        didSet {
            lblDate.text = string.empty
        }
    }
    @IBOutlet fileprivate weak var tblStatisticsInfo: UITableView! {
        didSet {
            tblStatisticsInfo.delegate = self
            tblStatisticsInfo.dataSource = self
        }
    }
    @IBOutlet fileprivate var datePickerContainer: UIView!
    @IBOutlet fileprivate weak var datePicker: UIPickerView! {
        didSet {
            datePicker.delegate = self
            datePicker.dataSource = self
        }
    }
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let state = statistics?.id, (state == 5 || state == 6 || state == 3 || state == 4) else { return }
         configureTableView()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBar()
        if (statistics?.id == 1 || statistics?.id == 2) {
            createPager()
            tblStatisticsInfo.isHidden = true
            dateView.isHidden = true
        } else {
            navigationController?.navigationBar.castShadow = "set"
            tblStatisticsInfo.isHidden = false
            dateView.isHidden = false
            updateUI()
        }
        tblStatisticsInfo.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tableViewOnTapped(_:))))
    }
    
    //MARK: - Actions

    @IBAction private func showDatePickerOnClick(_ sender: UIButton) {
        bottom.constant = 0
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
                       usingSpringWithDamping: 0.6,
                       initialSpringVelocity: 0.4,
                       options: .curveEaseInOut,
                       animations: { [weak self] in
                        self?.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @objc fileprivate func gotoMainOnClick(_ sender: UIBarButtonItem) {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    @objc private func tableViewOnTapped(_ gesture: UITapGestureRecognizer) {
        hidePicker()
    }
}


// MARK: - Helper functions

extension StatisticsDetails
{
    fileprivate func updateNavigationBar()-> void {
        updateNavigationBarDirections()
        navigationItem.title = statistics?.title ?? string.empty
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: getBackImage(), style: .plain, target: self, action: #selector(goBack(_:)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "home"), style: .plain, target: self, action: #selector(gotoMainOnClick(_:)))
        navigationController?.navigationBar.layer.shadowColor = Color.clear.cgColor
        
    }
    
    fileprivate func setupLink()-> string {
        var humenType = string.empty
        let state = statistics?.id ?? Number.zero.intValue
        if (state == 5 || state == 6) {
            switch (state) {
            case 5: humenType = "1"
            case 6: humenType = "2"
            default: break
            }
            return "http://192.232.214.91/~rawdatsudeer/\(Constants.BackendURLs.shared.localizeUrl)/q_st.php?type=\(humenType)&date_h=\(lblDate.text ?? string.empty)&key=RAWDAT_SUDAIR"
        } else {
            switch (state) {
            case 3: humenType = "2"
            case 4: humenType = "1"
            default: break
            }
            return "http://192.232.214.91/~rawdatsudeer/\(Constants.BackendURLs.shared.localizeUrl)/teacher.php?type=\(humenType)&date_h=\(lblDate.text ?? string.empty)&key=RAWDAT_SUDAIR"
        }
    }
    
    fileprivate func loadData()-> void {
        Keeper.getData(from: setupLink(), page: self) { [weak self] (results) -> void in
            self?.keeperDataSource = results
            print("main stat data: \(results)")
            UIApplication.showIndicator(by: false)
        }
    }
    
    fileprivate func updateUI()-> void {
        dateView.layer.cornerRadius = 7.0
        dateView.layer.borderWidth = 1.0
        dateView.layer.borderColor = Color(white: 0.80, alpha: 0.8).cgColor
        setupPicker()
        startSpin()
        loadDates { [weak self] in
            self?.loadData()
        }
    }
    
    fileprivate func loadDates(onComplete: @escaping()-> void)-> void {
        DateTime.getData(from: Constants.BackendURLs.shared.DateTime, page: self) { [weak self] (dates) -> void in
            guard let this = self else { return }
            this.datesDataSource = dates
            DispatchQueue.main.async { [weak self] in
                self?.lblDate.text = dates.first?.title ?? string.empty
                self?.datePicker.reloadAllComponents()
                onComplete()
            }
            this.stopSpin()
        }
    }
    
    fileprivate func createPager()-> void {
        var tabsNames = [string]()// ["secondstagelan".localized(), "firstsatgelan".localized()]
        var pager = CarbonTabSwipeNavigation()
        switch (phoneLanguage()) {
        case "ar":
            if(Localize.currentLanguage() == Constants.Language.Arabic) {
                print("arabic")
                tabsNames = ["firstsatgelan".localized(), "secondstagelan".localized()]
            } else {
                print("english")
                tabsNames = ["secondstagelan".localized(), "firstsatgelan".localized()]
            }
        case "en":
            if(Localize.currentLanguage() == Constants.Language.Arabic) {
                print("arabic")
                tabsNames = ["secondstagelan".localized(), "firstsatgelan".localized()]
            } else {
                print("english")
                tabsNames = ["firstsatgelan".localized(), "secondstagelan".localized()]
            }
        default:
            break
        }
        pager = CarbonTabSwipeNavigation(items: tabsNames, delegate: self)
        switch (phoneLanguage()) {
        case "ar":
            if(Localize.currentLanguage() == Constants.Language.Arabic) {
                print("arabic")
                pager.setCurrentTabIndex(0, withAnimation: true)
            } else {
                print("english")
                pager.setCurrentTabIndex(1, withAnimation: true)
            }
        case "en":
            if(Localize.currentLanguage() == Constants.Language.Arabic) {
                print("arabic")
                pager.setCurrentTabIndex(1, withAnimation: true)
            } else {
                print("english")
                pager.setCurrentTabIndex(0, withAnimation: true)
            }
        default:
            break
        }
        pager.setTabExtraWidth(30)
        pager.setTabBarHeight(45)
        pager.setIndicatorHeight(2)
        pager.insert(intoRootViewController: self)
        pager.carbonSegmentedControl?.backgroundColor = .normal
        pager.toolbar.isTranslucent = false
        let sectionWidth = view.bounds.width/2
        pager.carbonSegmentedControl?.setWidth(sectionWidth, forSegmentAt: 0)
        pager.carbonSegmentedControl?.setWidth(sectionWidth, forSegmentAt: 1)
        pager.setIndicatorColor(.fixed)
        pager.setNormalColor(.black, font: Font.systemFont(ofSize: 13))
        pager.setSelectedColor(.fixed, font: Font.systemFont(ofSize: 13))
        pager.toolbar.setShadow(with: Size(right: 0, top: 1), radius: 1, opacity: 0.8, color: .lightGray)
    }

    
    private func setupPicker()-> void {
        datePickerContainer.translatesAutoresizingMaskIntoConstraints = false
        datePickerContainer.layer.borderColor = Color.darkGray.cgColor
        datePickerContainer.layer.borderWidth = 0.7
        view.addSubview(datePickerContainer)
        datePickerContainer.heightAnchor.constraint(equalToConstant: view.bounds.height*0.30).isActive = true
        datePickerContainer.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        datePickerContainer.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        bottom =  datePickerContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: view.bounds.height*0.30)
        bottom.isActive = true
    }
    
    fileprivate func configureTableView()-> void {
        tblStatisticsInfo.register(Constants.Nib.Keeper, forCellReuseIdentifier: Constants.ReuseIdentifier.Keeper)
    }
    
    fileprivate func hidePicker()-> void {
        bottom.constant = view.bounds.height*0.40
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
                       usingSpringWithDamping: 0.6,
                       initialSpringVelocity: 0.4,
                       options: .curveEaseInOut,
                       animations: { [weak self] in
                        self?.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    
}


// MARK: - Table view delegate & data source functions

extension StatisticsDetails: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return keeperDataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.Keeper) as? KeeperCell {
            cell.keeper = keeperDataSource[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
}


// MARK: - Picker delegate & data source functions

extension StatisticsDetails: UIPickerViewDelegate, UIPickerViewDataSource
{
    
    internal func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    internal func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return datesDataSource.count
    }
    
    internal func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return datesDataSource[row].title ?? string.empty
    }
    
    internal func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        hidePicker()
        print(datesDataSource[row])
        lblDate.text = datesDataSource[row].title ?? string.empty
        loadData()
    }
    
}


// MARK: - Carbon delegate functions

extension StatisticsDetails: CarbonTabSwipeNavigationDelegate
{
    
    internal func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt) {
        //print(#function)
    }
    
    internal func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt) {
        //print(#function)
    }
    
    internal func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didFinishTransitionTo index: UInt) {
        //print(#function)
    }
    
    internal func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willBeginTransitionFrom index: UInt) {
        //print(#function)
    }
    
    internal func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        let state = statistics?.id ?? Number.zero.intValue
        var humen = Number.zero.intValue
        switch (state) {
        case 1: humen = 2 // womens
        case 2: humen = 1 // mens
        default: break
        }
        switch (phoneLanguage()) {
        case "ar":
            if(Localize.currentLanguage() == Constants.Language.Arabic) {
                print("arabic")
                switch (index) {
                case 0:
                    if let firstStageScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.FirstStage) as? FirstStage {
                        firstStageScreen.gender = humen
                        return firstStageScreen
                    }
                    
                case 1:
                    if let seconStageScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.SecondStage) as? SecondStage {
                        seconStageScreen.gender = humen
                        return seconStageScreen
                    }
                    
                default:
                    return UIViewController()
                }

            } else {
                print("english")
                switch (index) {
                case 1:
                    if let firstStageScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.FirstStage) as? FirstStage {
                        firstStageScreen.gender = humen
                        return firstStageScreen
                    }
                    
                case 0:
                    if let seconStageScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.SecondStage) as? SecondStage {
                        seconStageScreen.gender = humen
                        return seconStageScreen
                    }
                    
                default:
                    return UIViewController()
                }

            }
        case "en":
            if(Localize.currentLanguage() == Constants.Language.Arabic) {
                print("arabic")
                switch (index) {
                case 1:
                    if let firstStageScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.FirstStage) as? FirstStage {
                        firstStageScreen.gender = humen
                        return firstStageScreen
                    }
                    
                case 0:
                    if let seconStageScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.SecondStage) as? SecondStage {
                        seconStageScreen.gender = humen
                        return seconStageScreen
                    }
                    
                default:
                    return UIViewController()
                }

            } else {
                print("english")
                switch (index) {
                case 0:
                    if let firstStageScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.FirstStage) as? FirstStage {
                        firstStageScreen.gender = humen
                        return firstStageScreen
                    }
                    
                case 1:
                    if let seconStageScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.SecondStage) as? SecondStage {
                        seconStageScreen.gender = humen
                        return seconStageScreen
                    }
                    
                default:
                    return UIViewController()
                }

            }
        default:
            break
        }
        return UIViewController()
    }
    
    /*internal func barPosition(for carbonTabSwipeNavigation: CarbonTabSwipeNavigation) -> UIBarPosition {
     
     }*/

    
}

























