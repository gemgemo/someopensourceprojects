
import UIKit
import Gemo
import Alamofire


class CarUser: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var isUpdate = false, car: Car?
    
    // MARK: - Outlets
    
    @IBOutlet weak var driverImage: UIImageView!
    @IBOutlet weak var btnSelectDriverPhoto: UIButton!
    @IBOutlet weak var lblDriverPhoto: UILabel!
    @IBOutlet weak var txfDriverName: TextField! { didSet { txfDriverName.delegate = self } }
    @IBOutlet weak var txfUsername: TextField! { didSet { txfUsername.delegate = self } }
    @IBOutlet weak var txfPassword: TextField! { didSet { txfPassword.delegate = self } }
    @IBOutlet weak var btnSave: UIButton!
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        lblDriverPhoto.text = Localization.DriverPhoto.localized()
        btnSave.setTitle(Localization.Save.localized(), for: .normal)
        txfDriverName.hint = Localization.FullName.localized()
        txfUsername.hint = Localization.UserName.localized()
        txfPassword.hint = Localization.Password.localized()
        if (Localizer.instance.current == .arabic) {
            txfDriverName.textAlignment = .right
            txfUsername.textAlignment = .right
            txfPassword.textAlignment = .right
        } else {
            txfDriverName.textAlignment = .left
            txfUsername.textAlignment = .left
            txfPassword.textAlignment = .left
        }
        if (isUpdate) {
            driverImage.loadImage(from: car?.userPhoto ?? .empty)
            btnSelectDriverPhoto.setImage(nil, for: .normal)
            txfDriverName.text = car?.fullname ?? .empty
            txfDriverName.textFieldDidBeginEditing(txfDriverName)
            txfUsername.text = car?.username ?? .empty
            txfUsername.textFieldDidBeginEditing(txfUsername)
        }
    }
    
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    // MARK: - Actions
    
    @IBAction func saveOnCLick(_ sender: UIButton) {
        print(cType, cPrand, cModel, cStyle, cFuel, cANumbers, cENumbers, cALetters, cELetters, car?.id, UserDefaults.standard.string(forKey: Misc.UserId), fullName, username)
        guard Gemo.gem.isConnected else {
            displayAlert(with: Localization.InternetConnection.localized())
            return
        }
        guard !fullName.isEmpty || !username.isEmpty || !password.isEmpty else {
            displayAlert(with: Localization.SomeDataReqiured.localized())
            return
        }
        guard password.characters.count >= 6 else {
            displayAlert(with: Localization.PasswordCount.localized())
            return
        }
        guard let photoData = photo else {
            displayAlert(with: Localization.ImageReqiured.localized())
            return
        }
        let link = (isUpdate) ? Misc.getUrl(for: "edit_car") : Misc.getUrl(for: "add_car")
        Gemo.gem.startSpinning()
        Alamofire.upload(multipartFormData: { [weak self] (fromData) in
            guard let this = self else { return }
            if (this.isUpdate) {
                fromData.append((this.car?.id ?? .empty).toData, withName: "id")
            }
            
            fromData.append(Misc.MainValue.toData, withName: Misc.MainKey)
            fromData.append((UserDefaults.standard.string(forKey: Misc.UserId) ?? .empty).toData, withName: "user_id")
            fromData.append(Localizer.instance.current.rawValue.toData, withName: Misc.LanguageKey)
            fromData.append((cType ?? .empty).toData , withName: "type_car")
            fromData.append((cPrand ?? .empty).toData, withName: "brand_car")
            fromData.append((cModel ?? .empty).toData, withName: "model_car")
            fromData.append((cStyle ?? .empty).toData, withName: "year_car")
            fromData.append((cFuel ?? .empty).toData, withName: "fuel_type")
            fromData.append((cANumbers ?? .empty).toData, withName: "num_ar")
            fromData.append((cENumbers ?? .empty).toData, withName: "num_en")
            fromData.append((cALetters ?? .empty).toData, withName: "ar_car")
            fromData.append((cELetters ?? .empty).toData, withName: "en_car")
            fromData.append(this.fullName.toData, withName: "full_name")
            fromData.append(this.username.toData, withName: "username")
            fromData.append(this.password.toData, withName: "password")
            fromData.append(photoData , withName: "photo_user", fileName: "\(Date().timeIntervalSinceNow).png", mimeType: "image/png")
            if let carImage = cPhoto, let carPhotoData = UIImageJPEGRepresentation(carImage, ImageQuality.medium.rawValue) {
                fromData.append(carPhotoData , withName: "photo_car", fileName: "\(Date().timeIntervalSinceNow).png", mimeType: "image/png")
            }
            
        }, to: link) { [weak self] (result) in
            switch (result) {
            case.failure(let error):
                Gemo.gem.stopSpinning()
                self?.displayAlert(with: error.localizedDescription)
                print("add car error: \(error)")
                
            case.success(request: let upload, streamingFromDisk: _, streamFileURL: _):
//                upload.responseString(completionHandler: { (caruserjson) in
//                    print("register user car json: \(caruserjson) ")
//                })
                upload.responseJSON(completionHandler: { [weak self] (response) in
                    guard let this = self else { return }
                    let error = response.error
                    if (error != nil) {
                        Gemo.gem.stopSpinning()
                        self?.displayAlert(with: error?.localizedDescription)
                        print("add car error2: \(error!)")
                        return
                    }
                    Gemo.gem.stopSpinning()
                    if (this.isUpdate) {
                        guard let json = response.result.value as? [string: any],
                            let success = json["result"] as? string,
                            let message = json["data"] as? string else {
                                self?.displayAlert(with: Localization.NoData.localized())
                                return
                        }
                        if (success == "true") {
                            Threads.main { [weak self] in
                                guard let this = self else { return }
                                if (this.isUpdate) {
                                    self?.displayAlert(with: message)
                                }
                                self?.navigationController?.popToRootViewController(animated: true)
                                cELetters = nil
                                cENumbers = nil
                                cALetters = nil
                                cANumbers = nil
                                cPhoto = nil
                                cType = nil
                                cPrand = nil
                                cModel = nil
                                cStyle = nil
                                cFuel = nil
                            }
                        } else {
                            self?.displayAlert(with: message)
                        }

                    } else { // add new
                        guard let json = (response.result.value as? [[string: any]])?.first,
                            let success = json["result"] as? string,
                            let message = json["data"] as? string else {
                                self?.displayAlert(with: Localization.NoData.localized())
                                return
                        }
                        if (success == "true") {
                            Threads.main { [weak self] in
                                guard let this = self else { return }
                                if (this.isUpdate) {
                                    self?.displayAlert(with: message)
                                }
                                self?.navigationController?.popToRootViewController(animated: true)
                                cELetters = nil
                                cENumbers = nil
                                cALetters = nil
                                cANumbers = nil
                                cPhoto = nil
                                cType = nil
                                cPrand = nil
                                cModel = nil
                                cStyle = nil
                                cFuel = nil
                            }
                        } else {
                            self?.displayAlert(with: message)
                        }

                    }
                    //print("car user data: \(response.result.value as? [string: any] ?? [:])")
                   
                })
                
            }
        }
    }
    
    @IBAction func chooseImageOnCLick(_ sender: UIButton) {
        openImagePicker(delegate: self)
    }
    

    
}


// MARK: - computed properties 

fileprivate extension CarUser {
    
    var fullName: string {
        return txfDriverName.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    var username: string {
        return txfUsername.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    var password: string {
        return txfPassword.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    var photo: Data? {
        guard let pic = driverImage.image else { return nil }
        return UIImageJPEGRepresentation(pic, ImageQuality.medium.rawValue)
    }
    
}


// MARK: - Image picker delegate functions

extension CarUser: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImage = UIImage()
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            selectedImage = editedImage
        } else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            selectedImage = image
        }
        driverImage.image = selectedImage
        btnSelectDriverPhoto.setImage(nil, for: .normal)
        picker.dismiss(animated: true)
    }
    
    internal func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
}


// MARK: - Text Field delegate functions

extension CarUser: UITextFieldDelegate {
  
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}























