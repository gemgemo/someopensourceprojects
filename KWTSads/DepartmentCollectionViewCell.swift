//
//  DepartmentCollectionViewCell.swift
//  KWTSads
//
//  Created by Mostafa on 8/12/16.
//  Copyright © 2016 Mostafa. All rights reserved.
//

import UIKit

class DepartmentCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var firstMainImg: UIImageView!
    @IBOutlet weak var firstMainLogo: UIImageView!
    @IBOutlet weak var firstMainLbk: UILabel!
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var bannerBtn: UIButton!
    @IBOutlet weak var secondMainImg: UIImageView!
    @IBOutlet weak var secondMainLogo: UIImageView!
    @IBOutlet weak var secondMainLbl: UILabel!
    @IBOutlet weak var thirdMainLogo: UIImageView!
    @IBOutlet weak var thirdMainImg: UIImageView!
    @IBOutlet weak var thirdMainLbl: UILabel!
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var secondBtn: UIButton!
    @IBOutlet weak var thirdBtn: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var upperViewH: NSLayoutConstraint!
    @IBOutlet weak var bottomViewH: NSLayoutConstraint!
   
}
