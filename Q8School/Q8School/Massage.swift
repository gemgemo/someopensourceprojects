//
//  Massage.swift
//  Q8School
//
//  Created by Mostafa on 12/14/16.
//  Copyright © 2016 Atiaf. All rights reserved.
//

import Foundation
public class Massages{
    
    var msg:String
    var seen: String
    var date: String
    var msgID:String
    var userName:String
    var userID: String
    var img: String
    
    init(msg:String,seen:String,date:String,msgID:String,userName:String,userID:String,img:String) {
        self.msg = msg
        self.seen = seen
        self.date = date
        self.msgID = msgID
        self.userName = userName
        self.userID = userID
        self.img = img
        
    }
}
