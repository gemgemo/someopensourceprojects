
import Foundation


final class DateTime
{
    internal var id, title: string?
    
    
    init(dictionary: [string: any]) {
        id = dictionary["id"] as? string
        title = dictionary["title"] as? string
    }
    
    internal class func getData(from link: string, page: UIViewController?, onComplete: @escaping([DateTime])-> void)-> void {
        UIApplication.showIndicator(by: true)
        HttpClient.request(link: link, method: .get) { (result, response, error) -> void in
            if (error != nil) {
                (page as? BaseController)?.showAlert(with: "", and: Constants.Misc.ErrorMessage)
                return
            }
            if let dictionaries = result as? [[string: any]] {
                onComplete(dictionaries.map { DateTime(dictionary: $0) })
            } else {
                print("no data")
            }
        }
    }
    
}
















