//

import UIKit

class MassagesViewC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    
    var selectedID: String!
    var dataFound: Bool!
    var images_cache = [String:UIImage]()
    var myID:String!
    var types:String!
    var err: String!
    var msgUserID =  [String]()
    
    @IBOutlet weak var myAdsImg: UIImageView!
    @IBOutlet weak var offerLbl: UILabel!
    @IBOutlet weak var myAdsLbl: UILabel!
    @IBOutlet weak var addAdsLbl: UILabel!
    @IBOutlet weak var callUslbl: UILabel!
    @IBOutlet weak var openMenu: UIBarButtonItem!
    @IBOutlet weak var massageCollection: UICollectionView!
    var massages = Array<Massages>()
    var type:String!
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        myID = UserDefaults.standard.string(forKey: "users_ID")
        getMassages(Settings.lang, myID)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        massageCollection.delegate = self
        massageCollection.dataSource = self
        self.title = "Massages"
        if Settings.lang == "ar"{
            massageCollection.semanticContentAttribute = .forceRightToLeft
            self.title = "المحادثات"
            callUslbl.text = "اتصل بنا"
            addAdsLbl.text = "اضف اعلانك"
            myAdsLbl.text = "اعلاناتي"
            offerLbl.text = "العروض"
            
        }
        if revealViewController() != nil {
            revealViewController().rightViewRevealWidth = 235
            openMenu.target = revealViewController()
            openMenu.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        types = UserDefaults.standard.string(forKey: "userType")
        if types == "1"
        {
            if Settings.lang == "ar"
            {
                myAdsLbl.text = "المفضلة"
                myAdsImg.image = UIImage(named: "fav")
                
            }
            else
            {
                myAdsLbl.text = "Favorite"
                myAdsImg.image = UIImage(named: "fav")
                
                
            }
        }
        
    }
    
    func getMassages(_ lang:String,_ userID:String) {
        massages.removeAll()
        let request = NSMutableURLRequest(url: URL(string:Settings.AllMassages)!)
        request.httpMethod = "POST"
        let postString = "lang=\(lang)&userId=\(userID)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        { data, response, error in
            guard error == nil && data != nil else {
                print("error=\(String(describing: error))")
                return
            }
            
            
//            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
//            print("responseString = \(String(describing: responseString))")
            if let responseData = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                    print("ddd\(json)")
                    self.dataFound = json["Success"] as? Bool
                    if self.dataFound! {
                        let data = json["Result"] as! [NSDictionary]
                        print("message", data)
                        for i in data {
                            let msg = i["msg"] as! String
                            let seen = i["seen"] as! String
                            let date = i["date_time"] as! String
                            let msgId = i["msgId"] as! String
                            let username = i["username"] as! String
                            let userId = i["userId"] as! String
                            let img = i["img"] as! String
                            self.msgUserID.append(i["userId"] as! String)
                            let massage = Massages(msg: msg, seen: seen, date: date, msgID: msgId, userName: username, userID: userId, img: img)
                            self.massages.append(massage)
                        }
                    }
                    DispatchQueue.main.async {
                        print(self.msgUserID)
                        self.massageCollection.reloadData()
                    }
                } catch {
                    print ("Could not Serialize")
                }
                
            }
            
        }
        task.resume()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        let size = self.massageCollection.frame.width
        
        return CGSize(width: size - 25, height: 120)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "massageCell", for: indexPath)
            as! MassagesCell
        if Settings.lang == "ar"
        {
            cell.semanticContentAttribute = .forceRightToLeft
            load_image(self.massages[indexPath.row].img, imageview: cell.Img)
            cell.Img.layer.cornerRadius = 0.5 * cell.Img.bounds.size.width
            cell.Img.clipsToBounds = true
            cell.msgView.layer.cornerRadius = 10
            cell.msgView.layer.masksToBounds = true
            cell.userName.text = self.massages[indexPath.row].userName
            cell.msg.text = self.massages[indexPath.row].msg
            let fullNameArr = self.massages[indexPath.row].date.characters.split{$0 == " "}.map(String.init)
            if (!fullNameArr.isEmpty) {
                cell.dateLbl.text = fullNameArr[0]
                cell.timeLbl.text = fullNameArr[1]
            }
            cell.seenLbl.text = self.massages[indexPath.row].seen
            cell.deleteBtn.tag = indexPath.row
        } else {
            load_image(self.massages[indexPath.row].img, imageview: cell.Img)
            cell.Img.layer.cornerRadius = 0.5 * cell.Img.bounds.size.width
            cell.Img.clipsToBounds = true
            cell.msgView.layer.cornerRadius = 10
            cell.msgView.layer.masksToBounds = true
            cell.userName.text = self.massages[indexPath.row].userName
            cell.msg.text = self.massages[indexPath.row].msg
            let fullNameArr = self.massages[indexPath.row].date.characters.split{$0 == " "}.map(String.init)
            if (!fullNameArr.isEmpty) {
                cell.dateLbl.text = fullNameArr[0]
                cell.timeLbl.text = fullNameArr[1]
            }
            cell.seenLbl.text = self.massages[indexPath.row].seen
            cell.deleteBtn.tag = indexPath.row
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.massages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SendMassageVC") as! SendMassageVC
        vc.selectedID = self.massages[indexPath.row].userID
        vc.selectedName = self.massages[indexPath.row].userName
        vc.selectedImage = self.massages[indexPath.row].img
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func load_image(_ link:String, imageview:UIImageView)
    {
        
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.timeoutInterval = 10
        
        
        let task = session.dataTask(with: url, completionHandler:
        { (data :Data?, response :URLResponse?, error:Error?) in
            
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                
                return
            }
            
            
            var image = UIImage(data: data!)
            
            if (image != nil)
            {
                
                
                func set_image()
                {
                    self.images_cache[link] = image
                    imageview.image = image
                }
                
                
                DispatchQueue.main.async(execute: set_image)
                
            }
            
        })
        
        task.resume()
        
    }
    
    func displayAlerts(_ Massage: String)
    {
        var t = ""
        var b = ""
        
        if Settings.lang == "ar"{
            t = "تنبيه"
            b = "إغلاق"
        }
        else{
            t = "Alert"
            b = "Close"
        }
        
        let myAlert = UIAlertController(title: t, message:Massage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }
    
    
    
    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MainPageViewController") as! MainPageViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func CallUsBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CallUsVc") as! CallUsVc
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func addAdsBtn(_ sender: Any) {
        if types == "1"
        {
            if Settings.lang == "ar"
            {
                let msg = "انت طالب لا يمكنك اضافة اعلان"
                self.displayAlerts(msg)
                
            }
            else
            {
                let msg = "you'r student can't add Ads"
                self.displayAlerts(msg)
                
            }
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AddAdsVc") as! AddAdsVc
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        
    }
    
    
    @IBAction func myAdsBtn(_ sender: Any) {
        if types == "1"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyFavoriteVC") as! MyFavoriteVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyAdsVC") as! MyAdsVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        
        
        
    }
    
    @IBAction func offerBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AdsPageViewController") as! AdsPageViewController
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func deleteBtn(_ sender: UIButton) {
        var t = ""
        var b = ""
        var c = ""
        var d = ""
        if Settings.lang == "en"{
            t = "Alert"
            b = "Close"
            c = "Do you want to delete this Message?"
            d = "Ok"
        } else {
            t = "تنبيه"
            b = "إغلاق"
            c = "هل تريد حذف الرسالة؟"
            d = "موافق"
        }
        let myAlert = UIAlertController(title: t, message:c, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: d, style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
            let request = NSMutableURLRequest(url: URL(string: Settings.DeleteMsg)!)
            request.httpMethod = "POST"
            let postString = "lang=\(Settings.lang)&msgFrom=\(self.myID!)&msgTo=\(self.massages[sender.tag].userID)"
            print(postString)
            request.httpBody = postString.data(using: String.Encoding.utf8)
            let task =    URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {
                    print("error=\(String(describing: error))")
                    return
                }
                
                let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("responseString = \(String(describing: responseString))")
                
                if let responseData = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
                        print("ddd\(json)")
                        self.dataFound = json["Success"] as? Bool
                        if self.dataFound!
                        {
                            
                            self.err = json["Message"] as! String
                            
                            
                        }
                        else
                        {
                            self.err = json["Message"] as! String
                        }
                        DispatchQueue.main.async{
                            if self.dataFound!
                            {
                                self.displayAlerts(self.err!)
                                let buttonIndex:Int = sender.tag
                                self.massages.remove(at: buttonIndex)
                                let indexPath = IndexPath(item: buttonIndex, section: 0)
                                self.massageCollection.deleteItems(at: [indexPath])
                                self.massageCollection.reloadData()
                            }
                            else
                            {
                                self.displayAlerts(self.err!)
                                
                            }
                        }
                        
                    } catch {
                        print ("Could not Serialize")
                    }
                }
            }
            task.resume()
        })
        let cancelAction = UIAlertAction(title: b, style: UIAlertActionStyle.destructive, handler: nil)
        myAlert.addAction(okAction)
        myAlert.addAction(cancelAction)
        self.present(myAlert, animated: true)
        
    }
}
