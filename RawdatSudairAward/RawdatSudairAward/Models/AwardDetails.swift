
import Foundation

class AwardDetails
{
    
    internal var content: string?
    
    init(dictionay: [string: any]) {
        content = dictionay["content"] as? string
    }
    
    internal class func getData(by link: string, viewController: UIViewController?, onComplete: @escaping (AwardDetails)-> void)-> void {
        UIApplication.showIndicator(by: true)
        HttpClient.request(link: link, method: .get) { (data, response, error) -> void in
            if (error != nil) {
                (viewController as? BaseController)?.showAlert(with: "title", and: Constants.Misc.ErrorMessage)
                UIApplication.showIndicator(by: false)
                return
            }
            
            if let object = (data as? [[string: any]])?.first {
                onComplete(AwardDetails(dictionay: object))
            } else {
                if let barcodeValue = (data as? [string: any]) {
                    onComplete(AwardDetails(dictionay: barcodeValue))
                } else {
                    print("nix data")                    
                }
                UIApplication.showIndicator(by: false)
            }
        }
    }
    
}




typealias Barcode = AwardDetails





