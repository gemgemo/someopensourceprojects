
import UIKit
import Localize_Swift

final class EndedMissionMessage: BaseController
{
    
    // MARK: - Constants
    
    // MARK: - Variables
    
    internal var id, whoNum, phone, priceType, cId, name, carTypeA, carTypeE, carId, carColor, price, avatar: string?
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var icon: UIImageView! {
        didSet {
            icon.rounded()
        }
    }
    @IBOutlet fileprivate weak var lblName: UILabel!
    @IBOutlet fileprivate weak var lblCarType: UILabel!
    @IBOutlet fileprivate weak var lblCarId: UILabel!
    @IBOutlet fileprivate weak var lblCarColor: UILabel!
    @IBOutlet fileprivate weak var lblPriceTitle: UILabel!
    @IBOutlet fileprivate weak var lblPrice: UILabel!
    @IBOutlet fileprivate weak var btnMissionEnded: Button!
    @IBOutlet fileprivate weak var middlePanal: UIView!
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.superview?.layer.cornerRadius = 0.0
        updateUi()
    }
    
    internal override func updateNavgationBar() {
        navigationController?.navigationBar.isHidden = true
    }
    
    
    // MARK: - Actions
    
    @IBAction private func endMissionOnClick(_ sender: UIButton) {
        dismiss(animated: true) {
            DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
                NotificationCenter.default.post(name: Constants.Misc.RateView, object: nil)
            }
        }
    }
    
    
}




// MARK: - Helper functions

extension EndedMissionMessage
{
    
    fileprivate func updateUi()-> void {
        lblTitle.text = "endedmissionmessagelan".localized()
        btnMissionEnded.setTitle("endedmissionmessagebuttonlan".localized(), for: .normal)
        lblPriceTitle.text = "messagejobpricetitlelan".localized()
        if let avatarLink = avatar {
            icon.loadImage(from: avatarLink, onComplete: nil)
        }
        lblName.text = name ?? .empty
        lblPrice.text = price ?? .empty
        lblCarId.text = carId ?? .empty
        lblCarType.text = ((Localize.currentLanguage() == Constants.Language.Arabic) ? carTypeA : carTypeE) ?? .empty
        lblCarColor.text = carColor ?? .empty
        middlePanal.isHidden = isRateCompany
    }
    
    
}





















