
import UIKit

final class SignUpVC: UIViewController,UITextFieldDelegate//,UIImagePickerControllerDelegate, UINavigationControllerDelegate
{

    @IBOutlet weak var idBottom: UIView!
    //@IBOutlet weak var idImg: UIImageView!
    @IBOutlet var signUpView: UIView!
    @IBOutlet weak var emialV: UIView!
    @IBOutlet weak var idV: UIView!
    @IBOutlet weak var mobileV: UIView!
    @IBOutlet weak var nameV: UIView!
    //@IBOutlet weak var IDLbl: UILabel!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet private weak var btnHome: UIButton!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var red2: UIView!
    @IBOutlet weak var mobileTxt: UITextField!
    @IBOutlet weak var red1: UIView!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var red: UIView!
    @IBOutlet weak var txfIdentityNumber: UITextField!
    @IBOutlet weak var idNumberRed: UIView!
    
    
    internal var isInitial = false
    var keyboardLefted = false
    let defaults = UserDefaults.standard
    var phone: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        UIApplication.shared.statusBarStyle = .lightContent
        
        self.title = "Enter your data"
        continueBtn.layer.cornerRadius = 10
        continueBtn.layer.masksToBounds = true
        btnHome.layer.cornerRadius = 10
        btnHome.layer.masksToBounds = true
        //idImg.layer.cornerRadius = 10
        //idImg.layer.masksToBounds = true
        red.layer.cornerRadius = red.layer.bounds.width / 2
        red.clipsToBounds = true
        idNumberRed.layer.cornerRadius = idNumberRed.bounds.width/2
        idNumberRed.layer.masksToBounds = true
        red1.layer.cornerRadius = red.layer.bounds.width / 2
        red1.clipsToBounds = true
        red2.layer.cornerRadius = red.layer.bounds.width / 2
        red2.clipsToBounds = true
        nameTxt.delegate = self
        mobileTxt.delegate = self
        emailTxt.delegate = self
        txfIdentityNumber.delegate = self
        self.phone = UserDefaults.standard.string(forKey: "phone")!
        mobileTxt.text! = self.phone!

        if Settings.lang == "ar"
        {
            self.title = "أدخل البيانات"
            emailTxt.placeholder = "البريد الإلكتروني"
            nameTxt.placeholder = "الاسم"
            mobileTxt.placeholder = "رقم الجوال"
            txfIdentityNumber.placeholder = "رقم الهوية الوطنية"
            //IDLbl.text = "صورة الهوية الوطنية"
            continueBtn.setTitle("متابعة", for: .normal)
            nameV.semanticContentAttribute = .forceRightToLeft
            mobileV.semanticContentAttribute = .forceRightToLeft
            emialV.semanticContentAttribute = .forceRightToLeft
            idV.semanticContentAttribute = .forceRightToLeft
            //IDLbl.textAlignment = .center
            nameTxt.textAlignment = .right
            mobileTxt.textAlignment = .right
            emailTxt.textAlignment = .right
            txfIdentityNumber.textAlignment = .right
        }

    }
    func AlertMessage(_ Message:String)
    {
        var t = ""
        var b = ""
        
        if Settings.lang == "ar"{
            t = "تنبيه"
            b = "إغلاق"
        }
        else
        {
            t = "Alert"
            b = "Close"
        }
            let myAlert = UIAlertController(title: t, message:Message, preferredStyle: UIAlertControllerStyle.alert);
            let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler:nil)
            myAlert.addAction(okAction);
            self.present(myAlert, animated: true, completion: nil)
    }

    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        continueBtn.setTitle("متابعة", for: .normal)
        continueBtn.isHidden = isInitial
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == nameTxt {
            red.isHidden = true

        } else if textField == mobileTxt {
            red1.isHidden = true
        } else if (textField == txfIdentityNumber) {
            idNumberRed.isHidden = true
        } else {
            red2.isHidden = true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if nameTxt.text!.isEmpty == true
        {
            red.isHidden = false

        }
        if mobileTxt.text!.isEmpty == true
        {
            red1.isHidden = false

        }
        if emailTxt.text!.isEmpty == true
        {
            red2.isHidden = false

        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    /*func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
            idImg.image = selectedImage
            self.dismiss(animated: true, completion: nil)
        }*/


    @IBAction private func backToHomeOnClick(_ sender: UIButton) {
        if (Settings.isConnected) {
            if emailTxt.text!.isValidEmail() && !nameTxt.text!.isEmpty && !mobileTxt.text!.isEmpty && !txfIdentityNumber.text!.isEmpty {
                //let imageData = UIImageJPEGRepresentation(idImg.image!, 0.25)
                self.defaults.setValue(nameTxt.text!, forKey: "name")
                self.defaults.setValue(mobileTxt.text!, forKey: "mobile")
                self.defaults.setValue(emailTxt.text!, forKey: "email")
                self.defaults.setValue(txfIdentityNumber.text!, forKey: "image")
                self.defaults.setValue("1", forKey: "check")
                if let rootPage = storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController {
                    navigationController?.pushViewController(rootPage, animated: true)
                }
            } else {
                if Settings.lang == "ar" {
                    let msgA = "من فضلك أدخل البيانات صحيحة"
                    self.AlertMessage(msgA)
                } else {
                    let msg = "please Fill all data"
                    self.AlertMessage(msg)
                }
            }
        } else {
            dialogUser(with: "لا يوجد انترنت")
        }
    }
    
    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    /*@IBAction func imageBtn(_ sender: UIButton) {
        let alert = UIAlertController(title: "Select Image", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            let ImagePicker = UIImagePickerController()
            ImagePicker.delegate = self
            ImagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self.present(ImagePicker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            let ImagePicker = UIImagePickerController()
            ImagePicker.delegate = self
            ImagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.present(ImagePicker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
 
    }*/

    @IBAction func continueBtn(_ sender: UIButton) {
       register()
    }
    
    private func register()-> Void {        
        guard let idNum = txfIdentityNumber.text, idNum.characters.count == 10 else {
            if Settings.lang == "ar" {
                let msgA = "رقم هوية غير صحيح"
                self.AlertMessage(msgA)
            } else {
                let msg = "wrong id number"
                self.AlertMessage(msg)
            }
            return
        }
        if emailTxt.text!.isValidEmail() && !nameTxt.text!.isEmpty && !mobileTxt.text!.isEmpty && !txfIdentityNumber.text!.isEmpty
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ComplainVC") as! ComplainVC
            vc.selectedName = nameTxt.text!
            let NumberStr: String = mobileTxt.text!
            let Formatter: NumberFormatter = NumberFormatter()
            Formatter.locale = NSLocale(localeIdentifier: "EN") as Locale!
            let final = Formatter.number(from: NumberStr)
            vc.selectedMobile = String(describing: final!)
            vc.selectedEmail = emailTxt.text!            
            vc.idNumber = txfIdentityNumber.text ?? ""
            self.defaults.setValue(nameTxt.text!, forKey: "name")
            self.defaults.setValue(mobileTxt.text!, forKey: "mobile")
            self.defaults.setValue(emailTxt.text!, forKey: "email")
            self.defaults.setValue(txfIdentityNumber.text!, forKey: "image")
            self.defaults.setValue("1", forKey: "check")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            if Settings.lang == "ar"
            {
                let msgA = "من فضلك أدخل البيانات صحيحة"
                self.AlertMessage(msgA)
            }
            else
            {
                let msg = "please Fill all data"
                self.AlertMessage(msg)
            }
        }
    }
    
}
extension String {
    func isValidEmail() -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil
        } catch {
            return false
        }
    }
}

