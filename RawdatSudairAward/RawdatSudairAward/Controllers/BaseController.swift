
import UIKit

import Localize_Swift

class BaseController: UIViewController
{
    // MARK: - Constants
    
    //MARK: - Variables
    
    fileprivate lazy var indicator: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.hidesWhenStopped = true
        spinner.translatesAutoresizingMaskIntoConstraints = false
        return spinner
    }()
    
    fileprivate lazy var spinnerView: UIVisualEffectView = {
        let visualView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: UIBlurEffect(style: .light)))
        visualView.translatesAutoresizingMaskIntoConstraints = false
        return visualView
    }()
    
    fileprivate lazy var alertContainer: UIView = {
        let iv = UIView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.backgroundColor = .darkGray
        iv.alpha = 0
        return iv
    }()
    
    fileprivate lazy var alertView: UIView = {
        let alert = UIView()
        alert.translatesAutoresizingMaskIntoConstraints = false
        alert.backgroundColor = .white
        alert.layer.cornerRadius = 3.0
        alert.clipsToBounds = true
        return alert
    }()
    
    fileprivate lazy var lblTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.textAlignment = .center
        return label
    }()
    
    fileprivate lazy var lblMessage: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.textAlignment = .center
        return label
    }()
    
    fileprivate lazy var btnDone: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Done", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.tintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        return btn
    }()

    
    // MARK: - Outlets
    
    // MARK: - Overridden functions
    
    internal func phoneLanguage()-> string {
        return NSLocale.current.languageCode ?? string.empty
    }
    
    //MARK: - Actions
    
    internal func goBack(_ sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc fileprivate func doneAlertViewOnCLick(_ sender: UIButton) {
        print("done button tapped")
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options: .curveEaseIn, animations: { [weak self] in
            guard let this = self else { return }
            this.alertView.transform = CGAffineTransform().scaledBy(x: 0.0, y: 0.0)
            this.alertContainer.alpha = 0.0
        }) { [weak self] (isCompleted) in
            guard let this = self else { return }
            this.lblMessage.removeFromSuperview()
            this.lblTitle.removeFromSuperview()
            this.btnDone.removeFromSuperview()
            this.alertView.removeFromSuperview()
            this.alertContainer.removeFromSuperview()
        }
    }
    
    internal func updateNavigationBarDirections()-> void {
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            navigationController?.navigationBar.semanticContentAttribute = .forceLeftToRight
        } else {
            navigationController?.navigationBar.semanticContentAttribute = .forceRightToLeft
        }
    }
    
    internal func getBackImage()-> UIImage {
        return (Localize.currentLanguage() == Constants.Language.Arabic) ? #imageLiteral(resourceName: "back") : #imageLiteral(resourceName: "back-l")
    }
    
    
}


// MARK: - Functions 

extension BaseController
{
    
    internal func changeLanguage()-> void {
        let alertController = UIAlertController(title: "تغيير اللغه", message: "Change Language", preferredStyle: .actionSheet)
        alertController.popoverPresentationController?.sourceView = view
        alertController.popoverPresentationController?.sourceRect = Rect(x: view.bounds.width/2-100, y: view.bounds.height/2, width: 0, height: 0)
        alertController.popoverPresentationController?.permittedArrowDirections = .init(rawValue: 0)
        alertController.addAction(UIAlertAction(title: "عربي", style: .default, handler: { (action) in
            Localize.setCurrentLanguage(Constants.Language.Arabic)
        }))
        
        alertController.addAction(UIAlertAction(title: "English", style: .default, handler: { (action) in
            Localize.setCurrentLanguage(Constants.Language.English)
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    
    internal func share(this items: [any])-> void {
        let activityController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        activityController.excludedActivityTypes = [.copyToPasteboard, .mail, .message, .postToFacebook, .postToFlickr, .postToTencentWeibo, .openInIBooks, .addToReadingList, .airDrop, .postToVimeo, .postToWeibo, .print, .saveToCameraRoll]
        activityController.popoverPresentationController?.sourceRect = Rect(x: 0, y: 0, width: 0, height: 0)
        activityController.popoverPresentationController?.sourceView = view
        present(activityController, animated: true, completion: nil)
    }
    
    
//    internal func handle(this error: Error?)-> void {
//        print(error?.localizedDescription ?? string.nixStringMessage)
//    }
    
//    internal func dialog(with message: string)-> void {
//        print(message)
//        (self as? BaseController)?.showAlert(with: "Warning!", and: message)
//    }
    
    internal func startSpin()-> void {
        DispatchQueue.main.async { [weak self] in
            guard let this = self else { return }
            this.view.addSubview(this.spinnerView)
            this.spinnerView.addSubview(this.indicator)
            
            this.spinnerView.topAnchor.constraint(equalTo: this.view.topAnchor, constant: 0).isActive = true
            this.spinnerView.rightAnchor.constraint(equalTo: this.view.rightAnchor, constant: 0).isActive = true
            this.spinnerView.bottomAnchor.constraint(equalTo: this.view.bottomAnchor, constant: 0).isActive = true
            this.spinnerView.leftAnchor.constraint(equalTo: this.view.leftAnchor, constant: 0).isActive = true
            
            this.indicator.centerXAnchor.constraint(equalTo: this.spinnerView.centerXAnchor).isActive = true
            this.indicator.centerYAnchor.constraint(equalTo: this.spinnerView.centerYAnchor).isActive = true
            
            this.indicator.startAnimating()
        }
    }
    
    internal func stopSpin()-> void {
        DispatchQueue.main.async { [weak self] in
            guard let this = self else { return }
            this.indicator.stopAnimating()
            this.indicator.removeFromSuperview()
            this.spinnerView.removeFromSuperview()
        }
        UIApplication.showIndicator(by: false)
    }
    
    internal func showAlert(with title: string, and message: string)-> void {
        DispatchQueue.main.async { [weak self] in
            guard let this = self else { return }
            
            this.view.addSubview(this.alertContainer)
            this.alertContainer.addSubview(this.alertView)
            
            this.alertContainer.topAnchor.constraint(equalTo: this.view.topAnchor, constant: 0).isActive = true
            this.alertContainer.rightAnchor.constraint(equalTo: this.view.rightAnchor, constant: 0).isActive = true
            this.alertContainer.bottomAnchor.constraint(equalTo: this.view.bottomAnchor, constant: 0).isActive = true
            this.alertContainer.leftAnchor.constraint(equalTo: this.view.leftAnchor, constant: 0).isActive = true
            
            this.alertView.widthAnchor.constraint(equalToConstant: this.view.bounds.width*0.70).isActive = true
            this.alertView.heightAnchor.constraint(equalToConstant: this.view.bounds.height*0.23).isActive = true
            this.alertView.centerXAnchor.constraint(equalTo: this.alertContainer.centerXAnchor).isActive = true
            this.alertView.centerYAnchor.constraint(equalTo: this.alertContainer.centerYAnchor).isActive = true
            
            this.lblTitle.text = "Warning!"
            
            this.lblMessage.text = message
            
            this.alertView.addSubview(this.lblTitle)
            this.alertView.addSubview(this.lblMessage)
            this.alertView.addSubview(this.btnDone)
            
            this.lblTitle.topAnchor.constraint(equalTo: this.alertView.topAnchor, constant: 8).isActive = true
            this.lblTitle.centerXAnchor.constraint(equalTo: this.alertView.centerXAnchor).isActive = true
            
            this.btnDone.bottomAnchor.constraint(equalTo: this.alertView.bottomAnchor, constant: -8).isActive = true
            this.btnDone.centerXAnchor.constraint(equalTo: this.alertView.centerXAnchor, constant: 0).isActive = true
            
            this.lblMessage.centerXAnchor.constraint(equalTo: this.alertView.centerXAnchor).isActive = true
            this.lblMessage.centerYAnchor.constraint(equalTo: this.alertView.centerYAnchor).isActive = true
            
            this.btnDone.addTarget(self, action: #selector(this.doneAlertViewOnCLick(_:)), for: .touchUpInside)
            this.view.layoutIfNeeded()
            this.alertView.transform = CGAffineTransform().scaledBy(x: 0, y: 0)
            // show alert with animation
            UIView.animate(withDuration: 0.6, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.7, options: .curveEaseOut, animations: { [weak self] in
                guard let this = self else { return }
                this.alertContainer.alpha = 1.0
                this.alertContainer.backgroundColor = Color.lightGray.withAlphaComponent(0.7)
                this.alertView.transform = CGAffineTransform.identity
                }, completion: { (isFinished) in
                    
            })
            
        }
    }
    
    
    
}































