
import UIKit
import Gemo

class EditPassword: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    
    // MARK: - Outlets
    
    
    @IBOutlet weak var txfOldPassword: TextField!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var txfNewPassword: TextField!
    @IBOutlet weak var txfReNewPassword: TextField!
    @IBOutlet weak var btnSave: UIButton!
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        super.updateUi()
        setupNavigationBar(isLogo: false, isMainPage: false, title: Localization.UpdatePassword.localized())
        btnForgotPassword.setTitle(Localization.ForgotPassword.localized(), for: .normal)
        btnSave.setTitle(Localization.Save.localized(), for: .normal)
        btnSave.setCorner(radius: 4)
        
    }
    
    
    
    // MARK: - Actions
    
    @IBAction func saveOnCLick(_ sender: UIButton) {
        guard Gemo.gem.isConnected else {
            displayAlert(with: Localization.InternetConnection.localized())
            return
        }
        guard !oldPassword.isEmpty else {
            displayAlert(with: Localization.PasswordReqiured.localized())
            return
        }
        guard !password.isEmpty else {
            displayAlert(with: Localization.PasswordReqiured.localized())
            return
        }
        guard !confirmedPassword.isEmpty else {
            displayAlert(with: Localization.PasswordReqiured.localized())
            return
        }
        guard password == confirmedPassword else {
            displayAlert(with: Localization.PasswordNotConfirmed.localized())
            return
        }
        guard password.characters.count >= 6 || confirmedPassword.characters.count >= 6 else {
            displayAlert(with: Localization.PasswordCount.localized())
            return
        }
        let paramters: Dictionary<string, any> = [
            Misc.MainKey: Misc.MainValue,
            Misc.LanguageKey: Localizer.instance.current.rawValue,
            "user_id": UserDefaults.standard.string(forKey: Misc.UserId) ?? .empty,
            "current_pass": oldPassword,
            "password": password,
            "password_again": confirmedPassword
        ]
        Gemo.gem.startSpinning()
        Http.request(link: Misc.getUrl(for: "edit_pass"), method: .post, parameters: paramters)
            .response { [weak self] (response) in
                let error = response.error
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("edit password error: \(error!)")
                    self?.displayAlert(with: error?.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
                guard let json = (response.result as? [Dictionary<string, any>])?.first,
                    let success = json["result"] as? string,
                    let message = json["data"] as? string else {
                        self?.displayAlert(with: Localization.NoData.localized())
                        return
                }
                if (success == "true") {
                    Threads.main { [weak self] in
                        self?.navigationController?.popViewController(animated: true)
                    }
                } else {
                    self?.displayAlert(with: message)
                }
        }
    }
    
    @IBAction func forgotPasswordOnCLick(_ sender: UIButton) {
        let alertController = UIAlertController(title: Localization.ForgotPassword.localized(), message: .empty, preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.placeholder = Localization.Email.localized()
            textField.textAlignment = (Localizer.instance.current == .arabic) ? .right : .left
            textField.keyboardType = .emailAddress
        }
        alertController.addAction(UIAlertAction(title: Localization.Send.localized(), style: .default, handler: { [weak self] (action) in
            let email = alertController.textFields?[0].text?.trimmingCharacters(in: .whitespaces) ?? .empty
            guard email.isEmail else {
                self?.displayAlert(with: Localization.InvaildEmail.localized())
                return
            }
            Gemo.gem.startSpinning()
            let paramters: Dictionary<String, any> = [
                Misc.MainKey: Misc.MainValue,
                Misc.LanguageKey: Localizer.instance.current.rawValue,
                "email": email
            ]
            Http.request(link: Misc.getUrl(for: "recover"), method: .post, parameters: paramters)
                .response { [weak self] (response) in
                    let error = response.error
                    if (error != nil) {
                        Gemo.gem.stopSpinning()
                        print("reset email error: \(string(describing: error))")
                        self?.displayAlert(with: error?.localizedDescription)
                        return
                    }
                    Gemo.gem.stopSpinning()
                    guard let json = (response.result as? [Dictionary<string, any>])?.first, let message = json["data"] as? string else {
                        self?.displayAlert(with: Localization.NoData.localized())
                        return
                    }
                    self?.displayAlert(with: message)
            }
        }))
        present(alertController, animated: true)
    }
    

    
}


// MARK: - Computed properties

fileprivate extension EditPassword {
    
    var oldPassword: string {
        return txfOldPassword.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    var password: string {
        return txfNewPassword.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    var confirmedPassword: string {
        return txfReNewPassword.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
}
























