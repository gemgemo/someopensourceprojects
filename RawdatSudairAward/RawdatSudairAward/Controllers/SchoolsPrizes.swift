
import UIKit
import Localize_Swift

final class SchoolsPrizes: BaseController
{

    // MARK: - Constants
    
    //MARK: - Variables
    
    fileprivate var dataSource = Array<School>() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.schools.reloadData()
            }
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var schools: UICollectionView! {
        didSet {
            schools.delegate = self
            schools.dataSource = self
        }
    }
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
        loadData()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    //MARK: - Actions

}


// MARK: - Helper functions

extension SchoolsPrizes
{
    
    fileprivate func configureCollectionView()-> void {
        schools.register(Constants.Nib.Schools, forCellWithReuseIdentifier: Constants.ReuseIdentifier.Schools)
    }
    
    fileprivate func loadData()-> void {
        School.getData(by: Constants.BackendURLs.shared.Schools, viewController: self) { [weak self] (result) -> void in
            self?.dataSource = result
            UIApplication.showIndicator(by: false)
        }
    }
    
}


extension SchoolsPrizes: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    
    internal func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ReuseIdentifier.Schools, for: indexPath) as? SchoolCell {
            cell.school = dataSource[indexPath.item]
            return cell
        }
        return UICollectionViewCell()
    }
    
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.item)
        if let imagesScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.AwardListImagesDetails) as? AwardListImagesDetails {
            imagesScreen.school = dataSource[indexPath.item]
            selectedTab = (Localize.currentLanguage() == Constants.Language.Arabic) ? 0 : 1
            navigationController?.pushViewController(imagesScreen, animated: true)
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return Size(width: collectionView.bounds.width, height: 280.0)
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return Insets.zero
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}





























