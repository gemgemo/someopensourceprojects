
import UIKit
import Gemo

class AboutApp: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var txtInfo: UITextView! { didSet { txtInfo.text = .empty }}
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    internal override func updateUi() {
        super.updateUi()
        setupNavigationBar(isLogo: false, isMainPage: true, title: Localization.AboutApp.localized())
    }
    
    
    // MARK: - Actions
    

}


// MARK: - Helper function

fileprivate extension AboutApp {
    
    func loadData() -> void {
        guard Gemo.gem.isConnected else {
            displayAlert(with: Localization.InternetConnection.localized())
            return
        }
        Gemo.gem.startSpinning()
        Http.request(link: Misc.getUrl(for: "about_us"), method: .post, parameters: [Misc.MainKey: Misc.MainValue, Misc.LanguageKey: Localizer.instance.current.rawValue])
            .response { [weak self] (response) in
                let error = response.error
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("about app error: \(string(describing: error))")
                    self?.displayAlert(with: error?.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
                guard let json = response.result as? Dictionary<string, any>, let content = json["content"] as? string else {
                    self?.displayAlert(with: Localization.NoData.localized())
                    return
                }
                Threads.main { [weak self] in
                    self?.txtInfo.text = content
                }
        }
    }
}
























