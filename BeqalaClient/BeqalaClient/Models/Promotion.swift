
import Foundation

internal final class Promotion: Mapper<Any>
{
    
    internal var id, categoryId, shopId, title, price, quantity, photo1, photo2, photo3, photo4: string?
    
    internal override var description: String {
        return "\n id: \(string(describing: id)), title: \(string(describing: title)) \n"
    }
    
    internal override func mapping(mapper: Mapper<Any>) {
        id = mapper["id"]?.text
        categoryId = mapper["id_cat"]?.text
        shopId = mapper["id_matjer"]?.text
        title = mapper["title"]?.text
        price = mapper["price"]?.text
        quantity = mapper["quantity"]?.text
        photo1 = mapper["photo_1"]?.text
        photo2 = mapper["photo_2"]?.text
        photo3 = mapper["photo_3"]?.text
        photo4 = mapper["photo_4"]?.text
    }
    
    
    internal class func getData(_ complation: @escaping([Promotion])->())-> void {
        let parameters: Dictionary<string, string> = [
            Constants.Misc.MainKey: Constants.Misc.MainValue
        ]
        HttpClient.request(link: Constants.Urls.Promotions, method: .post, parameters: parameters) { (data, response, error) -> void in
            if (error != nil) {
                print("get promotion data for slider")
                return
            }
            guard let json = data as? Dictionary<string, any>, let value = json["all_data"] as? [Dictionary<string, any>] else {
                print("null promotions for slider")
                return
            }
            complation(value.map { Promotion(JSON: $0) })
        }
    }
    
    
    internal class func getData(page: BaseController, _ complation: @escaping([Promotion])->())-> void {
        let parameters: Dictionary<string, string> = [
            Constants.Misc.MainKey: Constants.Misc.MainValue
        ]
        HttpClient.request(link: Constants.Urls.Promotions, method: .post, parameters: parameters) { (data, response, error) -> void in
            if (error != nil) {
                Gemo.gem.stopSpinning()
                print("get promotion data")
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription, in: page.view)
                return
            }
            guard let json = data as? Dictionary<string, any>, let value = json["all_data"] as? [Dictionary<string, any>] else {
                Gemo.gem.stopSpinning()
                print("null promotions")
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized(), in: page.view)
                return
            }
            complation(value.map { Promotion(JSON: $0) })
        }

    }
    
    
}








































