
import UIKit
import Gemo


class MyPoints: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var lblUnderBuildUp: UILabel!
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        super.updateUi()
        setupNavigationBar(isLogo: false, isMainPage: true, title: Localization.MyPoints.localized())
        lblUnderBuildUp.text = Localization.UnderBuildUp.localized()
        
    }
    
    
    // MARK: - Actions

}



























