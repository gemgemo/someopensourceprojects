
import UIKit

class MembersVC: UIViewController,
    UICollectionViewDelegate,
    UICollectionViewDataSource
{
    
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var imageView: UIView!
    var members: Array<Members> = Array<Members>()
    var images_cache = [String:UIImage]()
    
    @IBOutlet weak var loadingLbl: UILabel!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var shadowView: UIView!
    var selectedImage:String!
    
    @IBOutlet weak var memberCollection: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        memberCollection.delegate = self
        memberCollection.dataSource = self
        loadingView.isHidden = true
        shadowView.isHidden = true
        imageView.isHidden = true
        if (Settings.isConnected) {
            GetMembers()
        } else {
            dialogUser(with: "لا يوجد انترنت")
        }
        self.title = "Council Members"
        if Settings.lang == "ar"
        {
            loadingLbl.text = "جاري التحميل..."
            self.title = "أعضاء المجلس"
        }
        
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(_:))))
    }
    
    func GetMembers() {
        loadingView.isHidden = false
        shadowView.isHidden = false
        loadingView.layer.cornerRadius = 10
        loadingView.layer.masksToBounds = true
        loadingActivity.startAnimating()
        let urlString = Settings.Members
        let sessions = URLSession.shared
        let urls = URL(string: urlString)!
        sessions.dataTask(with: urls, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
                
                if let responseData = data {
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [NSDictionary]
                        //print("json1\(json)")
                        for i in json
                        {
                            var name1 = ""
                            var postion = ""
                            if Settings.lang == "ar"
                            {
                                name1 = "name"
                                postion = "position"
                            }
                            else
                            {
                                name1 = "name_en"
                                postion = "position_en"
                                
                            }
                            
                            let id = i["id"] as! String
                            let name = i[name1] as! String
                            let postions = i[postion] as! String
                            let img = i["photo"] as! String
                            
                            let member = Members(id: id, name: name, postion: postions, photo: img)
                            self.members.append(member)
                            
                        }
                        
                        
                        DispatchQueue.main.async
                            {
                                self.loadingView.isHidden = true
                                self.shadowView.isHidden = true
                                self.loadingActivity.stopAnimating()
                                self.memberCollection.reloadData()
                        }
                    }
                        
                    catch
                    {
                        print ("Could not Serialize")
                    }
                    
                    
                    
                    
                    
                }
                
        }).resume()
        
        
    }
    
    @objc private func imageTapped(_ gesture: UITapGestureRecognizer) {
        print("image tapped")
        imageView.isHidden = true
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "memberCell", for: indexPath)
            as! Member
        if Settings.lang == "ar" {
            cell.memberName.text = self.members[indexPath.row].name
            let htmlText = members[indexPath.row].postion.trimmingCharacters(in: .newlines)
            //print(htmlText)
            if let textData = htmlText.data(using: .unicode, allowLossyConversion: true) {
                let attribute = try? NSAttributedString(data: textData, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
                cell.memberPostion.attributedText = attribute
            }
            cell.memberImg.image = nil
            self.load_image(self.members[indexPath.row].photo, imageview: cell.memberImg)
            cell.memberImg.layer.cornerRadius = cell.memberImg.layer.bounds.width / 2
            cell.memberImg.layer.masksToBounds = true
        } else {
            cell.memberName.text = self.members[indexPath.row].name
            cell.memberPostion.text = self.members[indexPath.row].postion
            cell.memberImg.image = nil
            self.load_image(self.members[indexPath.row].photo, imageview: cell.memberImg)
            cell.memberImg.layer.cornerRadius = cell.memberImg.layer.bounds.width / 2
            cell.memberImg.layer.masksToBounds = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        imageView.isHidden = false
        self.selectedImage = self.members[indexPath.row].photo
        self.load_image(self.selectedImage, imageview: image)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.members.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let width = self.memberCollection.frame.width
        let text = members[indexPath.row].postion.cleanHtml()
        let estimatedWidth = width//-80
        let estimatedSize = CGSize(width: estimatedWidth, height: 1000)
        let attributes = [NSFontAttributeName: UIFont(name: "Tahoma", size: 15.0) ?? UIFont.systemFont(ofSize: 15.0)]
        let estimatedFrame = NSString(string: text).boundingRect(with: estimatedSize, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
        let val: CGFloat = (UIDevice.current.userInterfaceIdiom == .pad) ? 20+44 : 33+16
        return CGSize(width: width, height: estimatedFrame.height+val)//-80+24
        
    }
    
    func load_image(_ link:String, imageview:UIImageView)
    {
        
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.timeoutInterval = 10
        
        
        let task = session.dataTask(with: url, completionHandler:
        { (data :Data?, response :URLResponse?, error:Error?) in
            
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                
                return
            }
            var image = UIImage(data: data!)
            if (image != nil) {
                func set_image() {
                    self.images_cache[link] = image
                    imageview.image = image
                }
                DispatchQueue.main.async(execute: set_image)
            }
        })
        task.resume()
    }
    
    
    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        self.navigationController!.popViewController(animated:true)
    }
    
    @IBAction func close(_ sender: UIButton) {
        imageView.isHidden = true
    }
    
}

extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0,y: 0,width: self.frame.height,height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x: 0,y: self.frame.height - thickness,width: UIScreen.main.bounds.width, height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x: 0,y: 5,width: thickness,height: self.frame.height - 10)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x: self.frame.width - thickness,y: 0,width: thickness,height: self.frame.height)
            break
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        self.addSublayer(border)
    }
    
}


