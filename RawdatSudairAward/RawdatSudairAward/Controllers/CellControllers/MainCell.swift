
import UIKit



final class MainCell: UICollectionViewCell, CellProvider
{
    // MARK: - Constants
    
    //MARK: - Variables
    
    internal var item: MainItem? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var container: UIView! {
        didSet {
            container.layer.cornerRadius = 5.0
            container.clipsToBounds = true
            container.setShadow(with: Size(right: 0, top: 1), radius: 1, opacity: 0.8, color: .gray)
        }
    }
    @IBOutlet private weak var picture: UIImageView!
    @IBOutlet private weak var lblTitle: UILabel!
    
    
    // MARK: - Overridden functions
    
    internal func updateUI() {
        if let title = item?.title {
            lblTitle.text = title
        }
        picture.image = item?.icon
    }
    
    //MARK: - Actions
}
