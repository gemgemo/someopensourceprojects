
import Foundation
import Gemo


final class Grocery: Mapper<Any>
{
    
    internal var id, name, password, email, phone, username, latitude, longitude, avatar, details: string?, departments = [String]()
    
    internal override var description: String {
        return "\n id: \(id ?? .empty), name: \(name ?? .empty), email: \(email ?? .empty), phone: \(phone ?? .empty), username: \(username ?? .empty), latitude: \(latitude ?? .empty), longitude: \(longitude ?? .empty), sections: \(departments), details: \(details ?? .empty) \n"
    }
    
    init(id: string?, name: string?, password: string?, email: string?, phone: string?, username: string?, latitude: string?, longitude: string?) {
        super.init()
        self.id = id
        self.name = name
        self.email = email
        self.password = password
        self.phone = phone
        self.username = username
        self.latitude = latitude
        self.longitude = longitude
    }
    
    internal override init(JSON: Mapper<Any>.dicObject) {
        super.init(JSON: JSON)
    }
    
    internal override func mapping(mapper: Mapper<Any>) {
        id = mapper["user_id"]?.string
        name = mapper["matjer_name"]?.string
        email = mapper["email"]?.string
        phone = mapper["phone"]?.string
        username = mapper["username"]?.string
        latitude = mapper["lati"]?.string
        longitude = mapper["longi"]?.string
        avatar = mapper["photo_users"]?.string
        details = mapper["description"]?.string
        if let specs = mapper["spec"]?.array as? [String] {
            departments = specs
        }
    }
    
    
    internal class func signIn(with params: Dictionary<String, any>,_ completion: @escaping(Grocery)->())-> void {
        Gemo.gem.startSpinning()
        Http.request(link: Urls.Login, method: .post, parameters: params)
            .response { (response) in
                let error = response.error
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("sign in response error \(String(describing: error))")
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
                print("login response: \n\(response.jsonString ?? .empty)")
                guard let json = (response.result as? [Dictionary<String, any>])?.first,
                    let success = json["result"] as? string else {
                        Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized())
                        return
                }
                if(success == "true") {
                    completion(Grocery(JSON: json))
                } else {
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.UnSuccessLogin.localized())
                }
        }
    }
    
    
}





































