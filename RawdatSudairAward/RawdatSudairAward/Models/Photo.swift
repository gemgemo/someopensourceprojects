
import Foundation



final class Photo
{
    
    internal var id, getId, link: string?
    
    init(dictionary: [string: any]) {
        id = dictionary["id"] as? string
        getId = dictionary["get_id"] as? string
        link = dictionary["photo_file"] as? string
    }
    
    
    internal class func getData(by link: string, viewController: UIViewController?, onComplete: @escaping([Photo])->void)-> void {
        UIApplication.showIndicator(by: true)
        HttpClient.request(link: link, method: .get) { (data, response, error) -> void in            
            if (error != nil) {
                print("error is: \(error)")
                let page = (viewController as? BaseController)
                page?.showAlert(with: string.empty, and: Constants.Misc.ErrorMessage)
                page?.stopSpin()
                UIApplication.showIndicator(by: false)
                
                return
            }
            if let images = data as? [[string: any]] {
                onComplete(images.map{ Photo(dictionary: $0) })
            } else {
                print("none data")
            }
        }
    }
    
    
    
    
}















