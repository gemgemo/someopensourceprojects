
import UIKit

class PhoneNumberVC: UIViewController {
    
    @IBOutlet weak var loadingLb: UILabel!
    @IBOutlet weak var loadingA: UIActivityIndicatorView!
    @IBOutlet weak var loadingV: UIView!
    @IBOutlet weak var shadowV: UIView!
    @IBOutlet weak var waitLbl: UILabel!
    @IBOutlet weak var resendLbl: UILabel!
    @IBOutlet weak var headerBottomView: UIView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var codeText: UITextField!
    @IBOutlet weak var pleaseLbl: UILabel!
    @IBOutlet weak var popBottomView: UIView!
    @IBOutlet weak var popHeaderView: UIView!
    @IBOutlet weak var popOverView: UIView!
    @IBOutlet weak var loadingLbl: UILabel!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var phoneText: UITextField!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var codeLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet private weak var btnSkip: UIButton! {
        didSet {
            btnSkip.layer.cornerRadius = 10
            btnSkip.layer.masksToBounds = true
        }
    }
    
    
    internal var isInitail = false
    var data:String!
    var dataEn:String!
    var token: String!
    var result: String!
    var region = "966"
    var phone: String!
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Enter Your Phone Number"
        phoneView.layer.cornerRadius = 10
        phoneView.layer.masksToBounds = true
        sendBtn.layer.cornerRadius = 10
        sendBtn.layer.masksToBounds = true
        popHeaderView.layer.cornerRadius = 10
        popHeaderView.layer.masksToBounds = true
        sendButton.layer.cornerRadius = 10
        sendButton.layer.masksToBounds = true
        headerBottomView.layer.cornerRadius = 10
        headerBottomView.layer.masksToBounds = true
        shadowView.isHidden = true
        loadingView.isHidden = true
        popOverView.isHidden = true
        popHeaderView.isHidden = true
        popBottomView.isHidden = true
        shadowV.isHidden = true
        loadingV.isHidden = true
        if Settings.lang == "ar" {
            headerView.semanticContentAttribute = .forceRightToLeft
            self.title = "أدخل رقم الهاتف"
            phoneLbl.text = "من فضلك أدخل رقم هاتفك حتى يتم إرسال"
            codeLbl.text = "كود التفعيل "
            phoneLbl.textAlignment = .right
            codeLbl.textAlignment = .right
            //phoneText.placeholder = "رقم الهاتف"
            sendBtn.setTitle("إرسال", for: .normal)
            loadingLbl.text = "جاري التحميل..."
            pleaseLbl.text = "من فضلك أدخل كود التفعيل"
            codeText.placeholder = "كود التفعيل"
            sendButton.setTitle("إرسال", for: .normal)
            resendLbl.text = "إعادة إرسال الكود"
            waitLbl.text = "انتظر دقيقة لإعادة إرسال الكود"
            loadingLb.text = "جاري التحميل..."
        }
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func displayAlertMessage(_ Message:String,_ Error: String) {
        var t = ""
        var b = ""
        
        if Settings.lang == "ar" {
            t = "تنبيه"
            b = "إغلاق"
        } else {
            t = "Alert"
            b = "Close"
        }
        if Error == "" {
            let myAlert = UIAlertController(title: t, message:Message, preferredStyle: UIAlertControllerStyle.alert);
            
            let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
                
                self.popOverView.isHidden = false
                self.popHeaderView.isHidden = false
                self.popBottomView.isHidden = false
                
            })
            myAlert.addAction(okAction);
            
            self.present(myAlert, animated: true, completion: nil)
        } else {
            let myAlert = UIAlertController(title: t, message:Error, preferredStyle: UIAlertControllerStyle.alert);
            
            let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler:nil)
            
            myAlert.addAction(okAction);
            
            self.present(myAlert, animated: true, completion: nil)
        }
    }
    
    func AlertMessage(_ Message:String,_ Error: String) {
        var t = ""
        var b = ""
        
        if Settings.lang == "ar" {
            t = "تنبيه"
            b = "إغلاق"
        } else {
            t = "Alert"
            b = "Close"
        }
        if Error == "" {
            let myAlert = UIAlertController(title: t, message:Message, preferredStyle: UIAlertControllerStyle.alert);
            
            let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler: { [weak self] (action: UIAlertAction!) in
                guard let this = self else { return }
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
                vc.isInitial = this.isInitail
                this.navigationController?.pushViewController(vc, animated: true)
                this.defaults.setValue(this.phone!, forKey: "phone")
            })
            myAlert.addAction(okAction);
            
            self.present(myAlert, animated: true, completion: nil)
        } else {
            let myAlert = UIAlertController(title: t, message:Error, preferredStyle: UIAlertControllerStyle.alert);
            
            let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler:nil)
            
            myAlert.addAction(okAction)
            
            self.present(myAlert, animated: true, completion: nil)
            
            
        }
        
    }
    
    func sendNumber(_ number: String) {
        let NumberStr: String = number
        let Formatter: NumberFormatter = NumberFormatter()
        Formatter.locale = NSLocale(localeIdentifier: "EN") as Locale!
        let final = Formatter.number(from: NumberStr)
        
        self.phone = "\(self.region)" + "\(final!)"
        loadingView.isHidden = false
        shadowView.isHidden = false
        loadingView.layer.cornerRadius = 10
        loadingView.layer.masksToBounds = true
        loadingActivity.startAnimating()
        let urlString = "http://192.232.214.91/~almglsalbldyinmk//number.php?user_num=\(phone!)&key=albldy"
        print(urlString)
        let sessions = URLSession.shared
        let urls = URL(string: urlString)!
        sessions.dataTask(with: urls, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
                
                if let responseData = data {
                    
                    do {
                        let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [NSDictionary]
                        print("json1\(json)")
                        for d in 0..<json.count
                        {
                            let data = json[d]
                            self.result = data["result"] as! String
                            if self.result == "true"
                            {
                                self.data = data["data"] as! String
                                self.dataEn = data["data_en"] as! String
                                self.token = data["token_num"] as! String
                            }
                            else
                            {
                                self.data = data["data"] as! String
                                self.dataEn = data["data_en"] as! String
                                
                            }
                        }
                        DispatchQueue.main.async
                            {
                                if Settings.lang == "ar"
                                {
                                    if self.result == "true"
                                    {
                                        
                                        self.loadingView.isHidden = true
                                        self.shadowView.isHidden = true
                                        self.loadingActivity.stopAnimating()
                                        self.displayAlertMessage(self.data, "")
                                        
                                    }
                                    else
                                    {
                                        self.loadingView.isHidden = true
                                        self.shadowView.isHidden = true
                                        self.loadingActivity.stopAnimating()
                                        self.displayAlertMessage("", self.data)
                                        
                                    }
                                }
                                else
                                {
                                    if self.result == "true"
                                    {
                                        self.loadingView.isHidden = true
                                        self.shadowView.isHidden = true
                                        self.loadingActivity.stopAnimating()
                                        self.displayAlertMessage(self.dataEn, "")
                                    }
                                    else
                                    {
                                        self.loadingView.isHidden = true
                                        self.shadowView.isHidden = true
                                        self.loadingActivity.stopAnimating()
                                        self.displayAlertMessage("", self.dataEn)
                                    }
                                    
                                }
                        }
                    }
                        
                    catch
                    {
                        print ("Could not Serialize")
                    }
                }
                
        }).resume()
        
        
    }
    
    func sendCode(_ number: String!,_ code: String!)
    {
        print("phone\(number)")
        
        let NumberStr: String = code
        let Formatter: NumberFormatter = NumberFormatter()
        Formatter.locale = NSLocale(localeIdentifier: "EN") as Locale!
        let final = Formatter.number(from: NumberStr)
        loadingV.isHidden = false
        shadowV.isHidden = false
        loadingV.layer.cornerRadius = 10
        loadingV.layer.masksToBounds = true
        loadingA.startAnimating()
        let urlString = Settings.x + "\(number!)&token_num=\(final!)&key=albldy"
        print(urlString)
        let sessions = URLSession.shared
        let urls = URL(string: urlString)!
        sessions.dataTask(with: urls, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
                
                if let responseData = data {
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [NSDictionary]
                        print("json1\(json)")
                        for d in 0..<json.count
                        {
                            let data = json[d]
                            self.data = data["data"] as! String
                            self.dataEn = data["data_en"] as! String
                            self.result = data["result"] as! String
                        }
                        DispatchQueue.main.async
                            {
                                if Settings.lang == "ar"
                                {
                                    if self.result == "true"
                                    {
                                        
                                        self.loadingV.isHidden = true
                                        self.shadowV.isHidden = true
                                        self.loadingA.stopAnimating()
                                        self.AlertMessage(self.data, "")
                                        
                                    }
                                    else
                                    {
                                        self.loadingV.isHidden = true
                                        self.shadowV.isHidden = true
                                        self.loadingA.stopAnimating()
                                        self.AlertMessage("", self.data)
                                    }
                                }
                                else
                                {
                                    if self.result == "true"
                                    {
                                        self.loadingV.isHidden = true
                                        self.shadowV.isHidden = true
                                        self.loadingA.stopAnimating()
                                        self.AlertMessage(self.dataEn, "")
                                    }
                                    else
                                    {
                                        self.loadingV.isHidden = true
                                        self.shadowV.isHidden = true
                                        self.loadingA.stopAnimating()
                                        self.AlertMessage("", self.dataEn)
                                    }
                                    
                                }
                        }
                    }
                        
                    catch
                    {
                        print ("Could not Serialize")
                    }
                }
                
        }).resume()
        
        
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (isInitail) {
            navigationItem.leftBarButtonItem = UIBarButtonItem()
            navigationItem.hidesBackButton = true
        }
        btnSkip.isHidden = !isInitail
    }
    
    
    @IBAction private func skipClicked(_ sender: UIButton) {
        (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController = storyboard?.instantiateInitialViewController()
    }
    
    
    @IBAction func sendCodeBtn(_ sender: UIButton) {
        if codeText.text!.isEmpty == true
        {
            if Settings.lang == "ar"
            {
                let msgA = "من فضلك أدخل كود التفعيل"
                self.AlertMessage("", msgA)
            }
            else
            {
                let msg = "Please enter your code"
                self.AlertMessage("", msg)
            }
            
        }
        else
        {
            print("phone\(self.phone)")
            if (Settings.isConnected) {
                sendCode(self.phone, codeText.text!)
            } else {
                dialogUser(with: "لا يوجد انترنت")
            }
        }
        
        
    }
    
    @IBAction func resendBtn(_ sender: UIButton) {
        if (Settings.isConnected) {
            sendNumber(self.phone!)
        } else {
            dialogUser(with: "لا يوجد انترنت")
        }
    }
    @IBAction func sendBtn(_ sender: UIButton) {
        if phoneText.text!.isEmpty == true || phoneText.text!.characters.count != 9
        {
            if Settings.lang == "ar"
            {
                let msgA = "من فضلك أدخل رقم هاتف صحيح"
                self.AlertMessage("", msgA)
            }
            else
            {
                let msg = "Please enter correct number "
                self.AlertMessage("", msg)
            }
        }
        else
        {
            
            sendNumber(phoneText.text!)
        }
    }
    
    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        self.navigationController!.popViewController(animated: true)
    }
}
