
import Foundation
import Gemo

final class NotificationProductCell: BaseTableCell
{
    
    // MARK: - Constatnts
    
    
    // MARK: - Variables
    
    internal var product: NotificationProduct? { didSet { updateUi() } }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var mainPanel: UIView!
    @IBOutlet private weak var productIcon: UIImageView! { didSet {
        productIcon.setCorner(radius: productIcon.frame.height/2)
        productIcon.setBorder(with: 1, and: Color(white: 0.88, alpha: 0.9))
        }}
    @IBOutlet private weak var lblProductName: UILabel!
    @IBOutlet private weak var lblQuantityTitle: UILabel!
    @IBOutlet private weak var lblQuantity: UILabel!
    @IBOutlet private weak var lblPriceTitle: UILabel!
    @IBOutlet private weak var lblPrice: UILabel!
    
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        lblQuantityTitle.text = Localization.Quantity.localized()
        lblPriceTitle.text = Localization.Price.localized()
        if let imageLink = product?.photo {
            productIcon.loadImage(from: imageLink, nil)
        }
        lblProductName.text = product?.name ?? .empty
        lblQuantity.text = product?.quantity ?? .empty
        lblPrice.text = "\(product?.price?.toString ?? .empty) \(Localization.SR.localized())"
        setupConstraints()
    }
    
    
    private func setupConstraints()-> void {
        if (Localizer.instance.current == .arabic) {
            lblProductName.textAlignment = .right
            productIcon.rightAnchor.constraint(equalTo: mainPanel.rightAnchor, constant: -8).isActive = true
            lblProductName.rightAnchor.constraint(equalTo: productIcon.leftAnchor, constant: -8).isActive = true
            lblProductName.leftAnchor.constraint(equalTo: mainPanel.leftAnchor, constant: 8).isActive = true
            lblQuantityTitle.rightAnchor.constraint(equalTo: productIcon.leftAnchor, constant: -8).isActive = true
            lblQuantity.rightAnchor.constraint(equalTo: lblQuantityTitle.leftAnchor, constant: -8).isActive = true
            lblPrice.leftAnchor.constraint(equalTo: mainPanel.leftAnchor, constant: 8).isActive = true
            lblPriceTitle.leftAnchor.constraint(equalTo: lblPrice.rightAnchor, constant: 8).isActive = true
        } else {
            lblProductName.textAlignment = .left            
            productIcon.leftAnchor.constraint(equalTo: mainPanel.leftAnchor, constant: 8).isActive = true
            lblProductName.leftAnchor.constraint(equalTo: productIcon.rightAnchor, constant: 8).isActive = true
            lblProductName.rightAnchor.constraint(equalTo: mainPanel.rightAnchor, constant: -8).isActive = true
            lblQuantityTitle.leftAnchor.constraint(equalTo: productIcon.rightAnchor, constant: 8).isActive = true
            lblQuantity.leftAnchor.constraint(equalTo: lblQuantityTitle.rightAnchor, constant: 8).isActive = true
            lblPrice.rightAnchor.constraint(equalTo: mainPanel.rightAnchor, constant: -8).isActive = true
            lblPriceTitle.rightAnchor.constraint(equalTo: lblPrice.leftAnchor, constant: -8).isActive = true
        }
        
    }
    
    // MARK: - Actions
    
    
}

































