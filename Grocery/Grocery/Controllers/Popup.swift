
import UIKit
import Gemo

final class Popup: BaseController
{
    
    // MARK: - Constatnts
    
    
    // MARK: - Variables (PopoverItem, IndexPath)-> void = {_, _ in}
    
    internal var items = Array<PopupItem>(), didSelect: (int)->() = { _ in }, selectFirstCell = false
    
    // MARK: - Outlets
    
    @IBOutlet private weak var tblItems: UITableView! { didSet {
        tblItems.delegate = self
        tblItems.dataSource = self
        }}
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.superview?.layer.cornerRadius = 0.0
        view.superview?.clipsToBounds = true
        view.superview?.setShadow(with: .zero, radius: 4.0, opacity: 1.0, color: .black)
    }
    
    internal override func updateUi() {
        tabBar.isHidden = true
        tblItems.reloadData()
        if (selectFirstCell) {
            let indexPath = IndexPath(row: 0, section: 0)
            tblItems.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
    }
    
    
    // MARK: - Actions
    
    
}


// MARK:- Table view delegate & data source functions

extension Popup: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifiers.Popup, for: indexPath) as? PopoverCell else { return UITableViewCell() }
        cell.item = items[indexPath.row]
        return cell
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true)
        didSelect(indexPath.row)
    }
    
}


























