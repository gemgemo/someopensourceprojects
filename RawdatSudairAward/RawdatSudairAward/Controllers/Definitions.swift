
import UIKit

final class Definitions: BaseController
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    // MARK: - Outlets
    
    
    @IBOutlet fileprivate weak var textView: UITextView!
   
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
    }
    
    
    //MARK: - Actions 
    
}


// MARK: - Helper functions

extension Definitions
{
    
    fileprivate func loadData()-> void {
        print(Constants.BackendURLs.shared.AboutCompetition)
        Barcode.getData(by: Constants.BackendURLs.shared.AboutCompetition, viewController: self) { [weak self] (barcode) -> void in
            DispatchQueue.main.async { [weak self] in
                self?.updateUi(barcode: barcode)
            }
            UIApplication.showIndicator(by: false)
        }
    }
    
    private func updateUi(barcode: Barcode)-> void {
        /*URLCache.shared.removeAllCachedResponses()
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }*/
        if let htmlString = barcode.content {
            //print(htmlString)
            if let textData = htmlString.data(using: .unicode, allowLossyConversion: true) {
                let attribute = try? NSAttributedString(data: textData, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
                textView.attributedText = attribute
            }
        }
    }

    
}



// MARK: - Web view delegate functions

extension Definitions: UIWebViewDelegate
{
    
    
    internal func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.showIndicator(by: true)
    }
    
    internal func webViewDidFinishLoad(_ webView: UIWebView) {
        UIApplication.showIndicator(by: false)
    }
    
    internal func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        UIApplication.showIndicator(by: false)
    }
    
}




























