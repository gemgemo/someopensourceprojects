
import UIKit

final class MapPopup: BaseController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var markerData: any?
    
    // MARK: - Outlets
    
    @IBOutlet private weak var icon: UIImageView!
    @IBOutlet private weak var lblShopName: UILabel!
    @IBOutlet private weak var lblDetials: UILabel!
    @IBOutlet private weak var labelsPanel: UIView!
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
    }
    
    // MARK: - Actions
    
    @IBAction private func cellClicked(_ sender: UIButton) {
        guard let shopLocation = markerData as? StorsLocations else { return }
        guard let storePage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.Store) as? Store else { return }
        let store = Shop()
        store.id = shopLocation.userId
        store.shopName = shopLocation.shopName
        storePage.store = store
        navigate(to: storePage)
    }
   
    
    
    // MARK: - Functions
    
    private func updateUi()-> void {
        guard let shopLocation = markerData as? StorsLocations else { return }
        if let imageLink = shopLocation.photo {
            icon.loadImage(from: imageLink)
        }
        lblShopName.text = shopLocation.shopName ?? .empty
        lblDetials.text = shopLocation.info ?? .empty
        if (Localizer.instance.current == .arabic) {
            lblDetials.textAlignment = .right
            lblShopName.textAlignment = .right
            icon.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -8).isActive = true
            labelsPanel.rightAnchor.constraint(equalTo: icon.leftAnchor, constant: -8).isActive = true
            labelsPanel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8).isActive = true
        } else{
            lblDetials.textAlignment = .left
            lblShopName.textAlignment = .left
            icon.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8).isActive = true
            labelsPanel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -8).isActive = true
            labelsPanel.leftAnchor.constraint(equalTo: icon.rightAnchor, constant: 8).isActive = true
        }
    }

}




















