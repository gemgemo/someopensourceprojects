
import UIKit

@IBDesignable
final class Shape: UIView
{

    @IBInspectable
    internal var borderColor: UIColor = UIColor(white: 0.88, alpha: 0.9) {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    internal var borderWidth: CGFloat = 1.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    internal var cornerRadius: CGFloat = 10.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    internal override func draw(_ rect: CGRect) {
        clipsToBounds = true
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        backgroundColor = UIColor.rgb(red: 249, green: 249, blue: 249, alpha: 1.0)
    }
    
    
    // MARK: - Actions
    
    

}















