
import Foundation
import UIKit


internal final class SliderCell: BaseGridCell
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var promotion: Promotion? {
        didSet {
            updateUi()
        }
    }
    
    
    // MARK: - Outlets
    
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var cover: UIImageView!
    @IBOutlet internal weak var mainPanal: UIView!
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        lblTitle.text = promotion?.title ?? .empty
        print("promotion title", promotion?.title ?? .empty)
        if let photoLink = promotion?.photo1 {
            cover.loadImage(from: photoLink)
        }
        if (Localizer.instance.current == .arabic) {
            lblTitle.textAlignment = .right
        } else {
            lblTitle.textAlignment = .left
        }
    }
    
    
    // MARK: - Actions

    
}




internal class BaseGridCell: UICollectionViewCell
{
    
    internal func updateUi()-> void {
        
    }
    
}





































