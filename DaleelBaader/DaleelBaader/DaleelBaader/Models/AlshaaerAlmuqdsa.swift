
import Foundation

import Localize_Swift
import UIKit


final class AlshaaerAluqdsa: NSObject
{
    
    internal var id, title, latitude, longitude: string?
    
    internal override var description: String {
        return "\n id-> \(id), title-> \(title), latitude-> \(latitude), longitude-> \(longitude) \n"
    }
    
    
    init(id: string?, title: string?, latitude: string?, longitude: string?) {
        self.id = id
        self.title = title
        self.latitude = latitude
        self.longitude = longitude
    }
    
    override init() {
       super.init()
    }
    
    init(dictionary: [string: any]) {
        id = dictionary["id"] as? string
        title = dictionary["title"] as? string
        latitude = dictionary["lati"] as? string
        longitude = dictionary["long"] as? string
    }
    
    
    // MARK: - Functions 
    
    internal class func getData(viewController: UIViewController?,onComplete: @escaping ([AlshaaerAluqdsa])->void)-> void {
        UIApplication.showIndicator(by: true)
        let link = (Localize.currentLanguage() == Constants.Misc.ArabicSign) ? Constants.BackendURLs.AlshaaerAlmuqdasaAR : Constants.BackendURLs.AlshaaerAlmuqdasaEN
        if (link.isEmpty || !link.isLink) {
            viewController?.dialog(with: "ErrorMessage".localized())
            print("link error")
            UIApplication.showIndicator(by: false)
            return
        }
        
        guard let urlRequest = URL(string: link) else {
            viewController?.dialog(with: "ErrorMessage".localized())
            UIApplication.showIndicator(by: false)
            print("request error")
            return
        }
        
        let request = URLRequest(url: urlRequest)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if (error != nil) {
                viewController?.handle(this: error)
                UIApplication.showIndicator(by: false)
                return
            }
            
            do {
                guard let jsonData = data else {
                    viewController?.dialog(with: "ErrorMessage".localized())
                    print("nix data")
                    UIApplication.showIndicator(by: false)
                    return
                }
                guard let jsonString = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [[string: any]] else {
                    viewController?.dialog(with: "ErrorMessage".localized())
                    print("none data")
                    UIApplication.showIndicator(by: false)
                    return
                }
                onComplete(jsonString.map { AlshaaerAluqdsa(dictionary: $0)})
            } catch {
                viewController?.handle(this: error)
                UIApplication.showIndicator(by: false)
            }
        }.resume()
        
    }
    
}





























