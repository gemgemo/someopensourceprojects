
import UIKit
import Gemo
import GoogleMaps
import Firebase
import GooglePlaces
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Threads.background(QOS: .userInitiated) { 
            FIRApp.configure()
            GMSPlacesClient.provideAPIKey(Misc.MapKey)
            GMSServices.provideAPIKey(Misc.MapKey)
        }
        if (UserDefaults.standard.bool(forKey: Misc.UserLoggedIn)) {
            window?.rootViewController = UIStoryboard(name: Storyboards.Main, bundle: nil).instantiateInitialViewController()
        }
        print("store id is ========> \(UserDefaults.standard.string(forKey: Misc.GroceryId) ?? .empty)")
        Threads.delay(after: 0.3) {
            print("delay splash")
        }
        return true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return .portrait
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

}



// MARK: - Remote Notifications

extension AppDelegate
{
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        connectToFCM()
        NotificationCenter.default.post(name: .UpdateNotificationsSwitch, object: nil)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("remote notifications integration failed reson: \(string(describing: error))")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: .sandbox)
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: .prod)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenDevice(_:)), name: .firInstanceIDTokenRefresh, object: nil)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        //print("deliverd notification: \(string(describing: userInfo))")
        if (UIApplication.shared.applicationState == .active) {
            if (isNotificationsPresentd) {
                NotificationCenter.default.post(name: .RefreshNotifications, object: nil)
            }
        }
        UNUserNotificationCenter.current().delegate = self
        completionHandler(.newData)
    }
    
}


// MARK: - Helper functions

extension AppDelegate {
    
    fileprivate func connectToFCM()-> void {
        guard FIRInstanceID.instanceID().token() != nil else { return }
        FIRMessaging.messaging().disconnect()
        FIRMessaging.messaging().connect { (error) in
            if (error != nil) {
                print("FCM connecting error \(string(describing: error))")
                return
            }
            print("FCM Connected")
        }
    }
    
    @objc fileprivate func refreshTokenDevice(_ notification: Notification)-> void {
        guard let token = FIRInstanceID.instanceID().token() else { return }
        let parameters: Dictionary<string, any> = [
            Misc.MainKey: Misc.MainValue,
            "user_id": UserDefaults.standard.string(forKey: Misc.GroceryId) ?? .empty,
            "d_token": token,
            "type": 1
        ]
        Http.request(link: Urls.AddToken, method: .post, parameters: parameters)
            .response { (response) in
                print("device token json string: \(response.jsonString ?? .empty)")
                if (response.error != nil) {
                    print("send device token error \(string(describing: response.error))")
                    return
                }
                print("send device token data \(string(describing: response.result))")
        }
        connectToFCM()
    }
    
}

// MARK: - User notifications delegate and firebase messageing functions

extension AppDelegate: UNUserNotificationCenterDelegate, FIRMessagingDelegate
{
    
    internal func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        //print("ios 10 user notification received response")
        print(#function)        
        if (UIApplication.shared.applicationState == .active) {
            if (isNotificationsPresentd) {
                NotificationCenter.default.post(name: .RefreshNotifications, object: nil)
            } else {
                NotificationCenter.default.post(name: .OpenNotifications, object: nil)
                completionHandler()
                return
            }
        } else {
            NotificationCenter.default.post(name: .OpenNotifications, object: nil)
            completionHandler()
        }
    }
    
    internal func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        //print("ios 10 notification presented in foreground", notification.request.content.userInfo)
        completionHandler([.alert, .sound])
    }
    
    internal func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        print("message data: \(remoteMessage.appData)")
    }
    
}































