
import UIKit
import Localize_Swift

final class TransportTypeCell: BaseTableCell
{
   
    // MARK: - Constants
    
    // MARK: - Variables
    
    internal var subCar: SubCar? {
        didSet {
            updateUi()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet internal weak var container: UIView! {
        didSet {
            container.setCorner(radius: 8)
        }
    }
    @IBOutlet private weak var radioView: RadioView!
    @IBOutlet internal weak var lblTitle: UILabel!
    
    
    // MARK: - Overridden functions
    
    internal override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        radioView.isSelected = selected
    }
    
    internal override func updateUi() {
        lblTitle.text = ((Localize.currentLanguage() == Constants.Language.Arabic) ? subCar?.arabicName : subCar?.englishName) ?? .empty
    }
    
    // MARK: - Actions

}
