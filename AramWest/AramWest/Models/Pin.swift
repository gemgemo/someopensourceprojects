
import UIKit
import MapKit

class Pin: NSObject, MKAnnotation
{
    
    internal var title: String?
    internal var subtitle: String?
    internal var coordinate: CLLocationCoordinate2D
    
    init(coordinate: CLLocationCoordinate2D, title: string, subtitle: string) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }
    
}





final class SourcePin: Pin {
    
}

final class DestinationPin: Pin {
    
}


