
import UIKit
import Localize_Swift


final class MainCell: UITableViewCell
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    internal var content: MainContent? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet internal weak var picture: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var divider: UIView! {
        didSet {
            divider.backgroundColor = Color(white: 0.80, alpha: 0.9)
        }
    }
    @IBOutlet private weak var logoContainer: UIView! {
        didSet {
            logoContainer.layer.cornerRadius = logoContainer.bounds.height/2
            logoContainer.layer.borderWidth = 1.0
            logoContainer.layer.borderColor = Color(white: 0.80, alpha: 0.9).cgColor
        }
    }
    
    @IBOutlet internal weak var imageTop: NSLayoutConstraint!
    @IBOutlet internal weak var imageRight: NSLayoutConstraint!
    @IBOutlet internal weak var imageLeft: NSLayoutConstraint!
    @IBOutlet internal weak var imageBottom: NSLayoutConstraint!
    
    
    // MARK: - Functions
    
    private func updateUI()-> void {
        if let title = content?.title {
            titleLabel.text = title
        }
        picture.image = content?.logo
        
    }
    
    //MARK: - Actions

}
