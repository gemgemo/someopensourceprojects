
import Foundation



final class Video
{
    
    internal var id, title, content, link: string?
    
    init(dictionary: [string: any]) {
        id = dictionary["id"] as? string
        title = dictionary["title"] as? string
        content = dictionary["content"] as? string
        link = dictionary["url"] as? string
    }
    
    
    internal class func getData(by link: string, viewController: UIViewController?, onComplete: @escaping([Video])->void)-> void {
        UIApplication.showIndicator(by: true)
        HttpClient.request(link: link, method: .get) { (data, response, error) -> void in
            if (error != nil) {
                (viewController as? BaseController)?.showAlert(with: string.empty, and: Constants.Misc.ErrorMessage)
                UIApplication.showIndicator(by: false)
                return
            }
            if let videos = data as? [[string: any]] {
                onComplete(videos.map{ Video(dictionary: $0) })
            } else {
                print("null videos")
            }
        }
    }
    
}

