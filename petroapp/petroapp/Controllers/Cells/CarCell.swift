
import Foundation
import Gemo


class CarCell: BaseTableCell {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var car: Car? { didSet { updateUi() } }
    
    // MARK: - Outlets
    
    @IBOutlet weak var checkBox: CheckBox!
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var carIcon: UIImageView!
    @IBOutlet weak var carBoard: UIImageView!
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var iconsPanel: UIView!
    @IBOutlet weak var lblALetters: UILabel!
    @IBOutlet weak var lblELetters: UILabel!
    @IBOutlet weak var lblANumbers: UILabel!
    @IBOutlet weak var lblENumbers: UILabel!
    @IBOutlet weak var btnCheckBox: UIButton!
    
    
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        iconsPanel.setCorner(radius: iconsPanel.frame.height / 2)
        iconsPanel.clipsToBounds = true
        carImage.loadImage(from: car?.userCarPhoto ?? .empty)
        carIcon.loadImage(from: car?.icon ?? .empty)
        lblCarName.text =  (Localizer.instance.current == .arabic) ? "\(car?.prand ?? .empty)-\(car?.model ?? .empty)" : "\(car?.model ?? .empty)-\(car?.prand ?? .empty)"
        lblALetters.text = car?.aLetters ?? .empty
        lblELetters.text = car?.eLetters ?? .empty
        lblANumbers.text = car?.aNumbers ?? .empty
        lblENumbers.text = car?.eNumbers ?? .empty
        carBoard.loadImage(from: car?.photo ?? .empty)
        lblALetters.textAlignment = .center
        lblELetters.textAlignment = .center
        lblANumbers.textAlignment = .center
        lblENumbers.textAlignment = .center
        
    }
    
    
    // MARK: - Actions

}




























