

import UIKit

class NewsVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var loadingLbl: UILabel!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var newCollection: UICollectionView!
    var new = Array<New>()
    var images_cache = [String:UIImage]()
    var dataFound:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newCollection.delegate = self
        newCollection.dataSource = self
        self.title = "News"
        loadingView.isHidden = true
        shadowView.isHidden = true
        if Settings.lang == "ar"
        {
            newCollection.semanticContentAttribute = .forceRightToLeft
            self.title = "الأخبار"
            loadingLbl.text = "جاري التحميل..."
        }
        if (Settings.isConnected) {
            News()
        } else {
            dialogUser(with: "لا يوجد انترنت")
        }

        // Do any additional setup after loading the view.
    }
    func News() {
        loadingView.isHidden = false
        shadowView.isHidden = false
        loadingView.layer.cornerRadius = 10
        loadingView.layer.masksToBounds = true
        loadingActivity.startAnimating()
        let urlString = Settings.News
        let sessions = URLSession.shared
        let urls = URL(string: urlString)!
        sessions.dataTask(with: urls, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
                
                if let responseData = data {
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                        print("json1\(json)")
                        self.dataFound = json["Success"] as! String
                        if self.dataFound == "True"
                        {
                            let data = json["Data"] as! [NSDictionary]
                            for i in data
                            {
                                var name1 = ""
                                if Settings.lang == "ar"
                                {
                                    name1 = "title"
                                }
                                else
                                {
                                    name1 = "title"
                                    
                                }
                                let id = i["news_id"] as! String
                                let name = i[name1] as! String
                                let img = i["images"] as! String
                                
                                let news = New(id: id, title: name, photo: img)
                                self.new.append(news)
                                
                            }
                            
                        }
                        
                        
                        
                        
                        DispatchQueue.main.async
                            {
                                self.loadingView.isHidden = true
                                self.shadowView.isHidden = true
                                self.loadingActivity.stopAnimating()
                                self.newCollection.reloadData()
                            }
                    }
                        
                    catch
                    {
                        print ("Could not Serialize")
                    }
                }
                
        }).resume()
        

    }
    
    func load_image(_ link:String, imageview:UIImageView) {
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: url)
        request.timeoutInterval = 10
        let task = session.dataTask(with: url, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
                guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                    return
                }
                var image = UIImage(data: data!)
                if (image != nil)
                {
                    func set_image()
                    {
                        self.images_cache[link] = image
                        imageview.image = image
                    }
                    DispatchQueue.main.async(execute: set_image)
                }
        })
        
        task.resume()
        
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "newCell", for: indexPath)
            as! News
        if Settings.lang == "ar"
        {
            cell.title.text = (self.new[indexPath.row].title).cleanHtml()
            self.load_image(self.new[indexPath.row].photo, imageview: cell.newImage)
            cell.title.textAlignment = .right
            cell.layer.shadowColor = UIColor.gray.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 2.0;
            cell.layer.shadowOpacity = 1.0;
            cell.layer.masksToBounds = false;
            cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath;
        }
        else
        {
            cell.title.text = self.new[indexPath.row].title
            self.load_image(self.new[indexPath.row].photo, imageview: cell.newImage)
            cell.layer.shadowColor = UIColor.gray.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 2.0;
            cell.layer.shadowOpacity = 1.0;
            cell.layer.masksToBounds = false;
            cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath;

        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        let size = self.newCollection.frame.width        
        return CGSize(width: size  - 10, height: view.frame.height*0.35)
        
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.new.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetialNewsVC") as! DetialNewsVC
        vc.selectedID = self.new[indexPath.row].id
        vc.selectedTitle = self.new[indexPath.row].title
        self.navigationController?.pushViewController(vc, animated: true)

        
    }
    

   
    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        self.navigationController!.popViewController(animated: true)
    }
}






extension String {
    
    internal func cleanHtml()-> String {
        return self.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil).replacingOccurrences(of: "&nbsp;", with: "").replacingOccurrences(of: "&bull;", with: "")
    }
    
    
}






