
import Foundation
import Alamofire

internal final class CallInfo: Mapper
{
    
    internal var id, sourceLatitude, sourceLongitude, destinationLatitude, destinationLongitude, timestamp, clientId, type, payType, arabicCarName, englishCarName, weight, distance, load, carCount, salary, drivarAvatar, driverName, rate: string?
    
    internal override var description: String {
        return "\n id: \(id), driver name: \(driverName) \n"
    }
    
    internal override func mapping(mapper: Mapper) {
        id = mapper["ID"]?.text
        sourceLatitude = mapper["Slat"]?.text
        sourceLongitude = mapper["Slan"]?.text
        destinationLatitude = mapper["Elat"]?.text
        destinationLongitude = mapper["Elan"]?.text
        timestamp = mapper["datetime"]?.text
        clientId = mapper["clientID"]?.text
        type = mapper["type"]?.text
        payType = mapper["paytype"]?.text
        arabicCarName = mapper["carNameA"]?.text
        englishCarName = mapper["carNameE"]?.text
        weight = mapper["Wight"]?.text
        distance = mapper["Kilo"]?.text
        load = mapper["Load"]?.text
        carCount = mapper["CountCar"]?.text
        salary = mapper["Salary"]?.text
        drivarAvatar = mapper["DriverImage"]?.text
        driverName = mapper["DriverName"]?.text
        rate = mapper["Rate"]?.text
        
    }
    
    // MARK: - Functions
    
    internal class func getData(byId id: string, page: BaseController?, _ onCompplete: @escaping(CallInfo)->())-> void {
        page?.startSpin()
        Alamofire.request(Constants.BackendURLs.GetCallDetails, method: .post, parameters: ["RequestID": id])
            .responseJSON { (response) in
                let result = response.result
                if (result.error != nil) {
                    page?.showAlert(with: .empty, and: Constants.Localizer.Error.localized())
                    page?.stopSpin()
                    print("get call details error \(result.error)")
                    return
                }
                if let value = result.value as? Dictionary<string, any>, let data = value["Data"] as? Dictionary<string, any>, let success = value["Success"] as? string {
                    if (success == "True") {
                        onCompplete(CallInfo(JSON: data))
                    } else {
                        page?.showAlert(with: .empty, and: Constants.Localizer.NoData.localized())
                        page?.stopSpin()
                        print("get call details success false")
                    }
                } else {
                    page?.showAlert(with: .empty, and: Constants.Localizer.NoData.localized())
                    page?.stopSpin()
                    print("get call details nix data error")
                }
        }
        
    }
    
}

















