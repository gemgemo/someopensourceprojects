
import UIKit

@IBDesignable
final class RadioView: UIView
{

    // MARK: - Properties
    @IBInspectable
    internal var isSelected: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    internal var borderColor: UIColor = UIColor.darkGray {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    internal var borderWidth: CGFloat = 1.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    internal var image: UIImage = UIImage() {
        didSet {
            setNeedsDisplay()
        }
    }
    
    private lazy var borderView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var checkedImage: UIImageView = {
       let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.contentMode = .scaleAspectFit
        return img
    }()
    
    // MARK: - Main function
    internal override func draw(_ rect: CGRect) {
        setupBorderView()
        setupImageView()
    }

    
    // MARK: - Helper functions
    
    private func setupBorderView()-> void {
        borderView.layer.borderWidth = borderWidth
        borderView.layer.borderColor = borderColor.cgColor
        borderView.layer.cornerRadius = 2.0
        addSubview(borderView)
        
        // constraints
        borderView.widthAnchor.constraint(equalToConstant: 25.0).isActive = true
        borderView.heightAnchor.constraint(equalToConstant: 25.0).isActive = true
        borderView.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0.0).isActive = true
        borderView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0.0).isActive = true
    }
    
    private func setupImageView()-> void {
        checkedImage.image = (isSelected) ? image : nil
        addSubview(checkedImage)
        // constraints
        checkedImage.widthAnchor.constraint(equalToConstant: 26).isActive = true
        checkedImage.heightAnchor.constraint(equalToConstant: 26).isActive = true
        checkedImage.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 3.0).isActive = true
        checkedImage.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -5.0).isActive = true
    }

}






















