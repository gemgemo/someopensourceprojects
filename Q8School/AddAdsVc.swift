
import UIKit

final class AddAdsVc: UIViewController,
    UIPopoverPresentationControllerDelegate,
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate,
    UITextViewDelegate
{

    @IBOutlet weak var adsImagebtn: UIButton!
    @IBOutlet weak var addAdsBtn: UIButton!
    @IBOutlet weak var myAdsImg: UIImageView!
    @IBOutlet weak var myAdsLbl: UILabel!
    @IBOutlet weak var offerLbl: UILabel!
    @IBOutlet weak var callUsLbl: UILabel!
    @IBOutlet weak var addAdsLbl: UILabel!
    @IBOutlet weak var openMneu: UIBarButtonItem!
    @IBOutlet weak var loadingLbl: UILabel!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var agreeBtn: UIButton!
    @IBOutlet weak var popUpLbl: UILabel!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var popView: UIView!
    @IBOutlet weak var adsImage: UIImageView!
    @IBOutlet weak var specialLbl: UILabel!
    @IBOutlet weak var freeLbl: UILabel!
    @IBOutlet weak var specialView: UIView!
    @IBOutlet weak var freeView: UIView!
    @IBOutlet weak var adsDescTxtV: UITextView!
    @IBOutlet weak var adsNameTxt: UITextField!
    @IBOutlet weak var subLbl: UILabel!
    @IBOutlet weak var mainLbl: UILabel!
    
    private var mainNameLbl: String!
    private var mainIDS: String!
    private var SubNameLbl: String!
    private var SubIDS: String!
    private var type = "0"
    private var dataFound: Bool!
    private var Message:String!
    private var types:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adsDescTxtV.delegate = self
        popView.isHidden = true
        popUpView.isHidden = true
        shadowView.isHidden = true
        loadingView.isHidden = true
        adsImage.layer.cornerRadius = 0.5 * adsImage.bounds.size.width
        adsImage.clipsToBounds = true
        self.title = "Add Ads"
        if Settings.lang == "ar" {
         adsDescTxtV.text = "تفاصيل الاعلان"
            adsNameTxt.placeholder = "اسم الاعلان"
            popUpLbl.text = "اذا كنت ترغب باعلان مميز الرجاء الضغط علي موافق وسيتم التواصل معك من قبل الادارة"
            agreeBtn.setTitle("موافق", for: .normal)
            cancelBtn.setTitle("الغاء", for: .normal)
            loadingLbl.text = "جاري التحميل"
            callUsLbl.text = "اتصل بنا"
            addAdsLbl.text = "اضف اعلانك"
            myAdsLbl.text = "اعلاناتي"
            offerLbl.text = "العروض"
            freeLbl.text = "مجانا"
            specialLbl.text = "مميز"
            self.title = "اضف اعلان"
        }
        if revealViewController() != nil {
            revealViewController().rightViewRevealWidth = 235
            openMneu.target = revealViewController()
            openMneu.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        types = UserDefaults.standard.string(forKey: "userType")
        if types == "1" {
            if Settings.lang == "ar" {
                myAdsLbl.text = "المفضلة"
                myAdsImg.image = UIImage(named: "fav")
            } else {
                myAdsLbl.text = "Favorite"
                myAdsImg.image = UIImage(named: "fav")
            }
        }
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    

    func displayAlert(_ Massage: String)
    {
        var t = ""
        var b = ""
        
        if Settings.lang == "ar"{
            t = "تنبيه"
            b = "إغلاق"
        }
        else{
            t = "Alert"
            b = "Close"
        }
        
        let myAlert = UIAlertController(title: t, message:Massage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default,handler: { (action: UIAlertAction!) in
            
        })
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }


    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if Settings.lang == "ar"
        {
            if self.adsDescTxtV.text == "تفاصيل الاعلان"{
                self.adsDescTxtV.text = ""
            }
 
        }
        else
        {
            if self.adsDescTxtV.text == "Ads Description"{
                self.adsDescTxtV.text = ""
            }
            print("here")
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if Settings.lang == "ar" {
            print("here1")
            if self.adsDescTxtV.text == ""{
                self.adsDescTxtV.text = "تفاصيل الاعلان"
            }
        } else {
            if self.adsDescTxtV.text == "" {
                self.adsDescTxtV.text = "Ads Description"
            }
        }

    }
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "user-profile\(NSUUID().uuidString).png"
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    
    
    func generateBoundaryString() -> String
    {
        return "Boundary-\(NSUUID().uuidString)"
    }

    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle
    {
        return.none
    }
    
    @IBAction func unwindToAds(_ segue:UIStoryboardSegue) {
        if let userTypeView = segue.source as? MainTableVC,
            let selectedUser = userTypeView.mainNameLbl {
            mainNameLbl = selectedUser
            print("this user\(mainNameLbl)")
            mainLbl.text = self.mainNameLbl
            let mainID = userTypeView.mainID
            mainIDS = mainID
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "mainPop" {
            let vc = segue.destination as! MainTableVC
            vc.preferredContentSize = CGSize(width: 250, height: 380)
            let controller = vc.popoverPresentationController
            controller?.permittedArrowDirections = .up
            if controller != nil {
                controller?.delegate = self
            }
        } else {
            let vc = segue.destination as! SubMainTableVC
            vc.preferredContentSize = CGSize(width: 250, height: 350)
            let controller = vc.popoverPresentationController
            controller?.permittedArrowDirections = .up
            if controller != nil {
                controller?.delegate = self
            }
        }
    }
    
    @IBAction func unwindSub(_ segue:UIStoryboardSegue) {
        if let userTypeView = segue.source as? SubMainTableVC,
            let selectedUser = userTypeView.subName{
            SubNameLbl = selectedUser
            subLbl.text = self.SubNameLbl
            let mainID = userTypeView.subID
            SubIDS = mainID
        }
    }
    

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitBtn(_ sender: UIButton) {
        shadowView.isHidden = false
        loadingView.isHidden = false
        loadingView.layer.cornerRadius = 4
        loadingView.layer.masksToBounds = true
        loadingActivity.startAnimating()
        let myUrl = NSURL(string:Settings.AddADs);
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        let ID = UserDefaults.standard.string(forKey: "users_ID")
        let param = [
            "lang"  : Settings.lang,
            "user_id"    : ID!,
            "scholarPhase": mainIDS,
            "subject"    : SubIDS,
            "title"    : adsNameTxt.text!,
            "details"    : adsDescTxtV.text!,
            "type"    : type,
            ] as [String : String]
        
        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
     
        let imageData = UIImageJPEGRepresentation(adsImage.image!, 0.5)
        
        if(imageData == nil)  {  }
        
        request.httpBody = createBodyWithParameters(parameters: param, filePathKey: "img", imageDataKey: imageData! as NSData, boundary: boundary) as Data
        
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
            
            if let responseData = data {
                
                do {
                    
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                    print("ddd\(json)")
                    self.dataFound = json["Success"] as? Bool
                    if self.dataFound!
                    {
                        self.Message = json["Message"] as! String
                    }
                    else
                    {
                        self.Message = json["Message"] as! String

                    }
                    
                    DispatchQueue.main.async
                        {
                            self.shadowView.isHidden = true
                            self.loadingView.isHidden = true
                            self.loadingActivity.stopAnimating()
                            self.displayAlert(self.Message)
                    }
                    
                }
                catch
                {
                    print ("Could not Serialize")
                }
                
            }
            
        }
        task.resume()

        
    }

    @IBAction func freeBtn(_ sender: UIButton) {
        type = "0"
       
        //: Old
        /*freeView.backgroundColor = UIColor(red: 222/255, green: 140/255, blue: 0/255, alpha: 1)
        specialView.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
        freeLbl.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        specialLbl.textColor = UIColor(red: 255/255, green: 128/255, blue: 0/255, alpha: 1)*/

        specialView.backgroundColor = UIColor(red: 222/255, green: 140/255, blue: 0/255, alpha: 1)
        freeView.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
        specialLbl.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        freeLbl.textColor = UIColor(red: 255/255, green: 128/255, blue: 0/255, alpha: 1)

        
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        specialView.backgroundColor = UIColor(red: 222/255, green: 140/255, blue: 0/255, alpha: 1)
        freeView.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
        specialLbl.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        freeLbl.textColor = UIColor(red: 255/255, green: 128/255, blue: 0/255, alpha: 1)

    }
    
    @IBAction func specialBtn(_ sender: UIButton) {
        //: Old
        /*specialView.backgroundColor = UIColor(red: 222/255, green: 140/255, blue: 0/255, alpha: 1)
        freeView.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
        specialLbl.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        freeLbl.textColor = UIColor(red: 255/255, green: 128/255, blue: 0/255, alpha: 1)*/
        type = "1"
        freeView.backgroundColor = UIColor(red: 222/255, green: 140/255, blue: 0/255, alpha: 1)
        specialView.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
        freeLbl.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        specialLbl.textColor = UIColor(red: 255/255, green: 128/255, blue: 0/255, alpha: 1)
        
        popView.isHidden = false
        popUpView.isHidden = false



    }
    @IBAction func imagePressed(_ sender: UIButton) {
        let ImagePicker = UIImagePickerController()
        ImagePicker.delegate = self
        ImagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        
        self.present(ImagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        adsImage.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        adsImage.isHidden = false
        adsImagebtn.setImage(UIImage(), for: UIControlState.normal)


        
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func agreeBtn(_ sender: UIButton) {
        type = "1"
        popView.isHidden = true
        popUpView.isHidden = true
    }
    
    @IBAction func cancelBtn(_ sender: UIButton) {
        popView.isHidden = true
        popUpView.isHidden = true

        type = "0"                
        specialView.backgroundColor = UIColor(red: 222/255, green: 140/255, blue: 0/255, alpha: 1)
        freeView.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
        specialLbl.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        freeLbl.textColor = UIColor(red: 255/255, green: 128/255, blue: 0/255, alpha: 1)

    }

    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MainPageViewController") as! MainPageViewController
        self.navigationController?.pushViewController(vc, animated: false)

        
    }

    @IBAction func callUsBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CallUsVc") as! CallUsVc
        self.navigationController?.pushViewController(vc, animated: false)

    }
    
    @IBAction func addAdsBtn(_ sender: Any) {
        if types == "1"
        {
            if Settings.lang == "ar"
            {
                let msg = "انت طالب لا يمكنك اضافة اعلان"
                self.displayAlert(msg)
                
            }
            else
            {
                let msg = "you'r student can't add Ads"
                self.displayAlert(msg)
                
            }
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AddAdsVc") as! AddAdsVc
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        

    }
    
    @IBAction func myAdsBtn(_ sender: Any) {
        if types == "1"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyFavoriteVC") as! MyFavoriteVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyAdsVC") as! MyAdsVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        

    }
    
    @IBAction func offerBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AdsPageViewController") as! AdsPageViewController
        self.navigationController?.pushViewController(vc, animated: false)
        

    }
    
    
    
}
