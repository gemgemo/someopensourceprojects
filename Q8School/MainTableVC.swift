//
//  MainTableVC.swift
//  Q8School
//
//  Created by mac on 12/18/16.
//  Copyright © 2016 Atiaf. All rights reserved.
//

import UIKit

class MainTableVC: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    var mainNameLbl: String!
    var mainID: String!
    var mainNames = [String]()
    var mainIDs = [String]()
    var dataFound: Bool!
    var phases: Array<Phase> = Array<Phase>()
    let defaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        loadMainPhase(Settings.lang)
        

        // Do any additional setup after loading the view.
    }
    func loadMainPhase(_ lang: String)
    {
        let request = NSMutableURLRequest(url: URL(string:Settings.MainPhase)!)
        request.httpMethod = "POST"
        let postString = "lang=\(lang)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
            
            if let responseData = data {
                
                do {
                    
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                    print("ddd\(json)")
                    self.dataFound = json["Success"] as? Bool
                    if self.dataFound!
                    {
                        let data = json["Result"] as! [NSDictionary]
                        for i in data
                        {
                            var name1 = ""
                            if Settings.lang == "ar"
                            {
                                name1 = "name_ar"
                            }
                            else
                            {
                                name1 = "name"
                                
                            }
                            
                            let id = i["id"] as! String
                            let name = i[name1] as! String
                            
                            let phase = Phase(id: id, name: name)
                            self.phases.append(phase)
                            
                        }
                        
                        
                        
                    }
                    DispatchQueue.main.async
                        {
                            self.tableView.reloadData()
                    }
                    
                }
                catch
                {
                    print ("Could not Serialize")
                }
                
            }
            
        }
        task.resume()
        

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {        // #warning Incomplete implementation, return the number of sections
        return self.phases.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mainCell", for: indexPath) as! MainTable
        if Settings.lang == "ar"
        {
            cell.mainLbl.text = self.phases[indexPath.row].name
            cell.mainID = self.phases[indexPath.row].id
            
        }
        else
        {
            cell.mainLbl.text = self.phases[indexPath.row].name
            cell.mainID = self.phases[indexPath.row].id
            
        }
        return cell
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
        
    {
        if segue.identifier == "unwindMain"
        {
            if let cell = sender as? UITableViewCell
            {
                if Settings.lang == "ar"
                {
                    let indexPath = tableView.indexPath(for: cell)
                    mainNameLbl = self.phases[(indexPath?.row)!].name
                    print("user name\(mainNameLbl)")
                    mainID = self.phases[(indexPath?.row)!].id
                    print("user name\(mainID)")
                    self.defaults.setValue(self.phases[(indexPath?.row)!].id, forKey: "mainPhase")

                    
                }
                else
                {
                    let indexPath = tableView.indexPath(for: cell)
                    mainNameLbl = self.phases[(indexPath?.row)!].name
                    print("user name\(mainNameLbl)")
                    mainID = self.phases[(indexPath?.row)!].id
                    print("user name\(mainID)")
                    self.defaults.setValue(self.phases[(indexPath?.row)!].id, forKey: "mainPhase")

                }
                
            }
            
        }
        
    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
