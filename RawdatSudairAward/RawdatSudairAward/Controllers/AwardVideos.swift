
import UIKit
import Localize_Swift

final class AwardVideos: BaseController
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    fileprivate var dataSource = Array<Video>() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tblVideos.reloadData()
            }
        }
    }
   
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var tblVideos: UITableView! {
        didSet {
            tblVideos.delegate = self
            tblVideos.dataSource = self
        }
    }
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()        
        loadData()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBar()
        updateNavigationBarDirections()
    }
    
  
    
    //MARK: - Actions
    
    
}


// MARK: - Helper functions

extension AwardVideos
{
    
    
    fileprivate func updateNavigationBar()-> void {
        navigationItem.title = "videogallerylan".localized()
        navigationController?.navigationBar.castShadow = "set"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: getBackImage(), style: .plain, target: self, action: #selector(goBack(_:)))
    }
    
    fileprivate func configureTableView()-> void {
        tblVideos.register(Constants.Nib.AwardVideos, forCellReuseIdentifier: Constants.ReuseIdentifier.AwardVideos)
    }
    
    fileprivate func loadData()-> void {
        startSpin()
        Video.getData(by: Constants.BackendURLs.shared.Videos, viewController: self) { [weak self] (videos) -> void in
            if (!videos.isEmpty) {
                self?.dataSource = videos
            }
            self?.stopSpin()
        }
    }       
    
    
}


// MARK: - Table view delegate & data source functions

extension AwardVideos: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.AwardVideos) as? AwardVideosCell {
            cell.video = dataSource[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let videoDetailsScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.AwardVideosDetails) as? AwardVideosDetails {
            videoDetailsScreen.video = dataSource[indexPath.row]
            videoDetailsScreen.videos = dataSource
            navigationController?.pushViewController(videoDetailsScreen, animated: true)
        }
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}





























