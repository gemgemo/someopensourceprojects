
import UIKit
import Alamofire
import Cosmos

final class Rate: BaseController
{
    
    // MARK: - Constants
    
    // MARK: - Variables
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var lblRateTitle: UILabel!
    @IBOutlet fileprivate weak var rateView: CosmosView! {
        didSet {
            rateView.rating = 0.0
        }
    }
    @IBOutlet fileprivate weak var btnAdd: Button!
    
    
    // MARK: - Overridden functions
    
    internal override func updateNavgationBar() {
        navigationController?.navigationBar.isHidden = true
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
    }
    
    /*internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }*/
    
    // MARK: - Actions
    
    @IBAction private func addRateOnClick(_ sender: UIButton) {
        print("user rate is \(rateView.rating)")
        // send to backend to rate service
        startSpin()
        if (isRateDriver) {
            isRateDriver = false
            Alamofire.request(Constants.BackendURLs.DriverRate, method: .post, parameters: ["callID": callId, "rate": rateView.rating])
                .responseJSON { [weak self] (response) in
                    let result = response.result
                    self?.dismiss(animated: true, completion: nil)
                    if (result.error != nil) {
                        self?.stopSpin()
                        self?.showAlert(with: .empty, and: "erroralertlan".localized())
                        print("send rating error: \(String(describing: result.error))")
                        return
                    }
                    
                    if let value = result.value {
                        print("send rating value", value)
                        self?.stopSpin()
                        self?.popFunction()
                    } else {
                        self?.stopSpin()
                        self?.showAlert(with: .empty, and: "erroralertlan".localized())
                        print("send rating none value")
                    }
            }

        } else if (isRateCompany) {
            isRateCompany = false
            Alamofire.request(Constants.BackendURLs.RateCompany, method: .post, parameters: ["requestID": reqId, "rate": rateView.rating])
                .responseJSON { [weak self] (response) in
                    let result = response.result
                    self?.dismiss(animated: true, completion: nil)
                    if (result.error != nil) {
                        self?.stopSpin()
                        self?.showAlert(with: .empty, and: "erroralertlan".localized())
                        print("send rating error: \(String(describing: result.error))")
                        return
                    }
                    
                    if let value = result.value {
                        print("send rating value", value)
                        self?.stopSpin()
                        self?.popFunction()
                    } else {
                        self?.stopSpin()
                        self?.showAlert(with: .empty, and: "erroralertlan".localized())
                        print("send rating none value")
                    }
            }

        } else {
            Alamofire.request(Constants.BackendURLs.Rating, method: .post, parameters: ["callID": callId, "rate": rateView.rating])
                .responseJSON { [weak self] (response) in
                    let result = response.result
                    self?.dismiss(animated: true, completion: nil)
                    if (result.error != nil) {
                        self?.stopSpin()
                        self?.showAlert(with: .empty, and: "erroralertlan".localized())
                        print("send rating error: \(String(describing: result.error))")
                        return
                    }
                    
                    if let value = result.value {
                        print("send rating value", value)
                        self?.stopSpin()
                        self?.popFunction()
                    } else {
                        self?.stopSpin()
                        self?.showAlert(with: .empty, and: "erroralertlan".localized())
                        print("send rating none value")
                    }
            }

        }
    }
}



// MARK: - Helper functions

extension Rate
{
    
    fileprivate func updateUi()-> void {
        lblTitle.text = "ratetitlelan".localized()
        lblRateTitle.text = "ratetitlemiddlelan".localized()
        btnAdd.setTitle("ratebuttonlan".localized(), for: .normal)
    }
    
    
    fileprivate func popFunction()-> void {
        NotificationCenter.default.post(name: Notification.Name("navigatetohome"), object: nil)
    }
    
}

























