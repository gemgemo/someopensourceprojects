
import Foundation



final class AnnualReport
{
    
    internal var id, title, link: string?
    
    init(dictionary: [string: any]) {
        id = dictionary["id"] as? string
        title = dictionary["title"] as? string
        link = dictionary["file"] as? string
    }
    
    
    internal class func getData(from link: string, viewController: UIViewController?, onComplet: @escaping([AnnualReport])-> void )-> void {
        UIApplication.showIndicator(by: true)
        HttpClient.request(link: link, method: .get) { (result, response, error) -> void in
            if (error != nil) {
                (viewController as? BaseController)?.showAlert(with: string.empty, and: Constants.Misc.ErrorMessage)
                return
            }
            if let data = result as? [[string: any]] {                
                onComplet(data.map{ AnnualReport(dictionary: $0) })
            } else {
                print("null data")
            }
        }
    }
    
}
