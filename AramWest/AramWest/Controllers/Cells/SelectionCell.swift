
import UIKit

final class SelectionCell: BaseTableCell
{

    // MARK: - Constants
    
    // MARK: - Variables
    
    
    
    internal var payment: PaymenyWay? {
        didSet {
            updateUi()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet internal weak var mainPanal: UIView!
    @IBOutlet private weak var radioView: RadioView!
    @IBOutlet internal weak var lblTitle: UILabel!
    
    
    // MARK: - Overridden functions
    
    final override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        radioView.isSelected = selected
    }
    
    internal override func updateUi() {
        lblTitle.text = payment?.title ?? .empty
//        if (isRight) {
//            lblTitle.textAlignment = .right
//            mainPanal.semanticContentAttribute = .forceRightToLeft
//        } else {
//            lblTitle.textAlignment = .left
//            mainPanal.semanticContentAttribute = .forceLeftToRight
//        }
    }
    
    // MARK: - Actions

}
