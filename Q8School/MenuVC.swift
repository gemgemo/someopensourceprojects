
import UIKit

class MenuVC: UIViewController,
    UICollectionViewDelegate,
    UICollectionViewDataSource
{

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var menuCollection: UICollectionView!
    var name = ["Main","Department","Messages","About Us","Add Ads","My Ads","Favorite","Call Us","Settings"]
    var nameA = ["الرئسية","الاقسام","المرسلات","عنا","اضف اعلان","اعلاناتي","المفضلة","اتصل بنا","الاعدادات"]
    var image: [UIImage] = [
        UIImage(named: "home")!,
        UIImage(named: "section")!,
        UIImage(named: "message-1")!,
        UIImage(named: "aboutus")!,
        UIImage(named: "addads")!,
        UIImage(named: "ads")!,
        UIImage(named: "fav")!,
        UIImage(named: "contactus")!,
        UIImage(named: "ic_setting_x1")!]
    var userImages: String!
    var type:String!
    var images_cache = [String:UIImage]()
    let defaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()
        menuCollection.delegate = self
        menuCollection.dataSource = self
        userImage.layer.cornerRadius = 0.5 * userImage.bounds.size.width
        userImage.clipsToBounds = true
        type = UserDefaults.standard.string(forKey: "userType")

        



        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        let username = UserDefaults.standard.string(forKey: "userName")
        userNameLbl.text = username
        userImages = UserDefaults.standard.string(forKey: "userImage")
        if userImages == ""
        {
            userImage.image = UIImage(named: "pro_img")
        }
        else
        {
            load_image(userImages!, imageview: userImage)
            
        }
        self.menuCollection.reloadData()



    }
    func displayAlertMessage(_ Message:String)
    {
        var t = ""
        var b = ""
        var c = ""
        
        if Settings.lang == "ar"{
            t = "تنبيه"
            b = "اوافق"
            c = "الغاء"
        }
        else{
            t = "Alert"
            b = "Ok"
            c = "Cancel"
        }
        
            let myAlert = UIAlertController(title: t, message:Message, preferredStyle: UIAlertControllerStyle.alert);
            
            let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
                self.defaults.removeObject(forKey: "users_ID")
                self.defaults.removeObject(forKey: LoggedInKey)
                self.performSegue(withIdentifier: "signIn", sender: self)

                
            })
            myAlert.addAction(okAction);
        let cancelAction = UIAlertAction(title: c, style: UIAlertActionStyle.destructive, handler: nil)
            myAlert.addAction(cancelAction)
            
            self.present(myAlert, animated: true, completion: nil)
            
        
        
    }
    func displayAlerts(_ Massage: String)
    {
        var t = ""
        var b = ""
        
        if Settings.lang == "ar"{
            t = "تنبيه"
            b = "إغلاق"
        }
        else{
            t = "Alert"
            b = "Close"
        }
        
        let myAlert = UIAlertController(title: t, message:Massage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }


    func load_image(_ link:String, imageview:UIImageView)
    {
        
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.timeoutInterval = 10
        
        
        let task = session.dataTask(with: url, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
                
                guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                    
                    return
                }
                
                
                var image = UIImage(data: data!)
                
                if (image != nil)
                {
                    
                    
                    func set_image()
                    {
                        self.images_cache[link] = image
                        imageview.image = image
                    }
                    
                    
                    DispatchQueue.main.async(execute: set_image)
                    
                }
                
        })
        
        task.resume()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "menuCell", for: indexPath)
            as! Menu
                if Settings.lang == "ar"
                {
                    cell.menuLabel.text = nameA[indexPath.row]
                    cell.menuImage.image = image[indexPath.row]
                    cell.layer.addBorder(edge: UIRectEdge.bottom, color: UIColor(red: 222/255, green: 139/255, blue: 2/255, alpha: 1), thickness: 2)
        
                }
                else
                {
                    cell.menuLabel.text = name[indexPath.row]
                    cell.menuImage.image = image[indexPath.row]
                    cell.layer.addBorder(edge: UIRectEdge.bottom, color: UIColor(red: 222/255, green: 139/255, blue: 2/255, alpha: 1), thickness: 2)
                }


        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return name.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
//        let size = self.menuCollection.frame.width
        
        return CGSize(width: 230 , height: 40)
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            self.performSegue(withIdentifier: "mainPage", sender: self)
        }
        else if indexPath.row == 1
        {
            self.performSegue(withIdentifier: "departPage", sender: self)

        }
        else if indexPath.row == 2
        {
            self.performSegue(withIdentifier: "myMsg", sender: self)

        }
        else if indexPath.row == 3
        {
            self.performSegue(withIdentifier: "aboutUs", sender: self)
        }
        else if indexPath.row == 4
        {
            if type == "1"
            {
                if Settings.lang == "ar"
                {
                    let msg = "انت طالب لا يمكنك اضافة اعلان"
                    self.displayAlerts(msg)
                    
                }
                else
                {
                    let msg = "you'r student can't add Ads"
                    self.displayAlerts(msg)

                }
            
            }
            else
            {
                self.performSegue(withIdentifier: "AddAds", sender: self)
            }

        }
        else if indexPath.row == 5
        {
            if type == "1"
            {
                if Settings.lang == "ar"
                {
                    let msg = "انت طالب لا يمكنك اضافة اعلان"
                    self.displayAlerts(msg)
                    
                }
                else
                {
                    let msg = "you'r student can't add Ads"
                    self.displayAlerts(msg)
                    
                }
                
            }
            else
            {
                self.performSegue(withIdentifier: "MyAds", sender: self)
            }


        }
        else if indexPath.row == 6
            
        {

            self.performSegue(withIdentifier: "MyFav", sender: self)
            
        }
        else if indexPath.row == 7
        {
            self.performSegue(withIdentifier: "callUs", sender: self)
        }
        else
        {
            self.performSegue(withIdentifier: "settings", sender: self)

        }
    }

    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func logOutBtn(_ sender: UIButton) {
        if Settings.lang == "ar"
        {
            let msg = "هل تريد الخروج"
            self.displayAlertMessage(msg)
        }
        else
        {
            let msg = "Do you want to log out"
            self.displayAlertMessage(msg)
        }
    }

    @IBAction func editBtn(_ sender: UIButton)
    {
        self.performSegue(withIdentifier: "editProfile", sender: self)
    }
    
}
extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0,y: 0,width: self.frame.height,height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x: 0,y: self.frame.height - thickness,width: UIScreen.main.bounds.width, height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x: 0,y: 5,width: thickness,height: self.frame.height - 10)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x: self.frame.width - thickness,y: 0,width: thickness,height: self.frame.height)
            break
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        self.addSublayer(border)
    }
    
}

