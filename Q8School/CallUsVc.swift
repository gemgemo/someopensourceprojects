
import UIKit

class CallUsVc: UIViewController {

    @IBOutlet weak var myAdsImg: UIImageView!
    @IBOutlet weak var offerLbl: UILabel!
    @IBOutlet weak var myAdsLbl: UILabel!
    @IBOutlet weak var addAdsLbl: UILabel!
    @IBOutlet weak var callUsLbl: UILabel!
    @IBOutlet weak var loadingLbl: UILabel!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var openMenu: UIBarButtonItem!
    @IBOutlet weak var messageLbl: UITextField!
    @IBOutlet weak var phoneNumberTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var userNameLbl: UITextField!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var whatsAppLbl: UILabel!
    @IBOutlet weak var mobileLbl: UILabel!
    @IBOutlet weak var socialView: UIView!
    @IBOutlet weak var messageView: UIView!
    var dataFound: Bool!
    var msg: String!
    var youtube:String!
    var twitter:String!
    var facebook:String!
    var type: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        shadowView.isHidden = true
        loadingView.isHidden = true
        self.title = "call Us"

        socialLink(Settings.lang)
        userNameLbl.text = UserDefaults.standard.string(forKey: "userName")
        emailTxt.text = UserDefaults.standard.string(forKey: "userEmail")
        phoneNumberTxt.text = UserDefaults.standard.string(forKey: "userMobile")
        messageView.layer.cornerRadius = 4
        messageView.layer.masksToBounds = true
        socialView.layer.cornerRadius = 4
        socialView.layer.masksToBounds = true
        if revealViewController() != nil {
            
            revealViewController().rightViewRevealWidth = 235
            openMenu.target = revealViewController()
            openMenu.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        if Settings.lang == "ar"
        {
            callUsLbl.text = "اتصل بنا"
            addAdsLbl.text = "اضف اعلانك"
            myAdsLbl.text = "اعلاناتي"
            offerLbl.text = "العروض"
            self.title = "اتصل بنا"


        }
        type = UserDefaults.standard.string(forKey: "userType")
        if type == "1"
        {
            if Settings.lang == "ar"
            {
                myAdsLbl.text = "المفضلة"
                myAdsImg.image = UIImage(named: "fav")
                
            }
            else
            {
                myAdsLbl.text = "Favorite"
                myAdsImg.image = UIImage(named: "fav")
                
                
            }
        }




        // Do any additional setup after loading the view.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    func contactUs(_ lang:String,_ name:String,_ email:String,_ mobile:String,_ msg:String)
    {
        shadowView.isHidden = false
        loadingView.isHidden = false
        loadingView.layer.cornerRadius = 4
        loadingView.layer.masksToBounds = true
        loadingActivity.startAnimating()
        let request = NSMutableURLRequest(url: URL(string:Settings.ContactUs)!)
        request.httpMethod = "POST"
        let postString = "lang=\(lang)&name=\(name)&email=\(email)&mobile=\(mobile)&msg=\(msg)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
            
            if let responseData = data {
                
                do {
                    
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                    print("ddd\(json)")
                    self.dataFound = json["Success"] as? Bool
                    if self.dataFound!
                    {
                      self.msg = json["Message"] as! String
                    }

                    else
                    {
                        self.msg = json["Message"] as! String
 
                    }
                    
                    DispatchQueue.main.async
                    {
                      if self.dataFound!
                      {
                        self.shadowView.isHidden = true
                        self.loadingView.isHidden = true
                        self.loadingActivity.stopAnimating()
                        self.displayAlert(self.msg)
                      }
                      else
                      {
                        self.shadowView.isHidden = true
                        self.loadingView.isHidden = true
                        self.loadingActivity.stopAnimating()
                        self.displayAlert(self.msg)

                      }
                    }
                    
                }
                catch
                {
                    print ("Could not Serialize")
                }
                
            }
            
        }
        task.resume()
        
    }
    func socialLink(_ lang: String)
    {
        let request = NSMutableURLRequest(url: URL(string:Settings.AboutUs)!)
        request.httpMethod = "POST"
        let postString = "lang=\(lang)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
            
            if let responseData = data {
                
                do {
                    
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                    print("ddd\(json)")
                    DispatchQueue.main.async {
                            self.dataFound = json["Success"] as? Bool
                            if (self.dataFound!) {
                                let data = json["Result"] as! NSDictionary
                                self.facebook = data["facebook"] as! String
                                self.youtube = data["instagram"] as! String
                                self.twitter = data["twitter"] as! String
                                self.whatsAppLbl.text = data["whatsapp"] as? String
                                self.emailLbl.text = data["email"] as? String
                                self.mobileLbl.text = data["phone"] as? String
                            }
                    }
                    
                } catch {
                    print ("Could not Serialize")
                }
            }
        }
        task.resume()
    }
    
    func displayAlert(_ Massage: String) {
        var t = ""
        var b = ""
        
        if Settings.lang == "ar"{
            t = "تنبيه"
            b = "إغلاق"
        }
        else{
            t = "Alert"
            b = "Close"
        }
        
        let myAlert = UIAlertController(title: t, message:Massage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default,handler: { (action: UIAlertAction!) in
            
        })
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }
    func displayAlerts(_ Massage: String)
    {
        var t = ""
        var b = ""
        
        if Settings.lang == "ar"{
            t = "تنبيه"
            b = "إغلاق"
        }
        else{
            t = "Alert"
            b = "Close"
        }
        
        let myAlert = UIAlertController(title: t, message:Massage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }
    
    @IBAction func submitBtn(_ sender: Any) {
        contactUs(Settings.lang, userNameLbl.text!, emailTxt.text!, phoneNumberTxt.text!, messageLbl.text!)
    }
    
    @IBAction func facebookBtn(_ sender: UIButton) {
        UIApplication.shared.open(URL(string:"https://\(self.facebook!)")!, options: [:], completionHandler: nil)
    }
    
    @IBAction func twitterBtn(_ sender: UIButton) {
        UIApplication.shared.open(URL(string:"https://\(self.twitter!)")!, options: [:], completionHandler: nil)
    }

    @IBAction func youtubeBtn(_ sender: UIButton) {
        UIApplication.shared.open(URL(string:"https://\(self.youtube!)")!, options: [:], completionHandler: nil)
    }
    
    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MainPageViewController") as! MainPageViewController
        self.navigationController?.pushViewController(vc, animated: false)

    }
    
    @IBAction func callUsBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CallUsVc") as! CallUsVc
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func addAdsBtn(_ sender: Any) {
        if type == "1"
        {
            if Settings.lang == "ar"
            {
                let msg = "انت طالب لا يمكنك اضافة اعلان"
                self.displayAlerts(msg)
                
            }
            else
            {
                let msg = "you'r student can't add Ads"
                self.displayAlerts(msg)
                
            }
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AddAdsVc") as! AddAdsVc
            self.navigationController?.pushViewController(vc, animated: false)
            
        }

    }
    
    @IBAction func myAdsBtn(_ sender: Any) {
        if type == "1"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyFavoriteVC") as! MyFavoriteVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyAdsVC") as! MyAdsVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
    }
    
    @IBAction func offerBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AdsPageViewController") as! AdsPageViewController
        self.navigationController?.pushViewController(vc, animated: false)

    }
    
}
