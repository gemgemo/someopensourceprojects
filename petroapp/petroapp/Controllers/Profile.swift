
import UIKit
import Gemo
import GoogleMaps
import MobileCoreServices


class Profile: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblBalance: UILabel!
    @IBOutlet weak var lblCurrentBalance: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhoneNo: UILabel!
    @IBOutlet weak var btnEditPassword: UIButton!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnEditProfile: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        super.updateUi()
        setupNavigationBar(isLogo: false, isMainPage: true, title: Localization.MyAccount.localized())
        lblUserName.text = "\(Localization.UserName.localized())   \(UserDefaults.standard.string(forKey: Misc.Username) ?? .empty)"
        lblEmail.text = "\(Localization.Email.localized())   \(UserDefaults.standard.string(forKey: Misc.Email) ?? .empty)"
        lblPhoneNo.text = "\(Localization.PhoneNumber.localized())   \(UserDefaults.standard.string(forKey: Misc.Phone) ?? .empty)"
        btnEditPassword.setTitle(Localization.UpdatePassword.localized(), for: .normal)
        lblLocation.text = Localization.Location.localized()
        btnEditProfile.setTitle(Localization.EditMyData.localized(), for: .normal)
        btnEditProfile.setCorner(radius: 3)
        lblCurrentBalance.text = Localization.CurrentBalance.localized()
        lblName.text = UserDefaults.standard.string(forKey: Misc.Name) ?? .empty
        if let latitude = UserDefaults.standard.string(forKey: Misc.Latitude)?.toDouble, let longitude = UserDefaults.standard.string(forKey: Misc.Longitude)?.toDouble {
            let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            GMSGeocoder().reverseGeocodeCoordinate(coordinate) { [weak self] (response, error) in
                if (error != nil) {
                    print("get address error: \(string(describing: error))")
                    return
                }
                Threads.main { [weak self] in
                    self?.lblAddress.text = response?.firstResult()?.lines?.joined(separator: ",") ?? .empty
                }
            }
            mapView.clear()
            let marker = GMSMarker(position: coordinate)
            marker.map = mapView
            marker.icon = #imageLiteral(resourceName: "marker-map2")
            marker.appearAnimation = .pop
            mapView.camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: 16.0)
        }
        userPhoto.loadImage(from: UserDefaults.standard.string(forKey: Misc.Photo) ?? .empty)
        userPhoto.rounded(cornerRadius: userPhoto.bounds.height/2)
    }
    
    
    
    
    // MARK: - Actions
        
    @IBAction func editPasswordOnClick(_ sender: UIButton) {
        guard let editPasswordPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.EditPassword) as? EditPassword else {
            return
        }
        navigate(to: editPasswordPage)
    }
    
    @IBAction func editProfileOnCLick(_ sender: UIButton) {
        guard let editProfilePage = storyboard?.instantiateViewController(withIdentifier: Storyboards.EditProfile) as? EditProfile else {
            return
        }
        navigate(to: editProfilePage)
    }
    
    
    
}






















