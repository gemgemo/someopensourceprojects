
import UIKit
import Gemo


internal var isNotificationsPresentd = false
final class Notifications: BaseController
{
    
    // MARK: - Constatnts
    
    
    // MARK: - Variables
    
    fileprivate var dataSource = Array<OrderNotification>() { didSet {
        Threads.main { [weak self] in
            self?.tblNotifications.reloadData()
        }}}
    fileprivate var refersher: UIRefreshControl?
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var tblNotifications: UITableView! { didSet {
        tblNotifications.delegate = self
        tblNotifications.dataSource = self
        }}
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        setupMenuButton()
        prepareTableView()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        selectedTabBarItem = 2
        tabBarItems?.selectItem(at: IndexPath(item: selectedTabBarItem, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        loadDate()
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isNotificationsPresentd = true
        NotificationCenter.default.addObserver(self, selector: #selector(fetchNotifications), name: .RefreshNotifications, object: nil)
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        isNotificationsPresentd = false
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    internal override func updateUi() {
        navigationItem.title = Localization.Notifications.localized()        
    }
    
    
    // MARK: - Actions
    
    @objc fileprivate func acceptOrderOnClick(_ sender: UIButton)-> void {
        let notification = dataSource[sender.tag]
        OrderNotification.action(this: notification.id ?? .empty, with: OrderStatus.accept.rawValue) { [weak self] (order) in
            Threads.main { [weak self] in
                self?.notifiyUser(with: Localization.AddToOrdersMessage.localized()) { [weak self] in
                    self?.navigateToOrders(lastOrders: false)
                }
            }
        }
    }

    @objc fileprivate func rejectOrderOnCLick(_ sender: UIButton)-> void {
        let notification = dataSource[sender.tag]
        OrderNotification.action(this: notification.id ?? .empty, with: OrderStatus.reject.rawValue) { [weak self] (order) in
            self?.fetchNotifications()
        }
    }
    
    @objc fileprivate func refreshNotifications(_ sender: UIRefreshControl)-> void {
        fetchNotifications()
    }
    
    
}

// MARK:- Helper functions

extension Notifications
{
    
    fileprivate func prepareTableView()-> void {
        tblNotifications.alwaysBounceVertical = true
        tblNotifications.register(Nibs.Notification, forCellReuseIdentifier: ReuseIdentifiers.Notification)
        let insets = Insets(top: 0, left: 0, bottom: 60, right: 0)
        tblNotifications.contentInset = insets
        tblNotifications.scrollIndicatorInsets = insets
        refersher = UIRefreshControl()
        refersher?.addTarget(self, action: #selector(refreshNotifications(_:)), for: .valueChanged)
        tblNotifications.addSubview(refersher!)
        
    }
    
    fileprivate func notifiyUser(with message: String, _ completion: @escaping()->())-> void {
        guard let window = UIApplication.shared.keyWindow else { return }
        let overlayView = UIView(frame: UIScreen.main.bounds)
        overlayView.backgroundColor = Color.black.withAlphaComponent(0.4)
        overlayView.alpha = 0
        window.addSubview(overlayView)
        let messageView = UIView()
        messageView.backgroundColor = .white
        messageView.translatesAutoresizingMaskIntoConstraints = false
        messageView.alpha = 0
        messageView.setCorner(radius: 10.0)
        window.addSubview(messageView)
        messageView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height * 0.50).isActive = true
        messageView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width - 30).isActive = true
        messageView.centerXAnchor.constraint(equalTo: window.centerXAnchor).isActive = true
        messageView.centerYAnchor.constraint(equalTo: window.centerYAnchor).isActive = true
        let icon = UIImageView()
        icon.image = #imageLiteral(resourceName: "order")
        icon.contentMode = .scaleAspectFit
        icon.translatesAutoresizingMaskIntoConstraints = false
        messageView.addSubview(icon)
        icon.topAnchor.constraint(equalTo: messageView.topAnchor, constant: 8).isActive = true
        icon.widthAnchor.constraint(equalToConstant: messageView.frame.width * 0.80).isActive = true
        icon.heightAnchor.constraint(equalToConstant: messageView.frame.height * 0.60).isActive = true
        icon.centerXAnchor.constraint(equalTo: messageView.centerXAnchor, constant: 0).isActive = true
        let label = UILabel()
        label.font = Font.boldSystemFont(ofSize: 20.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        label.textAlignment = .center
        messageView.addSubview(label)
        label.topAnchor.constraint(equalTo: icon.bottomAnchor, constant: 8).isActive = true
        label.rightAnchor.constraint(equalTo: messageView.rightAnchor, constant: -8).isActive = true
        label.bottomAnchor.constraint(equalTo: messageView.bottomAnchor, constant: 0).isActive = true
        label.text = message
        label.leftAnchor.constraint(equalTo: messageView.leftAnchor, constant: 8).isActive = true
        let animator = UIViewPropertyAnimator(duration: 3.5, curve: .easeIn) {
            overlayView.alpha = 1.0
            messageView.alpha = 1.0
        }
        animator.startAnimation()
        /*Threads.delay(after: 0.5) {
            animator.addAnimations {
                overlayView.alpha = 0.0
                messageView.alpha = 0.0
            }
        }*/
        animator.addCompletion { (position) in
            overlayView.removeFromSuperview()
            messageView.removeFromSuperview()
            completion()
        }
        
    }
    
    fileprivate func loadDate()-> void {
        if (dataSource.isEmpty) {
            fetchNotifications()
        }
    }
    
    @objc fileprivate func fetchNotifications()-> void {
        if (Gemo.gem.isConnected) {
            dataSource.removeAll()
            OrderNotification.fetchNotifications { [weak self] (notifications) in
                if (!notifications.isEmpty) {
                    self?.dataSource = notifications
                }
                Threads.main { [weak self] in
                    self?.refersher?.endRefreshing()
                }
            }
        } else {
            refersher?.endRefreshing()
            dialogUserWithNoInternetConnectionMessage()
        }
    }
    
}


// MARK:- Table view delegate & data source functions

extension Notifications: UITableViewDelegate, UITableViewDataSource {
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifiers.Notification, for: indexPath) as? NotificationCell else { return UITableViewCell() }
        cell.item = dataSource[indexPath.row]
        cell.btnAccept.tag = indexPath.row
        cell.btnReject.tag = indexPath.row
        cell.btnAccept.addTarget(self, action: #selector(acceptOrderOnClick(_:)), for: .touchUpInside)
        cell.btnReject.addTarget(self, action: #selector(rejectOrderOnCLick(_:)), for: .touchUpInside)
        return cell
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: cgFloat = 70 + 180 + 16 + 50 + 20 + 30
        let products = dataSource[indexPath.row].products
        if (!products.isEmpty) {
            height += (products.count*80).toCGFloat
        }
        return height
    }
    
}

















