//
//  BannerViewerViewController.swift
//  KWTSads
//
//  Created by Mostafa on 8/20/16.
//  Copyright © 2016 Mostafa. All rights reserved.
//

import UIKit
import Social
import MessageUI

class BannerViewerViewController: UIViewController ,MFMessageComposeViewControllerDelegate ,MFMailComposeViewControllerDelegate{
    var selectedImage:String!
    var images_cache = [String:UIImage]()
    var bannerNames:String!
    var bannermob1:String!
    var bannermob2:String!
    var department = [Department]()
    var bannerString = "ولمعرفة مزيد من الاعلانات تفضل بتحميل برنامج كويت ادز "

    @IBOutlet weak var bannerImage: UIImageView!


    override func viewDidLoad() {
        super.viewDidLoad()
        print("selected image is: \(selectedImage)")
        if Reachability.isConnectedToNetwork() == true {
            self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "ac_back-arrow")
            self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "ac_back-arrow")
            self.navigationController?.navigationBar.tintColor = UIColor(red: 0.96, green: 0.69, blue:  0.25, alpha: 1 )
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage
            DispatchQueue.main.async { self.load_image(self.selectedImage, imageview:self.bannerImage) }
        } else {
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage
            
            let err = "Internet connection FAILED"
            self.displayAlertMessage(err)
            
        }
    }
    
    func load_image(_ link:String, imageview:UIImageView) {
        
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.timeoutInterval = 10
        
        
        let task = session.dataTask(with: url, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
            
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                
                return
            }
            
            
            var image = UIImage(data: data!)
            
            if (image != nil)
            {
                
                
                func set_image()
                {
                    self.images_cache[link] = image
                    imageview.image = image
                }
                
                
                DispatchQueue.main.async(execute: set_image)
                
            }
            
        }) 
        
        task.resume()
    }
    func displayAlertMessage(_ userMessage:String)
    {
        let myAlert = UIAlertController(title: "Alert", message:userMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func post(toService service: String) {
        if(SLComposeViewController.isAvailable(forServiceType: service)) {
            let socialController = SLComposeViewController(forServiceType: service)
            socialController?.setInitialText(bannerNames+"\n"+bannerString)
            self.present(socialController!, animated: true, completion: nil)
        }
    }

    
    @IBAction func bannerFacebook(_ sender: AnyObject) {
        post(toService: SLServiceTypeFacebook)

    }

    @IBAction func bannerWhatsapp(_ sender: AnyObject) {
        let urlString = bannerNames+"\n"+bannerString
        let urlStringEncoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let url  = URL(string: "whatsapp://send?text=\(urlStringEncoded!)!")
        
        if UIApplication.shared.canOpenURL(url!) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            
        } else {
            self.displayAlertMessage("Your device is not able to send WhatsApp messages")
        }

    }
    
    @IBAction func bannerTwitter(_ sender: AnyObject) {
        post(toService: SLServiceTypeTwitter)

    }
    
    @IBAction func bannerSMS(_ sender: AnyObject) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = bannerNames+"\n"+bannerString
            controller.recipients = ["    "]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
        else
        {
            self.displayAlertMessage("Your device is not able to send  messages")
            

    }
    }
    
    @IBAction func bannerGmail(_ sender: AnyObject) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = bannerNames+"\n"+bannerString
            controller.recipients = ["    "]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
        else
        {
            self.displayAlertMessage("Your device is not able to send  messages")
            

        
        }
    
   }
    
    @IBAction func callMob1(_ sender: AnyObject) {
        if let url = URL(string: "tel://\(bannermob1!)") {
            print(bannermob1)
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func callMob2(_ sender: AnyObject) {
        
            if let url = URL(string: "tel://\(bannermob2!)") {
                print(bannermob2)
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }

        
    }
    @IBAction func bannerWebsite(_ sender: AnyObject) {
        
        UIApplication.shared.open(NSURL(string:"\(self.bannerNames!)") as! URL, options: [:], completionHandler: nil)

    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }


}
