
import UIKit
import Gemo

class SendComplaint: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var txfName: TextField! { didSet { txfName.delegate = self } }
    @IBOutlet weak var txfEmail: TextField! { didSet { txfEmail.delegate = self } }
    @IBOutlet weak var txfComplaintTitle: TextField! { didSet { txfComplaintTitle.delegate = self } }
    @IBOutlet weak var txfComplaintText: TextField! { didSet { txfComplaintText.delegate = self } }
    @IBOutlet weak var btnSend: UIButton!
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        super.updateUi()
        setupNavigationBar(isLogo: false, isMainPage: true, title: Localization.SendComplaint.localized())
        btnSend.setCorner(radius: 3)
        btnSend.setTitle(Localization.Send.localized(), for: .normal)
        txfName.hint = Localization.Name.localized()
        txfEmail.hint = Localization.Email.localized()
        txfComplaintText.hint = Localization.ComplaintText.localized()
        txfComplaintTitle.hint = Localization.ComplaintTitle.localized()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    // MARK: - Actions

    @IBAction func sendComplaintOnClick(_ sender: UIButton) {
        view.endEditing(true)
        guard validation().isEmpty else {
            displayAlert(with: validation())
            return
        }
        let paramters: Dictionary<string, any> = [
            Misc.MainKey: Misc.MainValue,
            Misc.LanguageKey: Localizer.instance.current.rawValue,
            "user_id": UserDefaults.standard.string(forKey: Misc.UserId) ?? .empty,
            "f_name": name,
            "email": email,
            "subject": subject,
            "message": message
        ]
        Gemo.gem.startSpinning()
        Http.request(link: Misc.getUrl(for: "comp"), method: .post, parameters: paramters)
            .response { [weak self] (response) in
                let error = response.error
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("send complaint error: \(string(describing: error))")
                    self?.displayAlert(with: error?.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
                guard let json = response.result as? Dictionary<string, any>, let message = json["data"] as? string else {
                    self?.displayAlert(with: Localization.NoData.localized())
                    return
                }
                self?.displayAlert(with: message)
        }
    }
    
}

// MARK: - Helper functions 

fileprivate extension SendComplaint {
    
    func validation()-> string {
        guard Gemo.gem.isConnected else {
            return Localization.InternetConnection.localized()
        }
        guard !name.isEmpty else {
            return Localization.UserNameReqiured.localized()
        }
        guard email.isEmail else {
            return Localization.InvaildEmail.localized()
        }
        guard !subject.isEmpty, !message.isEmpty else {
            return Localization.SomeDataReqiured.localized()
        }
        return .empty
    }
}


// MARK: - Computed properties

fileprivate extension SendComplaint {
    
    var name: string {
        return txfName.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    var email: string {
        return txfEmail.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    var subject: string {
        return txfComplaintTitle.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    var message: string {
        return txfComplaintText.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
}


// MARK: - Text Field delegate functions

extension SendComplaint: UITextFieldDelegate {
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}


















