
import UIKit
import Gemo
import Firebase
import UserNotifications

final class SignIn: BaseController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var footerPanel: UIView! {
        didSet {
            footerPanel.setShadow(with: .zero, radius: 2, opacity: 0.4, color: Color(white: 0.88, alpha: 0.9))
        }
    }
    @IBOutlet fileprivate weak var txfUsername: TextField! {
        didSet {
            txfUsername.delegate = self
        }
    }
    @IBOutlet fileprivate weak var txfSPassword: TextField! {
        didSet {
            txfSPassword.delegate = self
        }
    }
    @IBOutlet fileprivate weak var btnSReset: UIButton!
    @IBOutlet fileprivate weak var btnSLogin: UIButton!
    @IBOutlet fileprivate weak var btnSRegister: UIButton!
    @IBOutlet fileprivate weak var sRegisterIcon: UIImageView!
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()        
        navigationController?.navigationBar.isHidden = true       
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    
    // MARK: - Actions
    
    @IBAction private func resetStorePasswordOnClick(_ sender: UIButton) {
        let alert = UIAlertController(title: Localization.UpdatePassword.localized(), message: Localization.EnterPasswordMessage.localized(), preferredStyle: .alert)
        alert.view.tintColor = .main
        alert.addTextField { (textField) in
            textField.placeholder = Localization.Email.localized()
            textField.textAlignment = .right
        }
        alert.addAction(UIAlertAction(title: Localization.Submit.localized(), style: .default, handler: { [weak self] (action) in
            guard let this = self else { return }
            if (Gemo.gem.isConnected) {
                let email = alert.textFields?[0].text?.trimmingCharacters(in: .whitespaces) ?? .empty
                guard email.isEmail && !email.isEmpty else {
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InvaildEmail.localized())
                    return
                }
                let parameters: [string: any] = [
                    Misc.MainKey: Misc.MainValue,
                    "email": email
                ]
                print("rest paramters is: \(parameters)")
                this.resetPassword(withParameters: parameters)
            } else {
                self?.dialogUserWithNoInternetConnectionMessage()
            }
        }))
        alert.addAction(UIAlertAction(title: Localization.Cancel.localized(), style: .cancel))
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction private func StoreSignInOnCLick(_ sender: UIButton) {
        if (Gemo.gem.isConnected) {
            guard !userName.isEmpty else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.UserNameReqiured.localized())
                return
            }
            guard !password.isEmpty else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.PasswordReqiured.localized())
                return
            }
            
            guard !(password.characters.count < 6) else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.PasswordCount.localized())
                return
            }
            let params: [string: any] = [
                Misc.MainKey: Misc.MainValue,
                "username": userName,
                "password": password
            ]
            print(params)            
            login(withParameters: params)
        } else {
            dialogUserWithNoInternetConnectionMessage()
        }
    }
    
    @IBAction private func StoreSignUpOnClick(_ sender: UIButton) {
        guard let signUpScreen = storyboard?.instantiateViewController(withIdentifier: Storyboards.SignUp) as? SignUp  else { return }
        signUpScreen.isUpdate = false
        navigate(to: signUpScreen)
    }


}


// MARK: - Text Field delegate functions

extension SignIn: UITextFieldDelegate
{
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}



// MARK: - Computed Properties

extension SignIn
{
    
    fileprivate var userName: string {
        return txfUsername.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var password: string {
        return txfSPassword.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    
}


// MARK: - Helper Functions

extension SignIn
{
    
    internal override func updateUi()-> void {
        btnSLogin.setCorner(radius: 20.0)
        btnSLogin.setShadow(with: .zero, radius: 0.4, opacity: 0.9, color: #colorLiteral(red: 0.3008661568, green: 0.4334716499, blue: 0.5458672643, alpha: 1))
        footerPanel.setShadow(with: .zero, radius: 0.5, opacity: 0.9, color: #colorLiteral(red: 0.8155969381, green: 0.8157377839, blue: 0.815588057, alpha: 1))
        navigationController?.navigationBar.isHidden = true
        tabBar.isHidden = true
        if (Localizer.instance.current == .arabic) {
            txfUsername.textAlignment = .right
            txfSPassword.textAlignment = .right
            footerPanel.semanticContentAttribute = .forceRightToLeft
            btnSReset.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16.0).isActive = true
            sRegisterIcon.rightAnchor.constraint(equalTo: footerPanel.rightAnchor, constant: -12.0).isActive = true
        } else {
            txfUsername.textAlignment = .left
            txfSPassword.textAlignment = .left
            footerPanel.semanticContentAttribute = .forceLeftToRight
            btnSReset.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16.0).isActive = true
            sRegisterIcon.leftAnchor.constraint(equalTo: footerPanel.leftAnchor, constant: 12.0).isActive = true
        }
        txfUsername.placeholder = Localization.UserName.localized()
        txfSPassword.placeholder = Localization.Password.localized()
        btnSLogin.setTitle(Localization.Login.localized(), for: .normal)
        btnSRegister.setTitle(Localization.RegisterButton.localized(), for: .normal)
        btnSReset.setTitle(Localization.ForgotPassword.localized(), for: .normal)
        btnSRegister.titleLabel?.font = Font.systemFont(ofSize: 10)
    }       
    
    fileprivate func resetPassword(withParameters params: Dictionary<string, any>)-> void {
        Gemo.gem.startSpinning()
        Http.request(link: Urls.ResetPassword, method: .post, parameters: params)
            .response { (response) in
                let error = response.error
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("reset password error: \(string(describing: error))")
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
                print("reset password response: \(response.jsonString ?? .empty)")
                guard let json = (response.result as? [Dictionary<string, any>])?.first,
                    let _ = json["result"] as? string,
                    let message = json["data"] as? string else {
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized())
                    return
                }
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: message)
        }
    }
    
   
    fileprivate func login(withParameters params: Dictionary<string, any>)-> void {
        Grocery.signIn(with: params) { [weak self] (store) in
            print("store info is: \(store)")
            Threads.main { [weak self] in
                UserDefaults.standard.set(true, forKey: Misc.UserLoggedIn)
                self?.save(this: store)
                self?.configureRemoteNotifications()
                guard let token = FIRInstanceID.instanceID().token() else { return }
                let parameters: Dictionary<string, any> = [
                    Misc.MainKey: Misc.MainValue,
                    "user_id": UserDefaults.standard.string(forKey: Misc.GroceryId) ?? .empty,
                    "d_token": token,
                    "type": 1
                ]
                Http.request(link: Urls.AddToken, method: .post, parameters: parameters)
                    .response { (response) in
                        print("device token json string: \(response.jsonString ?? .empty)")
                        if (response.error != nil) {
                            print("send device token error \(string(describing: response.error))")
                            return
                        }
                        print("send device token data \(string(describing: response.result))")
                }
                UIApplication.shared.keyWindow?.rootViewController = UIStoryboard(name: Storyboards.Main, bundle: nil).instantiateInitialViewController()
            }
        }
    }
}
































