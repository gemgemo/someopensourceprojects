
import UIKit
import Gemo

class Reports: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    var cars: Dictionary<int, CarCell> = [:]
    private var timeType = TimeType.all, isFrom = false
    enum TimeType: int { case all = 0, time = 1 }
    fileprivate var fromDate = "" , toDate = "", dataSource = Array<Report>()
    
    // MARK: - Outlets
    
    @IBOutlet weak var tblReports: UITableView! { didSet {
        tblReports.delegate = self
        tblReports.dataSource = self
        }}
    @IBOutlet weak var headerPanel: UIView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblSpentFuel: UILabel!
    @IBOutlet weak var btnFrom: UIButton!
    @IBOutlet weak var btnTo: UIButton!
    @IBOutlet weak var btnChooseTime: UIButton!
    @IBOutlet weak var datePanel: UIView!
    @IBOutlet weak var datePanelHeight: NSLayoutConstraint!
    @IBOutlet weak var datePickerPanel: UIView! { didSet { datePickerPanel.isHidden = true } }
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        datePanel.isHidden = true
        datePanelHeight.constant = 0.0
        headerPanel.bounds.size.height -= 40
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //print("cars======----->", cars)
        [btnTo, btnFrom].forEach {
            $0?.setBorder(with: 1, and: Color(white: 0.88, alpha: 0.9))
            $0?.setCorner(radius: 4)
        }
    }
    
    internal override func updateUi() {
        super.updateUi()
        setupNavigationBar(isLogo: false, isMainPage: false, title: Localization.Reports.localized())
        btnChooseTime.setTitle(Localization.All.localized(), for: .normal)
        btnFrom.setTitle(Localization.From.localized(), for: .normal)
        btnTo.setTitle(Localization.To.localized(), for: .normal)
        lblSpentFuel.text = Localization.SpentLitresNo.localized()
        lblPrice.text = Localization.AmountsSpent.localized()
        fetchReports()
    }
    
    // MARK: - Actions
    
    @IBAction func chooseTimeOnCLick(_ sender: UIButton) {
        let items: [PopupMainMenu] = [ PopupMainMenu(title: Localization.All.localized()),
                                       PopupMainMenu(title: Localization.SelectTime.localized()) ]
        let rect = sender.convert(sender.frame, to: view)
        showPopover(in: rect, with: items, with: Size(width: sender.bounds.width, height: 90)) { [weak self] (index) in
            sender.setTitle(items[index].title, for: .normal)
            switch (index) {
            case 0: // all
                self?.timeType = .all
                self?.datePanel.isHidden = true
                self?.datePanelHeight.constant = 0.0
                self?.headerPanel.bounds.size.height -= 40
                self?.fetchReports()
            case 1: // select time
                self?.timeType = .time
                self?.datePanel.isHidden = false
                self?.datePanelHeight.constant = 40.0
                self?.headerPanel.bounds.size.height += 40
                self?.fetchReports(from: self!.fromDate, to: self!.toDate)
            default:
                break
            }
        }

    }
    
    @IBAction func selectFromDateOnCLick(_ sender: UIButton) {
        isFrom = true
        datePickerPanel.isHidden = false
    }
    
    @IBAction func selectToDateOnCLick(_ sender: UIButton) {
        isFrom = false
        datePickerPanel.isHidden = false
    }
    
    @IBAction func setDateOnCLick(_ sender: UIButton) {
        datePickerPanel.isHidden = true
        print("date========> \(date)")
        if (isFrom) {
            fromDate = date
            btnFrom.setTitle(date, for: .normal)
        } else {
            toDate = date
            btnTo.setTitle(date, for: .normal)
        }
    }

}

// MARK: - Helper functions

fileprivate extension Reports {
    
    func fetchReports(from: String = .empty, to: string = .empty) -> void {
        guard Gemo.gem.isConnected else {
            displayAlert(with: Localization.InternetConnection.localized())
            return
        }
        var carsIds = [string]()
        for carCell in cars.values {
            carsIds.append(carCell.car?.id ?? .empty)
        }
        let paramters: Dictionary<string, any> = [
            Misc.MainKey: Misc.MainValue,
            Misc.LanguageKey: Localizer.instance.current.rawValue,
            "user_id": UserDefaults.standard.string(forKey: Misc.UserId) ?? .empty,
            "date_from": fromDate,
            "date_to": toDate,
            "car_id": carsIds
        ]
        Gemo.gem.startSpinning()
        Http.request(link: Misc.getUrl(for: "all_report"), method: .post, parameters: paramters)
            .response { [weak self] (response) in
                let error = response.error
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("\(#function) error: \(error!)")
                    self?.displayAlert(with: error?.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
                guard let json = response.result as? Dictionary<string, any> else {
                    self?.displayAlert(with: Localization.NoData.localized())
                    return
                }
                let reports = ReportsRes(JSON: json)
                self?.dataSource = reports.reports
                Threads.main { [weak self] in
                    guard let this = self else { return }
                    self?.lblPrice.text = "\(Localization.AmountsSpent.localized())   \(reports.reportsTotalData?.price ?? .empty)"
                    self?.lblSpentFuel.text = "\(Localization.SpentLitresNo.localized())   \(reports.reportsTotalData?.liters ?? .empty)"
                    if (!this.dataSource.isEmpty) {
                        self?.tblReports.reloadData()
                    }
                }
        }
    }
    
}



// MARK: - Computed prperties

fileprivate extension Reports {
    
    var date: string {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = "YYYY-MM-dd"
        return formatter.string(from: datePicker.date)
    }
}


// MARK: - Table views delegate & data source functions

extension Reports: UITableViewDelegate, UITableViewDataSource {
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifiers.Report, for: indexPath) as! ReportCell
        cell.report = dataSource[indexPath.row]
        return cell
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected cell: \(indexPath)")
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180.0
    }
    
}






















