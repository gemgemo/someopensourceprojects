
import Foundation
import Gemo



final class Order: Mapper<any>
{
    
    internal var storeId, userId, enterdPhone, gender, username, userPhoto, id, name, phone, building, floor, address, type, typeStatus, time, date: string?, products = Array<NotificationProduct>()
    
    internal override var description: String {
        return "\n username: \(username ?? .empty), enteredPhone: \(enterdPhone ?? .empty), phone: \(phone ?? .empty) \n"
    }
    
    internal override func mapping(mapper: Mapper<any>) {
        storeId = mapper["id_matjer"]?.string
        userId = mapper["user_id"]?.string
        gender = mapper["gender"]?.string
        enterdPhone = mapper["phone_user"]?.string
        username = mapper["user_name"]?.string
        userPhoto = mapper["user_photo"]?.string
        id = mapper["id"]?.string
        name = mapper["name"]?.string
        phone = mapper["phone"]?.string
        building = mapper["building"]?.string
        floor = mapper["floor"]?.string
        address = mapper["addres"]?.string
        type = mapper["type_send"]?.string
        time = mapper["time_add"]?.string
        date = mapper["date_add"]?.string
        typeStatus = mapper["type_status"]?.string
        if let prods = mapper["Data"]?.array {
            prods.forEach { (objc) in
                guard let product = objc as? Dictionary<string, any> else { return }
                products.append(NotificationProduct(JSON: product))
            }
        }
    }
    
    
    internal class func fetchOrders(by link: string, with parameters: Dictionary<string, any>, _ completion: @escaping([Order])->())-> void {
        Gemo.gem.startSpinning()
        Http.request(link: link, method: .post, parameters: parameters)
            .response { (response) in
                let error = response.error
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("fetch orders error: \(String(describing: error))")
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
//                print("fetched orders: \(response.jsonString ?? .empty)")
                guard let json = response.result as? Array<Dictionary<string, any>> else {
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized())
                    return
                }
                completion(json.map { Order(JSON: $0) })
        }
    }
    
    
    internal class func action(this key: string, for id: string, _ completion: @escaping([Order])->())-> void {
        Gemo.gem.startSpinning()
        let parameters: [string: any] = [
            Misc.MainKey: Misc.MainValue,
            Misc.LanguageParameterName: Localizer.instance.current.rawValue,
            "id_matjer": UserDefaults.standard.string(forKey: Misc.GroceryId) ?? .empty,
            "id": id,
            key: key
        ]
        //print("action paramters: \(parameters)")
        Http.request(link: Urls.CurrentOrders, method: .post, parameters: parameters)
            .response { (response) in
                let error = response.error
                print("action response---------->: \(response.jsonString ?? .empty)")
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("action error: \(String(describing: error))")
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
                guard let json = response.result as? [Dictionary<string, any>] else {
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized())
                    return
                }
                completion(json.map { Order(JSON: $0) })
        }
    }
    
}












































