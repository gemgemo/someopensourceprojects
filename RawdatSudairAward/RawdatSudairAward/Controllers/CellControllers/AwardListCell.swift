
import UIKit
import Localize_Swift

final class AwardListCell: UITableViewCell, CellProvider
{

    // MARK: - Constants
    
    //MARK: - Variables
    
    internal var item: AwardListItem? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var picture: UIImageView!
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var mainPanal: UIView!
    
    
    // MARK: - Overridden functions
    
    internal func updateUI() {
        if let title = item?.title {
            lblTitle.text = title
            lblTitle.textAlignment = (Localize.currentLanguage() == Constants.Language.Arabic) ? .right : .left
        }
        picture.image = item?.icon
        mainPanal.semanticContentAttribute = (Localize.currentLanguage() == Constants.Language.Arabic) ? .forceLeftToRight : .forceRightToLeft
    }
    
    //MARK: - Actions
}
