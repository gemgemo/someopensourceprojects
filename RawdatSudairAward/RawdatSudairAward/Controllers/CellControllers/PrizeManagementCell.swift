
import UIKit
import Localize_Swift

final class PrizeManagementCell: UITableViewCell, CellProvider
{
    // MARK: - Constants
    
    //MARK: - Variables
    
    internal var person: Person? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var picture: UIImageView! {
        didSet {
            picture.clipsToBounds = true           
            picture.round()
        }
    }
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var lblSubTitle: UILabel!
    @IBOutlet private weak var mainPanal: UIView!
    
    
    // MARK: - Overridden functions
    
    internal func updateUI() {
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            lblTitle.textAlignment = .right
            lblSubTitle.textAlignment = .right
            mainPanal.semanticContentAttribute = .forceLeftToRight
        } else {
            lblTitle.textAlignment = .left
            lblSubTitle.textAlignment = .left
            mainPanal.semanticContentAttribute = .forceRightToLeft
        }
        lblTitle.text = person?.name ?? string.empty
        lblSubTitle.text = person?.position ?? string.empty
        
        if let imageLink = person?.photo {
            picture.loadImage(from: imageLink, onComplete: nil)
        }
    }
    
    //MARK: - Actions
    
}
