
import Foundation

import UIKit
import SystemConfiguration


 typealias string = String
 typealias void = Void
 typealias any = Any
 typealias double = Double
 typealias bool = Bool
 typealias float = Float
 typealias int = Int
 typealias Number = NSNumber
 typealias object = AnyObject
 typealias cgFloat = CGFloat
 typealias Color = UIColor
 typealias Font = UIFont
 typealias Size = CGSize
 typealias Rect = CGRect
 typealias Insets = UIEdgeInsets
 typealias uint = UInt



final class AppManager: NSObject
{
    
   
    
    internal static let shared = AppManager()
    
    internal func getFonts()-> void {
        for family in UIFont.familyNames {
            print("--------------\(family)-----------------")
            for fontName in UIFont.fontNames(forFamilyName: family) {
                print("font name is :- \(fontName)")
            }
        }
    }
    
    internal func configureNavigationBar()-> void {
        let nav = UINavigationBar.appearance()
        if let navTitleFont = Font(name: Constants.Misc.FontHacenPromoterMd, size: 17.0) {
            nav.titleTextAttributes = [NSForegroundColorAttributeName: Color.black, NSFontAttributeName: navTitleFont]
        }
        nav.barTintColor = .white
        nav.setBackgroundImage(UIImage(), for: .default)
        nav.shadowImage = UIImage()
        nav.backgroundColor = .white
        nav.isTranslucent = false
        nav.castShadow = string.empty
        nav.tintColor = .black
    }
    
    
    internal var isConnected: bool {
        switch Reach().connectionStatus() {
        case .offline:
            return false
        default:
            return true
        }
    }
 
    
}








let ReachabilityStatusChangedNotification = "ReachabilityStatusChangedNotification"

enum ReachabilityType: CustomStringConvertible {
    case wwan
    case wiFi
    
    var description: String {
        switch self {
        case .wwan: return "WWAN"
        case .wiFi: return "WiFi"
        }
    }
}

enum ReachabilityStatus: CustomStringConvertible  {
    case offline
    case online(ReachabilityType)
    case unknown
    
    var description: String {
        switch self {
        case .offline: return "Offline"
        case .online(let type): return "Online (\(type))"
        case .unknown: return "Unknown"
        }
    }
}

internal class Reach {
    
    func connectionStatus() -> ReachabilityStatus {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return .unknown
        }
        
        var flags : SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return .unknown
        }
        
        return ReachabilityStatus(reachabilityFlags: flags)
    }
    
    
    func monitorReachabilityChanges() {
        let host = "google.com"
        var context = SCNetworkReachabilityContext(version: 0, info: nil, retain: nil, release: nil, copyDescription: nil)
        let reachability = SCNetworkReachabilityCreateWithName(nil, host)!
        
        SCNetworkReachabilitySetCallback(reachability, { (_, flags, _) in
            let status = ReachabilityStatus(reachabilityFlags: flags)
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: ReachabilityStatusChangedNotification),
                                            object: nil,
                                            userInfo: ["Status": status.description])
            
        }, &context)
        
        SCNetworkReachabilityScheduleWithRunLoop(reachability, CFRunLoopGetMain(), RunLoopMode.commonModes as CFString)
    }
    
}

extension ReachabilityStatus {
    init(reachabilityFlags flags: SCNetworkReachabilityFlags) {
        let connectionRequired = flags.contains(.connectionRequired)
        let isReachable = flags.contains(.reachable)
        let isWWAN = flags.contains(.isWWAN)
        
        if !connectionRequired && isReachable {
            if isWWAN {
                self = .online(.wwan)
            } else {
                self = .online(.wiFi)
            }
        } else {
            self =  .offline
        }
    }
}
