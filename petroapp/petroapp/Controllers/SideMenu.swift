
import UIKit
import Gemo
import KYDrawerController


var selectedMainPage = 0
class SideMenu: BaseController {
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var items = Array<MenuItem>() { didSet { Threads.main { [weak self] in
        self?.tblItems.reloadData()
        } } }
    
    // MARK: - Outlets
    
    @IBOutlet weak var headerPanel: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var tblItems: UITableView! { didSet {
        tblItems.delegate = self
        tblItems.dataSource = self
        } }
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        items = MenuItem.items()
    }
    
    internal override func updateUi() {
        //super.updateUi()
        if (Localizer.instance.current == .arabic) {
            lblEmail.textAlignment = .right
            lblName.textAlignment = .right
            userImage.rightAnchor.constraint(equalTo: headerPanel.rightAnchor, constant: -8).isActive = true
        } else {
            lblEmail.textAlignment = .left
            lblName.textAlignment = .left
            userImage.leftAnchor.constraint(equalTo: headerPanel.leftAnchor, constant: 8).isActive = true
        }
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        lblName.text = UserDefaults.standard.string(forKey: Misc.Name) ?? .empty
        lblEmail.text = UserDefaults.standard.string(forKey: Misc.Email) ?? .empty
        userImage.loadImage(from: UserDefaults.standard.string(forKey: Misc.Photo) ?? .empty)
        userImage.rounded(cornerRadius: userImage.bounds.height/2)
    }
    
    
    // MARK: - Actions

}


// MARK: - Helper funtions 

fileprivate extension SideMenu {
    
    func closeMenu() -> void {
        (parent as? KYDrawerController)?.setDrawerState(.closed, animated: true)
    }
    
    func open(page: BaseController)-> void {
        let drawer = (parent as? KYDrawerController)
        (drawer?.mainViewController as? UINavigationController)?.pushViewController(page, animated: false)
    }
    
    
}


// MARK: - Table views delegate & data source functions

extension SideMenu: UITableViewDelegate, UITableViewDataSource {
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifiers.SideMenu) as! SideMenuCell
        cell.item = items[indexPath.row]
        return cell
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected cell: \(indexPath)")
        closeMenu()
        if (selectedMainPage != indexPath.row) {
            switch (indexPath.row) {
            case 0:
                guard let homePage = storyboard?.instantiateViewController(withIdentifier: Storyboards.Home) as? Home else {
                    return
                }
                open(page: homePage)
                
            case 1: // account
                guard let accountPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.Profile) as? Profile else {
                    return
                }
                open(page: accountPage)
                
            case 2: // my point
                guard let pointsPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.MyPoints) as? MyPoints else {
                    return
                }
                open(page: pointsPage)
                
                
            case 3: // contact us
                guard let contactUsPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.ContactUs) as? ContactUs else {
                    return
                }
                open(page: contactUsPage)
                
            case 4: // send complaint
                guard let complaintsPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.SendComplaint) as? SendComplaint else {
                    return
                }
                open(page: complaintsPage)
                
            case 5: // about app
                guard let aboutappPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.AboutApp) as? AboutApp else {
                    return
                }
                open(page: aboutappPage)
                
            case 7: // logout
                logout()
                
            default:
                break
            }
            selectedMainPage = indexPath.row
        }
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    
}
























