
import UIKit

final class SignInPageViewController: UIViewController,
    UIPopoverPresentationControllerDelegate
{
    
    var usersTypeName: String!
    var usersTypeID = "1"
    var keyboardLefted = false
    var code = 0

    @IBOutlet weak var personsView: UIView!
    @IBOutlet weak var loadingLbl: UILabel!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var userPhoneNumber: UITextField!
    @IBOutlet weak var forgetPassword: UILabel!
    @IBOutlet weak var userNameText: UITextField!
    @IBOutlet weak var chooseLbl: UILabel!
    @IBOutlet weak var newAccountLbl: UILabel!
    @IBOutlet weak var signUpLbl: UILabel!
    @IBOutlet weak var forgetPasswordView: UIView!
    @IBOutlet weak var manImage: UIImageView!
    @IBOutlet weak var femaleView: UIView!
    @IBOutlet weak var maleView: UIView!
    @IBOutlet weak var womanImage: UIImageView!
    @IBOutlet private weak var activationPanel: UIView! { didSet {
        activationPanel.backgroundColor = UIColor.black.withAlphaComponent(0.50)
        activationPanel.isHidden = true
        }}
    @IBOutlet private weak var txfActivationCode: UITextField!
    
    private var enterdCode: Int {
        return Int(txfActivationCode.text?.trimmingCharacters(in: .whitespaces) ?? "") ?? 0
    }
    
    
    @IBAction private func activateOnCLick(_ sender: UIButton) {
        view.endEditing(true)
        activationPanel.isHidden = true
        shadowView.isHidden = false
        loadingView.isHidden = false
        loadingView.layer.cornerRadius = 4
        loadingView.layer.masksToBounds = true
        loadingActivity.startAnimating()
        guard enterdCode == code else {
            displayAlert("Invalide code")
            return
        }
        guard let url = URL(string: Settings.ConfirmSignIn) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = "mobile=\(userPhoneNumber.text ?? "")".data(using: .utf8)
        URLSession.shared
            .dataTask(with: request) { [weak self] (result, response, error) in
                guard let this = self else { return }
                if (error != nil) {
                    this.shadowView.isHidden = true
                    this.loadingView.isHidden = true
                    this.loadingActivity.startAnimating()
                    this.displayAlert(error!.localizedDescription)
                    return
                }
                this.shadowView.isHidden = true
                this.loadingView.isHidden = true
                this.loadingActivity.startAnimating()
                do {
                    guard let jsonData = result else { return }
                    guard let json = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? Dictionary<String, Any> else {
                        return
                    }
                    print("confirmed login json: ", json)
                    guard let isSuccess = json["Success"] as? Bool, isSuccess else {
                        this.displayAlert(json["Message"] as? String ?? "خطأ")
                        return
                    }
                    guard let data = json["Result"] as? Dictionary<String, Any> else { return }
                    print("data is : \(data)")
                    UserDefaults.standard.set(data["id"] as? String, forKey: "users_ID")
                    UserDefaults.standard.set(data["username"] as? String, forKey: "userName")
                    UserDefaults.standard.set(data["mobile"] as? String, forKey: "userMobile")
                    UserDefaults.standard.set(data["img"] as? String, forKey: "userImage")
                    UserDefaults.standard.set(data["type"] as? String, forKey: "userType")
                    UserDefaults.standard.set(data["gender"] as? String, forKey: "userGender")
                    this.checkAds()
                } catch {
                    this.displayAlert(error.localizedDescription)
                }
        }
        .resume()
    }
    
    @IBAction private func resendActivateCodeOnCLick(_ sender: UIButton) {
        guard let userPhone = UserDefaults.standard.string(forKey: "userPhone") else { return }
        let request = NSMutableURLRequest(url: URL(string:Settings.ResendCode)!)
        request.httpMethod = "POST"
        let postString = "mobile=\(userPhone)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { [weak self] data, response, error in
            guard let me = self else { return }
            guard error == nil && data != nil else {
                print("error=\(String(describing: error))")
                me.displayAlert(error!.localizedDescription)
                return
            }
            //let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            //print("responseString = \(String(describing: responseString))")
            if let responseData = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                    //print("ddd\(json)")
                    if let success = json["Success"] as? Bool, success {
                        me.displayAlert("resend code successfully")
                        if let recievedCode = json["Code"] as? Int {
                            me.code = recievedCode
                            /*DispatchQueue.main.async {
                                UserDefaults.standard.set(recievedCode, forKey: SentCode)
                            }*/
                        }
                    }
                } catch {
                    print ("Could not Serialize, error: \(error)")
                }
            }
        }
        task.resume()

    }
    

    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        shadowView.isHidden = true
        loadingView.isHidden = true
        loadingView.layer.cornerRadius = 4
        loadingView.layer.masksToBounds = true
        personsView.layer.cornerRadius = 4
        personsView.layer.masksToBounds = true
        forgetPasswordView.layer.cornerRadius = 4
        forgetPasswordView.layer.masksToBounds = true
        chooseLbl.text = "Student"
        
        if Settings.lang == "ar" {
            signUpLbl.text = "حساب جديد"
            newAccountLbl.text = "تسجيل الدخول"
            newAccountLbl.font = UIFont(name: newAccountLbl.font.fontName, size:12)
            chooseLbl.text = "اختر"
            userNameText.placeholder = "اسم المستخدم"
            userPhoneNumber.placeholder = "رقم الجوال"
            forgetPassword.text = "نسيت كلمة المرور"
            loadingLbl.text = "جاري التحميل..."
            chooseLbl.text = "طالب"
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func showKeyboard(_ sender: NSNotification) {
        if !keyboardLefted {
            view.frame.origin.y -= 216
            keyboardLefted = true
        }
    }
    
    func hideKeyboard(_ sender: NSNotification) {
        view.frame.origin.y = 0
        keyboardLefted = false
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController)-> UIModalPresentationStyle {
        return.none
    }

    @IBAction func unwindToSignIn(_ segue:UIStoryboardSegue) {
        if let userTypeView = segue.source as? UserType1ViewController,
            let selectedUser = userTypeView.usersTypeName {
            usersTypeName = selectedUser
            print("this user\(usersTypeName)")
            chooseLbl.text = self.usersTypeName
            let userTypeID = userTypeView.usersTypeID
            usersTypeID = userTypeID!
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "userType") {
            let vc = segue.destination as! UserType1ViewController
            vc.preferredContentSize = CGSize(width: 300, height: 88)
            let controller = vc.popoverPresentationController
            controller?.permittedArrowDirections = .up
            if (controller != nil) {
                controller?.delegate = self
            }
            
        }
    }

    func loadPopOver() {
        let popOverVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForgetPasswordViewController")as? ForgetPasswordViewController
        self.addChildViewController(popOverVc!)
        
        popOverVc?.view.frame = self.view.frame
        self.view.addSubview((popOverVc?.view)!)
        popOverVc?.didMove(toParentViewController: self)
    }
    
    private func gotoEditProfile() {
        DispatchQueue.main.async { [weak self] in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: "EditProfileVC") as? EditProfileVC {
                self?.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func signIn(_ lang: String,_ userType: String,_ userName: String,_ mobile: String) {
        shadowView.isHidden = false
        loadingView.isHidden = false
        loadingView.layer.cornerRadius = 4
        loadingView.layer.masksToBounds = true
        loadingActivity.startAnimating()
        let request = NSMutableURLRequest(url: URL(string:Settings.SignIn)!)
        request.httpMethod = "POST"
        let postString = "lang=\(lang)&type=\(userType)&username=\(userName)&mobile=\(mobile)"
//        print(postString)
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { [weak self] data, response, error in
            guard let this = self else { return }
            guard error == nil && data != nil else {
                print("error=\(String(describing: error))")
                return
            }
            
//            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
//            print("responseString = \(String(describing: responseString))")
            if let responseData = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: responseData, options: .allowFragments) as! [String:AnyObject]
                    print("sign in json data: \(json)")
                    if let isSuccess = json["Success"] as? Bool, isSuccess {
                        guard let c = json["Code"] as? Int else {
                            print("can't cast code")
                            return
                        }
                        this.code =  c
                        DispatchQueue.main.async { [weak self] in
                            guard let this = self else { return }
                            this.shadowView.isHidden = true
                            this.loadingView.isHidden = true
                            this.loadingActivity.stopAnimating()
                            this.activationPanel.isHidden = false
                        }
                    } else {
                        this.displayAlert(json["Message"] as? String ?? "")
                    }
                    /*if self.dataFound! {
                        let data = json["Result"] as! NSDictionary
                        self.userID = data["id"] as! String
                        self.userName = data["username"] as! String
                        self.userMobile = data["mobile"] as! String
                        self.userType = data["type"] as! String
                        self.userImage = data["img"] as! String
                    }*/
                } catch {
                    print ("Could not Serialize, error: \(error)")
                }
            }
        }
        task.resume()
    }
    
    private func navigateToAdsPage()-> Void {
        DispatchQueue.main.async { [weak self] in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AdsPageViewController") as! AdsPageViewController
            self?.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    private func checkAds()-> Void {
        isLogged = true
        UserDefaults.standard.set(true, forKey: LoggedInKey)
        URLSession.shared
            .dataTask(with: URL(string: Settings.FoundAds)!) { [weak self] (result, response, error) in
                if (error != nil) {
                    print("check ads error \(String(describing: error))")
                    self?.gotoEditProfile()
                    return
                }
                do {
                    if let json = try JSONSerialization.jsonObject(with: result!, options: .allowFragments) as? Dictionary<String, Any> {
                        let success = String(describing: json["Success"] ?? "")
                        print("success: \(success)")
                        if (success == "1") {
                            self?.navigateToAdsPage()
                        } else {
                            self?.gotoEditProfile()
                        }
                    }
                } catch {
                    print("catch error \(String(describing: error))")
                    self?.gotoEditProfile()
                }
            }.resume()
        
    }
    
    func displayAlert(_ Massage: String)
    {
        var t = ""
        var b = ""

        if Settings.lang == "ar"{
            t = "تنبيه"
            b = "إغلاق"
        }
        else{
            t = "Alert"
            b = "Close"
        }

        let myAlert = UIAlertController(title: t, message:Massage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    @IBAction func forgetPassword(_ sender: Any)
    {
        loadPopOver()
    }
    
    @IBAction func signInBtn(_ sender: Any)
    {
        view.endEditing(true)
        if usersTypeID == "" || userNameText.text == "" || userPhoneNumber.text == ""
        {
            self.displayAlert("Please Fill All Fields")
        }
        else
        {
           signIn(Settings.lang, usersTypeID, userNameText.text!, userPhoneNumber.text!)
        }
    }
    
    @IBAction func signUpBtn(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: false)

    }

    @IBAction func femaleBtn(_ sender: Any) {
        femaleView.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
        maleView.backgroundColor = UIColor(red: 222/255, green: 139/255, blue: 0, alpha: 1)
        womanImage.image = UIImage(named:"woman2")
        manImage.image = UIImage(named:"man")

    }
    
    @IBAction func maleBtn(_ sender: Any) {
        femaleView.backgroundColor = UIColor(red: 222/255, green: 139/255, blue: 0, alpha: 1)
        maleView.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
        womanImage.image = UIImage(named:"woman")
        manImage.image = UIImage(named:"man2")

    }
    
    
}
