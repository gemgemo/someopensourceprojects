
import UIKit
import Gemo
import CoreLocation
import GoogleMaps
import MobileCoreServices
import UserNotifications
import Firebase

class BaseController: UIViewController
{
    
    // MARK: - Constants
    
    // MARK: - Variables
    
    internal var btnBack: UIBarButtonItem?
    
    // MARK: - Outlets
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()        
        UIApplication.shared.isStatusBarHidden = false
        btnBack = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(back(_:)))
        setupTabBar()
        tabBarItems?.reloadData()
        tabBarItems?.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .centeredHorizontally)
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBar.isHidden = false
        navigationItem.backBarButtonItem = UIBarButtonItem()
        navigationItem.hidesBackButton = true
        updateUi()
    }
    
    internal override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        automaticallyAdjustsScrollViewInsets = false
        tabBarItems?.collectionViewLayout.invalidateLayout()
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToNotifications), name: .OpenNotifications, object: nil)
//        if let notificationPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.Notifications) as? Notifications {
//            NotificationCenter.default.addObserver(self, selector: #selector(notificationPage.fetchNotifications), name: .RefreshNotifications, object: nil)
//        }
    }
    
    
    // MARK: - Actions
       
    internal func back(_ sender: UIBarButtonItem)-> void {
        view.endEditing(true)
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc private func openMenu(_ sender: UIBarButtonItem) {
        let items = [PopupItem(id: 0, title: Localization.LastOrders.localized()), PopupItem(id: 1, title: Localization.Complaints.localized())]
        let rect = Rect(x: (Localizer.instance.current == .arabic) ? view.frame.width - 10 : 10, y: 10, width: 0, height: 0)
        print(rect)
        openPopover(with: items, in: rect, and: Size(width: view.frame.width * 0.40, height: 80)) { [weak self] (index) in
            switch (index) {
            case 0: // goto Orders
                self?.navigateToOrders(lastOrders: true)
                
            case 1: // goto complaints
                self?.navigateToComplaints()
                
            default:
                break
            }
        }
    }
    
    // MARK: - Functions
    
    internal func configureRemoteNotifications()-> void {
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            if (granted) {
                FIRMessaging.messaging().remoteMessageDelegate = self
                UIApplication.shared.registerForRemoteNotifications()
            } else {
                print("notification error \(string(describing: error))")
            }
        }
    }
    
    @objc fileprivate func navigateToNotifications()-> void {
        guard let notificationsPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.Notifications) as? Notifications else { return }        
        navigate(to: notificationsPage, animated: false)
    }
    
    /*internal func fetchNotifications()-> void {
        if (Gemo.gem.isConnected) {
            dataSource.removeAll()
            OrderNotification.fetchNotifications { [weak self] (notifications) in
                if (!notifications.isEmpty) {
                    self?.dataSource = notifications
                }
                Threads.main { [weak self] in
                    self?.refersher?.endRefreshing()
                }
            }
        } else {
            refersher?.endRefreshing()
            dialogUserWithNoInternetConnectionMessage()
        }
    }*/
    
    internal func openImagePicker(with delegate: BaseController)-> void {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = delegate as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.3008661568, green: 0.4334716499, blue: 0.5458672643, alpha: 1)
        imagePicker.allowsEditing = true
        imagePicker.mediaTypes = [kUTTypeImage as string]
        let alert = UIAlertController(title: Localization.ChooseImageTitle.localized(), message: Localization.ChooseImageMessage.localized(), preferredStyle: .alert)
        alert.view.tintColor = #colorLiteral(red: 0.3008661568, green: 0.4334716499, blue: 0.5458672643, alpha: 1)
        alert.addAction(UIAlertAction(title: Localization.Camera.localized(), style: .default, handler: { [weak self] (action) in
            if (UIImagePickerController.isCameraDeviceAvailable(.rear)) {
                imagePicker.sourceType = .camera
                self?.present(imagePicker, animated: true)
            } else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: "Can not open camera")
            }
        }))
        alert.addAction(UIAlertAction(title: Localization.PhotoLibrary.localized(), style: .default, handler: { [weak self] (action) in
            imagePicker.sourceType = .photoLibrary
            self?.present(imagePicker, animated: true)
        }))
        alert.addAction(UIAlertAction(title: Localization.Cancel.localized(), style: .cancel))
        present(alert, animated: true)
    }
    
    internal func setupMenuButton()-> void {
        let menuButton = UIBarButtonItem(image: #imageLiteral(resourceName: "show-more-button"), style: .plain, target: self, action: #selector(openMenu(_:)))
        if (Localizer.instance.current == .arabic) {
            navigationItem.rightBarButtonItem = menuButton
        } else {
            navigationItem.leftBarButtonItem = menuButton
        }
    }
    
    internal func navigateToOrders(lastOrders: bool)-> void {
        guard let ordersPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.Orders) as? Orders else { return }
        ordersPage.isLastOrders = lastOrders
        navigate(to: ordersPage, animated: false)
    }
    
    internal func navigateToComplaints()-> void {
        guard let complaintsPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.Complaints) as? Complaints else { return }
        navigate(to: complaintsPage)
    }
    
    internal func navigateToEditProduct(isNew: bool, product: Product?)-> void {
        guard let editProductPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.EditProduct) as? EditProduct else { return }
        editProductPage.isNew = isNew
        editProductPage.product = product
        navigate(to: editProductPage)
    }
    
    internal func openPopover(with items: [PopupItem], in rect: Rect, and size: Size, _ arrow: UIPopoverArrowDirection = UIPopoverArrowDirection.init(rawValue: 0), _ selectCell: bool = false, _ selection: @escaping(int)->())-> void {
        guard let popover = storyboard?.instantiateViewController(withIdentifier: Storyboards.Popover) as? Popup else { return }
        popover.modalPresentationStyle = .popover
        popover.popoverPresentationController?.delegate = self
        popover.popoverPresentationController?.permittedArrowDirections = arrow
        popover.preferredContentSize = size//Size(width: view.frame.width * 0.40, height: (items.count * 40).toCGFloat)
        popover.popoverPresentationController?.sourceView = view
        popover.popoverPresentationController?.sourceRect = rect
        popover.items = items
        popover.didSelect = selection
        popover.selectFirstCell = selectCell
        present(popover, animated: true)
    }

    internal func dialogUserWithNoInternetConnectionMessage()-> void {
        Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized())
    }
    
    internal func address(for coordinate: CLLocationCoordinate2D, _ completion: @escaping(string)->())-> void {
        GMSGeocoder().reverseGeocodeCoordinate(coordinate) { (response, error) in
            if (error != nil) {
                return
            }
            completion(response?.firstResult()?.lines?.joined(separator: ", ") ?? .empty)
        }
    }

    
    internal dynamic func updateUi()-> void {
        if (Localizer.instance.current == .arabic) {
            btnBack?.image = #imageLiteral(resourceName: "back").withRenderingMode(.alwaysTemplate)
            navigationItem.leftBarButtonItem = btnBack
            navigationItem.leftBarButtonItem?.tintColor = .main
        } else {
            btnBack?.image = #imageLiteral(resourceName: "leftBack").withRenderingMode(.alwaysTemplate)
            navigationItem.rightBarButtonItem = btnBack
            navigationItem.rightBarButtonItem?.tintColor = .main
        }
    }
    
    internal func navigate(to controller: UIViewController, animated: bool = true)-> void {
        navigationController?.pushViewController(controller, animated: animated)
    }
    
    internal func save(this store: Grocery)-> void {
        UserDefaults.standard.set(store.id, forKey: Misc.GroceryId)
        UserDefaults.standard.set(store.name, forKey: Misc.GroceryName)
        UserDefaults.standard.set(store.username, forKey: Misc.GroceryUsername)
        UserDefaults.standard.set(store.email, forKey: Misc.GroceryEmail)
        UserDefaults.standard.set(store.avatar, forKey: Misc.GroceryAvatar)
        UserDefaults.standard.set(store.departments, forKey: Misc.GroceryDepartments)
        UserDefaults.standard.set(store.details, forKey: Misc.GroceryDescription)
        UserDefaults.standard.set(store.phone, forKey: Misc.GroceryPhone)
        UserDefaults.standard.set(store.latitude, forKey: Misc.GroceryLatitude)
        UserDefaults.standard.set(store.longitude, forKey: Misc.GroceryLongitude)        
        UserDefaults.standard.synchronize()
    }
    
    
    // MARK: - Custom tab bar
    
    fileprivate let tabBarDataSource = [#imageLiteral(resourceName: "store"), #imageLiteral(resourceName: "order"), #imageLiteral(resourceName: "notice"), #imageLiteral(resourceName: "setting")]
    
    internal lazy var tabBar: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.setShadow(with: Size(right: 0, top: -1), radius: 1.0, opacity: 0.4, color: .black)
        v.isHidden = false
        return v
    }()
    
    internal lazy var tabBarItems: UICollectionView? = { [weak self] in
        guard let this = self else { return nil }
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let grid = UICollectionView(frame: this.tabBar.frame, collectionViewLayout: layout)
        grid.translatesAutoresizingMaskIntoConstraints = false
        grid.alwaysBounceHorizontal = false
        grid.alwaysBounceVertical = false
        grid.backgroundColor = .white
        grid.delegate = self
        grid.dataSource = self
        return grid
        }()
    
    fileprivate let tabBarCellId = "com.tab.bar.cell.id"
    
    private func setupTabBar()-> void {
        view.addSubview(tabBar)
        tabBarItems?.bounces = false
        tabBarItems?.alwaysBounceVertical = false
        tabBarItems?.alwaysBounceHorizontal = false
        tabBarItems?.isScrollEnabled = false
        tabBarItems?.showsVerticalScrollIndicator = false
        tabBarItems?.showsHorizontalScrollIndicator = false
        if let collectionView = tabBarItems {
            tabBar.addSubview(collectionView)
            collectionView.register(TabBarCell.self, forCellWithReuseIdentifier: tabBarCellId)
        }
        // main view constraints
        tabBar.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tabBar.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tabBar.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tabBar.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        // items constraints
        tabBarItems?.rightAnchor.constraint(equalTo: tabBar.rightAnchor).isActive = true
        tabBarItems?.bottomAnchor.constraint(equalTo: tabBar.bottomAnchor).isActive = true
        tabBarItems?.leftAnchor.constraint(equalTo: tabBar.leftAnchor).isActive = true
        tabBarItems?.topAnchor.constraint(equalTo: tabBar.topAnchor).isActive = true
        if (Localizer.instance.current == .arabic) {
            tabBarItems?.semanticContentAttribute = .forceRightToLeft
        } else {
            tabBarItems?.semanticContentAttribute = .forceLeftToRight
        }
    }
                       
    
}

internal var selectedTabBarItem = 0


// MARK: - collection view delegate & data source functions 

extension BaseController: UICollectionViewDelegate,
    UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout
{
    internal func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabBarDataSource.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: tabBarCellId, for: indexPath) as? TabBarCell else {
            return UICollectionViewCell()
        }
        cell.image = tabBarDataSource[indexPath.item]
        return cell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("selected tab bar item: \(indexPath)")
        if (selectedTabBarItem != indexPath.item) {
            switch(indexPath.item) {
            case 0 : // products
                guard let productsPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.Home) as? Home else { return }
                navigate(to: productsPage, animated: false)
                
            case 1 : // orders
                navigateToOrders(lastOrders: false)
                
            case 2 : //notifications
                guard let notificationsPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.Notifications) as? Notifications else { return }
                navigate(to: notificationsPage, animated: false)
            case 3 : // settings
                guard let settingsPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.Settings) as? Settings else { return }
                navigate(to: settingsPage, animated: false)
                
            default:
                break
            }
            selectedTabBarItem = indexPath.item
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = collectionView.frame.width / 4//tabBar.frame.size.width*0.25
        
        return Size(width: cellWidth, height: collectionView.frame.size.height-0.3)
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
}



// MARK: - Tab bar cell

internal final class TabBarCell: UICollectionViewCell
{
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    internal var image: UIImage? {
        didSet {
            icon.image = image
        }
    }
    
    internal lazy var mainPanal: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var icon: UIImageView = { [weak self] in
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
        }()
    
    private func setupViews()-> void {
        contentView.addSubview(mainPanal)
        mainPanal.addSubview(icon)
        // main panal constraints
        mainPanal.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        mainPanal.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        mainPanal.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        mainPanal.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        // icon constraints y,x,w,h
        icon.centerXAnchor.constraint(equalTo: mainPanal.centerXAnchor).isActive = true
        icon.centerYAnchor.constraint(equalTo: mainPanal.centerYAnchor).isActive = true
        icon.widthAnchor.constraint(equalToConstant: 25.0).isActive = true
        icon.heightAnchor.constraint(equalToConstant: 25.0).isActive = true
    }
    
    override var isSelected: Bool {
        didSet {
            mainPanal.backgroundColor = (isSelected) ? Color(white: 0.88, alpha: 0.9) : .white
        }
    }
    
}


// MARK:- Popover delegate functions

extension BaseController: UIPopoverPresentationControllerDelegate
{
    internal func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}


// MARK: - User notifications delegate and firebase messageing functions

extension BaseController: UNUserNotificationCenterDelegate, FIRMessagingDelegate
{
    
    internal func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("ios 10 user notification received")
        completionHandler()
    }
    
    internal func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("ios 10 notification preseted in foreground", notification.request.content.userInfo)
        completionHandler([.alert, .sound])
    }
    
    internal func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        print("message data: \(remoteMessage.appData)")
    }
    
}







































