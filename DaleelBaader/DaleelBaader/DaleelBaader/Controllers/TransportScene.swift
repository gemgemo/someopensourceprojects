
import UIKit

import Localize_Swift

final class TransportScene: BaseController
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    fileprivate var dataSource = [Transport]()
    
    // MARK: - Outlets
    
    @IBOutlet private weak var tblTransport: UITableView! {
        didSet {
            tblTransport.delegate = self
            tblTransport.dataSource = self
        }
    }
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBar()
        dataSource = Transport.loadData()
        tblTransport.reloadData()
    }
    
    //MARK: - Actions
    
}


// MARK: - Helper functions

extension TransportScene
{
    
    fileprivate func updateNavigationBar()-> void {
        navigationItem.title = "transporttitlefornav".localized()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(goBack(_:)))
    }
    
}


// MARK: - Table view delegate & data source functions 

extension TransportScene: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.Transport) as? TransportCellScene {
            cell.transport = dataSource[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
}


































