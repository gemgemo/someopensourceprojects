
import Foundation
import Gemo


class ReportPerCarCell: BaseTableCell {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var mainPanel: View!
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var carIcon: UIImageView!
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var boardNo: UIImageView!
    @IBOutlet weak var lblSpentLitres: UILabel!
    @IBOutlet weak var lblSpentAmounts: UILabel!
    
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        
    }
    
    // MARK: - Actions

}


























