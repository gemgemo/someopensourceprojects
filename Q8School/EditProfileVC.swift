
import UIKit

let LoggedInKey = "com.loogedin.id"

final class EditProfileVC: UIViewController,
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate
{
    
    @IBOutlet weak var editProfileView: UIView!
    @IBOutlet weak var loadingLbl: UILabel!
    @IBOutlet weak var editProfile: UIView!
    @IBOutlet weak var openMenu: UIBarButtonItem!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var femaleImage: UIImageView!
    @IBOutlet weak var maleImage: UIImageView!
    @IBOutlet weak var femaleView: UIView!
    @IBOutlet weak var maleView: UIView!
    @IBOutlet weak var genderView: UIView!
    //@IBOutlet weak var userPasswordTxt: UITextField!
    @IBOutlet weak var userNameTxt: UITextField!
    @IBOutlet weak var userImage: UIImageView!
    //@IBOutlet weak var userEmailTxt: UITextField!
    @IBOutlet weak var userPhoneTxt: UITextField!
    @IBOutlet weak var callUsLbl: UILabel!
    @IBOutlet weak var addAdsLbl: UILabel!
    @IBOutlet weak var myAdsLbl: UILabel!
    @IBOutlet weak var offerlbl: UILabel!
    @IBOutlet weak var myAdsImg: UIImageView!
    
    
    private var msg: String!
    private var err: String!
    private var dataFound: Bool!
    private var Gender: String!
    private let defaults = UserDefaults.standard
    private var userID: String!
    private var userName: String!
    private var userEmail: String!
    private var userMobile: String!
    private var userType: String!
    private var userImg: String!
    private var images_cache = [String:UIImage]()
    private var userImages: String!
    private var type:String!
    
    
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.7608320713, green: 0.2725405097, blue: 0.00439185882, alpha: 1)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        shadowView.isHidden = true
        loadingView.isHidden = true
        userImages = UserDefaults.standard.string(forKey: "userImage") ?? ""
        if userImages == "" {
            userImage.image = UIImage(named: "pro_img")
        } else {
            load_image(userImages!, imageview: userImage)
        }
        
        userImage.layer.cornerRadius = 0.5 * userImage.bounds.size.width
        userImage.clipsToBounds = true
        
        if UserDefaults.standard.string(forKey: "userGender") ?? "" == "m" {
            femaleView.backgroundColor = UIColor(red: 222/255, green: 139/255, blue: 0, alpha: 1)
            maleView.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
            femaleImage.image = UIImage(named:"woman")
            maleImage.image = UIImage(named:"man2")
            Gender = "m"
        } else {
            femaleView.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
            maleView.backgroundColor = UIColor(red: 222/255, green: 139/255, blue: 0, alpha: 1)
            femaleImage.image = UIImage(named:"woman2")
            maleImage.image = UIImage(named:"man")
            Gender = "f"
            
        }
        userNameTxt.text = UserDefaults.standard.string(forKey: "userName")
        userPhoneTxt.text = UserDefaults.standard.string(forKey: "userMobile")
        if Settings.lang == "ar" {
            userNameTxt.placeholder = "اسم المستخدم"
            userPhoneTxt.placeholder = "رقم الموبيل"
            loadingLbl.text = "جاري التحميل"
            callUsLbl.text = "اتصل بنا"
            addAdsLbl.text = "اضف اعلانك"
            myAdsLbl.text = "اعلاناتي"
            offerlbl.text = "العروض"
        }
        if revealViewController() != nil {
            revealViewController().rightViewRevealWidth = 235
            openMenu.target = revealViewController()
            openMenu.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        
        type = UserDefaults.standard.string(forKey: "userType") ?? ""
        if type == "1" {
            if Settings.lang == "ar" {
                myAdsLbl.text = "المفضلة"
                myAdsImg.image = UIImage(named: "fav")
            } else {
                myAdsLbl.text = "Favorite"
                myAdsImg.image = UIImage(named: "fav")
            }
        }
        
        print("user type: \(userType), name: \(userName), phone: \(userPhoneTxt.text ?? ""), image: \(userImages), gender: \(Gender)")
    }
    
    /*func showKeyboard(_ notification: NSNotification) {
        
    }
    
    func hideKeyboard(_ notification: NSNotification){
        
    }*/
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        let filename = "user-profile\(NSUUID().uuidString).png"
        let mimetype = "image/jpg"
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        body.appendString(string: "--\(boundary)--\r\n")
        return body
    }
    
    
    
    func generateBoundaryString() -> String
    {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func displayAlertMessage(_ Message:String,_ Error: String)
    {
        var t = ""
        var b = ""
        
        if Settings.lang == "ar"{
            t = "تنبيه"
            b = "إغلاق"
        }
        else{
            t = "Alert"
            b = "Close"
        }
        if Error == ""
        {
            let myAlert = UIAlertController(title: t, message:Message, preferredStyle: UIAlertControllerStyle.alert);
            
            let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "MainPageViewController") as!                             MainPageViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
            })
            myAlert.addAction(okAction);
            
            self.present(myAlert, animated: true, completion: nil)
            
        }
        else
        {
            let myAlert = UIAlertController(title: t, message:Error, preferredStyle: UIAlertControllerStyle.alert);
            
            let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler:nil)
            
            myAlert.addAction(okAction);
            
            self.present(myAlert, animated: true, completion: nil)
            
            
        }
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func load_image(_ link:String, imageview:UIImageView)
    {
        
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.timeoutInterval = 10
        
        
        let task = session.dataTask(with: url, completionHandler:
        { (data :Data?, response :URLResponse?, error:Error?) in
            
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                
                return
            }
            
            
            var image = UIImage(data: data!)
            
            if (image != nil)
            {
                
                
                func set_image()
                {
                    self.images_cache[link] = image
                    imageview.image = image
                }
                
                
                DispatchQueue.main.async(execute: set_image)
                
            }
            
        })
        
        task.resume()
    }
    func displayAlerts(_ Massage: String)
    {
        var t = ""
        var b = ""
        
        if Settings.lang == "ar"{
            t = "تنبيه"
            b = "إغلاق"
        }
        else{
            t = "Alert"
            b = "Close"
        }
        
        let myAlert = UIAlertController(title: t, message:Massage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func femaleBtn(_ sender: Any) {
        femaleView.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
        maleView.backgroundColor = UIColor(red: 222/255, green: 139/255, blue: 0, alpha: 1)
        femaleImage.image = UIImage(named:"woman2")
        maleImage.image = UIImage(named:"man")
        Gender = "f"
        
    }
    
    @IBAction func maleBtn(_ sender: Any) {
        femaleView.backgroundColor = UIColor(red: 222/255, green: 139/255, blue: 0, alpha: 1)
        maleView.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
        femaleImage.image = UIImage(named:"woman")
        maleImage.image = UIImage(named:"man2")
        Gender = "m"
        
    }
    
    @IBAction func submitBtn(_ sender: Any)
    {
        shadowView.isHidden = false
        loadingView.isHidden = false
        loadingActivity.startAnimating()
        let myUrl = NSURL(string:Settings.EditProfile);
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        let ID = UserDefaults.standard.string(forKey: "users_ID")
        let param = [
            "lang"  : Settings.lang,
            "userId"    : ID!,
            "gender"    : Gender,
            "username"    : userNameTxt.text!,
            "mobile"    : userPhoneTxt.text!,
            ] as [String : String]
        print("edit profile paramters", param)
        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let imageData = UIImageJPEGRepresentation(userImage.image!, 0.40)
        
        if(imageData == nil) { return }
        request.httpBody = createBodyWithParameters(parameters: param, filePathKey: "img", imageDataKey: imageData! as NSData, boundary: boundary) as Data
        
        let task = URLSession.shared
            .dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {
                    print("error=\(String(describing: error))")
                    self.shadowView.isHidden = true
                    self.loadingView.isHidden = true
                    self.loadingActivity.stopAnimating()
                    self.displayAlertMessage("", error!.localizedDescription)
                    return
                }
                let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("responseString = \(String(describing: responseString))")
                if let responseData = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                        print("ddd\(json)")
                        self.dataFound = json["Success"] as? Bool
                        let message = json["Message"] as? String
                        if self.dataFound! {
                            //self.msg = "Done"
                            let data = json["Result"] as! NSDictionary
                            self.userID = data["id"] as! String
                            self.userName = data["username"] as! String
                            self.userEmail = data["email"] as! String
                            self.userMobile = data["mobile"] as! String
                            self.Gender = data["gender"] as! String
                            self.userImg = data["img"] as! String
                        } else {
                            self.shadowView.isHidden = true
                            self.loadingView.isHidden = true
                            self.loadingActivity.stopAnimating()
                            //self.err = "Failer"
                        }
                        
                        DispatchQueue.main.async {
                            if self.userID != nil {
                                self.defaults.setValue(self.userID, forKey: "users_ID")
                                self.defaults.setValue(self.userName, forKey: "userName")
                                self.defaults.setValue(self.userEmail, forKey: "userEmail")
                                self.defaults.setValue(self.userMobile, forKey: "userMobile")
                                self.defaults.setValue(self.Gender, forKey: "userGender")
                                self.defaults.setValue(self.userImg, forKey: "userImage")
                                UserDefaults.standard.set(true, forKey: LoggedInKey)
                                let userImages = UserDefaults.standard.string(forKey: "userImage")
                                self.load_image(userImages!, imageview: self.userImage)
                                self.displayAlertMessage(message ?? "", "")
                                self.shadowView.isHidden = true
                                self.loadingView.isHidden = true
                                self.loadingActivity.stopAnimating()
                            } else {
                                self.displayAlertMessage("", message ?? "")
                                self.shadowView.isHidden = true
                                self.loadingView.isHidden = true
                                self.loadingActivity.stopAnimating()
                            }
                        }
                    } catch {
                        print ("Could not Serialize", error)
                        self.shadowView.isHidden = true
                        self.loadingView.isHidden = true
                        self.loadingActivity.stopAnimating()
                    }
                    
                }
                
        }
        task.resume()
        
    }
    
    
    private func navigateToMain()-> Void {
        DispatchQueue.main.async { [weak self] in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MainPageViewController") as! MainPageViewController
            self?.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    @IBAction func backPressed(_ sender: Any) {
        navigateToMain()
    }
    
    @IBAction func callUsBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CallUsVc") as! CallUsVc
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func addAdsBtn(_ sender: Any) {
        if type == "1"
        {
            if Settings.lang == "ar"
            {
                let msg = "انت طالب لا يمكنك اضافة اعلان"
                self.displayAlerts(msg)
                
            }
            else
            {
                let msg = "you'r student can't add Ads"
                self.displayAlerts(msg)
                
            }
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AddAdsVc") as! AddAdsVc
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        
    }
    
    @IBAction func myAdsBtn(_ sender: Any) {
        if type == "1"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyFavoriteVC") as! MyFavoriteVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyAdsVC") as! MyAdsVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        
        
    }
    
    @IBAction func offerBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AdsPageViewController") as! AdsPageViewController
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func chooseImgBtn(_ sender: Any)
    {
        
        let ImagePicker = UIImagePickerController()
        ImagePicker.delegate = self
        ImagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        
        self.present(ImagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        userImage.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        self.dismiss(animated: true, completion: nil)
    }
    
}



extension NSMutableData {
    
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

