
import UIKit

final class RateCell: UITableViewCell
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var rate: Rate? { didSet { updateUi() } }
    
    // MARK: - Outlets
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var rateNumber: UILabel!
    @IBOutlet weak var ratePanel: UIView! { didSet { ratePanel.layer.cornerRadius = 10.0 } }
    
    
    private func updateUi()-> Void {
        name.text = rate?.commenter ?? ""
        if let message = rate?.comment?.data(using: .utf8) {
            comment.text = String(data: message, encoding: .utf8)
        }
//        print("fetched rate is \(rate?.rate)")
        rateNumber.text = "\(rate?.rate ?? "0")"
    }
    

}
















