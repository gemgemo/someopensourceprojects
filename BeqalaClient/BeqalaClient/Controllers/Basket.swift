
import UIKit

final class Basket: BaseController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var dataSource = Array<Product>() {
        didSet {
            ThreadQueue.main { [weak self] in
                guard let this = self else { return }
                if (this.dataSource.isEmpty) {
                    this.footerPanelHeight.constant = 0.0
                    this.btnSendOrder.isHidden = true
                } else {
                    this.footerPanelHeight.constant = 120.0
                    this.btnSendOrder.isHidden = false
                }
                UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.4, options: .allowUserInteraction, animations: { [weak self] in
                    self?.view.layoutIfNeeded()
                }, completion: nil)
            }
        }
    }
    fileprivate var totalPrice = Number.zero.doubleValue
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var titlePanel: UIView! {
        didSet {
            shadow(for: titlePanel)
        }
    }
    @IBOutlet fileprivate weak var basketItems: UICollectionView! {
        didSet {
            basketItems.delegate = self
            basketItems.dataSource = self
        }
    }
    @IBOutlet fileprivate weak var sendOrderPanel: UIView! {
        didSet {
            sendOrderPanel.setShadow(with: .zero, radius: 0.7, opacity: 0.6, color: .black)
        }
    }
    @IBOutlet fileprivate weak var lblTotalTitle: UILabel!
    @IBOutlet fileprivate weak var lblTotalPrice: UILabel! { didSet { lblTotalPrice.text = "0.0 \(Localization.SR.localized())" } }
    @IBOutlet fileprivate weak var btnSendOrder: Button! { didSet { btnSendOrder.isHidden = true } }
    @IBOutlet fileprivate weak var footerPanelHeight: NSLayoutConstraint! { didSet { footerPanelHeight.constant = 0.0 } }
    @IBOutlet fileprivate weak var btnMenu: UIButton!
    @IBOutlet fileprivate weak var btnSearch: UIButton!
    @IBOutlet fileprivate weak var btnFavorites: UIButton!
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        loadData()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarItems?.selectItem(at: IndexPath(item: 2, section: 0), animated: false, scrollPosition: .top)
        selectedTabBarItem = 2
        updateUi()
        selectedItemIndex = 2
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(calculateTotalPrice), name: .CalculatePriceKey, object: nil)
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: - Actions
    
    @IBAction private func searchClicked(_ sender: UIButton) {
        navigateToSearch()
    }
    
    @IBAction private func favoriteClicked(_ sender: UIButton) {
        navigateToFavorites()
    }

    @IBAction private func menuClicked(_ sender: UIButton) {
        menu(withState: .opened)
    }
    
    @objc fileprivate func removeItemOnClick(_ sender: UIButton) {
        deleteItem(by: sender.tag)
    }
    
    @IBAction private func sendOrderOnCLick(_ sender: Button) {
        if (!dataSource.isEmpty) {
            var quantities = Array<int>()
            for (index, _) in dataSource.enumerated() {
                print("index \(index)")
                if let cell = basketItems.cellForItem(at: IndexPath(item: index, section: 0)) as? BasketItem {
                    print("product count",  cell.counter)
                    quantities.append(cell.counter)
                }
            }
            guard let shoppingOnePage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.ShoppingOne) as? ShoppingOne else { return }
            shoppingOnePage.quantities = quantities
            navigate(to: shoppingOnePage)
        }
    }
    
}


// MARK: - Helper functions 

extension Basket
{
    
    @objc fileprivate func calculateTotalPrice()-> void {
        ThreadQueue.thread(QOS: .userInitiated) { [weak self] in
            guard let this = self else { return }
            this.totalPrice = Number.zero.doubleValue
            for (index, item) in this.dataSource.enumerated() {
                let indexPath = IndexPath(item: index, section: 0)
                if let cell = this.basketItems.cellForItem(at: indexPath) as? BasketItem {
                    let price = item.price?.toDouble ?? Number.zero.doubleValue
                    let totalPerItem = price*cell.counter.toDouble
                    this.totalPrice += totalPerItem
                }
            }
            ThreadQueue.main { [weak self] in
                guard let me = self else { return }
                let price = "\(me.totalPrice.toString) \(Localization.SR.localized())"
                print("final price \(price)")
                me.lblTotalPrice.text = price
            }
        }
    }
    
    fileprivate func updateUi()-> void {
        view.bringSubview(toFront: titlePanel)
        view.bringSubview(toFront: tabBar)
        lblTotalTitle.text = Localization.Total.localized()
        btnSendOrder.setTitle(Localization.SendOrder.localized(), for: .normal)
        btnMenu.transform = CGAffineTransform.identity
        if (Localizer.instance.current == .arabic) {
            btnMenu.rightAnchor.constraint(equalTo: titlePanel.rightAnchor, constant: -8.0).isActive = true
            btnFavorites.leftAnchor.constraint(equalTo: titlePanel.leftAnchor, constant: 8.0).isActive = true
            btnSearch.leftAnchor.constraint(equalTo: btnFavorites.rightAnchor, constant: 20.0).isActive = true
            lblTotalTitle.rightAnchor.constraint(equalTo: sendOrderPanel.rightAnchor, constant: -8.0).isActive = true
            lblTotalPrice.rightAnchor.constraint(equalTo: lblTotalTitle.leftAnchor, constant: -16.0).isActive = true
            lblTotalPrice.leftAnchor.constraint(equalTo: sendOrderPanel.leftAnchor, constant: 8.0).isActive = true
            lblTotalTitle.textAlignment = .right
            lblTotalPrice.textAlignment = .right
            basketItems.semanticContentAttribute = .forceRightToLeft
        } else {
            btnMenu.leftAnchor.constraint(equalTo: titlePanel.leftAnchor, constant: 8.0).isActive = true
            btnFavorites.rightAnchor.constraint(equalTo: titlePanel.rightAnchor, constant: -8.0).isActive = true
            btnSearch.rightAnchor.constraint(equalTo: btnFavorites.leftAnchor, constant: -20.0).isActive = true
            lblTotalTitle.leftAnchor.constraint(equalTo: sendOrderPanel.leftAnchor, constant: 8.0).isActive = true
            lblTotalPrice.leftAnchor.constraint(equalTo: lblTotalTitle.rightAnchor, constant: 16.0).isActive = true
            lblTotalPrice.rightAnchor.constraint(equalTo: sendOrderPanel.rightAnchor, constant: -8.0).isActive = true
            lblTotalTitle.textAlignment = .left
            lblTotalPrice.textAlignment = .left
            basketItems.semanticContentAttribute = .forceLeftToRight
            btnMenu.transform = btnMenu.transform.rotated(by: cgFloat.pi)
        }
    }
    
    fileprivate func setupCollectionView()-> void {
        basketItems.alwaysBounceVertical = true
        basketItems.register(Constants.Nibs.Basket, forCellWithReuseIdentifier: Constants.ReuseIdentifiers.Basket)
    }
    
    fileprivate func loadData()-> void {
        if (Gemo.gem.isConnected) {
            let parameters: Dictionary<string, any> = [
                Constants.Misc.MainKey: Constants.Misc.MainValue,
                "user_id": UserDefaults.standard.string(forKey: Constants.Misc.UserID) ?? .empty
            ]
            Product.getData(from: Constants.Urls.BasketForUser, with: parameters, in: self) { [weak self] (items) in
                Gemo.gem.stopSpinning()
                if (!items.isEmpty) {
                    self?.dataSource = items
                    ThreadQueue.main { [weak self] in
                        self?.basketItems.reloadData()
                        ThreadQueue.delay(after: 0.1) { [weak self] in
                            self?.calculateTotalPrice()
                        }
                    }
                }
            }
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
    
    fileprivate func deleteItem(by index: int)-> void {
        if (Gemo.gem.isConnected) {
            guard let id = dataSource[index].id else { return }
            let parameters: [string: any] = [
                Constants.Misc.MainKey: Constants.Misc.MainValue,
                "user_id": UserDefaults.standard.string(forKey: Constants.Misc.UserID) ?? .empty,
                "product_id": id
            ]
            Product.removeItem(from: Constants.Urls.RemoveItem, with: parameters, in: self) { [weak self] in                
                ThreadQueue.main { [weak self] in
                    self?.dataSource.remove(at: index)
                    self?.basketItems.reloadData()
                    ThreadQueue.delay(after: 0.2) { [weak self] in
                        self?.calculateTotalPrice()
                    }
                }
            }
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
}


extension Basket
{
    internal override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == basketItems) {
            return dataSource.count
        }
        return super.collectionView(collectionView, numberOfItemsInSection: section)
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (collectionView == basketItems) {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ReuseIdentifiers.Basket, for: indexPath) as? BasketItem else { return UICollectionViewCell() }
            cell.product = dataSource[indexPath.item]
            cell.btnRemoveItem.tag = indexPath.item
            cell.btnRemoveItem.addTarget(self, action: #selector(removeItemOnClick(_:)), for: .touchUpInside)
            return cell
        }
        return super.collectionView(collectionView, cellForItemAt: indexPath)
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView == basketItems) {
            print("selected row is \(indexPath.item.toString)")
        } else {
            super.collectionView(collectionView, didSelectItemAt: indexPath)
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if (collectionView == basketItems) {
            return 8
        }
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if (collectionView == basketItems) {
            return 0
        }
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (collectionView == basketItems) {
            return Size(width: (view.frame.width/2)-14, height: view.frame.height*0.35)
        }
        return super.collectionView(collectionView, layout: collectionViewLayout, sizeForItemAt: indexPath)
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return Insets(top: 0.0, left: 8.0, bottom: 0.0, right: 8.0)
    }
    
}


















