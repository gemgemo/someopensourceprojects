
import UIKit
import Firebase
import UserNotifications
import Alamofire
import GoogleMaps


internal var notificationText = "show test text"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UIApplication.shared.statusBarStyle = .lightContent
        UINavigationBar.appearance().titleTextAttributes = [
            NSFontAttributeName: UIFont(name: "Tahoma", size: 16)!,
            NSForegroundColorAttributeName: UIColor.white

        ]
        if #available(iOS 10.0, *) {
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 display notification (sent via APNS)
            
            UNUserNotificationCenter.current().delegate = self
            
            // For iOS 10 data message (sent via FCM)
            
            FIRMessaging.messaging().remoteMessageDelegate = self
        } else {
            
            let settings: UIUserNotificationSettings =
                
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            
            application.registerUserNotificationSettings(settings)
            
        }
        application.registerForRemoteNotifications()
        // [END register_for_notifications]
        
        FIRApp.configure()
        // Add observer for InstanceID token refresh callback.
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.tokenRefreshNotification),
                                               name: .firInstanceIDTokenRefresh,
                                               object: nil)
        //DispatchQueue.global(qos: .userInteractive).async {
            GMSServices.provideAPIKey("AIzaSyDPl3JC2tZbu7sZrtWBWOpuszd0_ahm6j4")
        //}
        let navBar = UINavigationBar.appearance()
        navBar.barTintColor = #colorLiteral(red: 0.1096298024, green: 0.2917023003, blue: 0.2262378931, alpha: 1)
        if UserDefaults.standard.string(forKey: "phone") == nil && UserDefaults.standard.string(forKey: "check") == nil {
            print("with no number")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: "PhoneNumberVC") as? PhoneNumberVC {
                vc.isInitail = true
                window?.rootViewController = UINavigationController(rootViewController: vc)
            } else {
                print("no register view controller")
            }
        }
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        connectToFcm()
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        //print("old way notification body is: \( notification.alertBody)")
        notificationText = notification.alertBody ?? ""
        NotificationCenter.default.post(name: Notification.Name(POPOVER_FUNC_NAME), object: nil)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo["gcmMessageIDKey"] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        //print("ios 10 user info2: ", userInfo)
        notificationText = (userInfo["aps"] as? [String: Any])?["alert"] as? String ?? ""
        NotificationCenter.default.post(name: Notification.Name(POPOVER_FUNC_NAME), object: nil)
        completionHandler()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        if let messageID = userInfo["gcmMessageIDKey"] {
            print("Message ID: \(messageID)")
        }
        print("short func user info", userInfo)
    }
    
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if let messageID = userInfo["gcmMessageIDKey"] {
            print("Message ID: \(messageID)")
        }
        print("long func user info", userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.sandbox)
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.prod)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(#function, error)
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        print("register succsses----------------->>>>")
    }
    
    func tokenRefreshNotification(_ notification: Notification) {
        
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            
            print("InstanceID token: \(refreshedToken)")
            
            let link = "http://192.232.214.91/~almglsalbldyinmk//register.php?key=albldy"
            Alamofire.request(link, method: .post, parameters: ["token": refreshedToken, "type": "1"])
                .responseJSON { (response) in
                    let result = response.result
                    if (result.error == nil) {
                        print("result is: \(result.value)")
                    } else {
                        print("send token error is: \(result.error)")
                    }
            }
            
        }
        
        connectToFcm()
        
    }
    
    // [END refresh_token]
    
    // [START connect_to_fcm]
    
    func connectToFcm() {
        
        // Won't connect since there is no token
        
        guard FIRInstanceID.instanceID().token() != nil else {
            
            return;
            
        }
        FIRMessaging.messaging().disconnect()
        
        FIRMessaging.messaging().connect { (error) in
            
            if error != nil {
                
                print("Unable to connect with FCM. \(error)")
                
            } else {
                
                print("Connected to FCM.")
                
            }
            
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
}

extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo["gcmMessageIDKey"] {
            print("Message ID: \(messageID)")
        }
        print("ios 10 notification will present: ", userInfo)
        completionHandler([.alert, .sound])
        
    }
    
    
    
    
}



extension AppDelegate : FIRMessagingDelegate {
    
    // Receive data message on iOS 10 devices while app is in the foreground.
    
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        
        print(remoteMessage.appData)
        
    }
    
}

