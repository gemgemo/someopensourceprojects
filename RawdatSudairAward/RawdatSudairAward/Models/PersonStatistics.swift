

import Foundation
import UIKit

final class PersonStatistics
{
    
    
    internal var id, name, school, className, percentage, rank, result: string?
    
    init(dictionary: [string: any]) {
        id = dictionary["id"] as? string
        name = dictionary["name"] as? string
        school = dictionary["school"] as? string
        className = dictionary["class"] as? string
        percentage = dictionary["percent"] as? string
        rank = dictionary["rank"] as? string
        result = dictionary["result"] as? string
    }
    
    
    
    internal class func getData(from link: string, page: UIViewController?, onComplete: @escaping([PersonStatistics])-> Void)-> void {
        UIApplication.showIndicator(by: true)
        HttpClient.request(link: link, method: .get) { (result, response, error) -> void in
            if (error != nil) {
                (page as? BaseController)?.showAlert(with: string.empty, and: Constants.Misc.ErrorMessage)
                return
            }
            if let data = result as? [[string: any]] {
                onComplete(data.map { PersonStatistics(dictionary: $0)})
            } else {
                print("nix person statistics data")
            }
        }
    }
    
}

