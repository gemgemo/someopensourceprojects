
import UIKit

import Localize_Swift
import MapKit

class BaseController: UIViewController
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    private lazy var indicator: UIActivityIndicatorView = {
       let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.hidesWhenStopped = true
        spinner.translatesAutoresizingMaskIntoConstraints = false
        return spinner
    }()
    
    private lazy var spinnerView: UIVisualEffectView = {
        let visualView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: UIBlurEffect(style: .light)))
        visualView.translatesAutoresizingMaskIntoConstraints = false
        return visualView
    }()
    
    private lazy var alertContainer: UIView = {
        let iv = UIView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.backgroundColor = .darkGray
        iv.alpha = 0
        return iv
    }()
    
    private lazy var alertView: UIView = {
       let alert = UIView()
        alert.translatesAutoresizingMaskIntoConstraints = false
        alert.backgroundColor = .white
        alert.layer.cornerRadius = 3.0
        alert.clipsToBounds = true
        return alert
    }()
    
    private lazy var lblTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.textAlignment = .center
        label.font = Font(name: Constants.Misc.FontHacenPromoterMd, size: 14.0)
        return label
    }()
    
    private lazy var lblMessage: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = Font(name: Constants.Misc.FontDroidNaskhShiftAlt, size: 12.0)
        label.textAlignment = .center
        return label
    }()
    
    private lazy var btnDone: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Done", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.tintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        return btn
    }()
    
    // MARK: - Outlets
    
    // MARK: - Overridden functions
    
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //MARK: - Actions
    
    
    @objc private func doneAlertViewOnCLick(_ sender: UIButton) {
        print("done button tapped")
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.8, options: .curveEaseIn, animations: { [weak self] in
            guard let this = self else { return }
            this.alertView.transform = CGAffineTransform().scaledBy(x: 0.0, y: 0.0)
            this.alertContainer.alpha = 0.0
        }) { [weak self] (isCompleted) in
            guard let this = self else { return }
            this.lblMessage.removeFromSuperview()
            this.lblTitle.removeFromSuperview()
            this.btnDone.removeFromSuperview()
            this.alertView.removeFromSuperview()
            this.alertContainer.removeFromSuperview()
        }
    }
    
    
    // MARK: - Functions
    
    internal func openMap(with name: string, and location: CLLocationCoordinate2D)-> void {
        let span = MKCoordinateRegionMakeWithDistance(location, 1000, 1000)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: span.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: span.span)
        ]
        let placeMark = MKPlacemark(coordinate: location, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placeMark)
        mapItem.name = name
        mapItem.openInMaps(launchOptions: options)        
    }
    
    internal func makeLocationCoordinate(latitude: string, longitude: string)-> CLLocationCoordinate2D {
        let formatter = NumberFormatter()
        let lati = formatter.number(from: latitude)?.doubleValue ?? Number.zero.doubleValue
        let longi = formatter.number(from: longitude)?.doubleValue ?? Number.zero.doubleValue
        return CLLocationCoordinate2D(latitude: lati, longitude: longi)
    }
    
    internal func gotoPDFViewer(with link: string)-> void {
        if (AppManager.shared.isConnected) {
            self.startSpin()
            loadPDFFile(from: link) { [weak self] (link) -> void in
                self?.openPDF(from: link)
                UIApplication.showIndicator(by: false)
                self?.stopSpin()
            }
        } else {
            dialog(with: Constants.Misc.InternetMessage)
        }
        
    }
    
    internal func loadPDFFile(from link: string, onComplete: @escaping(string)-> void)-> void {
        UIApplication.showIndicator(by: true)         
        
        if (link.isEmpty || !link.isLink) {
            print("link error!")
            UIApplication.showIndicator(by: false)
            dialog(with: Constants.Misc.ErrorMessage)
            return
        }
        
        guard let url = URL(string: link) else {
            print("url error")
            UIApplication.showIndicator(by: false)
            dialog(with: Constants.Misc.ErrorMessage)
            return
        }
        
        URLSession.shared.dataTask(with: URLRequest(url: url)) { [weak self] (data, response, error) in
            guard let this = self else { return }
            if (error != nil) {
                print("request error")
                UIApplication.showIndicator(by: false)
                this.handle(this: error)
                return
            }
            
            guard let jsonData = data else {
                print("none data error")
                UIApplication.showIndicator(by: false)
                this.dialog(with: Constants.Misc.ErrorMessage)
                return
            }
            //print(string(data: jsonData, encoding: .utf8))
            do {
                if let json = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [string: string], let pdfLink = json["image"] {
                    onComplete(pdfLink)
                } else {
                    print("serrialization error")
                }
            } catch {
                print("catch error")
                UIApplication.showIndicator(by: false)
                this.handle(this: error)
            }
            
            }.resume()
    }
    
    internal func openPDF(from link: String)-> void {
        DispatchQueue.main.async { [weak self] in
            if let viewer = self?.storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.PDFViewer) as? PDFViewerScene {
                viewer.link = link
                self?.present(UINavigationController(rootViewController: viewer), animated: true, completion: nil)
            }
        }
    }
    
    internal func startSpin()-> void {
        DispatchQueue.main.async { [weak self] in
            guard let this = self else { return }
            this.view.addSubview(this.spinnerView)
            this.spinnerView.addSubview(this.indicator)
            
            this.spinnerView.topAnchor.constraint(equalTo: this.view.topAnchor, constant: 0).isActive = true
            this.spinnerView.rightAnchor.constraint(equalTo: this.view.rightAnchor, constant: 0).isActive = true
            this.spinnerView.bottomAnchor.constraint(equalTo: this.view.bottomAnchor, constant: 0).isActive = true
            this.spinnerView.leftAnchor.constraint(equalTo: this.view.leftAnchor, constant: 0).isActive = true
            
            this.indicator.centerXAnchor.constraint(equalTo: this.spinnerView.centerXAnchor).isActive = true
            this.indicator.centerYAnchor.constraint(equalTo: this.spinnerView.centerYAnchor).isActive = true
            
            this.indicator.startAnimating()
        }
    }
    
    internal func stopSpin()-> void {        
        DispatchQueue.main.async { [weak self] in
            guard let this = self else { return }
            this.indicator.stopAnimating()
            this.indicator.removeFromSuperview()
            this.spinnerView.removeFromSuperview()
        }
    }
    
    internal func showAlert(with title: string, and message: string)-> void {
        DispatchQueue.main.async { [weak self] in
            guard let this = self else { return }
            
            this.view.addSubview(this.alertContainer)
            this.alertContainer.addSubview(this.alertView)
           
            this.alertContainer.topAnchor.constraint(equalTo: this.view.topAnchor, constant: 0).isActive = true
            this.alertContainer.rightAnchor.constraint(equalTo: this.view.rightAnchor, constant: 0).isActive = true
            this.alertContainer.bottomAnchor.constraint(equalTo: this.view.bottomAnchor, constant: 0).isActive = true
            this.alertContainer.leftAnchor.constraint(equalTo: this.view.leftAnchor, constant: 0).isActive = true
            
            this.alertView.widthAnchor.constraint(equalToConstant: this.view.bounds.width*0.70).isActive = true
            this.alertView.heightAnchor.constraint(equalToConstant: this.view.bounds.height*0.23).isActive = true
            this.alertView.centerXAnchor.constraint(equalTo: this.alertContainer.centerXAnchor).isActive = true
            this.alertView.centerYAnchor.constraint(equalTo: this.alertContainer.centerYAnchor).isActive = true
            
            this.lblTitle.text = "Warning!"
            
            this.lblMessage.text = message
            
            this.alertView.addSubview(this.lblTitle)
            this.alertView.addSubview(this.lblMessage)
            this.alertView.addSubview(this.btnDone)
            
            this.lblTitle.topAnchor.constraint(equalTo: this.alertView.topAnchor, constant: 8).isActive = true
            this.lblTitle.centerXAnchor.constraint(equalTo: this.alertView.centerXAnchor).isActive = true
            
            this.btnDone.bottomAnchor.constraint(equalTo: this.alertView.bottomAnchor, constant: -8).isActive = true
            this.btnDone.centerXAnchor.constraint(equalTo: this.alertView.centerXAnchor, constant: 0).isActive = true
            
            this.lblMessage.centerXAnchor.constraint(equalTo: this.alertView.centerXAnchor).isActive = true
            this.lblMessage.centerYAnchor.constraint(equalTo: this.alertView.centerYAnchor).isActive = true
            
            this.btnDone.addTarget(self, action: #selector(this.doneAlertViewOnCLick(_:)), for: .touchUpInside)
            this.view.layoutIfNeeded()
            this.alertView.transform = CGAffineTransform().scaledBy(x: 0, y: 0)
            // show alert with animation
            UIView.animate(withDuration: 0.6, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.7, options: .curveEaseOut, animations: { [weak self] in
                guard let this = self else { return }
                this.alertContainer.alpha = 1.0
                this.alertContainer.backgroundColor = Color.lightGray.withAlphaComponent(0.7)
                this.alertView.transform = CGAffineTransform.identity
            }, completion: { (isFinished) in
                
            })
            
        }
    }
    
    
}



















