
import UIKit
import Gemo

class CarDetails: BaseController {

    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var car: Car? 
    
    // MARK: - Outlets
    
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var carIcon: UIImageView!
    @IBOutlet weak var lblCarType: UILabel!
    @IBOutlet weak var lblPrand: UILabel!
    @IBOutlet weak var lblModel: UILabel!
    @IBOutlet weak var lblStyle: UILabel!
    @IBOutlet weak var lblSpentFuelType: UILabel!
    @IBOutlet weak var fuelDotPanel: View!
    @IBOutlet weak var lblBoardNo: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblCodeTitle: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblBalance: UILabel!
    @IBOutlet weak var lblBalanceTitle: UILabel!
    @IBOutlet weak var lblAddBalanceTitle: UILabel!
    @IBOutlet weak var boardIcon: UIImageView!
    @IBOutlet weak var aLetters: UILabel!
    @IBOutlet weak var aNumbers: UILabel!
    @IBOutlet weak var eLetters: UILabel!
    @IBOutlet weak var eNumbers: UILabel!
    @IBOutlet weak var addBalancePanel: View!
    @IBOutlet weak var stackPanel: UIStackView!
    @IBOutlet weak var usernamePanel: UIView!
    @IBOutlet weak var usernamePanelHeight: NSLayoutConstraint!
    @IBOutlet weak var lblUsernameForCar: UILabel!
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let userType = Usertype(rawValue: UserDefaults.standard.integer(forKey: Misc.UserTypeKey)) ?? .sub
        if (userType == .sub) {
            stackPanel.removeArrangedSubview(addBalancePanel)
            addBalancePanel.isHidden = true
        } else {
            usernamePanel.isHidden = true
            usernamePanelHeight.constant = 0.0            
        }
        
    }
    
    internal override func updateUi() {
        super.updateUi()
        setupNavigationBar(isLogo: false, isMainPage: false, title: Localization.CarDetails.localized())
        let btnMenu = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_more").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(openMenuOnCLick(_:)))
        if (Localizer.instance.current == .arabic) {
            navigationItem.leftBarButtonItem = btnMenu
        } else {
            navigationItem.rightBarButtonItem = btnMenu
        }
        carImage.loadImage(from: car?.userCarPhoto ?? .empty)
        carIcon.loadImage(from: car?.icon ?? .empty)
        lblCarType.text = "\(Localization.CarType.localized())   \(car?.type ?? .empty)"
        lblPrand.text = "\(Localization.Prand.localized())   \(car?.prand ?? .empty)"
        lblModel.text = "\(Localization.Model.localized())   \(car?.model ?? .empty)"
        lblStyle.text = "\(Localization.Style.localized())   \(car?.style ?? .empty)"
        lblSpentFuelType.text = "\(Localization.SpentFuelType.localized())   \(car?.fuel ?? .empty)"
        lblBoardNo.text = Localization.BoardNumber.localized()
        boardIcon.loadImage(from: car?.photo ?? .empty)
        aLetters.text = car?.aLetters ?? .empty
        eLetters.text = car?.eLetters ?? .empty
        aNumbers.text = car?.aNumbers ?? .empty
        eNumbers.text = car?.eNumbers ?? .empty
        lblName.text = Localization.User.localized()
        lblUsername.text = car?.fullname ?? .empty
        userPhoto.loadImage(from: car?.userPhoto ?? .empty)
        lblCode.text = car?.code ?? .empty
        lblCodeTitle.text = Localization.Code.localized()
        lblBalance.text = car?.balance ?? .empty
        lblBalanceTitle.text = Localization.Balance.localized()
        lblAddBalanceTitle.text = Localization.AddBalance.localized()
        userPhoto.rounded(cornerRadius: userPhoto.frame.height/2)
        lblUsernameForCar.text = "\(Localization.UserName.localized())   \(car?.username ?? .empty)"
        aLetters.textAlignment = .center
        eLetters.textAlignment = .center
        aNumbers.textAlignment = .center
        eNumbers.textAlignment = .center

        
    }
    
    
    // MARK: - Actions
    
    @IBAction func addBalanceOnClick(_ sender: UIButton) {
        
    }
    
    @objc fileprivate func openMenuOnCLick(_ sender: UIBarButtonItem) {
        let items: [PopupMainMenu] = [PopupMainMenu(title: Localization.Reports.localized()),
                                      PopupMainMenu(title: Localization.Edit.localized()),
                                      PopupMainMenu(title: Localization.Delete.localized())]
        let x = (Localizer.instance.current == .arabic) ? 0.0 : UIScreen.main.bounds.width - 5
        showPopover(in: Rect(x: x, y: 0, width: 0, height: 0), with: items, with: Size(width: 110, height: 3*45)) { [weak self] (index) in
            switch (index) {
            case 0: //reports
                guard let reportPerCarPage = self?.storyboard?.instantiateViewController(withIdentifier: Storyboards.ReportsPerCar) as? ReportsPerCar else {
                    return
                }
                reportPerCarPage.car = self?.car
                self?.navigate(to: reportPerCarPage)
                
            case 1: // edition
                guard let editCarPage = self?.storyboard?.instantiateViewController(withIdentifier: Storyboards.AddCar) as? AddCar else {
                    return
                }
                editCarPage.isUpdate = true
                editCarPage.car = self?.car
                self?.navigate(to: editCarPage)
                
            case 2: // delete
                Gemo.gem.startSpinning()
                let paramters: Dictionary<String, any> = [
                    Misc.MainKey: Misc.MainValue,
                    Misc.LanguageKey: Localizer.instance.current.rawValue,
                    "id": self?.car?.id ?? .empty,
                    "user_id": UserDefaults.standard.string(forKey: Misc.UserId) ?? .empty
                ]
                Http.request(link: Misc.getUrl(for: "delete_car"), method: .post, parameters: paramters)
                    .response { [weak self] (response) in
                        let error = response.error
                        if (error != nil) {
                            Gemo.gem.stopSpinning()
                            print("\(#function) error: \(error!)")
                            self?.displayAlert(with: error?.localizedDescription)
                            return
                        }
                        Gemo.gem.stopSpinning()
                        //print(response.jsonString)
                        guard let json = response.result as? Dictionary<string, any>,
                            let success = json["result"] as? string,
                            let message = json["data"] as? string else {
                            self?.displayAlert(with: Localization.NoData.localized())
                            return
                        }
                        if (success == "true") {
                            Threads.main { [weak self] in
                                self?.navigationController?.popViewController(animated: true)
                            }
                        } else {
                            self?.displayAlert(with: message)
                        }
                    }
            default:
                break
            }
        }
    }
    

}






























