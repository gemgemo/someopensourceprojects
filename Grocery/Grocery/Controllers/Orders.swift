
import UIKit
import Gemo

final class Orders: BaseController
{

    
    // MARK: - Constatnts
    
    
    // MARK: - Variables
    
    internal var isLastOrders = false
    fileprivate var dataSource = Array<Order>() { didSet {
        Threads.main { [weak self] in
            self?.tblOrders.reloadData()
        }}}
    fileprivate enum OrderState {
        case last, current
    }
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var tblOrders: UITableView! { didSet {
        tblOrders.delegate = self
        tblOrders.dataSource = self
        }}
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        prepareTableview()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        selectedTabBarItem = 1
        tabBarItems?.selectItem(at: IndexPath(item: selectedTabBarItem, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        (isLastOrders) ? fetchOrders(with: .last) : fetchOrders(with: .current)
    }
    
    internal override func updateUi() {
        setupMenuButton()
        navigationItem.title = (isLastOrders) ? Localization.LastOrders.localized() : Localization.CurrentOrders.localized()
        
    }
    
    
    
    // MARK: - Actions
    
    @objc fileprivate func editStateOnClick(_ sender: UIButton) {
        guard Gemo.gem.isConnected else {
            dialogUserWithNoInternetConnectionMessage()
            return
        }
        guard !isLastOrders else { return }
        guard let cell = tblOrders.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as? OrderCell else { return }
        let items = [PopupItem(id: 0, title: Localization.ProcessingOfTheOrder.localized()),
                     PopupItem(id: 1, title: Localization.ConnectingOrder.localized()),
                     PopupItem(id: 2, title: Localization.OrderDelivered.localized())]
        let rect = cell.bottomPanel.convert(sender.frame, to: view)
        let id = dataSource[sender.tag].id ?? .empty
        openPopover(with: items, in: rect, and: Size(width: 230, height: 125), .init(rawValue: 0), true) { (index) in
            switch (index) {
            case 1:
                Order.action(this: "order_2", for: id) {  [weak self] (data) in
                    /*self?.dataSource.removeAll()
                    self?.dataSource = data*/
                    self?.fetchOrders(with: .current)
                }
                
            case 2:
                Order.action(this: "order_3", for: id) { [weak self] (data) in
                    //self?.dataSource.removeAll()
                    //self?.dataSource = data
                    Threads.main { [weak self] in
                        self?.navigationController?.popToRootViewController(animated: true)
                    }
                    
                }
                
            default:
                break
            }
        }      
    }

}

// MARK: - Helper functions 

extension Orders
{
    
    fileprivate func prepareTableview()-> void {
        tblOrders.alwaysBounceVertical = true
        tblOrders.register(Nibs.Order, forCellReuseIdentifier: ReuseIdentifiers.Order)
        let insets = Insets(top: 0, left: 0, bottom: 60, right: 0)
        tblOrders.scrollIndicatorInsets = insets
        tblOrders.contentInset = insets
    }
    
    fileprivate func fetchOrders(with state: OrderState)-> void {
        guard Gemo.gem.isConnected else {
            dialogUserWithNoInternetConnectionMessage()
            return
        }
        let paramters: Dictionary<string, any> = [
            Misc.MainKey: Misc.MainValue,
            Misc.LanguageParameterName: Localizer.instance.current.rawValue,
            "id_matjer": UserDefaults.standard.string(forKey: Misc.GroceryId) ?? .empty
        ]
        var link: String
        switch (state) {
        case .last:
            link = Urls.LastOrders
        case .current:
            link = Urls.CurrentOrders
        }
        Order.fetchOrders(by: link, with: paramters) { [weak self] (orders) in
            self?.dataSource = orders
        }
    }

    
}


// MARK: - Table views delegate & data source functions

extension Orders: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifiers.Order, for: indexPath) as? OrderCell else { return UITableViewCell() }
        cell.isLastOrder = isLastOrders
        cell.order = dataSource[indexPath.row]
        if (!isLastOrders) {
            cell.btnEditOrderState.tag = indexPath.row
            cell.btnEditOrderState.addTarget(self, action: #selector(editStateOnClick(_:)), for: .touchUpInside)
        }
        return cell
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print("selected cell: \(indexPath)")
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: cgFloat = 60 + 40 + 160 + 30
        if (!isLastOrders) {
            height += 40 + 16
        }
        let products = dataSource[indexPath.row].products
        if (!products.isEmpty) {
            height += (products.count*80).toCGFloat
        }
        return height

    }
    
}



















