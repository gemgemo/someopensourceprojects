
import UIKit

final class SendComplaint: BaseController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var storeId: string?
    
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var titlePanel: UIView! { didSet { shadow(for: titlePanel) } }
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var lblComeliantTitle: UILabel!
    @IBOutlet fileprivate weak var txfEmail: UITextField! { didSet { txfEmail.delegate = self } }
    @IBOutlet fileprivate weak var txfCompelaintTitle: UITextField! { didSet { txfCompelaintTitle.delegate = self } }
    @IBOutlet fileprivate weak var txtComplaintContent: TextView!
    @IBOutlet fileprivate weak var btnSendComlpaint: Button!
    @IBOutlet fileprivate weak var btnBack: UIButton!
    
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    
    // MARK: - Actions

    @IBAction private func backOnClick(_ sender: UIButton) {
        back()
    }
    
    @IBAction private func sendComplaintOnClick(_ sender: Button) {
        (Gemo.gem.isConnected) ? sendComplaint() : Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
    }
    
    
    
}



// MARK: - Helper Functions

extension SendComplaint
{
    
    fileprivate func updateUi()-> void {
        tabBar.isHidden = true
        view.bringSubview(toFront: titlePanel)
        btnSendComlpaint.setTitle(Localization.Send.localized(), for: .normal)
        lblTitle.text = Localization.SendComplaint.localized()
        lblComeliantTitle.text = Localization.SendComplaint.localized()
        txfEmail.placeholder = Localization.Email.localized()
        txfCompelaintTitle.placeholder = Localization.ComplaintTitle.localized()
        txtComplaintContent.placeholder = Localization.ComplaintText.localized()
        btnBack.transform = CGAffineTransform.identity
        if (Localizer.instance.current == .arabic) {
            btnBack.leftAnchor.constraint(equalTo: titlePanel.leftAnchor, constant: 8).isActive = true
            lblComeliantTitle.textAlignment = .right
            txfEmail.textAlignment = .right
            txfCompelaintTitle.textAlignment = .right
            txtComplaintContent.textAlignment = .right
        } else {
            btnBack.rightAnchor.constraint(equalTo: titlePanel.rightAnchor, constant: -8).isActive = true
            btnBack.transform = btnBack.transform.rotated(by: cgFloat.pi)
            lblComeliantTitle.textAlignment = .left
            txfEmail.textAlignment = .left
            txfCompelaintTitle.textAlignment = .left
            txtComplaintContent.textAlignment = .left
        }
    }
    
    fileprivate func sendComplaint()-> void {
        guard email.isEmail else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InvaildEmail.localized(), in: view)
            return
        }
        guard !complaintTitle.isEmpty || !complaintText.isEmpty else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.SomeDataReqiured.localized(), in: view)
            return
        }
        var parameters: [string: any] = [
            Constants.Misc.MainKey: Constants.Misc.MainValue,
            "user_id": UserDefaults.standard.string(forKey: Constants.Misc.UserID) ?? .empty,
            "email": email,
            "subject": complaintTitle,
            "message": complaintText
        ]
        if (storeId != nil) {
            parameters["id_matjer"] = storeId ?? .empty
        }
        print("complint parameters: \(parameters)")
        Gemo.gem.startSpinning(in: view)
        HttpClient.request(link: Constants.Urls.Complaint, method: .post, parameters: parameters) { [weak self] (result, response, error) -> void in
            guard let this = self else { return }
            if (error != nil) {
                Gemo.gem.stopSpinning()
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription, in: this.view)
                return
            }
            Gemo.gem.stopSpinning()
            print("complaint request result is: \(string(describing: result))")
            if let json = (result as? [Dictionary<string, any>])?.first,
                let aMessage = json["data"] as? string,
                let eMessage = json["data_en"] as? string {
                Gemo.gem
                    .showAlert(withTitle: Localization.Warning.localized(),
                                   message: (Localizer.instance.current == .arabic) ? aMessage : eMessage,
                                   in: this.view)
                ThreadQueue.main { [weak self] in
                    self?.txtComplaintContent.text = .empty
                    self?.txfEmail.text = .empty
                    self?.txfCompelaintTitle.text = .empty
                }
            }
        }
    }
    
}


// MARK: - Computed properties

extension SendComplaint
{
    
    fileprivate var email: string {
        return txfEmail.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var complaintTitle: string {
        return txfCompelaintTitle.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var complaintText: string {
        return txtComplaintContent.text.trimmingCharacters(in: .whitespaces)
    }
    
}


// MARK: - Text Field delegate functions

extension SendComplaint: UITextFieldDelegate
{
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}

























