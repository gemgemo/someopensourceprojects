
import UIKit


final class MenuCell: UITableViewCell, CellProvider
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    internal var item: MenuItem? {
        didSet {
            updateUI()
        }
    }
    
    
    // MARK: - Outlets
    
    @IBOutlet private weak var lblRowTitle: UILabel!
    @IBOutlet private weak var container: UIView!
    
    
    // MARK: - Overridden functions
    
    internal override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    internal override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        container.backgroundColor = (selected) ?  .selected : .normal
    }
    
    internal func updateUI() {
        if let rowName = item?.title {
            lblRowTitle.text = rowName
        }
    }
    
    //MARK: - Actions
  

}






protocol CellProvider: class {
    func updateUI()-> void
}
