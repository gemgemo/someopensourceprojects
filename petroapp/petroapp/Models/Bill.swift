
import Foundation
import Gemo

class Bill: Mapper<any> {
    internal var carPhoto, carPrand, carModel, carType, carIcon, carTypePhoto, carStyle, fuel, carCode, fName, userPhoto, aNumbers, aLetters, eNumbers, eLetters, id, userId, carId, price, liters, timestamp, name, latitude, longitude, address: string?
    
    override func mapping(mapper: Mapper<any>) {
        carPhoto = mapper["photo_car"]?.string
        carPrand = mapper["brand_car"]?.string
        carModel = mapper["model_car"]?.string
        carType = mapper["type_car"]?.string
        carIcon = mapper["photo_b"]?.string
        carTypePhoto = mapper["photo_n"]?.string
        carStyle = mapper["year_car"]?.string
        fuel = mapper["fuel_type"]?.string
        carCode = mapper["code_car"]?.string
        fName = mapper["full_name"]?.string
        userPhoto = mapper["photo_user"]?.string
        aNumbers = mapper["num_ar"]?.string
        eNumbers = mapper["num_en"]?.string
        aLetters = mapper["ar_car"]?.string
        eLetters = mapper["en_car"]?.string
        id = mapper["id"]?.string
        userId = mapper["user_id"]?.string
        carId = mapper["car_id"]?.string
        price = mapper["price"]?.string
        liters = mapper["num_lit"]?.string
        timestamp = mapper["time_add"]?.string
        name = mapper["petro_name"]?.string
        latitude = mapper["petro_lati"]?.string
        longitude = mapper["petro_longi"]?.string
        address = mapper["petro_address"]?.string
        
    }
    
    class func fetchBills(from funcname: string, _ page: BaseController?, _ completion: @escaping([Bill])->())-> void {
        guard Gemo.gem.isConnected else {
            page?.displayAlert(with: Localization.InternetConnection.localized())
            return
        }
        let id = (funcname == "all_report_1") ? "user_id" : "id"
        let parameters: [string: any] = [
            Misc.MainKey: Misc.MainValue,
            Misc.LanguageKey: Localizer.instance.current.rawValue,
            id: UserDefaults.standard.string(forKey: Misc.UserId) ?? .empty
        ]
        Gemo.gem.startSpinning()
        Http.request(link: Misc.getUrl(for: funcname), method: .post, parameters: parameters) //all_report_sub, "all_report_1"
            .response { (response) in
                let error = response.error
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("\(#function) error: \(error!)")
                    page?.displayAlert(with: error?.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
                guard let json = response.result as? [Dictionary<String, any>] else {
                    page?.displayAlert(with: Localization.NoData.localized())
                    return
                }
                completion(json.map { Bill(JSON: $0)})
        }
    }
    
}







