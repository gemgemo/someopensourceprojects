
import Foundation
import UIKit

final class SideMenuCell: BaseTableCell
{
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var item: MenuItem? {
        didSet {
            updateUi()
        }
    }
    
    
    // MARK: - Outlets
    
    @IBOutlet private weak var mainPanal: UIView!
    @IBOutlet private weak var icon: UIImageView!
    @IBOutlet private weak var lblTitle: UILabel! {
        didSet {
            lblTitle.text = .empty
        }
    }
    
    
    // MARK: - Overridden functions
    
    internal override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        mainPanal.backgroundColor = (selected) ? .lightGray : .white
    }
    
    internal override func updateUi() {
        lblTitle.text = item?.title ?? .empty
        icon.image = item?.icon        
        if (Localizer.instance.current == .arabic) {
            icon.rightAnchor.constraint(equalTo: mainPanal.rightAnchor, constant: -16.0).isActive = true
            lblTitle.rightAnchor.constraint(equalTo: icon.leftAnchor, constant: -16.0).isActive = true
            lblTitle.leftAnchor.constraint(equalTo: mainPanal.leftAnchor, constant: 16.0).isActive = true
            lblTitle.textAlignment = .right
        } else {
            icon.leftAnchor.constraint(equalTo: mainPanal.leftAnchor, constant: 16.0).isActive = true
            lblTitle.rightAnchor.constraint(equalTo: mainPanal.rightAnchor, constant: -16.0).isActive = true
            lblTitle.leftAnchor.constraint(equalTo: icon.rightAnchor, constant: 16.0).isActive = true
            lblTitle.textAlignment = .left
        }
    }
    
    
    // MARK: - Actions

    
}











internal class BaseTableCell: UITableViewCell {
    internal func updateUi()-> void {
        
    }
}






































