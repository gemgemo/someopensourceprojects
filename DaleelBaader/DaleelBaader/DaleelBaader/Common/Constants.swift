
import Foundation
import UIKit
import Localize_Swift


internal struct Constants
{
    
    internal struct Storyboard {
        internal static let Main = "mainscreenid", MainNav = "mainNavBarid", SideMenu = "sideMenuviewid", ManagerMessage = "managerMessageviewid", AboutUs = "aboutUsViewid", Services = "servicesViewid", AlshaaerAlmuqdasa = "alshaaeralmuqdasaViewid", Transport = "transportViewdid", Videos = "videosViewdwid", Video = "videoDetailsviewid", Map = "mapviewid", ContactUs = "contactViewid", PDFViewer = "pdfViewerId", ImagePreviwer = "com.image.preview.view"
    }
    
    internal struct Nib {
        internal static let InfoTitles = UINib(nibName: "InfoTitlesCell", bundle: nil), InfoDetails = UINib(nibName: "InfoDetails", bundle: nil), AlshaaerAlmuqdasa = UINib(nibName: "AlshaaerAluqdsaCell", bundle: nil), Info = UINib(nibName: "InfoCell", bundle: nil)
    }
    
    internal struct ReuseIdentifier {
        internal static let SideMenu = "sideMenuCell", MainCell = "maincellview", AboutUs = "aboutUsview", InfoTitles = "infotitlesviewid", InfoDetails = "infoDetails", AlshaaerAlmuqdasa = "alshaaerAlmuqdasaCellid", Transport = "transportcellid", Video = "videocellid", Info = "com.inf.cell.view"
    }
    
    internal struct Misc {
        internal static let FontDroidNaskhShiftAlt = "DroidNaskhShiftAlt",
        FontHacenPromoterMd = "HacenPromoterMd",
        ArabicSign = "ar-SA", EnglishSign = "en",
        AppLink = "https://itunes.apple.com/us/app/دليل-بادر-التطوعي/id1207634497?ls=1&mt=8",
        ErrorMessage = "ErrorMessage".localized(),
        TranslatedAppLink = AppLink,
        InternetMessage = "internetConnectionmessage".localized()
    }
    
    internal struct HttpMethod {
        internal static let Post = "POST",
        Get = "GET"
    }
    
    internal struct BackendURLs {
        internal static let BaseURL = "http://192.232.214.91/" ,
                            AlshaaerAlmuqdasaAR = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON/sacred.php?key=Daleel_Bader",
                            AlshaaerAlmuqdasaEN = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON_EN/sacred.php?key=Daleel_Bader",
                            HealthServicesAR = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON/serv_health.php?key=Daleel_Bader",
                            HealthServicesEN = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON_EN/serv_health.php?key=Daleel_Bader",
                            HotelServicesAR = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON/serv_hotel.php?key=Daleel_Bader",
                            HotelServicesEN = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON_EN/serv_hotel.php?key=Daleel_Bader",
                            EashaaServicesAR = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON/aliment.php?key=Daleel_Bader",
                            EashaaServicesEN = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON_EN/aliment.php?key=Daleel_Bader",
                            BaaderCenterAR = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON/bader_center.php?key=Daleel_Bader",
                            BaaderCenterEN = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON_EN/bader_center.php?key=Daleel_Bader",
                            ErshaadAR = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON/wand.php?key=Daleel_Bader",
                            ErshaadEN = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON_EN/wand.php?key=Daleel_Bader",
                            TranslationAR = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON/trans.php?key=Daleel_Bader",
                            TranslationEN = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON_EN/trans.php?key=Daleel_Bader",
                            MenAR = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON/man_bath.php?key=Daleel_Bader",
                            MenEN = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON_EN/man_bath.php?key=Daleel_Bader",
                            WomenAR = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON/wo_bath.php?key=Daleel_Bader",
                            WomenEN = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON_EN/wo_bath.php?key=Daleel_Bader",
                            VideosAR = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON/videos.php?key=Daleel_Bader",
                            VideosEN = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON_EN/videos.php?key=Daleel_Bader",
                            CallUsGet = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON/contact_us_2.php?key=Daleel_Bader",
                            CallUsPost = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON/contact_us.php?key=Daleel_Bader",
                            ManagerMessage = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON/mang_w.php?key=Daleel_Bader",
                            AboutUs = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON/about_us.php?key=Daleel_Bader",
                            OurServices = "\(BackendURLs.BaseURL)~baaderapplicatio/JSON/our_serv.php?key=Daleel_Bader"
    }
}

























