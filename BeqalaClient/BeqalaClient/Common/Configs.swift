
import Foundation
import KYDrawerController


internal struct Constants
{

    // MARK: - Storyboard
    struct Storyboards {
        internal static let RegisterMainNav = "com.register.main.nav.view.id",
                            SignIn = "com.signIn.view.id",
                            SignUp = "com.signup.view.id",
                            MainNavCon = "com.main.nav.con.id",
                            Deparments = "com.departments.view.id",
                            SideMenu = "com.side.menu.view.id",
                            Profile = "com.profile.view.id",
                            Settings = "com.settings.view.id",
                            AboutApp = "com.about.app.view.id",
                            TabBar = "com.tab.bar.view.id",
                            Promotions = "com.promotions.view.id",
                            Basket = "com.basket.view.id",
                            ShopsLocations = "com.shops.locations.view.id",
                            CategoryDetails = "com.category.details.view.id",
                            Shops = "com.shops.view.id",
                            MapPopup = "com.map.popup.view.id",
                            Favorites = "com.favorites.view.id",
                            CurrentOrders = "com.current.orders.view.id",
                            Store = "com.store.page.id",
                            StoreInfo = "com.store.info.page.id",
                            StoreProducts = "com.store.products.page.id",
                            ShoppingOne = "com.shopping.one.view.id",
                            StoreShopping = "com.store.shopping.view.id",
                            ReceiverOrder = "com.receiver.view.id",
                            ProductDetails = "com.product.info.view.id",
                            Search = "com.search.view.id",
                            SendComplaint = "com.send.complaint.view.id"
    }
    
    // MARK: - Nibs
    struct Nibs {
        internal static let Menu = UINib(nibName: "MenuCell", bundle: nil),
                            Slider = UINib(nibName: "SliderCell", bundle: nil),
                            Category = UINib(nibName: "CategoryCell", bundle: nil),
                            SectionDetails = UINib(nibName: "SectionDetailsCell", bundle: nil),
                            Shop = UINib(nibName: "ShopCell", bundle: nil),
                            Basket = UINib(nibName: "BasketItem", bundle: nil),
                            Order = UINib(nibName: "Order", bundle: nil),
                            ProductOrder = UINib(nibName: "ProductOrderCell", bundle: nil)
    }
    
    // MARK: - Reuse Identifier
    struct ReuseIdentifiers {
        internal static let SideMenu = "com.side.menu.cell.view.id",
                            Slider = "com.slider.cell.view.id",
                            Category = "com.category.cell.view.id",
                            SectionDetails = "com.category.cell.view.id",
                            Shop = "com.shop.cell.view.id",
                            Basket = "com.basket.item.cell.view.id",
                            ProductSlider = "com.product.slider.view.cell.id",
                            Order = "com.order.cell.view.id",
                            ProductOrder = "com.product.order.cell.view.id"
    }
    
    // MARK: - Misc
    struct Misc {
        internal static let MainKey = "key",
        MainValue = "BeQALa",
        UserID = "com.user.id",
        IsLoggedIn = "com.is.logged.in.id",
        FirstName = "com.first.name.id",
        LastName = "com.last.name.id",
        Phone = "com.phone.id",
        Email = "com.user.email.id",
        UserName = "com.username.id.saved",
        Gender = "com.user.gender.id",
        LanguageBackendKey = "lang",
        MapKey = "AIzaSyCUnUBl_V3_dI3mGKQLljjpVNAbBhbkU0A",
        UserLatitude = "com.user.latitude.id",
        UserLongitude = "com.user.longitude.id"
    }
    
    
    // MARK: - Backend Urls
    
    struct Urls {
        private static let base = "http://192.232.214.91/~shopkeeper/JSON_CLIENT/", ext = ".php"
        internal static let SignUp = getUrl(from: "register"),
                            SignIn = getUrl(from: "login"),
                            ResetPassword = getUrl(from: "recover"),
                            EditProfile = getUrl(from: "edit_user"),
                            AboutApp = "http://192.232.214.91/~shopkeeper/JSON/about_us.php",
                            Categories = getUrl(from: "category"),
                            Slider = getUrl(from: "all_offers"),
                            Promotions = getUrl(from: "offer_product"),
                            ProductsPerSection = getUrl(from: "all_product"),
                            Favorite = getUrl(from: "add_fav"),
                            Basket = getUrl(from: "add_cart"),
                            Shops = getUrl(from: "all_matjer"),
                            Favorites = getUrl(from: "select_all_fav"),
                            DeleteOneFavoriteProduct = getUrl(from: "delete_one_fav"),
                            Store = getUrl(from: "one_matjer"),
                            StoreProducts = getUrl(from: "all_product_matjer"),
                            SortByPrice = getUrl(from: "all_product_price"),
                            SortBySection = getUrl(from: "all_product_cat"),
                            BasketForUser = getUrl(from: "select_all_carts_2"),
                            RemoveItem = getUrl(from: "delete_one_cart"),
                            SendOrder = getUrl(from: "select_all_carts"),
                            CurrentOrders = getUrl(from: "select_all_orders_2"),
                            OneProduct = getUrl(from: "one_product"),
                            DeviceToken = getUrl(from: "users_noti"),
                            Search = getUrl(from: "search"),
                            Logout = getUrl(from: "delete_token"),
                            Complaint = getUrl(from: "comp"),
                            RateStore = getUrl(from: "add_rate_matjer"),
                            RateProduct = getUrl(from: "add_rate_product"),
                            StoreSections = getUrl(from: "matjer_cat")
        
        private static func getUrl(from funcName: string)-> string {
            return "\(base)\(funcName)\(ext)"
        }
        
        internal static func storsLocations()-> string {
            let latitude = UserDefaults.standard.value(forKey: Constants.Misc.UserLatitude) as? string ?? .empty
            let longitude = UserDefaults.standard.value(forKey: Constants.Misc.UserLongitude) as? string ?? .empty
            return "http://192.232.214.91/~shopkeeper/JSON_CLIENT/side.php?latitude=\(latitude)&longitude=\(longitude)"
        }
    }
    

}


// MARK: - Enumerations

internal enum Gender: int {
    case male = 1, female = 2
}


// MARK: - Extensions

extension Font {
    
    internal static var fedra: Font {
        return Font(name: "FedraArabic_beta", size: 16.0) ?? Font.systemFont(ofSize: 16.0)
    }
    
}

extension Color {
    
    internal static var main: Color {
        return Color.rgb(red: 77, green: 111, blue: 139, alpha: 1.0)
    }
    
}


// MARK: - App Manager

final class AppManager: NSObject
{
    
    internal static var manage: AppManager {
        struct Object {
            fileprivate static let manager = AppManager()
        }
        return Object.manager
    }
    
    internal func sideMenu(for window: UIWindow?)-> void {
        guard let win = window else { return }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let sideMenu = storyboard.instantiateViewController(withIdentifier: Constants.Storyboards.SideMenu) as? SideMenu
        let initialScreen = storyboard.instantiateInitialViewController()
        let drawer = KYDrawerController(drawerDirection: (Localizer.instance.current == .arabic) ? .right : .left, drawerWidth: win.bounds.width*0.80)
        drawer.mainViewController = initialScreen
        drawer.drawerViewController = sideMenu
        win.rootViewController = drawer
        
    }
    
    internal func setupNavigationBar()-> void {
        let nav = UINavigationBar.appearance()
        nav.isHidden = true
    }
    
}

// MARK: - Notification names

extension NSNotification.Name
{
    
    internal static var CalculatePriceKey: Notification.Name {
        return Notification.Name("com.calc.total.price.id")
    }
    
    internal static var OpenProductDetails: Notification.Name {
        return Notification.Name("com.product.details.by.id.io")
    }
    
    internal static var OpenCurrentOrders: Notification.Name {
        return Notification.Name("com.open.current.orders")
    }
    
}


// MARK: - Localization

struct Localization {
    internal static let Warning = "warninglan",
    InternetConnection = "internentconnectionlan",
    FirstNameReqiured = "firstnamereqlan",
    SecondNameReqiured = "secondnamereqlan",
    UserNameReqiured = "usernamereqlan",
    InvaildEmail = "invalidemaillan",
    PhoneNumberReqiured = "phonenumberreqlan",
    PasswordReqiured = "passwordreqlan",
    RePasswordReqiured = "repasswordreqlan",
    PasswordNotConfirmed = "unconfirmedpasswordlan",
    Error = "errorlan",
    OpenLocation = "openlocationlan",
    InvalidLocation = "invalidlocationlan",
    PasswordCount = "passwordcountlan",
    Profile = "profilelan",
    Main = "mainlan",
    Basket = "basketlan",
    Promotions = "promotionslan",
    Shops = "shopslan",
    Favorits = "favoritslan",
    Orders = "orderslan",
    Settings = "settingslan",
    AboutApp = "aboutapplan",
    UpdateProfile = "updateprofilelan",
    Logout = "logoutlan",
    AppLanguage = "chanageapplangaugelan",
    FirstName = "firstNamelan",
    LastName = "lastnamelan",
    UserName = "usernamelan",
    Email = "emaillan",
    PhoneNumber = "phonenumberlan",
    Password = "passwordlan",
    RePassword = "re-passwordlan",
    Male = "malelan",
    Female = "femalelan",
    Register = "registerlan",
    NewSignUp = "newsignuplan",
    EditProfile = "editprofillan",
    NoData = "nodatalan",
    ShowInMenu = "showinmenulan",
    Sections = "departmentstitlelan",
    CurrentOrders = "currentorderstitlelan",
    AboutStore = "storeinfotitlelan",
    StoreProducts = "storeproductstitlelan",
    Location = "locationtitlelan",
    Types = "typestitlelan",
    ProductsCount = "productscounttitlelan",
    Rate = "ratetitlelan",
    Product = "producttitlelan",
    SortedBy = "sortedbytitlelan",
    Submit = "submittitlelan",
    Section = "sectiontitlelan",
    Price = "pricetitlelan",
    All = "alltitlelan",
    Juices = "Juicestitlelan",
    Cocktail = "cocktailtitlelan",
    IceCream = "icecreamtitlelan",
    ColdDrinks = "Colddrinkstitlelan",
    SoftDrinks = "Softdrinkstitlelan",
    Default = "defaulttitlelan",
    HighestPrice = "fromhieghestpricetitlelan",
    LowestPrice = "fromlowestpricelan",
    Total = "totaltitlelan",
    SendOrder = "sendordertitlelan",
    SR = "srtitlelan",
    Shopping = "shoppingtitlelan",
    DeliveryWay = "Choosethedeliverymethodlan",
    DeliveryByStore = "deliverybystorelan",
    ReceivingOrder = "receivingorderlan",
    Continuation = "continuationlan",
    Name = "nametitlelan",
    BuildinHouseNumber = "buildingandhousenumberlan",
    Floor = "floortitlelan",
    Send = "sendtitlelan",
    AddTitle = "addtitlelan",
    SomeDataReqiured = "somedatareqlan",
    RecipientName = "Recipient'snametitlelan",
    AddRecipient = "Recipient'saddtitlelan",
    Description = "descriptiontitlelan",
    OrderState = "orderstatetitlelan",
    Store = "storetitlelan",
    DeliveryMode = "deliverymodetitlelande",
    Building = "buildingtitlelantow",
    OrderDate = "orderdatetitlelan",
    Quantity = "quantitytitlelan",
    SendComplaint = "sendcomplainttitlelan",
    ComplaintTitle = "complainttitlelantile",
    ComplaintText = "complaintcontenttitlelan",
    AddRate = "addratetitlelan",
    ForgotPassword = "Forgotpasswordtitlelan",
    Login = "logintitlelan",
    RegisterButton = "btnregistertitlelan",
    QuantityAvailable = "Quantityavailabletitlelan",
    Recipient = "com.recipient.title.lan",
    Mobile = "com.mobile.title.lan",
    MyAccount = "com.my.account.title.lan"
    
}






















