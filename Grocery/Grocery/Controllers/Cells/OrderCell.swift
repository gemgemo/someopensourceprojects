
import Foundation
import Gemo


final class OrderCell: BaseTableCell
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var isLastOrder = false
    internal var order: Order? { didSet {updateUi()} }
    fileprivate var dataSource = Array<NotificationProduct>() { didSet {
        tblProducts.reloadData()
        tableViewHeight.constant = tblProducts.contentSize.height
        }}
    
    
    // MARK: - Outlets
    
    @IBOutlet private weak var mainPanel: View! { didSet {
        mainPanel.setCorner(radius: 10)
        mainPanel.setBorder(with: 1, and: Color(white: 0.88, alpha: 0.9))
        }}
    @IBOutlet private weak var headerPanel: UIView!
    @IBOutlet private weak var lblOrderState: UILabel!
    @IBOutlet private weak var lblTimestamp: UILabel!
    @IBOutlet private weak var userIcon: UIImageView!
    @IBOutlet private weak var lblUsername: UILabel!
    @IBOutlet private weak var lblEnterdPhone: UILabel!
    @IBOutlet internal weak var bottomPanel: UIView!
    @IBOutlet private weak var lblAddOrderState: UILabel!
    @IBOutlet private weak var lblAddOrderStateStatus: UILabel!
    @IBOutlet internal weak var btnEditOrderState: UIButton!
    @IBOutlet private weak var bottomPanelHeight: NSLayoutConstraint!
    @IBOutlet private weak var footerPanel: View!
    @IBOutlet private weak var lblDeliveryWayTitle: UILabel!
    @IBOutlet private weak var lblDeliveryWay: UILabel!
    @IBOutlet private weak var locationPanel: UIView!
    @IBOutlet private weak var lblLocation: UILabel!
    @IBOutlet private weak var lblBuildingTitle: UILabel!
    @IBOutlet private weak var lblBuilding: UILabel!
    @IBOutlet private weak var lblFloorTitle: UILabel!
    @IBOutlet private weak var lblFloor: UILabel!
    @IBOutlet private weak var tblProducts: UITableView! { didSet {
        tblProducts.delegate = self
        tblProducts.dataSource = self
        }}
    @IBOutlet private weak var locationPanelHeight: NSLayoutConstraint!
    @IBOutlet private weak var tableViewHeight: NSLayoutConstraint! { didSet { tableViewHeight.constant = 0.0 } }
    @IBOutlet private weak var locationIcon: UIImageView!
    @IBOutlet private weak var lblTotal: UILabel! { didSet { lblTotal.text = .empty }}
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        //print("order: \(order)")
        setupConstraints()
        tblProducts.alwaysBounceVertical = false
        tblProducts.alwaysBounceHorizontal = false
        tblProducts.register(Nibs.NotificationProduct, forCellReuseIdentifier: ReuseIdentifiers.NotificationProduct)
        bottomPanel.isHidden = isLastOrder
        if let products = order?.products {
            dataSource = products
        }
        if (isLastOrder) {
            lblOrderState.text = Localization.OrderFinished.localized()
        } else {
            lblOrderState.text = order?.typeStatus
        }
        lblTimestamp.text = "\(order?.date ?? .empty)  \(order?.time ?? .empty)"
        if let gender = order?.gender {
            userIcon.image = (gender == "1") ? #imageLiteral(resourceName: "man") : #imageLiteral(resourceName: "women")
        }
        lblUsername.text = order?.username
        lblEnterdPhone.text = "\(Localization.Call.localized()) \(order?.enterdPhone ?? .empty)"
        lblDeliveryWayTitle.text = Localization.DeliveryWay.localized()
        lblDeliveryWay.text = order?.type ?? .empty
        lblLocation.text = order?.address ?? .empty
        if let building = order?.building, building.isEmpty {
            // recieved from recipient
            lblBuildingTitle.text = Localization.RecipientName.localized()
            lblFloorTitle.text = Localization.PhoneNumber.localized()
            lblBuilding.text = order?.name ?? .empty
            lblFloor.text = order?.phone ?? .empty
            locationPanel.isHidden = true
            locationPanelHeight.constant = 0.0
        } else {
            // recieved from building
            lblBuildingTitle.text = "\(Localization.Building.localized()): "
            lblFloorTitle.text = "\(Localization.Floor.localized()): "
            lblBuilding.text = order?.building ?? .empty
            lblFloor.text = order?.floor ?? .empty
            locationPanel.isHidden = false
            locationPanelHeight.constant = 60.0
        }
        lblAddOrderState.text = Localization.AddOrderState.localized()
        lblAddOrderStateStatus.text = order?.typeStatus ?? .empty
        Threads.background(QOS: .userInitiated) { [weak self] in
            var total = 0
            self?.order?.products.forEach { total += $0.price ?? 0 }
            Threads.main { [weak self] in
                self?.lblTotal.text = "\(Localization.Total.localized()) \(total.toDouble) \(Localization.SR.localized())"
            }
        }
        
        
    }
    
    internal override func updateConstraints() {
        super.updateConstraints()
        bottomPanelHeight.constant = (isLastOrder) ? 0.0 : 40.0
    }
    
    
    // MARK: - Functions 
    
    private func setupConstraints()-> void {
        if (Localizer.instance.current == .arabic) {
            lblLocation.textAlignment = .right
            //header
            lblOrderState.rightAnchor.constraint(equalTo: headerPanel.rightAnchor, constant: -8).isActive = true
            lblTimestamp.leftAnchor.constraint(equalTo: headerPanel.leftAnchor, constant: 0).isActive = true
            userIcon.rightAnchor.constraint(equalTo: headerPanel.rightAnchor, constant: -8).isActive = true
            lblUsername.rightAnchor.constraint(equalTo: userIcon.leftAnchor, constant: -8).isActive = true
            lblEnterdPhone.leftAnchor.constraint(equalTo: headerPanel.leftAnchor, constant: 0).isActive = true
            //footer
            lblDeliveryWayTitle.rightAnchor.constraint(equalTo: footerPanel.rightAnchor, constant: -8).isActive = true
            lblDeliveryWay.rightAnchor.constraint(equalTo: lblDeliveryWayTitle.leftAnchor, constant: -8).isActive = true
            locationIcon.rightAnchor.constraint(equalTo: locationPanel.rightAnchor, constant: 0).isActive = true
            lblLocation.rightAnchor.constraint(equalTo: locationIcon.leftAnchor, constant: 0).isActive = true
            lblLocation.leftAnchor.constraint(equalTo: locationPanel.leftAnchor, constant: 0).isActive = true
            lblBuildingTitle.rightAnchor.constraint(equalTo: footerPanel.rightAnchor, constant: -8).isActive = true
            lblBuilding.rightAnchor.constraint(equalTo: lblBuildingTitle.leftAnchor, constant: -8).isActive = true
            lblFloorTitle.rightAnchor.constraint(equalTo: footerPanel.rightAnchor, constant: -8).isActive = true
            lblFloor.rightAnchor.constraint(equalTo: lblFloorTitle.leftAnchor, constant: -8).isActive = true
            //bottom
            lblAddOrderState.rightAnchor.constraint(equalTo: bottomPanel.rightAnchor, constant: -8).isActive = true
            lblAddOrderStateStatus.rightAnchor.constraint(equalTo: lblAddOrderState.leftAnchor, constant: -8).isActive = true
            btnEditOrderState.leftAnchor.constraint(equalTo: bottomPanel.leftAnchor, constant: 8).isActive = true
            
        } else {
            lblLocation.textAlignment = .left
            //header
            lblOrderState.leftAnchor.constraint(equalTo: headerPanel.leftAnchor, constant: 8).isActive = true
            lblTimestamp.rightAnchor.constraint(equalTo: headerPanel.rightAnchor, constant: 0).isActive = true
            userIcon.leftAnchor.constraint(equalTo: headerPanel.leftAnchor, constant: 8).isActive = true
            lblUsername.leftAnchor.constraint(equalTo: userIcon.rightAnchor, constant: 8).isActive = true
            lblEnterdPhone.rightAnchor.constraint(equalTo: headerPanel.rightAnchor, constant: 0).isActive = true
            //footer
            lblDeliveryWayTitle.leftAnchor.constraint(equalTo: footerPanel.leftAnchor, constant: 8).isActive = true
            lblDeliveryWay.leftAnchor.constraint(equalTo: lblDeliveryWayTitle.rightAnchor, constant: 8).isActive = true
            locationIcon.leftAnchor.constraint(equalTo: locationPanel.leftAnchor, constant: 0).isActive = true
            lblLocation.leftAnchor.constraint(equalTo: locationIcon.rightAnchor, constant: 0).isActive = true
            lblLocation.rightAnchor.constraint(equalTo: locationPanel.rightAnchor, constant: 0).isActive = true
            lblBuildingTitle.leftAnchor.constraint(equalTo: footerPanel.leftAnchor, constant: 8).isActive = true
            lblBuilding.leftAnchor.constraint(equalTo: lblBuildingTitle.rightAnchor, constant: 8).isActive = true
            lblFloorTitle.leftAnchor.constraint(equalTo: footerPanel.leftAnchor, constant: 8).isActive = true
            lblFloor.leftAnchor.constraint(equalTo: lblFloorTitle.rightAnchor, constant: 8).isActive = true
            //bottom
            lblAddOrderState.leftAnchor.constraint(equalTo: bottomPanel.leftAnchor, constant: 8).isActive = true
            lblAddOrderStateStatus.leftAnchor.constraint(equalTo: lblAddOrderState.rightAnchor, constant: 8).isActive = true
            btnEditOrderState.rightAnchor.constraint(equalTo: bottomPanel.rightAnchor, constant: -8).isActive = true
        }
    }
    
    

    
}



// MARK:- Table view delegate & data source functions

extension OrderCell: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifiers.NotificationProduct) as? NotificationProductCell else { return UITableViewCell() }
        cell.product = dataSource[indexPath.row]
        return cell
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
}










































