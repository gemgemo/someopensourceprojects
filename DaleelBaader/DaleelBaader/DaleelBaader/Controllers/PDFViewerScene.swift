
import UIKit

import Localize_Swift

final class PDFViewerScene: UIViewController
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    internal var link = string.empty
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var pdfViewer: UIWebView!
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        configureNavBar()
        setupWebView()
    }
    
    //MARK: - Actions
    
    @objc fileprivate func dismissOnCLick(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
}




extension PDFViewerScene
{
    
    fileprivate func configureNavBar()-> void {
        navigationItem.title = string.empty
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_close"), style: .plain, target: self, action: #selector(dismissOnCLick(_:)))
    }
    
    fileprivate func setupWebView()-> void {
        if (!link.isEmpty) {
            if let url = URL(string: link) {
                pdfViewer.delegate = self
                pdfViewer.loadRequest(URLRequest(url: url))
            }
        }
    }
    
}


extension PDFViewerScene: UIWebViewDelegate
{
    
    internal func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.showIndicator(by: true)
    }
    
    internal func webViewDidFinishLoad(_ webView: UIWebView) {
        UIApplication.showIndicator(by: false)
    }
    
    internal func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        UIApplication.showIndicator(by: false)
    }
    
}














