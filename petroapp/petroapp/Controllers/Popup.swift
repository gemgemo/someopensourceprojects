
import UIKit
import Gemo
import Alamofire

class Popup: BaseController {
    
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var items = Array<Item>(), didSelect: ((int)->())?
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var tblItems: UITableView! { didSet {
        tblItems.delegate = self
        tblItems.dataSource = self
        }}
    
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        //super.updateUi()
        view.superview?.setCorner(radius: 4)
        view.superview?.setShadow(with: .zero, radius: 1.5, opacity: 1.0, color: .black)
        tblItems.reloadData()
    }
    
    
    // MARK: - Actions

    
}

// MARK: - Table views delegate & data source functions

extension Popup: UITableViewDelegate, UITableViewDataSource {
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifiers.Popup, for: indexPath) as! PopupCell
        cell.item = items[indexPath.row]
        return cell
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print("selected cell: \(indexPath)")
        didSelect?(indexPath.row)
        dismiss(animated: true)
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45.0
    }
    
}























