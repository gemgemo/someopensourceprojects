
import Foundation
import Localize_Swift

final class InfoImage
{
    
    internal var id: int?, title: string?, icon: UIImage?
    
    init(id: int, title: string, icon: UIImage) {
        self.id = id
        self.title = title
        self.icon = icon
    }
    
    
    internal class func getData()-> [InfoImage] {
        var items = [InfoImage]()
        items.append(InfoImage(id: 1, title: "info1imagelan".localized(), icon: #imageLiteral(resourceName: "info2")))
        items.append(InfoImage(id: 2, title: "info2imagelan".localized(), icon: #imageLiteral(resourceName: "infp1")))
        return items
    }
    
}


/*
 "info1imagelan" = "Ground Floor services for people with special needs as Haram al-Makki";
 "info2imagelan" = "The first round for the disabled Haram Makki services";
 */





















