
import Foundation


final class Keeper
{
    
    internal var id, name, rank, details, school: string?
    
    
    init(dictionary: [string: any]) {
        id = dictionary["id"] as? string
        name = dictionary["name"] as? string
        rank = dictionary["rank"] as? string
        details = dictionary["details"] as? string
        school = dictionary["school"] as? string
        
    }
    
    
    internal class func getData(from link: string, page: UIViewController?, onComplete: @escaping([Keeper])-> void )-> void {
        UIApplication.showIndicator(by: true)
        HttpClient.request(link: link, method: .get) { (result, response, error) -> void in
            if (error != nil) {
                (page as? BaseController)?.showAlert(with: string.empty, and: Constants.Misc.ErrorMessage)
                return
            }
            
            if let data = result as? [[string: any]] {
                onComplete(data.map { Keeper( dictionary: $0)})
            } else {
                print("null data")
            }
        }
    }
    
    
    
}
