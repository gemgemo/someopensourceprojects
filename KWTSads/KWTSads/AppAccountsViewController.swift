
import UIKit
import SwiftSpinner


class AppAccountsViewController: UIViewController
{
    
    var facebookUrl: String!
    var twitterUrl: String!
    var instagramUrl: String!
    var whatsAppUrl: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        if Reachability.isConnectedToNetwork() == true {
            self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "ac_back-arrow")
            self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "ac_back-arrow")
            self.navigationController?.navigationBar.tintColor = UIColor(red: 0.96, green: 0.69, blue:  0.25, alpha: 1 )
            
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
            
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage
            loadSocail()
        }
        else
        {
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage
            
            let err = "Internet connection FAILED"
            self.displayAlertMessage(err)
            
        }
    }
    func loadSocail()
    {
        SwiftSpinner.show("Loading...")

        let urlString = "\(baseUrl)social.php"
        
        let session = URLSession.shared
        let url = URL(string: urlString)!
        session.dataTask(with: url, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
                if let responseData = data {
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments)as! [String:AnyObject]
                        
                        let data = json["data"] as! NSArray
                        for i in 0..<data.count
                        {
                            if let dict = data[i] as? NSDictionary {
                                
                                self.facebookUrl = dict["facebook"] as! String
                                self.twitterUrl = dict["twitter"] as! String
                                self.instagramUrl = dict["instgram"] as! String
                                self.whatsAppUrl = dict["youtube"] as! String
                            }
                        
                        }
                        DispatchQueue.main.async {
                            
                            SwiftSpinner.hide()
                            
                        }
                    
                    }
                    catch
                    {
                        print ("Could not Serialize")
                    }
                }
                
        }).resume()
        
    }
    func displayAlertMessage(_ userMessage:String)
    {
        let myAlert = UIAlertController(title: "Alert", message:userMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func departTapped(_ sender: AnyObject) {
        //        dismissViewControllerAnimated(true, completion: nil)
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "departmentPage") as! DepartmentsViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
        
    }
    
    
       
    @IBAction func MenuTapped(_ sender: AnyObject) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuPage") as! MenuPageViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dailyNew(_ sender: AnyObject) {
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "DailyNews") as! DailyNewsViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func favPage(_ sender: AnyObject) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "FavPage") as! FavPageViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
        
    }
    @IBAction func faceBookTapped(_ sender: AnyObject) {
        
     UIApplication.shared.open(NSURL(string:"\(self.facebookUrl!)") as! URL, options: [:], completionHandler: nil)

        

    }
    
    @IBAction func twitterTapped(_ sender: AnyObject) {
        
        UIApplication.shared.open(NSURL(string:"\(self.twitterUrl!)") as! URL, options: [:], completionHandler: nil)
        

    }
    
    @IBAction func instagramTapped(_ sender: AnyObject) {
       
        UIApplication.shared.open(NSURL(string:"\(self.instagramUrl!)") as! URL, options: [:], completionHandler: nil)

        

    }
    
    @IBAction func whatsAppTapped(_ sender: AnyObject) {
        
        UIApplication.shared.open(NSURL(string:"\(self.whatsAppUrl!)") as! URL, options: [:], completionHandler: nil)
        

    }
}

