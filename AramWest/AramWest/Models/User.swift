
import UIKit
import Alamofire

final class User: NSObject
{
    
    internal class var instance: User {
        struct Object {
            internal static let object = User()
        }
        return Object.object
    }
    
    internal var id, created, stop, email, password, phone, forign, userName, activationKey, type, fullName, avatar, lastLogin, active, latitude, longitude, deviceType, deviceToken: string?
    
    internal override var description: String {
        return "\n id: \(id), email: \(email), fullName: \(fullName), userName: \(userName) \n"
    }
    
    init(dictionary: [string: any]) {
        id = dictionary["ID"] as? string
        created = dictionary["createdatetime"] as? string
        stop = dictionary["stop"] as? string
        email = dictionary["email"] as? string
        password = dictionary["password"] as? string
        phone = dictionary["phone"] as? string
        forign = dictionary["forign"] as? string
        userName = dictionary["username"] as? string
        activationKey = dictionary["activetionkey"] as? string
        type = dictionary["type"] as? string
        fullName = dictionary["fullname"] as? string
        avatar = dictionary["img"] as? string
        lastLogin = dictionary["lastlogin"] as? string
        active = dictionary["active"] as? string
        latitude = dictionary["lat"] as? string
        longitude = dictionary["lng"] as? string
        deviceType = dictionary["devicetype"] as? string
        deviceToken = dictionary["devicetoken"] as? string
    }
    
    override init() {
        super.init()
    }
    
    internal func getData(by id: string, page: BaseController?, complationHandler: @escaping(bool, User)-> void)-> void {
        UIApplication.showIndicator(by: true)
        Alamofire.request(Constants.BackendURLs.UserInfo, method: .post, parameters: ["ClientID": id])
            .responseJSON { (response) in
                let result = response.result
                if (result.error != nil) {
                    UIApplication.showIndicator(by: false)
                    page?.showAlert(with: string.empty, and: "erroralertlan".localized())
                    return
                }
                //print(result.value)
                if let value = result.value as? [string: any], let data = value["Data"] as? [string: any], let success = value["Success"] as? string {
                    complationHandler((success == "True"), User(dictionary: data))
                } else {
                    print("none data when get user info------->>>>>")
                    UIApplication.showIndicator(by: false)
                    page?.showAlert(with: string.empty, and: "erroralertlan".localized())
                    page?.stopSpin()
                }
        }
    }
    
    
    internal func updateData(with parameters: [string: string], page: BaseController?, complationHanler: @escaping(bool)-> void)-> void {
        UIApplication.showIndicator(by: true)
        Alamofire.request(Constants.BackendURLs.UpdateUserInfo, method: .post, parameters: parameters)
            .responseJSON { (response) in
                let result = response.result
                if (result.error != nil) {
                    print("update user info error is: \(result.error)")
                    page?.stopSpin()
                    page?.showAlert(with: string.empty, and: "updateprofilemessageerrorlan".localized())
                    return
                }
                print("update user data value: \(result.value)")
                if let value = result.value as? [string: any], let success = value["Success"] as? string {
                    complationHanler(success == "True")
                } else {
                    print("nix data \(#function)")
                    page?.stopSpin()
                    page?.showAlert(with: string.empty, and: "updateprofilemessageerrorlan".localized())
                }
        }
    }
    

}






















