
import Foundation
import Gemo


final class SliderCell: BaseCollectionCell
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var item: Slider? { didSet { updateUi() } }
    
    
    // MARK: - Outlets
    
    @IBOutlet private weak var mainPanel: UIView!
    @IBOutlet private weak var cover: UIImageView!
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        if let link = item?.imageLink {
            cover.loadImage(from: link, nil)
        }
    }
    
    
    // MARK: - Actions

    
}


final class Slider
{
    internal var imageLink: string?
    
    init(imageLink: string?) {
        self.imageLink = imageLink
    }
}










































