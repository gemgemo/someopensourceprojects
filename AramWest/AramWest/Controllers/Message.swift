
import UIKit
import Localize_Swift
import Alamofire

final class Message: BaseController
{
    
    // MARK: - Constants
    
    // MARK: - Variables
    internal var id, whoNum, phone, priceType, cId, name, carTypeA, carTypeE, carId, carColor, price, avatar: string?    
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var icon: UIImageView! {
        didSet {
            icon.rounded()
        }
    }
    @IBOutlet fileprivate weak var lblDelegateName: UILabel!
    @IBOutlet fileprivate weak var lblCarType: UILabel!
    @IBOutlet fileprivate weak var lblCarId: UILabel!
    @IBOutlet fileprivate weak var lblCarColor: UILabel!
    @IBOutlet fileprivate weak var lblPriceTitle: UILabel!
    @IBOutlet fileprivate weak var lblPrice: UILabel!
    @IBOutlet fileprivate weak var btnAccept: Button!
    @IBOutlet fileprivate weak var btnReject: Button!
    @IBOutlet fileprivate weak var lblSubtitle: UILabel!
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.superview?.layer.cornerRadius = 0.0
        updateUi()
    }
    
    internal override func updateNavgationBar() {
        navigationController?.navigationBar.isHidden = true
    }
    
    internal override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
    }
    
    // MARK: - Actions
    
    @IBAction private func acceptOnClick(_ sender: UIButton) {
        //print(#function)
        startSpin()
        guard let callID = cId, let clientID = UserDefaults.standard.value(forKey: Constants.Misc.UserId), let dID = id else {
            stopSpin()
            showAlert(with: .empty, and: "reqiurefieldlan".localized())
            return
        }
        let params: [string: any] = [
            "CallID": callID,
            "ClientID": clientID,
            "DriverID": dID
        ]
        Alamofire.request(Constants.BackendURLs.AcceptDelegate, method: .post, parameters: params)
            .responseJSON { [weak self] (response) in
                let result = response.result
                if (result.error != nil) {
                    self?.stopSpin()
                    print("accept delegate error \(result.error)")
                    self?.showAlert(with: .empty, and: "erroralertlan".localized())
                    return
                }
                
                if let value = result.value {
                    print(value)
                    self?.stopSpin()
                    self?.dismiss(animated: true, completion: nil)
                } else {
                    self?.stopSpin()
                    print("nix accept delegate value")
                    self?.showAlert(with: .empty, and: "erroralertlan".localized())
                }
        }
    
    }
    
    @IBAction private func declineOnClick(_ sender: UIButton) {
        //print(#function)
        startSpin()
        guard let callID = cId, let clientId = UserDefaults.standard.value(forKey: Constants.Misc.UserId), let dID = id, let salaryT = priceType else {
            stopSpin()
            showAlert(with: .empty, and: "reqiurefieldlan".localized())
            return
        }
        let params: [string: any] = [
            "CallID": callID,
            "ClientID": clientId,
            "DriverID": dID,
            "SalaryType": salaryT
        ]
        
        Alamofire.request(Constants.BackendURLs.DeclineDelegate, method: .post, parameters: params)
            .responseJSON { [weak self] (response) in
                let result = response.result
                if (result.error != nil) {
                    print("decline driver error is \(result.error)")
                    self?.stopSpin()
                    self?.showAlert(with: .empty, and: "erroralertlan".localized())
                    return
                }
                if let value = result.value {
                    print(value)
                    self?.stopSpin()
                    self?.dismiss(animated: true, completion: nil)
                } else {
                    self?.stopSpin()
                    self?.showAlert(with: .empty, and: "erroralertlan".localized())
                    print("decline nil data")
                }
        }
    }
    
}


// MARK: - Helper Functions

extension Message
{
    
    fileprivate func updateUi()-> void {
        lblTitle.text = "messagetitlelan".localized()
        lblSubtitle.text = "messagesubtitlelan".localized()
        lblPriceTitle.text = "messagejobpricetitlelan".localized()
        btnAccept.setTitle("messageacceptbuttnlan".localized(), for: .normal)
        btnReject.setTitle("messagedeclinebuttonlan".localized(), for: .normal)
        lblDelegateName.text = name ?? .empty
        lblCarType.text = ((Localize.currentLanguage() == Constants.Language.Arabic) ? carTypeA : carTypeE) ?? .empty
        lblCarId.text = carId ?? .empty
        lblCarColor.text = carColor ?? .empty
        lblPrice.text = price ?? .empty
        if let iconLink = avatar {
            icon.loadImage(from: iconLink, onComplete: nil)
        }
    }
    
    
}























