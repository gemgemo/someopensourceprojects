
import UIKit

class ImageDetial: UICollectionViewCell
{
    internal var imageLink: String? {
        didSet {
            updateUi()
        }
    }
    
    
    @IBOutlet private weak var image: UIImageView!
    
    
    
    private func updateUi()-> Void {
        guard let link = imageLink else { return }
        image.loadImage(from: link, onComplete: nil)
    }
    
    
    
    
    
}
