
import Foundation

import Localize_Swift
import UIKit


final class ContactUs
{
    
    private static let session = URLSession.shared
    
    internal var result, phone, phone2, email: string?
    
    init(result: string, phone: string, phone2: string, email: string) {
        self.result = result
        self.phone = phone
        self.phone2 = phone2
        self.email = email
    }
    
    init(dictionary: [string: any]) {
        result = dictionary["result"] as? string
        phone = dictionary["phone"] as? string
        phone2 = dictionary["phone 2"] as? string
        email = dictionary["email"] as? string
    }
    
    // Functions
    
    internal class func getData(viewController: UIViewController?, onComplete: @escaping (ContactUs)-> void)-> void {
        UIApplication.showIndicator(by: true)
        let link = Constants.BackendURLs.CallUsGet
        if (link.isEmpty || !link.isLink) {
            viewController?.dialog(with: Constants.Misc.ErrorMessage)
            print("link error")
            UIApplication.showIndicator(by: false)
            return
        }
        
        guard let urlRequest = URL(string: link) else {
            viewController?.dialog(with: Constants.Misc.ErrorMessage)
            print("url request error")
            UIApplication.showIndicator(by: false)
            return
        }
        
        session.dataTask(with: URLRequest(url: urlRequest)) { (data, response, error) in
            if (error != nil) {
                viewController?.handle(this: error)
                print("request error")
                UIApplication.showIndicator(by: false)
                return
            }
            
            guard let jsonData = data else {
                viewController?.dialog(with: Constants.Misc.ErrorMessage)
                print("none data error")
                UIApplication.showIndicator(by: false)
                return
            }
            
            //print(string(data: jsonData, encoding: .utf8))
            do {
                if let json = (try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [string: any])?["Contact Us"] as? [[string: any]] {
                     if let dic = json.first {
                        onComplete(ContactUs(dictionary: dic))
                    }
                }
            } catch {
                viewController?.handle(this: error)
                print("catch error")
                UIApplication.showIndicator(by: false)
            }
            
            }.resume()
        
    }
    
    internal class func postData(parameters: [string: any], viewController: UIViewController?, onComplete: @escaping(_ message: string)-> void)-> void {
        UIApplication.showIndicator(by: true)
        let link = Constants.BackendURLs.CallUsPost
        if (link.isEmpty || !link.isLink) {
            viewController?.dialog(with: Constants.Misc.ErrorMessage)
            print("link error")
            UIApplication.showIndicator(by: false)
            return
        }
        guard let urlRequest = URL(string: link) else {
            viewController?.dialog(with: Constants.Misc.ErrorMessage)
            print("url request error")
            UIApplication.showIndicator(by: false)
            return
        }
        
        var request = URLRequest(url: urlRequest)
        request.httpMethod = Constants.HttpMethod.Post
        var params = string.empty
        parameters.forEach { (key, value) in
            params += "\(key)=\(value)&"
        }
        
        params.remove(at: params.index(before: params.endIndex))
        request.httpBody = params.data(using: .utf8)
        session.dataTask(with: request) { (data, response, error) in
            if (error != nil) {
                viewController?.handle(this: error)
                UIApplication.showIndicator(by: false)
                print("error in response")
                return
            }
            
            guard let jsonData = data else {
                viewController?.dialog(with: Constants.Misc.ErrorMessage)
                UIApplication.showIndicator(by: false)
                print("nix data error")
                return
            }
            
            //print(string(data: jsonData, encoding: .utf8))
            do {
                if let json = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [[string: any]] {
                    guard let message = (Localize.currentLanguage() == Constants.Misc.ArabicSign) ? json.first?["data"] as? string : json.first?["data_en"]as? string else {
                        print("null message")
                        return
                    }
                    //viewController?.dialog(with: message)
                    onComplete(message)
                } else {
                    print("null json")
                }
            } catch let caErr{
                viewController?.handle(this: caErr)
                UIApplication.showIndicator(by: false)
                //print(caErr, error)
                print("catch error")
            }
            
            }.resume()
    }
    
}

