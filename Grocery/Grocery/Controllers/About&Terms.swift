
import UIKit
import Gemo

final class About_Terms: BaseController {
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var isTerms = true
    
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var txtDetails: UITextView! { didSet {
        txtDetails.text = .empty
        txtDetails.alwaysBounceVertical = true
        } }
    
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        super.updateUi()
        tabBar.isHidden = true
        txtDetails.textAlignment = (Localizer.instance.current == .arabic) ? .right : .left
        if (isTerms) {
            navigationItem.title = Localization.TermsOfUse.localized()
            fetchDetails(by: Urls.Terms)
        } else {
            navigationItem.title = Localization.AboutApp.localized()
            fetchDetails(by: Urls.About)
        }

    }
    
    
    // MARK: - Actions

}


// MARK: - Helper functions

extension About_Terms
{
    
    
    fileprivate func fetchDetails(by link: string)-> void {
        if (Gemo.gem.isConnected) {
            AboutATerms.getData(from: link) { [weak self] (item) in
                Threads.main { [weak self] in
                    self?.txtDetails.text = item.content ?? .empty
                }
            }
        } else {
            dialogUserWithNoInternetConnectionMessage()
        }
    }
    
    
}
























