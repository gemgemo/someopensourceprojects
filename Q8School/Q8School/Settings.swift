//
//  Settings.swift
//  Q8School
//
//  Created by mac on 12/8/16.
//  Copyright © 2016 Atiaf. All rights reserved.
//

import Foundation
import UIKit

public class Settings{
    
    // Web service links
    static var GetAllAds = "http://192.232.214.91/~q8school/q8school/WS/Api/slider"
    static var SignUp = "http://192.232.214.91/~q8school/q8school/WS/Api/signUp"
    static var SignIn = "http://192.232.214.91/~q8school/q8school/WS/Api/signIn"
    static var MainPhase = "http://192.232.214.91/~q8school/q8school/WS/Api/viewScholarPhase"
    static var Subjects = "http://192.232.214.91/~q8school/q8school/WS/Api/viewSubjects"
    static var Teachers = "http://192.232.214.91/~q8school/q8school/WS/Api/getAllAds"
    static var TeacherDetials = "http://192.232.214.91/~q8school/q8school/WS/Api/getAdDetails"
    static var AllMassages = "http://192.232.214.91/~q8school/q8school/WS/Api/showAllMsgs"
    static var ShowMassags = "http://192.232.214.91/~q8school/q8school/WS/Api/showMsg"
    static var SendMessages = "http://192.232.214.91/~q8school/q8school/WS/Api/sendMsg"
    static var AboutUs = "http://192.232.214.91/~q8school/q8school/WS/Api/showAbout"
    static var EditProfile = "http://192.232.214.91/~q8school/q8school/WS/Api/edit_data"
    static var ContactUs = "http://192.232.214.91/~q8school/q8school/WS/Api/contactUs"
    static var AddADs = "http://192.232.214.91/~q8school/q8school/WS/Api/addAds"
    static var MyADs = "http://192.232.214.91/~q8school/q8school/WS/Api/getMyAds"
    static var MyFavorite = "http://192.232.214.91/~q8school/q8school/WS/Api/getFromFav"
    static var ForgetPassword = "http://192.232.214.91/~q8school/q8school/WS/Api/recover_pass"
    static var LikeUnlike = "http://192.232.214.91/~q8school/q8school/WS/Api/like_unlike"
    static var ReportAds = "http://192.232.214.91/~q8school/q8school/WS/Api/reportAds"
    static var Notifcation = "http://192.232.214.91/~q8school/q8school/WS/Api/device_type_token"
    static var ActivationCode = "http://192.232.214.91/~q8school/q8school/WS/Api/checkVerificationCode"
    static var DeleteMsg = "http://192.232.214.91/~q8school/q8school/WS/Api/deleteMsg"
    static var ResendCode = "http://192.232.214.91/~q8school/q8school/WS/Api/sendNewVerificationCode",
    FoundAds = "http://192.232.214.91/~q8school/q8school/WS/Api/checkSlider",
    Block = "http://192.232.214.91/~q8school/q8school/WS/Api/blockStudent",
    ConfirmSignIn = "http://192.232.214.91/~q8school/q8school/WS/Api/confirmLogin"

    
    // Language settings
    
    static var lang = "ar"
    
    
    // Check for internet
}

























