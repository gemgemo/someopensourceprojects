
import UIKit
import Localize_Swift

final class AwardVideosCell: UITableViewCell, CellProvider
{
    // MARK: - Constants
    
    //MARK: - Variables
    
    internal var video: Video? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var poster: UIImageView!
    @IBOutlet internal weak var lblTitle: UILabel!
    @IBOutlet private weak var mainPanal: UIView!
    
    
    // MARK: - Overridden functions
    
    internal func updateUI() {
        lblTitle.text = video?.title ?? string.empty
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            mainPanal.semanticContentAttribute = .forceLeftToRight
            lblTitle.textAlignment = .right
        } else {
            mainPanal.semanticContentAttribute = .forceRightToLeft
            lblTitle.textAlignment = .left
        }
        if let videoLink = video?.link {
            //print("link", videoLink)
            if (videoLink.contains("https://www.youtube.com")) {
                let youtubeVidId = videoLink.components(separatedBy: "=").last ?? string.empty
                //print("video id", youtubeVidId)
                poster.loadImage(from:"https://img.youtube.com/vi/\(youtubeVidId)/0.jpg", onComplete: nil)
            }
        }
    }
    
    //MARK: - Actions
    
}
