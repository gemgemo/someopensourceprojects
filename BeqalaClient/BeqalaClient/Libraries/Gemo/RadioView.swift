
import Foundation
import UIKit

@IBDesignable
final class RadioView: UIView
{
    
    @IBInspectable
    internal var isSelected: Bool = false { didSet { setNeedsDisplay() } }
    
    @IBInspectable
    internal var borderColor: UIColor = UIColor.darkGray { didSet { setNeedsDisplay() } }
    
    @IBInspectable
    internal var selectedColor: UIColor = UIColor.blue { didSet { setNeedsDisplay() } }
    
    
    private var markView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    private var mainPanel: UIView = {
        let panel = UIView()
        panel.translatesAutoresizingMaskIntoConstraints = false
        panel.backgroundColor = .white
        return panel
    }()
    
    
    internal override func draw(_ rect: CGRect) {
        configure()
    }
    
    private func configure()-> void {
        mainPanel.layer.cornerRadius = bounds.height/2
        mainPanel.layer.borderWidth = 2.0
        mainPanel.layer.masksToBounds = true
        backgroundColor = .clear
        addSubview(mainPanel)
        mainPanel.topAnchor.constraint(equalTo: topAnchor).isActive = true
        mainPanel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        mainPanel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        mainPanel.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        //layer.cornerRadius = bounds.height/2
        //layer.borderWidth = 2.0
        if (isSelected) {
            markView.backgroundColor = selectedColor
            mainPanel.layer.borderColor = selectedColor.cgColor
            mainPanel.addSubview(markView)
            // constraints
            markView.heightAnchor.constraint(equalToConstant: bounds.height/2).isActive = true
            markView.widthAnchor.constraint(equalToConstant: bounds.width/2).isActive = true
            markView.centerXAnchor.constraint(equalTo: mainPanel.centerXAnchor, constant: 0).isActive = true
            markView.centerYAnchor.constraint(equalTo: mainPanel.centerYAnchor, constant: 0).isActive = true
            markView.layer.cornerRadius = (bounds.height/2)/2
            markView.clipsToBounds = true
        } else {
            mainPanel.layer.borderColor = borderColor.cgColor
            markView.removeFromSuperview()
        }
    }
    
    
}








































