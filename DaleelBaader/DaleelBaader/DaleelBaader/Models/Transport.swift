

import Foundation

import Localize_Swift


final class Transport
{
    
    
    internal var arabicName, englishName, arabicInfo, englishInfo, link, iconName: string?
    
    init(nameAr: string, nameEn: string, infoAr: string, infoEn: string, link: string, icon: string) {
        self.arabicName = nameAr
        self.englishName = nameEn
        self.arabicInfo = infoAr
        self.englishInfo = infoEn
        self.link = link
        self.iconName = icon
    }
    
    
    internal class func loadData()-> [Transport] {
        var trans = Array<Transport>()
        trans.append(Transport(nameAr: "اوبر", nameEn: "Uber", infoAr: "احصل علي مشوار موثوق في دقائق مع تطبيق أوبر", infoEn: "Get reliable journey in minutes with uber", link: "https://itunes.apple.com/kw/app/uber/id368677368?mt=8", icon: "u"))
        trans.append(Transport(nameAr: "كريم", nameEn: "Careem", infoAr: "الان يمكنك حجز سياره اونلاين بإستخدام كريم", infoEn: "Now you can reserve a car online using cream", link: "https://itunes.apple.com/kw/app/careem-krym-online-cab-booking/id592978487?mt=8", icon: "k"))
        trans.append(Transport(nameAr: "سابتكو", nameEn: "Saptco", infoAr: "سابتكو شركه نقل عام سعوديه تقدم خدمات اونلاين جديده", infoEn: "Saudi Public Transport Company \"SAPTCO\" offers new online services", link: "https://itunes.apple.com/kw/app/saptco/id1074204412?mt=8", icon: "s"))
        return trans
    }
    
    
}
