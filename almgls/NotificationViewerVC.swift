

import UIKit

final class NotificationViewerVC: UIViewController
{
    
    // MARK: - Constants
    
    // MARK: - Variables
    
    internal var text = ""
    
    // MARK: - Outlets
    
    @IBOutlet private weak var textView: UITextView!
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        textView.text = text
    }
    
    // MARK: - Actions

}
