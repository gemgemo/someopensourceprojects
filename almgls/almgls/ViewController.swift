
import UIKit

let POPOVER_FUNC_NAME = "COM.POP.FUN.NAME"

final class ViewController: UIViewController
{
    
    
    @IBOutlet weak var popOverBtn: UIButton!
    @IBOutlet private weak var vistorViewBottom: NSLayoutConstraint! {
        didSet {
            vistorViewBottom.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? 100.0 : 37.0
        }
    }
    
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(showPopover), name: Notification.Name(POPOVER_FUNC_NAME), object: nil)
    }
    
    internal override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch (segue.identifier ?? "") {
        case "showPopOver":
             let popOverVC = segue.destination as! NotificationViewerVC
             popOverVC.text = notificationText
             popOverVC.popoverPresentationController?.permittedArrowDirections = .init(rawValue: 0)
             popOverVC.preferredContentSize = CGSize(width: 300, height: 300)
             popOverVC.popoverPresentationController?.delegate = self
        default:
            break
        }
        
    }
    
    @objc private func showPopover()-> Void {
         self.popOverBtn.sendActions(for: .touchUpInside)
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    @IBOutlet weak var loadingLbl: UILabel!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var visitorLbl: UILabel!
    @IBOutlet weak var numberVisitor: UILabel!
    @IBOutlet weak var visitorView: UIView!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var callUsLbl: UILabel!
    @IBOutlet weak var proposalLbl: UILabel!
    @IBOutlet weak var complaintLbl: UILabel!
    @IBOutlet weak var aboutUsLbl: UILabel!
    @IBOutlet weak var imageLbl: UILabel!
    @IBOutlet weak var memberLbl: UILabel!
    @IBOutlet weak var newsLbl: UILabel!
    var count: String!
    let defaults = UserDefaults.standard
    var flag = false
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        shadowView.isHidden = true
        loadingView.isHidden = true
        if Settings.lang == "ar" {
            visitorLbl.text = "عدد زوار"
            visitorView.semanticContentAttribute = .forceRightToLeft
            buttonView.semanticContentAttribute = .forceRightToLeft
            callUsLbl.text = "اتصل بنا"
            proposalLbl.text = "إرسال مقترح"
            complaintLbl.text = "إرسال شكوى"
            aboutUsLbl.text = "من نحن"
            imageLbl.text = "الصور"
            memberLbl.text = "أعضاء المجلس"
            newsLbl.text = "الأخبار"
            loadingLbl.text = "جاري التحميل..."

        }
        if (Settings.isConnected) {
            if UserDefaults.standard.bool(forKey: "bool") == false {
                AppVisitor()
                CurrentVisitor()
                flag = true
                self.defaults.setValue(flag, forKey: "bool")
                print("here1")
                
            } else {
                CurrentVisitor()
                print("here2")
                
            }
        } else {
            dialogUser(with: "لا يوجد انترنت")
        }
      
        

    }
    
    
    func CurrentVisitor() {
        loadingView.isHidden = false
        shadowView.isHidden = false
        loadingView.layer.cornerRadius = 10
        loadingView.layer.masksToBounds = true
        loadingActivity.startAnimating()
        let urlString = Settings.CurrentVisitor
        let sessions = URLSession.shared
        let urls = URL(string: urlString)!
        sessions.dataTask(with: urls, completionHandler:
            {[weak self] (data :Data?, response :URLResponse?, error:Error?) in
                
                if let responseData = data {
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [NSDictionary]
                        //print("json1\(json)")
                        for i in json
                        {
                            self?.count = i["visitor"] as! String
                            
                        }
                        
                        
                        DispatchQueue.main.async
                            { [weak self] in
                                guard let this = self else { return }
                                //self?.numberVisitor.text = self?.count
                                self?.visitorLbl.text = "عدد الزوار "+this.count
                                self?.loadingView.isHidden = true
                                self?.shadowView.isHidden = true
                                self?.loadingActivity.stopAnimating()
                        }
                    }
                        
                    catch
                    {
                        print ("Could not Serialize")
                    }
                    
                    
                    
                    
                    
                }
                
        }).resume()
        
        
    }
    
    func AppVisitor()
    {
        let urlString = Settings.Visitors
        let sessions = URLSession.shared
        let urls = URL(string: urlString)!
        sessions.dataTask(with: urls, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
                
                if let responseData = data {
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [NSDictionary]
                        //print("json2\(json)")
                        
                        DispatchQueue.main.async
                            {
                        }
                    }
                        
                    catch
                    {
                        print ("Could not Serialize")
                    }
                    
                    
                    
                    
                    
                }
                
        }).resume()
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }


  
    @IBAction func imagesBtn(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ImagesVC") as! ImagesVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func aboutUsBtn(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "com.about.app.id") as! AppInfo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func proposalBtn(_ sender: Any) {
        if UserDefaults.standard.string(forKey: "phone") != nil && UserDefaults.standard.string(forKey: "check") != nil
        
        {

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ComplainVC") as! ComplainVC
            self.navigationController?.pushViewController(vc, animated: true)
            Settings.complainV = "0"
            
        }
        else
        {
    
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PhoneNumberVC") as! PhoneNumberVC
            self.navigationController?.pushViewController(vc, animated: true)
            Settings.complainV = "0"
            
        }
    }

    @IBAction func callUsBtn(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CallUsVC") as! CallUsVC
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func memberBtn(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MembersVC") as! MembersVC
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func newsBtn(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NewsVC") as! NewsVC
        self.navigationController?.pushViewController(vc, animated: true)

        
    }
    
    @IBAction func complaint(_ sender: UIButton) {
        if UserDefaults.standard.string(forKey: "phone") != nil && UserDefaults.standard.string(forKey: "check") != nil {
            print("with no number")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ComplainVC") as! ComplainVC
            self.navigationController?.pushViewController(vc, animated: true)
            Settings.complainV = "1"
        } else {
            print("with number")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PhoneNumberVC") as! PhoneNumberVC
            self.navigationController?.pushViewController(vc, animated: true)
            Settings.complainV = "1"
        }

    }
    
    
}



extension ViewController: UIPopoverPresentationControllerDelegate
{
    
    internal func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    /*internal func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .popover
    }*/
    
}





















