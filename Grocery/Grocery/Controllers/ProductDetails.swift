
import UIKit
import Gemo

final class ProductDetails: BaseController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var product: Product?
    fileprivate var images = Array<Slider>(), isDeleted = false
    
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var sliderCollection: UICollectionView! { didSet {
        sliderCollection.delegate = self
        sliderCollection.dataSource = self
        sliderCollection.showsHorizontalScrollIndicator = false
        }}
    @IBOutlet fileprivate weak var pageControl: UIPageControl! { didSet { pageControl.numberOfPages = 0 }}
    @IBOutlet private weak var headerPanelHeight: NSLayoutConstraint!
    @IBOutlet private weak var footerPanel: View!
    @IBOutlet private weak var lblProductName: UILabel!
    @IBOutlet private weak var lblPrice: UILabel!
    @IBOutlet private weak var lblQuantity: UILabel!
    @IBOutlet private weak var lblDetailsTitle: UILabel!
    @IBOutlet private weak var lblDetails: UILabel!
    @IBOutlet private weak var btnEdit: UIButton!
    @IBOutlet private weak var btnDelete: UIButton!
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        headerPanelHeight.constant = view.frame.height * 0.45
    }
    
    internal override func updateUi() {
        super.updateUi()
        tabBar.isHidden = true
        navigationItem.title = product?.name ?? .empty
        getImages()
        lblDetailsTitle.text = Localization.Description.localized()
        btnEdit.setTitle(Localization.Update.localized(), for: .normal)
        btnDelete.setTitle(Localization.Delete.localized(), for: .normal)
        lblProductName.text = product?.name ?? .empty
        lblPrice.text = "\(Localization.Price.localized()) \(product?.price ?? .empty) \(Localization.SR.localized())"
        lblQuantity.text = "\(Localization.QuantityAvailable.localized()): \(product?.quantity ?? .empty)"
        lblDetails.text = product?.details ?? .empty
        if (Localizer.instance.current == .arabic) {
            lblProductName.textAlignment = .right
            lblPrice.textAlignment = .right
            lblQuantity.textAlignment = .right
            lblDetailsTitle.textAlignment = .right
            lblDetails.textAlignment = .right
            
        } else {
            lblProductName.textAlignment = .left
            lblPrice.textAlignment = .left
            lblQuantity.textAlignment = .left
            lblDetailsTitle.textAlignment = .left
            lblDetails.textAlignment = .left
            
        }
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if (isDeleted) {
            NotificationCenter.default.post(name: .UpdateProducts, object: self)
        }
    }
    
    // MARK: - Actions
    
    @IBAction private func updateProductOnCLick(_ sender: UIButton) {        
        navigateToEditProduct(isNew: false, product: product)
    }
    
    @IBAction private func deleteProductOnClick(_ sender: UIButton) {
        if (Gemo.gem.isConnected) {
            product?.delete { [weak self] in
                Threads.main { [weak self] in
                    self?.isDeleted = true
                    _ = self?.navigationController?.popViewController(animated: true)
                }
            }
        } else {
            dialogUserWithNoInternetConnectionMessage()
        }
    }
    

}

// MARK: - Helper functions

extension ProductDetails
{
    
    fileprivate func getImages()-> void {
        images.removeAll()
        var imagesLinks = Array<string>()
        imagesLinks.append(product?.photo1 ?? .empty)
        imagesLinks.append(product?.photo2 ?? .empty)
        imagesLinks.append(product?.photo3 ?? .empty)
        imagesLinks.append(product?.photo4 ?? .empty)
        for link in imagesLinks where !link.isEmpty {
            images.append(Slider(imageLink: link))
        }
        pageControl.numberOfPages = images.count
        sliderCollection.reloadData()
    }
    
}


// MARK: - collection view delegate & data source functions

extension ProductDetails
{
    internal override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (collectionView == sliderCollection) ? images.count : super.collectionView(collectionView, numberOfItemsInSection: section)
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (collectionView == sliderCollection) {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ReuseIdentifiers.Slider, for: indexPath) as? SliderCell else { return UICollectionViewCell() }
            cell.item = images[indexPath.item]
            return cell
        }
        return super.collectionView(collectionView, cellForItemAt: indexPath)
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView == sliderCollection) {
            print("selected item is: \(indexPath)")
        } else {
            super.collectionView(collectionView, didSelectItemAt: indexPath)
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return (collectionView == sliderCollection) ? 0 : super.collectionView(collectionView, layout: collectionViewLayout, minimumLineSpacingForSectionAt: section)
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return (collectionView == sliderCollection) ? 0 : super.collectionView(collectionView, layout: collectionViewLayout, minimumInteritemSpacingForSectionAt: section)
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return (collectionView == sliderCollection) ? Size(width: collectionView.frame.width, height: collectionView.frame.height) : super.collectionView(collectionView, layout: collectionViewLayout, sizeForItemAt: indexPath)
    }
    
    internal func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let currentPage = targetContentOffset.pointee.x / view.frame.width
        print("current page: \(currentPage)")
        pageControl.currentPage = currentPage.toInt
    }
    
}



























