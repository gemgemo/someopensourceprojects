

import Foundation
import Localize_Swift

struct Constants
{
    
    
    // MARK: - Storyboard
    struct Storyboard {
        internal static let MainNav          = "mainNavid",
                            SlideMenu        = "slidemenuviewid",
                            Main             = "mainviewid",
                            RegisterLoginNav = "login-registernaviid",
                            Login            = "logninViewid",
                            Register         = "registerviewid",
                            Activation       = "activationcodeviewid",
                            Profile          = "profileviewid",
                            Missions         = "com.missions.view",
                            Popover          = "com.popover.view",
                            Notification     = "com.notification.screen",
                            UserLocation     = "com.userlocation.update.id",
                            MissionEndedMessage = "com.view.end.mission.id",
                            Rate            = "com.rate.dialog.view",
                            DriverMap       = "com.driver.map.view.id",
                            DriverOrderInfo = "com.driver.order.info.view",
                            DriverOrderMessage = "com.driver.view.order.popup.id",
                            AboutApp       = "com.about.app.view.id",
                            UpadteUserProfile = "com.update.user.profile.view.id",
                            Trips     = "com.view.trips.id",
                            CallDetails = "com.call.view.id"
    }
    
    // MARK: - Nibs
    
    struct Nib {
        internal static let Menu             = UINib(nibName: "MenuCell", bundle: nil),
                            Selection        = UINib(nibName: "SelectionCell", bundle: nil),
                            TransportType    = UINib(nibName: "TransportTypeCell", bundle: nil),
                            Trip             = UINib(nibName: "TripCell", bundle: nil)
        
    }
    
    // MARK: - Reuse Identifier
    struct ReuseIdentifier {
        internal static let Menu             = "menuitemcellviewid",
                            Popover          = "com.popover.cell.view",
                            Selection        = "com.selection.cell.view",
                            TranspoertType   = "com.transport.type.cell.view",
                            Trip             = "com.trip.cell.view"
    }
    
    
    // MARK: - Misc
    struct Misc {
        internal static let UserLoggedIn          = "com.user.login",
                            UserId                = "com.user.id",
                            SourceAnnotation      = "com.source.annotation.identifer",
                            DestinationAnnotation = "com.destination.annotation.id",
                            Message               = Notification.Name("com.message.view.id"),
                            UserJourney           = Notification.Name("com.user.journey.map.view"),
                            MissionEnded          = Notification.Name("com.mission.ended.open"),
                            RateView              = Notification.Name("com.open.rate.view"),
                            ForgroundNotification = Notification.Name("com.notification.alert.forground"),
                            DriverMessage         = Notification.Name("com.driver.message.notification"),
                            GoogleMapApiKey       = "AIzaSyCbxlEJiQMNY0S9RPTDaEksiYm-iinJ29w"                            
        
        
    }
    
    // MARK: - Localizations
    
    struct Localizer {
        internal static let InternetConnection = "internetconnectionmessagelan",
                            Trips = "tripstitlelan",
                            TripsDay = "trpisdaylan",
                            Error = "erroralertlan",
                            NoData = "nodatalan"
    }
    
    // MARK: - Language
    struct Language {
        internal static let Arabic = "ar-SA", English = "en"
    }
    
    // MARK: - Backend URLs
    struct BackendURLs {
        private static let baseUrl            = "http://192.232.214.91/~aramweast/API/"
        internal static let Register          = "\(baseUrl)Client/SignUp",
                            ActivateKey       = "\(baseUrl)Client/ActivationKey",
                            ResendKey         = "\(baseUrl)Client/ReSendActiveKey",
                            Login             = "\(baseUrl)Client/SignIn",
                            ResetPassword     = "\(baseUrl)Client/ForgetPass",
                            UserInfo          = "\(baseUrl)Client/GetPersonalData",
                            UpdateUserInfo    = "\(baseUrl)Client/UpdateData",
                            MainCars          = "\(baseUrl)Client/GetMainCar",
                            SubCars           = "\(baseUrl)Client/GetSubCar",
                            SendDelegateOrder = "\(baseUrl)Client/MakeCallDelegate",
                            DeviceToken       = "\(baseUrl)Client/UpdateDevice",
                            DeclineDelegate   = "\(baseUrl)Client/RejectDelegate",
                            AcceptDelegate    = "\(baseUrl)Client/AcceptDelegate",
                            Rating            = "\(baseUrl)Client/AddRatingForDelegate",
                            CallDriver        = "\(baseUrl)Client/MakeCallDriver",
                            RejectDriver      = "\(baseUrl)Client/RejectDriver",
                            AcceptDriver      = "\(baseUrl)Client/AcceptDriver",
                            DriverRate        = "\(baseUrl)Client/AddRatingForDriver",
                            AboutData         = "\(baseUrl)Client/GetAboutData",
                            TermsData         = "\(baseUrl)Client/GetPloiceData",
                            CallCompany       = "\(baseUrl)Client/MakeCallForCompany",
                            AcceptCompany     = "\(baseUrl)Client/AcceptCompany",
                            DeclineCompany    = "\(baseUrl)Client/RejectCompany",
                            RateCompany       = "\(baseUrl)Client/AddRatingForCompany",
                            GetCallEnded      = "\(baseUrl)Client/GetCallEnded",
                            GetCallDetails    = "\(baseUrl)Client/GetCallDetails"
    }
    
    
}






/*internal let localizer = Localization()
// MARK: - Localization
class Localization {
    //internal let
    //ReqiuredFields            = "reqiurefieldlan".localized(),
    //Error                     = "erroralertlan".localized(),
    //EmailValidation           = "invalidemaillan".localized(),
    //Successful                = "successproccesslan".localized(),
    //Warning                   = "warningtitlelan".localized(),
    //DefineLocation            = "locationsubtitlelan".localized(),
    //Cancel                    = "cancellan".localized(),
    //OK                        = "okLan".localized(),
    //Failure                   = "failurelan".localized(),
    //NoDelegates               = "noDelegateslan".localized(),
    //OrderSendSuccessfully     = "yourordersendlan".localized(),
    //ChooseDate                = "datepickechoosebuttonlan".localized(),
    //CloseDate                 = "datepickerclosebuttonlan".localized(),
    //Delegate                  = "delegatelan".localized(),
    //Driver                    = "driverlan".localized(),
    //Equipments                = "Rentalequipmentlan".localized(),
    //From                      = "labelfromlan".localized(),
    //To                        = "labeltolan".localized(),
//    MissionDetailsPlaceholder = "missiondetailsplaceholderlan".localized(),
//    PaymentWay                = "paymentwaylabellan".localized(),
//    Send                      = "sendorderbuttonlan".localized(),
    //MessageTitle              = "messagetitlelan".localized(),
//    MessageSubtitle           = "messagesubtitlelan".localized(),
//    JobPrice                  = "messagejobpricetitlelan".localized(),
//    Accept                    = "messageacceptbuttnlan".localized(),
//    Decline                   = "messagedeclinebuttonlan".localized(),
//    UserJourney               = "userlocationmapdetailslan".localized(),
//    EndMessageTitle           = "endedmissionmessagelan".localized(),
//    MissionEned               = "endedmissionmessagebuttonlan".localized(),
//    RateTitle                 = "ratetitlelan".localized(),
//    RateButton                = "ratebuttonlan".localized(),
//    RateSubtitle              = "ratetitlemiddlelan".localized(),
//    NotificationMessage       = "notificationMessagelan".localized(),
//    DriverTranportType = "driverTypeoftransportationlan".localized(),
//    DriverWeight = "driverweiaghtlan".localized(),
//    DriverNumberOf = "drivernumberofplacholderlan".localized(),
//    DriverPricing = "driverPricingwaylan".localized(),
//    FollowUp = "driverfollowupbuttonlan".localized(),
//    OrderInfo = "orderinfolan".localized(),
//    MissionType =  "missiontypelan".localized(),
//    Behemoth = "behemothlan".localized(),
//    Distance = "distancelan".localized(),
//    KiloMeter = "kmlan".localized(),
//    SuccessDriverOrder = "driversucessdialoglan".localized(),
//    ResendOrder = "resendbuttontitlelan".localized(),
//    PriceForMission = "priceformissionlan".localized(),
//    DriverMessageSubtitle = "drivermessagesubtitlelan".localized(),
//    DriverMessageTitle = "drivermessagetitlelan".localized(),
//    AboutApp = "aboutapptitlelan".localized(),
//    Terms   = "termstitlelan".localized(),
//    EditProfile = "editprofileupdatetitlelan".localized()
    /*
     
     */
}
*/






















