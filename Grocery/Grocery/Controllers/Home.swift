
import UIKit
import Gemo

final class Home: BaseController
{

    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var dataSource = Array<Product>() { didSet {
        Threads.main { [weak self] in
            self?.productsCollection.reloadData()
        }}}
    
    
    // MARK: - Outlets
    
    @IBOutlet private weak var cover: UIImageView!
    @IBOutlet private weak var icon: UIImageView!
    @IBOutlet private weak var headerHeight: NSLayoutConstraint!
    @IBOutlet private weak var iconHeight: NSLayoutConstraint!
    @IBOutlet private weak var iconWidth: NSLayoutConstraint!
    @IBOutlet private weak var headerPanel: UIView!
    @IBOutlet fileprivate weak var productsCollection: UICollectionView! { didSet {
        productsCollection.delegate = self
        productsCollection.dataSource = self
        }}
    @IBOutlet private weak var btnAddProduct: UIButton!
    @IBOutlet private weak var lblStoreName: UILabel!
    @IBOutlet fileprivate weak var lblNoData: UILabel!
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(retrieveProducts), name: .UpdateProducts, object: nil)
        prepareCollectionView()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        selectedTabBarItem = 0
        tabBarItems?.selectItem(at: IndexPath(item: selectedTabBarItem, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        fetchData()
    }
    
    internal override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        headerHeight.constant = view.frame.height * 0.30
        let dimention = headerPanel.frame.height * 0.50
        iconWidth.constant = dimention
        iconHeight.constant = dimention
        icon.rounded(withBorder: .gray, borderWidth: 1.0, cornerRadius: dimention / 2)
    }
    
    internal override func updateUi() {
        setupMenuButton()
        if (Localizer.instance.current == .arabic) {
            btnAddProduct.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        } else {
            btnAddProduct.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        }
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        lblStoreName.text = UserDefaults.standard.string(forKey: Misc.GroceryName) ?? .empty
        if let imageLink = UserDefaults.standard.string(forKey: Misc.GroceryAvatar) {
            print("image link: ", imageLink)
            cover.loadImage(from: imageLink, nil)
            icon.loadImage(from: imageLink, nil)
        }
        btnAddProduct.setCorner(radius: btnAddProduct.frame.height / 2)
        btnAddProduct.setShadow(with: Size(right: 1, top: 1), radius: 0.4, opacity: 0.4, color: .gray)
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.shadowImage = UIImage(named: "2pxWidthLineImage")
        navigationController?.navigationBar.setBackgroundImage(UIImage(named: "NavigationBar.png"), for: .default)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: - Actions
    
    @IBAction private func gotoAddProductScreen(_ sender: UIButton) {
        navigateToEditProduct(isNew: true, product: nil)
    }
    
    @objc fileprivate func cellMenuButtonClickd(_ sender: UIButton)-> void {
//        print(#function, sender.tag)
        guard let cell = productsCollection.cellForItem(at: IndexPath(item: sender.tag, section: 0)) as? ProductCell else { return }
        let rect =  cell.container.convert(sender.frame, to: view)
        PopupItem.editsItems { [weak self] (items) in
            if (!items.isEmpty) {
                Threads.main { [weak self] in
                    guard let this = self else { return }
                    self?.openPopover(with: items, in:rect, and: Size(width: this.view.frame.width * 0.40, height: 80)) { (index) in
                        let product = self?.dataSource[sender.tag]
                        switch (index) {
                        case 0: // update product
                            self?.navigateToEditProduct(isNew: false, product: product)
                            
                        case 1: // delete product
                            product?.delete { [weak self] in
                                self?.retrieveProducts()
                            }
                        default:
                            break
                        }
                    }
                }
            }
        }
    }
    

}


// MARK:- Helper functions 

extension Home
{
    fileprivate func prepareCollectionView()-> void {
        productsCollection.register(Nibs.Product, forCellWithReuseIdentifier: ReuseIdentifiers.Product)
        productsCollection.alwaysBounceVertical = true
        productsCollection.scrollIndicatorInsets = Insets(top: 0, left: 0, bottom: 60, right: 0)
    }
    
    fileprivate func fetchData()-> void {
        if (dataSource.isEmpty) {
            retrieveProducts()
        }
    }
    
    @objc fileprivate func retrieveProducts()-> void {
        if (Gemo.gem.isConnected) {
            Product.getProducts { [weak self] (products) in
                if (!products.isEmpty) {
                    self?.dataSource.removeAll()
                    self?.dataSource = products
                    Threads.main { [weak self] in
                        self?.lblNoData.isHidden = true
                    }
                } else {                    
                    Threads.main { [weak self] in
                        self?.lblNoData.isHidden = false
                    }
                }
            }
        } else {
            dialogUserWithNoInternetConnectionMessage()
        }
    }
}


// MARK: - collection view delegate & data source functions

extension Home
{
    
    internal override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (collectionView == productsCollection) ? dataSource.count : super.collectionView(collectionView, numberOfItemsInSection: section)
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (collectionView == productsCollection) {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ReuseIdentifiers.Product, for: indexPath) as? ProductCell else { return UICollectionViewCell() }
            cell.product = dataSource[indexPath.item]
            cell.btnEdits.tag = indexPath.item
            cell.btnEdits.addTarget(self, action: #selector(cellMenuButtonClickd(_:)), for: .touchUpInside)
            return cell
        }
        return super.collectionView(collectionView, cellForItemAt: indexPath)
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView == productsCollection) {
//            print("selected item is: \(indexPath)")
            guard let productInfoPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.ProductDetails) as? ProductDetails else { return }
            productInfoPage.product = dataSource[indexPath.item]
            navigate(to: productInfoPage)
        } else {
            super.collectionView(collectionView, didSelectItemAt: indexPath)
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return (collectionView == productsCollection) ? 10 : super.collectionView(collectionView, layout: collectionViewLayout, minimumLineSpacingForSectionAt: section)
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return (collectionView == productsCollection) ? 0 : super.collectionView(collectionView, layout: collectionViewLayout, minimumInteritemSpacingForSectionAt: section)
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return (collectionView == productsCollection) ? Size(width: collectionView.frame.width - 20, height: collectionView.frame.height * 0.33) : super.collectionView(collectionView, layout: collectionViewLayout, sizeForItemAt: indexPath)
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return (collectionView == productsCollection) ? Insets(top: 10, left: 10, bottom: 60, right: 10) : super.collectionView(collectionView, layout: collectionViewLayout, insetForSectionAt: section)
    }
    
    
}






























