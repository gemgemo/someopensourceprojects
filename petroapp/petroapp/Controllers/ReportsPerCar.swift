
import UIKit
import Gemo

class ReportsPerCar: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
        
    internal var car: Car?
    private var fromDate = "" , toDate = "", timeType = TimeType.all, isFrom = false
    enum TimeType: int { case all = 0, time = 1 }
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var btnChooseDatetime: UIButton!
    @IBOutlet weak var headerPanel: UIView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblSpentLitres: UILabel!
    @IBOutlet weak var btnTo: UIButton!
    @IBOutlet weak var btnFrom: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var datePanel: UIView! { didSet { datePanel.isHidden = true } }
    @IBOutlet weak var selectDatePanel: UIView!
    @IBOutlet weak var selectDatePanelHeight: NSLayoutConstraint!
    @IBOutlet weak var toPanel: View!
    @IBOutlet weak var fromPanel: View!
    @IBOutlet weak var headerHeight: NSLayoutConstraint!
    @IBOutlet weak var lblCarData: UILabel!
    @IBOutlet weak var carIcon: UIImageView!
    @IBOutlet weak var carImage: UIImageView!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        selectDatePanel.isHidden = true
        selectDatePanelHeight.constant = 0.0
        headerHeight.constant -= 40
        fetchReport()
    }
    
    internal override func updateUi() {
        super.updateUi()
        setupNavigationBar(isLogo: false, isMainPage: false, title: Localization.Reports.localized())
        btnChooseDatetime.setTitle(Localization.All.localized(), for: .normal)
        lblPrice.text = Localization.AmountsSpent.localized()
        lblSpentLitres.text = Localization.SpentLitresNo.localized()
        btnTo.setTitle(Localization.To.localized(), for: .normal)
        btnFrom.setTitle(Localization.From.localized(), for: .normal)
        carIcon.loadImage(from: car?.icon ?? .empty)
        carImage.loadImage(from: car?.userCarPhoto ?? .empty)
        lblCarData.text = "\(car?.prand ?? .empty)-\(car?.model ?? .empty)"
        btnTo.setBorder(with: 1, and: Color(white: 0.88, alpha: 0.9))
        btnFrom.setBorder(with: 1, and: Color(white: 0.88, alpha: 0.9))
        btnFrom.setCorner(radius: 4)
        btnTo.setCorner(radius: 4)
    }
    
    
    // MARK: - Actions
    
    @IBAction func defineTypeOnCLick(_ sender: UIButton) {
        let items: [PopupMainMenu] = [ PopupMainMenu(title: Localization.All.localized()),
                                       PopupMainMenu(title: Localization.SelectTime.localized()) ]
        let rect = sender.convert(sender.frame, to: view)
        showPopover(in: rect, with: items, with: Size(width: sender.bounds.width, height: 90)) { [weak self] (index) in
            sender.setTitle(items[index].title, for: .normal)
            self?.toPanel.backgroundColor = .white
            self?.fromPanel.backgroundColor = .white
            switch (index) {
            case 0: // all
                self?.timeType = .all
                self?.selectDatePanel.isHidden = true
                self?.selectDatePanelHeight.constant = 0.0
                self?.headerHeight.constant -= 40
                self?.fetchReport()
            case 1: // select time
                self?.timeType = .time
                self?.selectDatePanel.isHidden = false
                self?.selectDatePanelHeight.constant = 40.0
                self?.headerHeight.constant += 40
                self?.fetchReport(from: self!.fromDate, to: self!.toDate)
            default:
                break
            }
        }
    }
    
    @IBAction func chooseFromDateOnCLick(_ sender: UIButton) {
        isFrom = true
        datePanel.isHidden = false
    }
    
    @IBAction func chooseToDateOnCLick(_ sender: UIButton) {
        isFrom = false
        datePanel.isHidden = false
    }
    
    @IBAction func closeDatePickerOnCLick(_ sender: UIButton) {
        datePanel.isHidden = true
        //print("date========> \(date)")
        if (isFrom) {
            fromDate = date
            btnFrom.setTitle(date, for: .normal)
        } else {
            toDate = date
            btnTo.setTitle(date, for: .normal)
        }
    }
    

}


// MARK: - Helper functions

fileprivate extension ReportsPerCar {
    
    func fetchReport(from: string = .empty, to: string = .empty)-> void {
        let paramters: [string: any] = [
            Misc.MainKey: Misc.MainValue,
            Misc.LanguageKey: Localizer.instance.current.rawValue,
            "car_id": car?.id ?? .empty,
            "date_from": from,
            "date_to": to
        ]
        //print(paramters)
        Gemo.gem.startSpinning()
        Http.request(link: Misc.getUrl(for: "report_one_car"), method: .post, parameters: paramters)
            .response { [weak self] (response) in
                let error = response.error
                print("fetch reports: \(response.jsonString ?? .empty)")
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("\(#function) error: \(error!)")
                    return
                }
                Gemo.gem.stopSpinning()
                guard let json = response.result as? Dictionary<string, any>, let success = json["result"] as? string else {
                    self?.displayAlert(with: Localization.NoData.localized())
                    return
                }
                if (success == "true") {
                    Threads.main { [weak self] in
                        self?.lblSpentLitres.text = "\(Localization.SpentLitresNo.localized())   \(json["total_lit"] as? string ?? .empty)"
                        self?.lblPrice.text = "\(Localization.AmountsSpent.localized())   \(json["total_price"] as? string ?? .empty)"
                    }
                } else {
                    self?.displayAlert(with: json["data"] as? string)
                }
        }
    }
    
}

// MARK: - Computed prperties 

fileprivate extension ReportsPerCar {
    
    var date: string {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = "YYYY-MM-dd"
        return formatter.string(from: datePicker.date)
    }
}

























