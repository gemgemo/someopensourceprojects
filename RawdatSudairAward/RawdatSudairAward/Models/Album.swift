
import Foundation


final class Album
{
    
    internal var id, title, cover: string?, photoNumbers: int?
    
    init(dictionary: [string: any]) {
        id = dictionary["id"] as? string
        title = dictionary["title"] as? string
        //englishTitle = dictionary["title_en"] as? string
        cover = dictionary["image"] as? string
        photoNumbers = dictionary["photo_num"] as? int
    }
    
    
    internal class func getData(by link: string, viewController: UIViewController?, onComplete: @escaping([Album])->void)-> void {
        UIApplication.showIndicator(by: true)
        HttpClient.request(link: link, method: .get) { (data, response, error) -> void in
            if (error != nil) {
                (viewController as? BaseController)?.showAlert(with: string.empty, and: Constants.Misc.ErrorMessage)
                UIApplication.showIndicator(by: false)
                return
            }
            if let dictionaries = data as? [[string: any]] {
                onComplete(dictionaries.map{ Album(dictionary: $0)} )
            } else {
                print("no albums")
            }
        }
    }
    
}
