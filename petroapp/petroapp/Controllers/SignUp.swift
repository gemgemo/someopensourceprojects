
import UIKit
import Gemo

final class SignUp: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var txfName: TextField! { didSet { txfName.delegate = self } }
    @IBOutlet fileprivate weak var txfUsername: TextField! { didSet { txfUsername.delegate = self } }
    @IBOutlet fileprivate weak var txfEmail: TextField! { didSet { txfEmail.delegate = self } }
    @IBOutlet fileprivate weak var txfPhoneNumber: TextField! { didSet { txfPhoneNumber.delegate = self } }
    @IBOutlet fileprivate weak var txfPassword: TextField! { didSet { txfPassword.delegate = self } }
    @IBOutlet fileprivate weak var txfConfirmedPassword: TextField! { didSet { txfConfirmedPassword.delegate = self } }
    @IBOutlet private weak var btnContinue: UIButton!
    @IBOutlet private weak var scrollView: UIScrollView?
    
    // MARK: - Overridden functions
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(_:)), name: .UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide(_:)), name: .UIKeyboardDidHide, object: nil)
    }
    
    internal override func updateUi() {
        super.updateUi()
        setupNavigationBar(isLogo: true, isMainPage: false)
        scrollView?.keyboardDismissMode = .interactive
        btnContinue.setCorner(radius: 3)
        btnContinue.setShadow(with: .zero, radius: 0.7, opacity: 0.8, color: .main)
        btnContinue.setTitle(Localization.ContinueAsMaster.localized(), for: .normal)
        txfName.hint = Localization.Name.localized()
        txfEmail.hint = Localization.Email.localized()
        txfPassword.hint = Localization.Password.localized()
        txfUsername.hint = Localization.UserName.localized()
        txfPhoneNumber.hint = Localization.PhoneNumber.localized()
        txfConfirmedPassword.hint = Localization.RePassword.localized()
        if (Localizer.instance.current == .arabic) {
            txfName.textAlignment = .right
            txfEmail.textAlignment = .right
            txfPassword.textAlignment = .right
            txfUsername.textAlignment = .right
            txfPhoneNumber.textAlignment = .right
            txfConfirmedPassword.textAlignment = .right
        } else {
            txfName.textAlignment = .left
            txfEmail.textAlignment = .left
            txfPassword.textAlignment = .left
            txfUsername.textAlignment = .left
            txfPhoneNumber.textAlignment = .left
            txfConfirmedPassword.textAlignment = .left
        }
        
        /*txfName.textFieldDidEndEditing(txfName)
        txfEmail.textFieldDidEndEditing(txfEmail)
        txfPassword.textFieldDidEndEditing(txfPassword)
        txfUsername.textFieldDidEndEditing(txfUsername)
        txfPhoneNumber.textFieldDidEndEditing(txfPhoneNumber)
        txfConfirmedPassword.textFieldDidEndEditing(txfConfirmedPassword)*/
        
    }
    
    internal override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    internal override func becomeFirstResponder() -> Bool {
        return true
    }
    
    // MARK: - Actions

    @IBAction private func contiuneOnCLick(_ sender: UIButton) {
        guard Gemo.gem.isConnected else {
            displayAlert(with: Localization.InternetConnection.localized())
            return
        }
        guard !name.isEmpty else {
            displayAlert(with: Localization.FirstNameReqiured.localized())
            return
        }
        guard !username.isEmpty else {
            displayAlert(with: Localization.UserNameReqiured.localized())
            return
        }
        guard !phone.isEmpty else {
            displayAlert(with: Localization.PhoneNumberReqiured.localized())
            return
        }
        guard email.isEmail else {
            displayAlert(with: Localization.InvaildEmail.localized())
            return
        }
        guard !password.isEmpty else {
            displayAlert(with: Localization.PasswordReqiured.localized())
            return
        }
        guard !repassword.isEmpty else {
            displayAlert(with: Localization.RePasswordReqiured.localized())
            return
        }
        guard password.characters.count >= 6 else {
            displayAlert(with: Localization.PasswordCount.localized())
            return
        }
        guard password == repassword else {
            displayAlert(with: Localization.PasswordNotConfirmed.localized())
            return
        }

        guard let locationPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.DefineLocation) as? DefineLocation else { return }
        locationPage.name = name
        locationPage.username = username
        locationPage.email = email
        locationPage.phone = phone
        locationPage.password = password
        locationPage.rePassword = repassword
        navigate(to: locationPage)
    }
    
    @objc private func keyboardDidShow(_ notification: Notification) {
        //print("keyboard info \(notification.userInfo)")
        guard let keybordRect = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? Rect else { return }
        scrollView?.contentInset = Insets(top: 0, left: 0, bottom: keybordRect.height, right: 0)
    }
    
    @objc private func keyboardDidHide(_ notification: Notification) {
        scrollView?.contentInset = .zero
    }
    
}

// MARK: - Text Field delegate functions

extension SignUp: UITextFieldDelegate {
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}


// MARK: - Computed properties

extension SignUp {
    
    fileprivate var name: string {
        return txfName.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var username: string {
        return txfUsername.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var email: string {
        return txfEmail.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var password: string {
        return txfPassword.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var repassword: string {
        return txfConfirmedPassword.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var phone: string {
        return txfPhoneNumber.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
}
















