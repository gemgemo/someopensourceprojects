
import Foundation
import Gemo

final class PopoverCell: BaseTableCell
{
    
    // MARK: - Constatnts
    
    
    // MARK: - Variables
    
    internal var item: PopupItem? { didSet { updateUi() }}
    
    // MARK: - Outlets
    
    @IBOutlet private weak var mainPanel: UIView!
    @IBOutlet private weak var lblTitle: UILabel!
    
    // MARK: - Overridden functions
    
    internal override func setSelected(_ selected: Bool, animated: Bool) {
        lblTitle.textColor = (selected) ? #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1) : .darkGray
    }
    
    internal override func updateUi() {
        lblTitle.text = item?.title ?? .empty
        lblTitle.textAlignment = (Localizer.instance.current == .arabic) ? .right : .left
    }
    
    
    // MARK: - Actions
    
    
}
































