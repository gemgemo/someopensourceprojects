
import UIKit

final class Favorites: BaseController
{

    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var dataSource = Array<Product>() {
        didSet {
            ThreadQueue.main { [weak self] in
                self?.favoriteItems.reloadData()
            }
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var titleView: UIView! {
        didSet {
            shadow(for: titleView)
        }
    }
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var favoriteItems: UICollectionView! {
        didSet {
            favoriteItems.delegate = self
            favoriteItems.dataSource = self            
        }
    }
    @IBOutlet fileprivate weak var btnBasket: UIButton!
    @IBOutlet fileprivate weak var btnBack: UIButton!
    
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        if (Gemo.gem.isConnected) {
            loadData()
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
    }
    
    
    // MARK: - Actions

    @IBAction private func backClicked(_ sender: UIButton) {
        back()
    }
    
    @IBAction private func basketClicked(_ sender: UIButton) {
        guard let basketPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.Basket) as? Basket else { return }
        navigate(to: basketPage)
    }
    
    @objc fileprivate func addToBasketClicked(_ sender: UIButton) {
        guard let userId = UserDefaults.standard.value(forKey: Constants.Misc.UserID) as? string, let productId = dataSource[sender.tag].id else { return }
        add(to: Constants.Urls.Basket, forUserId: userId, andProductId: productId)
    }
    
    @objc fileprivate func removeFromFavoritesClicked(_ sender: UIButton) {
        let removedItem = dataSource[sender.tag]
        guard let userId = UserDefaults.standard.string(forKey: Constants.Misc.UserID), let productId = removedItem.id else { return }
        delete(from: Constants.Urls.DeleteOneFavoriteProduct, userId: userId, productId: productId) { [weak self] in
            self?.dataSource.remove(at: sender.tag)
        }
    }
    
    
}


// MARK: - Helper functions

extension Favorites
{
    
    fileprivate func updateUi()-> void {
        tabBar.isHidden = true
        view.bringSubview(toFront: titleView)
        lblTitle.text = Localization.Favorits.localized()
        btnBack.transform = CGAffineTransform.identity
        if (Localizer.instance.current == .arabic) {
            btnBasket.rightAnchor.constraint(equalTo: titleView.rightAnchor, constant: -8).isActive = true
            btnBack.leftAnchor.constraint(equalTo: titleView.leftAnchor, constant: 8).isActive = true
        } else {
            btnBasket.leftAnchor.constraint(equalTo: titleView.leftAnchor, constant: 8).isActive = true
            btnBack.rightAnchor.constraint(equalTo: titleView.rightAnchor, constant: -8).isActive = true
            btnBack.transform = btnBack.transform.rotated(by: cgFloat.pi)
        }
    }
    
    fileprivate func setupCollectionView()-> void {
        favoriteItems.alwaysBounceVertical = true
        favoriteItems.register(Constants.Nibs.SectionDetails, forCellWithReuseIdentifier: Constants.ReuseIdentifiers.SectionDetails)
    }
    
    fileprivate func loadData()-> void {
        guard let userId = UserDefaults.standard.string(forKey: Constants.Misc.UserID) else { return }
        let parameters: Dictionary<string, any> = [
            Constants.Misc.MainKey: Constants.Misc.MainValue,
            "user_id": userId
        ]
        Product.getData(from: Constants.Urls.Favorites, with: parameters, in: self) { [weak self] (products) in
            Gemo.gem.stopSpinning()
            if (!products.isEmpty) {
                self?.dataSource = products
            }
        }
    }
}



extension Favorites
{
    internal override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch(collectionView) {
        case favoriteItems:
            return dataSource.count
        default:
            return super.collectionView(collectionView, numberOfItemsInSection: section)
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (collectionView == favoriteItems) {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ReuseIdentifiers.SectionDetails, for: indexPath) as? SectionDetailsCell else {
                return super.collectionView(collectionView, cellForItemAt: indexPath)
            }
            cell.product = dataSource[indexPath.item]
            cell.btnLeft.setImage(#imageLiteral(resourceName: "ic-remove"), for: .normal)
            cell.btnLeft.imageView?.contentMode = .scaleAspectFit
            cell.btnLeft.tag = indexPath.item
            cell.btnRight.tag = indexPath.item
            cell.btnRight.addTarget(self, action: #selector(addToBasketClicked(_:)), for: .touchUpInside) // add to basket
            cell.btnLeft.addTarget(self, action: #selector(removeFromFavoritesClicked(_:)), for: .touchUpInside) // remove from favorites
            return cell
        }
        return super.collectionView(collectionView, cellForItemAt: indexPath)
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView == favoriteItems) {
            print("selected item is: \(indexPath)")
        } else {
            super.collectionView(collectionView, didSelectItemAt: indexPath)
        }
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    internal override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (collectionView == favoriteItems) {
            return Size(width: collectionView.frame.width, height: collectionView.frame.height*0.33)
        } else {
            return super.collectionView(collectionView, layout: collectionViewLayout, sizeForItemAt: indexPath)
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        switch (collectionView) {
        case favoriteItems:
            return Insets(top: 8, left: 0, bottom: 0, right: 0)
        default:
            return .zero
        }
    }
    
}




















