
import UIKit
import Gemo


internal var cELetters, cENumbers, cALetters, cANumbers: String?
class BoardNumber: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var isUpdate = false, car: Car?
    
    // MARK: - Outlets
    
    @IBOutlet weak var lblBoardNo: UILabel!
    @IBOutlet weak var boardNoPhoto: UIImageView!
    @IBOutlet weak var txfALetters: TextField! { didSet { txfALetters.delegate = self } }
    @IBOutlet weak var txfANumber: TextField! { didSet { txfANumber.delegate = self } }
    @IBOutlet weak var txfELetters: TextField! { didSet { txfELetters.delegate = self } }
    @IBOutlet weak var txfENumbers: TextField! { didSet { txfENumbers.delegate = self } }
    @IBOutlet weak var btnContiune: UIButton!
    
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        lblBoardNo.text = Localization.EnterCarBoardno.localized()
        txfALetters.hint = Localization.ArabicLetters.localized()
        txfANumber.hint = Localization.ArabicNumbers.localized()
        txfELetters.hint = Localization.EnglishLetters.localized()
        txfENumbers.hint = Localization.EnglishNumbers.localized()
        btnContiune.setTitle(Localization.Continuation.localized(), for: .normal)
        if (isUpdate) {
            txfALetters.text = car?.aLetters ?? .empty
            txfALetters.textFieldDidBeginEditing(txfALetters)
            txfANumber.text = car?.aNumbers ?? .empty
            txfANumber.textFieldDidBeginEditing(txfANumber)
            txfELetters.text = car?.eLetters ?? .empty
            txfELetters.textFieldDidBeginEditing(txfELetters)
            txfENumbers.text = car?.eNumbers ?? .empty
            txfENumbers.textFieldDidBeginEditing(txfENumbers)
            
        }
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    
    // MARK: - Actions
    
    @IBAction func contiuneOnCLick(_ sender: UIButton) {
        view.endEditing(true)
        guard !aLetters.isEmpty || !eLetters.isEmpty || !aNumbers.isEmpty || !eNumbers.isEmpty else {
            displayAlert(with: Localization.NoData.localized())
            return
        }
        cALetters = aLetters
        cANumbers = aNumbers
        cELetters = eLetters
        cENumbers = eNumbers
        NotificationCenter.default.post(name: .ShowCarUser, object: nil)
    }
    

}


// MARK: - Computed properties

fileprivate extension BoardNumber {
    var aLetters: string {
        return txfALetters.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    var aNumbers: string {
        return txfANumber.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    var eLetters: string {
        return txfELetters.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    var eNumbers: string {
        return txfENumbers.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
}


// MARK: - Text Field delegate functions

extension BoardNumber: UITextFieldDelegate {
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}























