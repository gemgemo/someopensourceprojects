
import UIKit
import Localize_Swift

final class Main: BaseController
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    fileprivate var dataSource = Array<MainItem>()//, previousScrollOffset: cgFloat = 0, maxHeight: cgFloat = 0
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var btnMenu: UIBarButtonItem!
    @IBOutlet fileprivate weak var collectionView: UICollectionView! {
        didSet {
            collectionView.delegate = self
            collectionView.dataSource = self
        }
    }
    @IBOutlet fileprivate weak var collectionContainer: UIView!
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        configureSideMenu()
        configureCollectionView()
        
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBar()
        UIApplication.shared.isStatusBarHidden = false
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         NotificationCenter.default.addObserver(self, selector: #selector(updateNavigationBar), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    internal override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    //MARK: - Actions
    

    
}




// MARK: - Helper functions

extension Main
{
    
    fileprivate func configureSideMenu()-> void {
        revealViewController().rightViewRevealWidth = (UIDevice.current.userInterfaceIdiom == .pad) ? view.bounds.width*0.69 : view.bounds.width*0.68
        btnMenu.target = revealViewController()
        btnMenu.action = #selector(revealViewController().rightRevealToggle(_:))
        view.addGestureRecognizer(revealViewController().panGestureRecognizer())
    }
    
    @objc fileprivate func updateNavigationBar()-> void {
        navigationController?.navigationBar.semanticContentAttribute = .forceLeftToRight
        navigationController?.navigationBar.layer.shadowColor = Color.clear.cgColor
        collectionContainer.setShadow(with: Size(right: 0, top: 0), radius: 2.5, opacity: 1.0, color: .darkGray)
        dataSource = MainItem.getItems()
        collectionView.reloadData()
    }
    
    fileprivate func configureCollectionView()-> void {
        collectionView.register(Constants.Nib.Main, forCellWithReuseIdentifier: Constants.ReuseIdentifier.Main)
    }
    
  }


// MARK: - Collection view delegate & data source functions 

extension Main: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    
    internal func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ReuseIdentifier.Main, for: indexPath) as? MainCell {
            cell.item = dataSource[indexPath.item]
            return cell
        }
        return UICollectionViewCell()
    }
    
    internal func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "mainHeadercellid", for: indexPath) as? MainHeader {
            return header
        }
        return UICollectionReusableView()
    }
    
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.item)
        guard revealViewController().frontViewPosition == .left else {
            revealViewController().rightRevealToggle(animated: true)
            return
        }
        switch (indexPath.item) {
        case 0: // open albumes
            if let albumsScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.AwardListImages) as? AwardListImages {
                navigationController?.pushViewController(albumsScreen, animated: true)
            }
            
        case 1: // open award list
            if let awardListScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.AwardList) as? AwardList {
                navigationController?.pushViewController(awardListScreen, animated: true)
            }
            
        case 2:
            if let statisticsScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Statistics) as? Statistics {
                navigationController?.pushViewController(statisticsScreen, animated: true)
            }
            
        case 3: // open video gallery
            if let videoGalleryScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.AwardVideos) as? AwardVideos {
                navigationController?.pushViewController(videoGalleryScreen, animated: true)
            }
            
        case 4: // open award board of management
            if let managersScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.BoardOfAwardManagement) as? BoardOfPrizeManagement {
                navigationController?.pushViewController(managersScreen, animated: true)
            }
            
        case 5: // open board of trustess
            if let trusteesScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.BoardOfTrustees) as? BoardOfTrustees {
                navigationController?.pushViewController(trusteesScreen, animated: true)
            }
            
        case 6:
            if let annualReportsScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.AnnualReports) as? AnnualReports {
                navigationController?.pushViewController(annualReportsScreen, animated: true)
            }
            
        case 7:
            if let barCodeScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.BarcodeCompetition) as? BarcodeCometition {
                navigationController?.pushViewController(barCodeScreen, animated: true)
            }
            
        default:
            break
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let dimantions = collectionView.bounds.width/2-5
        return Size(width: dimantions, height: dimantions)
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return Insets.zero
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return Size(width: view.bounds.width, height: view.bounds.height*0.27)
    }
    
    
    
}




























