
import Foundation
import UIKit

@IBDesignable
final class CheckBox: UIView
{
    @IBInspectable
    internal var isChecked: Bool = false { didSet { setNeedsDisplay() } }
    @IBInspectable
    internal var image: UIImage = UIImage() { didSet { setNeedsDisplay() } }
    @IBInspectable
    internal var selectedColor: UIColor = UIColor.blue { didSet { setNeedsDisplay() } }
    
    private var markImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private var mainPanel: UIView = {
        let panel = UIView()
        panel.translatesAutoresizingMaskIntoConstraints = false
        panel.backgroundColor = .white
        return panel
    }()
    
    internal override func draw(_ rect: CGRect) {
        setup()
    }
    
    private func setup()-> void {
        mainPanel.layer.borderWidth = 1.6
        mainPanel.layer.cornerRadius = 2.0
        mainPanel.layer.masksToBounds = true
        layer.cornerRadius = 2.0
        layer.masksToBounds = true
        backgroundColor = .clear
        addSubview(mainPanel)
        mainPanel.topAnchor.constraint(equalTo: topAnchor).isActive = true
        mainPanel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        mainPanel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        mainPanel.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        if (isChecked) {
            mainPanel.layer.borderColor = selectedColor.cgColor
            mainPanel.backgroundColor = selectedColor
            markImage.backgroundColor = selectedColor
            markImage.image = image
            mainPanel.addSubview(markImage)
            // mark image constraints-> t,r,b,l
            markImage.topAnchor.constraint(equalTo: topAnchor, constant: 0.0).isActive = true
            markImage.rightAnchor.constraint(equalTo: rightAnchor, constant: -1.0).isActive = true
            markImage.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0.0).isActive = true
            markImage.leftAnchor.constraint(equalTo: leftAnchor, constant: 1.0).isActive = true
        } else {
            mainPanel.layer.borderColor = UIColor.darkGray.cgColor
            mainPanel.backgroundColor = .white
            markImage.removeFromSuperview()
        }
    }
    
    
}































