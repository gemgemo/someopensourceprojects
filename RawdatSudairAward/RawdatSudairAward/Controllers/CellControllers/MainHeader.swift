
import UIKit

final class MainHeader: UICollectionReusableView
{
    
    // MARK: - Constants
    
    // MARK: - Variables
    
    // MARK: - Outlets
        
    //@IBOutlet fileprivate weak var subTitleView: UIView!
    @IBOutlet fileprivate weak var shadowPanal: UIView! {
        didSet {
            /*let gradient = CAGradientLayer()
            gradient.frame = shadowPanal.bounds
            gradient.colors = [Color.normal.cgColor, Color.normal.cgColor, Color.normal.cgColor, Color.lightGray.withAlphaComponent(0.3).cgColor]
            shadowPanal.layer.addSublayer(gradient)*/
            //shadowPanal.setShadow(with: Size(right: 0, top: 1), radius: 1, opacity: 0.7, color: .lightGray)
        }
    }
    @IBOutlet private weak var container: UIView! {
        didSet {
            container.setShadow(with: Size(right: 0, top: 1), radius: 1, opacity: 0.7, color: .lightGray)
        }
    }
    
    // MARK: - Overridden functions
    
    // MARK: - Actions
    
  
    
}
