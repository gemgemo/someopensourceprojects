
import UIKit
import Localize_Swift
import CoreLocation
import GoogleMaps

final class DriverMap: BaseController
{
    // MARK: - Constants
    
    fileprivate let locationManager = CLLocationManager()
    
    // MARK: - Variables
    
    fileprivate var sourceLocation: CLLocation?,
                    destinationLocation: CLLocation?,
                    //sourcePin: SourcePin?, // stop it
                    sourceMarker: GMSMarker?,
                    destinationMarker: GMSMarker?,
                    //destinationPin: DestinationPin?, // stop it
                    sourceCoordinate2D: CLLocationCoordinate2D?,
                    destinationCoordinate2D: CLLocationCoordinate2D?,
                    routeDistance: CLLocationDistance?,
                    polyline: GMSPolyline?
    
    
    internal var weight: string?,
                 numberOf: string?,
                 selectedMainCar: DriverMainCar?,
                 selectedSubCar: SubCar?,
                 pricingWay: int?,
                 paymentWay: int?,
                 typeTime: int?,
                 date: string?,
                 dirverCarType: string?
    
    
    // MARK: - Outlets
    
    /*@IBOutlet fileprivate weak var mapView: MKMapView! {
        didSet {
            mapView.delegate = self
        }
    }*/
    @IBOutlet fileprivate weak var fromView: UIView! {
        didSet {
            fromView.setShadow(with: .zero, radius: 2, opacity: 0.6, color: .black)
        }
    }
    @IBOutlet fileprivate weak var lblFrom: UILabel!
    @IBOutlet fileprivate weak var txfSource: AutoCompleteTextField! {
        didSet {
            txfSource.clearButtonMode = .never
            txfSource.delegate = self
        }
    }
    @IBOutlet fileprivate weak var toView: UIView! {
        didSet {
            toView.setShadow(with: .zero, radius: 2, opacity: 0.6, color: .black)
        }
    }
    @IBOutlet fileprivate weak var lblTo: UILabel!
    @IBOutlet fileprivate weak var txfDestination: AutoCompleteTextField! {
        didSet {
            txfDestination.clearButtonMode = .never
            txfDestination.delegate = self
        }
    }
    @IBOutlet fileprivate weak var btnSendOrder: Button!
    @IBOutlet fileprivate weak var mapPanal: GMSMapView! {
        didSet {
            //mapPanal.camera = GMSCameraPosition.camera(withLatitude: 21.390571, longitude: 39.852031, zoom: 15.0)
            mapPanal.mapType = .terrain
        }
    }
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        configureAutoPlaceCompleteFields()
        autoPlaceCompleteHandlers()
        setUserLocation()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        txfSource.clearButtonMode = .never
        txfDestination.clearButtonMode = .never
        NotificationCenter.default.addObserver(self, selector: #selector(updateUi), name: Notification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    internal override func updateNavgationBar() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ac_back"), style: .plain, target: self, action: #selector(goBack(_:)))
        let btnMenu = UIBarButtonItem(image: #imageLiteral(resourceName: "ac_manu"), style: .plain, target: revealViewController(), action: #selector(revealViewController().rightRevealToggle(_:)))
        navigationItem.rightBarButtonItem = btnMenu
        navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "ac_logo"))
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Actions
    
    @IBAction private func setSourceLocationOnClick(_ sender: UIButton) {
        guard !from.isEmpty else { return }
        /*if let annotation = sourcePin {
            mapView.removeAnnotation(annotation)
        }*/
        sourceMarker?.map = nil
        geocode(this: from) { [weak self] (coordinate) -> void in
            guard let this = self else { return }
            //this.sourcePin = SourcePin(coordinate: coordinate, title: .empty, subtitle: .empty)
            /*if let sourceCoordinate = self?.sourceCoordinate2D {
                self?.mapView.removeAnnotation(SourcePin(coordinate: sourceCoordinate, title: .empty, subtitle: .empty))
            }
            if let sA = this.sourcePin {
                self?.mapView.addAnnotation(sA)
            }
            self?.mapView.setRegion(in: coordinate)*/
            this.mapPanal.camera = this.region(for: coordinate)
            this.sourceMarker = this.annotation(for: coordinate, in: this.mapPanal, icon: #imageLiteral(resourceName: "map_mark1"))
            self?.sourceCoordinate2D = coordinate
            
        }
    }
    
    @IBAction private func clearSourceTextOnCLick(_ sender: UIButton) {
        txfSource.text = .empty
    }
    
    @IBAction private func setDestinationLocationOnClick(_ sender: UIButton) {
        guard !to.isEmpty else { return }
        destinationMarker?.map = nil
        geocode(this: to) { [weak self] (coordinate) -> void in
            guard let this = self else { return }
            this.mapPanal.camera = this.region(for: coordinate)
            this.destinationMarker = this.annotation(for: coordinate, in: this.mapPanal, icon: #imageLiteral(resourceName: "map_mark2"))
            this.destinationCoordinate2D = coordinate
            /*this.destinationPin = DestinationPin(coordinate: coordinate, title: .empty, subtitle: .empty)
            if let destinationCoordinate = self?.destinationCoordinate2D {
                this.mapView.removeAnnotation(DestinationPin(coordinate: destinationCoordinate, title: .empty, subtitle: .empty))
            }
            if let sD = this.destinationPin {
                this.mapView.addAnnotation(sD)
            }
            this.mapView.setRegion(in: coordinate)
            this.destinationCoordinate2D = coordinate
            */
            
            
            // TODO:- Draw route
            this.polyline?.map = nil
            if let sLocation = self?.sourceCoordinate2D, let dLocation = self?.destinationCoordinate2D {
                this.drawRoute(from: sLocation, to: dLocation, for: this.mapPanal) { [weak self] (polyline, distance) -> void in
                    self?.polyline = polyline
                    self?.routeDistance = round(distance) 
                    print("distance is \(this.routeDistance ?? 0)")
                }
                /*this.drawRoute(from: sLocation, to: dLocation, for: this.mapPanal) { [weak self] (polyline, distance) -> void in
                    self?.polyline = polyline
                    self?.routeDistance = distance
                    print("distance is \(this.routeDistance)")
                }*/
                /*let path = GMSMutablePath()
                path.add(sLocation)
                path.add(dLocation)
                this.polyline = GMSPolyline(path: path)
                this.polyline?.geodesic = true
                this.polyline?.strokeColor = .darkGray
                this.polyline?.strokeWidth = 3.0
                this.polyline?.map = this.mapPanal
                if let distance = this.polyline?.path?.length(of: .geodesic) {
                    this.routeDistance = distance/1000
                    print("distance is \(this.routeDistance)")
                }*/
                
                /*this.drawRoute(from: sLocation, to: dLocation, for: this.mapView) { (distance) -> void in
                    this.routeDistance = distance
                }*/
            }
            
        }
    }
    
    @IBAction private func clearDestinationTextOnClick(_ sender: UIButton) {
        txfDestination.text = .empty
    }
    
    @IBAction private func sendOrderOnClick(_ sender: Button) {
        if let driverInfoScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.DriverOrderInfo) as? DriverOrderInfo {
            guard let distance = routeDistance, distance != Number.zero.doubleValue else {
                showAlert(with: .empty, and: "distanceerrormessagelan".localized())
                return
            }
            driverInfoScreen.selectedMainCar = selectedMainCar
            driverInfoScreen.selectedSubCar = selectedSubCar
            driverInfoScreen.paymentWay = paymentWay
            driverInfoScreen.pricingWay = pricingWay
            driverInfoScreen.weight = weight
            driverInfoScreen.numberOf = numberOf
            driverInfoScreen.fromTitle = from
            driverInfoScreen.toTitle = to
            driverInfoScreen.from = sourceCoordinate2D
            driverInfoScreen.to = destinationCoordinate2D
            driverInfoScreen.distance = routeDistance
            driverInfoScreen.typeTime = typeTime
            driverInfoScreen.date = date
            driverInfoScreen.driverCarType = dirverCarType
            navigate(to: driverInfoScreen)
        }
    }
    
    
}



// MARK: - Helper Functions

extension DriverMap
{
    
    @objc fileprivate func updateUi()-> void {
        lblFrom.text = "labelfromlan".localized()
        lblTo.text = "labeltolan".localized()
        btnSendOrder.setTitle("sendorderbuttonlan".localized(), for: .normal)
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            fromView.semanticContentAttribute = .forceLeftToRight
            toView.semanticContentAttribute = .forceLeftToRight
        } else {
            fromView.semanticContentAttribute = .forceRightToLeft
            toView.semanticContentAttribute = .forceRightToLeft
        }
    }
    
    fileprivate func configureAutoPlaceCompleteFields()-> void {
        [txfSource, txfDestination].forEach { (field) in
            field?.autoCompleteTextColor = .darkGray
            field?.autoCompleteTextFont = Font(name: "HelveticaNeue-Light", size: 12.0)!
            field?.autoCompleteCellHeight = 44.0
            if let tableView = field?.autoCompleteTableView {
                field?.superview?.superview?.superview?.bringSubview(toFront: tableView)
            }
            field?.maximumAutoCompleteCount = 10
            field?.hidesWhenSelected = true
            field?.hidesWhenEmpty = true
            field?.enableAttributedText = true
            var attributes = [string: object]()
            attributes[NSForegroundColorAttributeName] = UIColor.black
            attributes[NSFontAttributeName] = UIFont(name: "HelveticaNeue-Bold", size: 12.0)
            field?.autoCompleteAttributes = attributes
        }
    }
    
    fileprivate func autoPlaceCompleteHandlers()-> void {
        txfSource.onTextChange = { [weak self] (text) in
            guard let this = self else { return }
            this.toView.isHidden = true
            if (!text.isEmpty) {
                this.getPlaces(by: text) { (places) -> void in
                    DispatchQueue.main.async { [weak self] in
                        self?.txfSource.autoCompleteStrings = places
                    }
                    UIApplication.showIndicator(by: false)
                }
            } else {
                this.toView.isHidden = false
            }
        }
        
        txfDestination.onTextChange = { [weak self] (text) in
            guard let this = self else { return }
            if (!text.isEmpty) {
                this.getPlaces(by: text) { (places) -> void in
                    DispatchQueue.main.async { [weak self] in
                        self?.txfDestination.autoCompleteStrings = places
                    }
                    UIApplication.showIndicator(by: false)
                }
            }
        }
        
        txfSource.onSelect = { [weak self] (text, index) in
            //print(text, index)
            self?.view.endEditing(true)
        }
        
        txfDestination.onSelect = { [weak self] (text, index) in
            // print(text, index)
            self?.view.endEditing(true)
        }
        
    }
    
    fileprivate func setUserLocation()-> void {
        //mapView.setupCamera()
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let this = self else { return }
            this.locationManager.delegate = self
            this.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            this.locationManager.requestAlwaysAuthorization()
            if (CLLocationManager.locationServicesEnabled()) {
                this.locationManager.startUpdatingLocation()
            } else {
                DispatchQueue.main.async { [weak self] in
                    guard let this = self else { return }
                    let alert = UIAlertController(title: "warningtitlelan".localized(), message: "locationsubtitlelan".localized() , preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "okLan".localized(), style: .default, handler: { (action) in
                        if let settingsUrl = URL(string: UIApplicationOpenSettingsURLString), UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.openURL(settingsUrl)
                        }
                    }))
                    alert.addAction(UIAlertAction(title: "cancellan".localized(), style: .default, handler: nil))
                    this.present(alert, animated: true, completion: nil)
                }
            }
        }

    }
}



// MARK: - Text Field delegate functions

extension DriverMap: UITextFieldDelegate
{
    
    internal func textFieldDidEndEditing(_ textField: UITextField) {
        if (toView.isHidden) {
            toView.isHidden = false
        }
    }
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (toView.isHidden) {
            toView.isHidden = false
        }
        view.endEditing(true)
        return true
    }
    
}



// MARK: - Map view delegate functions
/*
extension DriverMap: MKMapViewDelegate
{
    
    internal func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        UIApplication.showIndicator(by: false)
    }
    
    internal func mapViewDidStopLocatingUser(_ mapView: MKMapView) {
        UIApplication.showIndicator(by: false)
    }
    
    internal func mapViewWillStartLoadingMap(_ mapView: MKMapView) {
        UIApplication.showIndicator(by: true)
    }
    
    internal func mapViewWillStartLocatingUser(_ mapView: MKMapView) {
        UIApplication.showIndicator(by: true)
    }
    
    internal func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if (annotation.isKind(of: MKUserLocation.self)) {
            return nil
        }
        if (annotation is SourcePin) {
            let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: Constants.Misc.SourceAnnotation)
            annotationView.image = #imageLiteral(resourceName: "map_mark1")
            return annotationView
        } else if (annotation is DestinationPin) {
            let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: Constants.Misc.DestinationAnnotation)
            annotationView.image = #imageLiteral(resourceName: "map_mark2")
            return annotationView
        } else {
            let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: Constants.Misc.SourceAnnotation)
            annotationView.image = #imageLiteral(resourceName: "map_mark1")
            return annotationView
        }

    }
    
    internal func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let render = MKPolylineRenderer(overlay: overlay)
        render.strokeColor = .darkGray
        render.lineWidth = 3.0
        return render
    }
    
    
}
*/

// MARK: - Location Maneger

extension DriverMap: CLLocationManagerDelegate
{
    
    internal func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("location manager error \(error)")
    }
    
    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        /*if let userLocation = locations.first {
            sourceLocation = userLocation
            let coordinate = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
            sourceCoordinate2D = coordinate
            mapView.annotations.forEach { mapView.removeAnnotation($0) }
            mapView.setRegion(in: coordinate)
            sourcePin = SourcePin(coordinate: coordinate, title: .empty, subtitle: .empty)
            if let annotation = sourcePin {
                mapView.addAnnotation(annotation)
            }            
            address(from: userLocation) { [weak self] (address) -> void in
                self?.txfSource.text = address
            }
        }*/
        if let userLocation = locations.last {
            mapPanal.clear()
            sourceLocation = userLocation
            let coordinate = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
            sourceCoordinate2D = coordinate
            mapPanal.camera = region(for: coordinate)
            sourceMarker = annotation(for: coordinate, in: mapPanal, icon: #imageLiteral(resourceName: "map_mark1"))
            // reverse location to address
            address(from: userLocation) { [weak self] (address) in
                self?.txfSource.text = address
            }
        }
        manager.stopUpdatingLocation()
    }
    
}

// MARK: - Computed properties
 extension DriverMap
{
    
    fileprivate var to: string {
        return txfDestination.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var from: string {
        return txfSource.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
}

















