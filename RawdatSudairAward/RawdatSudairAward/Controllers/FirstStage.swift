
import UIKit

final class FirstStage: BaseController
{
    
    // MARK: - Constants
    
    // MARK: - Variables
    
    internal var gender: int?
    fileprivate var datesDataSource = Array<DateTime>(), bottom: NSLayoutConstraint!, dataSource = Array<PersonStatistics>()
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var tblData: UITableView! {
        didSet {
            tblData.delegate = self
            tblData.dataSource = self
        }
    }
    @IBOutlet fileprivate weak var lblDate: UILabel! {
        didSet {
            lblDate.text = string.empty
        }
    }
    @IBOutlet fileprivate var datePickerContainer: UIView!
    @IBOutlet fileprivate weak var datePicker: UIPickerView! {
        didSet {
            datePicker.delegate = self
            datePicker.dataSource = self
        }
    }
    @IBOutlet fileprivate weak var dateView: UIView!
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        tblData.register(Constants.Nib.StatisticsDetails, forCellReuseIdentifier: Constants.ReuseIdentifier.StatisticsDetails)
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUI()
        tblData.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tableViewTapped(_:))))
    }
    
    
    // MARK: - Actions
    
    @IBAction private func openDatePicker(_ sender: UIButton) {
        bottom.constant = 0
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
                       usingSpringWithDamping: 0.6,
                       initialSpringVelocity: 0.4,
                       options: .curveEaseInOut,
                       animations: { [weak self] in
                        self?.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    @objc private func tableViewTapped(_ gesture: UITapGestureRecognizer) {
        hidePicker()
    }
    
    
}


// MARK: - Helper functions

extension FirstStage
{
    
    
    fileprivate func loadData()-> void {
        print(setupLink())
        PersonStatistics.getData(from: setupLink(), page: self) { [weak self] (statistics) in
            guard let this = self else { return }
            this.dataSource = statistics
            DispatchQueue.main.async { [weak self] in
                self?.tblData.reloadData()
            }
            UIApplication.showIndicator(by: false)
        }
    }
    
    private func setupLink()-> string {
        return "http://192.232.214.91/~rawdatsudeer/\(Constants.BackendURLs.shared.localizeUrl)/stud.php?type=\(gender ?? Number.zero.intValue)&stage=1&date_h=\(lblDate.text ?? string.empty)&key=RAWDAT_SUDAIR"
    }
    
    fileprivate func hidePicker()-> void {
        bottom.constant = view.bounds.height*0.40
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
                       usingSpringWithDamping: 0.6,
                       initialSpringVelocity: 0.4,
                       options: .curveEaseInOut,
                       animations: { [weak self] in
                        self?.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    private func setupPicker()-> void {
        datePickerContainer.translatesAutoresizingMaskIntoConstraints = false
        datePickerContainer.layer.borderColor = Color.darkGray.cgColor
        datePickerContainer.layer.borderWidth = 0.7
        view.addSubview(datePickerContainer)
        datePickerContainer.heightAnchor.constraint(equalToConstant: view.bounds.height*0.30).isActive = true
        datePickerContainer.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        datePickerContainer.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        bottom =  datePickerContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: view.bounds.height*0.30)
        bottom.isActive = true
    }
    
    fileprivate func updateUI()-> void {
        dateView.layer.cornerRadius = 7.0
        dateView.layer.borderWidth = 1.0
        dateView.layer.borderColor = Color(white: 0.80, alpha: 0.8).cgColor
        setupPicker()
        loadDates { [weak self] in
            self?.loadData()
        }
    }
    
    
    private func loadDates(onComplete: @escaping()-> void)-> void {
        DateTime.getData(from: Constants.BackendURLs.shared.DateTime, page: self) { [weak self] (dates) -> void in
            guard let this = self else { return }
            this.datesDataSource = dates
            DispatchQueue.main.async { [weak self] in
                self?.lblDate.text = dates.first?.title ?? string.empty
                self?.datePicker.reloadAllComponents()
                onComplete()
            }
            UIApplication.showIndicator(by: false)
        }
    }
    
}


// MARK: - Table view delegate & data source functions

extension FirstStage: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.StatisticsDetails) as? StatisticsDetailsCell {
            cell.statistics = dataSource[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
}



// MARK: - Picker delegate & data source functions

extension FirstStage: UIPickerViewDelegate, UIPickerViewDataSource
{
    
    internal func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    internal func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return datesDataSource.count
    }
    
    internal func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return datesDataSource[row].title ?? string.empty
    }
    
    internal func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        lblDate.text = datesDataSource[row].title ?? string.empty
        loadData()
        hidePicker()
    }
    
}





















