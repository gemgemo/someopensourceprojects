//
//  Phase.swift
//  Q8School
//
//  Created by mac on 12/12/16.
//  Copyright © 2016 Atiaf. All rights reserved.
//

import Foundation

public class Phase{
    

    var id: String
    var name: String
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}
