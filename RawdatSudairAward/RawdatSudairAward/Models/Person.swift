
import Foundation




final class Person
{
    
    internal var id , name, position, photo: string?
    
    init(dictionary: [string: any]) {
        id = dictionary["id"] as? string
        name = dictionary["name"] as? string
        //englishName = dictionary["name_en"] as? string
        position = dictionary["position"] as? string
        //englishPosition = dictionary["position_en"] as? string
        photo = dictionary["photo"] as? string
    }
    
    
    
    internal class func getData(by link: string, viewController: UIViewController?, onComplete: @escaping([Person])->void)-> void {
        UIApplication.showIndicator(by: true)
        HttpClient.request(link: link, method: .get) { (data, response, error) -> void in
            if (error != nil) {
                (viewController as? BaseController)?.showAlert(with: string.empty, and: Constants.Misc.ErrorMessage)
                UIApplication.showIndicator(by: false)
                return
            }
            
            if let dics = data as? [[string: any]] {
                onComplete(dics.map{ Person(dictionary: $0) })
            } else {
                print("none dictionaries")
            }
            
        }
    }
    
    
    
}
