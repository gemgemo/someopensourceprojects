
import Foundation

final class AwardListItem
{
    
    internal var id:int?, title: string?, icon: UIImage?
    
    
    init(id: int, title: string, icon: UIImage) {
        self.id = id
        self.title = title
        self.icon = icon
    }
    
    
    internal class func getItems()-> [AwardListItem] {
        var items = Array<AwardListItem>()
        items.append(AwardListItem(id: 1, title: "visionlan".localized(), icon: #imageLiteral(resourceName: "vision")))
        items.append(AwardListItem(id: 2, title: "messagelan".localized(), icon: #imageLiteral(resourceName: "letter")))
        items.append(AwardListItem(id: 3, title: "goalslan".localized(), icon: #imageLiteral(resourceName: "target")))
        items.append(AwardListItem(id: 4, title: "custodianlan".localized(), icon: #imageLiteral(resourceName: "star")))
        
        return items
    }
    
}













