
import UIKit
import GoogleMaps

final class StoreInfo: BaseController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var store: Shop?
    fileprivate var rate = Number.zero.intValue
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var icon: UIImageView!
    @IBOutlet fileprivate weak var lblStoreName: UILabel! { didSet { lblStoreName.text = .empty } }
    @IBOutlet fileprivate weak var lblStoreDetails: UILabel! { didSet { lblStoreDetails.text = .empty } }
    @IBOutlet fileprivate weak var lblPhoneTitle: UILabel!
    @IBOutlet fileprivate weak var lblPhone: UILabel!
    @IBOutlet fileprivate weak var lblLocationTitle: UILabel!
    @IBOutlet fileprivate weak var lblLocation: UILabel!
    @IBOutlet fileprivate weak var lblTypesTitle: UILabel!
    @IBOutlet fileprivate weak var lblTypes: UILabel!
    @IBOutlet fileprivate weak var lblProductsCountTitle: UILabel!
    @IBOutlet fileprivate weak var lblProductsCount: UILabel!
    @IBOutlet fileprivate weak var lblRateTitle: UILabel!
    @IBOutlet fileprivate var stars: [UIImageView]!
    @IBOutlet fileprivate weak var btnSendComplaint: UIButton!
    @IBOutlet fileprivate weak var btnShowRatePanel: UIButton!
    @IBOutlet fileprivate weak var btnAddRate: Button!
    @IBOutlet fileprivate var ratePanel: UIView!
    @IBOutlet private var reviews: [UIButton]!
    @IBOutlet fileprivate weak var lblRatePanelTitle: UILabel!
    @IBOutlet fileprivate weak var headerPanel: UIView!
    @IBOutlet fileprivate weak var contentPanel: UIView!
    @IBOutlet fileprivate weak var headerLabelsPanel: UIView!
    @IBOutlet fileprivate weak var reviewComponents: UIView!
    @IBOutlet fileprivate weak var mainStack: UIStackView!
    @IBOutlet fileprivate weak var sideStack: UIStackView!
    @IBOutlet fileprivate weak var starsStack: UIStackView!
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        setupRatePanel()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBar.isHidden = true
        updateUi()
        if (Gemo.gem.isConnected) {
            loadData()
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
    
    
    // MARK: - Actions
    
    @IBAction private func sendComplaintOnCLick(_ sender: UIButton) {
        guard let sendComplaintPage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.SendComplaint) as? SendComplaint else { return }
        sendComplaintPage.storeId = store?.id
        navigate(to: sendComplaintPage)
    }
    
    @IBAction private func showRatePanelOnCLick(_ sender: UIButton) {
        ratePanel.isHidden = false
        let animator = UIViewPropertyAnimator(duration: 0.5, dampingRatio: 0.5) { [weak self] in
            self?.ratePanel.alpha = 1
        }
        animator.startAnimation()
    }
    
    @IBAction private func addRateOnCLick(_ sender: Button) {
        func hideRatePanel() {
            let animator = UIViewPropertyAnimator(duration: 0.5, dampingRatio: 0.5) { [weak self] in
                self?.ratePanel.alpha = 0
            }
            animator.addCompletion { [weak self] (position) in
                self?.ratePanel.isHidden = true
            }
            animator.startAnimation()
        }
        if (Gemo.gem.isConnected) {
            print("rate value \(rate)")
            let paramters: [string: any] = [
                Constants.Misc.MainKey: Constants.Misc.MainValue,
                "user_id": UserDefaults.standard.string(forKey: Constants.Misc.UserID) ?? .empty,
                "id_matjer": store?.id ?? .empty,
                "rate": rate
            ]
            UIApplication.showIndicator(by: true)
            HttpClient.request(link: Constants.Urls.RateStore, method: .post, parameters: paramters) { [weak self] (result, response, error) -> void in
                if (error != nil) {
                    UIApplication.showIndicator(by: false)
                    print("rate error: \(string(describing: error))")
                    return
                }
                UIApplication.showIndicator(by: false)
                print("request result: \(string(describing: result))")
                ThreadQueue.main { hideRatePanel() }
                self?.loadData()
            }

        } else {
            hideRatePanel()
        }
    }
    
    @IBAction private func setRateOnCLick(_ sender: UIButton) {
        reviews.forEach { $0.setImage(#imageLiteral(resourceName: "star-border"), for: .normal) }
        rate = sender.tag
        for i in stride(from: 0, to: rate, by: 1) {
            print("index: \(i)")
            reviews[i].setImage(#imageLiteral(resourceName: "star"), for: .normal)
        }
    }
    
    @objc fileprivate func ratePanelTapped(_ gesture: UITapGestureRecognizer) {
        /*let animator = UIViewPropertyAnimator(duration: 0.5, dampingRatio: 0.5) { [weak self] in
            self?.ratePanel.alpha = 0
        }
        animator.addCompletion { [weak self] (position) in
            self?.ratePanel.isHidden = true
        }
        animator.startAnimation()*/
    }
    
    
}


// MARK: - Helper functions 

extension StoreInfo
{
    
    fileprivate func setupRatePanel()-> void {
        view.addSubview(ratePanel)
        ratePanel.isHidden = true
        ratePanel.alpha = 0
        ratePanel.frame = UIScreen.main.bounds
        ratePanel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ratePanelTapped(_:))))
    }
    
    fileprivate func loadData()-> void {
        guard let storeId = store?.id else { return }
        let parameters: Dictionary<string, any> = [
            Constants.Misc.MainKey: Constants.Misc.MainValue,
            "user_id": storeId, // send shop id not user id but web develper is lazy 
            Constants.Misc.LanguageBackendKey: Localizer.instance.current.rawValue
        ]
        print("parameters \(parameters)")
        Shop.getData(from: Constants.Urls.Store, withParameters: parameters, self) { [weak self] (shops) in
            if let store = shops.first {
                print("fetched shop \(store)")
                ThreadQueue.main { [weak self] in
                    self?.lblStoreName.text = store.shopName ?? .empty
                    self?.lblStoreDetails.text = store.info ?? .empty
                    self?.lblPhone.text = store.phone ?? .empty
                    self?.lblTypes.text = store.productsTypes.joined(separator: " - ")
                    self?.icon.loadImage(from: store.photo ?? .empty)
                    self?.lblProductsCount.text = "\(store.productsCount?.toString ?? .empty) \(Localization.Product.localized())"
                    if let rate = NumberFormatter.formate.number(from: store.rate ?? "0")?.doubleValue, int(ceil(rate)) <= 5 {
                        print("rate is: \(ceil(rate))")
                        if (Localizer.instance.current == .arabic) {
                            self?.stars.reverse()
                        }
                        for i in stride(from: 0, to: int(ceil(rate)), by: 1) {
                            print("show rate i \(i)")
                            self?.stars[i].image = #imageLiteral(resourceName: "star")
                        }
                    }
                    let numberFormatter = NumberFormatter()
                    numberFormatter.locale = Locale(identifier: "en")
                    if let latitude = numberFormatter.number(from: store.latitude ?? .empty)?.doubleValue, let longitude = numberFormatter.number(from: store.longitude ?? .empty)?.doubleValue {
                        GMSGeocoder().reverseGeocodeCoordinate(CLLocationCoordinate2D(latitude: latitude, longitude: longitude), completionHandler: { [weak self] (response, error) in
                            if (error != nil) {
                                print("geocoding error is: \(string(describing: error))")
                                return
                            }
                            if let address = response?.firstResult() {
                                print(address)
                                self?.lblLocation.text = address.lines?.joined(separator: ", ")
                            }
                        })
                    } else {
                        print("error when geocodeing")
                    }
                    
                }
            }
        }
    }
    
    fileprivate func updateUi()-> void {
        lblRateTitle.text = Localization.Rate.localized()
        lblPhoneTitle.text = Localization.PhoneNumber.localized()
        lblProductsCountTitle.text = Localization.ProductsCount.localized()
        lblTypesTitle.text = Localization.Types.localized()
        lblLocationTitle.text = Localization.Location.localized()
        btnShowRatePanel.setTitle(Localization.AddRate.localized(), for: .normal)
        btnAddRate.setTitle(Localization.AddRate.localized(), for: .normal)
        lblRatePanelTitle.text = Localization.AddRate.localized()
        btnSendComplaint.setTitle(Localization.SendComplaint.localized(), for: .normal)
        if (Localizer.instance.current == .arabic) {
            lblStoreName.textAlignment = .right
            lblStoreDetails.textAlignment = .right
            lblPhoneTitle.textAlignment = .right
            lblPhone.textAlignment = .right
            lblLocation.textAlignment = .right
            lblLocationTitle.textAlignment = .right
            lblTypesTitle.textAlignment = .right
            lblTypes.textAlignment = .right
            lblProductsCountTitle.textAlignment = .right
            lblProductsCount.textAlignment = .right
            lblRateTitle.textAlignment = .right
            btnSendComplaint.contentHorizontalAlignment = .right
            icon.rightAnchor.constraint(equalTo: headerPanel.rightAnchor, constant: 0).isActive = true
            headerLabelsPanel.rightAnchor.constraint(equalTo: icon.leftAnchor, constant: -8).isActive = true
            headerLabelsPanel.leftAnchor.constraint(equalTo: headerPanel.leftAnchor, constant: 0).isActive = true
            mainStack.rightAnchor.constraint(equalTo: contentPanel.rightAnchor, constant: -20).isActive = true
            mainStack.leftAnchor.constraint(equalTo: contentPanel.leftAnchor, constant: 8).isActive = true
            sideStack.rightAnchor.constraint(equalTo: contentPanel.rightAnchor, constant: -8).isActive = true
            btnShowRatePanel.leftAnchor.constraint(equalTo: reviewComponents.leftAnchor, constant: 8).isActive = true
            starsStack.rightAnchor.constraint(equalTo: reviewComponents.rightAnchor, constant: 0).isActive = true
        } else {
            lblStoreName.textAlignment = .left
            lblStoreDetails.textAlignment = .left
            lblPhoneTitle.textAlignment = .left
            lblPhone.textAlignment = .left
            lblLocation.textAlignment = .left
            lblLocationTitle.textAlignment = .left
            lblTypesTitle.textAlignment = .left
            lblTypes.textAlignment = .left
            lblProductsCountTitle.textAlignment = .left
            lblProductsCount.textAlignment = .left
            lblRateTitle.textAlignment = .left
            btnSendComplaint.contentHorizontalAlignment = .left
            icon.leftAnchor.constraint(equalTo: headerPanel.leftAnchor, constant: 0).isActive = true
            headerLabelsPanel.rightAnchor.constraint(equalTo: headerPanel.rightAnchor, constant: 0).isActive = true
            headerLabelsPanel.leftAnchor.constraint(equalTo: icon.rightAnchor, constant: 8).isActive = true
            mainStack.rightAnchor.constraint(equalTo: contentPanel.rightAnchor, constant: -8).isActive = true
            mainStack.leftAnchor.constraint(equalTo: contentPanel.leftAnchor, constant: 20).isActive = true
            sideStack.leftAnchor.constraint(equalTo: contentPanel.leftAnchor, constant: 8).isActive = true
            btnShowRatePanel.rightAnchor.constraint(equalTo: reviewComponents.rightAnchor, constant: -8).isActive = true
            starsStack.leftAnchor.constraint(equalTo: reviewComponents.leftAnchor, constant: 0).isActive = true
        }
    }
    
}






















