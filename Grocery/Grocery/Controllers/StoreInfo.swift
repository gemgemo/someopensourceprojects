
import UIKit
import Gemo
import GoogleMaps
import GooglePlaces
import MobileCoreServices
import Alamofire

final class StoreInfo: BaseController
{
    
    // MARK: - Constants
    
    fileprivate let rightCellId = "com.right.cell.id.io", leftCellId = "com.left.cell.id.io"
    
    // MARK: - Variables
    
    internal var isUpdate = false, grocery: Grocery?, password: string?
                fileprivate var selectedImage = UIImage(),
                annotation: GMSMarker?,
                location: CLLocationCoordinate2D?,
                bottomMenuPanelBottomMargin = NSLayoutConstraint()
    fileprivate var typesDataSource = Array<Section>(), selectedSections = [Section]()
    
    fileprivate lazy var overlayPanel: UIImageView = { [weak self] in
        guard let this = self else { return UIImageView() }
        let overlay = UIImageView()
        overlay.backgroundColor = Color.black.withAlphaComponent(0.5)
        let tapGesture = UITapGestureRecognizer(target: this, action: #selector(bottomPanalTapped(_:)))
        overlay.addGestureRecognizer(tapGesture)
        overlay.isUserInteractionEnabled = true
        return overlay
        }()
    
    fileprivate lazy var bottomMenuContainerPanel: UIView = {
        let panal = UIView()
        panal.translatesAutoresizingMaskIntoConstraints = false
        panal.backgroundColor = .white
        panal.isUserInteractionEnabled = true
        panal.setShadow(with: .zero, radius: 0.6, opacity: 0.6, color: .darkGray)
        return panal
    }()
    
    fileprivate lazy var headerPanel: UIView = {
        let panal = UIView()
        panal.translatesAutoresizingMaskIntoConstraints = false
        panal.setBorder(with: 1, and: Color(white: 0.88, alpha: 0.9))
        panal.backgroundColor = .white
        return panal
    }()
    
    fileprivate lazy var lblHeaderTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .right
        label.text = Localization.Types.localized()
        label.font = Font.boldSystemFont(ofSize: 11)
        return label
    }()
    
    fileprivate lazy var btnClose: UIButton = { [weak self] in
        guard let this = self else { return UIButton() }
        let btn = UIButton(type: .custom)
        btn.setImage(#imageLiteral(resourceName: "cancel"), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(this, action: #selector(closeBottomPanalOnCLick(_:)), for: .touchUpInside)
        return btn
        }()
    
    fileprivate var footerPanel: UIView = {
        let panel = UIView()
        panel.backgroundColor = #colorLiteral(red: 0.3008661568, green: 0.4334716499, blue: 0.5458672643, alpha: 1)
        panel.setShadow(with: .zero, radius: 0.8, opacity: 0.9, color: #colorLiteral(red: 0.3008661568, green: 0.4334716499, blue: 0.5458672643, alpha: 1))
        panel.translatesAutoresizingMaskIntoConstraints = false
        return panel
    }()
    
    fileprivate lazy var btnSubmit: UIButton = { [weak self] in
        guard let this = self else { return UIButton() }
        let btn = UIButton(type: .custom)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(this, action: #selector(submitOnCLick(_:)), for: .touchUpInside)
        if (Localizer.instance.current == .arabic) {
            btn.semanticContentAttribute = .forceRightToLeft
            btn.imageEdgeInsets = Insets(top: 0, left: 20, bottom: 0, right: 0)
        } else {
            btn.semanticContentAttribute = .forceLeftToRight
            btn.imageEdgeInsets = Insets(top: 0, left: -20, bottom: 0, right: 0)
        }
        btn.setImage(#imageLiteral(resourceName: "ic-true"), for: .normal)
        btn.setTitle(Localization.Accept.localized(), for: .normal)
        return btn
        }()
    
    fileprivate lazy var tblTypes: UITableView = { [weak self] in
        guard let this = self else { return UITableView() }
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.alwaysBounceVertical = true
        tableView.backgroundColor = .white
        tableView.register(TypesCell.self, forCellReuseIdentifier: this.leftCellId)
        return tableView
        }()
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var storeIconPanel: View!
    @IBOutlet fileprivate weak var storeIcon: UIImageView!
    @IBOutlet fileprivate weak var txtStoreDescription: TextView!
    @IBOutlet fileprivate weak var lblTypes: UILabel!
    @IBOutlet fileprivate weak var btnAddTypes: UIButton!
    @IBOutlet fileprivate weak var btnSearchOnMap: UIButton!
    @IBOutlet fileprivate weak var mapView: GMSMapView! { didSet {
        mapView.mapType = .terrain
        } }
    @IBOutlet fileprivate weak var btnSignup: UIButton!
    @IBOutlet fileprivate weak var typesPanel: View!
    @IBOutlet fileprivate weak var searchMapPanel: UIView!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        txtStoreDescription.placeholder = Localization.DescriptionForStore.localized()
        fillBottomMenu()
        btnSearchOnMap.setTitle(Localization.SearchMap.localized(), for: .normal)
        guard let latitude = grocery?.latitude?.toDouble, let longitude = grocery?.longitude?.toDouble else { return }
        setMarker(with: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
        guard let coordinate = location else { return }
        address(for: coordinate) { [weak self] (address) in
            self?.btnSearchOnMap.setTitle(address, for: .normal)
        }
        fillStoreWithSavedDate()
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    // MARK: - Actions
    
    @IBAction private func setStoreIconOnClick(_ sender: UIButton) {
        openImagePicker(with: self)
    }
    
    @IBAction private func addTypesOnClick(_ sender: UIButton) {
        openBottomPanal()
    }
    
    @IBAction private func mapSearchingOnClick(_ sender: UIButton) {
        let autoCompletePage = GMSAutocompleteViewController()
        autoCompletePage.delegate = self
        autoCompletePage.tintColor = #colorLiteral(red: 0.3008661568, green: 0.4334716499, blue: 0.5458672643, alpha: 1)
        present(autoCompletePage, animated: true)
    }
    
    @IBAction private func signupOnCLick(_ sender: UIButton) {
        if (Gemo.gem.isConnected) {
            guard !storeDetails.isEmpty else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(),
                                   message: Localization.DescriptionReqiured.localized())
                return
            }
            guard location != nil else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(),
                                   message: Localization.LocationReqiured.localized())
                return
            }
            guard storeIcon.image != nil else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(),
                                   message: Localization.ImageReqiured.localized())
                return
            }
            (isUpdate) ? updateStore() : registerNewStore()
        } else {
            dialogUserWithNoInternetConnectionMessage()
        }
    }
    
    @objc fileprivate func bottomPanalTapped(_ gesture: UITapGestureRecognizer) {
        closeBottomPanal()
    }
    
    @objc private func closeBottomPanalOnCLick(_ sender: UIButton) {
        closeBottomPanal()
    }
    
    @objc private func submitOnCLick(_ sender: UIButton) {
        closeBottomPanal()
    }
    
}


// MARK: - Helper functions

extension StoreInfo
{
    
    fileprivate func fillStoreWithSavedDate()-> void {
        if (isUpdate) {
            let db = UserDefaults.standard
            print("grocery: \(string(describing: grocery))", "is update value = \(isUpdate)")
            storeIcon.loadImage(from: db.string(forKey: Misc.GroceryAvatar) ?? .empty)
            txtStoreDescription.text = db.string(forKey: Misc.GroceryDescription) ?? .empty
            let latitude = (db.string(forKey: Misc.GroceryLatitude) ?? "0.0").toDouble
            let longitude = (db.string(forKey: Misc.GroceryLongitude) ?? "0.0").toDouble
            let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            setMarker(with: coordinate)
        }
    }
    
    fileprivate func allSection()-> Section {
        let allSection = Section()
        allSection.id = "allData"
        allSection.title = Localization.All.localized()
        return allSection
    }
    
    fileprivate func fillBottomMenu()-> void {
        setupBottomMenu()
        typesDataSource.append(allSection())
        let parameters: Dictionary<string, any> = [
            Misc.MainKey: Misc.MainValue,
            Misc.LanguageParameterName: Localizer.instance.current.rawValue
        ]
        Section.getSections(for: Urls.Sections, with: parameters) { [weak self] (sections) in
            guard let me = self else { return }
            if (!sections.isEmpty) {
                me.typesDataSource += sections
                //me.selectedSections = me.typesDataSource
                Threads.main { [weak self] in
                    guard let this = self else { return }
                    this.tblTypes.reloadData()
                }
            }
        }
    }
    
    internal override func updateUi()-> void {
        super.updateUi()
        tabBar.isHidden = true
        searchMapPanel.setCorner(radius: 0.5)
        searchMapPanel.setShadow(with: .zero, radius: 3.0, opacity: 0.4, color: .gray)
        btnSignup.setCorner(radius: 20.0)
        btnSignup.setShadow(with: .zero, radius: 0.5, opacity: 0.9, color: #colorLiteral(red: 0.3008661568, green: 0.4334716499, blue: 0.5458672643, alpha: 1))
        navigationItem.title = Localization.StoreDescription.localized()
        lblTypes.text = Localization.ProductsTypes.localized()
        btnSignup.setTitle((isUpdate) ? Localization.Update.localized() : Localization.Register.localized(), for: .normal)
        if (Localizer.instance.current == .arabic) {
            txtStoreDescription.textAlignment = .right
            lblTypes.textAlignment = .right
            btnSearchOnMap.contentHorizontalAlignment = .right
            btnAddTypes.leftAnchor.constraint(equalTo: typesPanel.leftAnchor, constant: 8).isActive = true
            lblTypes.rightAnchor.constraint(equalTo: typesPanel.rightAnchor, constant: -8).isActive = true
            lblTypes.leftAnchor.constraint(equalTo: btnAddTypes.rightAnchor, constant: 8).isActive = true
        } else {
            txtStoreDescription.textAlignment = .left
            lblTypes.textAlignment = .left
            btnSearchOnMap.contentHorizontalAlignment = .left
            btnAddTypes.rightAnchor.constraint(equalTo: typesPanel.rightAnchor, constant: -8).isActive = true
            lblTypes.rightAnchor.constraint(equalTo: btnAddTypes.leftAnchor, constant: -8).isActive = true
            lblTypes.leftAnchor.constraint(equalTo: typesPanel.leftAnchor, constant: 8).isActive = true
        }
        
    }
    
    fileprivate func setMarker(with coordinate: CLLocationCoordinate2D)-> void {
        annotation?.map = nil
        annotation = GMSMarker()
        annotation?.map = mapView
        annotation?.position = coordinate
        annotation?.icon = #imageLiteral(resourceName: "marker-store")
        mapView.camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: 16.0)
        annotation?.appearAnimation = .pop
        location = coordinate
    }
    
    
    fileprivate func setupBottomMenu()-> void {
        guard let window = UIApplication.shared.keyWindow else { return }
        overlayPanel.frame = window.frame
        overlayPanel.alpha = 0.0
        overlayPanel.isHidden = true
        let height = window.frame.height*0.70
        window.addSubview(overlayPanel)
        window.addSubview(bottomMenuContainerPanel)
        bottomMenuContainerPanel.addSubview(headerPanel)
        headerPanel.addSubview(lblHeaderTitle)
        headerPanel.addSubview(btnClose)
        footerPanel.addSubview(btnSubmit)
        bottomMenuContainerPanel.addSubview(tblTypes)
        bottomMenuContainerPanel.addSubview(footerPanel)
        // sorting panel constraints -> h,b,r,l
        bottomMenuContainerPanel.heightAnchor.constraint(equalToConstant: height).isActive = true
        bottomMenuContainerPanel.rightAnchor.constraint(equalTo: window.rightAnchor).isActive = true
        bottomMenuPanelBottomMargin = bottomMenuContainerPanel.bottomAnchor.constraint(equalTo: window.bottomAnchor, constant: height)
        bottomMenuPanelBottomMargin.isActive = true
        bottomMenuContainerPanel.leftAnchor.constraint(equalTo: window.leftAnchor).isActive = true
        // header panal constaraints-> h,t,l,r
        headerPanel.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        headerPanel.topAnchor.constraint(equalTo: bottomMenuContainerPanel.topAnchor).isActive = true
        headerPanel.rightAnchor.constraint(equalTo: bottomMenuContainerPanel.rightAnchor).isActive = true
        headerPanel.leftAnchor.constraint(equalTo: bottomMenuContainerPanel.leftAnchor).isActive = true
        // header label constraints-> cy, r, l
        lblHeaderTitle.centerYAnchor.constraint(equalTo: headerPanel.centerYAnchor).isActive = true
        // close  button constraints-> cy, l
        btnClose.centerYAnchor.constraint(equalTo: headerPanel.centerYAnchor, constant: 0.0).isActive = true
        if (Localizer.instance.current == .arabic) {
            lblHeaderTitle.textAlignment = .right
            btnClose.leftAnchor.constraint(equalTo: headerPanel.leftAnchor, constant: 8.0).isActive = true
            lblHeaderTitle.rightAnchor.constraint(equalTo: headerPanel.rightAnchor, constant: -8.0).isActive = true
        } else {
            lblHeaderTitle.textAlignment = .left
            lblHeaderTitle.leftAnchor.constraint(equalTo: headerPanel.leftAnchor, constant: 8.0).isActive = true
            btnClose.rightAnchor.constraint(equalTo: headerPanel.rightAnchor, constant: -8.0).isActive = true
        }
        // footer panel constraints-> r,b,h,l
        footerPanel.rightAnchor.constraint(equalTo: bottomMenuContainerPanel.rightAnchor).isActive = true
        footerPanel.bottomAnchor.constraint(equalTo: bottomMenuContainerPanel.bottomAnchor).isActive = true
        footerPanel.leftAnchor.constraint(equalTo: bottomMenuContainerPanel.leftAnchor).isActive = true
        footerPanel.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        // submit button constraints-> t,r,b,l
        btnSubmit.topAnchor.constraint(equalTo: footerPanel.topAnchor).isActive = true
        btnSubmit.rightAnchor.constraint(equalTo: footerPanel.rightAnchor).isActive = true
        btnSubmit.bottomAnchor.constraint(equalTo: footerPanel.bottomAnchor).isActive = true
        btnSubmit.leftAnchor.constraint(equalTo: footerPanel.leftAnchor).isActive = true
        // left panel constraints-> t,r,b,l
        tblTypes.topAnchor.constraint(equalTo: headerPanel.bottomAnchor, constant: 0.0).isActive = true
        tblTypes.bottomAnchor.constraint(equalTo: footerPanel.topAnchor, constant: 0.0).isActive = true
        tblTypes.rightAnchor.constraint(equalTo: bottomMenuContainerPanel.rightAnchor).isActive = true
        tblTypes.leftAnchor.constraint(equalTo: bottomMenuContainerPanel.leftAnchor, constant: 0.0).isActive = true
    }
    
    
    fileprivate func openBottomPanal()-> void {
        overlayPanel.isHidden = false
        bottomMenuPanelBottomMargin.constant = 0
        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseInOut, animations: {
            [weak self] in
            UIApplication.shared.keyWindow?.layoutIfNeeded()
            self?.overlayPanel.alpha = 1.0
        })
    }
    
    fileprivate func closeBottomPanal()-> void {
        bottomMenuPanelBottomMargin.constant = UIApplication.shared.keyWindow!.frame.height*70
        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseInOut, animations: {
            [weak self] in
            UIApplication.shared.keyWindow?.layoutIfNeeded()
            self?.overlayPanel.alpha = 0.0
        }) {  [weak self] (hasFinished) in
            self?.overlayPanel.isHidden = true
        }
    }
    
    fileprivate func registerNewStore()-> void {
        Gemo.gem.startSpinning()
        Alamofire.upload(multipartFormData: { [weak self] (multipart) in
            guard let this = self else { return }
            guard let image = this.storeIcon.image, let imageData = UIImageJPEGRepresentation(image, 0.40) else { return }
            multipart.append(Misc.MainValue.toData, withName: Misc.MainKey)
            multipart.append(imageData, withName: "photo_users", fileName: "\(NSUUID().uuidString)\(Date()).png", mimeType: "image/png")
            multipart.append((this.grocery?.name ?? .empty).toData, withName: "matjer_name")
            multipart.append((this.grocery?.username ?? .empty).toData, withName: "username")
            multipart.append((this.grocery?.email ?? .empty).toData, withName: "email")
            multipart.append((this.grocery?.phone ?? .empty).toData, withName: "phone")
            multipart.append((this.password ?? .empty).toData, withName: "password")
            multipart.append((this.password ?? .empty).toData, withName: "password_again")
            multipart.append(this.storeDetails.toData, withName: "description")
            multipart.append((this.location?.latitude ?? 0).toString.toData, withName: "lati")
            multipart.append((this.location?.longitude ?? 0).toString.toData, withName: "longi")
            if let sp = try? JSONSerialization.data(withJSONObject: this.selectedSections.map { $0.id ?? .empty }) {
                multipart.append(sp, withName: "spec")
            }
            print("data multiparted")
        }, to: Urls.Register) { (result) in
            switch(result) {
            case .failure(let error):
                print("register error is: \(string(describing: error))")
                Gemo.gem.stopSpinning()
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error.localizedDescription)
            case .success(request: let data, streamingFromDisk: _, streamFileURL: _):
                data.responseJSON(completionHandler: { (response) in
                    let error = response.error
                    if (error != nil) {
                        print("register response error: \(string(describing: error))")
                        Gemo.gem.stopSpinning()
                        Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription)
                        return
                    }
                    Gemo.gem.stopSpinning()
                    print("register response is: \(string(describing: response.result.value))")
                    guard let json = (response.result.value as? [Dictionary<string, any>])?.first, let success = json["result"] as? string else {
                        Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized())
                        return
                    }
                    if (success == "true") {
                        Threads.main { [weak self] in
                            self?.navigationController?.popToRootViewController(animated: true)
                        }
                    } else {
                        let arabicMessage = json["data"] as? string
                        let englishMessage = json["data_en"] as? string
                        Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: ((Localizer.instance.current == .arabic) ? arabicMessage : englishMessage) ?? .empty)
                    }
                })
            }
        }
    }
    
    fileprivate func updateStore()-> void {
        Gemo.gem.startSpinning()
        Alamofire.upload(multipartFormData: { [weak self] (multipart) in
            guard let this = self else { return }
            guard let image = this.storeIcon.image, let imageData = UIImageJPEGRepresentation(image, 0.40) else { return }
            multipart.append(Misc.MainValue.toData, withName: Misc.MainKey)
            multipart.append(imageData, withName: "photo_users", fileName: "\(NSUUID().uuidString)\(Date()).png", mimeType: "image/png")
            multipart.append((UserDefaults.standard.string(forKey: Misc.GroceryId) ?? .empty).toData, withName: "user_id")
            multipart.append((this.grocery?.name ?? .empty).toData, withName: "matjer_name")
            multipart.append((this.grocery?.username ?? .empty).toData, withName: "username")
            multipart.append((this.grocery?.email ?? .empty).toData, withName: "email")
            multipart.append((this.grocery?.phone ?? .empty).toData, withName: "phone")
            multipart.append((this.password ?? .empty).toData, withName: "password")
            multipart.append(this.storeDetails.toData, withName: "description")
            multipart.append((this.location?.latitude ?? 0).toString.toData, withName: "lati")
            multipart.append((this.location?.longitude ?? 0).toString.toData, withName: "longi")
            if let sp = try? JSONSerialization.data(withJSONObject: this.selectedSections.map { $0.id ?? .empty }) {
                multipart.append(sp, withName: "spec")
            }
            print("data multiparted")
        }, to: Urls.UpdateStore) { (result) in
            switch(result) {
            case .failure(let error):
                print("register error is: \(string(describing: error))")
                Gemo.gem.stopSpinning()
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error.localizedDescription)
            case .success(request: let data, streamingFromDisk: _, streamFileURL: _):
                data.responseJSON(completionHandler: { (response) in
                    let error = response.error
                    if (error != nil) {
                        print("register response error: \(string(describing: error))")
                        Gemo.gem.stopSpinning()
                        Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription)
                        return
                    }
                    Gemo.gem.stopSpinning()
                    print("register response is: \(string(describing: response.result.value))")
                    guard let json = (response.result.value as? [Dictionary<string, any>])?.first,
                        let success = json["result"] as? string else {
                            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized())
                            return
                    }
                    if (success == "true") {
                        Threads.main { [weak self] in
                            let store = Grocery(JSON: json)
                            self?.save(this: store)
                            self?.navigationController?.popToRootViewController(animated: true)
                        }
                    } else {
                        let arabicMessage = json["data"] as? string
                        let englishMessage = json["data_en"] as? string
                        Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: ((Localizer.instance.current == .arabic) ? arabicMessage : englishMessage) ?? .empty)
                    }
                })
            }
        }
        
    }
}


// MARK: - Image picker delegate functions

extension StoreInfo: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            selectedImage = editedImage
        } else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            selectedImage = image
        }
        storeIcon.image = selectedImage
        picker.dismiss(animated: true)
    }
    
    internal func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
}


// MARK: - Auto complete delegate functions

extension StoreInfo: GMSAutocompleteViewControllerDelegate
{
    
    internal func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        btnSearchOnMap.setTitle(place.formattedAddress ?? .empty, for: .normal)
        location = place.coordinate
        setMarker(with: place.coordinate)
        dismiss(animated: true)
    }
    
    internal func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("auto complete error is \(string(describing: error))")
    }
    
    internal func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.showIndicator(by: true)
    }
    
    internal func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.showIndicator(by: false)
    }
    
    internal func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true)
    }
    
}



// MARK: - Table view delegate & data source functions

extension StoreInfo: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return typesDataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: leftCellId, for: indexPath) as? TypesCell else { return UITableViewCell() }
        let section = typesDataSource[indexPath.row]
        cell.section = section
        let isFound = selectedSections.contains { $0.id == section.id }
        if (isFound) {
            cell.checkBox.isChecked = true
            cell.lblTitle.textColor = #colorLiteral(red: 0.3008661568, green: 0.4334716499, blue: 0.5458672643, alpha: 1)
        } else {
            cell.checkBox.isChecked = false
            cell.lblTitle.textColor = .black
        }
        return cell
        
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = typesDataSource[indexPath.row]
        if (section.id == "allData" && selectedSections.contains { $0.id == "allData" }) {
            selectedSections.removeAll()
            tableView.reloadData()
        } else {
            // checked sections
            if (section.id == "allData" && !selectedSections.contains { $0.id == "allData" } ) {
                selectedSections.removeAll()
                selectedSections = typesDataSource
            } else {
                let isFound = selectedSections.contains { $0.id == section.id }
                if (isFound && selectedSections.contains { $0.id == "allData" }) { // remove from array
                    selectedSections.removeAll()
                    tableView.reloadData()
                    selectedSections.append(section)
                } else if (isFound) {
                    guard let index = selectedSections.index(of: section) else { return }
                    selectedSections.remove(at: index)
                } else {
                    // add in array
                    selectedSections.append(section)
                }
            }
        }
        for (index, section) in typesDataSource.enumerated() {
            if let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? TypesCell {
                if (selectedSections.contains(section)) {
                    cell.checkBox.isChecked = true
                    cell.lblTitle.textColor = #colorLiteral(red: 0.3008661568, green: 0.4334716499, blue: 0.5458672643, alpha: 1)
                } else {
                    cell.checkBox.isChecked = false
                    cell.lblTitle.textColor = .black
                }
            }
        }
        
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    internal func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let typeCell = cell as? TypesCell else { return }        
        if selectedSections.contains(where: { $0.id == "allData" }) {
            typeCell.checkBox.isChecked = true
            typeCell.checkBox.selectedColor = #colorLiteral(red: 0.3008661568, green: 0.4334716499, blue: 0.5458672643, alpha: 1)
            typeCell.lblTitle.textColor = #colorLiteral(red: 0.3008661568, green: 0.4334716499, blue: 0.5458672643, alpha: 1)
        }
    }
    
}


// MARK: - Right table view cell

// MARK: - Left table view cell

fileprivate final class TypesCell: BaseTableCell
{
    
    // MARK: - Variables
    
    internal var title: string? { didSet { updateUi() } }
    internal var section: Section? { didSet { updateUi() } }
    private var mainPanel: UIView = {
        let panel = UIView()
        panel.translatesAutoresizingMaskIntoConstraints = false
        panel.backgroundColor = .white
        return panel
    }()
    
    internal var lblTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.font = Font.systemFont(ofSize: 14.0)
        return label
    }()
    
    fileprivate var checkBox: CheckBox = {
        let chkBox = CheckBox()
        chkBox.translatesAutoresizingMaskIntoConstraints = false
        chkBox.image = #imageLiteral(resourceName: "ic-true")
        chkBox.selectedColor = #colorLiteral(red: 0.3008661568, green: 0.4334716499, blue: 0.5458672643, alpha: 1)
        return chkBox
    }()
    
    // MARK: - Overridden functions
    
    /*internal override func setSelected(_ selected: Bool, animated: Bool) {
     super.setSelected(selected, animated: animated)
     checkBox.isChecked = selected
     lblTitle.textColor = (selected) ? #colorLiteral(red: 0.3008661568, green: 0.4334716499, blue: 0.5458672643, alpha: 1) : .black
     }*/
    
    internal override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        setupViews()
    }
    
    internal required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    internal override func updateUi() {
        mainPanel.addSubview(checkBox)
        if (Localizer.instance.current == .arabic) {
            lblTitle.leftAnchor.constraint(equalTo: checkBox.rightAnchor).isActive = true
            checkBox.leftAnchor.constraint(equalTo: mainPanel.leftAnchor, constant: 8.0).isActive = true
        } else {
            lblTitle.rightAnchor.constraint(equalTo: checkBox.leftAnchor).isActive = true
            checkBox.rightAnchor.constraint(equalTo: mainPanel.rightAnchor, constant: -8.0).isActive = true
        }
        checkBox.heightAnchor.constraint(equalToConstant: 16.0).isActive = true
        checkBox.widthAnchor.constraint(equalToConstant: 16.0).isActive = true
        checkBox.centerYAnchor.constraint(equalTo: mainPanel.centerYAnchor).isActive = true
        lblTitle.text = section?.title ?? .empty
        
    }
    
    
    // MARK: - Functions
    
    private func setupViews()-> void {
        addSubview(mainPanel)
        mainPanel.addSubview(lblTitle)
        // main panel constraints-> t,r,b,l
        if (Localizer.instance.current == .arabic) {
            lblTitle.textAlignment = .right
            lblTitle.rightAnchor.constraint(equalTo: mainPanel.rightAnchor, constant: -16.0).isActive = true
        } else {
            lblTitle.textAlignment = .left
            lblTitle.leftAnchor.constraint(equalTo: mainPanel.leftAnchor, constant: 16.0).isActive = true
        }
        mainPanel.topAnchor.constraint(equalTo: topAnchor).isActive = true
        mainPanel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        mainPanel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        mainPanel.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        // title label constraints -> t,r,b,l
        lblTitle.topAnchor.constraint(equalTo: mainPanel.topAnchor).isActive = true
        lblTitle.bottomAnchor.constraint(equalTo: mainPanel.bottomAnchor).isActive = true
    }
    
    
}



// MARK: - Computed properties

extension StoreInfo
{
    
    fileprivate var storeDetails: string {
        return txtStoreDescription.text.trimmingCharacters(in: .whitespaces)
    }
    
}




























