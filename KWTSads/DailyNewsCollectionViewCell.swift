
import UIKit
import Alamofire

final class DailyNewsCollectionViewCell: UICollectionViewCell {
    
    internal var isExpired = false, id = ""
    
    @IBOutlet var dailyName: UILabel!
    @IBOutlet var dailyFDate: UILabel!
    @IBOutlet var dailyEDate: UILabel!
    @IBOutlet var dailyView: UILabel!
    @IBOutlet var dailyLikes: UILabel!
    @IBOutlet var dailyImg: UIImageView!
    @IBOutlet var dailyBottomLabel: UILabel!
    @IBOutlet var dailylikeImg: UIImageView!
    @IBOutlet var expireDateImg: UIImageView!
    @IBOutlet internal weak var lblLike: UILabel!
    @IBOutlet internal weak var lblDislike: UILabel!
    
    
    private func doLike(by state: Int)-> Void {
        print("id", id)
        let parameters: Parameters = [
            "adv_id": id,
            "device_id": UIDevice.current.identifierForVendor?.uuidString ?? "",
            "liked": state
        ]
        Alamofire.request(DoLike, method: .post, parameters: parameters)
            .responseJSON { [weak self] (response) in
                switch (response.result) {
                case .success(let result):
                    guard let json = result as? Dictionary<String, Any>,
                        let likes = json["data"] as? Dictionary<String, Any> else {
                            print("no likes")
                            return
                    }
                    print(json)
                    self?.lblDislike.text = likes["dislike_num"] as? String ?? ""
                    self?.lblLike.text = likes["likes_num"] as? String ?? ""
                case .failure(let error):
                    print("do like error: \(error)")
                }
        }
        
        
        

    
}

    @IBAction private func doLikeOnCLick(_ sender: UIButton) {
        doLike(by: 1)
    }

    @IBAction private func doDisLikeOnCLick(_ sender: UIButton) {
        doLike(by: 0)
    }



}

























