
import UIKit
import Localize_Swift

final class SchoolCell: UICollectionViewCell, CellProvider
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    
    internal var school: School? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var cover: UIImageView!
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var icon: UIImageView!
    @IBOutlet private weak var mainPanal: UIView! {
        didSet {
            mainPanal.setShadow(with: Size(right: 0, top: 1), radius: 1, opacity: 0.9, color: .darkGray)
        }
    }
    @IBOutlet private weak var footerStack: UIStackView!
    
    
    
    // MARK: - Overridden functions
    
    internal func updateUI() {
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            lblTitle.textAlignment = .right
            footerStack.semanticContentAttribute = .forceLeftToRight
        } else {
            lblTitle.textAlignment = .left
            footerStack.semanticContentAttribute = .forceRightToLeft
        }
        if let iconLink = school?.icon {
            icon.loadImage(from: iconLink, onComplete: nil)
        }
        if let coverLink = school?.cover {
            cover.loadImage(from: coverLink, onComplete: nil)
        }
        icon.round()
        lblTitle.text = school?.title ?? string.empty
    }
    
    
    //MARK: - Actions
    
}























