//
//  CallUsViewController.swift
//  KWTSads
//
//  Created by Mostafa on 8/14/16.
//  Copyright © 2016 Mostafa. All rights reserved.
//

import UIKit
import SwiftSpinner

class CallUsViewController: UIViewController ,UICollectionViewDelegate ,UICollectionViewDataSource{
    var callUsText = [String]()

    @IBOutlet var callUSView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        if Reachability.isConnectedToNetwork() == true {
            callUSView.delegate = self
            callUSView.dataSource = self
            SwiftSpinner.show("Loading...")
            self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "ac_back-arrow")
            self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "ac_back-arrow")
            self.navigationController?.navigationBar.tintColor = UIColor(red: 0.96, green: 0.69, blue:  0.25, alpha: 1 )
            
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage
            loadCallUs()

        }
        else
        {
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage
            
            let err = "Internet connection FAILED"
            self.displayAlertMessage(err)
        }
    }
    func loadCallUs()
    {
        let urlString = "\(baseUrl)detail.php?d=3"
        let session = URLSession.shared
        let url = URL(string: urlString)!
        session.dataTask(with: url, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
                if let responseData = data {
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments)as! [String:AnyObject]
                        
                        
                        let data = json["data"] as! NSArray
                        for i in 0..<data.count
                        {
                            if let dict = data[i] as? NSDictionary {
                                
                                self.callUsText.append(dict["detail_text"] as! String)
                            }
                        }
                        DispatchQueue.main.async
                            {
                                self.callUSView.reloadData()
                                SwiftSpinner.hide()
                                
                        }
                        
                        
                        
                    }
                    catch
                    {
                        print ("Could not Serialize")
                    }
                    
                    
                    
                    
                }
                
        }).resume()
    }
    func displayAlertMessage(_ userMessage:String)
    {
        let myAlert = UIAlertController(title: "Alert", message:userMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "callCell", for: indexPath) as! CallUsCollectionViewCell
        
        
        cell.callLabel.text = callUsText[(indexPath as NSIndexPath).row]
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return callUsText.count
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func departTapped(_ sender: AnyObject) {
        //        dismissViewControllerAnimated(true, completion: nil)
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "departmentPage") as! DepartmentsViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
        
    }
    
   
    @IBAction func MenuTapped(_ sender: AnyObject) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuPage") as! MenuPageViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dailyNew(_ sender: AnyObject) {
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "DailyNews") as! DailyNewsViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func favPage(_ sender: AnyObject) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "FavPage") as! FavPageViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
        
    }
}
