
import UIKit
import Localize_Swift
import CoreLocation
import Alamofire

final class DriverOrderInfo: BaseController
{
    // MARK: - Constants
    
    // MARK: - Variables
    
    private var parameters = Dictionary<string, any>()
    internal var weight: string?,
                 numberOf: string?,
                 selectedMainCar: DriverMainCar?,
                 selectedSubCar: SubCar?,
                 pricingWay: int?,
                 paymentWay: int?,
                 distance: double?,
                 fromTitle: string?,
                 toTitle: string?,
                 from: CLLocationCoordinate2D?,
                 to: CLLocationCoordinate2D?,
                 typeTime: int?,
                 date: string?,
                 driverCarType: string?
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var fristView: UIView!
    @IBOutlet fileprivate weak var secondView: UIView!
    @IBOutlet fileprivate weak var thirdView: UIView!
    @IBOutlet fileprivate weak var fourthView: UIView!
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var lblFromTitle: UILabel!
    @IBOutlet fileprivate weak var lblFrom: UILabel!
    @IBOutlet fileprivate weak var lblToTitle: UILabel!
    @IBOutlet fileprivate weak var lblTo: UILabel!
    @IBOutlet fileprivate weak var lblMissionTypeTitle: UILabel!
    @IBOutlet fileprivate weak var lblMission: UILabel!
    @IBOutlet fileprivate weak var lblBehemothTitle: UILabel!
    @IBOutlet fileprivate weak var lblBehemoth: UILabel!
    @IBOutlet fileprivate weak var lblWeightTitle: UILabel!
    @IBOutlet fileprivate weak var lblWeight: UILabel!
    @IBOutlet fileprivate weak var lblNumberTitle: UILabel!
    @IBOutlet fileprivate weak var lblNumber: UILabel!
    @IBOutlet fileprivate weak var lblDistanceTitle: UILabel!
    @IBOutlet fileprivate weak var lblDistance: UILabel!
    @IBOutlet fileprivate weak var btnOk: Button!
    @IBOutlet fileprivate weak var btnCancel: Button!
    @IBOutlet fileprivate weak var headerView: UIView!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(updateUi), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
        
        resendOrder = { [weak self] in
            guard let this = self else { return }
            print("resend order")
            this.senderOrderToDriver(with: this.parameters)
        }
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupViews()
        updateUi()
    }
    
    internal override func updateNavgationBar() {
        super.updateNavgationBar()
        navigationItem.backBarButtonItem = UIBarButtonItem()
        navigationItem.hidesBackButton = true
        navigationItem.leftBarButtonItem = UIBarButtonItem()
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ac_manu"), style: .plain, target: revealViewController(), action: #selector(revealViewController().rightRevealToggle(_:)))
        view.addGestureRecognizer(revealViewController().panGestureRecognizer())
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: - Actions
    
    @IBAction private func acceptOnClick(_ sender: Button) {
        guard let userId = UserDefaults.standard.value(forKey: Constants.Misc.UserId) else {
            showAlert(with: .empty, and: "reqiurefieldlan".localized())
            return
        }
        guard let fromLatitude = from?.latitude, let fromLongitude = from?.longitude else {
            showAlert(with: .empty, and: "reqiurefieldlan".localized())
            return
        }
        guard let toLatitude = to?.latitude, let toLongitude = to?.longitude else {
            showAlert(with: .empty, and: "reqiurefieldlan".localized())
            return
        }
        var timestamp = string.empty
        let typeOfTime = typeTime ?? 3
        if (typeOfTime == 0) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .short
            timestamp = dateFormatter.string(from: Date())
        } else if (typeOfTime == 1) {
            timestamp = date ?? .empty
        }
        guard let pricing = pricingWay else {
            showAlert(with: .empty, and: "reqiurefieldlan".localized())
            return
        }
        guard let kilo = distance else {
            showAlert(with: .empty, and: "reqiurefieldlan".localized())
            return
        }
        guard let subCarId = selectedSubCar?.id else {
            showAlert(with: .empty, and: "reqiurefieldlan".localized())
            return
        }
        let w = weight ?? "0", l = "0", cc = numberOf ?? "0"
        guard let pay = paymentWay else {
            showAlert(with: .empty, and: "reqiurefieldlan".localized())
            return
        }
        parameters = [
            "UserID" : userId,
            "slat" : fromLatitude,
            "slan" : fromLongitude,
            "elat" : toLatitude,
            "elan" : toLongitude,
            "date" : timestamp,
            "typetime" : typeOfTime,
            "SalaryType": pricing,
            "Kilo": kilo,
            "carType": subCarId,
            "weight": w,
            "Load": l,
            "countcar": cc,
            "paytype": pay
        ]
        //print("params", parameters)
        if (Gemo.gem.isConnected) {
            switch (driverCarType ?? .empty) {
            case "1":
                senderOrderToDriver(with: parameters)
                
            case "2":
                senderCompanyOrder(with: parameters)
                
            default:
                break
            }
        } else {
            showAlert(with: .empty, and: Constants.Localizer.InternetConnection.localized())
        }
        
    }
    
    @IBAction private func cancelOnClick(_ sender: Button) {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    
}


// MARK: - Helper functions

extension DriverOrderInfo
{

    fileprivate func setupViews()-> void {
        [fristView, secondView, thirdView, fourthView].forEach { (aview) in
            aview?.setShadow(with: .zero, radius: 2, opacity: 0.6, color: .black)
        }
    }
    
    fileprivate func senderOrderToDriver(with params: Dictionary<string, any>)-> void {
        startSpin()
        print("url: \(Constants.BackendURLs.CallDriver)")
        print("parameters: \(params)")
        Alamofire.request(Constants.BackendURLs.CallDriver, method: .post, parameters: params)
            .responseJSON { [weak self] (response) in
                guard let this = self else { return }
                let result = response.result
                if (result.error != nil) {
                    print("call driver error \(String(describing: result.error))")
                    this.showAlert(with: .empty, and: "erroralertlan".localized())
                    this.stopSpin()
                    return
                }
                if let value = result.value as? Dictionary<string, any>, let success = value["Success"] as? string {
                    print("call driver value: \(value)")
                    this.stopSpin()
                    if (success == "True") {
                        // show sucess dialog
                        this.showAramWestDialog(in: this.view, for: .success, with: "driversucessdialoglan".localized()) { [weak self] () -> void in
                            self?.closeAramWestAlert(after: 3) {
                                _ = self?.navigationController?.popToRootViewController(animated: true)
                            }
                        }
                    } else {
                        // show dialog to resend order
                        this.showAramWestDialog(in: this.view, for: .fail, with: .empty) { }
                    }
                } else {
                    this.stopSpin()
                    print("call driver empty value")
                    this.showAlert(with: .empty, and: "erroralertlan".localized())
                }
        }
        

    }
    
    fileprivate func senderCompanyOrder(with parameters: Dictionary<string, any>)-> void {
        startSpin()
       Alamofire.request(Constants.BackendURLs.CallCompany, method: .post, parameters: parameters)
            .responseJSON { [weak self] (response) in
                guard let this = self else { return }
                let result = response.result
                if (result.error != nil) {
                    print("call company error \(result.error)")
                    this.showAlert(with: .empty, and: "erroralertlan".localized())
                    this.stopSpin()
                    return
                }
                if let value = result.value as? Dictionary<string, any>, let success = value["Success"] as? string {
                    print("company value =>>>>>>>> \(value)")
                    if (success == "True") {
                        this.stopSpin()
                        // show sucess dialog
                        this.showAramWestDialog(in: this.view, for: .success, with: "driversucessdialoglan".localized()) { [weak self] () -> void in
                            self?.closeAramWestAlert(after: 3) {
                                _ = self?.navigationController?.popToRootViewController(animated: true)
                            }
                        }
                    } else {
                        // show dialog to resend order
                        this.showAramWestDialog(in: this.view, for: .fail, with: .empty) { }
                    }
                } else {
                    this.stopSpin()
                    print("call company empty value")
                    this.showAlert(with: .empty, and: "erroralertlan".localized())
                }
        }
    }
    
    @objc fileprivate func updateUi()-> void {
        lblTitle.text = "orderinfolan".localized()
        lblFromTitle.text = "labelfromlan".localized()
        lblToTitle.text = "labeltolan".localized()
        lblMissionTypeTitle.text = "missiontypelan".localized()
        lblBehemothTitle.text = "behemothlan".localized()
        lblWeightTitle.text = "\("driverweiaghtlan".localized()):"
        lblNumberTitle.text = "\("drivernumberofplacholderlan".localized()):"
        lblDistanceTitle.text = "distancelan".localized()
        btnOk.setTitle("okLan".localized(), for: .normal)
        btnCancel.setTitle("cancellan".localized(), for: .normal)
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            headerView.semanticContentAttribute = .forceRightToLeft
            fristView.semanticContentAttribute = .forceRightToLeft
            secondView.semanticContentAttribute = .forceRightToLeft
            thirdView.semanticContentAttribute = .forceRightToLeft
            fourthView.semanticContentAttribute = .forceRightToLeft
            lblTitle.textAlignment = .right
        } else {
            headerView.semanticContentAttribute = .forceLeftToRight
            fristView.semanticContentAttribute = .forceLeftToRight
            secondView.semanticContentAttribute = .forceLeftToRight
            thirdView.semanticContentAttribute = .forceLeftToRight
            fourthView.semanticContentAttribute = .forceLeftToRight
            lblTitle.textAlignment = .left
        }
        
        // TODO:- show data
        if let mainCar = selectedMainCar, let mainCarId = mainCar.id {
            if (mainCarId == "1") {
                thirdView.removeFromSuperview()
            } else {
                lblWeight.text = weight ?? .empty
                lblNumber.text = numberOf ?? .empty
            }
        }
        lblTo.text = toTitle?.components(separatedBy: ",").first ?? .empty
        lblFrom.text = fromTitle?.components(separatedBy: ",").first ?? .empty
        let missionType = ((Localize.currentLanguage() == Constants.Language.Arabic) ? selectedMainCar?.arabicName : selectedMainCar?.englishName) ?? .empty
//        print("selected driver main car", selectedMainCar?.id)
        lblMission.text = missionType
        lblDistance.text = "\(distance ?? Number.zero.doubleValue) \("kmlan".localized())"
    }
    
}




























