
import UIKit

final class ImagePreviewer: BaseController
{
    
    // MARK: - Constants
    
    // MARK: - Variables
    
    internal var info: InfoImage?
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var scrollView: UIScrollView! {
        didSet {
            scrollView.delegate = self
            scrollView.alwaysBounceHorizontal = true
            scrollView.alwaysBounceVertical = true
        }
    }
    @IBOutlet fileprivate weak var imageView: UIImageView!
    
    
    // MARK: - Overridden functions
    
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        updateNavigationBar()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureUi()
    }
    
    // MARK: - Actions
    
    @objc fileprivate func dismissOnCLick(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

}


// MARK: - Helper functions

extension ImagePreviewer
{
    
    fileprivate func updateNavigationBar()-> void {
        navigationItem.title = info?.title ?? .empty
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_close"), style: .plain, target: self, action: #selector(dismissOnCLick(_:)))
    }
    
    fileprivate func configureUi()-> void {
        imageView.image = info?.icon
        imageView.contentMode = .scaleAspectFit
        scrollView.minimumZoomScale = 1
        scrollView.maximumZoomScale = 6
        scrollView.contentSize = imageView.image?.size ?? .zero
    }
    
}



// MARK: - Scroll view delegate functions

extension ImagePreviewer: UIScrollViewDelegate
{
    
    internal func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
}














