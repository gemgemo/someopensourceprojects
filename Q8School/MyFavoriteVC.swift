//
//  MyFavoriteVC.swift
//  Q8School
//
//  Created by mac on 12/19/16.
//  Copyright © 2016 Atiaf. All rights reserved.
//

import UIKit

class MyFavoriteVC: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate{

    @IBOutlet weak var myAdsImg: UIImageView!
    @IBOutlet weak var offerLbl: UILabel!
    @IBOutlet weak var myAdsLbl: UILabel!
    @IBOutlet weak var addAdsLbl: UILabel!
    @IBOutlet weak var callUsLbl: UILabel!
    @IBOutlet weak var openMenu: UIBarButtonItem!
    @IBOutlet weak var favoriteCollection: UICollectionView!
    var dataFound: Bool!
    var teachers: Array<Teacher> = Array<Teacher>()
    var images_cache = [String:UIImage]()
    var myID:String!
    var types: String!


    override func viewDidLoad() {
        super.viewDidLoad()
        favoriteCollection.dataSource = self
        favoriteCollection.delegate = self
        myID = UserDefaults.standard.string(forKey: "users_ID")
        if revealViewController() != nil {
            
            revealViewController().rightViewRevealWidth = 235
            openMenu.target = revealViewController()
            openMenu.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        favorite(Settings.lang, myID)
        if Settings.lang == "ar"
        {
            callUsLbl.text = "اتصل بنا"
            addAdsLbl.text = "اضف اعلانك"
            myAdsLbl.text = "اعلاناتي"
            offerLbl.text = "العروض"

        }
        types = UserDefaults.standard.string(forKey: "userType")
        if types == "1"
        {
            if Settings.lang == "ar"
            {
                myAdsLbl.text = "المفضلة"
                myAdsImg.image = UIImage(named: "fav")
                
            }
            else
            {
                myAdsLbl.text = "Favorite"
                myAdsImg.image = UIImage(named: "fav")
                
                
            }
        }


        // Do any additional setup after loading the view.
    }
    func favorite(_ lang:String,_ userID: String)
    {
//        shadowView.isHidden = false
//        loadingView.isHidden = false
//        loadingActivity.startAnimating()
        let request = NSMutableURLRequest(url: URL(string:Settings.MyFavorite)!)
        request.httpMethod = "POST"
        let postString = "lang=\(lang)&user_id=\(userID)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
            
            if let responseData = data {
                
                do {
                    
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                    print("ddd\(json)")
                    self.dataFound = json["Success"] as? Bool
                    if self.dataFound!
                    {
                        
                        let data = json["Result"] as! [NSDictionary]
                        for i in data
                        {
                            
                            let id = i["id"] as! String
                            let teacherName = i["username"] as! String
                            let teacherImg = i["img"] as! String
                            let subjectName = i["subjectName"] as! String
                            let phase = i["scholarPhaseName"] as! String
                            let detial = i["details"] as! String
                            let date = i["date_time"] as! String
                            
                            
                            let teacher = Teacher(id: id, teacherName: teacherName, teacherImg: teacherImg, subjectName: subjectName, phase: phase, detial: detial, date: date)
                            self.teachers.append(teacher)
                            
                        }
                    }
                    DispatchQueue.main.async
                        {
//                            self.shadowView.isHidden = true
//                            self.loadingView.isHidden = true
//                            self.loadingActivity.stopAnimating()
                            self.favoriteCollection.reloadData()
                            
                    }
                    
                }
                catch
                {
                    print ("Could not Serialize")
                }
                
            }
            
        }
        task.resume()
        

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        let size = self.favoriteCollection.frame.width
        //        let height = self.teacherCollection.frame.height / 3
        
        return CGSize(width: size  - 10, height: 150)
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "favoriteCell", for: indexPath)
            as! MyFavorite
        if Settings.lang == "ar"
        {
            if self.teachers[indexPath.row].teacherImg != ""
            {
                cell.teacherImg.image = nil
                load_image(self.teachers[indexPath.row].teacherImg, imageview: cell.teacherImg)
                cell.teacherLbl.text = "الاستاذ:"
                cell.detialsLbl.text = "تفاصيل:"
                cell.subjectLbl.text = "\(self.teachers[indexPath.row].subjectName):"
                cell.phaseName.text = self.teachers[indexPath.row].phase
                cell.teacherName.text = self.teachers[indexPath.row].teacherName
                cell.detailLbl.text = self.teachers[indexPath.row].detial
                cell.teacherImg.layer.cornerRadius = 0.5 * cell.teacherImg.bounds.size.width
                cell.teacherImg.clipsToBounds = true
                cell.teacherImg.layer.borderWidth = 2
                cell.teacherImg.layer.borderColor = UIColor.white.cgColor
                cell.teacherView.layer.cornerRadius = 10
                cell.teacherView.layer.masksToBounds = true
                cell.teacherV2.layer.cornerRadius = 10
                cell.teacherV2.layer.masksToBounds = true
                let fullNameArr = self.teachers[indexPath.row].date.characters.split{$0 == " "}.map(String.init)
                cell.dateLbl.text = fullNameArr[0]
                cell.timeLbl.text = fullNameArr[1]
                cell.id = self.teachers[indexPath.row].id
                
                
            }
            else
            {
                cell.teacherImg.image = nil
                cell.teacherImg.image = UIImage(named: "pro_img")
                cell.teacherLbl.text = "الاستاذ:"
                cell.detialsLbl.text = "تفاصيل:"
                cell.subjectLbl.text = "\(self.teachers[indexPath.row].subjectName):"
                cell.phaseName.text = self.teachers[indexPath.row].phase
                cell.teacherName.text = self.teachers[indexPath.row].teacherName
                cell.detailLbl.text = self.teachers[indexPath.row].detial
                cell.teacherImg.layer.cornerRadius = 0.5 * cell.teacherImg.bounds.size.width
                cell.teacherImg.clipsToBounds = true
                cell.teacherImg.layer.borderWidth = 2
                cell.teacherImg.layer.borderColor = UIColor.white.cgColor
                cell.teacherView.layer.cornerRadius = 10
                cell.teacherView.layer.masksToBounds = true
                cell.teacherV2.layer.cornerRadius = 10
                cell.teacherV2.layer.masksToBounds = true
                let fullNameArr = self.teachers[indexPath.row].date.characters.split{$0 == " "}.map(String.init)
                cell.dateLbl.text = fullNameArr[0]
                cell.timeLbl.text = fullNameArr[1]

                cell.id = self.teachers[indexPath.row].id
                
                
                
            }
            
        }
        else
        {
            if self.teachers[indexPath.row].teacherImg != ""
            {
                cell.teacherImg.image = nil
                load_image(self.teachers[indexPath.row].teacherImg, imageview: cell.teacherImg)
                cell.subjectLbl.text = "\(self.teachers[indexPath.row].subjectName):"
                cell.phaseName.text = self.teachers[indexPath.row].phase
                cell.teacherName.text = self.teachers[indexPath.row].teacherName
                cell.detailLbl.text = self.teachers[indexPath.row].detial
                cell.teacherImg.layer.cornerRadius = 0.5 * cell.teacherImg.bounds.size.width
                cell.teacherImg.clipsToBounds = true
                cell.teacherImg.layer.borderWidth = 2
                cell.teacherImg.layer.borderColor = UIColor.white.cgColor
                cell.teacherView.layer.cornerRadius = 10
                cell.teacherView.layer.masksToBounds = true
                cell.teacherV2.layer.cornerRadius = 10
                cell.teacherV2.layer.masksToBounds = true
                let fullNameArr = self.teachers[indexPath.row].date.characters.split{$0 == " "}.map(String.init)
                cell.dateLbl.text = fullNameArr[0]
                cell.timeLbl.text = fullNameArr[1]
                cell.id = self.teachers[indexPath.row].id
                
                
            }
            else
            {
                cell.teacherImg.image = nil
                cell.teacherImg.image = UIImage(named: "pro_img")
                cell.subjectLbl.text = "\(self.teachers[indexPath.row].subjectName):"
                cell.phaseName.text = self.teachers[indexPath.row].phase
                cell.teacherName.text = self.teachers[indexPath.row].teacherName
                cell.detailLbl.text = self.teachers[indexPath.row].detial
                cell.teacherImg.layer.cornerRadius = 0.5 * cell.teacherImg.bounds.size.width
                cell.teacherImg.clipsToBounds = true
                cell.teacherImg.layer.borderWidth = 2
                cell.teacherImg.layer.borderColor = UIColor.white.cgColor
                cell.teacherView.layer.cornerRadius = 10
                cell.teacherView.layer.masksToBounds = true
                cell.teacherV2.layer.cornerRadius = 10
                cell.teacherV2.layer.masksToBounds = true
                let fullNameArr = self.teachers[indexPath.row].date.characters.split{$0 == " "}.map(String.init)
                cell.dateLbl.text = fullNameArr[0]
                cell.timeLbl.text = fullNameArr[1]
                
                cell.id = self.teachers[indexPath.row].id
                
                
                
            }
            

        }
        
        
        return cell
        
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.teachers.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let cell = favoriteCollection.cellForItem(at: indexPath) as! MyFavorite
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TeacherDetialsVC") as! TeacherDetialsVC
        vc.selectedID = cell.id
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func load_image(_ link:String, imageview:UIImageView)
    {
        
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.timeoutInterval = 10
        
        
        let task = session.dataTask(with: url, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
                
                guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                    
                    return
                }
                
                
                var image = UIImage(data: data!)
                
                if (image != nil)
                {
                    
                    
                    func set_image()
                    {
                        self.images_cache[link] = image
                        imageview.image = image
                    }
                    
                    
                    DispatchQueue.main.async(execute: set_image)
                    
                }
                
        })
        
        task.resume()
        
    }
    func displayAlerts(_ Massage: String)
    {
        var t = ""
        var b = ""
        
        if Settings.lang == "ar"{
            t = "تنبيه"
            b = "إغلاق"
        }
        else{
            t = "Alert"
            b = "Close"
        }
        
        let myAlert = UIAlertController(title: t, message:Massage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MainPageViewController") as! MainPageViewController
        self.navigationController?.pushViewController(vc, animated: false)

    }

    @IBAction func callUsBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CallUsVc") as! CallUsVc
        self.navigationController?.pushViewController(vc, animated: false)

    }
    
    @IBAction func addAdsBtn(_ sender: Any) {
        if types == "1"
        {
            if Settings.lang == "ar"
            {
                let msg = "انت طالب لا يمكنك اضافة اعلان"
                self.displayAlerts(msg)
                
            }
            else
            {
                let msg = "you'r student can't add Ads"
                self.displayAlerts(msg)
                
            }
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AddAdsVc") as! AddAdsVc
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        

    }
    
    @IBAction func myAdsBtn(_ sender: Any) {
        if types == "1"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyFavoriteVC") as! MyFavoriteVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyAdsVC") as! MyAdsVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        
    }
    
    @IBAction func offerBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AdsPageViewController") as! AdsPageViewController
        self.navigationController?.pushViewController(vc, animated: false)

    }
    
}
