//
//  AboutUsVC.swift
//  Q8School
//
//  Created by mac on 12/15/16.
//  Copyright © 2016 Atiaf. All rights reserved.
//

import UIKit

class AboutUsVC: UIViewController {

    @IBOutlet weak var myAdsImg: UIImageView!
    @IBOutlet weak var offerLbl: UILabel!
    @IBOutlet weak var myAdsLbl: UILabel!
    @IBOutlet weak var addAdsLbl: UILabel!
    @IBOutlet weak var callUsLbl: UILabel!
    @IBOutlet weak var loadingLbl: UILabel!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var aboutUsView: UIView!
    @IBOutlet weak var aboutUsImage: UIImageView!
    @IBOutlet weak var ourGoalLbl: UILabel!
    @IBOutlet weak var aboutUsLbl: UILabel!
    @IBOutlet weak var attentionLbl: UILabel!
    var dataFound: Bool!
    var type: String!
    
    @IBOutlet weak var openMenu: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        aboutUsView.layer.cornerRadius = 10
        aboutUsView.layer.masksToBounds = true
        aboutUsImage.layer.cornerRadius = 0.5 * aboutUsImage.bounds.size.width
        aboutUsImage.clipsToBounds = true
        self.title = "About Us"
        shadowView.isHidden = true
        loadingView.isHidden = true
        AboutUs(Settings.lang)
        if Settings.lang == "ar"{
            ourGoalLbl.text = "هدفنا"
            attentionLbl.text = "هذا البرنامج للمدرسين و المدرسات والطلاب داخل الكويت فقط"
            self.title = "عنا"
            aboutUsView.semanticContentAttribute = .forceRightToLeft
            loadingLbl.text = "جاري التحميل"
            callUsLbl.text = "اتصل بنا"
            addAdsLbl.text = "اضف اعلانك"
            myAdsLbl.text = "اعلاناتي"
            offerLbl.text = "العروض"

        }
        if revealViewController() != nil {
            
            revealViewController().rightViewRevealWidth = 235
            openMenu.target = revealViewController()
            openMenu.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        type = UserDefaults.standard.string(forKey: "userType")
        if type == "1"
        {
            if Settings.lang == "ar"
            {
                myAdsLbl.text = "المفضلة"
                myAdsImg.image = UIImage(named: "fav")
                
            }
            else
            {
                myAdsLbl.text = "Favorite"
                myAdsImg.image = UIImage(named: "fav")
                
                
            }
        }



    }
    func AboutUs(_ lang: String)
    {
        shadowView.isHidden = false
        loadingView.isHidden = false
        loadingActivity.startAnimating()
        let request = NSMutableURLRequest(url: URL(string:Settings.AboutUs)!)
        request.httpMethod = "POST"
        let postString = "lang=\(lang)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
            
            if let responseData = data {
                
                do {
                    
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                    print("ddd\(json)")
                    DispatchQueue.main.async
                        {
                            self.dataFound = json["Success"] as? Bool
                            if self.dataFound!
                            {
                                
                                let data = json["Result"] as! NSDictionary
                                self.aboutUsLbl.text = data["about_us"] as? String
                                self.shadowView.isHidden = true
                                self.loadingView.isHidden = true
                                self.loadingActivity.stopAnimating()

                                
                            }

                            
                    }
                    
                }
                catch
                {
                    print ("Could not Serialize")
                }
                
            }
            
        }
        task.resume()
        

        
    }
    func displayAlerts(_ Massage: String)
    {
        var t = ""
        var b = ""
        
        if Settings.lang == "ar"{
            t = "تنبيه"
            b = "إغلاق"
        }
        else{
            t = "Alert"
            b = "Close"
        }
        
        let myAlert = UIAlertController(title: t, message:Massage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MainPageViewController") as! MainPageViewController
        self.navigationController?.pushViewController(vc, animated: false)

        
    }

    @IBAction func callUsBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CallUsVc") as! CallUsVc
        self.navigationController?.pushViewController(vc, animated: false)
        

    }
    
    @IBAction func addAdsbtn(_ sender: Any) {
        if type == "1"
        {
            if Settings.lang == "ar"
            {
                let msg = "انت طالب لا يمكنك اضافة اعلان"
                self.displayAlerts(msg)
                
            }
            else
            {
                let msg = "you'r student can't add Ads"
                self.displayAlerts(msg)
                
            }
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AddAdsVc") as! AddAdsVc
            self.navigationController?.pushViewController(vc, animated: false)
            
        }

    }
    
    @IBAction func myAdsBtn(_ sender: Any) {
        if type == "1"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyFavoriteVC") as! MyFavoriteVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyAdsVC") as! MyAdsVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }

    }
    
    @IBAction func offerBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AdsPageViewController") as! AdsPageViewController
        self.navigationController?.pushViewController(vc, animated: false)

    }
    
    
    

}
