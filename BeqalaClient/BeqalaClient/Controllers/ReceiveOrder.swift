
import UIKit

final class ReceiveOrder: BaseController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var deliveryWay = Number.zero.intValue, quantities = Array<int>()
    
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var titlePanel: UIView! { didSet { shadow(for: titlePanel) } }
    @IBOutlet fileprivate weak var lblTitle: UILabel! { didSet { lblTitle.text = .empty } }
    @IBOutlet fileprivate weak var lblAddReceiver: UILabel! { didSet { lblAddReceiver.text = .empty } }
    @IBOutlet fileprivate weak var txfReceiverName: TextField! { didSet { txfReceiverName.delegate = self } }
    @IBOutlet fileprivate weak var txfPhoneNumber: TextField! { didSet { txfPhoneNumber.delegate = self } }
    @IBOutlet fileprivate weak var btnSend: Button!
    @IBOutlet fileprivate weak var btnBack: UIButton!
    @IBOutlet fileprivate weak var lblRightPointer: UILabel!
    @IBOutlet fileprivate weak var lblLeftPointer: UILabel!
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    
    // MARK: - Actions
    
    @IBAction private func backOnCLick(_ sender: UIButton) {
        back()
    }
    
    @IBAction private func sendOrderOnCLick(_ sender: Button) {
        if (Gemo.gem.isConnected) {
            guard !recipient.isEmpty, !phone.isEmpty else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.SomeDataReqiured.localized(), in: view)
                return
            }
            print("delivery mode", deliveryWay)
            let parameters: [string: any] = [
                Constants.Misc.MainKey: Constants.Misc.MainValue,
                "user_id": UserDefaults.standard.string(forKey: Constants.Misc.UserID) ?? .empty,
                "type_send": deliveryWay ,
                "name": recipient,
                "phone": phone,
                "building": string.empty,
                "floor": string.empty,
                "address": string.empty,
                "quantity": quantities
            ]
            print("paramters: ", parameters)
            Gemo.gem.startSpinning(in: view)
            HttpClient.request(link: Constants.Urls.SendOrder, method: .post, parameters: parameters) { [weak self] (data, response, error) -> void in
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("send order error: \(string(describing: error))")
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription, in: self!.view)
                    return
                }
                print("send order response \(string(describing: data))")
                Gemo.gem.stopSpinning()
                guard let json = (data as? [Dictionary<string, any>])?.first, let result = json["result"] as? string,
                    let aMessage = json["data"] as? string,
                    let eMessage = json["data_en"] as? string else {
                    return
                }
                //Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: (Localizer.instance.current == .arabic) ? aMessage : eMessage, in: self!.view)
                if (result == "true") {
                    ThreadQueue.main { [weak self] in
                        self?.navigationController?.popToRootViewController(animated: true)
                    }
                }
            }
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
    
}


// MARK: - Helper Functions

extension ReceiveOrder
{
    
    fileprivate func updateUi()-> void {
        tabBar.isHidden = true
        view.bringSubview(toFront: titlePanel)
        lblTitle.text = Localization.Shopping.localized()
        lblAddReceiver.text = Localization.AddRecipient.localized()
        txfReceiverName.placeholder = Localization.RecipientName.localized()
        txfPhoneNumber.placeholder = Localization.PhoneNumber.localized()
        btnSend.setTitle(Localization.Send.localized(), for: .normal)
        btnBack.transform = CGAffineTransform.identity
        if (Localizer.instance.current == .arabic) {
            btnBack.leftAnchor.constraint(equalTo: titlePanel.leftAnchor, constant: 8).isActive = true
            lblRightPointer.text = 1.toString
            lblLeftPointer.text = 2.toString
            txfPhoneNumber.textAlignment = .right
            txfReceiverName.textAlignment = .right
            
        } else {
            btnBack.rightAnchor.constraint(equalTo: titlePanel.rightAnchor, constant: -8).isActive = true
            btnBack.transform = btnBack.transform.rotated(by: cgFloat.pi)
            lblRightPointer.text = 2.toString
            lblLeftPointer.text = 1.toString
            txfPhoneNumber.textAlignment = .left
            txfReceiverName.textAlignment = .left
            
        }
    }
    
}


// MARK: - Text Field delegate functions

extension ReceiveOrder: UITextFieldDelegate
{
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}


// MARK: - Computed properties

extension ReceiveOrder
{
    
    fileprivate var recipient: string {
        return txfReceiverName.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var phone: string {
        return txfPhoneNumber.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
}




















