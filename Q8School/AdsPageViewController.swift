
import UIKit

final class AdsPageViewController: UIViewController
{
    
    
    @IBOutlet private weak var adsCollection: UICollectionView? {  didSet {
        adsCollection?.delegate = self
        adsCollection?.dataSource = self
        }}
    
    fileprivate var images = [String]()
    private var sizeImage: CGFloat!
    private var timer: Timer?
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        LoadImages(Settings.lang)
    }
    
    private func LoadImages(_ lang: String)-> Void {
        let request = NSMutableURLRequest(url: URL(string: Settings.GetAllAds)!)
        request.httpMethod = "POST"
        let postString = "lang=\(lang)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared
            .dataTask(with: request as URLRequest) { [weak self] data, response, error in
                guard let this = self else { return }
                guard error == nil && data != nil else {
                    print("error=\(String(describing: error))")
                    return
                }
                let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("responseString = \(String(describing: responseString))")
                if let responseData = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                        print("ads json data\(json)")
                        let dataFound = json["Success"] as? Bool
                        if (dataFound!) {
                            let data = json ["Result"] as! [NSDictionary]
                            print("this data\(data)")
                            for i in data {
                                this.images.append(i["img"] as! String)
                            }
                        } else {
                            DispatchQueue.main.async { [weak self] in
                                self?.timer?.invalidate()
                                self?.performSegue(withIdentifier: "mainPage", sender: self)
                            }
                        }
                        DispatchQueue.main.async { [weak self] in
                            guard let me = self else { return }
                            self?.adsCollection?.reloadData()
                            if (me.images.count > 1) {
                                me.timer = Timer.scheduledTimer(timeInterval: 3.0, target: me, selector: #selector(me.swipImage), userInfo: nil, repeats: true)
                                me.timer?.fire()
                            }
                        }
                        /*DispatchQueue.main.async {
                            print("count\(this.images.count)")
                            for i in 0..<this.images.count {
                                this.load_image(this.images[i])
                            }
 
                        }*/
                    } catch {
                        print ("Could not Serialize")
                    }
                }
        }
        task.resume()
    }
    
    private var counter = 0
    
    @objc private func swipImage() {
        if (counter == images.count) {
            counter = 0
        }
        let indexPath = IndexPath(item: counter, section: 0)
        adsCollection?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        counter += 1
    }
    

    internal override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        UIApplication.shared.isStatusBarHidden = true
    }
    
    internal override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        timer?.invalidate()
        UIApplication.shared.isStatusBarHidden = false
    }
    
    @IBAction func submitButton(_ sender: Any) {
        timer?.invalidate()
        if (isLogged) {
            isLogged = false
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: "EditProfileVC") as? EditProfileVC {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            self.performSegue(withIdentifier: "mainPage", sender: self)
        }
    }
    
}



extension AdsPageViewController: UICollectionViewDelegate,
    UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout
{
    internal func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "com.ads.slider.image.view.id", for: indexPath) as? AdsSlider else { return UICollectionViewCell() }
        cell.adImage.loadImage(from: images[indexPath.item], onComplete: nil)
        return cell
    }
    
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("selected item is: \(indexPath)")
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
    
}





// MARK: - image caching

private let imageCache = NSCache<NSString,UIImage>()

internal extension UIImageView
{
    internal func loadImage(from link: String, onComplete: ((_ isLoaded: Bool)-> ())?)-> Void {
        image = nil
        if (link.isEmpty) {
            print("empty image link or invalid link")
            onComplete?(false)
            return
        }
        
        if let cachedImage = imageCache.object(forKey: link as NSString) {
            image = cachedImage
            onComplete?(true)
            
            return
        }
        // download and cache image
        // download
        if let url = URL(string: link as String) {
            URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
                if (error != nil) {
                    onComplete?(false)
                    return
                }
                if let imageData = data, let imageToCache = UIImage(data: imageData) {
                    DispatchQueue.main.async { [weak self] in
                        // cache image
                        imageCache.setObject(imageToCache, forKey: link as NSString)
                        self?.image = imageToCache
                        onComplete?(true)
                    }
                } else {
                    onComplete?(false)
                }
                }.resume()
        }
    }
}




































