
import UIKit
import CoreLocation


final class SignUp: BaseController
{
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var isUpdate = false
    fileprivate var gender = Gender.male,
    userLocation = CLLocation(),
    locationManager = CLLocationManager()
    
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var lblTitleHeader: UILabel!
    @IBOutlet fileprivate weak var titlePanal: UIView! {
        didSet {
            shadow(for: titlePanal)
        }
    }
    @IBOutlet fileprivate weak var txfFirstName: TextField! {
        didSet {
            txfFirstName.delegate = self
        }
    }
    @IBOutlet fileprivate weak var txfsecondName: TextField! {
        didSet {
            txfsecondName.delegate = self
        }
    }
    @IBOutlet fileprivate weak var txfUserName: TextField! {
        didSet {
            txfUserName.delegate = self
        }
    }
    @IBOutlet fileprivate weak var txfEmail: TextField! {
        didSet {
            txfEmail.delegate = self
        }
    }
    @IBOutlet fileprivate weak var txfPhoneNumber: TextField! {
        didSet {
            txfPhoneNumber.delegate = self
        }
    }
    @IBOutlet fileprivate weak var txfPassword: TextField! {
        didSet {
            txfPassword.delegate = self
        }
    }
    @IBOutlet fileprivate weak var rdMale: RadioView!
    @IBOutlet fileprivate weak var rdFmale: RadioView!
    @IBOutlet fileprivate weak var btnSignUp: Button!
    @IBOutlet fileprivate weak var genderPanal: View!
    @IBOutlet fileprivate weak var lblFemale: UILabel!
    @IBOutlet fileprivate weak var lblMale: UILabel!
    @IBOutlet fileprivate weak var namePanal: UIView!
    @IBOutlet fileprivate weak var namesStack: UIStackView!
    @IBOutlet fileprivate weak var btnBack: UIButton!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        configureLocation()
        print("update mode = \(isUpdate)")
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBar.isHidden = true
        navigationController?.navigationBar.isHidden = true
        updateUi()
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    /*internal override func updateNavigationBar() {
        navigationController?.navigationBar.isHidden = false
        navigationItem.title = "تسجيل جديد"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(goBackOnCLick(_:)))
    }*/
    
    
    // MARK: - Actions
    
    
    @IBAction private func backOnClick(_ sender: UIButton) {
        back()
    }
    
    @IBAction private func signUpOnCLick(_ sender: Button) {
        if (Gemo.gem.isConnected) {
            guard !firstName.isEmpty else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.FirstNameReqiured.localized(), in: view)
                return
            }
            guard !secondName.isEmpty else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.SecondNameReqiured.localized(), in: view)
                return
            }
            guard !userName.isEmpty else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.UserNameReqiured.localized(), in: view)
                return
            }
            guard !email.isEmpty || email.isEmail else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InvaildEmail.localized(), in: view)
                return
            }
            guard !phoneNumber.isEmpty else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.PhoneNumberReqiured.localized(), in: view)
                return
            }
            guard !password.isEmpty else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.PasswordReqiured.localized(), in: view)
                return
            }            
            guard userLocation.coordinate.latitude != Number.zero.doubleValue || userLocation.coordinate.longitude != Number.zero.doubleValue else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InvalidLocation.localized(), in: view)
                return
            }
            
            if (isUpdate) {
                let params: [string: any] = [
                    Constants.Misc.MainKey: Constants.Misc.MainValue,
                    "user_id": UserDefaults.standard.value(forKey: Constants.Misc.UserID) as? string ?? .empty,
                    "first_name" : firstName,
                    "second_name": secondName,
                    "username": userName,
                    "email": email,
                    "phone": phoneNumber,
                    "password": password,
                    "password_again": password,
                    "gender": gender.rawValue,
                    "lati": userLocation.coordinate.latitude,
                    "longi": userLocation.coordinate.longitude
                ]
                editProfile(with: params)
            } else {
                let params: [string: any] = [
                    Constants.Misc.MainKey: Constants.Misc.MainValue,
                    "first_name" : firstName,
                    "second_name": secondName,
                    "username": userName,
                    "email": email,
                    "phone": phoneNumber,
                    "password": password,
                    "password_again": password,
                    "gender": gender.rawValue,
                    "lati": userLocation.coordinate.latitude,
                    "longi": userLocation.coordinate.longitude
                ]
                register(with: params)
            }
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
    
    @IBAction private func chooseGenderOnClick(_ sender: UIButton) {
        switch (sender.tag) {
        case 0:
            gender = Gender.male
            rdFmale.isSelected = false
            rdMale.isSelected = true
        case 1:
            gender = Gender.female
            rdFmale.isSelected = true
            rdMale.isSelected = false
        default:
            break
        }
    }
    
    
}


// MARK: - Helper functions

extension SignUp
{
    
    fileprivate func updateUi()-> void {
        btnBack.transform = CGAffineTransform.identity
        if (Localizer.instance.current == .arabic) {
            txfEmail.textAlignment = .right
            txfPassword.textAlignment = .right
            txfUserName.textAlignment = .right
            txfFirstName.textAlignment = .right
            
            txfsecondName.textAlignment = .right
            txfPhoneNumber.textAlignment = .right
            genderPanal.semanticContentAttribute = .forceLeftToRight
            namePanal.semanticContentAttribute = .forceLeftToRight
            namesStack.semanticContentAttribute = .forceLeftToRight
            btnBack.leftAnchor.constraint(equalTo: titlePanal.leftAnchor, constant: 8).isActive = true
        } else {
            txfEmail.textAlignment = .left
            txfPassword.textAlignment = .left
            txfUserName.textAlignment = .left
            txfFirstName.textAlignment = .left
            
            txfsecondName.textAlignment = .left
            txfPhoneNumber.textAlignment = .left
            genderPanal.semanticContentAttribute = .forceRightToLeft
            namePanal.semanticContentAttribute = .forceRightToLeft
            namesStack.semanticContentAttribute = .forceRightToLeft
            btnBack.rightAnchor.constraint(equalTo: titlePanal.rightAnchor, constant: -8).isActive = true
            btnBack.transform = btnBack.transform.rotated(by: cgFloat.pi)
        }
        txfEmail.placeholder = Localization.Email.localized()
        txfPassword.placeholder = Localization.Password.localized()
        txfUserName.placeholder = Localization.UserName.localized()
        txfFirstName.placeholder = Localization.FirstName.localized()
        
        txfsecondName.placeholder = Localization.LastName.localized()
        txfPhoneNumber.placeholder = Localization.PhoneNumber.localized()
        lblFemale.text = Localization.Female.localized()
        lblMale.text = Localization.Male.localized()
        if (isUpdate) {
            btnSignUp.setTitle(Localization.UpdateProfile.localized(), for: .normal)
            lblTitleHeader.text = Localization.UpdateProfile.localized()
            txfFirstName.text = UserDefaults.standard.value(forKey: Constants.Misc.FirstName) as? string ?? .empty
            txfsecondName.text = UserDefaults.standard.value(forKey: Constants.Misc.LastName) as? string ?? .empty
            txfPhoneNumber.text = UserDefaults.standard.value(forKey: Constants.Misc.Phone) as? string ?? .empty
            txfEmail.text = UserDefaults.standard.value(forKey: Constants.Misc.Email) as? string ?? .empty
            txfUserName.text = UserDefaults.standard.value(forKey: Constants.Misc.UserName) as? string ?? .empty
            let genderString = UserDefaults.standard.value(forKey: Constants.Misc.Gender) as? string ?? "1"
            let gender = NumberFormatter.formate.number(from: genderString)?.intValue ?? 1
            switch(gender) {
            case 1:
                rdMale.isSelected = true
                rdFmale.isSelected = false
            case 2:
                rdMale.isSelected = false
                rdFmale.isSelected = true
                
            default:
                break
            }
        } else {
            btnSignUp.setTitle(Localization.Register.localized(), for: .normal)
            lblTitleHeader.text = Localization.NewSignUp.localized()
        }
    }
    
    fileprivate func configureLocation()-> void {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        guard CLLocationManager.locationServicesEnabled() else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.OpenLocation.localized(), in: view)
            return
        }
        locationManager.startUpdatingLocation()
        
    }
    
    fileprivate func editProfile(with parameters: Dictionary<string,any>)-> void  {
        Gemo.gem.startSpinning(in: view)
        HttpClient.request(link: Constants.Urls.EditProfile, method: .post, parameters: parameters) { [weak self] (data, response, error) -> void in
            guard let this = self else { return }
            if (error != nil) {
                print("edit profile error \(string(describing: error))")
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.Error.localized(), in: this.view)
                Gemo.gem.stopSpinning()
                return
            }
            print("edit profile response \(string(describing: data))")
            Gemo.gem.stopSpinning()
            if let value = (data as? [[string: any]])?.first, let success = value["result"] as? string, let message = value["data"] as? string {
                let user = User(JSON: value)
                //print("updated user data \(user)")
                if (success == "true") {
                    UserDefaults.standard.set(user.firstName, forKey: Constants.Misc.FirstName)
                    UserDefaults.standard.set(user.secondName, forKey: Constants.Misc.LastName)
                    UserDefaults.standard.set(user.phone, forKey: Constants.Misc.Phone)
                    UserDefaults.standard.set(user.name, forKey: Constants.Misc.UserName)
                    UserDefaults.standard.set(user.email, forKey: Constants.Misc.Email)
                    UserDefaults.standard.set(user.gender, forKey: Constants.Misc.Gender)
                    UserDefaults.standard.set(user.latitude, forKey: Constants.Misc.UserLatitude)
                    UserDefaults.standard.set(user.longitude, forKey: Constants.Misc.UserLongitude)
                    DispatchQueue.main.async { [weak self] in
                        guard let weakSelf = self else { return }
                        _ = weakSelf.navigationController?.popViewController(animated: true)
                    }
                } else {
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: message, in: this.view)
                }
            } else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.Error.localized(), in: this.view)
            }
        }

    }
    
    fileprivate func register(with parameters: Dictionary<string, any>)-> void {
        Gemo.gem.startSpinning(in: view)
        HttpClient.request(link: Constants.Urls.SignUp, method: .post, parameters: parameters) { [weak self] (data, response, error) -> void in
            guard let this = self else { return }
            if (error != nil) {
                print("sign up error \(string(describing: error))")
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.Error.localized(), in: this.view)
                Gemo.gem.stopSpinning()
                return
            }
            print("sign up response \(string(describing: data))")
            Gemo.gem.stopSpinning()
            if let value = (data as? [[string: any]])?.first, let success = value["result"] as? string, let message = value["data"] as? string {
                if (success == "true") {
                    DispatchQueue.main.async { [weak self] in
                        guard let weakSelf = self else { return }
                        _ = weakSelf.navigationController?.popViewController(animated: true)
                    }
                } else {
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: message, in: this.view)
                }
            } else {
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.Error.localized(), in: this.view)
            }
        }
    }
    
    
}



// MARK: - Computed properties

extension SignUp
{
    
    
    fileprivate var firstName: string {
        return txfFirstName.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var secondName: string {
        return txfsecondName.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var userName: string {
        return txfUserName.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var email: string {
        return txfEmail.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var phoneNumber: string {
        return txfPhoneNumber.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    fileprivate var password: string {
        return txfPassword.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }

    
    
    
}



// MARK: - Text Field delegate functions

extension SignUp: UITextFieldDelegate
{
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}



// MARK: - Location

extension SignUp: CLLocationManagerDelegate
{
    
    internal func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("location error \(string(describing: error))")
        manager.stopUpdatingLocation()
    }
    
    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            userLocation = location
        }
        manager.stopUpdatingLocation()
    }
    
}
























