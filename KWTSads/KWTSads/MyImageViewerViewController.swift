
import UIKit
import SwiftSpinner


class MyImageViewerViewController: UIViewController,
    UIScrollViewDelegate
{
    @IBOutlet var myImageView: UIImageView!
    var selectedImage:String!
    var images_cache = [String:UIImage]()

    @IBOutlet var scrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
        if Reachability.isConnectedToNetwork() == true {
            self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "ac_back-arrow")
            self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "ac_back-arrow")
            self.navigationController?.navigationBar.tintColor = UIColor(red: 0.96, green: 0.69, blue:  0.25, alpha: 1 )
            
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage
            self.scrollView.minimumZoomScale = 1.0
            self.scrollView.maximumZoomScale = 6.0
            SwiftSpinner.show("Loading...")
            print("selected image ====>>>> \(selectedImage)")
            DispatchQueue.main.async {
                self.load_image(self.selectedImage, imageview:self.myImageView)
            }
            SwiftSpinner.hide()
        } else {
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage
            
            let err = "Internet connection FAILED"
            self.displayAlertMessage(err)

        }

    }
    func displayAlertMessage(_ userMessage:String)
    {
        let myAlert = UIAlertController(title: "Alert", message:userMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }

    func load_image(_ link:String, imageview:UIImageView)
    {
        
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.timeoutInterval = 10
        
        
       let task = session.dataTask(with: url, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
            
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                
                return
            }
            
            
            var image = UIImage(data: data!)
            
            if (image != nil)
            {
                
                
                func set_image()
                {
                    self.images_cache[link] = image
                    imageview.image = image
                }
                
                
                DispatchQueue.main.async(execute: set_image)
                
            }
        })
        task.resume()
        
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.myImageView
    }
}































