//
//  UserType1ViewController.swift
//  Q8School
//
//  Created by mac on 12/11/16.
//  Copyright © 2016 Atiaf. All rights reserved.
//

import UIKit

class UserType1ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    var userTypeName = ["Student","Teacher"]
    var userTypeNameA = ["طالب","مدرس"]
    var userTypeID = ["1","0"]
    var usersTypeName: String!
    var usersTypeID: String!

    @IBOutlet var userTypeTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        userTypeTable.delegate = self
        userTypeTable.dataSource = self
        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {        // #warning Incomplete implementation, return the number of sections
        return userTypeID.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "userTypeCell", for: indexPath) as! UserTypeTableViewCell
        if Settings.lang == "ar"
        {
            cell.userTypeLabel.text = userTypeNameA[indexPath.row]
            cell.userTypeId = userTypeID[indexPath.row]
            
        }
        else
        {
            cell.userTypeLabel.text = userTypeName[indexPath.row]
            cell.userTypeId = userTypeID[indexPath.row]
            
        }
        return cell
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
        
    {
        if segue.identifier == "unwindUser"
        {
            if let cell = sender as? UITableViewCell
            {
                print("hhhhhhhhgggggg")
                if Settings.lang == "ar"
                {
                    let indexPath = userTypeTable.indexPath(for: cell)
                    usersTypeName = userTypeNameA[(indexPath?.row)!]
                    print("user name\(usersTypeName)")
                    usersTypeID = userTypeID[(indexPath?.row)!]
                    print("user name\(usersTypeName)")
                    
                }
                else
                {
                    let indexPath = userTypeTable.indexPath(for: cell)
                    usersTypeName = userTypeName[(indexPath?.row)!]
                    print("user name\(usersTypeName)")
                    usersTypeID = userTypeID[(indexPath?.row)!]
                    print("user name\(usersTypeName)")
                    
                }
                
            }
            
        }
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
