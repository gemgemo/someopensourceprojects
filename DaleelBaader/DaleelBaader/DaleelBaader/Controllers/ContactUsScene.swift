
import UIKit

import Localize_Swift

final class ContactUsScene: BaseController
{

   
    // MARK: - Constants
    
    fileprivate let color = Color.black
    
    //MARK: - Variables
    
    fileprivate var placeholderColor = Color.lightGray
    fileprivate var placeholder =  string.empty
    
    // MARK: - Outlets
    
    @IBOutlet private weak var topView: UIView! {
        didSet {
            setBorder(for: topView)
        }
    }
    @IBOutlet private weak var footerView: UIView! {
        didSet {
            setBorder(for: footerView)
        }
    }
    @IBOutlet fileprivate weak var phone1: UILabel!
    @IBOutlet fileprivate weak var phone2: UILabel!
    @IBOutlet fileprivate weak var email: UILabel!
    @IBOutlet fileprivate weak var suggestion: UILabel!
    @IBOutlet fileprivate weak var userEmail: UITextField! {
        didSet {
            userEmail.delegate = self
        }
    }
    @IBOutlet fileprivate weak var suggestionTitle: UITextField! {
        didSet {
            suggestionTitle.delegate = self
        }
    }
    @IBOutlet fileprivate weak var divider: UIView! {
        didSet {
            divider.backgroundColor = Color(white: 0.80, alpha: 0.9)
        }
    }
    @IBOutlet fileprivate weak var suggesstionContent: UITextView! {
        didSet {
            suggesstionContent.delegate = self
        }
    }
    @IBOutlet fileprivate weak var btnSender: UIButton!
    @IBOutlet fileprivate weak var userEmailPanal: UIView! {
        didSet {
            setBorder(for: userEmailPanal)
        }
    }
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        if (AppManager.shared.isConnected) {
            updateUI()
        } else {
            dialog(with: Constants.Misc.InternetMessage)
        }
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBar()
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    //MARK: - Actions
    
    @objc fileprivate func dismissOnCLick(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func sendMessageOnClick(_ sender: UIButton) {
        
        guard let userMail = userEmail.text?.trimmingCharacters(in: .whitespaces), userMail.isEmail else {
            dialog(with: "emailvalidation".localized()) // email validation
            return
        }
        
        guard let subject = suggestionTitle.text?.trimmingCharacters(in: .whitespaces), !subject.isEmpty else {
            dialog(with: "invalidfield".localized())
            return
        }
        
        guard let contentMessage = suggesstionContent.text?.trimmingCharacters(in: .whitespaces), !contentMessage.isEmpty, contentMessage != placeholder else {
            dialog(with: "invalidfield".localized())
            return
        }
        if (AppManager.shared.isConnected) {
            ContactUs.postData(parameters:  ["email": userMail, "subject": subject, "message": contentMessage], viewController: self) { [weak self] (message) in
                self?.dialog(with: message)
                UIApplication.showIndicator(by: false)
            }
        } else {
            dialog(with: Constants.Misc.InternetMessage)
        }
    }
    
    
}


// MARK: - Helper Functions

extension ContactUsScene
{
    
    fileprivate func updateNavigationBar()-> void {
        navigationItem.title = "sideMenuCallUsTitle".localized()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_close"), style: .plain, target: self, action: #selector(dismissOnCLick(_:)))
    }
    
    fileprivate func setBorder(for view: UIView)-> void {
        view.layer.borderColor = Color(white: 0.80, alpha: 0.9).cgColor
        view.layer.borderWidth = 1.0
    }
    
    fileprivate func updateUI()-> void {
        placeholder = "suggestionconentplaceholder".localized()
        startSpin()
        suggestion.text = "labelsuggesion".localized()
        userEmail.placeholder = "useremailplaseholder".localized()
        suggestionTitle.placeholder = "suggestiontitleplaceholder".localized()
        suggesstionContent.text = placeholder
        suggesstionContent.textColor = placeholderColor
        btnSender.setTitle("senderTitle".localized(), for: .normal)
        ContactUs.getData(viewController: self) { [weak self] (instance) -> void in
            DispatchQueue.main.async { [weak self] in
                guard let this = self else { return }
                this.phone1.text = "phonename".localized() + ": " + (instance.phone ?? string.empty)
                this.phone2.text = "phonename".localized() + ": " + (instance.phone2 ?? string.empty)
                this.email.text = "emailname".localized() + ": " + (instance.email ?? string.empty)
                UIApplication.showIndicator(by: false)
                this.stopSpin()
            }
        }
    }
    
}

//MARK: - Text field delegate functions
extension ContactUsScene: UITextFieldDelegate
{
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}

//MARK: - Text view delegate functions
extension ContactUsScene: UITextViewDelegate
{
    
    internal func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text == placeholder && textView.textColor == placeholderColor) {
            textView.text = string.empty
            textView.textColor = color
        }
    }
    
    internal func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text.isEmpty) {
            textView.text = placeholder
            textView.textColor = placeholderColor
        }
    }

    
    
}












