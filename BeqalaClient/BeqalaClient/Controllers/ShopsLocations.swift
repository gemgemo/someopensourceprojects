
import UIKit
import GoogleMaps

final class ShopsLocations: BaseController
{
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    fileprivate var shop: StorsLocations?
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var titlePanal: UIView! {
        didSet {
            shadow(for: titlePanal)
        }
    }
    @IBOutlet fileprivate weak var btnShowInMenu: UIButton!
    @IBOutlet fileprivate weak var subtitlePanal: UIView! {
        didSet {
            shadow(for: subtitlePanal)
        }
    }
    @IBOutlet fileprivate weak var mapView: GMSMapView! {
        didSet {
            mapView.delegate = self
        }
    }
    @IBOutlet fileprivate weak var btnMenu: UIButton!
    @IBOutlet fileprivate weak var btnSearch: UIButton!
    @IBOutlet fileprivate weak var btnFavorites: UIButton!
    @IBOutlet fileprivate weak var listIcon: UIImageView!
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)        
        tabBarItems?.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .top)
        selectedTabBarItem = 0
        updateUi()
        selectedItemIndex = 4
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if (Gemo.gem.isConnected) {
            loadData()
        } else {
            Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.InternetConnection.localized(), in: view)
        }
    }
    
    
    // MARK: - Actions
    
    @IBAction private func menuClicked(_ sender: UIButton) {
        menu(withState: .opened)
    }
    
    @IBAction private func searchClicked(_ sender: UIButton) {
        navigateToSearch()
    }
    
    @IBAction private func favoritesClicked(_ sender: UIButton) {
        navigateToFavorites()
    }
    
    @IBAction private func showInMenuOnClick(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    

}



// MARK: - Helper functions

extension ShopsLocations
{
    
    fileprivate func updateUi()-> void {
        view.bringSubview(toFront: subtitlePanal)
        view.bringSubview(toFront: titlePanal)
        btnShowInMenu.setTitle(Localization.ShowInMenu.localized(), for: .normal)
        mapView.clear()
        mapView.mapType = .terrain
        btnMenu.transform = CGAffineTransform.identity
        if (Localizer.instance.current == .arabic) {
            btnMenu.rightAnchor.constraint(equalTo: titlePanal.rightAnchor, constant: -8.0).isActive = true
            btnFavorites.leftAnchor.constraint(equalTo: titlePanal.leftAnchor, constant: 8.0).isActive = true
            btnSearch.leftAnchor.constraint(equalTo: btnFavorites.rightAnchor, constant: 20.0).isActive = true
            listIcon.rightAnchor.constraint(equalTo: subtitlePanal.rightAnchor, constant: -8).isActive = true
            btnShowInMenu.rightAnchor.constraint(equalTo: listIcon.leftAnchor, constant: -8).isActive = true
            btnShowInMenu.leftAnchor.constraint(equalTo: subtitlePanal.leftAnchor, constant: 0).isActive = true
            btnShowInMenu.contentHorizontalAlignment = .right
        } else {
            btnMenu.leftAnchor.constraint(equalTo: titlePanal.leftAnchor, constant: 8.0).isActive = true
            btnFavorites.rightAnchor.constraint(equalTo: titlePanal.rightAnchor, constant: -8.0).isActive = true
            btnSearch.rightAnchor.constraint(equalTo: btnFavorites.leftAnchor, constant: -20.0).isActive = true
            listIcon.leftAnchor.constraint(equalTo: subtitlePanal.leftAnchor, constant: 8).isActive = true
            btnShowInMenu.leftAnchor.constraint(equalTo: listIcon.rightAnchor, constant: 8).isActive = true
            btnShowInMenu.rightAnchor.constraint(equalTo: subtitlePanal.rightAnchor, constant: 0).isActive = true
            btnShowInMenu.contentHorizontalAlignment = .left
            btnMenu.transform = btnMenu.transform.rotated(by: cgFloat.pi)
        }

    }
    
    fileprivate func loadData()-> void {
        StorsLocations.getData(self) { [weak self] (locations) in
            Gemo.gem.stopSpinning()
            if (!locations.isEmpty) {
                //print(locations)
                let numberFormatter = NumberFormatter()
                numberFormatter.locale = Locale(identifier: "en")
                locations.forEach { (location) in
                    guard let sLat = location.latitude, let slon = location.longitude, let latitude = numberFormatter.number(from: sLat)?.doubleValue, let longitude = numberFormatter.number(from: slon)?.doubleValue else { return }
                    //print("latitude: \(latitude), longitude: \(longitude)")
                    DispatchQueue.main.async { [weak self] in
                        let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                        let marker = GMSMarker()
                        marker.icon = #imageLiteral(resourceName: "marker-store")
                        marker.position = coordinate
                        marker.title = location.shopName
                        marker.map = self?.mapView
                        marker.appearAnimation = .pop
                        marker.userData = location
                        marker.isTappable = true
                        self?.mapView.camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: 10.0)
                    }
                }
            }
        }
    }
    
}


// MARK: - Map view delegate functions

extension ShopsLocations: GMSMapViewDelegate
{
    
    internal func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print("info view tapped")
        guard let shopLocation = shop else { return }
        guard let storePage = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboards.Store) as? Store else { return }
        let store = Shop()
        store.id = shopLocation.userId
        store.shopName = shopLocation.shopName
        storePage.store = store
        navigate(to: storePage)
    }
 
    internal func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let popup = UIView(frame: Rect(x: 0, y: 0, width: 220, height: 90))
        popup.isUserInteractionEnabled = true
        shop = marker.userData as? StorsLocations
        popup.backgroundColor = .white
        popup.setShadow(with: .zero, radius: 2, opacity: 0.7, color: .black)
        popup.layer.masksToBounds = true
        popup.layer.cornerRadius = 5
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        popup.addSubview(imageView)
        imageView.widthAnchor.constraint(equalToConstant: 80.0).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 80.0).isActive = true
        imageView.centerYAnchor.constraint(equalTo: popup.centerYAnchor).isActive = true
        imageView.rightAnchor.constraint(equalTo: popup.rightAnchor).isActive = true
        if let imageLink = shop?.photo {
            imageView.loadImage(from: imageLink)
        }
        let lblTitle = UILabel()
        lblTitle.isUserInteractionEnabled = true
        lblTitle.textAlignment = .right
        lblTitle.tintColor = #colorLiteral(red: 0.2598407269, green: 0.5441776514, blue: 0.7934442163, alpha: 1)
        lblTitle.font = Font.systemFont(ofSize: 16)
        popup.addSubview(lblTitle)
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.text = shop?.shopName ?? .empty
        lblTitle.topAnchor.constraint(equalTo: imageView.topAnchor, constant: 5.0).isActive = true
        lblTitle.rightAnchor.constraint(equalTo: imageView.leftAnchor, constant: -8.0).isActive = true
        lblTitle.leftAnchor.constraint(equalTo: popup.leftAnchor, constant: 8.0).isActive = true
        let lblDetails = UILabel()
        lblDetails.numberOfLines = 0
        lblDetails.isUserInteractionEnabled = true
        lblDetails.textAlignment = .right
        popup.addSubview(lblDetails)
        lblDetails.translatesAutoresizingMaskIntoConstraints = false
        lblDetails.tintColor = .black
        lblDetails.font = Font.systemFont(ofSize: 14.0)
        lblDetails.text = shop?.info ?? .empty
        lblDetails.topAnchor.constraint(equalTo: lblTitle.bottomAnchor, constant: 8.0).isActive = true
        lblDetails.rightAnchor.constraint(equalTo: lblTitle.rightAnchor).isActive = true
        lblDetails.leftAnchor.constraint(equalTo: lblTitle.leftAnchor).isActive = true
        lblDetails.bottomAnchor.constraint(equalTo: popup.bottomAnchor, constant: 8.0).isActive = true
        return popup
    }
    
}


/*extension ShopsLocations: UIPopoverPresentationControllerDelegate
{
    internal func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}*/
















