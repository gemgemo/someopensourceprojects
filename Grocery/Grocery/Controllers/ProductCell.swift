
import Foundation
import Gemo


final class ProductCell: BaseCollectionCell
{
    
    // MARK: - Constatnts
    
    
    // MARK: - Variables
    
    internal var product: Product? { didSet { updateUi() } }
    
    // MARK: - Outlets
    
    @IBOutlet internal weak var container: UIView! { didSet {
        container.setCorner(radius: 10)
        container.setBorder(with: 1, and: Color(white: 0.88, alpha: 0.9))
        }}
    @IBOutlet internal weak var btnEdits: UIButton!
    @IBOutlet private weak var productIcon: UIImageView!
    @IBOutlet private weak var lblProductName: UILabel!
    @IBOutlet private weak var lblProductQuantityTitle: UILabel!
    @IBOutlet private weak var lblQuantity: UILabel!
    @IBOutlet private weak var lblPriceTitle: UILabel!
    @IBOutlet private weak var lblPrice: UILabel!
    @IBOutlet private weak var productIconWidth: NSLayoutConstraint!
    @IBOutlet private weak var divider: UIView!
    @IBOutlet private weak var detailsStack: UIStackView!
    @IBOutlet private weak var detailsPanel: UIView!
    @IBOutlet private weak var quantityPanel: UIView!
    @IBOutlet private weak var pricePanel: UIView!
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        lblProductQuantityTitle.text = Localization.Quantity.localized()
        lblPriceTitle.text = Localization.Price.localized()
        productIcon.loadImage(from: getPhotoLink(), nil)
        lblProductName.text = product?.name ?? .empty
        lblQuantity.text = product?.quantity ?? .empty
        lblPrice.text = "\(product?.price ?? .empty) \(Localization.SR.localized())"
        
        if(Localizer.instance.current == .arabic) {
            lblPrice.textAlignment = .right
            lblProductName.textAlignment = .right
            lblQuantity.textAlignment = .right
            lblPriceTitle.textAlignment = .right
            lblProductQuantityTitle.textAlignment = .right
            
            btnEdits.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 8).isActive = true
            detailsPanel.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -8).isActive = true
            detailsPanel.leftAnchor.constraint(equalTo: btnEdits.rightAnchor, constant: 8).isActive = true
            productIcon.rightAnchor.constraint(equalTo: detailsPanel.rightAnchor, constant: -8).isActive = true
            divider.rightAnchor.constraint(equalTo: productIcon.leftAnchor, constant: -8).isActive = true
            detailsStack.rightAnchor.constraint(equalTo: divider.leftAnchor, constant: -8).isActive = true
            detailsStack.leftAnchor.constraint(equalTo: detailsPanel.leftAnchor, constant: 8).isActive = true
            lblProductQuantityTitle.rightAnchor.constraint(equalTo: quantityPanel.rightAnchor, constant: 0).isActive = true
            lblQuantity.rightAnchor.constraint(equalTo: lblProductQuantityTitle.leftAnchor, constant: -8).isActive = true
            lblQuantity.leftAnchor.constraint(equalTo: quantityPanel.leftAnchor, constant: 0).isActive = true
            lblPriceTitle.rightAnchor.constraint(equalTo: pricePanel.rightAnchor, constant: 0).isActive = true
            lblPrice.rightAnchor.constraint(equalTo: lblPriceTitle.leftAnchor, constant: -8).isActive = true
            lblPrice.leftAnchor.constraint(equalTo: pricePanel.leftAnchor, constant: 0).isActive = true
            
        } else {
            lblPrice.textAlignment = .left
            lblProductName.textAlignment = .left
            lblQuantity.textAlignment = .left
            lblPriceTitle.textAlignment = .left
            lblProductQuantityTitle.textAlignment = .left
            
            btnEdits.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -8).isActive = true
            detailsPanel.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 8).isActive = true
            detailsPanel.rightAnchor.constraint(equalTo: btnEdits.leftAnchor, constant: -8).isActive = true
            productIcon.leftAnchor.constraint(equalTo: detailsPanel.leftAnchor, constant: 8).isActive = true
            divider.leftAnchor.constraint(equalTo: productIcon.rightAnchor, constant: 8).isActive = true
            detailsStack.leftAnchor.constraint(equalTo: divider.rightAnchor, constant: 8).isActive = true
            detailsStack.rightAnchor.constraint(equalTo: detailsPanel.rightAnchor, constant: -8).isActive = true
            lblProductQuantityTitle.leftAnchor.constraint(equalTo: quantityPanel.leftAnchor, constant: 0).isActive = true
            lblQuantity.leftAnchor.constraint(equalTo: lblProductQuantityTitle.rightAnchor, constant: 8).isActive = true
            lblQuantity.rightAnchor.constraint(equalTo: quantityPanel.rightAnchor, constant: 0).isActive = true
            lblPriceTitle.leftAnchor.constraint(equalTo: pricePanel.leftAnchor, constant: 0).isActive = true
            lblPrice.leftAnchor.constraint(equalTo: lblPriceTitle.rightAnchor, constant: 8).isActive = true
            lblPrice.rightAnchor.constraint(equalTo: pricePanel.rightAnchor, constant: 0).isActive = true

        }
        
        
    }
    
    internal override func updateConstraints() {
        super.updateConstraints()
        productIconWidth.constant = productIcon.frame.height / 2
    }
    
    // MARK: - Functions
    
    private func getPhotoLink()-> String {
        var link: string?
        if let l1 = product?.photo1, !l1.isEmpty {
            link = l1
        } else if let l2 = product?.photo2, !l2.isEmpty {
            link = l2
        } else if let l3 = product?.photo3, !l3.isEmpty {
            link = l3
        } else if let l4 = product?.photo4, !l4.isEmpty {
            link = l4
        }
//        print("===================> \(link ?? .empty)")
        return link ?? .empty
    }
    
    
}




































