
import UIKit

import Localize_Swift
import Foundation

final class Service: NSObject
{
    
    internal var id, title, phone, latitude, longitude, link, email, instagramLink, twitterLink: string?
    
    internal override var description: String {
        return "\n id-> \(id), title-> \(title), phone-> \(phone), latitude-> \(latitude), longitude-> \(longitude), link-> \(link), email-> \(email), instagram-> \(instagramLink), twitter-> \(twitterLink) \n"
    }
    
    override init() {
        super.init()
    }
    
    init(id: string, title: string, phone: string, latitude: string, longitude: string, link: string, email: string, instagram: string, twitter: string) {
        self.id = id
        self.title = title
        self.phone = phone
        self.latitude = latitude
        self.longitude = longitude
        self.link = link
        self.email = email
        self.instagramLink = instagram
        self.twitterLink = twitter
    }
    
    init(dictionary: [string: any]) {
        id = dictionary["id"] as? string
        title = dictionary["title"] as? string
        phone = dictionary["phone"] as? string
        latitude = dictionary["lati"] as? string
        longitude = dictionary["long"] as? string
        link = dictionary["url"] as? string
        email = dictionary["email"] as? string
        instagramLink = dictionary["insta"] as? string
        twitterLink = dictionary["twit"] as? string
    }
    
    // MARK: - Functions
    
    
    internal class func getData(by link: string, viewController: UIViewController?, onComplete: @escaping ([Service])-> void)-> void {
        UIApplication.showIndicator(by: true)        
        if (link.isEmpty || !link.isLink) {
            print("link error")
            viewController?.dialog(with: Constants.Misc.ErrorMessage)
            UIApplication.showIndicator(by: false)
            return
        }
        guard let requestURL = URL(string: link) else {
            print("url error")
            viewController?.dialog(with: Constants.Misc.ErrorMessage)
            UIApplication.showIndicator(by: false)
            return
        }
        
        URLSession.shared.dataTask(with:  URLRequest(url: requestURL)) { (data, response, error) in
            if (error != nil) {
                print("response error")
                viewController?.dialog(with: Constants.Misc.ErrorMessage)
                UIApplication.showIndicator(by: false)
                return
            }
            guard let jsonData = data else {
                print("none data")
                viewController?.dialog(with: Constants.Misc.ErrorMessage)
                UIApplication.showIndicator(by: false)
                return
            }
            
            do {
                if let jsonString = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [[string: any]]{
                    print(jsonString.map { Service(dictionary: $0).phone })
                    onComplete(jsonString.map { Service(dictionary: $0) })
                }
            } catch {
                print("serialization error")
                viewController?.dialog(with: Constants.Misc.ErrorMessage)
                UIApplication.showIndicator(by: false)
            }
        }.resume()
    }
    

}


final class Location {
    internal var latitude, longitude, title: string?
    
    init() {
        
    }
    
    init(lati: string, longi: string, title: string) {
        latitude = lati
        longitude = longi
        self.title = title
    }
}
















