
import UIKit
import Gemo


final class ActivationCode: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var user: User?
    
    
    // MARK: - Outlets
    
    
    @IBOutlet private var textFields: [TextField]!
    @IBOutlet private weak var lblSubtitle: UILabel!
    @IBOutlet private weak var btnSend: UIButton!
    @IBOutlet private weak var btnResendCode: UIButton!
    
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        super.updateUi()
        setupNavigationBar(isLogo: false, isMainPage: false, title: Localization.ActivationCode.localized())
        btnSend.setTitle(Localization.Send.localized(), for: .normal)
        btnResendCode.setTitle(Localization.ReSendCode.localized(), for: .normal)
        lblSubtitle.text = Localization.ActivationCodeSendMessage.localized()
        btnSend.setCorner(radius: 3.0)
        textFields.forEach {
            $0.delegate = self
            $0.textAlignment = .center
            $0.keyboardType = .numberPad
        }
        textFields.first?.becomeFirstResponder()
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    
    // MARK: - Actions
    
    @IBAction private func sendActivationCodeOnCLick(_ sender: UIButton) {
        view.endEditing(true)
        let overlayPanel = UIView()
        overlayPanel.frame = UIScreen.main.bounds
        overlayPanel.backgroundColor = Color.black.withAlphaComponent(0.7)
        overlayPanel.alpha = 0.0
        UIApplication.shared.keyWindow?.addSubview(overlayPanel)
        let icon = UIImageView()
        icon.image = #imageLiteral(resourceName: "tag-faces")
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.contentMode = .scaleAspectFit
        overlayPanel.addSubview(icon)
        icon.widthAnchor.constraint(equalToConstant: 100).isActive = true
        icon.heightAnchor.constraint(equalToConstant: 100).isActive = true
        icon.centerYAnchor.constraint(equalTo: overlayPanel.centerYAnchor, constant: -50).isActive = true
        icon.centerXAnchor.constraint(equalTo: overlayPanel.centerXAnchor).isActive = true
        let label = UILabel()
        label.font = Font.notoKufi(of: 35)
        label.numberOfLines = 2
        label.textColor = .white
        label.textAlignment = .center
        label.text = "\(Localization.PetroActivationWelcomeMessage.localized()) \(user?.name ?? .empty)"
        label.translatesAutoresizingMaskIntoConstraints = false
        overlayPanel.addSubview(label)
        label.topAnchor.constraint(equalTo: icon.bottomAnchor, constant: 25).isActive = true
        label.rightAnchor.constraint(equalTo: overlayPanel.rightAnchor, constant: -40).isActive = true
        label.leftAnchor.constraint(equalTo: overlayPanel.leftAnchor, constant: 40).isActive = true
        let animator = UIViewPropertyAnimator(duration: 2.0, curve: .easeInOut) {
            overlayPanel.alpha = 1.0
        }
        guard let person = user else { return }
        let userModel = UserViewModel()
        animator.addCompletion { (position) in
            switch(position) {
            case .end:
                userModel.signUp(for: person, in: self) { (user, isSuccess) in
                    overlayPanel.alpha = 0.0
                    if (isSuccess) {
                        Threads.main {
                            userModel.save(this: user)
                            UserDefaults.standard.set(true, forKey: Misc.UserLoggedInKey)
                            UserDefaults.standard.synchronize()
                            AppManager.configureSideMenu()
                        }
                    }
                }
            default:
                break
            }
        }
        animator.startAnimation()
        
    }
    
    @IBAction private func reSendActivationCode(_ sender: UIButton) {
        
    }
    

}



// MARK: - Text Field delegate functions

extension ActivationCode: UITextFieldDelegate {
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print(string.characters.count)
        return true
    }
    
}





















