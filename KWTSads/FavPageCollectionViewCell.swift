//
//  FavPageCollectionViewCell.swift
//  KWTSads
//
//  Created by Mostafa on 8/14/16.
//  Copyright © 2016 Mostafa. All rights reserved.
//

import UIKit

class FavPageCollectionViewCell: UICollectionViewCell {
    @IBOutlet var favImg: UIImageView!
    @IBOutlet var favName: UILabel!
    @IBOutlet var favInsertDate: UILabel!
    @IBOutlet var deleteButton: UIButton!
    var arrayID: String!
}
