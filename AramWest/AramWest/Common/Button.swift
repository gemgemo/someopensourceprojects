import UIKit

@IBDesignable
final class Button: UIButton
{

    @IBInspectable
    internal var cornerRadius: CGFloat = 10.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    internal var borderWidth: CGFloat = 1.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    internal var borderColor: UIColor = .gray {
        didSet {
            setBorder()
        }
    }
    
    @IBInspectable
    internal var isBorder: Bool = false {
        didSet {
            if (isBorder) {
                setBorder()
            }
        }
    }

    internal override func draw(_ rect: CGRect) {
        clipsToBounds = true
        layer.cornerRadius = cornerRadius
    }
    
    private func setBorder()-> void {
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
    }


}
