
import UIKit
import Localize_Swift

final class KeeperCell: UITableViewCell, CellProvider
{
    // MARK: - Constants
    
    // MARK: - Variables
    
    internal var keeper: Keeper? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var container: UIView!
    @IBOutlet private weak var lblName: UILabel!
    @IBOutlet private weak var lblRank: UILabel!
    @IBOutlet private weak var lblNotes: UILabel!
    
    
    // MARK: - Overridden functions
    
    internal func updateUI() {
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            lblName.textAlignment = .right
            lblRank.textAlignment = .right
            lblNotes.textAlignment = .right
            container.semanticContentAttribute = .forceLeftToRight
        } else {
            lblName.textAlignment = .left
            lblRank.textAlignment = .left
            lblNotes.textAlignment = .left
            container.semanticContentAttribute = .forceRightToLeft
        }
        container.clipsToBounds = true
        let cornerRadius: cgFloat = 5.0
        container.layer.cornerRadius = cornerRadius
        container.layer.masksToBounds = true
        container.setShadow(with: Size(right: 0, top: 1), radius: 2, opacity: 0.8, color: .lightGray)
        container.layer.masksToBounds = false
        
        lblName.text = "\("namelan".localized()): \(keeper?.name ?? string.empty)"
        lblRank.text = "\("Rankinglan".localized()): \(keeper?.rank ?? string.empty)"
        if let school = keeper?.school {
            lblNotes.text = "\("schoollan".localized()): \(school)"
        }
        
        if let note = keeper?.details {
            lblNotes.text = "\("detailslan".localized()): \(note)"
        }

    }
    
    // MARK: - Actions

}



















