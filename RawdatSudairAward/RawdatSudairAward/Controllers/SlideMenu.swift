
import UIKit
import Localize_Swift

final class SlideMenu: BaseController
{

    // MARK: - Constants
    
    //MARK: - Variables
    
    fileprivate var items = Array<MenuItem>(), selectedRow = 0
    
    // MARK: - Outlets
    
    @IBOutlet private weak var titleContainer: UIView!
    @IBOutlet fileprivate weak var titleImage: UIImageView!
    @IBOutlet private weak var titleViewHeight: NSLayoutConstraint!
    @IBOutlet fileprivate weak var tblItems: UITableView! {
        didSet {
            tblItems.delegate = self
            tblItems.dataSource = self
        }
    }
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        titleViewHeight.constant = view.bounds.height*0.30
    }
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUI()
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    internal override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Actions
    
}




// MARK: - Helper functions

extension SlideMenu
{
    
    fileprivate func configureTableView()-> void {
        tblItems.register(Constants.Nib.Menu, forCellReuseIdentifier: Constants.ReuseIdentifier.Menu)
    }
    
    @objc fileprivate func updateUI()-> void {
        items = MenuItem.getItems()
        tblItems.reloadData()
        tblItems.selectRow(at: IndexPath(row: selectedRow, section: 0), animated: true, scrollPosition: .top)
        titleImage.translatesAutoresizingMaskIntoConstraints = false
        
    }
    
}



// MARK: - Table view delegate & data source functions 

extension SlideMenu: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.Menu) as? MenuCell {
            cell.item = items[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        revealViewController().revealToggle(animated: true)
        switch (indexPath.row) {
        case 0: // open main screen
            selectedRow = indexPath.row
            if let mainScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.MainNavigationBar) as? UINavigationController {
                revealViewController().pushFrontViewController(mainScreen, animated: true)
            }
            
        case 1: // open about app
            selectedRow = indexPath.row
            if let aboutScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.AboutApp) as? AboutApp {
                revealViewController().pushFrontViewController(UINavigationController(rootViewController: aboutScreen), animated: true)
            }
            
        case 2: // share app
            selectedRow = indexPath.row
            share(this: [Constants.Misc.AppLink])
            
        case 3: // change language
            changeLanguage()
            
        default:
            break
        }
    }
    
    
}























