

import Foundation


final class School
{
    
    internal var id, title, icon, cover: string?
    
    init(dictionary: [string: any]) {
        id = dictionary["id"] as? string
        title = dictionary["title"] as? string
        icon = dictionary["photo"] as? string
        cover = dictionary["image"] as? string
    }
    
    internal class func getData(by link: string, viewController: UIViewController?, onComplete: @escaping([School])-> void)-> void {
        UIApplication.showIndicator(by: true)
        HttpClient.request(link: link, method: .get) { (result, response, error) -> void in
            if (error != nil) {
                (viewController as? BaseController)?.showAlert(with: string.empty, and: Constants.Misc.ErrorMessage)
                return
            }
            
            if let data = result as? [[string: any]] {
                onComplete(data.map{ School(dictionary: $0) })
            } else {
                print("none data")
            }
        }
    }
    
    
}
