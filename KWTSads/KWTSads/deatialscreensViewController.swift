
import UIKit
import Alamofire
import Social
import SwiftSpinner
import MessageUI
import MapKit
import CoreLocation

let RateCellId = "com.rates.cell.view.id"

class deatialscreensViewController: UIViewController ,
    UICollectionViewDataSource ,
    UICollectionViewDelegate ,
    MFMessageComposeViewControllerDelegate ,
    MFMailComposeViewControllerDelegate,
    UITextFieldDelegate
{
    
    var departmentSids: String!
    let imagesUrl = "http://142.4.4.149/~kwtads/all_images/"
    var images_cache = [String:UIImage]()
    var detialImg = [String]()
    var tel = [String]()
    var detialLikes: String!
    var detialsName: String!
    var detialsEdate: String!
    var detialsFdate: String!
    var detialLatitude: String!
    var detiallongitude: String!
    var detailWeb: String!
    var tel2: String!
    var rateValue = 0
    internal var isExpired = false
    
    @IBOutlet weak var endDate: UILabel!
    @IBOutlet weak var fromDate: UILabel!
    @IBOutlet weak var adsTitle: UILabel!
    @IBOutlet weak var webLabel: UILabel!
    @IBOutlet weak var mobLabel: UILabel!
    @IBOutlet weak var adsLabel: UILabel!
    @IBOutlet weak var telLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var tel1: UILabel!
    @IBOutlet weak var tblReviews: UITableView! {
        didSet {
            tblReviews.delegate = self
            tblReviews.dataSource = self
            tblReviews.alwaysBounceVertical = false
            tblReviews.alwaysBounceHorizontal = false
        }
    }
    @IBOutlet weak var btnAddRateAndComment: UIButton!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet weak var ratesHeight: NSLayoutConstraint! { didSet { ratesHeight.constant = 0.0 }}
    @IBOutlet weak var webSiteLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 80, right: 0)
        }
    }
    @IBOutlet weak var imgLikes: UIImageView!
    var address = [String]()
    @IBOutlet weak var labelLikes: UILabel!
    let defaults = UserDefaults.standard
    @IBOutlet weak var detialScreenCollectionView: UICollectionView!
    @IBOutlet weak var rateCommentPanel: UIView!
    @IBOutlet var stars: [UIButton]!
    @IBOutlet weak var txtComment: TextView!
    @IBOutlet weak var txfUsername: UITextField! { didSet { txfUsername.delegate = self } }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("end editing")
        textField.resignFirstResponder()
        return true
    }
    
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    var selectedIndex = 0
    fileprivate var ratesDataSource = Array<Rate>() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tblReviews.reloadData()
                self?.ratesHeight.constant = CGFloat(80*self!.ratesDataSource.count)
                self?.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction private func addRateAndComment(_ sender: UIButton) {
        if (isExpired) {
            let alert  = UIAlertController(title: "تحذير", message: "اعلان منتهي", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "غلق", style: .cancel))
            present(alert, animated: true)
        } else {
            rateCommentPanel.isHidden = false
        }
    }
    
    @IBAction func sendRateCommentOnCLick(_ sender: UIButton) {
        guard let username = txfUsername.text else { return }
        guard !txtComment.text.isEmpty else { return }
        guard let url = URL(string: "http://142.4.4.149/~kwtads/w_m_s/make_comment.php") else { return }
        let parameters: Dictionary<String, Any> = [
            "device_id": UIDevice.current.identifierForVendor?.description ?? "",
            "adv_id": departmentSids ?? "",//UserDefaults.standard.string(forKey: "departmentSid")!
            "visitor_name": username,
            "comment": txtComment.text,
            "rate": rateValue
        ]
        print("rate and comment paramters \(String(describing: parameters))")
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        var params = ""
        parameters.forEach{ params += "\($0)=\($1)&"}
        let p = params.substring(to: params.index(params.endIndex, offsetBy: -1))
        print("params \(p)")
        request.httpBody = p.data(using: .utf8)
        URLSession.shared
            .dataTask(with: request) { (result, response, error) in
                if (error != nil) {
                    print("send comment error \(String(describing: error))")
                    return
                }
                guard let jsonData = result else {
                    print("nix json data")
                    return
                }
                print("comment replay: \(String(describing: String(data: jsonData, encoding: .utf8)))")
                do {
                    if let json = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? Dictionary<String, Any> {
                        print("send comment and rate response \(String(describing: json))")
                        DispatchQueue.main.async { [weak self] in self?.rateCommentPanel.isHidden = true }
                        if let isSuccess = json["result"] as? Bool, isSuccess {
                            print("successfully result", isSuccess)
                            DispatchQueue.main.async { [weak self] in
                                self?.ratesDataSource.removeAll()
                                self?.loadDetialAds()
                            }
                        } else {
                            DispatchQueue.main.async { [weak self] in
                                self?.displayAlertMessage("لديك تقييم لهذا المنتج بالفعل")
                            }
                        }
                    } else {
                        print("none data when fetch comments")
                    }
                } catch {
                    print("catch error \(error)")
                }
            }.resume()
        
    }
    
    @IBAction func closeRatePanelOnCLick(_ sender: UIButton) {
        rateCommentPanel.isHidden = true
    }
    
    @IBAction func setRatesOnClick(_ sender: UIButton) {
        stars.forEach { $0.setImage(#imageLiteral(resourceName: "starborder"), for: .normal) }
        rateValue = sender.tag
        for i in stride(from: 0, to: sender.tag, by: 1) {
            stars[i].setImage(#imageLiteral(resourceName: "starfill"), for: .normal)
        }
    }
    
    
    private func setupRateCommentPanel()-> Void {
        rateCommentPanel.frame = UIScreen.main.bounds
        UIApplication.shared.keyWindow?.addSubview(rateCommentPanel)
        rateCommentPanel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ratePanelTapped(_:))))
        rateCommentPanel.isHidden = true
    }
    
    @objc private func ratePanelTapped(_ gesture: UITapGestureRecognizer) {
        print("rate panel tapped")
        gesture.view?.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRateCommentPanel()
        if Reachability.isConnectedToNetwork() == true {
            let callOneTap = UITapGestureRecognizer(target: self, action: #selector(callPressed1(_:)))
            telLabel.addGestureRecognizer(callOneTap)
            let callTwoTap = UITapGestureRecognizer(target: self, action: #selector(callPressed2(_:)))
            tel1.addGestureRecognizer(callTwoTap)
            labelLikes.layer.cornerRadius = 12
            labelLikes.layer.masksToBounds = true
            scrollView.contentSize.height = 800
            detialScreenCollectionView.backgroundColor = UIColor.white.withAlphaComponent(0)
            self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "ac_back-arrow")
            self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "ac_back-arrow")
            self.navigationController?.navigationBar.tintColor = UIColor(red: 0.96, green: 0.69, blue:  0.25, alpha: 1 )
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
            detialScreenCollectionView.dataSource = self
            detialScreenCollectionView.delegate = self
            SwiftSpinner.show("Loading...")
            
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage
            //loadDetialAds()
            //loadImages()
        } else {
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage
            let err = "Internet connection FAILED"
            self.displayAlertMessage(err)
        }
        
    }
    
    internal override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)        
    }

    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Reachability.isConnectedToNetwork() == true {
            loadDetialAds()
            loadImages()
        } else {
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage
            let err = "Internet connection FAILED"
            self.displayAlertMessage(err)
        }
    }
    
    func loadImages() {
        //if let departsiD = departmentSids
        if let departsiD = departmentSids {
            let urlStrings = "\(baseUrl)v_adv_imgs.php?adv_id=\(departsiD)"
            print("images url", urlStrings)
            let sessions = URLSession.shared
            let urls = URL(string: urlStrings)!
            sessions.dataTask(with: urls, completionHandler:
                { (data :Data?, response :URLResponse?, error:Error?) in
                    if let responseData = data {
                        do {
                            let json = try JSONSerialization.jsonObject(with: responseData, options: .allowFragments)
                            print("this image\(json)")
                            if let dict = json as? NSDictionary {
                                let results = dict["result"] as! String
                                if (results != "false") {
                                    let data = dict["data"] as! NSArray
                                    for i in 0..<data.count {
                                        if let dict = data[i] as? NSDictionary {
                                            self.detialImg.append(dict["img"] as! String)
                                        }
                                    }
                                }
                                DispatchQueue.main.async {
                                    //print(self.detialImg)
                                    self.detialScreenCollectionView.reloadData()
                                }
                            }
                        } catch {
                            print ("Could not Serialize")
                        }
                    }
            }).resume()
        }
    }
    
    @IBAction private func openSiteOnClick(_ sender: UIButton) {
        guard let url = URL(string: webSiteLabel.text ?? "") else { return }
        guard UIApplication.shared.canOpenURL(url) else { return }
        UIApplication.shared.open(url, options: [:])
    }
    
    @IBOutlet private weak var mapHeight: NSLayoutConstraint!
    @IBOutlet private weak var midPanel: NSLayoutConstraint!
    
    private func loadDetialAds()-> Void  {
        if let departsiD = departmentSids { //UserDefaults.standard.string(forKey: "departmentSid")
            let urlString = "\(baseUrl)v_adv.php?adv_id=\(departsiD)&lang=0"
            print("selected link: \(urlString)")
            let session = URLSession.shared
            let url = URL(string: urlString)!
            session.dataTask(with: url, completionHandler: { (data, response, error) in
                    if let responseData = data {
                        do {
                            let json = try JSONSerialization.jsonObject(with: responseData, options: .allowFragments)as! Dictionary<String, Any>
                            print("ad details \(json)")
                            let data = json["data"] as! NSArray
                            if let dict = data[0] as? NSDictionary {
                                self.detialsName = dict["adv_name"] as! String
                                self.detialsFdate = dict["from_date"] as! String
                                self.detialsEdate = dict["expire_date"] as! String
                                self.detialLikes = dict["likes"] as! String
                                self.detialLatitude = dict["latitude"] as! String
                                self.detiallongitude = dict["longitude"] as! String
                                self.detialImg.append(dict["img_adv"] as! String)
                                self.address.append(dict["address"] as! String)
                                self.tel.append(dict["tel"] as! String)
                                self.detailWeb = dict["web_site"] as! String
                                self.tel2 = dict["tel2"] as! String
                                print("comments: \(String(describing: dict["comments"]))")
                                self.ratesDataSource.removeAll()
                                if let commentsObjects = dict["comments"] as? [NSDictionary] {
                                    if (commentsObjects.count > 0) {
                                        for item in commentsObjects {
                                            self.ratesDataSource.append(Rate(JSON: (item as? Dictionary<String, Any>)!))
                                            DispatchQueue.main.async { [weak self] in
                                                guard let this = self else { return }
                                                this.scrollView.contentInset = UIEdgeInsets(top: 0,
                                                                                            left: 0,
                                                                                            bottom: CGFloat(commentsObjects.count*80),
                                                                                            right: 0)
                                            }
                                        }
                                    }
                                }
                            }
                            DispatchQueue.main.async {
                                self.adsTitle.text = self.detialsName
                                self.fromDate.text = self.detialsFdate
                                self.endDate.text = self.detialsEdate
                                let departmentIDs = UserDefaults.standard.string(forKey: "departmentID")!
                                let dict = data[0] as! Dictionary<String, Any>
                                guard let catId = dict["cat_id"] as? String else { return }
                                if (catId == "2") {
                                    print("department id is: \(departmentIDs)")
                                    self.labelLikes.text = self.detialLikes
                                    self.adsLabel.isHidden = true
                                    self.mobLabel.isHidden = true
                                    self.webLabel.isHidden = true
                                    self.webSiteLabel.isHidden = true
                                    self.addressLabel.isHidden = true
                                    self.telLabel.isHidden = true
                                    self.tel1.isHidden = true
                                    self.mapView.isHidden = true
                                    self.midPanel.constant = 0.0
                                    self.mapHeight.constant = 0.0
                                    self.view.layoutIfNeeded()
                                } else {
                                    /*if (self.detialLatitude == nil || self.detialLatitude == "" ) {
                                     let location = CLLocationCoordinate2DMake(29.369720, 47.978330)
                                     let span = MKCoordinateSpanMake(0.0002, 0.0002)
                                     let region = MKCoordinateRegion(center: location, span: span)
                                     self.mapView.setRegion(region, animated: true)
                                     
                                     let annotation = MKPointAnnotation()
                                     annotation.coordinate = location
                                     annotation.title = (self.detialsName)
                                     print("fixed selected coordinate: \(location)")
                                     if self.address[0] == ""{
                                     annotation.subtitle = ""
                                     } else {
                                     annotation.subtitle = (self.address[0])
                                     }
                                     self.mapView.addAnnotation(annotation)
                                     } else {*/
                                    if let latitude = Double(self.detialLatitude), let longitude = Double(self.detiallongitude) {
                                        let location = CLLocationCoordinate2DMake( latitude, longitude)
                                        print("dynamic selected coordinate: \(location)")
                                        let span = MKCoordinateSpanMake(0.0002, 0.0002)
                                        let region = MKCoordinateRegion(center: location, span: span)
                                        self.mapView.setRegion(region, animated: true)
                                        let annotation = MKPointAnnotation()
                                        annotation.coordinate = (location)
                                        annotation.title = (self.detialsName)
                                        annotation.subtitle = (self.address[0])
                                        self.mapView.addAnnotation(annotation)
                                    }
                                    //}
                                    self.addressLabel.text = self.address[0]
                                    self.telLabel.text = self.tel[0]
                                    self.tel1.text! = self.tel2!
                                    self.webSiteLabel.text = self.detailWeb
                                    self.labelLikes.isHidden = true
                                    self.imgLikes.isHidden = true
                                }
                                self.detialScreenCollectionView.reloadData()
                                SwiftSpinner.hide()
                            }
                        } catch {
                            print ("Could not Serialize resone: \(error)")
                            SwiftSpinner.hide()
                        }
                    }
                    
            }).resume()
        }
      
    }
    
    @IBAction private func setLikeOnCLick(_ sender: UIButton) {
        let link = "http://142.4.4.149/~kwtads/w_m_s/v_adv_like.php?device_id=\(UIDevice.current.identifierForVendor!.uuidString)&adv_id=\(departmentSids!)&lang=0"
        guard let url = URL(string: link) else { return }
        URLSession.shared.dataTask(with: url) { [weak self] (data, resposne, error) in
            if (error != nil) {
                self?.displayAlertMessage(error!.localizedDescription)
                return
            }
            guard let jsonData = data else { return }
            do {
                guard let json = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? Dictionary<String, Any> else { return }
                print("set like json is: \(String(describing: json))")
                guard let message = json["data"] as? String else { return }
                self?.displayAlertMessage(message)
                self?.loadDetialAds()
            } catch {
                self?.displayAlertMessage(error.localizedDescription)
            }
        }.resume()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "detialScreenCell", for: indexPath) as! DetialScreenCollectionViewCell
        print("=-=-=-=-=-=-=-=-=-=->>>> image url \(imagesUrl+detialImg[indexPath.row])")
        load_image(imagesUrl+detialImg[indexPath.row], imageview: cell.detailImg)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return detialImg.count
    }
    
    func load_image(_ link:String, imageview:UIImageView)
    {
        
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.timeoutInterval = 10
        
        
        let task = session.dataTask(with: url, completionHandler:
        { (data :Data?, response :URLResponse?, error:Error?) in
            
            
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                
                return
            }
            
            
            var image = UIImage(data: data!)
            
            if (image != nil)
            {
                
                
                func set_image()
                {
                    self.images_cache[link] = image
                    imageview.image = image
                }
                
                
                DispatchQueue.main.async(execute: set_image)
                
            }
            
        })
        
        task.resume()
        
    }
    
    
    
    func refresh()
    {
        self.detialScreenCollectionView.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let myImageViewPage:MyImageViewerViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyImageView") as! MyImageViewerViewController
        myImageViewPage.selectedImage = imagesUrl+self.detialImg[(indexPath as NSIndexPath).row]
        self.navigationController?.pushViewController(myImageViewPage, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let screenSize: CGRect = self.detialScreenCollectionView.bounds
        
        return CGSize(width: screenSize.width, height: screenSize.height)
    }
    
    func displayAlertMessage(_ userMessage:String)
    {
        let myAlert = UIAlertController(title: "Alert", message:userMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }
    func callPressed1(_: UITapGestureRecognizer){
        if let url = URL(string: "tel://\(self.tel[0])") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        
    }
    func callPressed2(_: UITapGestureRecognizer){
        if let url = URL(string: "tel://\(self.tel2!)") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        
    }
    
    @IBAction func FavTapped(_ sender: AnyObject) {
        
        let deviceId = UserDefaults.standard.string(forKey: "MyDeviceID")!
        
        let advID = departmentSids!//UserDefaults.standard.string(forKey: "departmentSid")!
        
        let urlString = "\(baseUrl)favorite_add.php?device_id=\(deviceId)&id_f=\(advID)&lang=0"
        
        Alamofire.request(urlString, method: .post).validate().responseJSON
            { response in switch response.result {
            case .success(let JSON):
                print("Success with JSON: \(JSON)")
                
                
                let response = JSON as! NSDictionary
                
                let userMessage = response["data"] as? String
                self.displayAlertMessage(userMessage!)
                
                
            case .failure(let error):
                print("Request failed with error: \(error)")
                }
        }
    }
    
    @IBAction func twitterTapped(_ sender: AnyObject) {
        post(toService: SLServiceTypeTwitter)
    }
    
    @IBAction func facebookTapped(_ sender: AnyObject) {
        post(toService: SLServiceTypeFacebook)
    }
    
    @IBAction func whatsappTapped(_ sender: AnyObject) {
        let urlString = "رقم الهاتف: "+tel[0]+"\nالعنوان: "+address[0]+"\nاعلان: "+(imagesUrl+detialImg[0])
        let urlStringEncoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let url  = URL(string: "whatsapp://send?text=\(urlStringEncoded!)!")
        
        if UIApplication.shared.canOpenURL(url!) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        } else {
            self.displayAlertMessage("Your device is not able to send WhatsApp messages")
        }
    }
    
    @IBAction func gmailTapped(_ sender: AnyObject) {
        if !MFMailComposeViewController.canSendMail() {
            print("Mail services are not available")
            return
        } else {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            // Configure the fields of the interface.
            composeVC.setToRecipients(["example@example.com"])
            composeVC.setSubject("  ")
            composeVC.setMessageBody("رقم الهاتف: "+tel[0]+"\nالعنوان: "+address[0]+"\nاعلان: "+(imagesUrl+detialImg[0]), isHTML: false)
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func mailTapped(_ sender: AnyObject) {
        if !MFMailComposeViewController.canSendMail() {
            print("Mail services are not available")
            return
        }
        else
        {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            
            // Configure the fields of the interface.
            composeVC.setToRecipients(["example@example.com"])
            composeVC.setSubject("  ")
            composeVC.setMessageBody("رقم الهاتف: "+tel[0]+"\nالعنوان: "+address[0]+"\nاعلان: "+(imagesUrl+detialImg[0]), isHTML: false)
            
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
            
        }
        
        //        if (MFMessageComposeViewController.canSendText()) {
        //            let controller = MFMessageComposeViewController()
        //            controller.body = "رقم الهاتف: "+tel[0]+"\nالعنوان: "+address[0]+"\nاعلان:"+detialImg[0]
        //            controller.recipients = ["    "]
        //            controller.messageComposeDelegate = self
        //            self.present(controller, animated: true, completion: nil)
        //        }
        //        else
        //        {
        //            self.displayAlertMessage("Your device is not able to send  messages")
        //
        //        }
        
    }
    @IBAction func DepartTapped(_ sender: AnyObject) {
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "departmentPage") as! DepartmentsViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    @IBAction func MenuTapped(_ sender: AnyObject) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuPage") as! MenuPageViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dailyNew(_ sender: AnyObject) {
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "DailyNews") as! DailyNewsViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func favPage(_ sender: AnyObject) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "FavPage") as! FavPageViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
        
    }
    
    func post(toService service: String) {
        if(SLComposeViewController.isAvailable(forServiceType: service)) {
            let socialController = SLComposeViewController(forServiceType: service)
            socialController?.setInitialText("رقم الهاتف: "+tel[0]+"\nالعنوان: "+address[0]+"\nاعلان: \(imagesUrl+detialImg[0])")
            present(socialController!, animated: true)
        }
    }
    
    @IBAction func shareButtonTapped(_ sender: AnyObject) {
        let items = ["رقم الهاتف: "+tel[0], "\nالعنوان: "+address[0], "اعلان:  \n\n\n \(imagesUrl+detialImg[0])"] as [Any]
        let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        activityViewController.excludedActivityTypes = [ .airDrop, .postToFacebook ]
        present(activityViewController, animated: true)
        
    }
    
    @IBAction func nextButton(_ sender: AnyObject) {
        //get cell size
        let cellSize = CGSize(width: self.view.frame.width, height: self.view.frame.height);
        //get current content Offset of the Collection view
        let contentOffset = detialScreenCollectionView.contentOffset;
        
        //scroll to next cell
        detialScreenCollectionView.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width-10, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true);
    }
    
    @IBAction func backButton(_ sender: AnyObject) {
        
        //get cell size
        let cellSize = CGSize(width: self.view.frame.width, height: self.view.frame.height);
        
        //get current content Offset of the Collection view
        let contentOffset = detialScreenCollectionView.contentOffset;
        
        //scroll to next cell
        detialScreenCollectionView.scrollRectToVisible(CGRect(x: contentOffset.x - cellSize.width-10, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true);
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}



// MARK: - Table view delegate & data source functions

extension deatialscreensViewController: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ratesDataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: RateCellId) as? RateCell else { return UITableViewCell() }
        cell.rate = ratesDataSource[indexPath.row]
        return cell
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}



// MARK: - Rate model

final class Rate
{
    internal var id, adId, rate, comment, deviceId, commenter: String?
    
    init(JSON: Dictionary<String, Any>) {
        id = JSON["id"] as? String
        adId = JSON["adv_id"] as? String
        rate = JSON["rate"] as? String
        comment = JSON["comment"] as? String
        deviceId = JSON["device_id"] as? String
        commenter = JSON["visitor_name"] as? String
    }
    
    
}




































