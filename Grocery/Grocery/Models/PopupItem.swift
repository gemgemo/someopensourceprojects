
import Foundation
import Gemo


final class PopupItem
{
    
    internal var id: int?, title: string?
    
    init(id: int?, title: string?) {
        self.id = id
        self.title = title
    }
    
    init() { }
    
    internal class func editsItems(_ completion: @escaping([PopupItem])->())-> void {
        Threads.background(QOS: .default) { 
            var items = Array<PopupItem>()
            items.append(PopupItem(id: 0, title: Localization.Edit.localized()))
            items.append(PopupItem(id: 1, title: Localization.Delete.localized()))
            completion(items)
        }
    }
    
}

































