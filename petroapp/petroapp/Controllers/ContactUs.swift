
import UIKit
import Gemo

class ContactUs: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblWhatsapp: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    internal override func updateUi() {
        super.updateUi()
        setupNavigationBar(isLogo: false, isMainPage: true, title: Localization.ContactUs.localized())
        
    }
    
    
    // MARK: - Actions

}


// MARK: - Helper functions

fileprivate extension ContactUs {
    
    func loadData()-> void {
        guard Gemo.gem.isConnected else {
            displayAlert(with: Localization.InternetConnection.localized())
            return
        }
        Gemo.gem.startSpinning()
        Http.request(link: Misc.getUrl(for: "setting"), method: .post, parameters: [Misc.MainKey: Misc.MainValue])
            .response { [weak self] (response) in
                let error = response.error
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    self?.displayAlert(with: error?.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
                guard let json = response.result as? Dictionary<string, any> else {
                    self?.displayAlert(with: Localization.NoData.localized())
                    return
                }
                Threads.main { [weak self] in
                    self?.lblPhone.text = json["phone"] as? string ?? .empty
                    self?.lblEmail.text = json["email"] as? string ?? .empty
                    self?.lblWhatsapp.text = json["whatsapp"] as? string ?? .empty
                }
        }
    }
    
}























