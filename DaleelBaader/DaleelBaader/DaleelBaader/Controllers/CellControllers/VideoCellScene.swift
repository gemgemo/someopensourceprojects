
import UIKit

import Localize_Swift


final class VideoCellScene: UITableViewCell
{
    // MARK: - Constants
    
    //MARK: - Variables
    
    internal var video: Movie? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var poster: UIImageView! {
        didSet {
            poster.layer.borderColor = Color(white: 0.80, alpha: 0.9).cgColor
            poster.layer.borderWidth = 1.0
        }
    }
    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var divider: UIView! {
        didSet {
            divider.backgroundColor = Color(white: 0.80, alpha: 1.0)
        }
    }
    
    
    // MARK: - Functions
    
    private func updateUI()-> void {
        if let vName = video?.title {
            title.text = vName
        }
        
        if let vidLink = video?.link {
            if (vidLink.contains("https://www.youtube.com")) {
                let youtubeVidId = vidLink.components(separatedBy: "=").last ?? string.empty
                poster.loadImage(from:   "https://img.youtube.com/vi/\(youtubeVidId)/0.jpg", onComplete: nil)
            } else {
                /*let movLink = "http://www37.online-convert.com/download-file/aa921cf2be00dcb837620031aeec8e30/converted-233183c4.mov"
                 let aviLink = "http://www.engr.colostate.edu/me/facil/dynamics/files/cbw3.avi"
                 let mp4Link = "http://techslides.com/demos/sample-videos/small.mp4"
                 let flvLink = "http://www.sample-videos.com/video/flv/480/big_buck_bunny_480p_5mb.flv"
                 let threegpLink = "http://techslides.com/demos/sample-videos/small.3gp"
                 let mkvLink = "http://www.sample-videos.com/video/mkv/480/big_buck_bunny_480p_2mb.mkv"*/
                UIApplication.generateThumb(fromVideo: vidLink) { (image) in
                    DispatchQueue.main.async {
                        self.poster.image = image
                    }
                    UIApplication.showIndicator(by: false)
                }
            }
        }
    }
    
    
    //MARK: - Actions
    
    
}
















