
import UIKit
import Alamofire

final class SendMailVC: UIViewController
{
    
    // MARK: - Constants
    
    private let link = "http://192.232.214.91/~almglsalbldyinmk//contact_us.php?key=albldy"
    
    // MARK: - Variables
    
    fileprivate var placeholderColor = UIColor.lightGray
    fileprivate var placeholder =  ""
    
    // MARK: - Outlets
    
    @IBOutlet private weak var txfTitle: UITextField! {
        didSet {
            txfTitle.delegate = self
        }
    }
    @IBOutlet private weak var txtContent: UITextView! {
        didSet {
           txtContent.delegate = self
        }
    }
    @IBOutlet private weak var btnSend: UIButton!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    // MARK: - Actions
    
    @IBAction private func sendMailOnClick(_ sender: UIButton) {
        guard !msgTitle.isEmpty && !msgContent.isEmpty else {
            AlertMessage("تأكد من الحقول المدخله", "")
            return
        }
        if (Settings.isConnected) {
            sendMessage { [weak self] (message) in
                DispatchQueue.main.async { [weak self] in
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self?.AlertMessage(message, "")
                }
            }
        } else {
            dialogUser(with: "لا يوجد انترنت")
        }
    }
    
    
    
    // MARK: - Functions 
    
    private func updateUi()-> Void {
        placeholder = "نص الرسالة"
        placeholderColor = .lightGray
        btnSend.layer.cornerRadius = 10.0
        btnSend.layer.masksToBounds = true
        txtContent.layer.cornerRadius = 10.0
        txtContent.clipsToBounds = true
        txtContent.layer.borderColor = UIColor(white: 0.88, alpha: 0.8).cgColor
        txtContent.layer.borderWidth = 1.0
        txtContent.text = placeholder
        txtContent.textColor = placeholderColor
    }
    
    @objc private func goBack()-> Void {
        dismiss(animated: true, completion: nil)
    }
    
    private func AlertMessage(_ Message:String,_ Error: String) {
        var t = ""
        var b = ""
        if Settings.lang == "ar" {
            t = "تنبيه"
            b = "إغلاق"
        } else {
            t = "Alert"
            b = "Close"
        }
        if Error == "" {
            let myAlert = UIAlertController(title: t, message:Message, preferredStyle: UIAlertControllerStyle.alert);
            let okAction = UIAlertAction(title: b, style: .default) { [weak self] (action) in
                if let mainScreen = self?.storyboard?.instantiateViewController(withIdentifier: "mainNavView") as? UINavigationController {
                    (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController = mainScreen
                 }
                //self?.navigationController?.popToRootViewController(animated: true)
                //self?.dismiss(animated: true) { }
            }
            myAlert.addAction(okAction);
            self.present(myAlert, animated: true, completion: nil)
            
        } else {
            let myAlert = UIAlertController(title: t, message:Error, preferredStyle: UIAlertControllerStyle.alert);
            let okAction = UIAlertAction(title: b, style: .default, handler:nil)
            myAlert.addAction(okAction);
            self.present(myAlert, animated: true, completion: nil)
        }
        
        
    }
    
    private func setupNavigationBar()-> Void {
        navigationItem.title = ""
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_close_white"), style: .plain, target: self, action: #selector(goBack))
    }
    
    private var msgTitle: String {
        return txfTitle.text?.trimmingCharacters(in: .whitespaces) ?? ""
    }
    
    private var msgContent: String {
        if (txtContent.text.trimmingCharacters(in: .whitespaces) == placeholder) {
            return ""
        }
        return txtContent.text.trimmingCharacters(in: .whitespaces)
    }
    
    private func sendMessage(complationHandler: @escaping(String)-> Void )-> Void {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        Alamofire.request(link, method: .post, parameters: [ "subject": msgTitle, "message": msgContent ]).responseJSON { [weak self] (response) in
            let result = response.result
            if (result.error != nil) {
                self?.AlertMessage("", "حدث خطأ")
                return
            }
             //print("response value is: ", result.value)
            if let value = (result.value as? [[String: Any]])?.first, let message = value["data"] as? String {
                complationHandler(message)
            } else {
                print("null data")
            }
        }
    }
    
}



// MARK: - Text Fields Delegate functions

extension SendMailVC: UITextFieldDelegate
{
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}



//MARK: - Text view delegate functions
extension SendMailVC: UITextViewDelegate
{
    
    internal func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text == placeholder && textView.textColor == placeholderColor) {
            textView.text = ""
            textView.textColor = .black
        }
    }
    
    internal func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text.isEmpty) {
            textView.text = placeholder
            textView.textColor = placeholderColor
        }
    }
    
    
    
}







