
import UIKit
import Gemo

final class UserType: BaseController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    private var usertype = Usertype.main
    
    // MARK: - Outlets
    
    @IBOutlet private weak var lblWelcome: UILabel!
    @IBOutlet private weak var logoHeight: NSLayoutConstraint!
    @IBOutlet private weak var mainUserPanel: UIView!
    @IBOutlet private weak var mainUserIcon: UIImageView!
    @IBOutlet private weak var btnMainUser: UIButton!
    @IBOutlet private weak var rdMainUser: RadioView!
    @IBOutlet private weak var subuserPanel: UIView!
    @IBOutlet private weak var subuserIcon: UIImageView!
    @IBOutlet private weak var btnSubuser: UIButton!
    @IBOutlet private weak var rdSubuser: RadioView!
    @IBOutlet private weak var footerPanel: UIView!
    @IBOutlet private weak var nextIcon: UIImageView!
    @IBOutlet private weak var btnContinue: UIButton!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        usertype = .main
        UserDefaults.standard.set(usertype.rawValue, forKey: Misc.UserTypeKey)
        UserDefaults.standard.synchronize()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = false
        navigationController?.navigationBar.isHidden = true
    }
    
    internal override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.isHidden = false
    }        
    
    internal override func updateUi() {
        lblWelcome.text = Localization.WelcomePetroApp.localized()
        btnMainUser.setTitle(Localization.MainUser.localized(), for: .normal)
        btnSubuser.setTitle(Localization.SubOrDelegateUser.localized(), for: .normal)
        btnContinue.setTitle(Localization.Continuation.localized(), for: .normal)
        footerPanel.setShadow(with: .zero, radius: 0.7, opacity: 0.8, color: .main)
        if (Localizer.instance.current == .arabic) {
            btnMainUser.contentHorizontalAlignment = .right
            btnSubuser.contentHorizontalAlignment = .right
            nextIcon.rightAnchor.constraint(equalTo: footerPanel.rightAnchor).isActive = true
            //
            mainUserIcon.rightAnchor.constraint(equalTo: mainUserPanel.rightAnchor, constant: 0).isActive = true
            btnMainUser.rightAnchor.constraint(equalTo: mainUserIcon.leftAnchor, constant: -8).isActive = true
            rdMainUser.leftAnchor.constraint(equalTo: mainUserPanel.leftAnchor, constant: 8).isActive = true
            btnMainUser.leftAnchor.constraint(equalTo: rdMainUser.rightAnchor, constant: 8).isActive = true
            //
            subuserIcon.rightAnchor.constraint(equalTo: subuserPanel.rightAnchor, constant: 0).isActive = true
            btnSubuser.rightAnchor.constraint(equalTo: subuserIcon.leftAnchor, constant: -8).isActive = true
            rdSubuser.leftAnchor.constraint(equalTo: subuserPanel.leftAnchor, constant: 8).isActive = true
            btnSubuser.leftAnchor.constraint(equalTo: rdSubuser.rightAnchor, constant: 8).isActive = true
        } else {
            btnMainUser.contentHorizontalAlignment = .left
            btnSubuser.contentHorizontalAlignment = .left
            nextIcon.leftAnchor.constraint(equalTo: footerPanel.leftAnchor).isActive = true
            nextIcon.transform = CGAffineTransform.identity
            nextIcon.transform = nextIcon.transform.rotated(by: .pi)
            //
            mainUserIcon.leftAnchor.constraint(equalTo: mainUserPanel.leftAnchor, constant: 0).isActive = true
            btnMainUser.leftAnchor.constraint(equalTo: mainUserIcon.rightAnchor, constant: 8).isActive = true
            rdMainUser.rightAnchor.constraint(equalTo: mainUserPanel.rightAnchor, constant: -8).isActive = true
            btnMainUser.rightAnchor.constraint(equalTo: rdMainUser.leftAnchor, constant: -8).isActive = true
            //
            subuserIcon.leftAnchor.constraint(equalTo: subuserPanel.leftAnchor, constant: 0).isActive = true
            btnSubuser.leftAnchor.constraint(equalTo: subuserIcon.rightAnchor, constant: 8).isActive = true
            rdSubuser.rightAnchor.constraint(equalTo: subuserPanel.rightAnchor, constant: -8).isActive = true
            btnSubuser.rightAnchor.constraint(equalTo: rdSubuser.leftAnchor, constant: -8).isActive = true
        }
        
    }
    
    // MARK: - Actions
    
    @IBAction private func selectMainUserOnCLick(_ sender: UIButton) {
        rdMainUser.isSelected = true
        rdSubuser.isSelected = false
        usertype = .main
        UserDefaults.standard.set(usertype.rawValue, forKey: Misc.UserTypeKey)
        UserDefaults.standard.synchronize()
    }
    
    @IBAction private func selectSubuserOnCLick(_ sender: UIButton) {
        rdMainUser.isSelected = false
        rdSubuser.isSelected = true
        usertype = .sub
        UserDefaults.standard.set(usertype.rawValue, forKey: Misc.UserTypeKey)
        UserDefaults.standard.synchronize()
    }

    @IBAction private func continueOnClick(_ sender: UIButton) {
        guard let registerPage = storyboard?.instantiateViewController(withIdentifier: Storyboards.Register) as? Register else { return }
        registerPage.usertype = usertype        
        navigate(to: registerPage)
    }
}























