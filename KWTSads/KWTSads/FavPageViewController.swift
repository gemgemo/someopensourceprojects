//
//  FavPageViewController.swift
//  KWTSads
//
//  Created by Mostafa on 8/14/16.
//  Copyright © 2016 Mostafa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftSpinner



class FavPageViewController: UIViewController ,UICollectionViewDataSource ,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    @IBOutlet var favCollectionView: UICollectionView!
    let imagesUrl = "http://142.4.4.149/~kwtads/all_images/"
    var images_cache = [String:UIImage]()
    var advID = [String]()
    var getId = ""
    var advNames = [String]()
    var advDate = [String]()
    var advImg = [String]()
    var FavoID = [String]()
    let defaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        if Reachability.isConnectedToNetwork() == true {
            favCollectionView.delegate = self
            favCollectionView.dataSource = self
            SwiftSpinner.show("Loading...")
            self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "ac_back-arrow")
            self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "ac_back-arrow")
            self.navigationController?.navigationBar.tintColor = UIColor(red: 0.96, green: 0.69, blue:  0.25, alpha: 1 )
            
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
            
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage
            loadFavPage()

        }
        else
        {
            let barImage = UIImageView(image:  UIImage(named:"ac_logo"))
            barImage.frame = CGRect(x: self.view.frame.width/2-30, y: 10, width: 10, height: 35)
            self.navigationItem.titleView = barImage
            
            let err = "Internet connection FAILED"
            self.displayAlertMessage(err)
        }
    }
    
    private func loadFavPage()-> Void {
        advID.removeAll()
        advDate.removeAll()
        FavoID.removeAll()
        favCollectionView.reloadData()
        let myID = UserDefaults.standard.string(forKey: "MyDeviceID")! as String
        let urlString = "\(baseUrl)favorite_v.php?device_id=\(myID)&lang=0"
        let session = URLSession.shared
        let url = URL(string: urlString)!
        session.dataTask(with: url, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
                if let responseData = data {
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments)as! [String:AnyObject]
                        
                        let data = json["data"] as! NSArray
                        if data.count == 0
                        {
                            
                            let err = "No favorite added ,please add fav ads"
                            self.displayAlertMessage(err)
                        }
                        else
                        {
                            
                            for i in 0..<data.count
                            {
                                if let dict = data[i] as? NSDictionary {
                                    
                                    self.advID.append(dict["id_f"] as! String)
                                    self.advDate.append(dict["date_insert"] as! String)
                                    self.FavoID.append(dict["favo_id"] as! String)
                                }
                                
                                
                                
                            }
                        }
                        DispatchQueue.main.async
                            {
                                self.favCollectionView.reloadData()
                                SwiftSpinner.hide()
                                
                        }
                        
                        
                        
                    }
                    catch
                    {
                        print ("Could not Serialize")
                    }
                    
                }
                
        }).resume()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize =  self.favCollectionView.bounds.width
        
        return CGSize(width: screenSize - 20, height: 73)
    }
    
    
    func getDetails(_ advId : String,cell: FavPageCollectionViewCell,index: Int) {
        let urlString = "\(baseUrl)v_adv.php?adv_id=\(advID[index])&lang=0"
        Alamofire.request(urlString, method: .post).validate().responseJSON
            { response in switch response.result {
            case .success(let JSON):
                print("Success with JSON: \(JSON)")
                
                let response = JSON as! NSDictionary
                let array = response["data"] as! NSArray
                
                let object = array[0]
                if let dict = object as? NSDictionary {
                    
                    cell.favName.text = dict["adv_name"] as? String
                    self.load_image(self.imagesUrl+(dict["img_adv"] as? String)!, imageview: cell.favImg)
                    cell.favInsertDate.text = self.advDate[index]
                    
                    cell.deleteButton.tag = index
                    cell.deleteButton.addTarget(self, action: #selector(FavPageViewController.deleteFav(_:)), for: .touchUpInside)
                }
            case .failure(let error):
                print("Request failed with error: \(error)")
                }
        }
        
    }

    func deleteFav(_ sender:UIButton) {
        let urlString = "\(baseUrl)favorite_del.php?favo_id=\(FavoID[sender.tag])"
        print(urlString)
        Alamofire.request(urlString, method: .post)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let JSON):
                    print("Success with JSON: \(JSON)")
                    let response = JSON as! NSDictionary
                    let userMessage = response["data"] as? String
                    self.displayAlertMessage(userMessage!)
                    //self.FavoID.remove(at: sender.tag)
                    //self.favCollectionView.reloadData()
                    self.viewDidLoad()
                case .failure(let error):
                    print("Request failed with error: \(error)")
                }
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "favCell", for: indexPath) as! FavPageCollectionViewCell
        
        getDetails(advID[indexPath.row],cell: cell,index: (indexPath.row))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return FavoID.count
    }
    
    func load_image(_ link:String, imageview:UIImageView)
    {
        
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.timeoutInterval = 10
        
        
        let task = session.dataTask(with: url, completionHandler:
            { (data :Data?, response :URLResponse?, error:Error?) in
            
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                
                return
            }
            
            
            var image = UIImage(data: data!)
            
            if (image != nil)
            {
                
                
                func set_image()
                {
                    self.images_cache[link] = image
                    imageview.image = image
                }
                
                
                DispatchQueue.main.async(execute: set_image)
                
            }
            
        }) 
        
        task.resume()
        
    }
    
    
    
//    func refresh()
//    {
//        self.favCollectionView.reloadData()
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "com.show.from.fav" {
            let tabViewController = segue.destination as! deatialscreensViewController
            // Get the cell that generated this segue.
            if let selectedCell = sender as? FavPageCollectionViewCell{
                let indexPath = favCollectionView.indexPath(for: selectedCell)!
                let selectedAdvId = advID[indexPath.row]
                defaults.setValue(advID[indexPath.row], forKey: "departmentSid")
                tabViewController.departmentSids = selectedAdvId
            }
        }
        
        
    }
    
    @IBAction func departTapped(_ sender: AnyObject) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "departmentPage") as! DepartmentsViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func MenuTapped(_ sender: AnyObject) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuPage") as! MenuPageViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dailyNew(_ sender: AnyObject) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "DailyNews") as! DailyNewsViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    
    func displayAlertMessage(_ userMessage:String) {
        DispatchQueue.main.async
        {
        let myAlert = UIAlertController(title: "Alert", message:userMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        }
        
    }

    
  
}
