
import UIKit
import Localize_Swift
import CoreLocation

final class CallDetails: BaseController
{
    // MARK: - Constants
    
    // MARK: - Variables
    
    internal var trip: Trip?
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var titlePanal: UIView!
    @IBOutlet fileprivate weak var lblDriverName: UILabel!
    @IBOutlet fileprivate weak var lblFrom: UILabel!
    @IBOutlet fileprivate weak var lblTo: UILabel!
    @IBOutlet fileprivate weak var lblTaskType: UILabel!
    @IBOutlet fileprivate weak var lblDistance: UILabel!
    @IBOutlet fileprivate weak var lblPaymentWay: UILabel!
    @IBOutlet fileprivate weak var lblMissionCost: UILabel!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    internal override func updateNavgationBar() {
        super.updateNavgationBar()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ac_back"), style: .plain, target: self, action: #selector(goBack(_:)))
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //print(trip?.requestId)
        //updateUi()
    }
    
    
    
    // MARK: - Actions
    
    
    
}


// MARK: - Helper functions

extension CallDetails
{
    
    fileprivate func updateUi(from info: CallInfo)-> void {
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            titlePanal.semanticContentAttribute = .forceLeftToRight
            lblTitle.textAlignment = .right
            lblTo.textAlignment = .right
            lblFrom.textAlignment = .right
            lblDistance.textAlignment = .right
            lblTaskType.textAlignment = .right
            lblDriverName.textAlignment = .right
            lblPaymentWay.textAlignment = .right
            lblMissionCost.textAlignment = .right
        } else {
            titlePanal.semanticContentAttribute = .forceRightToLeft
            lblTitle.textAlignment = .left
            lblTo.textAlignment = .left
            lblFrom.textAlignment = .left
            lblDistance.textAlignment = .left
            lblTaskType.textAlignment = .left
            lblDriverName.textAlignment = .left
            lblPaymentWay.textAlignment = .left
            lblMissionCost.textAlignment = .left
        }
        lblTitle.text = "jobdetailslan".localized()
        lblDriverName.text = "drivernametitlelan".localized()+info.driverName!
        let numberFormatter = NumberFormatter()
        if let lat = numberFormatter.number(from: info.sourceLatitude!)?.doubleValue, let lan = numberFormatter.number(from: info.sourceLongitude!)?.doubleValue {
            address(from: CLLocation(latitude: lat, longitude: lan)) { [weak self] (address) -> void in
                self?.lblFrom.text = "\("labelfromlan".localized()): \(address)"
            }
        }
        if let lat = numberFormatter.number(from: info.destinationLatitude!)?.doubleValue, let lan = numberFormatter.number(from: info.destinationLongitude!)?.doubleValue {
            address(from: CLLocation(latitude: lat, longitude: lan)) { [weak self] (address) -> void in
                self?.lblTo.text = "\("labeltolan".localized()): \(address)"
            }
        }
        lblDistance.text = "\("distancelan".localized()) \(info.distance!) \("kmlan".localized())"
        lblTaskType.text = "\("missiontypelan".localized()) \((Localize.currentLanguage() == Constants.Language.Arabic) ? info.arabicCarName! : info.englishCarName!)"
        var pay = string.empty
        switch (info.payType!) {
        case "0":
            pay = "walletlan"
        case "1":
            pay = "cashlan"
        case "2":
            pay = "visalan"
        default:
            break
        }
        lblPaymentWay.text = "paymentwaytitlelan".localized()+pay.localized()
        lblMissionCost.text = "costofthemissionlan".localized()+info.salary!
        stopSpin()
    }
    
    
    fileprivate func loadData()-> void {
        guard let id = trip?.requestId else { return }
        CallInfo.getData(byId: id, page: self) { [weak self] (info) in
            self?.updateUi(from: info)
        }
    }
    
}
























