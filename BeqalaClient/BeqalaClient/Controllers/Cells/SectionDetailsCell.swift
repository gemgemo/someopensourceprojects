
import Foundation
import UIKit

internal final class SectionDetailsCell: BaseGridCell
{
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var product: Product? {
        didSet {
            updateUi()
        }
    }
    
    // MARK: - Outlets
    
    @IBOutlet private weak var mainPanal: View!
    @IBOutlet private weak var footerPanal: UIView!
    @IBOutlet private weak var cover: UIImageView!
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var lblPrice: UILabel!
    @IBOutlet internal weak var btnRight: UIButton!
    @IBOutlet internal weak var btnLeft: UIButton!
    @IBOutlet private weak var imageWidth: NSLayoutConstraint!
    
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        if let imageLink = product?.photo1 {
            cover.loadImage(from: imageLink)
        }
        lblTitle.text = product?.title ?? .empty
        lblPrice.text = "\(product?.price ?? .empty) \(Localization.SR.localized())"
        if (Localizer.instance.current == .arabic) {
            lblTitle.textAlignment = .right
            lblPrice.textAlignment = .right
            footerPanal.semanticContentAttribute = .forceLeftToRight
            mainPanal.semanticContentAttribute = .forceLeftToRight
        } else {
            lblTitle.textAlignment = .left
            lblPrice.textAlignment = .left
            footerPanal.semanticContentAttribute = .forceRightToLeft
            mainPanal.semanticContentAttribute = .forceRightToLeft
        }
    }
    
    internal override func updateConstraints() {
        super.updateConstraints()
        print("image height, \(cover.bounds.height)")
        imageWidth.constant = cover.bounds.height*0.60
    }
    
}





































