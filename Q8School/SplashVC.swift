
import UIKit

var isLogged = false

final class SplashVC: UIViewController
{
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        open { [weak self] in
            URLSession.shared.dataTask(with: URL(string: Settings.FoundAds)!) { [weak self] (result, response, error) in
                if (error != nil) {
                    print("check ads error \(String(describing: error))")
                    self?.navigateToMain()
                    return
                }
                guard let jsonData = result else { return }
                print("to show ads or not json string: \(String(data: jsonData, encoding: .utf8) ?? "")")
                do {
                    if let json = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? Dictionary<String, Any> {
                        let success = String(describing: json["Success"] ?? "")
                        print("show ads screen success: \(success)")
                        if (success == "1") {
                            DispatchQueue.main.async { [weak self] in
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "AdsPageViewController") as! AdsPageViewController
                                self?.navigationController?.pushViewController(vc, animated: false)
                            }
                        } else {
                            self?.navigateToMain()
                        }
                    }
                } catch {
                    print("catch error \(String(describing: error))")
                    self?.navigateToMain()
                }
                }.resume()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    private func open(_ callback: @escaping()->())-> Void {
        print("has logged in", UserDefaults.standard.bool(forKey: LoggedInKey))
        if (UserDefaults.standard.bool(forKey: LoggedInKey)) {
            callback()
        } else {
           navigateToSignIn()
        }
    }
    
    private func navigateToSignIn()-> Void {
        DispatchQueue.main.asyncAfter(deadline: .now()+2) { [weak self] in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            self?.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    private func navigateToMain()-> Void {
        DispatchQueue.main.async { [weak self] in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MainPageViewController") as! MainPageViewController
            self?.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
}




















