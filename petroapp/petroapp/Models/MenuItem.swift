
import Foundation
import Gemo

struct MenuItem {
    
    internal var id: int, title: string, icon: UIImage
    
    internal static func items() -> [MenuItem] {
        var items = [MenuItem]()
        items.append(MenuItem(id: 0, title: Localization.Main.localized(), icon: #imageLiteral(resourceName: "ic_home")))
        items.append(MenuItem(id: 1, title: Localization.MyAccount.localized(), icon: #imageLiteral(resourceName: "ic_menu_user")))
        items.append(MenuItem(id: 2, title: Localization.MyPoints.localized(), icon: #imageLiteral(resourceName: "ic_points")))
        items.append(MenuItem(id: 3, title: Localization.ContactUs.localized(), icon: #imageLiteral(resourceName: "ic_call")))
        items.append(MenuItem(id: 4, title: Localization.Complaints.localized(), icon: #imageLiteral(resourceName: "ic_mail")))
        items.append(MenuItem(id: 5, title: Localization.AboutApp.localized(), icon: #imageLiteral(resourceName: "ic_info")))
        items.append(MenuItem(id: 6, title: Localization.ShareApp.localized(), icon: #imageLiteral(resourceName: "ic_sharing")))
        items.append(MenuItem(id: 7, title: Localization.Logout.localized(), icon: #imageLiteral(resourceName: "ic_log_out")))
        return items
    }
    
}


class PopupMainMenu: Item {
    
    var title: String
    
    init(title: string) {
        self.title = title
    }
    
    internal class func items()-> [PopupMainMenu] {
        var items = [PopupMainMenu]()
        items.append(PopupMainMenu(title: Localization.Reports.localized()))
        items.append(PopupMainMenu(title: Localization.AboutApp.localized()))
        items.append(PopupMainMenu(title: Localization.ContactUs.localized()))
        items.append(PopupMainMenu(title: Localization.Complaints.localized()))
        items.append(PopupMainMenu(title: Localization.Logout.localized()))
        return items
    }
    
}
































