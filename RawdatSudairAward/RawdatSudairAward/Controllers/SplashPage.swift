
import UIKit


final class SplashPage: BaseController
{
    
    // MARK: - Constants
    
    // MARK: - Variables
    
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var logoBottom: NSLayoutConstraint!
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isStatusBarHidden = true
        logoBottom.constant = 1000
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        logoBottom.constant = -50.0
        UIView.animate(withDuration: 1.0, delay: 1.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseOut, animations: { [weak self] in
            self?.view.layoutIfNeeded()
        }) { [weak self] (isFinished) in
            if let reveal = self?.storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Reveal) as? SWRevealViewController {
                (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController = reveal
            }
        }
    }
    
    // MARK: - Actions
    
}
