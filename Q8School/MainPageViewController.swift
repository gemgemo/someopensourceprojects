//
//  MainPageViewController.swift
//  Q8School
//
//  Created by Mostafa on 12/11/16.
//  Copyright © 2016 Atiaf. All rights reserved.
//

import UIKit

class MainPageViewController: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate{
    
    @IBOutlet weak var offersLbl: UILabel!
    @IBOutlet weak var myAdsLbl: UILabel!
    @IBOutlet weak var addAdsLbl: UILabel!
    @IBOutlet weak var callUsLbl: UILabel!
    @IBOutlet weak var openMenu: UIBarButtonItem!
    var phases = Array<Phase>()
    var dataFound: Bool!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet weak var loadingLbl: UILabel!

    @IBOutlet weak var myAdsImg: UIImageView!
    @IBOutlet weak var mainCollection: UICollectionView!
    var type:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if  UserDefaults.standard.string(forKey: "DeviceToken") != nil{
            let deviceToken = UserDefaults.standard.string(forKey: "DeviceToken") ?? ""
            sendDeviceToken(deviceToken)
        } else {
            sendDeviceToken("0")
        }
        self.title = "Phases study"
        shadowView.isHidden = true
        loadingView.isHidden = true
        mainCollection.delegate = self
        mainCollection.dataSource = self
        loadingView.layer.cornerRadius = 4
        loadingView.layer.masksToBounds = true
        loadMainPhase(Settings.lang)
        let layout1:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout1.sectionInset = UIEdgeInsets(top:10,left:10,bottom:10,right:10)
        
        self.mainCollection.collectionViewLayout = layout1
        if Settings.lang == "ar"{
            mainCollection.semanticContentAttribute = .forceRightToLeft
            loadingLbl.text = "جاري التحميل..."
            self.title = "مراحل الدارسة"
            callUsLbl.text = "اتصل بنا"
            addAdsLbl.text = "اضف اعلانك"
            myAdsLbl.text = "اعلاناتي"
            offersLbl.text = "العروض"
            

        }
        if revealViewController() != nil {
            
            revealViewController().rightViewRevealWidth = 235
            openMenu.target = revealViewController()
            openMenu.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        type = UserDefaults.standard.string(forKey: "userType")
        if type == "1"
        {
            if Settings.lang == "ar"
            {
                myAdsLbl.text = "المفضلة"
                myAdsImg.image = UIImage(named: "fav")

            }
            else
            {
                myAdsLbl.text = "Favorite"
                myAdsImg.image = UIImage(named: "fav")


            }
        }

    }
    
    func sendDeviceToken(_ deviceToken: String) {
        print("Device \(deviceToken)")
        let userId = UserDefaults.standard.string(forKey: "users_ID")
        //print("user\(String(describing: userId))")
        let deviceType = "1"
        let request = NSMutableURLRequest(url: URL(string: Settings.Notifcation)!)
        request.httpMethod = "POST"
        let postString = "userId=\(userId!)&deviceToken=\(deviceToken)&deviceType=\(deviceType)"
        //print("post\(postString)")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        { data, response, error in
            guard error == nil && data != nil else {
                print(" device token error=\(String(describing: error))")
                return
            }
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("device token reponse = \(String(describing: responseString))")
            if let responseData = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                    print("send device token json: \(json)")
                }
                catch
                {
                    print ("Could not Serialize")
                }
                
            }
            
        }
        task.resume()
    }

    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.7608320713, green: 0.2725405097, blue: 0.00439185882, alpha: 1)
        navigationItem.leftBarButtonItem = UIBarButtonItem()
        navigationItem.backBarButtonItem = UIBarButtonItem()
        navigationItem.hidesBackButton = true
    }
    
    private func loadMainPhase(_ lang: String) {
        /*shadowView.isHidden = false
        loadingView.isHidden = false
        loadingActivity.startAnimating()*/
        let request = NSMutableURLRequest(url: URL(string:Settings.MainPhase)!)
        request.httpMethod = "POST"
        let postString = "lang=\(lang)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                         
                print("error=\(String(describing: error))")
                return
            }
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
            if let responseData = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                    print("ddd\(json)")
                    self.dataFound = json["Success"] as? Bool
                    if (self.dataFound!) {
                        let data = json["Result"] as! [NSDictionary]
                        for i in data {
                            var name1 = ""
                            if Settings.lang == "ar" {
                                name1 = "name_ar"
                            } else {
                                name1 = "name"
                            }
                            let id = i["id"] as! String
                            let name = i[name1] as! String
                            let phase = Phase(id: id, name: name)
                            self.phases.append(phase)
                        }
                    }
                    DispatchQueue.main.async {
//                            self.shadowView.isHidden = true
//                            self.loadingView.isHidden = true
//                            self.loadingActivity.stopAnimating()
                            self.mainCollection.reloadData()
                    }
                } catch {
                    print ("Could not Serialize")
                }
            }
        }
        task.resume()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let size = self.mainCollection.frame.width / 2
        return CGSize(width: size  - 15, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mainCell", for: indexPath)
            as! MainPageCollectionViewCell
        cell.mainNameLbl.text = self.phases[indexPath.row].name
        cell.id = self.phases[indexPath.row].id
        cell.layer.cornerRadius = 10
        cell.layer.masksToBounds = true
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.phases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = mainCollection.cellForItem(at: indexPath) as! MainPageCollectionViewCell
       
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SubMainPageViewController") as! SubMainPageViewController
        vc.selectedId = cell.id
        vc.selectedName = cell.mainNameLbl.text!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func displayAlerts(_ Massage: String) {
        var t = ""
        var b = ""
        if Settings.lang == "ar" {
            t = "تنبيه"
            b = "إغلاق"
        } else {
            t = "Alert"
            b = "Close"
        }
        
        let myAlert = UIAlertController(title: t, message:Massage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title: b, style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
        
    }

    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AdsPageViewController") as! AdsPageViewController
        self.navigationController?.pushViewController(vc, animated: false)


    }

    @IBAction func callUsBtn(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CallUsVc") as! CallUsVc
        self.navigationController?.pushViewController(vc, animated: false)

    }
    
    @IBAction func addAdsBtn(_ sender: UIButton) {
        if type == "1"
        {
            if Settings.lang == "ar"
            {
                let msg = "انت طالب لا يمكنك اضافة اعلان"
                self.displayAlerts(msg)
                
            }
            else
            {
                let msg = "you'r student can't add Ads"
                self.displayAlerts(msg)
                
            }

          
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AddAdsVc") as! AddAdsVc
            self.navigationController?.pushViewController(vc, animated: false)

        }
    }
    
    @IBAction func myAdsBtn(_ sender: UIButton) {
        if type == "1"
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyFavoriteVC") as! MyFavoriteVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyAdsVC") as! MyAdsVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }

    }
    
    @IBAction func offerBtn(_ sender: UIButton) {
        guard revealViewController().frontViewPosition == .left else {
            revealViewController().rightRevealToggle(animated: true)
            return
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AdsPageViewController") as! AdsPageViewController
        self.navigationController?.pushViewController(vc, animated: false)

        
    }
    
    
    
}
