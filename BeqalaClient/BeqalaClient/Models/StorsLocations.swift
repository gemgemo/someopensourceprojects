
import Foundation

internal final class StorsLocations: Mapper<Any>
{
    internal var result, arabicMessage, englishMessage, userId, userName, shopName, email, phone, info, latitude, longitude, photo: string?
    
    internal override var description: String {
        return "\n userId: \(string(describing: userId)), shopName: \(string(describing: shopName)), location: (latitude: \(string(describing: latitude)), longitude: \(string(describing: longitude))) \n"
    }
    
    internal override func mapping(mapper: Mapper<Any>) {
        result = mapper["result"]?.text
        arabicMessage = mapper["data"]?.text
        englishMessage = mapper["data_en"]?.text
        userId = mapper["user_id"]?.text
        userName = mapper["username"]?.text
        shopName = mapper["matjer_name"]?.text
        email = mapper["email"]?.text
        phone = mapper["phone"]?.text
        info = mapper["description"]?.text
        latitude = mapper["lati"]?.text
        longitude = mapper["longi"]?.text
        photo = mapper["photo_users"]?.text
    }
    
    internal class func getData(_ page: BaseController, _ completion: @escaping([StorsLocations])->())-> void {
        Gemo.gem.startSpinning(in: page.view)
        HttpClient.request(link: Constants.Urls.storsLocations(), method: .post, parameters: [Constants.Misc.MainKey: Constants.Misc.MainValue]) { (data, response, error) -> void in
            if (error != nil) {
                print("fetch stors locations error: \(string(describing: error))")
                Gemo.gem.stopSpinning()
                Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription, in: page.view)
                return
            }
            guard let json = data as? [Dictionary<string, any>] else {
                print("fetching shops locations data is null")
                Gemo.gem.stopSpinning()
                return
            }
            completion(json.map { StorsLocations(JSON: $0) })
        }
    }
    
    
}


































