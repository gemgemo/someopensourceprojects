
import Foundation
import Gemo


final class NotificationCell: BaseTableCell {
    
    
    // MARK: - Constatnts
    
    
    // MARK: - Variables
    
    internal var item: OrderNotification? { didSet { updateUi() }}
    
    fileprivate var dataSource = Array<NotificationProduct>() { didSet {
        tblProducts.reloadData()
        tableViewHeight.constant = tblProducts.contentSize.height
        }}
    
    // MARK: - Outlets
    
    @IBOutlet private weak var mainPanel: UIView! { didSet {
        mainPanel.setCorner(radius: 10)
        mainPanel.setBorder(with: 1, and: Color(white: 0.88, alpha: 0.9))
        }}
    @IBOutlet private weak var headerPanel: UIView!
    @IBOutlet private weak var lblNotificationState: UILabel!
    @IBOutlet private weak var lblTimestamp: UILabel!
    @IBOutlet private weak var userIcon: UIImageView!
    @IBOutlet private weak var lblUsername: UILabel!
    @IBOutlet private weak var tblProducts: UITableView! { didSet {
        tblProducts.delegate = self
        tblProducts.dataSource = self
        }}
    @IBOutlet private weak var bottomPanel: UIView! { didSet {
        bottomPanel.setBorder(with: 1, and: Color(white: 0.88, alpha: 0.9))
        }}
    @IBOutlet private weak var bottonLabelsPanel: UIView!
    @IBOutlet private weak var lblDelivreyWayTitle: UILabel!
    @IBOutlet private weak var lblDeliveryWay: UILabel!
    @IBOutlet private weak var bottomLocationPanel: UIView!
    @IBOutlet private weak var locationMarker: UIImageView!
    @IBOutlet private weak var lblLocation: UILabel!
    @IBOutlet private weak var lblBuildingTitle: UILabel!
    @IBOutlet private weak var lblBuildingNumber: UILabel!
    @IBOutlet private weak var lblFloorTitle: UILabel!
    @IBOutlet private weak var lblFloorNumber: UILabel!
    @IBOutlet private weak var actionPanel: UIView! { didSet {
        actionPanel.setShadow(with: Size(right: 0, top: -1), radius: 0.5, opacity: 0.4, color: .gray)
        }}
    @IBOutlet internal weak var btnAccept: UIButton!
    @IBOutlet internal weak var btnReject: UIButton!
    @IBOutlet private weak var tableViewHeight: NSLayoutConstraint! { didSet { tableViewHeight.constant = 0.0 } }
    @IBOutlet private weak var buttonsStack: UIStackView!
    @IBOutlet private weak var locationPanelHeight: NSLayoutConstraint!
    @IBOutlet private weak var lblTotal: UILabel!
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        tblProducts.alwaysBounceVertical = false
        tblProducts.alwaysBounceHorizontal = false
        tblProducts.register(Nibs.NotificationProduct, forCellReuseIdentifier: ReuseIdentifiers.NotificationProduct)
        lblDelivreyWayTitle.text = Localization.DeliveryWay.localized()
        btnAccept.setTitle(Localization.Accept.localized(), for: .normal)
        btnReject.setTitle(Localization.Reject.localized(), for: .normal)
        if let products = item?.products {
            dataSource = products
        }
        lblNotificationState.text = Localization.PurchaseOrder.localized()
        lblTimestamp.text = "\(item?.time ?? .empty)  \(item?.date ?? .empty)"
        if let gender = item?.gender {
            userIcon.image = (gender == "1") ? #imageLiteral(resourceName: "man") : #imageLiteral(resourceName: "women")
        }
        lblUsername.text = item?.username ?? .empty
        lblDeliveryWay.text = item?.type ?? .empty
        lblLocation.text = item?.address ?? .empty
        if let building = item?.building, building.isEmpty {
            // recived from recipient
            lblBuildingTitle.text = "\(Localization.RecipientName.localized()): "
            lblFloorTitle.text = "\(Localization.PhoneNumber.localized()): "
            lblBuildingNumber.text = item?.name ?? .empty
            lblFloorNumber.text = item?.phone ?? .empty
            bottomLocationPanel.isHidden = true
        } else {
            // recived from building
            lblBuildingTitle.text = "\(Localization.Building.localized()): "
            lblFloorTitle.text = "\(Localization.Floor.localized()): "
            lblBuildingNumber.text = item?.building ?? .empty
            lblFloorNumber.text = item?.floor ?? .empty
            bottomLocationPanel.isHidden = false
        }
        setupConstraints()
        Threads.background(QOS: .userInitiated) { [weak self] in
            var total = 0
            self?.item?.products.forEach { total += $0.price ?? 0 }
            Threads.main { [weak self] in
                self?.lblTotal.text = "\(Localization.Total.localized()) \(total.toDouble) \(Localization.SR.localized())"
            }
        }
    }
    
    internal override func updateConstraints() {
        super.updateConstraints()
        if let building = item?.building, building.isEmpty {
            locationPanelHeight.constant = 0.0
        } else {
            locationPanelHeight.constant = 70.0
        }
        
    }
    
    private func setupConstraints()-> void {
        if (Localizer.instance.current == .arabic) {
            // header
            lblNotificationState.rightAnchor.constraint(equalTo: headerPanel.rightAnchor, constant: -8).isActive = true
            lblTimestamp.leftAnchor.constraint(equalTo: headerPanel.leftAnchor, constant: 8).isActive = true
            userIcon.rightAnchor.constraint(equalTo: headerPanel.rightAnchor, constant: -8).isActive = true
            lblUsername.rightAnchor.constraint(equalTo: userIcon.leftAnchor, constant: -8).isActive = true
            //bottom header labels
            lblDeliveryWay.textAlignment = .right
            lblDelivreyWayTitle.textAlignment = .right
            lblDelivreyWayTitle.rightAnchor.constraint(equalTo: bottonLabelsPanel.rightAnchor, constant: -8).isActive = true
            lblDeliveryWay.rightAnchor.constraint(equalTo: lblDelivreyWayTitle.leftAnchor, constant: -8).isActive = true
            lblDeliveryWay.leftAnchor.constraint(equalTo: bottonLabelsPanel.leftAnchor, constant: -8).isActive = true
            //bottom location panel
            lblLocation.textAlignment = .right
            locationMarker.rightAnchor.constraint(equalTo: bottomLocationPanel.rightAnchor, constant: -8).isActive = true
            lblLocation.rightAnchor.constraint(equalTo: locationMarker.leftAnchor, constant: 0).isActive = true
            lblLocation.leftAnchor.constraint(equalTo: bottomLocationPanel.leftAnchor, constant: 8).isActive = true
            // bottom labels
            lblBuildingTitle.rightAnchor.constraint(equalTo: bottomPanel.rightAnchor, constant: -8).isActive = true
            lblBuildingNumber.rightAnchor.constraint(equalTo: lblBuildingTitle.leftAnchor, constant: -8).isActive = true
            lblFloorTitle.rightAnchor.constraint(equalTo: bottomPanel.rightAnchor, constant: -8).isActive = true
            lblFloorNumber.rightAnchor.constraint(equalTo: lblFloorTitle.leftAnchor, constant: -8).isActive = true
            buttonsStack.semanticContentAttribute = .forceLeftToRight
        } else {
            // header
            lblNotificationState.leftAnchor.constraint(equalTo: headerPanel.leftAnchor, constant: 8).isActive = true
            lblTimestamp.rightAnchor.constraint(equalTo: headerPanel.rightAnchor, constant: -8).isActive = true
            userIcon.leftAnchor.constraint(equalTo: headerPanel.leftAnchor, constant: 8).isActive = true
            lblUsername.leftAnchor.constraint(equalTo: userIcon.rightAnchor, constant: 8).isActive = true
            //bottom header labels
            lblDeliveryWay.textAlignment = .left
            lblDelivreyWayTitle.textAlignment = .left
            lblDelivreyWayTitle.leftAnchor.constraint(equalTo: bottonLabelsPanel.leftAnchor, constant: 8).isActive = true
            lblDeliveryWay.leftAnchor.constraint(equalTo: lblDelivreyWayTitle.rightAnchor, constant: 8).isActive = true
            lblDeliveryWay.rightAnchor.constraint(equalTo: bottonLabelsPanel.rightAnchor, constant: -8).isActive = true
            //bottom location panel
            lblLocation.textAlignment = .left
            locationMarker.leftAnchor.constraint(equalTo: bottomLocationPanel.leftAnchor, constant: 8).isActive = true
            lblLocation.leftAnchor.constraint(equalTo: locationMarker.rightAnchor, constant: 0).isActive = true
            lblLocation.rightAnchor.constraint(equalTo: bottomLocationPanel.rightAnchor, constant: -8).isActive = true
            // bottom labels
            lblBuildingTitle.leftAnchor.constraint(equalTo: bottomPanel.leftAnchor, constant: 8).isActive = true
            lblBuildingNumber.leftAnchor.constraint(equalTo: lblBuildingTitle.rightAnchor, constant: 8).isActive = true
            lblFloorTitle.leftAnchor.constraint(equalTo: bottomPanel.leftAnchor, constant: 8).isActive = true
            lblFloorNumber.leftAnchor.constraint(equalTo: lblFloorTitle.rightAnchor, constant: 8).isActive = true
            buttonsStack.semanticContentAttribute = .forceRightToLeft
        }
    }
    
    
    // MARK: - Actions

    
}


// MARK:- Table view delegate & data source functions

extension NotificationCell: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifiers.NotificationProduct) as? NotificationProductCell else { return UITableViewCell() }
        cell.product = dataSource[indexPath.row]
        return cell
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    /*internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected row: \(indexPath)")
    }*/
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
}

























