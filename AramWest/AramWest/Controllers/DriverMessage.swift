
import UIKit
import Alamofire
import Localize_Swift

final class DriverMessage: BaseController
{
    // MARK: - Constants
    
    // MARK: - Variables
    internal var id, whoNum, phone, priceType, cId, name, carTypeA, carTypeE, carId, carColor, price, avatar: string?
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var icon: UIImageView! {
        didSet {
            icon.rounded()
        }
    }
    @IBOutlet fileprivate weak var lblDriverName: UILabel!
    @IBOutlet fileprivate weak var lblCarType: UILabel!
    @IBOutlet fileprivate weak var lblCarId: UILabel!
    @IBOutlet fileprivate weak var lblCarColor: UILabel!
    @IBOutlet fileprivate weak var lblPriceTitle: UILabel!
    @IBOutlet fileprivate weak var lblPrice: UILabel!
    @IBOutlet fileprivate weak var btnAccept: Button!
    @IBOutlet fileprivate weak var btnReject: Button!
    @IBOutlet fileprivate weak var lblSubtitle: UILabel!
    @IBOutlet fileprivate weak var priceBox: Shape!
    @IBOutlet fileprivate weak var carInfoPanal: Shape!
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.superview?.layer.cornerRadius = 0.0
        updateUi()
    }
    
    internal override func updateNavgationBar() {
        navigationController?.navigationBar.isHidden = true
    }
    
    
    // MARK: - Actions
    
    @IBAction private func acceptOnClick(_ sender: UIButton) {
        (isCompany) ? acceptCompany() : acceptDriver()
    }
    
    @IBAction private func declineOnClick(_ sender: UIButton) {
        (isCompany) ? rejectCompany() : rejectDriver()
    }
    
}


// MARK: - Helper Functions

extension DriverMessage
{
    
    fileprivate func rejectCompany()-> void {
        startSpin()
        guard let clientId = UserDefaults.standard.value(forKey: Constants.Misc.UserId) else {
            stopSpin()
            showAlert(with: .empty, and: "reqiurefieldlan".localized())
            return
        }
        let parameters: [string: any] = [
            "ClientID": clientId,
            "ReqID": reqId,
            "CompanyID": companyId,
            "SalaryType": companySalaryType
        ]
        print(parameters)
        Alamofire.request(Constants.BackendURLs.DeclineCompany, method: .post, parameters: parameters)
            .responseJSON { [weak self] (response) in
                let result = response.result
                print("reject company result \(result)")
                if (result.error != nil) {
                    print("decline company error is \(result.error)")
                    self?.stopSpin()
                    self?.showAlert(with: .empty, and: "erroralertlan".localized())
                    return
                }
                if let value = result.value {
                    print(value)
                    self?.stopSpin()
                    self?.dismiss(animated: true, completion: nil)
                } else {
                    self?.stopSpin()
                    self?.showAlert(with: .empty, and: "erroralertlan".localized())
                    print("decline company nil data")
                }
        }
    }
    
    fileprivate func rejectDriver()-> void {
        startSpin()
        guard let callID = cId, let clientId = UserDefaults.standard.value(forKey: Constants.Misc.UserId), let dID = id, let salaryT = priceType else {
            stopSpin()
            showAlert(with: .empty, and: "reqiurefieldlan".localized())
            return
        }
        let params: [string: any] = [
            "CallID": callID,
            "ClientID": clientId,
            "DriverID": dID,
            "SalaryType": salaryT
        ]
        
        Alamofire.request(Constants.BackendURLs.RejectDriver, method: .post, parameters: params)
            .responseJSON { [weak self] (response) in
                let result = response.result
                if (result.error != nil) {
                    print("decline driver error is \(result.error)")
                    self?.stopSpin()
                    self?.showAlert(with: .empty, and: "erroralertlan".localized())
                    return
                }
                if let value = result.value {
                    print(value)
                    self?.stopSpin()
                    self?.dismiss(animated: true, completion: nil)
                } else {
                    self?.stopSpin()
                    self?.showAlert(with: .empty, and: "erroralertlan".localized())
                    print("decline nil data")
                }
        }

    }
    
    fileprivate func acceptDriver()-> void {
        startSpin()
        guard let callID = cId, let clientID = UserDefaults.standard.value(forKey: Constants.Misc.UserId), let dID = id else {
            stopSpin()
            showAlert(with: .empty, and: "reqiurefieldlan".localized())
            return
        }
        let params: [string: any] = [
            "CallID": callID,
            "ClientID": clientID,
            "DriverID": dID
        ]

        Alamofire.request(Constants.BackendURLs.AcceptDriver, method: .post, parameters: params)
            .responseJSON { [weak self] (response) in
                let result = response.result
                if (result.error != nil) {
                    self?.stopSpin()
                    print("accept driver error \(result.error)")
                    self?.showAlert(with: .empty, and: "erroralertlan".localized())
                    return
                }
                
                if let value = result.value {
                    print(value)
                    self?.stopSpin()
                    self?.dismiss(animated: true, completion: nil)
                } else {
                    self?.stopSpin()
                    print("nix accept driver value")
                    self?.showAlert(with: .empty, and: "erroralertlan".localized())
                }
        }

    }
    
    fileprivate func acceptCompany()-> void {
        startSpin()
        guard !reqId.isEmpty, let clientID = UserDefaults.standard.value(forKey: Constants.Misc.UserId), !companyId.isEmpty else {
            stopSpin()
            showAlert(with: .empty, and: "reqiurefieldlan".localized())
            return
        }
        let parameters: Dictionary<string, any> = [
            "ReqID": reqId,
            "ClientID": clientID,
            "CompanyID": companyId
        ]
        Alamofire.request(Constants.BackendURLs.AcceptCompany, method: .post, parameters: parameters)
            .responseJSON { [weak self] (response) in
                let result = response.result
                if (result.error != nil) {
                    self?.stopSpin()
                    print("accept company error \(result.error)")
                    self?.showAlert(with: .empty, and: "erroralertlan".localized())
                    return
                }
                
                if let value = result.value {
                    print(value)
                    self?.stopSpin()
                    self?.dismiss(animated: true, completion: nil)
                } else {
                    self?.stopSpin()
                    print("nix accept comapny value")
                    self?.showAlert(with: .empty, and: "erroralertlan".localized())
                }
        }
    }
    
    fileprivate func updateUi()-> void {
        if (priceType ?? .empty == "0") {
            priceBox.isHidden = true
        }
        carInfoPanal.isHidden = isCompany
        lblTitle.text = "drivermessagetitlelan".localized()
        lblSubtitle.text = "drivermessagesubtitlelan".localized()
        lblPriceTitle.text = "priceformissionlan".localized()
        btnAccept.setTitle("messageacceptbuttnlan".localized(), for: .normal)
        btnReject.setTitle("messagedeclinebuttonlan".localized(), for: .normal)
        if (isCompany) {
            icon.loadImage(from: companyImage, onComplete: nil)
            lblDriverName.text = companyName
        } else {
            lblDriverName.text = name ?? .empty
            lblCarType.text = ((Localize.currentLanguage() == Constants.Language.Arabic) ? carTypeA : carTypeE) ?? .empty
            lblCarId.text = carId ?? .empty
            lblCarColor.text = carColor ?? .empty
            if let iconLink = avatar {
                icon.loadImage(from: iconLink, onComplete: nil)
            }
        }
        lblPrice.text = price ?? .empty
    }

}
