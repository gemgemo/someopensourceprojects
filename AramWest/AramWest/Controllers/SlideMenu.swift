
import UIKit
import Localize_Swift

final class SlideMenu: BaseController
{
    
    // MARK: - Constants
    
    // MARK: - Variables
    
    fileprivate var dataSource = Array<Menu>()
    
    // MARK: - Outlets
    
    @IBOutlet private weak var headerHeight: NSLayoutConstraint!
    @IBOutlet fileprivate weak var tblItems: UITableView! {
        didSet {
            tblItems.delegate = self
            tblItems.dataSource = self
        }
    }
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
    }
    
    internal override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         NotificationCenter.default.addObserver(self, selector: #selector(loadData), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    internal override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Actions

    
}


// MARK: - Helper functions 

extension SlideMenu
{
    
    fileprivate func configureTableView()-> void {
        tblItems.register(Constants.Nib.Menu, forCellReuseIdentifier: Constants.ReuseIdentifier.Menu)
    }
    
    @objc fileprivate func loadData()-> void {
        dataSource = Menu.getItems()
        tblItems.reloadData()
    }
    
    fileprivate func gotoAboutData(isAbout: bool)-> void {
        if let aboutAppScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.AboutApp) as?  AboutApp {
            aboutAppScreen.isAbout = isAbout
            present(UINavigationController(rootViewController: aboutAppScreen), animated: true, completion: nil)
        }
    }
    
}




// MARK: - Table view delegate & data source functions

extension SlideMenu: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIdentifier.Menu) as? MenuCell {
            cell.item = dataSource[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        revealViewController().revealToggle(animated: true)
        switch (indexPath.row) {
        case 0: // goto profile screen
            if let profileScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Profile) as? Profile {
                revealViewController().pushFrontViewController(UINavigationController(rootViewController: profileScreen), animated: true)
            }
            
        case 1: // my orders
            if let ordersScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Trips) as? Trips {
                revealViewController().pushFrontViewController(UINavigationController(rootViewController: ordersScreen), animated: true)
            }
            
        case 2: // new order
            if let mainScreen = storyboard?.instantiateViewController(withIdentifier: Constants.Storyboard.Main) as? Main {
                revealViewController().pushFrontViewController(UINavigationController(rootViewController: mainScreen), animated: true)
            }
            
//        case 3: // notifications
//            break
            
        case 3: // canage language
            changeLanguage()
            
        case 4: // open terms view
            gotoAboutData(isAbout: false)
            
        case 5: // open about app view
            gotoAboutData(isAbout: true)
            
        default:
            break
        }
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}



























