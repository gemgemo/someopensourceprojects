
import Foundation
import Gemo


final class ComplaintCell: BaseTableCell
{
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var complaint: Complaint? { didSet { updateUi() } }
    
    
    // MARK: - Outlets
    
    @IBOutlet private weak var mainPanel: View!
    @IBOutlet private weak var userIcon: UIImageView!
    @IBOutlet private weak var lblUsername: UILabel!
    @IBOutlet private weak var lblEmail: UILabel!
    @IBOutlet private weak var lblComplaintTitle: UILabel!
    @IBOutlet private weak var lblContent: UILabel!
    
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        if let gender = complaint?.gender, gender == "1" {
            userIcon.image = #imageLiteral(resourceName: "man")
        } else {
            userIcon.image = #imageLiteral(resourceName: "women")
        }
        lblUsername.text = complaint?.name ?? .empty
        lblEmail.text = "\(Localization.Email.localized()): \(complaint?.email ?? .empty)"
        lblComplaintTitle.text = complaint?.title ?? .empty
        lblContent.text = complaint?.content ?? .empty
        // constraints 
        if (Localizer.instance.current == .arabic) {
            userIcon.rightAnchor.constraint(equalTo: mainPanel.rightAnchor, constant: -8).isActive = true
            lblUsername.rightAnchor.constraint(equalTo: userIcon.leftAnchor, constant: -8).isActive = true
            lblEmail.textAlignment = .right
            lblComplaintTitle.textAlignment = .right
            lblContent.textAlignment = .right
            
        } else {
            userIcon.leftAnchor.constraint(equalTo: mainPanel.leftAnchor, constant: 8).isActive = true
            lblUsername.leftAnchor.constraint(equalTo: userIcon.rightAnchor, constant: 8).isActive = true
            lblEmail.textAlignment = .left
            lblComplaintTitle.textAlignment = .left
            lblContent.textAlignment = .left
            
        }
    }
    
    
    // MARK: - Actions

    
    
}










































