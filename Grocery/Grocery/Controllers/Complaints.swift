
import UIKit
import Gemo

final class Complaints: BaseController
{
    
    // MARK: - Constatnts
    
    
    // MARK: - Variables
    
    fileprivate var dataSource = Array<Complaint>() { didSet {
        Threads.main { [weak self] in
            self?.tblComplaints.reloadData()
        }}}
    
    // MARK: - Outlets
    
    @IBOutlet private weak var tblComplaints: UITableView! { didSet {
        tblComplaints.delegate = self
        tblComplaints.dataSource = self
        }}
    
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        tblComplaints.alwaysBounceVertical = true
    }
    
    internal override func updateUi() {
        super.updateUi()
        tabBar.isHidden = true
        navigationItem.title = Localization.Complaints.localized()
        loadData()
    }
    
    
    
    // MARK: - Actions
    
    

}



// MARK: - Helper functions

extension Complaints
{
    fileprivate func loadData()-> void {
        if (Gemo.gem.isConnected) {
            Complaint.fetchComplaints { [weak self] (complaints) in
                self?.dataSource = complaints
            }
        } else {
            dialogUserWithNoInternetConnectionMessage()
        }
    }
}


// MARK: - Table views delegate & data source functions

extension Complaints: UITableViewDelegate, UITableViewDataSource
{
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifiers.Complaint, for: indexPath) as? ComplaintCell else { return UITableViewCell() }
        cell.complaint = dataSource[indexPath.row]
        return cell
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected cell: \(indexPath)")
    }
    
    internal func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    internal func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300.0
    }
    
}
















