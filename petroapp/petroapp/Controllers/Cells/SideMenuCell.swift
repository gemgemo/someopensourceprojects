
import Foundation
import Gemo


class SideMenuCell: BaseTableCell {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    internal var item: MenuItem? { didSet { updateUi() } }
    
    // MARK: - Outlets
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    
    // MARK: - Overridden functions
    
    internal override func updateUi() {
        lblTitle.text = item?.title ?? .empty
        icon.image = item?.icon
        if (Localizer.instance.current == .arabic) {
            icon.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -8).isActive = true
            lblTitle.rightAnchor.constraint(equalTo: icon.leftAnchor, constant: -30).isActive = true
            lblTitle.textAlignment = .right
        } else {
            icon.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 8).isActive = true
            lblTitle.leftAnchor.constraint(equalTo: icon.rightAnchor, constant: 30).isActive = true
            lblTitle.textAlignment = .left
        }
        
    }
    
    
    // MARK: - Actions

}





















