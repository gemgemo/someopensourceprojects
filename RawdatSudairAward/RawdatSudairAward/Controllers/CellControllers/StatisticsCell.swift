
import UIKit
import Localize_Swift

final class StatisticsCell: UITableViewCell, CellProvider
{

    // MARK: - Constants
    
    //MARK: - Variables
    
    internal var statistics: StatisticsNumbers? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Outlets
    @IBOutlet private weak var icon: UIImageView!
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var lblSubTitle: UILabel!
    @IBOutlet private weak var mainPanal: UIView!
    @IBOutlet private weak var mainStack: UIStackView!
    
    
    // MARK: - Overridden functions
    
    internal func updateUI() {
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            lblTitle.textAlignment = .right
            lblSubTitle.textAlignment = .right
            mainPanal.semanticContentAttribute = .forceLeftToRight
            mainStack.semanticContentAttribute = .forceLeftToRight
        } else {
            lblTitle.textAlignment = .left
            lblSubTitle.textAlignment = .left
            mainPanal.semanticContentAttribute = .forceRightToLeft
            mainStack.semanticContentAttribute = .forceRightToLeft
        }
        lblTitle.text = statistics?.title ?? string.empty
        lblSubTitle.text = statistics?.subTitle ?? string.empty
        icon.image = statistics?.icon
    }
    
    //MARK: - Actions
    
    
}
