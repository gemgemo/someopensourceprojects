
import UIKit
import Localize_Swift

final class AwardListImagesDetails: BaseController
{
    
    // MARK: - Constants
    
    //MARK: - Variables
    fileprivate var dataSource = Array<Photo>() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.pictures.reloadData()
            }
        }
    }
    
    internal var album: Album?, school: School?
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var picture: UIImageView! {
        didSet {
            picture.image = nil
        }
    }
    @IBOutlet fileprivate weak var lblDetails: UITextView!
    @IBOutlet fileprivate weak var pictures: UICollectionView! {
        didSet {
            pictures.delegate = self
            pictures.dataSource = self
            pictures.alwaysBounceHorizontal = true
            pictures.alwaysBounceVertical = false
        }
    }
    
    
    // MARK: - Overridden functions
    
    internal override func viewDidLoad() {
        super.viewDidLoad()
        
        configureCollectionView()
        if let albumId = album?.id  {
            loadData(from: Constants.BackendURLs.shared.awardListImagesLink(by: albumId))            
            lblDetails.text = "studentstakeawardeslan".localized()
            navigationItem.title = album?.title ?? String.empty
            lblTitle.text = album?.title ?? String.empty
        }
        
        if let schoolId = school?.id {
            loadData(from: Constants.BackendURLs.shared.schoolImagesLink(by: schoolId))
            navigationItem.title = school?.title ?? String.empty
            lblTitle.text = school?.title ?? String.empty
        }
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBar()
        view.backgroundColor = Color(red:0.141, green:0.141, blue:0.141, alpha:1)
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            lblTitle.textAlignment = .right
            lblDetails.textAlignment = .right
            pictures.semanticContentAttribute = .forceRightToLeft
        } else {
            lblTitle.textAlignment = .left
            lblDetails.textAlignment = .left
            pictures.semanticContentAttribute = .forceLeftToRight
        }
    }
    
    
    //MARK: - Actions
    
    @objc fileprivate func shareOnClick(_ sender: UIBarButtonItem) {
        share(this: dataSource.map { $0.link ?? string.empty })
    }
    
}



// MARK: - Helper functions

extension AwardListImagesDetails
{
    
    fileprivate func updateNavigationBar()-> void {
        updateNavigationBarDirections()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: getBackImage(), style: .plain, target: self, action: #selector(goBack(_:)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "sharing"), style: .plain, target: self, action: #selector(shareOnClick(_:)))
    }
    
    fileprivate func configureCollectionView()-> void {
        pictures.register(Constants.Nib.AwardImagesDetails, forCellWithReuseIdentifier: Constants.ReuseIdentifier.AwardImagesDetails)
    }
    
   
    
    fileprivate func loadData(from link: string)-> void {
        startSpin()
        print("link is: \(link)")
        Photo.getData(by: link, viewController: self) { [weak self] (photos) -> void in            
            if (!photos.isEmpty) {
                self?.dataSource = photos
                if let firstImageLink = photos.first?.link {
                    self?.picture.loadImage(from: firstImageLink) { [weak self](flag) in
                        // set first row is selected
                        self?.pictures.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .top)
                    }
                }
            }
            self?.stopSpin()
        }
    }
    
}


// MARK: - Collection view delegate & data source functions

extension AwardListImagesDetails: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    
    internal func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ReuseIdentifier.AwardImagesDetails, for: indexPath) as? AwardImagesDetailsCell {
            cell.photo = dataSource[indexPath.item]
            return cell
        }
        return UICollectionViewCell()
    }
    
    internal func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let imageLink = dataSource[indexPath.item].link {
            picture.loadImage(from: imageLink, onComplete: nil)
        }
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let dimentions = collectionView.bounds.height-8
        return Size(width: dimentions, height: dimentions)
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return Insets.zero
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
}
























