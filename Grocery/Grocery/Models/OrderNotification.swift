
import Foundation
import Gemo

internal enum OrderStatus: string {
    case accept = "accept", reject = "reject"
}

final class OrderNotification: Mapper<any>
{
    internal var storeId, userId, gender, username, userPhoto, id, name, phone, building, floor, address, type, time, date: string?, products = Array<NotificationProduct>()
    
    internal override func mapping(mapper: Mapper<any>) {
        storeId = mapper["id_matjer"]?.string
        userId = mapper["user_id"]?.string
        gender = mapper["gender"]?.string
        username = mapper["user_name"]?.string
        userPhoto = mapper["user_photo"]?.string
        id = mapper["id"]?.string
        name = mapper["name"]?.string
        phone = mapper["phone"]?.string
        building = mapper["building"]?.string
        floor = mapper["floor"]?.string
        address = mapper["addres"]?.string
        type = mapper["type_send"]?.string
        time = mapper["time_add"]?.string
        date = mapper["date_add"]?.string
        if let prods = mapper["Data"]?.array {
            prods.forEach { (objc) in
                guard let product = objc as? Dictionary<string, any> else { return }
                products.append(NotificationProduct(JSON: product))
            }
        }
    }
    
    
    internal class func fetchNotifications(_ completion: @escaping([OrderNotification])->())-> void {
        Gemo.gem.startSpinning()
        let parameters: [string: any] = [
            Misc.MainKey: Misc.MainValue,
            Misc.LanguageParameterName: Localizer.instance.current.rawValue,
            "id_matjer": UserDefaults.standard.string(forKey: Misc.GroceryId) ?? .empty
        ]
        Http.request(link: Urls.Notifications, method: .post, parameters: parameters)
            .response { (response) in
                let error = response.error
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("fetch notifications error: \(String(describing: error))")
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
//                print("fetched notifications: \(response.jsonString ?? .empty)")
                guard let json = response.result as? Array<Dictionary<string, any>> else {
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized())
                    return
                }
                completion(json.map { OrderNotification(JSON: $0) })
        }
    }
    
    internal class func action(this orderId: string, with action: string, _ completion: @escaping(OrderNotification)->())-> void {
        Gemo.gem.startSpinning()
        let parameters: [string: any] = [
            Misc.MainKey: Misc.MainValue,
            Misc.LanguageParameterName: Localizer.instance.current.rawValue,
            "id_matjer": UserDefaults.standard.string(forKey: Misc.GroceryId) ?? .empty,
            Misc.IdKey: orderId,
            action: action
        ]
        Http.request(link: Urls.Notifications, method: .post, parameters: parameters)
            .response { (response) in
                let error = response.error
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("set action for notification error: \(String(describing: error))")
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
                //print("taked action for notification response: \(response.jsonString ?? .empty)")
                guard let json = (response.result as? [Dictionary<string, any>])?.first else {
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized())
                    return
                }
                print("success action: \(json)")
                completion(OrderNotification(JSON: json))
        }
    }
    
}


final class NotificationProduct: Mapper<any>
{
    
    internal var id, name, quantity, photo: string?, price: int?
    
    internal override func mapping(mapper: Mapper<any>) {
        id = mapper["product_id"]?.string
        name = mapper["product_name"]?.string
        price = mapper["product_price"]?.integer
        quantity = mapper["quantity"]?.string
        photo = mapper["photo_1"]?.string
    }
    
}

































