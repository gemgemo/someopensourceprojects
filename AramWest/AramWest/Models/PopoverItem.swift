
import Foundation
import Alamofire

class PopoverItem: Object
{
    internal var id, arabicName, englishName, icon: string?
    
    // MARK: - Computed properties
    internal override var description: String {
        return "\n ID-> \(id), ArabicName-> \(arabicName), EnglishName-> \(englishName), Icon-> \(icon) \n"
    }
    
    init(dictionary: [string: any]) {
        self.id = dictionary["ID"] as? string
        self.arabicName = dictionary["nameA"] as? string
        self.englishName = dictionary["name"] as? string
        self.icon = dictionary["img"] as? string
    }
    
    override init() {
        super.init()
    }
    
}


final class MainCar: PopoverItem {
    
    internal class func getMainCars(from link: string, page: BaseController?, onComplete: @escaping([MainCar])->void)-> void {
        UIApplication.showIndicator(by: true)
        Alamofire.request(link)
            .responseJSON { (response) in
                let result = response.result
                if (result.error != nil) {
                    print("get main car \(result.error)")
                    //page?.showAlert(with: .empty, and: "erroralertlan".localized())
                    UIApplication.showIndicator(by: false)
                    return
                }
                if let value = result.value as? [string: any], let data = value["Data"] as? [[string: any]], let isSuccess = value["Success"] as? string {
                    if (isSuccess == "True") {
                        onComplete(data.map { MainCar(dictionary: $0) })
                    } else {
                        print("false value when fetch main car data")
                        UIApplication.showIndicator(by: false)
                        page?.showAlert(with: .empty, and: "erroralertlan".localized())
                    }
                } else {
                    print("nix data whene fetch main car data")
                    UIApplication.showIndicator(by: false)
                    page?.showAlert(with: .empty, and: "erroralertlan".localized())
                }
        }
    }
    
}

final class SubCar: PopoverItem {
    internal class func getSubCars(from link: string, by id: string, page: BaseController?, onComplete: @escaping([SubCar])->void)-> void {
        UIApplication.showIndicator(by: true)
        Alamofire.request(link, method: .post, parameters: ["type": id])
            .responseJSON { (response) in
                let result = response.result
                if (result.error != nil) {
                    print("get sub cars error is: \(result.error)")
                    UIApplication.showIndicator(by: false)
                    //page?.showAlert(with: .empty, and: "erroralertlan".localized())
                    return
                }
                
                if let value = result.value as? [string: any], let data = value["Data"] as? [[string: any]], let success = value["Success"] as? string {
                    //print(value)
                    if (success == "True") {
                        onComplete(data.map { SubCar(dictionary: $0) })
                    } else {
                        print("success false while get sub cars")
                        UIApplication.showIndicator(by: false)
                        page?.showAlert(with: .empty, and: "erroralertlan".localized())
                    }
                } else {
                    print("nix data when fetching sub cars")
                    UIApplication.showIndicator(by: false)
                    page?.showAlert(with: .empty, and: "erroralertlan".localized())
                }
                
        }
    }

}

final class DriverType: PopoverItem
{
    
    init(id: string, name: string, icon: string) {
        super.init()
        self.id = id
        self.arabicName = name
        self.englishName = name
        self.icon = icon
    }
    
    final internal class func getData()-> [DriverType] {
        var items = Array<DriverType>()
        items.append(DriverType(id: "0", name: "alllan".localized(), icon: ""))
        items.append(DriverType(id: "1", name: "Driverlan".localized(), icon: ""))
        items.append(DriverType(id: "2", name: "companylan".localized(), icon: ""))
        return items
    }
}


final class DriverMainCar: PopoverItem
{
    init(id: string, aName: string, eName: string, icon: string) {
        super.init()
        self.id = id
        self.arabicName = aName
        self.englishName = eName
        self.icon = icon
    }
}























