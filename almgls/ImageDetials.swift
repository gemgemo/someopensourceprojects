

import Foundation
public class ImageDetials{
    
    
    var id: String
    var get_id: String
    var photo_file: String
    
    init(id: String, get_id: String, photo_file:String) {
        self.id = id
        self.get_id = get_id
        self.photo_file = photo_file
    }
}
