
import UIKit
import Alamofire

class ImagesVC: UIViewController,
    UICollectionViewDataSource,
    UICollectionViewDelegate {
    
    var albums = Array<Album>()

    @IBOutlet weak var loadingLbl: UILabel!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var photoCollection: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Photos"
        photoCollection.delegate = self
        photoCollection.dataSource = self
        loadingView.isHidden = true
        shadowView.isHidden = true
        if Settings.lang == "ar" {
            self.title = "الصور"
            loadingLbl.text = "جاري التحميل..."
        }
        
        if (Settings.isConnected) {
            GetImages()
        } else {
            dialogUser(with: "لا يوجد انترنت")
        }
    }
    
    private func GetImages()
    {
        loadingView.isHidden = false
        shadowView.isHidden = false
        loadingView.layer.cornerRadius = 10
        loadingView.layer.masksToBounds = true
        loadingActivity.startAnimating()
        loadImages { [weak self] (cats) in
            self?.albums = cats
            DispatchQueue.main.async { [weak self] in
                    self?.loadingView.isHidden = true
                    self?.shadowView.isHidden = true
                    self?.loadingActivity.stopAnimating()
                    self?.photoCollection.reloadData()
            }
        }
  
        

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imagesCell", for: indexPath)
            as? Images {
            let album = albums[indexPath.row]
            cell.title.text = album.name ?? ""
            cell.imageCount.text = album.count?.description ?? ""
            cell.images.loadImage(from: album.cover ?? "", onComplete: nil)
           return cell
        }
        return UICollectionViewCell()

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return albums.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
        let size = self.photoCollection.frame.width
        return CGSize(width: size-15, height: 195)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ImageDetialsVC") as! ImageDetialsVC        
        let album = albums[indexPath.row]
        vc.album = album
        self.navigationController?.pushViewController(vc, animated: true)

        
    }
    

//http://hmmc.gov.sa/assets/uploads/thumbs/
    private func loadImages(onComplete: @escaping([Album])-> Void)-> Void {
        let link = "http://hmmc.gov.sa/Ws/GetPhotos"
        Alamofire.request(link).responseJSON { (response) in
            let result = response.result
            if (result.error != nil) {
                print("error occurred \(String(describing: result.error))")
                return
            }
            if let value = response.result.value as? [[String: Any]] {
                //print("images", value)
                onComplete(value.map { Album(dictionary: $0)})
            } else {
                print("null data")
            }
        }
    }      
    
    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        self.navigationController!.popViewController(animated: true)
    }

   

}


private let imageCache = NSCache<NSString,UIImage>()

internal extension UIImageView {
    
    internal func loadImage(from link: String, onComplete: ((_ isLoaded: Bool)-> ())?)-> () {
        image = nil
        if (link.isEmpty) {
            print("empty image link or invalid link")
            onComplete?(false)
            return
        }
        
        if let cachedImage = imageCache.object(forKey: link as NSString) {
            image = cachedImage
            onComplete?(true)
            return
        }
        if let url = URL(string: link as String) {
            URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
                if (error != nil) {
                    onComplete?(false)
                    
                    return
                }
                if let imageData = data, let imageToCache = UIImage(data: imageData) {
                    DispatchQueue.main.async { [weak self] in
                        // cache image
                        imageCache.setObject(imageToCache, forKey: link as NSString)
                        self?.image = imageToCache
                        onComplete?(true)
                        
                    }
                } else {
                    onComplete?(false)
                    
                }
                }.resume()
        }
    }
    
}






