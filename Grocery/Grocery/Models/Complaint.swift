
import Foundation
import Gemo


final class Complaint: Mapper<any>
{
    
    internal var id, photo, name, gender, email, title, content: string?
    
    internal override func mapping(mapper: Mapper<any>) {
        id = mapper["id"]?.string
        photo = mapper["photo_users"]?.string
        name = mapper["username"]?.string
        gender = mapper["gender"]?.string
        email = mapper["email"]?.string
        title = mapper["title"]?.string
        content = mapper["message"]?.string        
    }
    
    internal class func fetchComplaints(_ completion: @escaping([Complaint])->())-> void {
        Gemo.gem.startSpinning()
        let paramters: [string: any] = [
            Misc.MainKey: Misc.MainValue,
            Misc.LanguageParameterName: Localizer.instance.current.rawValue,
            "id_matjer": UserDefaults.standard.string(forKey: Misc.GroceryId) ?? .empty
        ]
        Http.request(link: Urls.Complaints, method: .post, parameters: paramters)
            .response { (response) in
                let error = response.error
//                print("complaints data: \(response.jsonString ?? .empty)")
                if (error != nil) {
                    Gemo.gem.stopSpinning()
                    print("fetch complaints error: \(String(describing: error))")
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: error!.localizedDescription)
                    return
                }
                Gemo.gem.stopSpinning()
                guard let json = response.result as? [Dictionary<string, any>] else {
                    Gemo.gem.showAlert(withTitle: Localization.Warning.localized(), message: Localization.NoData.localized())
                    return
                }
                completion(json.map { Complaint(JSON: $0) })
                
        }
    }
    
}












































