
import UIKit
import Gemo
import KYDrawerController
import MobileCoreServices
import CoreLocation
import GoogleMaps

class BaseController: UIViewController {
    
    
    // MARK: - Constants
    
    
    // MARK: - Variables
    
    
    // MARK: - Outlets
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
    }
    
    internal func updateUi()-> void {
        navigationItem.hidesBackButton = true
    }
    
    
    // MARK: - Actions
    
    @objc fileprivate func backOnClick(_ sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc fileprivate func openMenu(_ sender: UIBarButtonItem)-> void {
        if (Usertype(rawValue: UserDefaults.standard.integer(forKey: Misc.UserTypeKey)) ?? .sub == .sub) {
            let x = (Localizer.instance.current == .arabic) ? UIScreen.main.bounds.width - 5 : 0.0
            let items: [PopupMainMenu] = [PopupMainMenu(title: Localization.Reports.localized()),
                                          PopupMainMenu(title: Localization.AboutApp.localized()),
                                          PopupMainMenu(title: Localization.ContactUs.localized()),
                                          PopupMainMenu(title: Localization.Complaints.localized()),
                                          PopupMainMenu(title: Localization.Logout.localized())]
            showPopover(in: Rect(x: x, y: 0, width: 0, height: 0), with: items, with: Size(width: 180, height: 5*45)) { [weak self] (index) in
                switch (index) {
                case 0: // reports per car
                    guard let reportsPage = self?.storyboard?.instantiateViewController(withIdentifier: Storyboards.ReportsPerCar) as? ReportsPerCar else {
                        return
                    }
                    let car = Car()
                    car.id = UserDefaults.standard.string(forKey: Misc.CarId)
                    reportsPage.car = car
                    self?.navigate(to: reportsPage, false)
                    
                case 1: // about app
                    guard let aboutAppPage = self?.storyboard?.instantiateViewController(withIdentifier: Storyboards.AboutApp) as? AboutApp else {
                        return
                    }
                    self?.navigate(to: aboutAppPage, false)
                    
                case 2: // contact us
                    guard let contactUsPage = self?.storyboard?.instantiateViewController(withIdentifier: Storyboards.ContactUs) as? ContactUs else {
                        return
                    }
                    self?.navigate(to: contactUsPage, false)
                    
                case 3: // complaints
                    guard let complaintsPage = self?.storyboard?.instantiateViewController(withIdentifier: Storyboards.SendComplaint) as? SendComplaint else {
                        return
                    }
                    self?.navigate(to: complaintsPage, false)
                    
                case 4: // logout
                    self?.logout()
                    
                default:
                    break
                }
            }
        } else {
            (parent?.parent as? KYDrawerController)?.setDrawerState(.opened, animated: true)
        }
    }

}


// MARK: - Helper functions

extension BaseController {
    
    func logout() -> void {
        print("logout app")
        let db = UserDefaults.standard
        db.removeObject(forKey: Misc.UserLoggedInKey)
        db.removeObject(forKey: Misc.Name)
        db.removeObject(forKey: Misc.Email)
        db.removeObject(forKey: Misc.Phone)
        db.removeObject(forKey: Misc.UserId)
        db.removeObject(forKey: Misc.Username)
        db.removeObject(forKey: Misc.Latitude)
        db.removeObject(forKey: Misc.Longitude)
        db.removeObject(forKey: Misc.Address)
        db.synchronize()
        let registrationStoryboard = UIStoryboard(name: Storyboards.Registration, bundle: nil)
        UIApplication.shared.keyWindow?.rootViewController = registrationStoryboard.instantiateInitialViewController()
    }
    
    internal func move(this page: BaseController, in panel: UIView)-> void {
        page.remove()
        page.view.frame = panel.bounds
        page.willMove(toParentViewController: self)
        panel.addSubview(page.view)
        addChildViewController(page)
        page.didMove(toParentViewController: self)
    }
    
    internal func remove()-> void  {
        willMove(toParentViewController: nil)
        view.removeFromSuperview()
        removeFromParentViewController()
    }
    
    internal func navigate(to page: BaseController, _ animated: bool = true)-> void {
        navigationController?.pushViewController(page, animated: animated)
    }
    
    internal func setupNavigationBar(isLogo: bool, isMainPage: bool, title: string? = .empty)-> void {
        let btnBack = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back"), style: .plain, target: self, action: #selector(backOnClick(_:)))
        let titlePanel = UIView(frame: Rect(x: 0, y: 0, width: view.frame.width-40, height: 40))
        let logo = UIImageView(image: #imageLiteral(resourceName: "logo"))
        if (isLogo) {
            logo.contentMode = .scaleAspectFit
            logo.translatesAutoresizingMaskIntoConstraints = false
            titlePanel.addSubview(logo)
            logo.widthAnchor.constraint(equalToConstant: 70.0).isActive = true
            logo.heightAnchor.constraint(equalToConstant: 40).isActive = true
            logo.centerYAnchor.constraint(equalTo: titlePanel.centerYAnchor, constant: 0).isActive = true
            titlePanel.backgroundColor = .clear
            if (Localizer.instance.current == .arabic) {
                logo.rightAnchor.constraint(equalTo: titlePanel.rightAnchor, constant: 0).isActive = true
            } else {
                logo.leftAnchor.constraint(equalTo: titlePanel.leftAnchor, constant: 0).isActive = true
            }
        } else {
            let lblTitle = UILabel()
            lblTitle.translatesAutoresizingMaskIntoConstraints = false
            lblTitle.font = Font.notoKufi(of: 17.0)
            lblTitle.text = title ?? .empty
            titlePanel.addSubview(lblTitle)
            lblTitle.centerYAnchor.constraint(equalTo: titlePanel.centerYAnchor, constant: 0).isActive = true
            if (Localizer.instance.current == .arabic) {
                lblTitle.textAlignment = .right
                lblTitle.rightAnchor.constraint(equalTo: titlePanel.rightAnchor, constant: -8).isActive = true
            } else {
                lblTitle.textAlignment = .left
                lblTitle.leftAnchor.constraint(equalTo: titlePanel.leftAnchor, constant: 8).isActive = true
            }
        }
        if (!isMainPage) {
            if (Localizer.instance.current == .arabic) {
                btnBack.image = #imageLiteral(resourceName: "ic_back").withRenderingMode(.alwaysOriginal)
                navigationItem.rightBarButtonItem = btnBack
            } else {
                btnBack.image = #imageLiteral(resourceName: "ic_leftback").withRenderingMode(.alwaysOriginal)
                navigationItem.leftBarButtonItem = btnBack
            }
        } else {
            let userType = Usertype(rawValue: UserDefaults.standard.integer(forKey: Misc.UserTypeKey)) ?? .sub
            let btnMenu = UIBarButtonItem(image: nil, style: .plain, target: self, action: #selector(openMenu))
            if (Localizer.instance.current == .arabic) {
                if (userType == .main) {
                    btnMenu.image = #imageLiteral(resourceName: "ic_menu").withRenderingMode(.alwaysOriginal)
                } else {
                    btnMenu.image = #imageLiteral(resourceName: "ic_more").withRenderingMode(.alwaysOriginal)
                }
                navigationItem.rightBarButtonItem = btnMenu
            } else {
                if (userType == .main) {
                    btnMenu.image = #imageLiteral(resourceName: "ic_leftmenu").withRenderingMode(.alwaysOriginal)
                } else {
                    btnMenu.image = #imageLiteral(resourceName: "ic_more").withRenderingMode(.alwaysOriginal)
                }
                
                navigationItem.leftBarButtonItem = btnMenu
            }
        }
        navigationItem.titleView = titlePanel
    }
    
    internal func displayAlert(with message: string?)-> void {
        Threads.main { [weak self] in
            let alert = UIAlertController(title: Localization.Warning.localized(), message: message, preferredStyle: .alert)
            alert.view.tintColor = .main
            alert.addAction(UIAlertAction(title: Localization.Submit.localized(), style: .cancel))
            self?.present(alert, animated: true)
        }
    }
    
    func openImagePicker(delegate: UIImagePickerControllerDelegate & UINavigationControllerDelegate)-> void {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.delegate = delegate
        imagePicker.mediaTypes = [kUTTypeImage] as [string]
        let alert = UIAlertController(title: Localization.ChooseImageTitle.localized(), message: Localization.ChooseImageMessage.localized(), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Localization.Camera.localized(), style: .default, handler: { [weak self] (action) in
            imagePicker.sourceType = .camera
            self?.present(imagePicker, animated: true)
        }))
        alert.addAction(UIAlertAction(title: Localization.PhotoLibrary.localized(), style: .default, handler: { [weak self] (action) in
            imagePicker.sourceType = .photoLibrary
            self?.present(imagePicker, animated: true)
        }))
        alert.addAction(UIAlertAction(title: Localization.Cancel.localized(), style: .cancel))
        present(alert, animated: true)
    }
    
    func address(for point: CLLocationCoordinate2D, _ completion: @escaping(string)->())-> void {
        GMSGeocoder().reverseGeocodeCoordinate(point) { (response, error) in
            if (error != nil) {
                print("get address error: \(string(describing: error))")
                return
            }
            Threads.main { completion(response?.firstResult()?.lines?.joined(separator: ",") ?? .empty) }
        }
    }
    
    func showPopover(in rect: Rect, with items: [Item], with size: Size = Size(width: 250, height: 300), onSelect: @escaping(Int)->())-> void {
        guard let popup = storyboard?.instantiateViewController(withIdentifier: Storyboards.Popup) as? Popup else { return }
        popup.modalPresentationStyle = .popover
        popup.preferredContentSize = size//Size(width: 250, height: 300)
        popup.popoverPresentationController?.delegate = self
        popup.popoverPresentationController?.sourceView = view
        popup.popoverPresentationController?.sourceRect = rect
        popup.popoverPresentationController?.permittedArrowDirections = .unknown
        popup.items = items
        popup.didSelect = onSelect
        present(popup, animated: true)
        
    }
    
}

// MARK: - Popover delegate functions

extension BaseController: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
}



























