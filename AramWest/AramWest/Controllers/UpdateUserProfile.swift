
import UIKit
import Localize_Swift

class UpdateUserProfile: BaseController
{
    // MARK: - Constants
    
    // MARK: - Variables
    
    internal var userName, userEmail, userPhone: string?
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate weak var txfName: TextField! {
        didSet {
            txfName.delegate = self
        }
    }
    @IBOutlet fileprivate weak var txfEmail: TextField! {
        didSet {
            txfEmail.delegate = self
        }
    }
    @IBOutlet fileprivate weak var txfPhone: TextField! {
        didSet {
            txfPhone.delegate = self
        }
    }
    @IBOutlet fileprivate weak var btnEdit: Button!
    
    
    // MARK: - Overridden functions
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUi()
    }
    
    internal override func updateNavgationBar() {
        navigationItem.title = "editprofiletitlelannav".localized()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ac_back"), style: .plain, target: self, action: #selector(goBack(_:)))
    }
    
    // MARK: - Actions 
    
    @IBAction private func editOnClick(_ sender: Button) {
        guard !name.isEmpty && !phone.isEmpty && !email.isEmpty else {
            showAlert(with: string.empty, and: "reqiurefieldlan".localized())
            return
        }
        guard email.isEmail else {
            showAlert(with: string.empty, and: "invalidemaillan".localized())
            return
        }
        let params: [string: string] = [
            "ID": (UserDefaults.standard.value(forKey: Constants.Misc.UserId) as? string) ?? string.empty,
            "fullname": name,
            "phone": phone,
            "email": email
        ]
        startSpin()
        User.instance.updateData(with: params, page: self) { [weak self] (isSuccess) -> void in
            if (!isSuccess) {
                self?.stopSpin()
                self?.showAlert(with: string.empty, and: "updateprofilemessageerrorlan".localized())
                return
            }
            //self?.showAlert(with: string.empty, and: "successproccesslan".localized())
            self?.stopSpin()
            _ = self?.navigationController?.popViewController(animated: true)
        }

    }
    

}



// MARK: - Helper functions

extension UpdateUserProfile
{
    
    fileprivate func updateUi()-> void {
        txfName.text = userName ?? .empty
        txfEmail.text = userEmail ?? .empty
        txfPhone.text = userPhone ?? .empty
        btnEdit.setTitle("editprofileupdatetitlelan".localized(), for: .normal)
        if (Localize.currentLanguage() == Constants.Language.Arabic) {
            txfName.textAlignment = .right
            txfEmail.textAlignment = .right
            txfPhone.textAlignment = .right
        } else {
            txfName.textAlignment = .left
            txfEmail.textAlignment = .left
            txfPhone.textAlignment = .left
        }
    }
    
}


// MARK: - Computed properties

extension UpdateUserProfile
{
    
    internal var name: string {
        return txfName.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    internal var email: string {
        return txfEmail.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
    internal var phone: string {
        return txfPhone.text?.trimmingCharacters(in: .whitespaces) ?? .empty
    }
    
}


// MARK: - Text Field delegate functions

extension UpdateUserProfile: UITextFieldDelegate
{
    
    internal func textFieldDidBeginEditing(_ textField: UITextField) {
        //textField.text = .empty
    }
    
    internal func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}


















