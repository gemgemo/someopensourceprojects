
import Foundation
import UIKit


internal extension String {
    
    internal static var empty: string {
        return ""
    }
    
    internal static var nixStringMessage: string {
        return "returns none data"
    }
    
    internal var isLink: Bool {
        return contains("http://") || contains("https://")
    }
    
    internal var isEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPredicate.evaluate(with: self)
    }
    
}

internal extension Number {
    
    internal static var zero: Number {
        return 0
    }
    
}



// image caching
private let imageCache = NSCache<NSString,UIImage>()
internal extension UIImageView {
    
    internal func loadImage(from link: string, onComplete: ((_ isLoaded: Bool)-> void)?)-> void {
        UIApplication.showIndicator(by: true)
        image = nil
        if (!link.isLink || link.isEmpty) {
            print("empty link or invalid link")
            onComplete?(false)
            return
        }
        
        if let cachedImage = imageCache.object(forKey: link as NSString) {
            image = cachedImage
            onComplete?(true)
            UIApplication.showIndicator(by: false)
            return
        }
        // download and cache image
        // download
        if let url = URL(string: link as string) {
            URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
                if (error != nil) {
                    onComplete?(false)
                    UIApplication.showIndicator(by: false)
                    return
                }
                if let imageData = data, let imageToCache = UIImage(data: imageData) {
                    DispatchQueue.main.async { [weak self] in
                        // cache image
                        imageCache.setObject(imageToCache, forKey: link as NSString)
                        self?.image = imageToCache
                        onComplete?(true)
                        UIApplication.showIndicator(by: false)
                    }
                } else {
                    onComplete?(false)
                    UIApplication.showIndicator(by: false)
                }
                }.resume()
        }
    }
    
}



internal extension UIApplication {
    
    internal class func showIndicator(by isOn: bool)-> void {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = isOn
        }
    }
    
    internal class func playActions()-> void {
        if (UIApplication.shared.isIgnoringInteractionEvents) {
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
    
    internal class func pauseActions()-> void {
        if (!UIApplication.shared.isIgnoringInteractionEvents) {
            UIApplication.shared.beginIgnoringInteractionEvents()
        }
    }
    
    internal class func open(this link: string)-> void {
        guard let url = URL(string: link), UIApplication.shared.canOpenURL(url) else {
            print("can't open link")
            return
        }
        UIApplication.shared.openURL(url)
    }
    
}

// uicolor

internal extension UIColor {
    
    internal class func rgb(red: Int, green: Int, blue: Int, alpha: Float)-> UIColor {
        return UIColor(colorLiteralRed: Float(red)/255, green: Float(green)/255, blue: Float(blue)/255, alpha: alpha)
    }
    
    internal convenience init?(hex hash: string) {
        let r, g, b, a: CGFloat
        if (hash.hasPrefix("#")) {
            let start = hash.index(hash.startIndex, offsetBy: 1)
            let hexColor = hash.substring(from: start)
            let scanner = Scanner(string: hexColor)
            var hexNumber: UInt64 = 0
            if (scanner.scanHexInt64(&hexNumber)) {
                r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                a = CGFloat(hexNumber & 0x000000ff) / 255
                
                self.init(red: r, green: g, blue: b, alpha: a)
                return
            }
        }
        return nil
    }
    
    
    
}


// uiview

internal extension UIView {
    
    internal func setShadow(with offset: CGSize, radius: CGFloat, opacity: CGFloat, color: Color)-> Void {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOffset = offset
        layer.shadowRadius = radius
        layer.shadowOpacity = Float(opacity)
    }
    
}

internal typealias string = String
internal typealias void = Void
internal typealias any = Any
internal typealias double = Double
internal typealias bool = Bool
internal typealias float = Float
internal typealias int = Int
internal typealias Number = NSNumber
internal typealias object = AnyObject
internal typealias cgFloat = CGFloat
internal typealias Color = UIColor
internal typealias Font = UIFont
internal typealias Size = CGSize
internal typealias Rect = CGRect
internal typealias Insets = UIEdgeInsets
internal typealias uint = UInt




@IBDesignable
internal class TextView: UITextView, UITextViewDelegate
{
    @IBInspectable
    internal var placeholder = "placholder"
    
    @IBInspectable
    internal var placeholderColor = Color.lightGray
    
    @IBInspectable
    internal var color = Color.black
    
    
    internal override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        setup()
    }
    
    required internal init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup()-> void {
        delegate = self
        text = placeholder
        textColor = placeholderColor
    }
    
    internal func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text == placeholder && textView.textColor == placeholderColor) {
            textView.text = string.empty
            textView.textColor = color
        }
    }
    
    internal func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text.isEmpty) {
            textView.text = placeholder
            textView.textColor = placeholderColor
        }
    }
    
    
    
    
}


extension Size
{
    
    
    init(right: cgFloat, top: cgFloat) {
        width = right
        height = top
    }
    
}






























