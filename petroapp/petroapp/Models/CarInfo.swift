
import Foundation
import Gemo

class CarInfo: Mapper<any>, Item {
  
    var id = String.empty
    var title = String.empty
    
    override func mapping(mapper: Mapper<any>) {
        id = mapper["id"]?.string ?? .empty
        title = mapper["title"]?.string ?? .empty
    }
    
    internal class func fetchInfo(for link: string, _ page: BaseController?, _ completion: @escaping([CarInfo])->())-> void {
        Http.request(link: link, method: .post, parameters: [Misc.MainKey: Misc.MainValue, Misc.LanguageKey: Localizer.instance.current.rawValue])
            .response { (response) in
                let error = response.error
                if (error != nil) {
                    print("\(#function) error: \(error!)")
                    return
                }
                guard let json = response.result as? Array<Dictionary<string, any>> else {
                    page?.displayAlert(with: Localization.NoData.localized())
                    return
                }
                completion(json.map { CarInfo(JSON: $0) })
        }
    }
    
    
}







































