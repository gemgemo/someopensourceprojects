//
//  AdsPageCollectionViewCell.swift
//  Q8School
//
//  Created by mac on 12/8/16.
//  Copyright © 2016 Atiaf. All rights reserved.
//

import UIKit

class AdsPageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var adsImages: UIImageView!
    
}
